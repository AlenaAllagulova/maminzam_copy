<?php

namespace mastiffApp\tamaranga\modules\rating;

use mastiffApp\tamaranga\Module;

class Average extends Module
{
    private static $oInstance = null;

    public static function getInstance()
    {
        if (is_null(self::$oInstance)) {
            self::$oInstance = new self();
        }
        return self::$oInstance;
    }

    /**
     * Подсчет усредненнго рейтинга звёзд по качествам
     * и общего усредненного рейтинга по усредненным качествам
     * @param \Illuminate\Database\Eloquent\Collection $oRatingOpinions
     * @return array
     */
    public static function getCalc(\Illuminate\Database\Eloquent\Collection $oRatingOpinions)
    {
        $aData = [];
        foreach ($oRatingOpinions as $item) {
            $aStars = (static::getInstance()->isSerialized($item->stars)) ? unserialize($item->stars) : [];
            if (empty($aStars)) {
                continue;
            }
            if ( ! isset($aData[$item->user_id])) {
                $aData[$item->user_id] = [
                    'stars' => [],
                    'total' => 0,
                    'iterations' => 1
                ];
                foreach ($aStars as $k => $v) {
                    $aData[$item->user_id]['stars'][$k] = $v;
                }
            } else {
                foreach ($aStars as $k => $v) {
                    $aData[$item->user_id]['stars'][$k] += $v;
                }
                $aData[$item->user_id]['iterations']++;
            }
        }
        return $aData;
    }
}