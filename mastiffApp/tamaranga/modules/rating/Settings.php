<?php

namespace mastiffApp\tamaranga\modules\rating;

/**
 * Class Settings
 * @package mastiffApp\tamaranga\modules\rating
 */
class Settings
{
    private static $aSettings = null;
    private static $oInstance = null;

    public $name; // Название настройки
    public $value; // Значение настройки
    public $is; // Статус настройки относительно $value

    /**
     * Положительный отзыв
     * @return $this
     */
    public function getPositiveOpinion()
    {
        $value = \config::sys('users.rating.opinions.positive');
        return $this
            ->setName(__FUNCTION__)
            ->setValue($value)
            ->setActive( ! empty($value));
    }

    /**
     * Отрицательный отзыв
     * @return $this
     */
    public function getNegativeOpinion()
    {
        $value = \config::sys('users.rating.opinions.negative');
        return $this
            ->setName(__FUNCTION__)
            ->setValue($value)
            ->setActive( ! empty($value));
    }

    /**
     * Рейтинг за одну звезду
     * @return $this
     */
    public function getRatingOneStar()
    {
        return $this->setNameValueActive(__FUNCTION__, self::getRatingSetting(__FUNCTION__));
    }

    /**
     * Первый интервал дней
     * @return $this
     */
    public function getIntervalOne()
    {
        return $this->setNameValueActive(__FUNCTION__, self::getRatingSetting(__FUNCTION__));
    }

    /**
     * Второй интервал дней
     * @return $this
     */
    public function getIntervalTwo()
    {
        return $this->setNameValueActive(__FUNCTION__, self::getRatingSetting(__FUNCTION__));
    }

    /**
     * Третий интервал дней
     * @return $this
     */
    public function getIntervalTree()
    {
        return $this->setNameValueActive(__FUNCTION__, self::getRatingSetting(__FUNCTION__));
    }

    /**
     * Формула сортировки
     * @return Formula
     */
    public function getFormula()
    {
        return Formula::getInstance(
            $this->setNameValueActive(__FUNCTION__, self::getRatingSetting(__FUNCTION__))
        );
    }

    /**
     * Подготавливает формулу
     * @return Settings
     */
    public function getFormulaPrepare()
    {
        return $this->setNameValueActive(__FUNCTION__, self::getRatingSetting(__FUNCTION__));
    }

    /**
     * Последний запуск крона Rating::cronUsersPositionUpdate()
     * @return Settings
     */
    public function getLastActionCronPosition()
    {
        return $this->setNameValueActive(__FUNCTION__, self::getRatingSetting(__FUNCTION__));
    }

    /**
     * Последний запуск крона Rating::cronUserAverageRatingUpdate()
     * @return Settings
     */
    public function getLastActionCronAverage()
    {
        return $this->setNameValueActive(__FUNCTION__, self::getRatingSetting(__FUNCTION__));
    }

    /**
     * @return Settings
     */
    public static function get()
    {
        return self::getInstance();
    }

    /**
     * Set name, value and is
     * @param $name
     * @param null $value
     * @param null $active
     * @return $this
     */
    private function setNameValueActive($name, $value = null, $active = null)
    {
        return $this->setName($name)->setValue($value)->setActive(is_null($active) ?  ! empty($value) : $active);
    }

    /**
     * Настройки из bff_config для Rating
     * @return array|null
     */
    private static function getRatingSettings()
    {
        if (is_null(self::$aSettings)) {
            self::$aSettings = \Rating::i()->configLoad();
        }
        return self::$aSettings;
    }

    /**
     * Настройки из bff_config для Rating
     * @return array|null
     */
    private static function getRatingSetting($name)
    {
        $aSettings = self::getRatingSettings();
        if (array_key_exists($name, $aSettings)) {
            return $aSettings[$name];
        }
        return null;
    }

    /**
     * @return Settings
     */
    private static function getInstance()
    {
        if (is_null(self::$oInstance)) {
            self::$oInstance = new self();
        }
        return self::$oInstance;
    }

    private function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    private function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    private function setActive($active)
    {
        $this->is = $active;
        return $this;
    }
}