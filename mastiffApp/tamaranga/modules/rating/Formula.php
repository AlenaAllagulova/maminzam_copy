<?php

namespace mastiffApp\tamaranga\modules\rating;

use \Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use mastiffApp\db\models\Opinions;
use mastiffApp\db\models\OrdersOffers;
use mastiffApp\db\models\Users;

/**
 * Class Formula
 * @package mastiffApp\tamaranga\modules\rating
 */
class Formula
{
    const KEY_OPINIONS_POSITIVE = 'OP'; # Количествово положительных отзывов оставленных пользователю;
    const KEY_OPINIONS_NEGATIVE = 'ON'; # Количествово отрицательных отзывов оставленных пользователю;
    const KEY_RATING_USER = 'RU'; # рейтинг пользователя;
    const KEY_DAYS_INTERVAL_ONE = 'DI1'; # первый интервал в днях;
    const KEY_DAYS_INTERVAL_TWO = 'DI2'; # первый интервал в днях;
    const KEY_DAYS_INTERVAL_TREE = 'DI3'; # первый интервал в днях;
    const KEY_LAST_OPINIONS = 'LO'; # кол-во оставленных предложений пользователю;
    const OPERATOR_MODIFIER = '|'; # Приминить модификатор к параметру (вертикальная черта)
    const OPERATOR_RIGHT_BRACE = '{';
    const OPERATOR_LEFT_BRACE = '}';

    private static $oInstance = null;

    /**
     * @var Settings
     */
    public $formula = null;

    public $OP = self::KEY_OPINIONS_POSITIVE;
    public $ON = self::KEY_OPINIONS_NEGATIVE;
    public $RU = self::KEY_RATING_USER;
    public $DI1 = self::KEY_DAYS_INTERVAL_ONE;
    public $DI2 = self::KEY_DAYS_INTERVAL_TWO;
    public $DI3 = self::KEY_DAYS_INTERVAL_TREE;
    public $LO = self::KEY_LAST_OPINIONS;
    public $MODIFIER = self::OPERATOR_MODIFIER;
    public $RIGHT_BRACE = self::OPERATOR_RIGHT_BRACE;
    public $LEFT_BRACE = self::OPERATOR_LEFT_BRACE;

    /**
     * Массив ключей
     * @return array
     */
    public function keys()
    {
        return self::getKeys();
    }

    /**
     * Массив ключей
     * @return array
     */
    public static function getKeys()
    {
        return [
            self::KEY_OPINIONS_POSITIVE => _t('','Количество положительных отзывов оставленных пользователю (ключ)'),
            self::KEY_OPINIONS_NEGATIVE => _t('','Количество отрицательных отзывов оставленных пользователю (ключ)'),
            self::KEY_RATING_USER => _t('','Рейтинг пользователя (ключ)'),
            self::KEY_LAST_OPINIONS => _t('','Количество оставленных предложений пользователем к заказам (ключ)'),
            self::KEY_DAYS_INTERVAL_ONE => _t('','Первый интервал в днях (модификатор)'),
            self::KEY_DAYS_INTERVAL_TWO => _t('','Воторой интервал в днях (модификатор)'),
            self::KEY_DAYS_INTERVAL_TREE => _t('','Третий интервал в днях (модификатор)'),
        ];
    }

    /**
     * Instance
     * @param Settings $oFormula
     * @return Formula
     */
    public static function getInstance(Settings $oFormula)
    {
        if (is_null(self::$oInstance)) {
            self::$oInstance = new self();
            self::$oInstance->formula = $oFormula;
        }
        return self::$oInstance;
    }

    /**
     * Модификаторы
     * @param $sItem
     * @return array|null
     */
    public function getModifier($sItem)
    {
        switch (true) {
            case preg_match('/'.$this->DI1.'/', $sItem):
                return $this->getModifierItem(
                    $this->DI1,
                    Settings::get()->getIntervalOne()->value
                );
            break;
            case preg_match('/'.$this->DI2.'/', $sItem):
                return $this->getModifierItem(
                    $this->DI2,
                    Settings::get()->getIntervalTwo()->value,
                    Settings::get()->getIntervalOne()->value
                );
            break;
            case preg_match('/'.$this->DI3.'/', $sItem):
                return $this->getModifierItem(
                    $this->DI3,
                    Settings::get()->getIntervalTree()->value,
                    Settings::get()->getIntervalTwo()->value
                );
            break;
            default:
                return null;
        }
    }

    /**
     * Элемент модификатора
     * @param $sName
     * @param null $sFrom
     * @param null $sTo
     * @return array
     */
    private function getModifierItem($sName, $sFrom = null, $sTo = null)
    {
        return [
            0 => [
                'name'  => $sName,
                'col'   => 'created',
                'from'  => '(DATE(NOW()) - INTERVAL ' . (is_null($sFrom) ? 0 : $sFrom) . ' DAY)',
                'to'    => '(DATE(NOW()) - INTERVAL ' . (is_null($sTo) ? 0 : $sTo) . ' DAY)'
            ],
            'or' => [
                'name'  => $sName,
                'col'   => 'modified',
                'from'  => '(DATE(NOW()) - INTERVAL ' . (is_null($sFrom) ? 0 : $sFrom) . ' DAY)',
                'to'    => '(DATE(NOW()) - INTERVAL ' . (is_null($sTo) ? 0 : $sTo) . ' DAY)'
            ]
        ];
    }

    /**
     * Поготовка формулы
     * @param null $sFormula - формула
     * @return array
     */
    public function convertToArray($sFormula = null)
    {
        if (is_null($sFormula)) {
            $this->initFormula();
            $sFormula = $this->formula->value;
        }
        $aFormulaPrepare = preg_split('/[{*^}]/i', $sFormula, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        $aFormulaKeys = [];
        foreach ($aFormulaPrepare as $item) {
            if (preg_match('/['.$this->OP.'|'.$this->ON.'|'.$this->LO.'|'.$this->RU.']/', $item)) {
                $aFormulaKeys[$item] = null;
                switch (true) {
                    case preg_match('/'.$this->OP.'/', $item):
                        $aFormulaKeys[$item]['key'] = [
                            'name' => $this->OP,
                            'col' => 'type',
                            'op' => '=',
                            'val' => \Opinions::TYPE_POSITIVE
                        ];
                        break;
                    case preg_match('/'.$this->ON.'/', $item):
                        $aFormulaKeys[$item]['key'] = [
                            'name' => $this->ON,
                            'col' => 'type',
                            'op' => '=',
                            'val' => \Opinions::TYPE_NEGATIVE
                        ];
                        break;
                    case preg_match('/'.$this->LO.'/', $item):
                        $aFormulaKeys[$item]['key'] = [
                            'name' => $this->LO,
                            'col' => 'status',
                            'op' => '=',
                            'val' => \Orders::OFFER_STATUS_NEW
                        ];
                        break;
                    case preg_match('/'.$this->RU.'/', $item):
                        $aFormulaKeys[$item]['key'] = [
                            'name' => $this->RU,
                            'col' => 'rating',
                        ];
                        break;
                }
                if (($aMod = $this->getModifier($item)) && ( ! empty($aMod))) {
                    $aFormulaKeys[$item]['mod'] = $aMod;
                }
            }
        }
        return $aFormulaKeys;
    }

    public function getFormulaCalculate(Builder $oUser)
    {
        $this->initFormula();
        $oUser = $oUser->first();
        $aFormula = Settings::get()->getFormulaPrepare()->value;
        $sFormulaUser = $this->formula->value;

        if (is_array($aFormula)) {
            foreach ($aFormula as $key => $value) {
                if ( ! isset($value['key'])) {
                    continue;
                }
                $oQuery = null;
                switch ($value['key']['name']) {
                    case ($this->OP):
                    case ($this->ON):
                        $oQuery = Opinions::query()
                            ->where('user_id', $oUser->user_id)
                            ->where(
                                $value['key']['col'],
                                $value['key']['op'],
                                $value['key']['val']
                            );
                        break;
                    case ($this->LO):
                        $oQuery = OrdersOffers::query()
                            ->where('user_id', $oUser->user_id)
                            ->where(
                                $value['key']['col'],
                                $value['key']['op'],
                                $value['key']['val']
                            );
                        break;
                    case ($this->RU):
                        $sFormulaUser = str_replace('{'.$key.'}', \Users::rating($oUser->rating, $oUser->pro), $sFormulaUser);
                        break;
                }

                if ( ! is_null($oQuery)) {
                    if (isset($value['mod']) && ! empty($value['mod'])) {
                        $sSqlBind = '';
                        foreach ($value['mod'] as $operand => $mod) {
                            if (!empty($sSqlBind)) {
                                $sSqlBind .= ' ' . $operand . ' ';
                            }
                            $sSqlBind .= $mod['col'] . ' between ' . $mod['from'] . ' and ' . $mod['to'];
                        }
                        if (!empty($sSqlBind)) {
                            $oQuery = $oQuery
                                ->whereRaw('(' . $sSqlBind . ')');
                        }
                    }
                    $sFormulaUser = str_replace('{'.$key.'}', $oQuery->count(), $sFormulaUser);
                }
            }
        }

        $sCalculationResult = eval("return ($sFormulaUser);");
        return $sCalculationResult;
    }

    /**
     * Инициализация объекта
     * @return $this
     */
    private function initFormula()
    {
        if (is_null($this->formula)) {
            $this->formula = Settings::get()->getFormula();
        }
        return $this;
    }
}