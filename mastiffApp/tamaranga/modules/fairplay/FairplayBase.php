<?php


namespace mastiffApp\tamaranga\modules\fairplay;

use mastiffApp\db\models\FairplayWorkflows;

class FairplayBase extends \Module
{
    public function getOrderId($aData)
    {
        $nOrderId = 0;
        if (empty($aData)) {
            return $nOrderId;
        }
        if (isset($aData['order']['id'])) {
            $nOrderId = $aData['order']['id'];
        } elseif (isset($aData['id'])) {
            $oFairplayWorkflows = FairplayWorkflows::query()
                ->where('id', $aData['id']);
            if ($oFairplayWorkflows->exists()) {
                $nOrderId = $oFairplayWorkflows
                    ->first()->order_id;
            }
        }
        return $nOrderId;
    }
}