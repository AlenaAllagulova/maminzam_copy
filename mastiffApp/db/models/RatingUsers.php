<?php

namespace mastiffApp\db\models;

use mastiffApp\db\models\orm\OrmModel;

/**
 * Class RatingValues
 * @package mastiffApp\db\models
 *
 * @property integer    $id
 * @property integer    $user_id
 * @property integer    $total_average
 * @property string     $stars_average
 * @property string     $modified
 */
class RatingUsers extends OrmModel
{
    protected $table = 'rating_users';
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function user()
    {
        return $this->hasOne('mastiffApp\db\models\Users', 'user_id', 'user_id');
    }
}