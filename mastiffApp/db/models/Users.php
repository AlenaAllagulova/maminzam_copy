<?php

namespace mastiffApp\db\models;
use mastiffApp\db\models\orm\OrmModel;

class Users extends OrmModel
{
    protected $table = 'users';
    public $timestamps = false;
    protected $primaryKey = 'user_id';

    public function opinions()
    {
        return $this->hasMany('mastiffApp\db\models\Opinions', 'user_id', 'user_id');
    }

    public function author_opinions()
    {
        return $this->hasMany('mastiffApp\db\models\Opinions', 'user_id', 'author_id');
    }

    public function offers()
    {
        return $this->hasMany('mastiffApp\db\models\OrdersOffers', 'user_id', 'user_id');
    }
}