<?php

namespace mastiffApp\db\models;


use mastiffApp\db\models\orm\OrmModel;

class Orders extends OrmModel
{
    protected $table = 'orders';
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function user()
    {
        return $this->hasOne('mastiffApp\db\models\Users', 'user_id', 'user_id');
    }
}