<?php

namespace mastiffApp\db\models;

use mastiffApp\db\models\orm\OrmModel;

/**
 * Class Opinions
 * @package mastiffApp\db\models
 *
 * @property integer    id
 * @property integer    author_id
 * @property integer    user_id
 * @property integer    order_id
 * @property integer    type
 * @property integer    status
 * @property string     message
 * @property integer    moderated
 * @property string     created
 * @property string     modified
 * @property string     answer
 * @property string     answer_created
 * @property string     answer_modified
 */
class Opinions extends OrmModel
{
    protected $table = 'opinions';
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function author()
    {
        return $this->hasOne('mastiffApp\db\models\Users', 'user_id', 'author_id');
    }

    public function user()
    {
        return $this->hasOne('mastiffApp\db\models\Users', 'user_id', 'user_id');
    }

    public function orderItem()
    {
        return $this->hasOne('mastiffApp\db\models\Orders', 'id', 'order_id');
    }
}

