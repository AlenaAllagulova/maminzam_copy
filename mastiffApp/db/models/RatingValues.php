<?php

namespace mastiffApp\db\models;

use Illuminate\Database\Query\Builder;
use mastiffApp\db\models\orm\OrmModel;

/**
 * Class RatingValues
 * @package mastiffApp\db\models
 *
 * @property integer    $id
 * @property integer    $user_type
 * @property string     $sys_key
 * @property integer    $stars_count
 * @property integer    $stars_step
 * @property boolean    $enable
 *
 * @property Builder $langs
 */
class RatingValues extends OrmModel
{
    protected $table = 'rating_values';
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function langs()
    {
        return $this->hasMany('mastiffApp\db\models\RatingValuesLang', 'id', 'id');
    }

    public function lang()
    {
        return $this
            ->hasOne('mastiffApp\db\models\RatingValuesLang', 'id', 'id')
            ->where('lang', LNG);
    }
}