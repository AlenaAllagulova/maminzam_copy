<?php

namespace mastiffApp\db\models;


use mastiffApp\db\models\orm\OrmModel;

/**
 * Class RatingOpinions
 * @package mastiffApp\db\models
 * @property integer id
 * @property integer author_id
 * @property integer user_id
 * @property integer opinion_id
 * @property string stars
 * @property integer sum
 * @property string modified
 */
class RatingOpinions extends OrmModel
{
    protected $table = 'rating_opinions';
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function user()
    {
        return $this->hasOne('mastiffApp\db\models\Users', 'user_id', 'user_id');
    }

    public function author()
    {
        return $this->hasOne('mastiffApp\db\models\Users', 'user_id', 'author_id');
    }

    public function opinion()
    {
        return $this->hasOne('mastiffApp\db\models\Opinions', 'id', 'opinion_id');
    }
}