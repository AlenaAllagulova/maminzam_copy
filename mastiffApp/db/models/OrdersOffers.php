<?php

namespace mastiffApp\db\models;

use mastiffApp\db\models\orm\OrmModel;

/**
 * Class OrdersOffers
 * @package mastiffApp\db\models
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $user_id
 * @property integer $user_removed
 * @property integer $status
 * @property string $status_created
 * @property string $user_ip
 * @property float $price_from
 * @property float $price_to
 * @property integer $price_curr
 * @property integer $price_rate
 * @property float $price_rate_from
 * @property float $price_rate_to
 * @property string $price_rate_text
 * @property float $terms_from
 * @property float $terms_to
 * @property integer $terms_type
 * @property string $descr
 * @property integer $client_only
 * @property string $created
 * @property string $modified
 * @property integer $moderated
 * @property string $enotify_send
 * @property integer $is_new
 * @property integer $chatcnt
 * @property integer $chat_id
 * @property integer $chat_new_client
 * @property integer $chat_new_worker
 * @property integer $examplescnt
 * @property integer $imgfav
 * @property integer $imgcnt
 * @property integer $from_invite
 */
class OrdersOffers extends OrmModel
{
    protected $table = 'orders_offers';
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function user()
    {
        return $this->hasOne('mastiffApp\db\models\Users', 'user_id', 'user_id');
    }
}