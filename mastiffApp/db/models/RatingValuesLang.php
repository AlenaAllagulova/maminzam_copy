<?php

namespace mastiffApp\db\models;

use mastiffApp\db\models\orm\OrmModel;

/**
 * Class RatingValues
 * @package mastiffApp\db\models
 *
 * @property integer $id
 * @property string $lang
 * @property string $title
 */
class RatingValuesLang extends OrmModel
{
    protected $table = 'rating_values_lang';
    public $timestamps = false;
    protected $compositePrimaryKey = ['id', 'lang'];


}