'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Rating = function () {
    function Rating() {
        _classCallCheck(this, Rating);

        this._stars = {
            box: null,
            typesBox: null,
            typesData: null,
            list: null,
            item: null,
            data: {}
        };
        this._buttons = {
            opinion: null
        };
        this._parameters = {
            selectorItemId: null
        };
    }

    _createClass(Rating, [{
        key: 'initModalAdd',
        value: function initModalAdd(options) {
            this.starsBox = options.starsBox;
            this.starsTypesBox = options.starsTypesBox;
            this.starsTypesData = options.starsTypesData;
            this.starsList = options.starsList;
            this.starsItem = options.starsItem;
            this.btnOpinion = options.btnOpinion;

            var domStarsList = $(this.starsBox + ' ' + this.starsList);
            var domStarsListLoad = Boolean(domStarsList.length);
            var self = this;

            $('body').on('click', this.btnOpinion, function btnOpinionClick(e) {
                if (!domStarsListLoad) {
                    if (!Boolean(domStarsList.length)) {
                        var counter = 0;
                        var monitorId = setTimeout(function monitor() {
                            domStarsList = $(self.starsBox + ' ' + self.starsList);
                            if (!Boolean(domStarsList.length)) {
                                var itemId = $(e.currentTarget).data('id');
                                domStarsList = $(self.starsBox + itemId + ' ' + self.starsList);
                            }
                            counter++;
                            if (Boolean(domStarsList.length)) {
                                clearTimeout(monitorId);
                                domStarsListLoad = true;
                                return btnOpinionClick(e);
                            } else if (counter > 100) {
                                clearTimeout(monitorId);
                                console.error('FAILED method initModalAdd() from rating.js');
                                return false;
                            } else {
                                monitorId = setTimeout(monitor, 10);
                            }
                        }, 10);
                    }
                } else {
                    if (!Boolean(domStarsList)) {
                        return false;
                    }

                    var orderId = $(e.currentTarget).data('order_id');
                    if (!self.starsData.hasOwnProperty(orderId)) {
                        bff.ajax(bff.ajaxURL('rating', 'getStarsItems'), {
                            orderId: orderId
                        }, function (data) {
                            if (data.hasOwnProperty('success')) {
                                if (data.success) {
                                    var domItems = domStarsList.find(self.starsItem);
                                    if (!Boolean(domItems.length)) {
                                        var starsBox = self.starsBox;
                                        var starsList = $(starsBox + ' ' + self.starsList);
                                        self.selectorItemId = 'input[name="order_id"]';
                                        if (!Boolean(starsList.length)) {
                                            var itemId = $(e.currentTarget).data('id');
                                            starsBox = self.starsBox + itemId;
                                            starsList = $(starsBox + ' ' + self.starsList);
                                        }
                                        starsList.html(data.html);
                                        domItems = domStarsList.find(self.starsItem);
                                        self.setStars(domItems);
                                        self.setToggleStars($(starsBox + ' ' + self.starsTypesBox));
                                    }
                                    domItems.rating('update', 0);
                                    var item = {};
                                    item[orderId] = data.items;
                                    self.starsData = Object.assign(self.starsData, item);
                                    return btnOpinionClick(e);
                                }
                            }
                        });
                    } else {
                        if (self.starsData.hasOwnProperty(orderId)) {
                            var _iteratorNormalCompletion = true;
                            var _didIteratorError = false;
                            var _iteratorError = undefined;

                            try {
                                for (var _iterator = Object.entries(self.starsData[orderId])[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                                    var _step$value = _slicedToArray(_step.value, 2),
                                        k = _step$value[0],
                                        v = _step$value[1];

                                    if (self.starsData[orderId].hasOwnProperty(k)) {
                                        $(self.starsItem).filter('[data-sys_key="' + k + '"]').rating('update', v);
                                    }
                                }
                            } catch (err) {
                                _didIteratorError = true;
                                _iteratorError = err;
                            } finally {
                                try {
                                    if (!_iteratorNormalCompletion && _iterator.return) {
                                        _iterator.return();
                                    }
                                } finally {
                                    if (_didIteratorError) {
                                        throw _iteratorError;
                                    }
                                }
                            }
                        }
                        domStarsList = $(self.starsBox + ' ' + self.starsList);
                        domStarsListLoad = false;
                    }
                }
            });
        }
    }, {
        key: 'initEdit',
        value: function initEdit(options) {
            var _this = this;

            this.starsBox = options.starsBox;
            this.starsTypesBox = options.starsTypesBox;
            this.starsTypesData = options.starsTypesData;
            this.starsList = options.starsList;
            this.starsItem = options.starsItem;
            this.btnOpinion = options.btnOpinion;

            $(this.btnOpinion).on('click', function (e) {
                var domStarsBox = $(e.currentTarget).closest(_this.starsBox);

                if (!Boolean(domStarsBox.length)) {
                    return false;
                }

                var itemId = domStarsBox.find('input[name="id"]').val();
                if (!_this.starsData.hasOwnProperty(itemId)) {
                    var domItems = domStarsBox.find(_this.starsItem);
                    _this.selectorItemId = 'input[name="id"]';
                    _this.setStars(domItems);
                    _this.setToggleStars(domStarsBox.find(_this.starsTypesBox));

                    var item = {};
                    item[itemId] = {};
                    for (var i = 0; i < domItems.length; i++) {
                        item[itemId][domItems.eq(i).data('sys_key')] = domItems.eq(i).val();
                    }
                    _this.starsData = Object.assign(_this.starsData, item);
                }
            });
        }
    }, {
        key: 'initView',
        value: function initView(options) {
            this.starsItem = options.starsItem;

            var domItems = $(this.starsItem);
            if (!Boolean(domItems.length)) {
                return false;
            }
            domItems.rating({
                filledStar: '<i class="icon-star" aria-hidden="true"></i>',
                emptyStar: '<i class="icon-star" aria-hidden="true"></i>'
            });
        }
    }, {
        key: 'initModalView',
        value: function initModalView(options) {
            var _this2 = this;

            this.starsBox = options.starsBox;
            this.starsItem = options.starsItem;
            this.btnOpinion = options.btnOpinion;

            $('body').on('click', this.btnOpinion, function (e) {
                var itemId = $(e.currentTarget).data('id');
                if (Boolean(itemId)) {
                    var items = $(_this2.starsBox + itemId + ' ' + _this2.starsItem);
                    if (Boolean(items.length)) {
                        items.rating({
                            filledStar: '<i class="icon-star" aria-hidden="true"></i>',
                            emptyStar: '<i class="icon-star" aria-hidden="true"></i>'
                        });
                    }
                }
            });
        }
    }, {
        key: 'initViewTotalAverage',
        value: function initViewTotalAverage(options) {
            this.starsItem = options.starsItem;

            var domItems = $(this.starsItem);
            if (!Boolean(domItems.length)) {
                return false;
            }
            domItems.rating({
                filledStar: '<i class="icon-star" aria-hidden="true"></i>',
                emptyStar: '<i class="icon-star" aria-hidden="true"></i>',
                clearButton: '<i class="icon-star" aria-hidden="true"></i>',
                clearCaption: '0.0',
                starCaptions: {
                    0: '0.0',
                    1: '1.0',
                    2: '2.0',
                    3: '3.0',
                    4: '4.0',
                    5: '5.0'
                }
            });
        }
    }, {
        key: 'setStars',
        value: function setStars(domItems) {
            var _this3 = this;

            if (Boolean(domItems.length)) {
                domItems.rating({
                    filledStar: '<i class="icon-star" aria-hidden="true"></i>',
                    emptyStar: '<i class="icon-star" aria-hidden="true"></i>'
                });
                domItems.on('rating.change', function (e, value) {
                    var domStars = $(e.currentTarget);
                    var sys_key = domStars.data('sys_key');
                    var domStarsBox = domStars.closest(_this3.starsBox);
                    if (!Boolean(domStarsBox.length)) {
                        domStarsBox = domStars.closest('[id^="' + (/^#/.test(_this3.starsBox) ? _this3.starsBox.replace(/^#/, '') : _this3.starsBox) + '"]');
                    }
                    var itemId = domStarsBox.find(_this3.selectorItemId).val();

                    if (_this3.starsData.hasOwnProperty(itemId)) {
                        var item = _this3.starsData[itemId];
                        if (item.hasOwnProperty(sys_key)) {
                            item[sys_key] = value;
                        }
                    }
                });
            }
        }
    }, {
        key: 'setToggleStars',
        value: function setToggleStars(domTypesBox) {
            var _this4 = this;

            if (Boolean(domTypesBox.length)) {
                domTypesBox.find('input[name="type"][type="radio"]').on('change', function (e) {
                    _this4.toggleStars(e);
                });
            }
        }
    }, {
        key: 'toggleStars',
        value: function toggleStars(e) {
            var _this5 = this;

            var domTypeItem = $(e.currentTarget);
            var value = domTypeItem.val();
            var domStarsBox = domTypeItem.closest(this.starsBox);
            if (!Boolean(domStarsBox.length)) {
                domStarsBox = domTypeItem.closest('[id^="' + (/^#/.test(this.starsBox) ? this.starsBox.replace(/^#/, '') : this.starsBox) + '"]');
            }
            if (this.starsTypesData.hasOwnProperty(value)) {
                if (this.starsTypesData[value]) {
                    domStarsBox.find(this.starsList).show(function () {
                        var itemId = domStarsBox.find(_this5.selectorItemId).val();
                        if (_this5.starsData.hasOwnProperty(itemId)) {
                            var _iteratorNormalCompletion2 = true;
                            var _didIteratorError2 = false;
                            var _iteratorError2 = undefined;

                            try {
                                for (var _iterator2 = Object.entries(_this5.starsData[itemId])[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                                    var _step2$value = _slicedToArray(_step2.value, 2),
                                        k = _step2$value[0],
                                        v = _step2$value[1];

                                    if (_this5.starsData[itemId].hasOwnProperty(k)) {
                                        domStarsBox.find(_this5.starsItem).filter('[data-sys_key="' + k + '"]').rating('update', v);
                                    }
                                }
                            } catch (err) {
                                _didIteratorError2 = true;
                                _iteratorError2 = err;
                            } finally {
                                try {
                                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                        _iterator2.return();
                                    }
                                } finally {
                                    if (_didIteratorError2) {
                                        throw _iteratorError2;
                                    }
                                }
                            }
                        }
                    });
                } else {
                    domStarsBox.find(this.starsList).hide(function () {
                        domStarsBox.find(_this5.starsItem).rating('update', 0);
                    });
                }
            }
        }
    }, {
        key: 'starsBox',
        set: function set(data) {
            this._stars.box = data;
        },
        get: function get() {
            return this._stars.box;
        }
    }, {
        key: 'starsTypesBox',
        set: function set(data) {
            this._stars.typesBox = data;
        },
        get: function get() {
            return this._stars.typesBox;
        }
    }, {
        key: 'starsTypesData',
        set: function set(data) {
            this._stars.typesData = data;
        },
        get: function get() {
            return this._stars.typesData;
        }
    }, {
        key: 'starsList',
        set: function set(data) {
            this._stars.list = data;
        },
        get: function get() {
            return this._stars.list;
        }
    }, {
        key: 'starsItem',
        set: function set(data) {
            this._stars.item = data;
        },
        get: function get() {
            return this._stars.item;
        }
    }, {
        key: 'starsData',
        set: function set(data) {
            this._stars.data = data;
        },
        get: function get() {
            return this._stars.data;
        }
    }, {
        key: 'btnOpinion',
        set: function set(data) {
            this._buttons.opinion = data;
        },
        get: function get() {
            return this._buttons.opinion;
        }
    }, {
        key: 'selectorItemId',
        set: function set(data) {
            this._parameters.selectorItemId = data;
        },
        get: function get() {
            return this._parameters.selectorItemId;
        }
    }]);

    return Rating;
}();
//# sourceMappingURL=rating.js.map
