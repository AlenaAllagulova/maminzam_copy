class Rating {
    constructor ()
    {
        this._stars = {
            box: null,
            typesBox: null,
            typesData: null,
            list: null,
            item: null,
            data: {}
        };
        this._buttons = {
            opinion: null
        };
        this._parameters = {
            selectorItemId: null,
        };
    }

    initModalAdd(options)
    {
        this.starsBox = options.starsBox;
        this.starsTypesBox = options.starsTypesBox;
        this.starsTypesData = options.starsTypesData;
        this.starsList = options.starsList;
        this.starsItem = options.starsItem;
        this.btnOpinion = options.btnOpinion;

        let domStarsList = $(this.starsBox+' '+this.starsList);
        let domStarsListLoad = Boolean(domStarsList.length);
        let self = this;

        $('body').on('click', this.btnOpinion, function btnOpinionClick(e) {
            if ( ! domStarsListLoad) {
                if ( ! Boolean(domStarsList.length)) {
                    let counter = 0;
                    let monitorId = setTimeout(function monitor() {
                        domStarsList = $(self.starsBox+' '+self.starsList);
                        if (!Boolean(domStarsList.length)) {
                            let itemId = $(e.currentTarget).data('id');
                            domStarsList = $(self.starsBox+itemId+' '+self.starsList);
                        }
                        counter++;
                        if (Boolean(domStarsList.length)) {
                            clearTimeout(monitorId);
                            domStarsListLoad = true;
                            return btnOpinionClick(e);
                        } else if ((counter > 100)) {
                            clearTimeout(monitorId);
                            console.error('FAILED method initModalAdd() from rating.js');
                            return false;
                        } else {
                            monitorId = setTimeout(monitor, 10);
                        }
                    }, 10);
                }
            } else {
                if ( ! Boolean(domStarsList)) {
                    return false;
                }

                let orderId = $(e.currentTarget).data('order_id');
                if ( ! self.starsData.hasOwnProperty(orderId)) {
                    bff.ajax(
                        bff.ajaxURL('rating', 'getStarsItems'),
                        {
                            orderId : orderId
                        },
                        (data) => {
                            if (data.hasOwnProperty('success')) {
                                if (data.success) {
                                    let domItems = domStarsList.find(self.starsItem);
                                    if ( ! Boolean(domItems.length)) {
                                        let starsBox = self.starsBox;
                                        let starsList = $(starsBox+' '+self.starsList);
                                        self.selectorItemId = 'input[name="order_id"]';
                                        if (!Boolean(starsList.length)) {
                                            let itemId = $(e.currentTarget).data('id');
                                            starsBox = self.starsBox+itemId;
                                            starsList = $(starsBox+' '+self.starsList);
                                        }
                                        starsList.html(data.html);
                                        domItems = domStarsList.find(self.starsItem);
                                        self.setStars(domItems);
                                        self.setToggleStars($(starsBox+' '+self.starsTypesBox));
                                    }
                                    domItems.rating('update', 0);
                                    let item = {};
                                    item[orderId] = data.items;
                                    self.starsData = Object.assign(self.starsData, item);
                                    return btnOpinionClick(e);
                                }
                            }
                        }
                    );
                } else {
                    if (self.starsData.hasOwnProperty(orderId)) {
                        for (let [k, v] of Object.entries(self.starsData[orderId])) {
                            if (self.starsData[orderId].hasOwnProperty(k)) {
                                $(self.starsItem)
                                    .filter('[data-sys_key="'+k+'"]')
                                    .rating('update', v);
                            }
                        }
                    }
                    domStarsList = $(self.starsBox+' '+self.starsList);
                    domStarsListLoad = false;
                }
            }
        });
    }

    initEdit(options)
    {
        this.starsBox = options.starsBox;
        this.starsTypesBox = options.starsTypesBox;
        this.starsTypesData = options.starsTypesData;
        this.starsList = options.starsList;
        this.starsItem = options.starsItem;
        this.btnOpinion = options.btnOpinion;

        $(this.btnOpinion).on('click', (e) => {
            let domStarsBox = $(e.currentTarget).closest(this.starsBox);

            if (!Boolean(domStarsBox.length)) {
                return false;
            }

            let itemId = domStarsBox.find('input[name="id"]').val();
            if ( ! this.starsData.hasOwnProperty(itemId)) {
                let domItems = domStarsBox.find(this.starsItem);
                this.selectorItemId = 'input[name="id"]';
                this.setStars(domItems);
                this.setToggleStars(domStarsBox.find(this.starsTypesBox));

                let item = {};
                item[itemId] = {};
                for (let i = 0; i < domItems.length; i++) {
                    item[itemId][domItems.eq(i).data('sys_key')] = domItems.eq(i).val();
                }
                this.starsData = Object.assign(this.starsData, item);
            }
        });
    }

    initView(options)
    {
        this.starsItem = options.starsItem;

        let domItems = $(this.starsItem);
        if (!Boolean(domItems.length)) {
            return false;
        }
        domItems.rating({
            filledStar: '<i class="icon-star" aria-hidden="true"></i>',
            emptyStar: '<i class="icon-star" aria-hidden="true"></i>'
        });
    }

    initModalView(options)
    {
        this.starsBox = options.starsBox;
        this.starsItem = options.starsItem;
        this.btnOpinion = options.btnOpinion;

        $('body').on('click', this.btnOpinion, (e) => {
            let itemId = $(e.currentTarget).data('id');
            if (Boolean(itemId)) {
                let items = $(this.starsBox+itemId+' '+this.starsItem);
                if (Boolean(items.length)) {
                    items.rating({
                        filledStar: '<i class="icon-star" aria-hidden="true"></i>',
                        emptyStar: '<i class="icon-star" aria-hidden="true"></i>'
                    });
                }
            }
        });

    }

    initViewTotalAverage(options)
    {
        this.starsItem = options.starsItem;

        let domItems = $(this.starsItem);
        if (!Boolean(domItems.length)) {
            return false;
        }
        domItems.rating({
            filledStar: '<i class="icon-star" aria-hidden="true"></i>',
            emptyStar: '<i class="icon-star" aria-hidden="true"></i>',
            clearButton: '<i class="icon-star" aria-hidden="true"></i>',
            clearCaption: '0.0',
            starCaptions: {
                0: '0.0',
                1: '1.0',
                2: '2.0',
                3: '3.0',
                4: '4.0',
                5: '5.0'
            },
        });
    }

    set starsBox(data)
    {
        this._stars.box = data;
    }

    get starsBox()
    {
        return this._stars.box;
    }

    set starsTypesBox(data)
    {
        this._stars.typesBox = data;
    }

    get starsTypesBox()
    {
        return this._stars.typesBox;
    }

    set starsTypesData(data)
    {
        this._stars.typesData = data;
    }

    get starsTypesData()
    {
        return this._stars.typesData;
    }

    set starsList(data)
    {
        this._stars.list = data;
    }

    get starsList()
    {
        return this._stars.list;
    }

    set starsItem(data)
    {
        this._stars.item = data;
    }

    get starsItem()
    {
        return this._stars.item;
    }

    set starsData(data)
    {
        this._stars.data = data;
    }

    get starsData()
    {
        return this._stars.data;
    }

    set btnOpinion(data)
    {
        this._buttons.opinion = data;
    }

    get btnOpinion()
    {
        return this._buttons.opinion;
    }

    set selectorItemId(data)
    {
        this._parameters.selectorItemId = data;
    }

    get selectorItemId()
    {
        return this._parameters.selectorItemId;
    }

    setStars(domItems)
    {
        if (Boolean(domItems.length)) {
            domItems.rating({
                filledStar: '<i class="icon-star" aria-hidden="true"></i>',
                emptyStar: '<i class="icon-star" aria-hidden="true"></i>'
            });
            domItems.on('rating.change', (e, value) => {
                let domStars = $(e.currentTarget);
                let sys_key = domStars.data('sys_key');
                let domStarsBox = domStars.closest(this.starsBox);
                if (!Boolean(domStarsBox.length)) {
                    domStarsBox = domStars.closest('[id^="'+(
                            (/^#/.test(this.starsBox))
                                ? this.starsBox.replace(/^#/,'')
                                : this.starsBox
                        )+'"]');
                }
                let itemId = domStarsBox.find(this.selectorItemId).val();

                if (this.starsData.hasOwnProperty(itemId)) {
                    let item = this.starsData[itemId];
                    if (item.hasOwnProperty(sys_key)) {
                        item[sys_key] = value;
                    }
                }
            });
        }
    }

    setToggleStars(domTypesBox)
    {
        if (Boolean(domTypesBox.length)) {
            domTypesBox
                .find('input[name="type"][type="radio"]')
                .on('change', (e) => {
                    this.toggleStars(e);
                });
        }
    }

    toggleStars(e)
    {
        let domTypeItem = $(e.currentTarget);
        let value = domTypeItem.val();
        let domStarsBox = domTypeItem.closest(this.starsBox);
        if (!Boolean(domStarsBox.length)) {
            domStarsBox = domTypeItem.closest('[id^="'+(
                (/^#/.test(this.starsBox))
                    ? this.starsBox.replace(/^#/,'')
                    : this.starsBox
                )+'"]');
        }
        if (this.starsTypesData.hasOwnProperty(value)) {
            if (this.starsTypesData[value]) {
                domStarsBox.find(this.starsList).show(() => {
                    let itemId = domStarsBox.find(this.selectorItemId).val();
                    if (this.starsData.hasOwnProperty(itemId)) {
                        for (let [k, v] of Object.entries(this.starsData[itemId])) {
                            if (this.starsData[itemId].hasOwnProperty(k)) {
                                domStarsBox.find(this.starsItem)
                                    .filter('[data-sys_key="'+k+'"]')
                                    .rating('update', v);
                            }
                        }
                    }
                });
            } else {
                domStarsBox.find(this.starsList).hide(() => {
                    domStarsBox.find(this.starsItem).rating('update', 0);
                });
            }
        }
    }
}