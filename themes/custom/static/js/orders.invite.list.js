var jOrdersInviteList = (function(){
    var inited = false, o = {lang:{}, user_id:0};
    var $block, $modal = false, f, $f = false;
    var price = {cache:{}, $block:0, spec_id:0};

    function init()
    {
        // modal init
        $block = $('.j-user-block');
        $block.on('click', '.j-order-invite', function(e){
            o.user_id = $(this).data('id');
            e.preventDefault();
            if ($modal && $modal.length) {
                $modal.modal('show');
            } else {
                bff.ajax(bff.ajaxURL('orders', 'invite-modal'), {id: o.user_id, hash: app.csrf_token}, function (resp, errors) {
                    if (resp && resp.success) {
                        if (resp.html) {
                            $modal = $(resp.html);
                            app.$B.append($modal);
                            initModal();
                            $modal.modal('show');
                        }
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
        });
    }

    function initModal()
    {
        $f = $('#j-order-form');

        // specs select
        if (typeof(jSpecsSelect) == 'object') {
            jSpecsSelect.init({block:$f.find('.j-specs'), cnt:1, limit:1});
            jSpecsSelect.onSelect(function(){
                setPrice();
            });
        }

        // price select
        price.$block = $f.find('#j-order-price-block');
        price.hide = function(){
            price.$block.addClass('hidden');
        };
        price.set = function(specid){
            if (price.spec_id == specid) {
                price.$block.removeClass('hidden');
                return;
            }
            if (price.cache.hasOwnProperty(specid)) {
                price.show(price.cache[specid]);
                price.spec_id = specid;
            } else {
                bff.ajax(bff.ajaxURL('orders','price-form-sett'), {spec_id:specid}, function(resp){
                    if (resp && resp.success) {
                        price.show(price.cache[specid] = resp.html);
                        price.spec_id = specid;
                    }
                });
            }
        };
        price.show = function(data){
            price.$block.html(data);
            price.$block.removeClass('hidden');
        };
        price.$block.on('focus', '[name="price"]', function(){
            price.$block.find('.j-price-ex:first').prop('checked', true);
        });
        price.$block.on('focus', '[name="price_curr"]', function(){
            price.$block.find('.j-price-ex:first').prop('checked', true);
        });
        price.$block.on('focus', '[name="price_rate"]', function(){
            price.$block.find('.j-price-ex:first').prop('checked', true);
        });

        // new form submit


        f = app.form($f, function($f){

            var f = this;
            if ( ! f.checkRequired({focus:true}) ) return false;
            var $bl = $f.find('.j-specs:visible');
            if ($bl.length) {
                if ( ! intval($bl.find('.j-spec-value').val())) {
                    app.alert.error(o.lang.spec_wrong);
                    return false;
                }
            }

            f.ajax(bff.ajaxURL('orders&ev=add', ''),{create_invite:o.user_id},function(resp, errors){
                if (resp && resp.success) {
                    if (resp.id) {
                        f.$field('title').val('');
                        f.$field('descr').val('');
                        price.hide();
                        jSpecsSelect.reset($f.find('.j-spec-select'));
                        $modal.on('hidden.bs.modal', function(e){
                            $modal.remove();
                            $modal = false;
                        });
                        $modal.modal('hide');
                        app.alert.success(o.lang.invite_success);
                    }
                } else {
                    f.alertError(errors);
                }
            });
        });

        // form - terms
        var $term = $f.find('#j-term-title');
        f.$field('term').change(function(){
            var id = $(this).val();
            if (o.terms.hasOwnProperty(id)) {
                if (o.terms[id]) {
                    $term.html(o.terms[id]);
                    $term.parent().removeClass('hidden');
                } else {
                    $term.parent().addClass('hidden');
                }
            } else {
                $term.parent().addClass('hidden');
            }
        });

        // form - full submit
        $f.on('click', '.j-full-add', function(e){
            e.preventDefault();
            $f.unbind('submit');
            $f.append('<input type="hidden" name="worker_invite" value="'+o.user_id+'" />');
            $f.get(0).submit();
        });

        var $listing = $modal.find('.j-listing');
        var $invite = $modal.find('.j-invite');

        var order_id = 0;
        $listing.on('click', '.j-order', function(e){
            order_id = $(this).data('id');
            e.preventDefault();
            $invite.find('.j-order').html($(this).html());
            $listing.addClass('hidden');
            $invite.removeClass('hidden');
        });

        $invite.on('click', '.j-another', function(e){
            e.preventDefault();
            $invite.addClass('hidden');
            $listing.removeClass('hidden');
        });

        // invite to existing order (public / private)
        var $message = $invite.find('.j-message');
        $invite.on('click', '.j-submit', function(e){
            e.preventDefault();
            bff.ajax(bff.ajaxURL('orders', 'invite'), {user: o.user_id, order: order_id, message: $message.val(), hash: app.csrf_token}, function (resp, errors){
                if (resp && resp.success) {
                    $modal.modal('hide');
                    $message.val('');
                    app.alert.success(o.lang.invite_success);
                    $invite.addClass('hidden');
                    $listing.removeClass('hidden');
                    $listing.find('.j-order[data-id="'+order_id+'"]').remove();
                } else {
                    app.alert.error(errors);
                }
            });
        });
    }

    function setPrice()
    {
        var specid = 0;
        var $bl = $f.find('.j-specs:visible:first');
        if ($bl.length) {
            specid = intval($bl.find('.j-spec-value').val());
        }

        if ( ! specid) {
            price.hide();
            return;
        }
        price.set(specid);
    }

    return {
        init: function(options)
        {
            if (inited) return; inited = false;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());