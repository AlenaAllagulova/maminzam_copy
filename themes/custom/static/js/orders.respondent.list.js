var jOrdersRespondentList = (function(){

    var inited = false, o = {lang:{}}, listMngr;
    var $blstart, $blorderstart, $blinvites, $block, $form, $list, $pgn;
    var orderOpinions = false, invitesEnabled = false;

    function init()
    {
        orderOpinions = intval(o.ordersOpinions);
        invitesEnabled = intval(o.invitesEnabled);

        $block = $('#j-orders-respondent-list');

        $form = $('#j-orders-respondent-list-form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        $form.on('click', '.j-f-status', function(){
            var $el = $(this);
            $form.find('[name="st"]').val($el.data('id'));
            closePopup($el);
            massActions();
            listMngr.submit({}, true);
            return false;
        });

        $list.on('click', '.j-trash', function(){
            bff.ajax(bff.ajaxURL('orders', 'offer-trash'), {id:$(this).data('id'), hash: app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    listMngr.submit({popstate:true}, false);
                } else {
                    app.alert.error(errors);
                }
            });
            return false;
        });

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll) $.scrollTo($list, {offset: -150, duration:500});
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                var st = intval($form.find('[name="st"]').val());
                if(resp.hasOwnProperty('counts')){
                    for(var t in resp.counts){
                        if(resp.counts.hasOwnProperty(t)){
                            $form.find('.j-cnt-status-' + t ).text('(' + resp.counts[t] + ')');
                            if(st == t){
                                $form.find('.j-f-status-cnt').text('(' + resp.counts[t] + ')');
                            }
                        }
                    }
                }
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onPopstate: function() {
                massActions();
            },
            ajax: o.ajax
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

        if(orderOpinions){
            $blstart = $('#j-offers-performer-start');
            $blstart.on('click', '.j-agree', function(e){
                e.preventDefault();
                var $el = $(this);
                bff.ajax(bff.ajaxURL('orders&ev=offers_list', 'performer-agree'), {id:$el.data('id'), hash:app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $el.closest('.j-offer').remove();
                        if(listMngr){
                            listMngr.submit({}, false);
                        }else{
                            location.reload();
                        }
                    } else {
                        app.alert.error(errors);
                    }
                });
            });
            $blstart.on('click', '.j-decline', function(e){
                e.preventDefault();
                var $el = $(this);
                var id = $el.data('id');
                var $modal = $('#j-modal-decline-'+id);
                if($modal.length){
                    $modal.modal('show');
                }else{
                    bff.ajax(bff.ajaxURL('orders&ev=offers_list', 'modal-performer-decline'), {id:$el.data('id'), hash:app.csrf_token}, function(resp, errors){
                        if(resp && resp.success) {
                            $modal = $(resp.html);
                            app.$B.append($modal);
                            initModalDecline($modal);
                            $modal.modal('show');
                        } else {
                            app.alert.error(errors);
                        }
                    });
                }
            });

            $blorderstart = $('#j-order-view-performer-start');
            $blorderstart.on('click', '.j-agree', function(e){
                e.preventDefault();
                var $el = $(this);
                bff.ajax(bff.ajaxURL('orders&ev=offers_list', 'performer-agree'), {id:$el.data('id'), hash:app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $el.closest('.j-offer').remove();
                        // $blorderstart.hide();
                        // $('#j-order-view-performer-agree').show();
                        if(listMngr){
                            listMngr.submit({}, false);
                        }else{
                            location.reload();
                        }
                    } else {
                        app.alert.error(errors);
                    }
                });
            });
            $blorderstart.on('click', '.j-decline', function(e){
                e.preventDefault();
                var $el = $(this);
                var id = $el.data('id');
                var $modal = $('#j-modal-decline-'+id);
                if($modal.length){
                    $modal.modal('show');
                }else{
                    bff.ajax(bff.ajaxURL('orders&ev=offers_list', 'modal-performer-decline'), {id:$el.data('id'), hash:app.csrf_token}, function(resp, errors){
                        if(resp && resp.success) {
                            $modal = $(resp.html);
                            app.$B.append($modal);
                            initModalDecline($modal);
                            $modal.modal('show');
                        } else {
                            app.alert.error(errors);
                        }
                    });
                }
            });

            $list.on('click', '.j-performer-start-agree', function(e){
                e.preventDefault();
                var id = $(this).data('id');
                $blstart.find('.j-agree[data-id="'+id+'"]').trigger('click');
                return false;
            });

        }

        if(invitesEnabled){
            $blinvites = $('#j-orders-invites');

            $blinvites.on('click', '.j-decline', function(e){
                e.preventDefault();
                var $el = $(this);
                var id = $el.data('id');
                var $message = $el.closest('form').find('[name="message"]');
                if($message.val().length < 5){
                    app.alert.error(o.lang.decline_short);
                    $message.focus();
                    return;
                }
                bff.ajax(bff.ajaxURL('orders', 'invite-decline'), {id:$el.data('id'), message:$message.val(), hash:app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $('#j-modal-decline-'+id).modal('hide');
                        $blinvites.find('.j-invite[data-id="'+id+'"]').remove();
                        listMngr.submit({}, false);
                    } else {
                        app.alert.error(errors);
                    }
                });
            });

            var $offer = false;
            $blinvites.on('click', '.j-add-offer', function(e){
                e.preventDefault();
                var $el = $(this);
                var id = intval($el.data('id'));
                if($offer && $offer.length){
                    if(intval($offer.data('id')) == id){
                        $offer.modal('show');
                        return;
                    }
                    $offer.remove();
                }
                bff.ajax(bff.ajaxURL('orders', 'offer-modal'), {id:id, hash:app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $offer = $(resp.html);
                        app.$B.append($offer);
                        app.form($offer.find('form'), function(){
                            var f = this;
                            if( ! f.checkRequired({focus:true}) ) return;
                            f.ajax(bff.ajaxURL('orders&ev=offer_add&hash='+app.csrf_token, 'add'),{},function(data,errors){
                                if(data && data.success) {
                                    f.alertSuccess(o.lang.saved_success);
                                    $offer.on('hidden.bs.modal', function(e){
                                        $offer.remove();
                                    });
                                    $offer.modal('hide');
                                    $blinvites.find('.j-invite[data-id="'+id+'"]').remove();
                                    listMngr.submit({}, false);
                                } else {
                                    f.fieldsError(data.fields, errors);
                                }
                            });

                        }, {noEnterSubmit: true});
                        $offer.modal('show');
                    } else {
                        app.alert.error(errors);
                    }
                });

            });
        }

    }

    function closePopup($el)
    {
        var $p = $el.closest('.dropdown');
        if($p.length){
            $p.removeClass('open');
        }
    }

    function massActions()
    {
        var t = intval($form.find('[name="st"]').val());
        if(o.types.hasOwnProperty(t)){
            $form.find('.j-f-status-title').text(o.types[t].t);
        }
        var $t = $form.find('.j-f-status');
        $t.parent().removeClass('active');
        $t.each(function(){
            if($(this).data('id') == t){
                $(this).parent().addClass('active');
            }
        });
    }

    function initModalDecline($bl)
    {
        if($bl.hasClass('i')) return;
        $bl.addClass('i');
        $bl.on('click', '.j-decline', function(e){
            e.preventDefault();
            var $el = $(this);
            var id = $el.data('id');
            var $message = $el.closest('form').find('[name="message"]');
            if($message.val().length < 5){
                app.alert.error(o.lang.decline_short);
                $message.focus();
                return;
            }
            bff.ajax(bff.ajaxURL('orders&ev=offers_list', 'performer-decline'), {id:$el.data('id'), message:$message.val(), hash:app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    $('#j-modal-decline-'+id).modal('hide');
                    $blstart.find('.j-offer[data-id="'+id+'"]').remove();
                    if(listMngr){
                        listMngr.submit({}, false);
                    }else{
                        location.reload();
                    }
                } else {
                    app.alert.error(errors);
                }
            });
        });
    }

    function refresh()
    {
        listMngr.submit({}, false);
    }
    
    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        },
        refresh:refresh
    }
}());