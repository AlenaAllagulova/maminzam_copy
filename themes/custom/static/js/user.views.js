var jUserViews = (function(){
    var inited = false, o = {lang:{}};
    var $block;
    var avatarUploader = {};

    function init()
    {
        $block = $('#profile-collapse');
        var $statPopup = false;
        $('#j-show-stat').click(function(){
            if( $statPopup === false ) {
                bff.ajax(bff.ajaxURL('users', 'views-stat'), {id: o.id}, function(data, errors){
                    if(data && data.success) {
                        $block.after(data.popup);
                        $statPopup = $('#j-stat-modal');
                        bff.st.includeJS('d3.v3.min.js', function(){
                            viewsChart('#j-stat-chart', data.stat.data, data.lang);
                            $statPopup.modal('show');
                        });
                    } else {
                        app.alert.error(errors);
                    }
                });
            } else {
                $statPopup.modal('show');
            }
            return false;
        });

        // avatar uploader
        $cont = $('#j-my-profile-avatar');

        avatarUploader = new qq.FileUploaderBasic({
            button: $cont.find('#j-my-avatar-upload').get(0),
            action: bff.ajaxURL('users&ev=profileAvatar', ''),
            params: {hash: app.csrf_token, act: 'avatar-upload'},
            uploaded: 0, multiple: false, sizeLimit: o.avatarMaxsize,
            allowedExtensions: ['jpeg','jpg','png','gif'],
            onSubmit: function(id, fileName) {
                setAvatarImg(false, true);
            },
            onComplete: function(id, fileName, data) {
                if(data && data.success) {
                    setAvatarImg(data[o.avatarSzBig]);
                } else {
                    if(data.errors) {
                        app.alert.error(data.errors, {title: o.lang.ava_upload});
                        function reload() {
                            location.reload();
                        }
                        setTimeout(reload, 2000);
                    }
                }
                return true;
            },
            messages: o.lang.ava_upload_messages,
            showMessage: function(message, code) {
                app.alert.error(message, {title: o.lang.ava_upload});
            }
        });

        $cont.on('click', '#j-my-avatar-delete', function(e){ nothing(e);
            bff.ajax(
                bff.ajaxURL('users&ev=profileAvatar', 'avatar-delete'),
                {hash: app.csrf_token},
                function(data,errors){
                    if(data && data.success) {
                        setAvatarImg(data[o.avatarSzBig]);
                    }else{
                        if(errors) {
                            app.alert.error(errors);
                        }
                    }
                });
            return false;
        });

    }

    function setAvatarImg(img, bProgress)
    {
        var $img = $cont.find('#j-my-avatar-img');
        if(bProgress){
            $img.parent().addClass('hidden').after(o.avatarUploadProgress);
        }else{
            $img.parent().removeClass('hidden').next('.j-progress').remove();
        }

        if( img ) {
            $img.attr('src', img);
        } else {
            //
        }
    }

    function viewsChart(blockID, data, lang)
    {
        var dateFormat = d3.time.format("%Y-%m-%d");
        data.map(function(d) {
            var date = dateFormat.parse(d.date);
            d.total = +d.total;
            d.date = date.getDate()+' '+lang.shortMonths[date.getMonth()];
            d.dateFull = date.getDate()+' '+lang.months[date.getMonth()]+' '+date.getFullYear();
            return d;
        });

        var verticalDate = (( data.length >= 12 ) ? 25 : 0);
        var margin = {top: 5, right: 30, bottom: 20 + verticalDate, left: 45},
            width = 600 - margin.left - margin.right,
            height = (310 + verticalDate) - margin.top - margin.bottom;

        var x = d3.scale.ordinal().rangeRoundBands([0, width], .5);
        var xAxis = d3.svg.axis().scale(x).orient("bottom");

        var y = d3.scale.linear().range([height, 0]);
        var yAxis = d3.svg.axis().scale(y).orient("left");//.ticks(7);

        var chart = d3.select(blockID).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var barTooltip = d3.select(blockID).append("div")
            .attr("class", "bar-tooltip")
            .style("opacity", 0);

        x.domain(data.map(function(d){ return d.date; }));
        y.domain([0, d3.max(data, function(d){ return d.total; })+2]);

        chart.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);
        if( verticalDate ) {
            chart.selectAll("text")
                .attr("y", -4)
                .attr("x", -8)
                .attr("transform", "rotate(-90)")
                .style("text-anchor", "end");
        }

        chart.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", -40)
            .attr("x", -(height/2) )
            .attr("dy", ".71em")
            .style("text-anchor", "middle")
            .text(lang.y_title);

        var bar = chart.selectAll(".bar")
            .data(data)
            .enter().append("g")
            .attr("class", "bar")
            .attr("data-index", function(d,i){ return i; })
            .on("mousemove", function(d) {
                var pos = d3.mouse(this);
                barTooltip.transition().duration(100).style("opacity", 1);
                barTooltip.html("<span><b>"+d.dateFull+"</b></span> <span>"+lang.views+": <b> "+ d.total+"</b></span>")
                    .style("left", (pos[0] + 65) + "px")
                    .style("top", (pos[1] + 10) + "px");
            })
            .on("mouseout", function(d) {
                barTooltip.transition().duration(50).style("opacity", 0);
            });

        bar.append("rect")
            .attr("class", "bar-item")
            .attr("x", function(d) { return x(d.date); })
            .attr("width", x.rangeBand())
            .attr("y", function(d) { return y(d.total); })
            .attr("height", function(d) { return height - y(d.total); });

        bar.append("text")
            .attr("class", "bar-cnt-total")
            .attr("x", function(d) { return x(d.date) + ( x.rangeBand() / 2 ); })
            .attr("y", function(d) { return y(d.total) - 3; })
            .text(function(d) { return d.total; });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());

