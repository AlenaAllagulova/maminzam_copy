var jOrdersOpinions = (function() {
    var inited = false, o = {lang:{}, block:''};
    var $block, $list;

    function init()
    {
        $block = $('#'+ o.block);
        if( ! $block.length) return;
        $list = $block.find('.j-list');

        var $addModal = false, form = false;
        $list.on('click', '.j-opinion-add', function(e){
            e.preventDefault();
            var $el = $(this);
            if($addModal){
                form.$field('order_id').val($el.data('id'));
                form.$field('message').val('').trigger('keyup');
                $addModal.modal('show');
            }else{
                bff.ajax(bff.ajaxURL('opinions', 'modal-add'), {hash:app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        if(resp.html){
                            $addModal = $(resp.html);
                            app.$B.append($addModal);
                            form = app.form($addModal.find('form'), function($f){
                                var f = this;
                                if( ! form.checkRequired({focus:true}) ) return;
                                if( ! $f.find('[name="type"]:checked').length){
                                    app.alert.error(o.lang.check_type);
                                    return;
                                }
                                f.ajax(bff.ajaxURL('opinions', 'opinion-add'),{},function(resp, errors){
                                    if(resp && resp.success) {
                                        $addModal.modal('hide');
                                        if(resp.msg){
                                            f.alertSuccess(resp.msg);
                                        }
                                        if(typeof(jOrdersRespondentList) == 'object'){
                                            jOrdersRespondentList.refresh();
                                        }
                                        if(typeof(jOrdersOwnerList) == 'object'){
                                            jOrdersOwnerList.refresh();
                                        }
                                    } else {
                                        f.alertError(errors);
                                    }
                                });
                            });
                            bff.maxlength(form.$field('message'), {limit:500, message:$addModal.find('.j-help-block'), lang:{left: o.lang.left, symbols:o.lang.symbols}});
                            form.$field('order_id').val($el.data('id'));
                            $addModal.modal('show');
                        }
                    } else {
                        app.alert.error(errors);
                    }
                });
            }
        });

        $list.on('click', '.j-opinion-show', function(e){
            e.preventDefault();
            var $el = $(this);
            var id = $el.data('id');
            var $modal = $('#j-modal-opinion-'+id);
            if($modal.length){
                $modal.modal('show');
                return;
            }
            bff.ajax(bff.ajaxURL('opinions', 'modal-view'), {id:id,hash:app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    if(resp.html){
                        $modal = $(resp.html);
                        app.$B.append($modal);
                        $el.trigger('click');
                        return false;
                    }
                } else {
                    app.alert.error(errors);
                }
            });
        });

    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }

}());