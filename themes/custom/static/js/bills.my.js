var jBillsMyHistory = (function(){

    var inited = false, o = {lang:{}}, listMngr;
    var $block, $form, $list, $pgn;
    function init()
    {
        $block = $('#j-bills-history');

        $form = $('#j-bills-history-form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');

        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll){
                    $.scrollTo($list, {offset: -150, duration:500});
                }
                $list.html(resp.list);
                $pgn.html(resp.pgn);
            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onPopstate: function() {
            },
            ajax: o.ajax
        });

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
            $form.on('click', '.j-submit', function(e){
                nothing(e);
                listMngr.submit({}, true);
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());

var jBillsMyPay = (function(){
    var inited = false, o = {lang:{}, url_submit:''};

    function init()
    {
        var $form = $('#j-my-pay-form'); if ( ! $form.length ) return;
        var f = app.form($form, function(){
            if( ! f.checkRequired() ) return;
            f.ajax(o.url_submit, {}, function(resp, errors){
                if(resp && resp.success) {
                    if (resp.hasOwnProperty('redirect')) {
                        bff.redirect(resp.redirect);
                    } else if (resp.hasOwnProperty('message')) {
                        $form.html(resp.message);
                    } else {
                        jBillsSelectPay.pay(resp.form);
                    }
                } else {
                    app.alert.error(errors);
                }
            });
        });
        var $labels = $form.find('.j-ps-item');
        $form.on('change', '.j-ps-radio', function(){
            $labels.removeClass('active');
            $(this).closest('.j-ps-item').addClass('active');
        });

        $form.on('click', '.j-cancel', function(){
            if(document.referrer == ""){
                document.location = o.url_back;
            }else{
                history.back();
            }
            return false;
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
})();

var jBillsSelectPay = (function(){
    var inited = false, o = {lang:{}}, $formRequest, $block;

    function init()
    {
        $block = $('#j-bills-select-pay-system');

        app.$B.append('<div id="j-pay-form-request" class="hidden"></div>');
        $formRequest = $('#j-pay-form-request');

        var $labels = $block.find('.j-ps-item');
        $block.on('change', '.j-ps-radio', function(){
            $labels.removeClass('active');
            $(this).closest('.j-ps-item').addClass('active');
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        },
        pay: function(html){
            $formRequest.html(html).find('form:first').submit();
        }
    };
})();