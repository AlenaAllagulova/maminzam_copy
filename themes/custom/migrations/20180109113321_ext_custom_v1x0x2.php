<?php

use bff\db\migrations\Migration as Migration;

class ExtCustomV1x0x2 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(DB_PREFIX . 'specializations')
            ->addColumn('price_vacancy', 'integer', ['signed' => false])
            ->addColumn('period_vacancy', 'integer', ['signed' => false])
            ->addColumn('price_vacancy_enabled', 'boolean', ['default' => false])
            ->update();
        $this->table(DB_PREFIX . 'orders')
            ->addColumn('price_vacancy', 'integer', ['signed' => false])
            ->addColumn('vacancy_activated_to', 'datetime', ['default' => '0000-00-00 00:00:00'])
            ->update();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->table(DB_PREFIX . 'specializations')->removeColumn('price_vacancy')->update();
        $this->table(DB_PREFIX . 'specializations')->removeColumn('period_vacancy')->update();
        $this->table(DB_PREFIX . 'specializations')->removeColumn('price_vacancy_enabled')->update();
        $this->table(DB_PREFIX . 'orders')->removeColumn('price_vacancy')->update();
        $this->table(DB_PREFIX . 'orders')->removeColumn('vacancy_activated_to')->update();
    }
}