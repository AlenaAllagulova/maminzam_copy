<?php

use bff\db\migrations\Migration as Migration;

class ExtCustomV1x0x13 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(DB_PREFIX . 'specializations_lang')
            ->addColumn('title_index', 'string', ['limit' => 200, 'after'=>'title', 'null' => false, 'default' => ''])
            ->update();

    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->table(DB_PREFIX . 'specializations_lang')->removeColumn('title_index')->update();

    }
}