<?php

use bff\db\migrations\Migration as Migration;

class ExtCustomV1x0x8 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(DB_PREFIX . 'specializations_dp')
            ->addColumn('group_id', 'text', [
                'null'  => true,
            ])
            ->update();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->table(DB_PREFIX . 'specializations_dp')->removeColumn('group_id')->update();
    }
}