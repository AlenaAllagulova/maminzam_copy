<?php

use bff\db\migrations\Migration as Migration;

class ExtCustomV1x0x5 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(
            TABLE_ORDERS_NOTES,
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', ['signed' => false, 'identity' => true])
            ->addColumn('author_id', 'integer', ['signed' => false, 'null' => false, 'default' => false])
            ->addColumn('order_id', 'integer', ['signed' => false, 'null' => false, 'default' => false])
            ->addColumn('note', 'text', ['null' => true])
            ->addIndex(['author_id','order_id'], ['unique' => true, 'name' => 'uniq_author_order'])
            ->create();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->dropIfExists(TABLE_ORDERS_NOTES);

    }
}