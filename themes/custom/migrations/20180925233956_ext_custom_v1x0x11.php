<?php

use bff\db\migrations\Migration as Migration;

class ExtCustomV1x0x11 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(DB_PREFIX . 'users')
            ->addColumn('order_from_formula', 'float', [
                'default' => 0,
            ])
            ->update();

        $this->table(
            DB_PREFIX . 'rating_values', [
                'engine' => 'InnoDB',
            ])
            ->addColumn('user_type', 'integer', ['null' => true, 'default' => null])
            ->addColumn('sys_key', 'string', ['limit' => 10, 'null' => false])
            ->addColumn('enable', 'boolean')
            ->addColumn('stars_count', 'integer', ['limit' => 3, 'null' => true, 'default' => null])
            ->addColumn('stars_step', 'integer', ['limit' => 3, 'null' => true, 'default' => null])
            ->create();

        $this->table(
            DB_PREFIX . 'rating_values_lang', [
                'engine' => 'InnoDB',
                'id' => false,
                'primary_key' => ['id', 'lang']
            ])
            ->addColumn('id', 'integer', ['null' => false])
            ->addColumn('lang', 'string', ['limit' => 2, 'null' => false, 'default' => ''])
            ->addColumn('title', 'string', ['limit' => 200, 'null' => false, 'default' => ''])
            ->addIndex(['id', 'lang'], ['unique' => true])
            ->create();

        $this->table(
            DB_PREFIX . 'rating_users', [
                'engine' => 'InnoDB',
            ])
            ->addColumn('user_id', 'integer', ['limit' => 11, 'null' => false])
            ->addColumn('total_average', 'float', ['null' => false, 'default' => 0])
            ->addColumn('stars_average', 'text')
            ->addColumn('modified', 'datetime', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
            ->create();

        $this->table(
            DB_PREFIX . 'rating_opinions', [
                'engine' => 'InnoDB',
            ])
            ->addColumn('author_id', 'integer', ['limit' => 11, 'null' => false])
            ->addColumn('user_id', 'integer', ['limit' => 11, 'null' => false])
            ->addColumn('opinion_id', 'integer', ['limit' => 11, 'null' => false])
            ->addColumn('stars', 'text')
            ->addColumn('sum', 'integer', ['limit' => 11, 'null' => false, 'default' => 0])
            ->addColumn('modified', 'datetime', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
            ->addIndex(['author_id', 'user_id', 'opinion_id'], ['unique' => true])
            ->create();

        $data = [
            [
                'config_name' => 'rating_getFormula',
                'config_value' => '((({OP|DI1}-{ON|DI1})/100)*50)+((({OP|DI2}-{ON|DI2})/100)*20)+(({LO|DI1}/100)*30)+{RU}',
            ],
            [
                'config_name' => 'rating_getFormulaPrepare',
                'config_value' => '',
            ],
            [
                'config_name' => 'rating_getIntervalOne',
                'config_value' => '90',
            ],
            [
                'config_name' => 'rating_getIntervalTree',
                'config_value' => '3600',
            ],
            [
                'config_name' => 'rating_getIntervalTwo',
                'config_value' => '360',
            ],
            [
                'config_name' => 'rating_getLastActionCron',
                'config_value' => '2017-04-05 17:30:02',
            ],
            [
                'config_name' => 'rating_getNegativeOpinion',
                'config_value' => '-100',
            ],
            [
                'config_name' => 'rating_getNeutralOpinion',
                'config_value' => '0',
            ],
            [
                'config_name' => 'rating_getPositiveOpinion',
                'config_value' => '100',
            ],
            [
                'config_name' => 'rating_getRatingOneStar',
                'config_value' => '5',
            ]
        ];

        $this->table(DB_PREFIX . 'config')
            ->insert($data)
            ->update();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->table(DB_PREFIX . 'users')
            ->removeColumn('order_from_formula')
            ->update();
        
        $this->dropIfExists(DB_PREFIX . 'rating_values');
        $this->dropIfExists(DB_PREFIX . 'rating_values_lang');
        $this->dropIfExists(DB_PREFIX . 'rating_users');
        $this->dropIfExists(DB_PREFIX . 'rating_opinions');

        $this->execute('DELETE FROM ' . DB_PREFIX . 'config WHERE config_name like \'rating_get%\'');
    }
}