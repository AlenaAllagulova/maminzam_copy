<?php

use bff\db\migrations\Migration as Migration;

class ExtCustomV1x0x0 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $data = [ 'config_name'  => 'users_opinions_delay',
            'config_value' => 1,
            'is_dynamic'   => false
        ];


        $table = $this->table(DB_PREFIX.'config');
        $table->insert($data)->update();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->execute('DELETE FROM '.DB_PREFIX.'config WHERE config_name =  \'users_opinions_delay\'');
    }
}