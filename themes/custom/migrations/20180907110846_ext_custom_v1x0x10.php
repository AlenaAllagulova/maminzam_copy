<?php

use bff\db\migrations\Migration as Migration;

class ExtCustomV1x0x10 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(DB_PREFIX . 'orders')
            ->addColumn('start_date', 'timestamp', ['default' => '0000-00-00 00:00:00'])
            ->update();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->table(DB_PREFIX . 'orders')->removeColumn('start_date')->update();
    }
}