<?php

use bff\db\migrations\Migration as Migration;

class ExtCustomV1x0x6 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(
            TABLE_ORDERS_SCHEDULE,
            ['engine' => 'InnoDB', 'id' => false])
            ->addColumn('order_id', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('day_id', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('period_id', 'integer', ['signed' => false, 'null' => false])
            ->addIndex(['order_id', 'day_id','period_id'], ['unique' => true, 'name' => 'uniq_order_day_period'])
            ->create();

        $this->table(
            TABLE_USERS_SCHEDULE,
            ['engine' => 'InnoDB', 'id' => false])
            ->addColumn('user_id', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('day_id', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('period_id', 'integer', ['signed' => false, 'null' => false])
            ->addIndex(['user_id', 'day_id','period_id'], ['unique' => true, 'name' => 'uniq_user_day_period'])
            ->create();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->dropIfExists(TABLE_ORDERS_SCHEDULE);
        $this->dropIfExists(TABLE_USERS_SCHEDULE);

    }
}