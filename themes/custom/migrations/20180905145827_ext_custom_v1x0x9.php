<?php

use bff\db\migrations\Migration as Migration;

class ExtCustomV1x0x9 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {

        $sql = [];
        foreach (range(16, 20) as $number) {
            $sql[] = ' MODIFY f'.$number.' DOUBLE NOT NULL DEFAULT 0';
        }
        $this->execute('ALTER TABLE '.DB_PREFIX .'users_specializations '. join(',', $sql).';');

        $sql = [];
        foreach (range(21, 50) as $number) {
            $sql[] = ' ADD COLUMN f'.$number.' DOUBLE NOT NULL DEFAULT 0';
        }
        foreach (range(51, 55) as $number) {
            $sql[] = ' ADD COLUMN f'.$number.' TEXT NULL ';
        }

        $this->execute( 'ALTER TABLE '.DB_PREFIX .'users_specializations '.join(',', $sql).';');
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $sql = [];
        foreach (range(16, 20) as $number) {
            $sql[] = ' MODIFY f'.$number.' TEXT NULL';
        }
        $this->execute('ALTER TABLE '.DB_PREFIX .'users_specializations '. join(',', $sql).';');

        $sql = [];
        foreach (range(21, 55) as $number) {
            $sql[] = ' DROP COLUMN f'.$number;
        }
        $this->execute('ALTER TABLE '.DB_PREFIX .'users_specializations '. join(',', $sql).';');

    }
}