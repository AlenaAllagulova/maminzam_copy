<?php

use bff\db\migrations\Migration as Migration;

class ExtCustomV1x0x1 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $data = [ 'config_name'  => 'users_pro_contacts_cnt',
            'config_value' => 1,
            'is_dynamic'   => false
        ];


        $table = $this->table(DB_PREFIX.'config');
        $table->insert($data)->update();

        $this->table(DB_PREFIX . 'users')
            ->addColumn('pro_opening_cnt', 'integer', ['signed' => false])
            ->update();

        $this->table(
            TABLE_USERS_OPENED_CONTACTS,
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['user_id', 'opened_user_id']])
            ->addColumn('user_id', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('opened_user_id', 'integer', ['signed' => false, 'null' => false])
            ->addForeignKey(
                ['user_id'],
                TABLE_USERS,
                'user_id',
                ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->create();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->execute('DELETE FROM '.DB_PREFIX.'config WHERE config_name =  \'users_pro_contacts_cnt\'');

        $this->table(DB_PREFIX . 'users')->removeColumn('pro_opening_cnt')->update();

        $this->dropIfExists(TABLE_USERS_OPENED_CONTACTS);
    }
}