<?php

use bff\db\migrations\Migration as Migration;

class ExtCustomV1x0x4 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(DB_PREFIX . 'orders')
            ->addColumn('district_id', 'integer', ['signed' => false, 'default' => 0])
            ->update();

        $this->table(DB_PREFIX . 'users')
                    ->addColumn('district_id', 'integer', ['signed' => false, 'default' => 0])
                    ->update();

    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->table(DB_PREFIX . 'orders')->removeColumn('district_id')->update();
        $this->table(DB_PREFIX . 'users')->removeColumn('district_id')->update();

    }
}