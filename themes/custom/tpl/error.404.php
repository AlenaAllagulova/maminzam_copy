<div class="container">

    <section class="s-signBlock">

        <div class="s-signBlock-top">
            <div class="s-signBlock-logo">
                <a href="<?= Geo::url() ?>"><img src="<?= bff::url('/img/logo-green.svg') ?>" alt="" /></a>
            </div>
            <h1 class="small"><?= _t('error', 'Похоже, данная страница не найдена'); ?></h1>
        </div>

        <div class="error-page">
            <h2><?= $errno ?></h2>

            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <form class="form" role="form" method="get" action="<?= Orders::url('list') ?>">
                        <div class="input-group">
                            <input type="search" class="form-control" name="q" maxlength="80" placeholder="<?= _t('error', 'Что Вы ищите?'); ?>" />
                            <span class="input-group-btn">
                                <button class="btn-block-sm btn btn-sm btn-clear-filter " type="submit">
                                    <i class=" icon-search "></i></button>
                              </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
