<?php
# данные отсутствуют (общее их кол-во == 0)
if( $total <= 1 || ! $settings['pages'] ) return;
?>
<ul class="pagination">
    <? if($prev): ?><li><a <?= $prev ?>><i class="icon-arrow-point-to-left icon"></i></a></li><? else: ?><li class="disabled"><span><i class="icon-arrow-point-to-left icon"></i></span></li><? endif; ?>
    <? if($first): ?><li><a<?= $first['attr'] ?>><?= $first['page'] ?></a></li><li><span>...</span></li><? endif; ?>
    <? foreach($pages as $v): ?>
        <? if($v['active']): ?>
            <li class="active"><span><?= $v['page'] ?></span></li>
        <? else: ?>
            <li class="hidden-xs"><a <?= $v['attr'] ?>><?= $v['page'] ?></a></li>
        <? endif; ?>
    <? endforeach; ?>
    <? if($last): ?><li><span>...</span></li><li><a<?= $last['attr'] ?>><?= $last['page'] ?></a></li><? endif; ?>
    <? if($next): ?><li><a <?= $next ?>><i class="icon-arrow-point-to-right icon"></i></a></li><? else: ?><li class="disabled"><span><i class="icon-arrow-point-to-right icon"></i></span></li><? endif; ?>
</ul>
<? if($settings['pageto'] ): ?>
<div class="pagination-num">
    <label for="pagination-num"><?= _t('', 'Перейти на страницу'); ?></label>
    <input type="text" class="form-control j-pgn-goto" placeholder="№">
</div>
<? endif; ?>
