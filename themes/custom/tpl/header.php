<?php
    $url = array(
        'user.login'    => Users::url('login'),
        'user.register' => Users::url('register'),
        'user.logout'   => Users::url('logout'),
    );

    # Counters and data Contacts Packages
    $nAvalOpenContactsCnt = Users::avaliableTotalOpeningContactsCnt();
    $nAvalPayedContactsCnt = Users::avaliableTotalOpeningContactsCnt(true);
    $nAvalProOpenContactsCnt = Users::avaliableTotalOpeningContactsCnt(false, true);
    $aAvaliablePackages = Users::avaliableTotalOpeningContactsCnt(false, false, true);
    if(!empty($aAvaliablePackages)){
        $aAvaliablePackages['tooltip'] = '';
        foreach ($aAvaliablePackages as $k=>$package){
            # tooltip для пакетов контактов
            $aAvaliablePackages['tooltip'] .=  !isset($aAvaliablePackages[$k]['pack_settings']['title']) ? '' :
                                                $aAvaliablePackages[$k]['pack_settings']['title'].':&nbsp;'.
                                                $aAvaliablePackages[$k]['left_quantity'].'&nbsp;(из&nbsp;'.
                                                $aAvaliablePackages[$k]['pack_settings']['quantity'].')'.PHP_EOL;
        }
    }
    $bUserPackageUnlim = Users::avaliableCurrentUserPackageUnlim();


    # Default or selected main filter settings
    $sUrlBtnSearch = Site::btnSearchUrl();
    $aSpecList = Specializations::model()->specializationsListing(['enabled' =>true]);
    $aSelectedSpec =  Specializations::getSelectedSpec();
    $nSelectedDistrict = Site::i()->input->get('dt', TYPE_UINT);
    $bUserFilledQuestionary = Users::isUserFilledQuestionary();
    $bShowModalLeaveOrder = User::id() && User::isClient() && !Users::isUserLeaveOrder() && (Site::isCurrentUserProfilePage() || Site::isUsersListingPage());
?>
<?if($bShowModalLeaveOrder):?>
<div id="modalProposalAdd" class="modal fade modal-success" role="dialog">
    <div class="modal-dialog">
        <img src="<?= bff::url('/img/proposal-bg.png')?>" alt="">
        <div class="modal-content">
            <div class="modal-body text-center">
                <p class=" modal-success__article">
                    <?= _t('','Пусть няни сами вас находят!')?>
                </p>
                <span>
                    <?= _t('','Создайте объявление, и тогда няни или бебиситтеры сами свяжутся с вами.')?>
                </span>
            </div>
            <div class="text-center pdb30">
                <a href="<?= Svc::url('list') ?>" class="btn btn-primary pdr30 pdl30">
                    <?= _t('', 'Начать покупки') ?>
                </a>
            </div>
        </div>
    </div>
</div>
    <div>
        <span><?=_t('', 'Пусть няни сами вас находят ...')?></span>
        <a href="<?=Orders::url('add')?>" class="btn btn-primary"><?=_t('', 'Создать объявление')?></a>
    </div>
<?endif;?>
    <!-- Topline -->
<? if( ! bff::security()->isLogined() ) : ?>
    <div class="header-top">
        <div class="container">
            <div class="header-top__wrap">
                <? if(DEVICE_DESKTOP): ?>
                    <div class="header-loader j-loader-script">
                        <div class="container">
                            <div class="header-loader__wrap">
                                <div class=" flex flex_center">
                                    <div class="header-loader__circle mrgr20"></div>
                                    <div class="header-loader__box"></div>
                                </div>
                                <div class=" flex flex_center">
                                    <div class="header-loader__box-sm mrgr20"></div>
                                    <div class="header-loader__box-sm mrgr20"></div>
                                    <div class="header-loader__box-sm"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="flex flex_center flex_sb w100p">
                        <a class="header-top__logo mrgr10" href="<?= Geo::url(); ?>">
                            <img src="<?= bff::url('/img/logo-green.svg') ?>" alt="">
                        </a>

                        <a href="<?= $sUrlBtnSearch ?>" class="btn btn-primary ">
                            <i class="icon-search"></i>
                            <?= _t('','Искать маминзама')?>
                        </a>
                        <span></span>
                    </div>

                    <div class="enter-nav enter-nav_g nowrap">
                        <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_WORKER]) ?>" class="enter-nav__item">
                            <?= _t('users', 'Присоединиться как няня'); ?>
                        </a>
                        <a href="<?= $url['user.login'] ?>" class="enter-nav__item">
                            <?= _t('users', 'Вход'); ?>
                        </a>
                        <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_CLIENT]) ?>" class="enter-nav__item enter-nav__item_circle">
                            <?= _t('users', 'Регистрация'); ?>
                        </a>
                    </div>
                <? else: ?>
                    <div class="flex flex_center">
                        <a class="header-top__logo" href="<?= Geo::url(); ?>">
                            <img src="<?= bff::url('/img/logo-green.svg') ?>" alt="">
                        </a>
                    </div>

                    <div class="menu-enter-sm">
                        <div class="menu-enter-sm flex flex_center">
                            <a href="<?= $sUrlBtnSearch ?>" class="icon-search mrgr20"></a>
                            <a href="javascript:void(0);" class="icon-burger icon-burger_black j-open-menu" >
                                <span class="path1"></span>
                                <span class="path1"></span>
                                <span class="path1"></span>
                            </a>
                        </div>

                        <div class="collapse j-menu-list menu-sm fade">
                            <div class="header-top__wrap color-white">
                                <a class="header-top__logo" href="<?= Geo::url(); ?>">
                                    <img src="<?= bff::url('/img/logo-white.svg') ?>" alt="">
                                </a>
                                <i class="icon-cancel-music j-close-menu"></i>
                            </div>
                            <div class=" flex flex_column flex_center color-white bold">
                                <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_WORKER]) ?>" class="enter-nav__item mrgb30">
                                    <?= _t('users', 'Присоединиться как няня'); ?>
                                </a>
                                <a href="<?= $url['user.login'] ?>" class="enter-nav__item mrgb30">
                                    <?= _t('users', 'Вход'); ?>
                                </a>
                                <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_CLIENT]) ?>" class="enter-nav__item enter-nav__item_circle">
                                    <?= _t('users', 'Регистрация'); ?>
                                </a>
                            </div>
                            <div class=""></div>

                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
    <div class="header-top-70"></div>
<? else :
    $user = User::data(array('name', 'surname', 'login', 'balance', 'workflow', 'avatar', 'sex', 'user_id'));
    $user += bff::security()->userCounter(array());
    $user_avatar_small = UsersAvatar::url( $user['user_id'], $user['avatar'], UsersAvatar::szSmall, $user['sex']);
    $workflowEnabled = bff::fairplayEnabled() && $user['workflow'];
    if( ! isset($user['cnt_internalmail_new'])){ $user['cnt_internalmail_new'] = 0; }
    if( ! isset($user['cnt_orders_offers_client'])){ $user['cnt_orders_offers_client'] = 0; }
    if( ! isset($user['cnt_orders_offers_worker'])){ $user['cnt_orders_offers_worker'] = 0; }
    if( ! isset($user['cnt_fairplay_workflows'])){ $user['cnt_fairplay_workflows'] = 0; }
    if(Users::useClient()){
        if(User::isWorker()){
            $user['new_orders_cnt'] = $user['cnt_orders_offers_worker'];
        }else{
            $user['new_orders_cnt'] = $user['cnt_orders_offers_client'];
        }
        $msgOrders = tpl::declension($user['new_orders_cnt'], _t('orders', 'новое событие;новых события;новых событий'));
    }else{
        $user['new_orders_cnt'] = $user['cnt_orders_offers_client'];
        $user['new_offers_cnt'] = $user['cnt_orders_offers_worker'];
        $msgOrders = tpl::declension($user['new_orders_cnt'], _t('orders', 'новое событие;новых события;новых событий'));
        $msgOffers = tpl::declension($user['new_offers_cnt'], _t('orders', 'новое событие;новых события;новых событий'));
    }
    $userDropdown = array();
    $url += array(
        'my.settings' => Users::url('my.settings'),
        'my.messages' => InternalMail::url('my.messages'),
        'my.orders'   => Users::url('profile', array('login'=>$user['login'],'tab'=>'orders')),
        'my.offers'   => Users::url('profile', array('login'=>$user['login'],'tab'=>'offers')),
        'my.cabinet'  => Users::url('profile', array('login'=>$user['login'])),
        'my.history'  => Bills::url('my.history'),
    );
    if($workflowEnabled){
        $url += array(
            'my.workflows'   => Users::url('profile', array('login'=>$user['login'],'tab'=>'workflows')),
        );
        $msgWorkflows = tpl::declension($user['cnt_fairplay_workflows'], _t('orders', 'новое событие;новых события;новых событий'));
    }
    if(empty($user['name'])){
        $user['name'] = '['.$user['login'].']';
    }
    $isPro = User::isPro();
    $buyPro = bff::servicesEnabled() && User::isWorker() && ! $isPro && Users::proEnabled();
    $mobileCounter = $user['cnt_internalmail_new'] + $user['new_orders_cnt'] + (isset($user['new_offers_cnt']) ? $user['new_offers_cnt'] : 0);
?>
    <div class="header-top">
        <div class="container">
            <div class="header-top__wrap">
                <? if(DEVICE_DESKTOP): ?>
                    <div class="header-loader j-loader-script">
                        <div class="container">
                            <div class="header-loader__wrap">
                                <div class=" flex flex_center">
                                    <div class="header-loader__circle mrgr20"></div>
                                    <div class="header-loader__box"></div>
                                </div>
                                <div class=" flex flex_center">
                                    <div class="header-loader__box-sm mrgr20"></div>
                                    <div class="header-loader__box-sm mrgr20"></div>
                                    <div class="header-loader__circle"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="flex flex_center flex_sb w100p">
                        <a class="header-top__logo mrgr10" href="<?= Geo::url(); ?>">
                            <img src="<?= bff::url('/img/logo-green.svg') ?>" alt="">
                        </a>

                        <a href="<?= $sUrlBtnSearch ?>" class="btn btn-primary ">
                            <i class="icon-search"></i>
                            <? if(Users::useClient() && !User::isWorker()): ?>
                                <?= _t('','Искать маминзама')?>
                            <? else: ?>
                                <?= _t('','Искать родителя')?>
                            <? endif; ?>
                        </a>
                        <span></span>

                    </div>

                <? $sMsgTitle = tpl::declension( $user['cnt_internalmail_new'], _t('internalmail', 'новое сообщение;новых сообщений;новых сообщений') ); ?>
                    <ul class="user-nav nowrap">
                        <li class="user-nav__item">
                            <a href="<?= $url['my.messages'] ?>" class="user-nav__link">
                                <i class=" icon-envelope  mrgr5"></i>
                                <span><?= _t('', 'Сообщения'); ?></span>
                                <span class="user-nav__notif <?= ! $user['cnt_internalmail_new'] ? 'hidden' : '' ?> j-cnt-msg">
                                    +<?= $user['cnt_internalmail_new'] ?>
                                </span>
                            </a>
                        </li>
                        <li class="user-nav__item">
                            <a href="<?= $url['my.orders'] ?>" class="user-nav__link">
                                <i class="icon-copy mrgr5"></i>
                                <span><?= _t('users', 'Заказы') ?></span>

                                <span class="user-nav__notif <?= ! $user['new_orders_cnt'] ? 'hidden' : '' ?>">
                                    +<?= $user['new_orders_cnt'] ?>
                                </span>
                            </a>
                        </li>
                        <? if(!Users::useClient()) { ?>
                            <li class="user-nav__item">
                                <a href="<?= $url['my.offers'] ?>" class="user-nav__link">
                                    <i class="fa fa-comments mrgr5"></i>
                                    <span><?= _t('orders', 'Объявления') ?></span>
                                    <span class="user-nav__notif <?= ! $user['new_offers_cnt'] ? 'hidden' : '' ?>">
                                        +<?= $user['new_offers_cnt'] ?>
                                    </span>
                                </a>
                            </li>
                        <? } ?>
                        <? if(bff::servicesEnabled()): ?>
                            <li class="user-nav__item" >
                                <a href="<?= $url['my.history'] ?>" class="user-nav__link">
                                    <i class="icon-wallet mrgr5"></i>
                                    <span class="mrgr5"><?= _t('bill', 'Счет:'); ?> </span>
                                    <?= $user['balance'] ?> <?= Site::currencyDefault(); ?>
                                </a>
                            </li>
                        <? endif; ?>


                        <li class="user-nav__item flex_0-0-a">
                            <div class="flex flex_center flex_jcc">
                                <a href="<?= Users::url('profile', array('login' => $user['login'])) ?>" class="user-nav__avatar ">
                                    <? if(!empty($user_avatar_small)): ?>
                                        <img src="<?= $user_avatar_small ?>" alt="<?= tpl::avatarAlt($user); ?>" />
                                    <? else: ?>

                                    <? endif; ?>
                                </a>

                                <div class="mrgl10 relative">
                                    <a href="#" data-toggle="dropdown" class="user-nav__link">
                                        <i class=" icon-arrow-point-to-down"></i>
                                    </a>
                                    <ul class=" dropdown-head">
                                        <li>
                                            <a href="<?= $url['my.cabinet'] ?>" class="dropdown-head__item dropdown-head__avatar">
                                                <img src="<?= $user_avatar_small ?>" alt="<?= tpl::avatarAlt($user); ?>" />
                                                <span><?= $user['name'] ?></span>
                                                <? if($isPro): ?>
                                                    <span class="mrgl5 mrgr40">
                                                        <span class="pro">pro</span>
                                                    </span>
                                                <? endif; ?>
                                            </a>
                                        </li>
                                        <li class="">
                                            <?if($bUserPackageUnlim):?>
                                                <a href="<?= Packages::url('packages_buy')?>" class="dropdown-head__item">
                                                    <i class="icon-shopping-bag-check"></i>
                                                    <?= _t('','Безлимитное открытие контактов')?>
                                                </a>
                                            <?else:?>
                                                <a href="<?= Users::url('buy_contacts')?>" class="dropdown-head__item show-tooltip"
                                                   data-original-title="<?=_t('opening_contacts', ((Users::useClient() && User::isWorker()) ?
                                                           'Pro:&nbsp;[pro]'.PHP_EOL :
                                                           ''
                                                       ).'Оплачено:&nbsp;[payed]'.
                                                       (isset($aAvaliablePackages['tooltip'])?
                                                           PHP_EOL.$aAvaliablePackages['tooltip'] :
                                                           ''),
                                                       ['pro' => $nAvalProOpenContactsCnt, 'payed' => $nAvalPayedContactsCnt,])?>"
                                                   data-placement="bottom">
                                                    <i class="icon-copy icon-copy_tr"></i>
                                                    <span class="">
                                                        <?= _t('','Доступно') ?>
                                                        <?=tpl::declension($nAvalOpenContactsCnt, 'контакт;контакта;контактов');?>
                                                    </span>
                                                </a>
                                            <?endif;?>
                                        </li>
                                        <li>
                                            <a href="<?= Svc::url('list') ?>" class="dropdown-head__item">
                                                <i class=" icon-shopping-bag1 "></i>
                                                <?= _t('','Платные услуги')?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?= $url['my.settings'] ?>" class="dropdown-head__item">
                                                <i class=" icon-settings "></i>
                                                <?= _t('users', 'Настройки'); ?>
                                            </a>
                                        </li>
                                        <? if(Users::accountsJoin()): ?>
                                            <li>
                                                <a href="javascript:;" onclick="app.user.changeJoin();" class="dropdown-head__item">
                                                    <i class="fa fa-refresh"></i>
                                                    <?= User::isWorker() ? _t('users', 'В профиль заказчика') : _t('users', 'В профиль исполнителя') ?>
                                                </a>
                                            </li>
                                        <? endif; ?>
                                        <li>
                                            <a href="<?= $url['user.logout'] ?>" class="dropdown-head__item">
                                                <i class="icon-logout2"></i>
                                                <?= _t('users', 'Выход'); ?>
                                            </a>
                                        </li>
                                        <? if($buyPro): ?>
                                            <li class="dropdown-head__bt">
                                                <a href="<?= Svc::url('view', array('keyword' => 'pro')) ?>" class="dropdown-head__item">
                                                    <?= _t('svc', 'Стать'); ?>
                                                    <span class="mrgl5">
                                                        <span class="pro">pro</span>
                                                    </span>
                                                </a>
                                            </li>
                                        <? endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>

                <? else: ?>
                    <div class="flex flex_center">
                        <a class="header-top__logo" href="<?= Geo::url(); ?>">
                            <img src="<?= bff::url('/img/logo-green.svg') ?>" alt="">
                        </a>
                    </div>
                    <div class="menu-enter-sm flex flex_center">
                        <a href="<?= $sUrlBtnSearch ?>" class="icon-search mrgr20"></a>
                        <a href="javascript:void(0);" class="icon-burger icon-burger_black" data-toggle="collapse" data-target="#j-open-user-menu" >
                            <span class="path1"></span>
                            <span class="path1"></span>
                            <span class="path1"></span>
                        </a>
                    </div>

                    <div class="collapse fade drop-menu-sm" id="j-open-user-menu">
                        <div class="">
                            <div class="container">
                                <div class="header-sm-top">
                                    <a class="header-top__logo" href="<?= Geo::url(); ?>">
                                        <img src="<?= bff::url('/img/logo-green.svg') ?>" alt="">
                                    </a>
                                    <a class="icon-cancel-music" data-toggle="collapse" data-target="#j-open-user-menu" ></a>
                                </div>
                            </div>
                            <div class="bt1"></div>
                            <ul class=" dropdown-head dropdown-head_md">
                                <li>
                                    <a href="<?= $url['my.cabinet'] ?>" class="dropdown-head__item dropdown-head__avatar">
                                        <img src="<?= $user_avatar_small ?>" alt="<?= tpl::avatarAlt($user); ?>" />
                                        <span><?= $user['name'] ?></span>
                                        <? if($isPro): ?>
                                            <span class="mrgl5">
                                                <span class="pro">pro</span>
                                            </span>
                                        <? endif; ?>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="<?= $url['my.messages'] ?>" class="dropdown-head__item">
                                        <i class=" icon-envelope  mrgr5"></i>
                                        <span><?= _t('', 'Сообщения'); ?></span>
                                        <span class="user-nav__notif <?= ! $user['cnt_internalmail_new'] ? 'hidden' : '' ?> j-cnt-msg">
                                            +<?= $user['cnt_internalmail_new'] ?>
                                        </span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="<?= $url['my.orders'] ?>" class="dropdown-head__item">
                                        <i class="icon-copy mrgr5"></i>
                                        <span><?= _t('users', 'Заказы') ?></span>

                                        <span class="user-nav__notif <?= ! $user['new_orders_cnt'] ? 'hidden' : '' ?>">
                                            +<?= $user['new_orders_cnt'] ?>
                                        </span>
                                    </a>
                                </li>
                                <? if(!Users::useClient()) { ?>
                                    <li class="">
                                        <a href="<?= $url['my.offers'] ?>" class="dropdown-head__item">
                                            <i class="fa fa-comments mrgr5"></i>
                                            <span><?= _t('orders', 'Объявления') ?></span>
                                            <span class="user-nav__notif <?= ! $user['new_offers_cnt'] ? 'hidden' : '' ?>">
                                                +<?= $user['new_offers_cnt'] ?>
                                            </span>
                                        </a>
                                    </li>
                                <? } ?>
                                <? if(bff::servicesEnabled()): ?>
                                    <li class="" >
                                        <a href="<?= $url['my.history'] ?>" class="dropdown-head__item">
                                            <i class="icon-wallet mrgr5"></i>
                                            <span class="mrgr5"><?= _t('bill', 'Счет:'); ?> </span>
                                            <?= $user['balance'] ?> <?= Site::currencyDefault(); ?>
                                        </a>
                                    </li>
                                <? endif; ?>
                                <li>
                                    <?if($bUserPackageUnlim):?>
                                        <a href="<?= Packages::url('packages_buy')?>" class="dropdown-head__item">
                                            <i class="icon-shopping-bag-check"></i>
                                            <?= _t('','Безлимитное открытие контактов')?>
                                        </a>
                                    <?else:?>
                                        <a href="<?= Users::url('buy_contacts')?>" class="dropdown-head__item show-tooltip"
                                           data-original-title="<?=_t('opening_contacts', ((Users::useClient() && User::isWorker()) ?
                                                   'Pro:&nbsp;[pro]'.PHP_EOL :
                                                   ''
                                               ).'Оплачено:&nbsp;[payed]'.
                                               (isset($aAvaliablePackages['tooltip'])?
                                                   PHP_EOL.$aAvaliablePackages['tooltip'] :
                                                   ''),
                                               ['pro' => $nAvalProOpenContactsCnt, 'payed' => $nAvalPayedContactsCnt,])?>"
                                           data-placement="bottom">
                                            <i class="icon-copy icon-copy_tr"></i>
                                            <span class="">
                                                <?= _t('','Доступно') ?>
                                                <?=tpl::declension($nAvalOpenContactsCnt, 'контакт;контакта;контактов');?>
                                            </span>
                                        </a>
                                    <?endif;?>
                                </li>
                                <li>
                                    <a href="<?= Svc::url('list') ?>" class="dropdown-head__item">
                                        <i class=" icon-shopping-bag1 "></i>
                                        <?= _t('','Платные услуги')?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= $url['my.settings'] ?>" class="dropdown-head__item">
                                        <i class=" icon-settings "></i>
                                        <?= _t('users', 'Настройки'); ?>
                                    </a>
                                </li>
                                <? if(Users::accountsJoin()): ?>
                                    <li>
                                        <a href="javascript:;" onclick="app.user.changeJoin();" class="dropdown-head__item">
                                            <i class="fa fa-refresh"></i>
                                            <?= User::isWorker() ? _t('users', 'В профиль заказчика') : _t('users', 'В профиль исполнителя') ?>
                                        </a>
                                    </li>
                                <? endif; ?>
                                <li>
                                    <a href="<?= $url['user.logout'] ?>" class="dropdown-head__item">
                                        <i class="icon-logout2"></i>
                                        <?= _t('users', 'Выход'); ?>
                                    </a>
                                </li>
                                <? if($buyPro): ?>
                                    <li class="dropdown-head__bt">
                                        <a href="<?= Svc::url('view', array('keyword' => 'pro')) ?>" class="dropdown-head__item">
                                            <?= _t('svc', 'Стать'); ?>
                                            <span class="mrgl5">
                                                <span class="pro">pro</span>
                                            </span>
                                        </a>
                                    </li>
                                <? endif; ?>
                            </ul>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
    <div class="header-top-70"></div>

<? endif; ?>
<? if( User::isWorker() && !$bUserFilledQuestionary):  ?>
    <div class="alert-proposal <?=(Site::isSettingsQuestionaryPage()?'hide':'')?>" id="j-fill-questionary">
        <div class="alert-proposal__box">
            <img src="<?= bff::url('/img/wink.svg')?>" alt="" class="mrgr5" width="25">
            <?= _t('orders', '<a [href]>Заполните анкету,</a> чтобы получать заказы.',
                array('href' => 'href="'.(Users::url('my.settings', ['tab'=>'questionary'])).'"')); ?>
        </div>
    </div>
<? endif; ?>
<? # Баннер: Растяжка сверху (top) ?>
<? if($banner = Banners::view('top', array('pos'=>'top', 'no-empty'=>true))){ ?>
<div class="l-topBanner">
	<?= $banner ?>
</div>
<? } ?>

<?if($bShowModalLeaveOrder):?>
    <script type="text/javascript">
        <? js::start() ?>
        if('<?= Site::isCurrentUserProfilePage()?>' == '1' && localStorage.getItem('UserProfilePage') == 'true') {
        }else {
            $(window).on('load',function(){
                $('#modalProposalAdd').modal('show');
                localStorage.setItem('UserProfilePage', 'true');
            });
        }
        <? js::stop() ?>
    </script>
<? endif; ?>