<div class="container">

    <section class="s-signBlock">

        <div class="s-signBlock-top">
            <div class="s-signBlock-logo">
                <a href="<?= Geo::url() ?>"><img src="<?= bff::url('/img/logo-green.svg') ?>" alt="" /></a>
            </div><!-- /.signBlock-logo -->
            <h1 class="small"><?= $message ?></h1>
        </div><!-- /.signBlock-top -->

        <? if( ! empty($errno)): ?>
        <div class="error-page">
            <h2><?= $errno ?></h2>
        </div>
        <? endif; ?>

    </section>

</div><!-- /.container -->