<?php
    $url = array(
        'user.login'    => Users::url('login'),
        'user.register' => Users::url('register'),
        'user.logout'   => Users::url('logout'),
    );

    $nAvalOpenContactsCnt = Users::avaliableTotalOpeningContactsCnt();
    $nAvalPayedContactsCnt = Users::avaliableTotalOpeningContactsCnt(true);
    $nAvalProOpenContactsCnt = Users::avaliableTotalOpeningContactsCnt(false, true);
    $aAvaliablePackages = Users::avaliableTotalOpeningContactsCnt(false, false, true);
    if(!empty($aAvaliablePackages)){
        $aAvaliablePackages['tooltip'] = '';
        foreach ($aAvaliablePackages as $k=>$package){
            # tooltip для пакетов контактов
            $aAvaliablePackages['tooltip'] .=  !isset($aAvaliablePackages[$k]['pack_settings']['title']) ? '' :
                                                $aAvaliablePackages[$k]['pack_settings']['title'].':&nbsp;'.
                                                $aAvaliablePackages[$k]['left_quantity'].'&nbsp;(из&nbsp;'.
                                                $aAvaliablePackages[$k]['pack_settings']['quantity'].')'.PHP_EOL;
        }
    }
    $bUserPackageUnlim = Users::avaliableCurrentUserPackageUnlim();

    $sUrlBtnSearch = Site::btnSearchUrl();
    $aSpecList = Specializations::model()->specializationsListing(['enabled' =>true]);
?>


<div class="general-bg " style="background: url(<?= bff::url('/img/general-bg.jpg')?>);">
    <? if( ! bff::security()->isLogined() ) : ?>
        <div class="header-top header-top_index">
            <div class="container">
                <div class="header-top__wrap">
                    <a class="header-top__logo" href="<?= Geo::url(); ?>">
                        <img src="<?= bff::url('/img/logo-white.svg') ?>" alt="">
                    </a>
                    <? if(DEVICE_DESKTOP_OR_TABLET): ?>
                        <div class="enter-nav">
                            <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_WORKER]) ?>" class="enter-nav__item">
                                <?= _t('users', 'Присоединиться как няня'); ?>
                            </a>
                            <a href="<?= $url['user.login'] ?>" class="enter-nav__item">
                                <?= _t('users', 'Вход'); ?>
                            </a>
                            <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_CLIENT]) ?>" class="enter-nav__item enter-nav__item_circle">
                                <?= _t('users', 'Регистрация'); ?>
                            </a>
                        </div>
                    <? else: ?>
                        <div class="menu-enter-sm">
                            <a class="icon-burger j-open-menu" >
                                <span class="path1"></span>
                                <span class="path1"></span>
                                <span class="path1"></span>
                            </a>

                            <div class="collapse j-menu-list menu-sm fade">
                                <div class="header-top__wrap color-white">
                                    <a class="header-top__logo" href="<?= Geo::url(); ?>">
                                        <img src="<?= bff::url('/img/logo-white.svg') ?>" alt="">
                                    </a>
                                    <i class="icon-cancel-music j-close-menu"></i>
                                </div>
                                <div class=" flex flex_column flex_center color-white bold">
                                    <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_WORKER]) ?>" class="enter-nav__item mrgb30">
                                        <?= _t('users', 'Присоединиться как няня'); ?>
                                    </a>
                                    <a href="<?= $url['user.login'] ?>" class="enter-nav__item mrgb30">
                                        <?= _t('users', 'Вход'); ?>
                                    </a>
                                    <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_CLIENT]) ?>" class="enter-nav__item enter-nav__item_circle">
                                        <?= _t('users', 'Регистрация'); ?>
                                    </a>
                                </div>
                                <div class=""></div>

                            </div>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    <? endif; ?>

    <div class="container">
        <div class="search-general search-general_index">
            <h1><?= _t('','Найдите своему ребенку идеальную няню')?></h1>
            <div class="search-general__wrap">

                <div id="j-index-search-form">
                    <div class="search-general__select">
                        <? if( $geoFilter = Geo::filterEnabled()): ?>
                            <? $geo = Geo::filter();?>
                            <div class="index-select">
                                <div class="header-loader header-loader_index j-loader-script"></div>
                                <div class="index-select__item">
                                    <span  class="index-select__title index-select__title_abs index-select__title-md">
                                        <?= _t('','Я ищу')?>
                                    </span>
                                    <div class="dropdown dropdown-custom">
                                        <? $i = 0; ?>
                                        <? foreach ($aSpecList as $spec):?>
                                            <button value="<?=$spec['keyword']?>" class="j-input-select dropdown-custom__link-filter" id="dropdownMenu1" data-toggle="dropdown" >
                                                <?=$spec['title_index']?>
                                            </button>
                                            <? $i++; ?>
                                            <? if ($i > 0): ?>
                                                <? break ?>
                                            <? endif ?>
                                        <? endforeach;?>
                                        <ul class="dropdown-custom__menu dropdown-custom__menu_t40" aria-labelledby="dropdownMenu1">
                                            <? $t = 0; ?>
                                            <? foreach ($aSpecList as $spec):?>
                                                <li>
                                                    <? if ($t == 0): ?>
                                                        <a href="javascript:void(0);" class="j-select-keyword active" data-value="<?= $spec['keyword']?>">
                                                            <?=$spec['title_index']?>
                                                        </a>
                                                    <? else: ?>
                                                        <a href="javascript:void(0);" class="j-select-keyword" data-value="<?= $spec['keyword']?>">
                                                            <?=$spec['title_index']?>
                                                        </a>
                                                    <? endif; ?>
                                                </li>
                                                <? $t++; ?>

                                            <? endforeach;?>
                                        </ul>
                                    </div>

                                </div>

                                <div class="index-select__item ">
                                    <span  class="index-select__title index-select__title_abs index-select__title-md">
                                        <?= _t('','Район')?>
                                    </span>
                                    <?if(Geo::defaultCity()):?>
                                        <div class="select-custom select-custom_nb w100">
                                            <select name="dt" class="j-select-search" style="width: 100%">
                                                <option value="0" data-select2-id="0"><?= _t('','Все районы') ?></option>
                                                <?= HTML::selectOptions(Geo::districtList(Geo::defaultCity()), false, '', 'id', 'title');?>
                                            </select>
                                        </div>
                                    <?else:?>
                                    <?= $geoFilter ? Geo::i()->filterForm() : '' ?>
                                    <?endif;?>

                                </div>
                            </div>
                        <? endif; ?>
                        <a href="<?= $sUrlBtnSearch ?>" class="btn-search-g j-btn-form-search">
                            <?= _t('', 'Искать') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="">

    </div>

</div>

<script type="text/javascript">
<? js::start() ?>

$(document).on('click', '.j-btn-form-search', function (e) {
    e.preventDefault();
    $(this).attr('href', '');
    var link = '<?= $sUrlBtnSearch ?>';
    var type = $('.j-input-select').val();
    var distr = $('.j-select-search').val();
    var buildLink = link;

    if(type.length != 0){
        buildLink +=  type;
    }
    if(distr >= 1){
        buildLink += '/?dt=' + distr;
    }
    $(this).attr('href', buildLink);
    location = buildLink;
});

$(document).on('click','.j-select-keyword', function(){
    $input = $('.j-input-select');
    $('.j-select-keyword').removeClass('active');
    $(this).addClass('active');
    $input.html($(this).text());
    $input.val($(this).data('value'));
});

<? js::stop() ?>
</script>
