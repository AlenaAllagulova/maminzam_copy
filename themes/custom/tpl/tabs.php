<ul class="tabs" id="<?= $class ?>">
    <? foreach($tabs as $k => $v): ?>
        <li class="tabs__item j-tab-<?= $k ?><?= $a == $k ? ' active' : '' ?>">
            <a href="<?= ! empty($v['url']) ? $v['url'] : '#' ?>" data-a="<?= $k ?>" class="tabs__link">
                <?= $v['t'] ?>
            </a>
        </li>
    <? endforeach; ?>
</ul>
<? if(empty($noAjax)): ?>
<script type="text/javascript">
<? js::start() ?>
app.tabs('<?= $class ?>');
<? js::stop() ?>
</script>
<? endif; ?>
