<?php

const TABLE_USERS_OPENED_CONTACTS = DB_PREFIX.'users_opened_contacts';

class Theme_Custom extends Theme
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'theme_title'   => 'maminzam',
            'theme_version' => '1.0.0',
        ));
    }

    /**
     * Запуск темы
     */
    protected function start()
    {
        \bff::hookAdd('css.extra', function() {
            tpl::includeCSS('../libs/slick/slick');
            tpl::includeCSS('../libs/select2/select2.min');
            tpl::includeCSS('../libs/star-rating/star-rating.min');
            tpl::includeCSS('dist/maminzam');
        });

        \bff::hookAdd('js.extra', function() {
            tpl::includeJS($this->url('libs/slick/slick.min.js'));
            tpl::includeJS($this->url('libs/select2/select2.full.min.js'));
            tpl::includeJS('../libs/star-rating/star-rating.min');
            tpl::includeJS($this->url('js/dist/scripts.js'));
        });


        bff::classExtension('OpinionsBase_','Theme_Custom_OpinionsBase', $this->path('modules/opinions/opinions.bl.class.php'));
        bff::classExtension('Orders_','Theme_Custom_Orders', $this->path('modules/orders/orders.class.php'));
        bff::classExtension('OrdersModel_','Theme_Custom_OrdersModel', $this->path('modules/orders/orders.model.php'));
        bff::classExtension('OrdersBase_','Theme_Custom_OrdersBase', $this->path('modules/orders/orders.bl.class.php'));
        bff::classExtension('UsersBase_','Theme_Custom_UsersBase', $this->path('modules/users/users.bl.class.php'));
        bff::classExtension('Users_','Theme_Custom_UsersClass', $this->path('modules/users/users.class.php'));
        bff::classExtension('UsersModel_','Theme_Custom_UsersModel', $this->path('modules/users/users.model.php'));
        bff::classExtension('Specializations_','Theme_Custom_Specializations', $this->path('modules/specializations/specializations.class.php'));
        bff::classExtension('Svc_','Theme_Custom_Svc', $this->path('modules/svc/svc.class.php'));

        if (bff::adminPanel()){
            bff::classExtension('Users_','Theme_Custom_Users_Adm', $this->path('modules/users/users.adm.class.php'));
            bff::classExtension('Specializations_','Theme_Custom_Specializations_Adm', $this->path('modules/specializations/specializations.adm.class.php'));

        }

        bff::hookAdd('orders.url', function($url, $args){
            switch ($args['key']) {
                # Активация заказа (вакансии)
                case 'activation':
                $url .= '/orders/activation' . Orders::urlQuery($args['opts']);
                break;
            }
            return $url;
        });

        bff::hookAdd('users.url', function($url, $args){

            switch ($args['key']) {
                # Покупка контактов пользователя
                case 'buy_contacts':
                $url .= '/user/buy_contacts/';
                break;
            }
            return $url;
        });

        bff::hookAdd('orders.order.validate.step2', function($list){
            $this->input->postm(['price_vacancy' => TYPE_UINT], $list['data']);
            return $list;

        });

        bff::hookAdd('routes', function($list, $options){

            if($list['orders-action']){
                $list['orders-action']['pattern'] = 'orders/(add|edit|promote|activate|status|rss|activation)(/|)';
            }

            if($list['user-action']){
                $list['user-action']['pattern'] = 'user/(login|logout|register|forgot|activate|settings|buy_contacts)(/|)';
            }
            return $list;
        });

    }
}