<?php

class Theme_Custom_Specializations_Adm extends Theme_Custom_Specializations_Adm_Base
{
    /**
     * Обрабатываем параметры запроса, добавлены настройки платного размещения вакансии
     * @param integer $nSpecializationID ID специализации или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateSpecializationData($nSpecializationID, $bSubmit)
    {
        $aData = array();
        $this->input->postm_lang($this->model->langSpecializations, $aData);
        $aParam = array(
            'keyword'    => TYPE_NOTAGS, # URL-Keyword
            'enabled'    => TYPE_BOOL,   # Включена
            'price_sett' => TYPE_ARRAY,  # Настройки цены
            # Использовать базовый шаблон SEO:
            'orders_mtemplate' => TYPE_BOOL, # Модуль: Orders
            'users_mtemplate'  => TYPE_BOOL, # Модуль: Users
            'qa_mtemplate'     => TYPE_BOOL, # Модуль: Qa
            'price_mtemplate'  => TYPE_BOOL, # Модуль: Users
            'price_vacancy'    => TYPE_UINT, # Цена размещения вакансии
            'period_vacancy'    => TYPE_UINT, # Период размещения вакансии
            'price_vacancy_enabled'    => TYPE_BOOL, # Платное размещение вакансии
        );
        $this->input->postm($aParam, $aData);

        $this->validateSpecializationPriceSettings($aData['price_sett'], $bSubmit);
        if ($bSubmit) {
            # URL-Keyword
            $aData['keyword'] = $this->db->getKeyword($aData['keyword'], $aData['title'][LNG], TABLE_SPECIALIZATIONS, $nSpecializationID, 'keyword', 'id');

            if ($nSpecializationID != Specializations::ROOT_SPEC) {
                $aData['pid'] = Specializations::ROOT_SPEC;
            } else {
                $aData['pid'] = 0;
            }
            if(!$aData['price_vacancy'] || !$aData['period_vacancy']){
                $aData['price_vacancy_enabled'] = 0;
            }
        } else {
            if ( ! $nSpecializationID) {
                $aData['orders_mtemplate'] = 1;
                $aData['users_mtemplate']  = 1;
                $aData['qa_mtemplate']     = 1;
                $aData['price_mtemplate']  = 1;
            }
        }

        return $aData;
    }
}