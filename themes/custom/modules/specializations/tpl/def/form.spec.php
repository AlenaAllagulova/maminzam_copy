<?php
/**
 * @var $this Specializations
 * @var $inputName string
 */
    tpl::includeJS('specs.select', false, 5);
    if( ! isset($spec_id)) $spec_id = 0;
    if( ! isset($cat_id)) $cat_id = 0;

    if (isset($order_id)){
        $aOrderData =  Orders::model()->orderData($order_id, ['status']);
        $isClosedEditSpec = ($aOrderData['status'] == Orders::STATUS_ONACTIVATION);
    }
?>
<div class="dropdown j-spec-select<? if($disabled){ ?> hidden<? } ?>" data="{count:<?= $index ?>}" data-disabled="<?= $disabled ?>">
    <input type="hidden" class="j-disabled" name="<?= $inputName ?>[<?= $index ?>][disabled]" value="<?= $disabled ?>" autocomplete="off" />
    <input type="hidden" class="j-cat-value" name="<?= $inputName ?>[<?= $index ?>][1]" value="<?= $cat_id ?>" autocomplete="off"/>
    <input type="hidden" class="j-spec-value" name="<?= $inputName ?>[<?= $index ?>][spec]" value="<?= $spec_id ?>" autocomplete="off"/>
    <a href="#" class="dropdown-toggle pull-left j-title-empty<?= $spec_id ? ' hide' : '' ?>" data-toggle="dropdown">
            <span>
                <?= ! empty($emptyTitle) ? $emptyTitle : _t('specs', 'Выбрать специализацию'); ?>
            </span>
        <i class=" icon-arrow-point-to-down"></i>
    </a>
    <a <?= (isset($isClosedEditSpec) && $isClosedEditSpec)?'':'href="#"'?>
            class="dropdown-toggle ajax-link pull-left j-title-selected<?= ! $spec_id ? ' hide' : '' ?>"
        <?= (isset($isClosedEditSpec) && $isClosedEditSpec)?'':'data-toggle="dropdown"'?>>
        <span class="j-title"><?= ! empty($cat_title) && empty($noCatOnSelect) ? $cat_title.' / ' : '' ?><?= ! empty($spec_title) ? $spec_title : '' ?></span>
        <?= (isset($isClosedEditSpec) && $isClosedEditSpec)?'':'<i class="icon-arrow-point-to-down"></i>'?>
    </a>
    <? if( ! empty($allowDelete)): ?>
        <span class="p-profileCabinet-delete">
            <a href="#" class="p-delete j-delete" title="<?= _t('form', 'Удалить'); ?>">
                <i class="fa fa-trash-o"></i>
            </a>
        </span>
    <? endif; ?>
    <div class="clearfix"></div>
    <ul class="dropdown-menu dropdown-menu-def mega-dropdown j-menu" role="menu">
        <?= $this->viewPHP($aData, 'form.specs.ajax'); ?>
    </ul>
</div>