<?php use bff\db\Dynprops;

/**
 * @var $this bff\db\Dynprops
 * @var $dynprops array for simple view in order view.php
 */
$dynprops = json_encode($dynprops);
echo $dynprops;
