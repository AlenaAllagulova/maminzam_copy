<?php
/**
 * @var Specializations $this
 */
$aData = HTML::escape($aData, 'html', array('keyword'));
$edit = !empty($id);
$aTabs = array(
    'info' => array('t' => 'Основные'),
);
$is_root = ( $edit && $id == Specializations::ROOT_SPEC );
if ( ! $is_root) {
    if(Specializations::useServices()) { $aTabs['services'] = array('t' => 'Услуги', 'disabled' => ! $edit); }
    $aTabs['seo-orders'] = array('t' => 'SEO: Заказы');
    $aTabs['seo-users'] = array('t' => 'SEO: Исполнители');
    $aTabs['seo-qa'] = array('t' => 'SEO: Вопрос-Ответ');
    $aTabs['seo-price'] = array('t' => 'SEO: Прайс');
}
if( ! $edit ) {
    $price_sett['rates'] = array();
    $price_sett['ex'] = 0;
}
if (!bff::moduleExists('qa')) {
    unset($aTabs['seo-qa']);
}
?>
<div id="j-specializations-form-block">
    <form name="SpecializationsForm" id="SpecializationsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
        <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
        <input type="hidden" name="save" value="1" />
        <input type="hidden" name="id" value="<?= $id ?>" />
        <? if (sizeof($aTabs) > 1) { ?>
            <div class="tabsBar" id="SpecializationsFormTabs">
                <? foreach($aTabs as $k=>$v) { ?>
                    <span class="tab<? if($k == 'info') { ?> tab-active<? } ?><?= ! empty($v['disabled']) ? ' disabled' : '' ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v['t'] ?></a></span>
                <? } ?>
            </div>
        <? } ?>
        <!-- таб: Основные -->
        <div class="j-tab j-tab-info">
            <table class="admtbl tbledit">
                <? if($is_root): ?>
                    <?= $this->locale->buildForm($aData, 'specializations-item','
                    <tr class="">
                        <td class="row1 field-title">Название:</td>
                        <td class="row2">
                            <div class="bold <?= $key ?>" style="height: 25px;"><?= HTML::escape($aData[\'title\'][$key]); ?></div>
                            <input class="stretch <?= $key ?>" type="hidden" id="specialization-title-<?= $key ?>" name="title[<?= $key ?>]" value="<?= HTML::escape($aData[\'title\'][$key]); ?>" />
                        </td>
                    </tr>
                    ', array('onchange'=>'jSpecFormPrice.onLang')); ?>
                <? else: ?>
                    <?= $this->locale->buildForm($aData, 'specializations-item','
                        <tr class="required">
                            <td class="row1 field-title">Название<span class="required-mark">*</span>:</td>
                            <td class="row2">
                                <input class="stretch <?= $key ?>" type="text" id="specialization-title-<?= $key ?>" name="title[<?= $key ?>]" value="<?= HTML::escape($aData[\'title\'][$key]); ?>" />
                            </td>
                        </tr>
                        ', array('onchange'=>'jSpecFormPrice.onLang')); ?>
                    <?= $this->locale->buildForm($aData, 'specializations-item','
                        <tr class="required">
                            <td class="row1 field-title">Название на главной<span class="required-mark">*</span>:</td>
                            <td class="row2">
                                <input class="stretch <?= $key ?>" type="text" id="specialization-title-index-<?= $key ?>" name="title_index[<?= $key ?>]" value="<?= HTML::escape($aData[\'title_index\'][$key]); ?>" />
                            </td>
                        </tr>
                        ', array('onchange'=>'jSpecFormPrice.onLang')); ?>
                    <tr class="required">
                        <td class="row1 field-title" width="100">URL-Keyword<span class="required-mark">*</span>:<br /><a href="#" onclick="return bff.generateKeyword('#specialization-title-<?= LNG ?>', '#specialization-keyword');" class="ajax desc small"><?= _t('', 'generate') ?></a></td>
                        <td class="row2">
                            <input class="stretch" type="text" id="specialization-keyword" name="keyword" value="<?= $keyword ?>" />
                        </td>
                    </tr>
                <? endif; ?>
                <tr>
                    <td class="row1 field-title">Цена:</td>
                    <td class="row2">
                        <?= $priceForm; ?>
                    </td>
                </tr>
                <? if(isset($price_vacancy) && isset($period_vacancy) && isset($price_vacancy_enabled)):?>
                    <tr>
                        <td class="row1 field-title"><?=_t('', 'Платная вакансия')?></td>
                        <td class="row2">
                            <label class="checkbox">
                                <input type="checkbox" name="price_vacancy_enabled"<? if($price_vacancy_enabled){ ?> checked="checked"<? } ?> />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="row1 field-title" style="width: 150px"><?=_t('', 'Цена вакансии, [curr]:', ['curr' => Site::currencyDefault()])?></td>
                        <td class="row2">
                            <input type="number" min="0" step="1" class="short" name="price_vacancy" value="<?= HTML::escape($price_vacancy)?>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="row1 field-title"><?=_t('', 'Период размещения вакансии, дней:')?></td>
                        <td class="row2">
                            <input type="number" min="0" step="1" class="short" name="period_vacancy" value="<?= HTML::escape($period_vacancy)?>"/>
                        </td>
                    </tr>
                <? endif;?>
                <tr<? if($is_root) { ?> class="displaynone" <? } ?>>
                    <td class="row1 field-title"><?= _t('', 'Enabled') ?>:</td>
                    <td class="row2">
                        <label class="checkbox"><input type="checkbox" id="specialization-enabled" name="enabled"<? if($enabled){ ?> checked="checked"<? } ?> /></label>
                    </td>
                </tr>
            </table>
        </div>
        <!-- таб: SEO: Заказы -->
        <div class="j-tab j-tab-seo-orders hidden">
            <?= SEO::i()->form(Orders::i(), $aData, 'search-service', array('name_prefix'=>'orders_')); ?>
        </div>
        <!-- таб: SEO: Исполнители -->
        <div class="j-tab j-tab-seo-users hidden">
            <?= SEO::i()->form(Users::i(), $aData, 'search-spec', array('name_prefix'=>'users_')); ?>
        </div>
        <? if (bff::moduleExists('qa')) { ?>
            <!-- таб: SEO: Вопрос-Ответ -->
            <div class="j-tab j-tab-seo-qa hidden">
                <?= SEO::i()->form(Qa::i(), $aData, 'listing-spec', array('name_prefix'=>'qa_')); ?>
            </div>
        <? } ?>
        <!-- таб: SEO: Прайс -->
        <div class="j-tab j-tab-seo-price hidden">
            <?= SEO::i()->form(Users::i(), $aData, 'price-spec', array('name_prefix'=>'price_')); ?>
        </div>
        <div style="margin-top: 10px;" id="j-buttons-block">
            <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jSpecializationsForm.save(false);" />
            <? if($edit) { ?><input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jSpecializationsForm.save(true);" /><? } ?>
            <? if($edit && ! $is_root) { ?><input type="button" onclick="jSpecializationsForm.del(); return false;" class="btn btn-danger button delete" value="<?= _t('', 'Delete') ?>" /><? } ?>
            <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jSpecializationsFormManager.action('cancel');" />
        </div>
    </form>
    <? if(Specializations::useServices()): ?>
        <!-- таб: услуги -->
        <div class="j-tab j-tab-services hidden">
            <? if($edit): ?><?= $this->viewPHP($aData, 'admin.specialization.services.list'); ?><? else: ?>
                <div class="alert alert-info">Добавлять услуги можно после сохранения специализации</div>
            <? endif; ?>
        </div>
    <? endif; ?>
</div>

<script type="text/javascript">
    var jSpecializationsForm =
        (function(){
            var $block, $progress, $form, formChk, id = <?= $id ?>;
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $block = $('#j-specializations-form-block');
                $progress = $('#SpecializationsFormProgress');
                $form = $('#SpecializationsForm');

                // tabs
                var $buttons = $('#j-buttons-block');
                $form.find('#SpecializationsFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                    var key = $(this).data('key');
                    $block.find('.j-tab').addClass('hidden');
                    $block.find('.j-tab-'+key).removeClass('hidden');
                    $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
                    $buttons.toggleClass('displaynone', key == 'services');
                    if(key == 'services'){
                        <? if($edit): ?>jSpecializationServicesList.onShow();<? endif; ?>
                    }
                });
            });
            return {
                del: function()
                {
                    if( id > 0 ) {
                        bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                            false, {progress: $progress, repaint: false, onComplete:function(){
                                bff.success('Запись успешно удалена');
                                jSpecializationsFormManager.action('cancel');
                                jSpecializationsList.refresh();
                            }});
                    }
                },
                save: function(returnToList)
                {
                    if( ! formChk.check(true) ) return;
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('Данные успешно сохранены');
                            if(returnToList || ! id) {
                                jSpecializationsFormManager.action('cancel');
                                jSpecializationsList.refresh( ! id);
                            }else{
                                <? if(Specializations::useServices()): ?>
                                jSpecializationsServicesManager.update();
                                <? endif; ?>
                            }
                        }
                    }, $progress);
                },
                onShow: function()
                {
                    formChk = new bff.formChecker($form);
                }
            };
        }());
    var jSpecFormPrice =
        (function(){
            var $block, $rates, i = 1;
            var lng = '<?= $this->locale->getDefaultLanguage(); ?>';

            $(function(){
                $block = $('#j-spec-price');
                $rates = $('#j-price-rates');

                $rates.on('click', '.j-price-rates-del', function(){
                    $(this).closest('.j-rate').remove();
                    return false;
                });

                <? if( ! empty($price_sett['rates'])): foreach($price_sett['rates'] as $k => $v): ?>addRate(<?= func::php2js($v) ?>, <?= $k ?>);<? endforeach;endif; ?>
            });

            function addRate(o, index)
            {
                if(intval(index)){ i = intval(index); }
                var html = <?= func::php2js($this->locale->formField('price_sett[rates][__index__]', array(), 'text', array('class' => 'j-lang'))); ?>;
                html = html.replace(/__index__/g, i);
                $rates.append('<tr class="j-rate j-rate-'+i+'"><td>'+html+'<a class="but cross j-price-rates-del" href="#" style="margin-left:7px;"></a></td></tr>');

                if(typeof(o) == 'object'){
                    var $r = $rates.find('.j-rate-'+i);
                    for(var ln in o){
                        if(o.hasOwnProperty(ln)){
                            $r.find('input.j-lang-form-'+ln).val(o[ln]);
                        }
                    }
                }

                var $bl = $rates.find('.j-rate-'+i);
                $bl.find('.j-lang-form').addClass('displaynone');
                $bl.find('.j-lang-form-' + lng).removeClass('displaynone');
                i++;
            }

            return {
                addRate: addRate,
                onLang: function (lngKey) {
                    if(lngKey) lng = lngKey;
                }
            };
        }());
</script>