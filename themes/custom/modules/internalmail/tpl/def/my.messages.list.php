<?php

$lng_fav = _t('internalmail', 'Избранные');
$lng_ignore = _t('internalmail', 'Игнорирую');
$lng_move_f = _t('internalmail', '');
$conversation_url = InternalMail::url('my.messages', array('i'=>''));

$inFolder = function(&$userFolders, $folderID){
    return ( ! empty($userFolders) && in_array($folderID, $userFolders) );
};

if( ! empty($list)): ?>
    <ul class="i-imailList media-list">
<? foreach($list as $v):
    $message = &$v['message'];
    ?>
        <li class="">
            <div class="">
                <div class="row">
                    <div class="col-lg-5 col-sm-6 col-xs-12 i-imailList__col  flex flex_center ">
                        <? if( InternalMail::FOLDERS_ENABLED ):
                            $title = $lng_move_f;
                            if( ! empty($v['folders'])){
                                $title = array(); foreach($v['folders'] as $vv) { $title[] = $folders[ $vv ]['title']; }
                                $title = join(', ', $title);
                            } ?>
                            <div class="dropdown mrgr20">
                                <a href="#" id="dLabel" class="dropdown-toggle chat-header__dropdown ajax-link" data-toggle="dropdown">
                                    <i class=" icon-folder"></i>
                                    <span class="j-f-title"><?= $title ?></span>
                                    <i class=" icon-arrow-point-to-down"></i>
                                </a>
                                <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                                    <? foreach($folders as $kk => $vv): if( ! $kk) continue; ?>
                                        <li class="<?= ! empty($vv['class']) ? $vv['class'] : '' ?><?= in_array($kk, $v['folders']) ? ' active' : '' ?>"><a href="#" data-user-id="<?= $v['user_id'] ?>" data-folder-id="<?= $kk ?>" class="j-f-action"><?= $vv['title'] ?></a></li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        <? endif; ?>
                        <div class="user-box mrgr10 ">
                            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="user-box__avatar">
                                <img src="<?= $v['avatar'] ?>" alt="<?= tpl::avatarAlt($v); ?>" />
                                <?= tpl::userOnline($v) ?>
                            </a>
                            <span class="user-box__text mrgl10">
                                <span class="">
                                    <?= tpl::userLink($v,'no-login, no-verified, no-pro') ?>
                                </span>
                                <? if(!empty($v['contacts_only_phones'])):?>
                                <? foreach ($v['contacts_only_phones'] as $phone):?>
                                    <? if($phone['t'] == Users::CONTACT_TYPE_PHONE && !empty($phone['v'])):?>
                                        <span>
                                            <?=$phone['v']?>
                                        </span>
                                        <? break; # clazion: согласно интерфейсу: ограничение вывода только одного номера ?>
                                    <? endif;?>
                                <?endforeach;?>
                                <? endif;?>
                            </span>
                        </div>
                    </div>

                    <div class="col-lg-7 col-sm-6 col-xs-12 i-imailList__col flex flex_sb">
                        <a href="<?= InternalMail::url('my.chat', array('user' => $v['login'])) ?>" class="i-imailList__text <?= $message['author'] == User::id() ? 'my' : ''?>">
                            <? if($message['author'] == User::id()): ?>
                                <?= _t('','Вы:')?>
                            <? endif;?>
                            <?= strip_tags($message['message']) ?>
                        </a>
                        <div class="flex flex_center  mrgl10">
                            <? if($v['msgs_new']): ?>
                                <span class="i-new-message nowrap"><?= $v['msgs_new'] ?></span>
                            <? else: ?>
                                <span class="nowrap">
                                    <?= tpl::date_format3($message['created']) ?>
                                </span>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <? if($qq): ?>
                <div class="i-search-result">
                    <?= tpl::truncate( $message['message'], config::sysAdmin('internalmail.my.messages.list.message.truncate', 200, TYPE_UINT)) ?>
                </div>
            <? endif; ?>
        </li>
    <? endforeach; ?>
    </ul>
<? unset($message);
else:
    ?><div class="alert alert-info"><?= _t('internalmail', 'Список сообщений пуст') ?></div><?
endif;