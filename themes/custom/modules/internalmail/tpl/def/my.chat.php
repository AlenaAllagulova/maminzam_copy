<?php
    /**
     * Кабинет пользователя: Сообщения / Переписка
     * @var $this InternalMail
     * @var $i array - собеседник Interlocutor
     * @var $recipient array
     */
    tpl::includeJS('internalmail.my', false, 2);

    $url_back = HTML::escape($url_back);
    $lng_move_f = _t('internalmail', '');
?>
    <div class="container">

        <section class="l-mainContent">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 all-w-sm">
                    <div class="p-profileContent">
                        <div class="">
                            <div class="bg-white chat-header" id="j-my-chat-folders">
                                <div class="flex flex_center">
                                    <a href="<?= InternalMail::url('my.messages') ?>" class="chat-header__back mrgr10">
                                        <i class=" icon-arrow-point-to-left"></i>
                                        <span><?= _t('internalmail', 'Назад'); ?></span>
                                    </a>
                                    <div class="chat-header__user">
                                        <a href="<?= Users::url('profile', array('login' => $i['login'])); ?>">
                                            <img src="<?= $i['avatar'] ?>" alt="<?= tpl::avatarAlt($i); ?>" />
                                            <?= tpl::userOnline($i) ?>
                                        </a>
                                        <div class="chat-header__user-name mrgl10">
                                            <?= tpl::userLink($i, 'name, surname, no-verified, no-login') ?>
                                            <? if(!empty($i['contacts_only_phones'])):?>
                                                <? foreach ($i['contacts_only_phones'] as $phone):?>
                                                <? if($phone['t'] == Users::CONTACT_TYPE_PHONE && !empty($phone['v'])):?>
                                                        <div class="">
                                                            <?=$phone['v']?>
                                                        </div>
                                                        <? break; # clazion: согласно интерфейсу: ограничение вывода только одного номера ?>
                                                <? endif;?>
                                                <?endforeach;?>
                                            <? endif;?>
                                        </div>
                                    </div>
                                </div>
                                <? if( InternalMail::FOLDERS_ENABLED ):
                                    $title = $lng_move_f;
                                    if( ! empty($i['folders'])){
                                        $title = array(); foreach($i['folders'] as $vv) { $title[] = $folders[ $vv ]['title']; }
                                        $title = join(', ', $title);
                                    } ?>
                                <span class="dropdown">
                                    <a href="#" id="dLabel" class="dropdown-toggle chat-header__dropdown ajax-link" data-toggle="dropdown">
                                        <i class=" icon-folder"></i>
                                        <span class="j-f-title"><?= $title ?></span>
                                        <i class=" icon-arrow-point-to-down"></i>
                                    </a>
                                    <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                                        <? foreach($folders as $kk => $vv): if( ! $kk) continue; ?>
                                            <li class="<?= ! empty($vv['class']) ? $vv['class'] : '' ?><?= in_array($kk, $i['folders']) ? ' active' : '' ?>"><a href="#" data-user-id="<?= $i['user_id'] ?>" data-folder-id="<?= $kk ?>" class="j-f-action"><?= $vv['title'] ?></a></li>
                                        <? endforeach; ?>
                                    </ul>
                                </span>
                                <? endif; ?>
                            </div>
                        </div>

                        <div class="i-imailDialog-window" id="j-my-chat-list">
                            <?= $list ?>
                        </div>

                        <? # Форма отправки сообщения ?>
                        <? if( $i['blocked'] ): ?>
                            <div class="alert alert-error text-center">
                                <?= _t('internalmail', 'Аккаунт пользователя заблокирован') ?>
                            </div>
                        <? else:
                            if ( $i['ignoring'] ): ?>
                            <div class="alert alert-error text-center">
                                <?= _t('internalmail', 'Пользователь запретил отправлять ему сообщения') ?>
                            </div>
                        <? else: ?>
                            <form class="i-imailDialog-form form" method="POST" action="<?= InternalMail::url('my.messages', array('i'=>$i['login'])) ?>" id="j-my-chat-form" enctype="multipart/form-data">
                                <input type="hidden" name="act" value="send" />
                                <input type="hidden" name="i" value="<?= $i['login'] ?>" />

                                <div class="form-group flex">
                                    <img class="mrgr10 avatar-circle" src="<?= $recipient['avatar_url'] ?>">
                                    <textarea name="message" rows="4" class="form-control" placeholder="<?= _t('internalmail', 'Сообщение') ?>"></textarea>
                                </div>

                                <button class="btn btn-sm btn-primary min-w-200px mrgl60"><?= _t('internalmail', 'Отправить'); ?></button>
                                <? if(InternalMail::attachmentsEnabled()): ?>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="<?= $this->attach()->getMaxSize() ?>" />
                                    <input type="file" name="attach" class="j-upload-file hidden" />

                                    <div class="bold pull-right j-upload-file-btn" style="cursor: pointer">
                                        <?= _t('internalmail', 'Прикрепить') ?>
                                    </div>
                                    <div class="pull-right j-file-name hidden">
                                        <span class="j-name"></span> <a href="#" class="link-delete j-upload-delete"><i class="fa fa-times"></i></a>
                                    </div>
                                <div class="clearfix"></div>
                                <div class="help-block text-right mrgb0"><?= _t('internalmail', 'Максимальный размер файла - [maxSize]', array('maxSize'=>tpl::filesize($this->attach()->getMaxSize()) )) ?></div>
                                <? endif; ?>
                            </form>
                        <? endif; endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
<script type="text/javascript">
<? js::start() ?>
    $(function(){
        jMyChat.init(<?= func::php2js(array(
            'lang' => array(
                'message' => _t('internalmail','Сообщение слишком короткое'),
                'success' => _t('internalmail','Сообщение было успешно отправлено'),
                'move_folder' => $lng_move_f,
            ),
            'ajax' => true,
        )) ?>);
    });
<? js::stop() ?>
</script>