<?php
    /**
     * Кабинет пользователя: Сообщения
     * @var $this InternalMail
     */
    tpl::includeJS('internalmail.my', false, 2);
    tpl::includeJS('users.note', false, 2);
    $f['qq'] = HTML::escape($f['qq']);
?>
    <div class="container" id="j-my-messages-block">
        <h2 class="text-center"><?=_t('','Ваши сообщения')?></h2>
        <section class="l-mainContent mrgt50">
            <div class="row">
                <form action="" class="form-search" id="j-my-messages-form-list">
                    <input type="hidden" name="f" value="<?= $f['f'] ?>" id="j-my-messages-folder-value" />
                    <input type="hidden" name="page" value="<?= $f['page'] ?>" />
                    <input type="hidden" name="qq" value="<?= HTML::escape($f['qq']) ?>" />
                </form>
                <form action="" method="post" id="j-my-messages-form-act">
                    <aside class="col-md-3">
                        <div class="">
                            <div class="p-profile-collapse">
                                <div class="input-group">
                                    <input type="text" name="new_folder_name" maxlength="25" class="form-control input-sm"  value="" placeholder="<?= _t('internalmail', 'Новая папка'); ?>" />
                                    <span class="input-group-btn">
                                         <button class="btn btn-primary btn-sm btn-input j-make-folder" type="button"><?= _t('', 'Создать'); ?></button>
                                     </span>
                                </div>

                                <ul class="i-imail-folders mrgt20">
                                    <? $bCustom = false; foreach($folders as $k => $v): if( ! empty($v['custom'])) { $bCustom = true; continue; } ?>
                                        <li<?= $f['f'] == $k ? ' class="active"' : '' ?>>
                                            <a href="#" class="<?= ! empty($v['class']) ? $v['class'] : '' ?> j-folder-select" data-id="<?= $k ?>">
                                                <?= $v['title'] ?>
                                            </a>
                                        </li>
                                    <? endforeach; ?>
                                </ul>

                                <? if($bCustom): ?>
                                    <ul class="i-imail-folders">
                                        <? foreach($folders as $k => $v): if(empty($v['custom'])) continue; ?>
                                            <li class="flex flex_center flex_sb <?= $f['f'] == $k ? 'active' : '' ?>">
                                                <a href="#" class="<?= ! empty($v['class']) ? $v['class'] : '' ?> j-folder-select" data-id="<?= $k ?>">
                                                    <span class="j-title"><?= HTML::escape($v['title']) ?></span>
                                                </a>
                                                <span class="flex flex_center">
                                                    <a href="#" class="j-edit-folder" data-id="<?= $k ?>">
                                                        <i style="font-size: 15px;" class="color-accent fa fa-edit show-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?= _t('form', 'Редактировать'); ?>" data-original-title="<?= _t('form', 'Редактировать'); ?>"></i>
                                                    </a>
                                                    <a href="#" class="link-delete j-delete-folder" data-id="<?= $k ?>">
                                                        <i style="font-size: 11px;" class="color-error icon-cancel-music show-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?= _t('form', 'Удалить'); ?>" data-original-title="<?= _t('form', 'Удалить'); ?>"></i>
                                                    </a>
                                                </span>

                                            </li>
                                        <? endforeach; ?>
                                    </ul>
                                <? endif; ?>
                            </div>
                        </div>
                    </aside>
                </form>

                <div class="col-md-9 l-content-column">
                    <div class="p-profileContent">
                        <div class="row">
                            <div class="col-sm-6">
                            </div>
                            <form action="" method="get">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <input name="qq" value="<?= $f['qq'] ?>" type="search" class="form-control input-sm j-q" placeholder="<?= _t('internalmail', 'Поиск сообщений'); ?>">
                                        <span class="input-group-btn">
                                        <button class="btn btn-primary btn-sm btn-input j-q-submit" type="submit"><i class=" icon-search "></i></button>
                                      </span>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="j-list mrgt20"><?= $list ?></div>
                        <div class="j-pgn"><?= $pgn ?></div>

                    </div>

                </div>
            </div>

        </section>
    </div>
<script type="text/javascript">
<? js::start() ?>
    $(function(){
        jMyMessages.init(<?= func::php2js(array(
            'lang' => array(
                'move_folder' => _t('internalmail', ''),
                'set_new_folder_name' => _t('internalmail', 'Укажите название папки'),
                'ok' => _t('internalmail', 'ok'),
                'cancel' => _t('form', 'Отмена'),
            ),
            'folders' => $folders,
            'conv_url' => InternalMail::url('my.messages', array('i'=>'')),
            'ajax' => true,
        )) ?>);
    });
<? js::stop() ?>
</script>