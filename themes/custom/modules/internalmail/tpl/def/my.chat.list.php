<?php

/**
 * Кабинет пользователя: Сообщения / Переписка - список
 * @var $this InternalMail
 */

$attach = $this->attach();
if( ! isset($date_last)) $date_last = 0;


foreach($messages as $v):
    ?>
    <? if( $date_last !== $v['created_date']): ?><div class="i-message-date"><?= tpl::datePublicated($v['created_date'], 'Y-m-d', false, ' ') ?></div><? endif; ?>
    <? $date_last = $v['created_date']; ?>
    <div class="i-message-box">
        <div class="<?= $v['my'] ? 'i-message-self' : 'i-message-others'?>">
            <? if(!$v['my']):?>
            <a class="i-message-box__link" href="<?= Users::url('profile', array('login' => $i['login']))?>">
                <img class="i-message-box__avatar" src="<?=$i['avatar']?>">
            </a>
            <?endif;?>
            <div class="i-message">
                <?= $v['message'] ?>
            <? if( InternalMail::attachmentsEnabled() && ! empty($v['attach']) ): ?>
                <div class="i-message-addition">
                    <?= $attach->getAttachLink($v['attach']); ?>
                </div>
            <? endif; ?>
            </div>
            <div class="i-message-time"><?= date('H:i', strtotime($v['created'])) ?></div>
        </div>
    </div>
<? endforeach; unset($item);

if( empty($messages) ): ?>
    <div class="alert alert-info"><?= _t('internalmail', 'Список сообщений пуст') ?></div>
<? endif;


