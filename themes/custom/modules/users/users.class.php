<?php
class Theme_Custom_UsersClass extends Theme_Custom_UsersClass_Base
{

    public static function isOpenedContacts($nId)
    {
        return self::model()->isOpenedContacts($nId);

    }

    /**
     * Обнуляем количество открытий контактов для пользователей с аккаунтом Pro
     * Рекомендуемый интервал: раз в сутки в 00:00:00
     */
    public function cronUserProOpenedContacts()
    {
        if (!bff::cron()) {
            return;
        }
        $this->db->update(TABLE_USERS, ['pro_opening_cnt' => 0]);
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {

        return array(
            'cron' => array('period' => '10 0 * * *'),
            'cronSpecsPositions' => array('period' => '0 * * * *'),
            'cronUserProOpenedContacts' => array('period' => '0 0 * * *'),
        );
    }

    public function buy_contacts()
    {
        $aData = [];
        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => _t('users', 'Платные сервисы'), 'link' => Svc::url('list')),
            array('title' => _t('users', 'Покупка контактов'), 'active' => true),
        );

        $aData['nUserID'] = User::id();
        $aData['bClient'] = (Users::useClient() && User::isClient());
        $aData['nContactAmount'] = ($aData['bClient']) ?
                                    config::get('users_client_contact_amount') :
                                    config::get('users_worker_contact_amount');
        $aUserData = Users::model()->userData($aData['nUserID'], ['payed_opening_cnt']);
        $aData['nPayedOpeningCnt'] =  $aUserData['payed_opening_cnt'];
        return $this->viewPHP($aData, 'buy.contacts');
    }

    public function buyUserContacts()
    {
        $aResponse = [];
        $nUserID = User::id();

        if (!$nUserID || !Request::isAJAX()) {
            $this->errors->accessDenied();
            $this->ajaxResponseForm($aResponse);
        }

        $nContactsCnt = $this->input->getpost('cnt', TYPE_UINT);

        if (!$nContactsCnt) {
            $this->errors->set(_t('', 'Укажите количество анкет'));
            $this->ajaxResponseForm($aResponse);
        }

        # проверка баланса
        $aBalLink = Bills::url('my.history');
        $nBalance = User::balance();
        $sTitleCurrDefault = Site::currencyDefault();
        $bClient = (Users::useClient() && User::isClient());
        $nContactAmount = ($bClient) ? config::get('users_client_contact_amount') : config::get('users_worker_contact_amount');
        $nTotalSum = $nContactsCnt * $nContactAmount;

        if ($nTotalSum > $nBalance) {
            $this->errors->set(_t('specs_comission', "Недостаточно средств для оплаты открытия анкет.
                                                        <a href='{$aBalLink}'>Пополните счет</a> ,
                                                        Ваш баланс : {$nBalance} {$sTitleCurrDefault}"));
            $this->ajaxResponseForm($aResponse);
            return false;
        }

        $sBillDescr = _t('specs_commission', 'Оплата открытия контактов - [cnt]',
            ['cnt' => tpl::declension($nContactsCnt, 'анкета;анкеты;анкет')]);
        # 1) создаем закрытый счет снятия комиссии по заказу
        $nBillID = Bills::i()->createBill_OutService(0, 0, $nUserID, ($nBalance - $nTotalSum),
            $nTotalSum, $nTotalSum, Bills::STATUS_COMPLETED, $sBillDescr, []);

        # 2) снимаем деньги со счета пользователя
        $bSuccess = $this->bills()->updateUserBalance($nUserID, $nTotalSum, false);

        if (!$nBillID || !$bSuccess) {
            $this->errors->set(_t('specs_comission', "Ошибка оплаты открытия анкет "));
            $this->ajaxResponseForm($aResponse);
        }

        if($bSuccess && $this->errors->no()){
            $this->model->userSave($nUserID, array("payed_opening_cnt = payed_opening_cnt + $nContactsCnt"));
        }

        $aResponse['success'] = $this->errors->no();
        if ($aResponse['success']) {
            $aResponse['success_msg'] = _t('', 'Оплата прошла успешно');
            $aResponse['redirect'] = false;
        }

        $this->ajaxResponseForm($aResponse);

    }
    
    /**
     * Подготовка данных телефона
     * @param $sPhone
     * @return array
     */
    public function prepareTelephoneContacts($sPhone)
    {
        $aContacts = [
            'contacts' => [
                [
                    't' => static::CONTACT_TYPE_PHONE,
                    'v' => $sPhone,
                ]
            ]
        ];
        $this->cleanUserData($aContacts);
        
        return $aContacts;
    }
    
    /**
     * Валидация телефона
     * @param $sContactPhone
     * @return bool
     */
    public function isValidTelephone($sContactPhone)
    {
        if (empty($sContactPhone)) {
            return false;
        }
        
        if(!preg_match('/^[0-9\-\+\s\(\)]*$/', $sContactPhone)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Регистрация - добавлена обработка контактного телефона (без смс уведомления) #mark-1
     * добавлена обработка имени и фамилии пользователя
     * @var bool|array $embedded параметры для встраиваемой формы:
     *   - 'force_type' - предопределить тип пользователя
     *   - 'force_role' - предопределить роль пользователя
     *   - 'generate_password' - сгенерировать пользователю пароль
     *   - 'no_captcha' - не использовать каптчу
     *   - 'form' - JQuery form selector
     *   - 'step' - переопределить шаг
     * @return string HTML
     */
    public function register($embedded = false)
    {
        $rolesEnabled = Users::rolesEnabled();
        $bPhone = static::registerPhone(); # задействовать: номер телефона
        $generatePassword = ! empty($embedded['generate_password']);
        
        $step = $this->input->getpost('step', TYPE_STR);
        if ( ! empty($embedded['step']) && $embedded['step'] == 'confirm') {
            $step = $bPhone ? 'phone':'emailed';
        }

        $nUserType = $this->input->getpost('user_type', TYPE_NUM);
        
        switch ($step) {
            case 'phone': # Подтверждение номера телефона
                {
                    if (User::id()) {
                        if (Request::isPOST()) {
                            $this->errors->reloadPage();
                            $this->ajaxResponseForm();
                        }
                        $this->redirectToProfile();
                    }
                    $registerData = $this->security->getSESSION('users-register-data');
                    if (empty($registerData['id']) || empty($registerData['phone'])) {
                        $this->errors->error404();
                    }
                    $userID = $registerData['id'];
                    $userData = $this->model->userData($userID, array('password','activated','activate_key','blocked','blocked_reason'));
                    if (empty($userData) || $userData['blocked']) {
                        $this->errors->error404();
                    }
                    if ($userData['activated']) {
                        $this->userAuth($userID, 'user_id', $userData['password']);
                        $this->redirectToProfile();
                    }
                    if (Request::isPOST()) {
                        $this->register_phone($userID, static::url('register', array('step' => 'finished')));
                    }
                    
                    if ( ! empty($embedded)) {
                        $registerData['embedded'] = $embedded;
                        return $this->viewPHP($registerData, 'auth.register.phone');
                    }
                    return $this->authPage('auth.register.phone', _t('users', 'Подтверждение номера мобильного телефона'), $registerData);
                } break;
            case 'emailed': # Уведомление о письме "активации"
                {
                    $aData = $this->security->getSESSION('users-register-data');
                    if (!empty($aData['id'])) {
                        $aUser = $this->model->userData($aData['id'], array('activated', 'blocked'));
                        if (empty($aUser) || $aUser['activated'] || $aUser['blocked']) {
                            $aData = false;
                        }
                    } else {
                        $userID = $this->security->getSESSION('users-social-auth-notactivated');
                        if ($userID) {
                            $aUser = $this->model->userData($userID, array('email', 'activated', 'activate_key', 'activate_expire'));
                            if ( ! empty($aUser['email']) && ! $aUser['activated']) {
                                if ( ! $aUser['activate_key'] || (strtotime($aUser['activate_expire']) < time())) {
                                    $activationData = $this->getActivationInfo();
                                    $this->model->userSave($userID, array(
                                        'activate_key'    => $activationData['key'],
                                        'activate_expire' => $activationData['expire'],
                                    ));
                                    $aData = array(
                                        'id'            => $userID,
                                        'name'          => '',
                                        'password'      => '',
                                        'email'         => $aUser['email'],
                                        'activate_link' => $activationData['link'],
                                    );
                                    bff::sendMailTemplate($aData, 'users_register', $aData['email']);
                                } else {
                                    $aData = array(
                                        'id'            => $userID,
                                        'name'          => '',
                                        'password'      => '',
                                        'email'         => $aUser['email'],
                                        'activate_link' => static::url('activate', array('key' => $aUser['activate_key'])),
                                    );
                                }
                            }
                            $this->security->setSESSION('users-register-data', $aData);
                        }
                    }
                    if (Request::isPOST()) {
                        if (!$this->security->validateReferer()) {
                            $this->errors->reloadPage();
                        } else {
                            if (!User::id() && !empty($aData)) {
                                # Повторная отправка письма об успешной регистрации
                                bff::sendMailTemplate($aData, 'users_register', $aData['email']);
                                $this->security->setSESSION('users-register-data', null);
                            }
                        }
                        $this->ajaxResponseForm();
                    }
                    
                    $bResend = $this->input->get('resend', TYPE_BOOL);
                    $sTitle = ( $bResend ? _t('users', 'Письмо отправлено') : _t('users', 'Регистрация завершена') );
                    
                    $aData = array('retry_allowed' => !empty($aData));
                    if ( ! empty($embedded)) {
                        $aData['embedded'] = $embedded;
                        return $this->viewPHP($aData, 'auth.register.emailed');
                    }
                    
                    return $this->authPage('auth.register.emailed', $sTitle, $aData);
                }
                break;
            case 'social': # Регистрация через аккаунт в соц. сети
                {
                    if (User::id()) {
                        if (Request::isPOST()) {
                            $this->errors->reloadPage();
                            $this->ajaxResponseForm();
                        }
                        $this->redirectToProfile();
                    }
                    
                    $aSocialData = $this->social()->authData();
                    
                    if (Request::isPOST()) {
                        $aResponse = array('exists' => false);
                        $p = $this->input->postm(array(
                                'email'     => array(TYPE_NOTAGS, 'len' => 100, 'len.sys' => 'users.user.email.limit'), # E-mail
                                'agreement' => TYPE_BOOL, # Пользовательское соглашение
                                'type'      => TYPE_UINT, # Тип пользователя
                                'phone'     => TYPE_NOTAGS, # Телефон
                                'code'      => TYPE_STR,    # Код подтверждения
                            )
                        );
                        extract($p);
                        do {
                            if (!$this->security->validateReferer() || empty($aSocialData)) {
                                $this->errors->reloadPage();
                                break;
                            }
                            if (!array_key_exists($type, static::aTypes())) {
                                $this->errors->set(_t('users', 'Тип пользователя указан некорректно'), 'type');
                                break;
                            }
                            if (!$this->input->isEmail($email)) {
                                $this->errors->set(_t('users', 'E-mail адрес указан некорректно'), 'email');
                                break;
                            }
                            if ($bPhone) {
                                if ( ! $this->input->isPhoneNumber($phone)) {
                                    $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                                    break;
                                }
                                if ($this->model->userPhoneExists($phone)) {
                                    $this->errors->set(_t('users', 'Пользователь с таким номером телефона уже зарегистрирован.',
                                        array('link_forgot' => 'href="' . static::url('forgot') . '"')
                                    ), 'phone'
                                    );
                                    break;
                                }
                                $saveCode = $this->security->getSESSION('user-phone-confirm');
                                if (empty($code) || empty($saveCode)) {
                                    $activationData = $this->getActivationInfo();
                                    $res = $this->sms()->sendActivationCode($phone, $activationData['key']);
                                    if ($res) {
                                        $this->security->setSESSION('user-phone-confirm', $activationData['key']);
                                        $aResponse['msg'] = _t('users', 'Код подтверждения был отправлен на указанный вами номер');
                                        $aResponse['confirm'] = 1;
                                    } else {
                                        $this->errors->reloadPage();
                                    }
                                    break;
                                }
                                if (mb_strtolower($code) != mb_strtolower($saveCode)) {
                                    $this->errors->set(_t('users', 'Код подтверждения указан некорректно'), 'code');
                                    $aResponse['confirm'] = 1;
                                    break;
                                }
                            }
                            
                            
                            # антиспам фильтр: временные ящики
                            if (Site::i()->spamEmailTemporary($email)) {
                                $this->errors->set(_t('', 'Указанный вами email адрес находится в списке запрещенных, используйте например @gmail.com'));
                                break;
                            }
                            if ($mBanned = $this->checkBan(true, $email)) {
                                $this->errors->set(_t('users', 'Доступ заблокирован по причине: [reason]', array('reason' => $mBanned)));
                                break;
                            }
                            if ($this->model->userEmailExists($email)) {
                                $aResponse['exists'] = true;
                                break;
                            }
                            if (!$agreement) {
                                $this->errors->set(_t('users', 'Пожалуйста подтвердите, что Вы согласны с пользовательским соглашением'), 'agreement');
                            }
                            if (!$this->errors->no()) {
                                break;
                            }
                            
                            
                            # Создаем аккаунт пользователя
                            $aUserData = $this->userRegister(array(
                                'name'  => $aSocialData['name'], # ФИО из соц. сети
                                'email' => $email, # E-mail
                                'type'  => $type, # Тип пользователя
                            ));
                            if (empty($aUserData)) {
                                $this->errors->set(_t('users', 'Ошибка регистрации, обратитесь к администратору'));
                                break;
                            }
                            $nUserID = $aUserData['user_id'];
                            
                            # Загружаем аватар из соц. сети
                            if (!empty($aSocialData['avatar'])) {
                                $this->avatar($nUserID)->uploadSocial($aSocialData['provider_id'], $aSocialData['avatar'], true);
                            }
                            
                            # Закрепляем соц. аккаунт за пользователем
                            $this->social()->authFinish($nUserID);
                            
                            # Активируем аккаунт пользователя без подтверждения email адреса
                            if (!config::sysAdmin('users.register.social.email.activation', true, TYPE_BOOL)) {
                                $update = array(
                                    'activated' => 1
                                );
                                if ($bPhone) {
                                    $update['phone_number'] = $phone;
                                    $update['phone_number_verified'] = 1;
                                }
                                
                                $res = $this->model->userSave($nUserID, $update);
                                if (!$res) {
                                    $this->errors->reloadPage(); break;
                                }
                                
                                $res = $this->userAuth($nUserID, 'user_id', $aUserData['password'], false);
                                if ($res!==true) {
                                    $this->errors->reloadPage(); break;
                                }
                                
                                # Отправляем письмо об успешной регистрации
                                $aMailData = array(
                                    'name'     => $aSocialData['name'],
                                    'password' => $aUserData['password'],
                                    'email'    => $email,
                                );
                                bff::sendMailTemplate($aMailData, 'users_register_auto', $email);
                                InternalMail::i()->sendRegInfo($nUserID);
                                
                                $aResponse['success'] = true;
                                $aResponse['redirect'] = static::url('register', array('step' => 'finished'));
                                break;
                            }
                            
                            
                            # Отправляем письмо для активации аккаунта
                            $aMailData = array('id'            => $nUserID,
                                'name'          => $aSocialData['name'],
                                'password'      => $aUserData['password'],
                                'email'         => $email,
                                'activate_link' => $aUserData['activate_link']
                            );
                            bff::sendMailTemplate($aMailData, 'users_register', $email);
                            InternalMail::i()->sendRegInfo($nUserID);
                            
                            # Сохраняем данные для повторной отправки письма
                            $this->security->sessionStart();
                            $this->security->setSESSION('users-register-data', $aMailData);
                            
                            $aResponse['success'] = true;
                            $aResponse['redirect'] = static::url('register', array('step' => 'emailed')); # url результирующей страницы
                            
                        } while (false);
                        
                        $this->ajaxResponseForm($aResponse);
                    }
                    
                    # Данные о процессе регистрации через соц.сеть некорректны, причины:
                    # 1) неудалось сохранить в сессии
                    # 2) повторная попытка, вслед за успешной (случайный переход по ссылке)
                    if (empty($aSocialData)) {
                        $this->redirect(static::url('register'));
                    }
                    
                    # Аватар по-умолчанию
                    if (empty($aSocialData['avatar'])) {
                        $aSocialData['avatar'] = UsersAvatar::url(0, '', UsersAvatar::szNormal);
                    }
                    
                    return $this->authPage('auth.register.social', _t('users', 'Для завершения регистрации введите Вашу электронную почту'), $aSocialData);
                }
                break;
            case 'finished':
                {
                    return $this->authPage('auth.message', _t('users', 'Вы успешно зарегистрировались!'), array(
                            'message' => _t('users', 'Теперь вы можете <a [link_home]><span>перейти на главную страницу</span></a> или <a [link_profile]><span>в настройки своего профиля</span></a>.',
                                array('link_home'    => 'href="' . bff::urlBase() . '"',
                                    'link_profile' => 'href="' . self::url('my.settings') . '"'
                                )
                            )
                        )
                    );
                }
                break;
        }
        
        $bCaptcha = config::sysAdmin('users.register.captcha', true, TYPE_BOOL); # задействовать: капчу
        $bPasswordConfirm = config::sysAdmin('users.register.passconfirm', true, TYPE_BOOL); # задействовать: подтверждение пароля
        $bAgreement = config::sysAdmin('users.register.agreement', true, TYPE_BOOL); # задействовать: подтверждение соглашения
        
        if (Request::isPOST()) {
            $aResponse = array('captcha' => false);
            
            if (User::id()) {
                $this->ajaxResponseForm($aResponse);
            }
            
            $params = array(
                'email'     => array(TYPE_NOTAGS, 'len' => 100, 'len.sys' => 'users.user.email.limit'), # E-mail
                'pass'      => TYPE_NOTRIM, # Пароль
                'pass2'     => TYPE_NOTRIM, # Подтверждение пароля
                'back'      => TYPE_NOTAGS, # Ссылка возврата
                'captcha'   => TYPE_NOTAGS, # Капча
                'agreement' => TYPE_BOOL, # Пользовательское соглашение
                'type'      => TYPE_UINT, # Тип пользователя
                'phone'     => TYPE_NOTAGS, # Телефон
                'contact_phone' =>  TYPE_NOTAGS, # Телефон для связи
                'name'      => TYPE_NOTAGS, # имя
                'surname'   => TYPE_NOTAGS, # фамилия
        );

            if ($rolesEnabled) {
                $params['role_id'] = TYPE_UINT; # роль
            }
            $aData = $this->input->postm($params);
            extract($aData);
            $aResponse['back'] = $back;
            $sContactPhone = !empty($contact_phone) ? $contact_phone : null;
            
            do {
                if (!$this->security->validateReferer()) {
                    $this->errors->reloadPage();
                    break;
                }
                
                if ($bPhone && ! $this->input->isPhoneNumber($phone)) {
                    $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                    break;
                }
                
                if (isset($embedded['force_type'])) {
                    $type = $embedded['force_type'];
                }
                if (!array_key_exists($type, static::aTypes())) {
                    $this->errors->set(_t('users', 'Тип пользователя указан некорректно'), 'type');
                    break;
                }
                
                if ($rolesEnabled) {
                    if (isset($embedded['force_role'])) {
                        $role_id = $embedded['force_role'];
                    }
                    if (!array_key_exists($role_id, static::roles())) {
                        $this->errors->set(_t('users', 'Роль указана некорректно'), 'role_id');
                        break;
                    }
                    if (empty($name)) {
                        if ($role_id == Users::ROLE_COMPANY) {
                            $this->errors->set(_t('users', 'Укажите название компании'), 'role_id');
                        } else {
                            $this->errors->set(_t('users', 'Укажите имя'), 'role_id');
                        }
                        break;
                    }
                }
                
                if ($bPhone && $this->model->userPhoneExists($phone)) {
                    $this->errors->set(_t('users', 'Пользователь с таким номером телефона уже зарегистрирован. <a [link_forgot]>Забыли пароль?</a>',
                        array('link_forgot' => 'href="' . static::url('forgot') . '"')
                    ), 'phone'
                    );
                    break;
                }
                
                if (!$this->input->isEmail($email)) {
                    $this->errors->set(_t('users', 'E-mail адрес указан некорректно'), 'email');
                    break;
                }
                # антиспам фильтр: временные ящики
                if (Site::i()->spamEmailTemporary($email)) {
                    $this->errors->set(_t('', 'Указанный вами email адрес находится в списке запрещенных, используйте например @gmail.com'));
                    break;
                }
                
                if ($mBanned = $this->checkBan(true, $email)) {
                    $this->errors->set(_t('users', 'Доступ заблокирован по причине: [reason]', array('reason' => $mBanned)));
                    break;
                }
                
                if ($this->model->userEmailExists($email)) {
                    $this->errors->set(_t('users', 'Пользователь с таким e-mail адресом уже зарегистрирован. <a [link_forgot]>Забыли пароль?</a>',
                        array('link_forgot' => 'href="' . static::url('forgot') . '"')
                    ), 'email'
                    );
                    break;
                }
                
                if ( ! $generatePassword) {
                    if (empty($pass)) {
                        $this->errors->set(_t('users', 'Укажите пароль'), 'pass');
                    } elseif (mb_strlen($pass) < $this->passwordMinLength) {
                        $this->errors->set(_t('users', 'Пароль не должен быть короче [min] символов', array('min' => $this->passwordMinLength)), 'pass');
                    } else {
                        if($bPasswordConfirm && $pass != $pass2) {
                            $this->errors->set(_t('users', 'Подтверждение пароля указано неверно'), 'pass2');
                        }
                    }
                }
                
                if ($bCaptcha && empty($embedded['no_captcha'])) {
                    if (empty($captcha) || !CCaptchaProtection::isCorrect($captcha)) {
                        $this->errors->set(_t('users', 'Результат с картинки указан некорректно'), 'captcha');
                        $aResponse['captcha'] = true;
                        CCaptchaProtection::reset();
                    }
                }
                
                if ($bAgreement && ! $agreement) {
                    $this->errors->set(_t('users', 'Пожалуйста подтвердите, что Вы согласны с пользовательским соглашением'), 'agreement');
                }
                
                $aUserCreate = array(
                    'email'    => $email,
                    'type'     => $type,
                    'name'     => $name,
                    'surname'  => $surname,
                );
                if ( ! $generatePassword) {
                    $aUserCreate['password'] = $pass;
                }
                if ($rolesEnabled) {
                    $aUserCreate['role_id'] = $role_id;
                }
                if ($bPhone) {
                    $aUserCreate['phone_number'] = $phone;
                }
    
                #mark-1
                # Обязательный контактный номер телефона (без подтверждениея)
                if (!empty($sContactPhone)) {
                    if (!$this->isValidTelephone($sContactPhone)) {
                        $this->errors->set(_t('users', 'Контактный телефон указан некорректно'));
                        break;
                    }
                    
                    $aUserCreateContactTelephone = func::unserialize($this->prepareTelephoneContacts($sContactPhone));
                    $aUserCreate = array_merge($aUserCreate, $aUserCreateContactTelephone);
                }
                
                # Обязательный номер телефона (с подтверждением)
                if (static::registerPhone()) {
                    $aContacts = $this->prepareTelephoneContacts($phone);
                    $aPhones = func::unserialize($aContacts['contacts']);
                    if (empty($aPhones)) {
                        $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                        break;
                    }
                    $aUserCreate['contacts'] = $aPhones;
                }
                
                if (!$this->errors->no('users.register.submit',array('data'=>&$aUserCreate))) {
                    break;
                }
                
                # Создаем аккаунт пользователя
                $aUserData = $this->userRegister($aUserCreate);
                if (empty($aUserData)) {
                    $this->errors->set(_t('users', 'Ошибка регистрации, обратитесь к администратору'));
                    break;
                }
                $aResponse['success'] = true;
                $aResponse['redirect'] = static::url('register', array('step' => ($bPhone?'phone':'emailed'))); # url результирующей страницы
                
                # Отправляем письмо для активации аккаунта
                $aMailData = array('id'            => $aUserData['user_id'],
                    'name'          => $name,
                    'password'      => $generatePassword ? $aUserData['password'] : $pass,
                    'phone'         => $phone,
                    'email'         => $email,
                    'activate_link' => $aUserData['activate_link']
                );
                if ($bPhone) {
                    # Отправляем SMS с кодом активации для подтверждения номера телефона
                    # Письмо отправим после успешного подтверждения
                    $this->sms(false)->sendActivationCode($phone, $aUserData['activate_key']);
                } else {
                    bff::sendMailTemplate($aMailData, 'users_register', $email);
                }
                
                # Сохраняем данные для повторной отправки письма
                $this->security->sessionStart();
                $this->security->setSESSION('users-register-data', $aMailData);
                
            } while (false);
            
            if ( ! empty($embedded)) {
                if ( ! empty($aResponse['success'])) {
                    return $aUserData;
                } else {
                    return false;
                }
            }
            
            $this->ajaxResponseForm($aResponse);
        } else {
            if (User::id()) {
                $this->redirectToProfile();
            }
        }
        
        if ( ! $embedded) {
            # SEO: Регистрация
            $this->urlCorrection(static::url('register'));
            $this->seo()->canonicalUrl(static::url('register', array(), true));
            $this->setMeta('register');
        }
        
        $data = array(
            'captcha_on'      => $bCaptcha,
            'agreement_on'    => $bAgreement,
            'pass_confirm_on' => $bPasswordConfirm,
            'phone_on'        => $bPhone,
            'providers'       => $this->social()->getProvidersEnabled(),
            'user_type'       => $nUserType,
        );
        if ( ! empty($embedded)) {
            $data['embedded'] = $embedded;
            return $this->viewPHP($data, 'auth.register');
        }
        
        return $this->authPage('auth.register', _t('users', 'Зарегистрируйтесь на сайте с помощью электронной почты или через социальную сеть'), $data);
    }
}
