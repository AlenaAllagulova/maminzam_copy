<?php

class Theme_Custom_UsersModel extends Theme_Custom_UsersModel_Base
{
    public function isOpenedContacts($nId)
    {
        $nUserID = User::id();
        if (!$nUserID || !$nId) return false;

        $bResult = $this->db->exec('SELECT EXISTS 
                                          ( SELECT * 
                                                FROM ' . TABLE_USERS_OPENED_CONTACTS . ' 
                                                WHERE user_id = :user_id AND opened_user_id = :id 
                                           ) SUB',
            [':user_id' => $nUserID, ':id' => $nId]);

        if (is_array($bResult) && !empty($bResult)) {
            $bResult = reset($bResult);
        }
        return (boolean)$bResult['SUB'];

    }

    public function saveUserOpenedContact($nCurrentUserId, $nOpenUserId, $bPluginPackagesEnabled = false, $nPackageID = 0)
    {
        if (!$nCurrentUserId || !$nOpenUserId){
            return false;
        }
        $aFields = ['user_id', 'opened_user_id', 'date_opened'];
        $aBindFields = [':cur_user', ':open_user', ':date_opened'];
        $aBind = [
            ':cur_user' => $nCurrentUserId,
            ':open_user' => $nOpenUserId,
            ':date_opened' => $this->db->now(),
        ];

        if ($bPluginPackagesEnabled && $nPackageID){
            $aFields = array_merge($aFields, ['id_pack']);
            $aBindFields = array_merge($aBindFields, [':id_pack']);
            $aBind = array_merge($aBind, [':id_pack'=> $nPackageID]);
        }
        foreach ($aFields as $k=>$v){
            $aUpdateMatch[] = $aFields[$k].' = '.$aBindFields[$k];
        }


        return $this->db->exec("INSERT INTO " . TABLE_USERS_OPENED_CONTACTS . " (" . join(', ', $aFields) . ")
                        VALUES (" . join(', ', $aBindFields) . ")
                        ON DUPLICATE KEY UPDATE " . join(', ', $aUpdateMatch),
            $aBind
        );
    }
}
