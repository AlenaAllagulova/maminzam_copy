<?php

class Theme_Custom_UsersBase extends Theme_Custom_UsersBase_Base
{
    public function openContacts()
    {
        $nCurrentUserId = User::id();

        if (!$nCurrentUserId) {
            return false;
        }

        if (!\Request::isAJAX()) {
            return false;
        }

        $nOpenUserId = $this->input->postget('user_id', TYPE_UINT);

        if (!$nOpenUserId) {
            $this->errors->reloadPage();
        }

        $aCurrentUserData = $this->model->userData($nCurrentUserId, 'type, payed_opening_cnt');
        $isWorkerCurrentUser = ($aCurrentUserData['type'] == Users::TYPE_WORKER);
        $isPro = User::isPro();
        $aResponse['nPayedOpeningCnt'] = $aCurrentUserData['payed_opening_cnt'];
        $aOpenUserData = $this->model->userData($nOpenUserId, 'type, pro, login');

        # проверка на открытие контакта противоположного типа пользователя
        if ($aCurrentUserData['type'] == $aOpenUserData['type']) {
            $this->errors->set(_t('contacts', ' Доступно открытие контактов только для профилей [type]',
                ['type' => ($isWorkerCurrentUser) ? 'заказчиков' : 'исполнителей']));
        }

        # проверка включения плагина ContactsPackages
        $bPluginPackagesEnabled = false;
        if (class_exists('PluginLoaderContactsPackages')){
            $bPluginPackagesEnabled = PluginLoaderContactsPackages::getPluginInstance()->isEnabled();

        }

        if ($isWorkerCurrentUser) {

            if ($bPluginPackagesEnabled) {
                # проверка наличия актуального купленного пакета подписки на N дней на открытие контактов
                $aUserPackageUnlim = Packages::getUserActualPackage($nCurrentUserId, true);
                if (empty($aUserPackageUnlim) ) {

                    # проверка наличия для покупки пакета подписки на N дней на открытие контактов
                    $bAllowedPackageUnlim = Packages::isEnabledPackagesUnlim();
                    if(!$bAllowedPackageUnlim){
                        $bPluginPackagesEnabled = false;
                    }
                    $this->useProOrPayedOpening($isPro, $nCurrentUserId, $aResponse['nPayedOpeningCnt'], $bPluginPackagesEnabled, $bAllowedPackageUnlim);
                } else {
                    Packages::updateUserActualPackage($nCurrentUserId, $aUserPackageUnlim['id'], true);
                }

            } else {

                $this->useProOrPayedOpening($isPro, $nCurrentUserId, $aResponse['nPayedOpeningCnt'], $bPluginPackagesEnabled);
            }

        } else { # открытие контактов заказчиком

            if ($bPluginPackagesEnabled) {
                # проверка наличия актуального купленного пакета подписки на N дней на открытие контактов
                $aUserPackageUnlim = Packages::getUserActualPackage($nCurrentUserId, true);
                if (empty($aUserPackageUnlim) ) {
                    # проверка наличия актуального купленного пакета с номерами для открытия
                    $aUserPackage = Packages::getUserActualPackage($nCurrentUserId);
                    if (empty($aUserPackage)) {
                        # проверка наличия для покупки пакета подписки на N дней на открытие контактов
                        $bAllowedPackageUnlim = Packages::isEnabledPackagesUnlim();
                        $bAllowedPackageContacts = Packages::isEnabledPackagesContacts();
                        if(!$bAllowedPackageUnlim && !$bAllowedPackageContacts){
                            $bPluginPackagesEnabled = false;
                        }

                        $this->usePaidOpening($aResponse['nPayedOpeningCnt'], $isPro, $nCurrentUserId, $bPluginPackagesEnabled);
                    } else {
                        # списание открытого контакта с пакета  + запись открытого контакта в базу
                        $bResult = Packages::updateUserActualPackage($nCurrentUserId, $aUserPackage['id']);
                    }

                }else{
                    Packages::updateUserActualPackage($nCurrentUserId, $aUserPackageUnlim['id'], true);
                }

            } else {

                $this->usePaidOpening($aResponse['nPayedOpeningCnt'],
                                      $isPro,
                                      $nCurrentUserId,
                                      $bPluginPackagesEnabled);
            }


        }

        if (!$this->errors->no()) {
            $aResponse['errors'] = $this->errors->get();
            $this->ajaxResponseForm($aResponse);
        }

        # больший приоритет пакета подписки на N дней на открытие контактов
        $nPackageId = isset($aUserPackageUnlim['id'])?
                      $aUserPackageUnlim['id']:
                      (isset($aUserPackage['id']) ? $aUserPackage['id'] : 0) ;
        $bRes = $this->model->saveUserOpenedContact($nCurrentUserId, $nOpenUserId, $bPluginPackagesEnabled, $nPackageId);

        if($bRes && $this->errors->no()){

            $aResponse = ['success' => true];
            $aOpenUserData['open_user_id'] = $nOpenUserId;
            $aOpenUserData['contacts'] = Users::i()->getUserContacts($nOpenUserId);
            $aOpenUserData['from_profile'] = $this->input->postget('is_profile', TYPE_BOOL);
            $aOpenUserData['open_user_type'] = $aOpenUserData['type'];
            $aResponse['contacts_block'] = $this->viewPHP($aOpenUserData, 'contacts.block');
        }else{
            $aResponse = ['success' => false];

        }
        $aResponse['errors'] = $this->errors->get();

        $this->ajaxResponseForm($aResponse);
    }

    public function usePaidOpening($nPayedOpeningCnt, $bUserPro, $nCurrentUserId,
                                         $bPluginPackagesEnabled = false, $bPackageUnlim = false)
    {
        if (!$nPayedOpeningCnt) {
            $aLinks = ['link' => Users::url('buy_contacts')];
            $sMsg = 'Количество открытий контактов ограничено. ';
            $sLiks = '<a href="[link]"> Купить </a>разовое открытие контактов. ';

            if(User::isWorker()){
                $aLinks = array_merge($aLinks, ['cnt' => config::get('users_pro_contacts_cnt'),]);
                $sMsg.= ' Не более <b> [cnt] контактов </b> в день для Pro аккаунта.';

            }
            if (!$bUserPro && User::isWorker()) {
                $aLinks = array_merge($aLinks, ['pro_link' => Svc::url('view', array('keyword' => 'pro'))]);
                $sLiks .= '<a href="[pro_link]">Купить Pro</a>';
            }

            if ($bPluginPackagesEnabled){

                if ($bPackageUnlim){
                    $aLinks = array_merge($aLinks, ['link_packages' => Packages::url('packages_buy')]) ;
                    $sLiks .= ' <a href="[link_packages]"> Купить </a>пакет подписок для открытия контактов на N дней';
                }else {
                    $aLinks = array_merge($aLinks, ['link_packages' => Packages::url('packages_buy')]);
                    $sLiks .= ' <a href="[link_packages]"> Купить </a>пакет для открытия контактов';
                }
            }

            $this->errors->set(_t('contacts', $sMsg.$sLiks, $aLinks));
        } else {
            $this->model->userSave($nCurrentUserId, array("payed_opening_cnt = payed_opening_cnt - 1"));
        }
    }

    public function useProOrPayedOpening($isPro, $nCurrentUserId, $nPayedOpeningCnt,
                                         $bPluginPackagesEnabled = false, $bAllowedPackageUnlim = false)
    {
        $nAllowedOpeningCnt = config::get('users_pro_contacts_cnt');
        $nUserOpeningCnt = User::data('pro_opening_cnt');

        if (!$isPro || ( $isPro && ($nUserOpeningCnt >= $nAllowedOpeningCnt))) {
            $this->usePaidOpening($nPayedOpeningCnt,
                                    $isPro,
                                    $nCurrentUserId,
                                    $bPluginPackagesEnabled,
                                    $bAllowedPackageUnlim);
        } else {

            $this->db->update(TABLE_USERS, ['pro_opening_cnt = pro_opening_cnt + 1'], ['user_id' => $nCurrentUserId]);


        }
    }

    public static function avaliableTotalOpeningContactsCnt($bPayedCnt = false, $bProCnt = false, $bPackagesCnt = false)
    {
        $nAvaliableProCnt = 0;
        $nAvaliablePackagesCnt = 0;
        $aAvaliablePackages = [];
        $nAvaliablePayedCnt = User::data('payed_opening_cnt');

        if($bPayedCnt){
            return $nAvaliablePayedCnt;
        }

        if (User::isPro()) {
            $nUserOpeningCnt = User::data('pro_opening_cnt');
            $nAllowedOpeningCnt = config::get('users_pro_contacts_cnt');
            $nAvaliableProCnt = $nAllowedOpeningCnt - $nUserOpeningCnt;
        }
        if ($bProCnt) {
            return $nAvaliableProCnt;
        }

        if (class_exists('PluginLoaderContactsPackages')){
            if(PluginLoaderContactsPackages::getPluginInstance()->isEnabled() && User::isClient()){
                $aAvaliablePackages = Packages::getUserAllActualPackagesAndCounters();
                if(!empty($aAvaliablePackages)){
                    $nAvaliablePackagesCnt = $aAvaliablePackages['all_left_quantity'];
                }
            }

        }
        if ($bPackagesCnt){
            return $aAvaliablePackages;
        }

        $nAvaliableTotalCnt = (int)$nAvaliableProCnt + (int)$nAvaliablePayedCnt + (int)$nAvaliablePackagesCnt;
        return $nAvaliableTotalCnt;

    }

    public static function avaliableCurrentUserPackageUnlim()
    {
        if (!class_exists('PluginLoaderContactsPackages')){
            return false;

        }

        $bPluginPackagesEnabled = PluginLoaderContactsPackages::getPluginInstance()->isEnabled();

        if( !$bPluginPackagesEnabled){
            return false;
        }

        return Packages::isEnabledCurrentUserPackagesUnlim();

    }

}