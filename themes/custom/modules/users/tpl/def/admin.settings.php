<?php
$bUseClient = Users::useClient();
$aTabs = array(
    'general'  => 'Общие',
    'contacts' => 'Контакты',
);
?>
<?= tplAdmin::blockStart('Пользователи / '._t('','Settings'), false, array('id'=>'UsersSettingsFormBlock')); ?>

<form name="UsersSettingsForm" id="UsersSettingsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="edit" />
    <input type="hidden" name="save" value="1" />
    <div class="tabsBar" id="UsersSettingsFormTabs">
        <? foreach($aTabs as $k=>$v) { ?>
            <span class="tab<? if($k == 'general') { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
        <? } ?>
    </div>
    <div class="j-tab j-tab-general">
        <table class="admtbl tbledit">
            <tr>
                <td class="row1" colspan="2">
                    <div class="field-title" style="margin-bottom: 5px;">Кол-во доступных <u>дополнительных</u> специализаций:</div>
                    <table>
                        <tr>
                            <td width="200"><strong>PRO</strong> аккаунт:</td>
                            <td><input type="number" min="0" step="1" class="short" name="specialization_limit_pro" value="<?= HTML::escape($specialization_limit_pro) ?>" /></td>
                        </tr>
                        <tr>
                            <td>Обычный аккаунт:</td>
                            <td><input type="number" min="0" step="1" class="short" name="specialization_limit" value="<?= HTML::escape($specialization_limit) ?>"></td>
                        </tr>
                    </table>
                    <hr>
                    <table>
                        <? if(isset($opinions_delay)):?>
                            <tr>
                                <td width="200">
                                    <?=_t('', 'Отсрочка публикации отзыва, дней:')?>
                                </td>
                                <td>
                                    <input type="number" min="0" step="1" class="short" name="opinions_delay" value="<?= HTML::escape($opinions_delay) ?>">
                                </td>
                            </tr>
                        <? endif;?>
                    </table>
                    <hr>
                    <div class="field-title" style="margin-bottom: 5px;"><?= _t('','Кол-во открытий контактов:');?></div>
                    <table>
                        <? if(isset($pro_contacts_cnt)):?>
                            <tr>
                                <td width="200">
                                    <?=_t('', '<strong>PRO</strong> аккаунт:')?>
                                </td>
                                <td>
                                    <input type="number" min="0" step="1" class="short" name="pro_contacts_cnt" value="<?= HTML::escape($pro_contacts_cnt) ?>">
                                </td>
                            </tr>
                        <? endif;?>
                    </table>
                    <hr>
                    <div class="field-title" style="margin-bottom: 5px;"><?= _t('','Разовое открытие контактов:');?></div>
                    <table>
                            <tr>
                                <td width="200">
                                    <?=_t('', 'Стоимость анкеты [user]:', ['user' => ($bUseClient)? 'исполнителя': 'пользователя'])?>
                                </td>
                                <td>
                                    <input type="number" min="1" step="1" class="short" name="worker_contact_amount" value="<?= HTML::escape($worker_contact_amount) ?>">
                                     &nbsp;<?=Site::currencyDefault()?>
                                </td>
                            </tr>
                        <? if( $bUseClient ):?>
                            <tr>
                                <td width="200">
                                    <?=_t('', 'Стоимость анкеты заказчика:')?>
                                </td>
                                <td>
                                    <input type="number" min="1" step="1" class="short" name="client_contact_amount" value="<?= HTML::escape($client_contact_amount) ?>">
                                    &nbsp;<?=Site::currencyDefault()?>
                                </td>
                            </tr>
                        <? endif;?>
                    </table>
                </td>
            </tr>
            <? if(Users::verifiedEnabled()): ?>
                <tr><td colspan="2"><hr /></td></tr>
                <?= $this->locale->buildForm($aData, 'users-settings-verified-requirement', '
                <tr>
                    <td class="row1" width="120">Верификация пользователей:<br /><span class="desc small">(требования)</span></td>
                    <td class="row2"><?= tpl::jwysiwyg((isset($aData[\'verified_requirement_\'.$key]) ? $aData[\'verified_requirement_\'.$key] : \'\'), \'verified_requirement[\'.$key.\']\', 0, 120); ?></td>
                </tr>
            '); ?>
            <? endif; ?>
        </table>
    </div>

    <? $bUseClient = Users::useClient(); ?>
    <div class="j-tab j-tab-contacts hidden">
        <table class="admtbl tbledit">
            <tr>
                <td class="row1" colspan="2">
                    <div class="field-title bold" style="margin-bottom: 5px;">Контакты<? if($bUseClient) { ?> исполнителей<? } else { ?> пользователей<? } ?>&nbsp;<a href="javascript:void(0);" class="icon-question-sign" id="j-contacts-worker-help" style="opacity: 0.4;"></a><div id="j-contacts-worker-help-popover"></div></div>
                    <table class="admtbl tbledit">
                        <tr>
                            <td class="row1" width="120">Отображать:</td>
                            <td class="row1">
                                <label class="radio"><input type="radio" name="contacts_worker_display" value="<?= Users::CONTACTS_DISPLAY_ALL ?>" <?= $contacts_worker_display == Users::CONTACTS_DISPLAY_ALL ? 'checked="checked"' : '' ?>>во всех профилях</label>
                                <label class="radio"><input type="radio" name="contacts_worker_display" value="<?= Users::CONTACTS_DISPLAY_PRO ?>" <?= $contacts_worker_display == Users::CONTACTS_DISPLAY_PRO ? 'checked="checked"' : '' ?>>только в платных профилях</label>
                            </td>
                        </tr>
                        <tr>
                            <td class="row1">Видны:</td>
                            <td class="row1">
                                <label class="radio"><input type="radio" name="contacts_worker_view" value="<?= Users::CONTACTS_VIEW_ALL ?>"  <?= $contacts_worker_view == Users::CONTACTS_VIEW_ALL ? 'checked="checked"' : '' ?>>всем</label>
                                <label class="radio"><input type="radio" name="contacts_worker_view" value="<?= Users::CONTACTS_VIEW_AUTH ?>" <?= $contacts_worker_view == Users::CONTACTS_VIEW_AUTH ? 'checked="checked"' : '' ?>>только авторизованным</label>
                                <label class="radio"><input type="radio" name="contacts_worker_view" value="<?= Users::CONTACTS_VIEW_PRO ?>"  <?= $contacts_worker_view == Users::CONTACTS_VIEW_PRO ? 'checked="checked"' : '' ?>>только платным аккаунтам</label>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <? if($bUseClient): ?>
                <tr>
                    <td colspan="2">
                        <hr class="cut">
                    </td>
                </tr>
                <tr>
                    <td class="row1" colspan="2">
                        <div class="field-title bold" style="margin-bottom: 5px;">Контакты заказчиков&nbsp;<a href="javascript:void(0);" class="icon-question-sign" id="j-contacts-client-help" style="opacity: 0.4;"></a><div id="j-contacts-client-help-popover"></div></div>
                        <table class="admtbl tbledit">
                            <tr class="hidden">
                                <td class="row1" width="120">Отображать:</td>
                                <td class="row1">
                                    <label class="radio"><input type="radio" name="contacts_client_display" value="<?= Users::CONTACTS_DISPLAY_ALL ?>"  <?= $contacts_client_display == Users::CONTACTS_DISPLAY_ALL ?  'checked="checked"' : '' ?>>во всех профилях</label>
                                    <label class="radio"><input type="radio" name="contacts_client_display" value="<?= Users::CONTACTS_DISPLAY_PRO ?>" <?= $contacts_client_display == Users::CONTACTS_DISPLAY_PRO ? 'checked="checked"' : '' ?>>только в платных профилях</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="row1" width="120">Видны:</td>
                                <td class="row1">
                                    <label class="radio"><input type="radio" name="contacts_client_view" value="<?= Users::CONTACTS_VIEW_ALL ?>"  <?= $contacts_client_view == Users::CONTACTS_VIEW_ALL ?  'checked="checked"' : '' ?>>всем</label>
                                    <label class="radio"><input type="radio" name="contacts_client_view" value="<?= Users::CONTACTS_VIEW_AUTH ?>" <?= $contacts_client_view == Users::CONTACTS_VIEW_AUTH ? 'checked="checked"' : '' ?>>только авторизованным</label>
                                    <label class="radio"><input type="radio" name="contacts_client_view" value="<?= Users::CONTACTS_VIEW_PRO ?>" <?= $contacts_client_view == Users::CONTACTS_VIEW_PRO ? 'checked="checked"' : '' ?>>только платным аккаунтам</label>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <? endif; ?>
        </table>
    </div>

    <div style="margin-top: 10px;">
        <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jUsersSettingsForm.save(false);" />
        <div id="UsersSettingsFormProgress" class="progress" style="display: none;"></div>
        <div class="clearfix"></div>
    </div>

</form>
<?= tplAdmin::blockStop(); ?>

<script type="text/javascript">
    var jUsersSettingsForm =
        (function(){
            var $progress, $form, f;
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $progress = $('#UsersSettingsFormProgress');
                $form = $('#UsersSettingsForm');
                f = $form.get(0);

                // tabs
                $form.find('#UsersSettingsFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                    var key = $(this).data('key');
                    $form.find('.j-tab').addClass('hidden');
                    $form.find('.j-tab-'+key).removeClass('hidden');
                    $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
                });

                // help
                if (bff.bootstrapJS()) {
                    $form.find('#j-contacts-worker-help').popover({trigger:'hover',placement:'bottom',container:'#j-contacts-worker-help-popover',
                        title:'',html:true,
                        content:'<div>В профиле<? if($bUseClient){ ?> исполнителя<? } ?></div><? if(!$bUseClient){ ?><div>В заказе</div><? } ?><div>В заявке к заказу</div>'});
                    <? if($bUseClient){ ?>
                    $form.find('#j-contacts-client-help').popover({trigger:'hover',placement:'bottom',container:'#j-contacts-client-help-popover',
                        title:'',html:true,
                        content:'<div>В профиле заказчика</div><div>В заказе</div>'});
                    <? } ?>
                }
            });

            return {
                save: function()
                {
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('Данные успешно сохранены');
                        }
                    }, $progress);
                }
            };
        }());
</script>