<?php
    /**
     * Регистрация пользователя через соц. сети
     * @var $this Users
     */
$phone_on = Users::registerPhone();

$usersTypesMaminzam = bff::filter('users.types', array(
    static::TYPE_WORKER => array(
        'id'        => static::TYPE_WORKER,
        't'         => _t('users', 'Няня'),
        'genitivus' => _t('users', 'няня'),
        'dativus'   => _t('users', 'няня'),
        'plural'    => _t('users', 'Няня'),
    ),
    static::TYPE_CLIENT => array(
        'id'        => static::TYPE_CLIENT,
        't'         => _t('users', 'Родитель'),
        'genitivus' => _t('users', 'родитель'),
        'dativus'   => _t('users', 'родитель'),
        'plural'    => _t('users', 'Родитель'),
    ),
));

?>

<div class="s-signBlock-form">

    <div class="row">
        <div class="form-sm mrgb30">
            <div class="text-center">
                <img src="<?= $avatar ?>" alt="" style="width: 60px; border-radius: 50%" />
            </div>
            <div class="">
                <div class="s-signBlock_hello j-social">
                    <?= _t('users','Здравствуйте, <strong>[name]</strong>!', array('name'=>$name)) ?>
                </div>
                <div class="s-signBlock_hello displaynone j-social">
                    <?= _t('users', 'У вас уже есть профиль на [site_name]?', array('site_name'=>Site::title('users.register.social'))) ?>
                </div>
            </div>
            <form action="" class="form" role="form" id="j-u-register-social-form" autocomplete="off">
                <div class="form-group">
                    <input type="email" name="email" class="form-control j-required" id="j-u-register-social-email" autocomplete="off" placeholder="<?= _t('users','Введите Ваш Email') ?>" maxlength="100" />
                    <a class="link-ajax j-social" style="display:none;" id="j-u-register-social-email-change" href="#"><?= _t('users','Изменить e-mail') ?></a>
                </div>
                <? if($phone_on): ?>
                    <div class="form-group">
                        <?= $this->registerPhoneInput(array('id'=>'j-u-register-phone','name'=>'phone', 'placeholder' => _t('users','Введите номер Вашего телефона'))) ?>
                    </div>
                    <div class="form-group j-step2 hidden">
                        <input type="text" class="form-control" name="code" autocomplete="off" placeholder="<?= _t('users', 'Введите код из sms') ?>"/>
                        <a class="link-ajax j-phone-repeate" href="#"><?= _t('users', 'Выслать код повторно') ?></a>
                    </div>
                <? endif; ?>
                <? $usersTypes = $usersTypesMaminzam; if (sizeof($usersTypes) > 1): ?>
                    <? foreach($usersTypes as $v):?>
                        <div class="radio-inline  j-social">
                            <label>
                                <input type="radio" name="type" value="<?= $v['id'] ?>" autocomplete="off" />
                                <?= $v['t'] ?>
                            </label>
                        </div>
                    <? endforeach; ?>
                <? else: ?>
                    <input type="radio" name="type" value="<?= key($usersTypes) ?>" checked="checked" style="display: none;" />
                <? endif; ?>

                <div class="form-group displaynone j-social">
                    <input type="password" name="pass" class="form-control" id="j-u-register-social-pass" placeholder="<?= _t('users','Введите ваш пароль') ?>" autocomplete="off" maxlength="100" />
                    <a class="link-ajax" href="<?= Users::url('forgot', array('social'=>1)) ?>"><?= _t('users','Забыли пароль?') ?></a>
                </div>

                <div class="form-group j-social">
                    <div class="checkbox checkbox-none mrgt20">
                        <label>
                            <input type="checkbox" name="agreement" checked id="j-u-register-social-agreement" autocomplete="off" />
                            <?= _t('users', 'Регистрируясь, Вы соглашаетесь с
                                        <a href="[link_agreement]" target="_blank">правилами использования сервиса,</a>
                                        а также с передачей и обработкой Ваших данных.',
                                array('link_agreement'=>Users::url('agreement'))) ?>
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block j-social"><?= _t('users','Завершить регистрацию') ?></button>
                <button type="submit" class="btn btn-primary displaynone j-social"><?= _t('users','Объединить профили') ?></button>
            </form>
        </div>
    </div>


</div>
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    jUserAuth.registerSocial(<?= func::php2js(array(
        'login_url' => Users::url('login'),
        'lang' => array(
            'register'=>array(
                'title' => _t('users', 'Для завершения регистрации введите Вашу электронную почту'),
                'email' => _t('users', 'E-mail адрес указан некорректно'),
                'agreement' => _t('users', 'Пожалуйста подтвердите, что Вы согласны с пользовательским соглашением'),
                'type' => _t('users', 'Пожалуйста укажите тип пользователя'),
            ),
            'login'=>array(
                'title' => _t('users', 'Пользователь с таким e-mail адресом уже зарегистрирован.'),
                'pass' => _t('users', 'Укажите пароль'),
            )
        ),
        'phone' => $phone_on,
    )) ?>);
});
<? js::stop(); ?>
</script>