<?php
/**
 * Список фрилансеров.
 * @var $this Users
 */
if(Users::profileMap()){
    Geo::mapsAPI(true);
    if(Geo::mapsType() == Geo::MAPS_TYPE_GOOGLE){
        tpl::includeJS('markerclusterer/markerclusterer', false);
    }
}
tpl::includeJS('users.search', false, 5);
if (User::id()) {
    tpl::includeJS('users.note', false, 2);
}

tpl::includeJS(['dist/rating']);

echo Users::i()->svc_carousel_block();
?>
    <div class="container">

        <section class="l-mainContent" id="j-users-search">
            <div class="row">

                <!-- Left Column -->
                <aside class="col-md-3">
                    <?= $form ?>
                </aside>

                <div class="col-md-9" id="j-users-search-list">

                    <h6 class=" h6-text visible-lg visible-md  ">
                        <?= _t('users', 'Найдено'); ?> <span class="j-users-count">
                            <?= $count ?>
                        </span>
                    </h6>


                    <div class="l-mapView <?= ! $f['m'] ? 'hidden' : '' ?>">
                        <div id="map-desktop" class="map-google"></div>
                    </div>

                    <? if($f['m']): ?>
                        <div class="j-pagination"><?= $pgn ?></div>
                        <div class="j-list"><?= $list ?></div>
                    <? else: ?>
                        <div class="j-list"><?= $list ?></div>
                        <div class="j-pagination"><?= $pgn ?></div>
                    <? endif; ?>

                </div>

            </div>
        </section>

    </div>
<script type="text/javascript">
<? js::start() ?>
jUsersSearch.init(<?= func::php2js(array(
    'lang' => array(),
    'ajax' => true,
    'locationDefault' => Users::url('list'),
    'defCountry'    => Geo::defaultCountry(),
    'rootSpec'      => Specializations::ROOT_SPEC,
    'currSpec'      => $spec_id,
    'currCat'       => $cat_id,
    'points'        => ! empty($points) ? $points : array(),
    'coordorder'    => Geo::$ymapsCoordOrder,
    'coordDefaults' => Geo::$ymapsDefaultCoords,
    'preSuggest'    => Geo::countrySelect() && $f['c'] ? Geo::regionPreSuggest($f['c'], 2) : '',
    'cityID'        => Geo::filterEnabled() ? Geo::filter('id-city') : 0,
)) ?>);
<? if(User::id()):?>
jUsersNote.init();
<? endif;?>
(function(){
    $(function(){
        window.ratingViewTotalAverage = new Rating();
        window.ratingViewTotalAverage.initViewTotalAverage(
            <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
        );
    });
})();
<? js::stop() ?>
</script>