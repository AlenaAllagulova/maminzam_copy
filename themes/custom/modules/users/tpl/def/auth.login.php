<?php
    /**
     * Авторизация пользователя
     * @var $this Users
     */
?>
<div class="s-signBlock-form">
    <div class="row">
        <div class="form-sm mrgb30">
            <div class="form-sm__caption mrgb10">
                <?= _t('','Войдите в свой аккаунт через социальную сеть')?>
            </div>

            <div class="social-buttons mrgb10">
                <? foreach($providers as $k=>$v): ?>
                    <a href="#" class="social-buttons__items social-buttons_<?= $v['class'] ?> j-u-login-social-btn"  data="{provider:'<?= $v['key'] ?>',w:<?= $v['w'] ?>,h:<?= $v['h'] ?>}" ></a>
                <? endforeach; ?>
            </div>

            <div class="or-text mrgb20">
                <span>
                    <?= _t('','Или')?>
                </span>
            </div>

            <form action="" class="g-form" role="form" id="j-u-login-form">
                <? if( ! empty($back)): ?>
                    <input type="hidden" name="back" value="<?= HTML::escape($back) ?>" />
                <? endif; ?>
                <div class="form-group j-required">
                    <label for="j-u-login-email" class=" control-label">
                        <?= _t('users', 'Электронная почта') ?>
                        <i class="text-danger">*</i>
                    </label>
                    <div class="">
                        <input type="text"  name="email" class="form-control" id="j-u-login-email" placeholder="<?= _t('users', 'Введите ваш email или login') ?>" />
                    </div>
                </div>
                <div class="form-group j-required">
                    <div class="flex flex_sb flex_center">
                        <label for="j-u-login-pass" class=" control-label">
                            <?= _t('users', 'Пароль') ?>
                            <i class="text-danger">*</i>
                        </label>
                        <a class="" href="<?= Users::url('forgot') ?>">
                            <?= _t('users', 'Забыли пароль?') ?>
                        </a>
                    </div>
                    <div class="">
                        <input type="password" name="pass" class="form-control" id="j-u-login-pass" placeholder="<?= _t('users', 'Введите ваш пароль') ?>" />
                    </div>
                </div>

                <div class="form-group mrgt10">
                    <div class="checkbox ">
                        <label>
                            <input type="checkbox" checked="checked" name="remember"/>
                            <span class="checkbox__text checkbox__text_grey">
                                <?= _t('users', 'Запомнить меня'); ?>
                            </span>
                        </label>
                    </div>
                </div>
                <div class="form-group mrgt20 mrgb30">
                    <button type="submit" class="btn btn-primary btn-block j-submit" >
                        <?= _t('users', 'Войти на сайт') ?>
                    </button>
                </div>
                <div class="text-center">
                    <div class="flex flex_center">
                        <span>
                            <?= _t('users', 'Еще не с нами?') ?>
                        </span>
                        <a class="mrgl5 " href="<?= Users::url('register', ['user_type'=> Users::TYPE_CLIENT]); ?>">
                            <?= _t('users', 'Зарегистрируйтесь') ?>
                        </a>
                    </div>

                </div>
            </form>

        </div>

    </div>


</div><!-- /.signBlock-form -->
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    jUserAuth.login(<?= func::php2js(array(
        'login_social_url' => Users::url('login.social'),
        'login_social_return' => $back,
        'lang' => array(
            'email' => _t('users', 'E-mail адрес указан некорректно'),
            'pass' => _t('users', 'Укажите пароль'),
        ),
    )) ?>);
});
<? js::stop(); ?>
</script>