<?php
/**
 * Восстановление пароля. Шаг1: Инициация восстановления пароля по E-mail адресу
 * @var $this Users
 */
?>
<div class="s-signBlock-form">
    <div class="row">
        <div class="form-sm mrgb30">
            <form action="" class="form g-form" role="form" id="j-u-forgot-start-form">
                <input type="hidden" name="social" value="<?= $social ?>" />
                <div class="form-sm__caption mrgb20">
                    <?= _t('','Востановить пароль')?>
                </div>
                <div class="form-group">
                    <label for="j-u-forgot-start-email">
                        <?= _t('users', 'Электронная почта или логин') ?>
                        <i class="text-danger">*</i>
                    </label>
                    <input type="text" class="form-control j-required" name="email" id="j-u-forgot-start-email" placeholder="<?= _t('users', 'Введите ваш email или логин') ?>" maxlength="100" />
                </div>

                <button type="submit" class=" mrgt20 mrgb20 btn btn-primary btn-block"><?= _t('users', 'Восстановить пароль') ?></button>

                <div class="text-center">
                    <div class="flex flex_center">
                        <span>
                            <?= _t('users', 'Еще не с нами?') ?>
                        </span>
                        <a class="mrgl5 link-bold" href="<?= Users::url('register') ?>">
                            <?= _t('users', 'Зарегистрируйтесь') ?>
                        </a>
                    </div>

                </div>

            </form>

        </div>
    </div>


</div>
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    jUserAuth.forgotStart(<?= func::php2js(array(
        'lang' => array(
            'email' => _t('users', 'E-mail адрес указан некорректно'),
            'success' => _t('users', 'На ваш электронный ящик были высланы инструкции по смене пароля.'),
        ),
    )) ?>);
});
<? js::stop(); ?>
</script>