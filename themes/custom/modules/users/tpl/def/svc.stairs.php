<?php
tpl::includeJS('svc', false, 3);
tpl::includeJS(['dist/rating']);
$aData['cur'] = $cur = Site::currencyDefault();
$i = 1; $aData['i'] = & $i;
?>
    <div class="container">
        <section class="l-mainContent" id="j-stairs-block">

            <div class="row">
                <div class="col-md-12">

                    <?= tpl::getBreadcrumbs($breadcrumbs); ?>

                    <div class="text-center">
                        <h1 class=""><?= $svc['title_view'][LNG] ?></h1>
                        <p class="max-w400 m0-a mrgb40">
                            <?= $svc['description_full'][LNG] ?>
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="row">

                                <aside class="col-md-4" id="j-stairs-specs">

                                    <span class="btn-filter-md flex mrgb10" data-toggle="collapse" data-target="#left-column">
                                        <?= _t('svc', 'Показать разделы'); ?>
                                        <i class="icon-arrow-point-to-down mrgl10"></i>
                                    </span>

                                    <div class="l-leftColumn collapse" id="left-column">

                                        <h5><?= _t('', 'Выберите раздел'); ?> <i class="text-danger">*</i></h5>

                                        <ul class="list-def">
                                            <? foreach($cats as $v):
                                                $bSpecs = ! empty($v['specs']);
                                                $bDisabled = false;
                                                if ($v['id'] < 0 ) {
                                                    if (isset($list['spec'][$v['id']])) {
                                                        $bDisabled = true;
                                                    }
                                                } else {
                                                    if (isset($list['cat'][ $v['id'] ])) {
                                                        $bDisabled = true;
                                                    }
                                                }
                                                ?>
                                            <li <?= $bSpecs ? 'class="opened"' : '' ?>>
                                                <?= $bSpecs ? '<span>' : '' ?>
                                                <div class="checkbox">
                                                  <label>
                                                      <input type="checkbox" name="<?= $bSpecs ? 'cat' : 'spec' ?>[]" value="<?= $v['id'] ?>" <?= $bDisabled ? ' checked="checked" disabled="disabled" ' : '' ?> />
                                                      <span class="checkbox__text">
                                                           <?= $v['title'] ?>
                                                      </span>
                                                  </label>
                                                </div>
                                                <?= $bSpecs ? '</span>' : '' ?>
                                            <? if($bSpecs): ?>
                                                <ul class="list-def">
                                                    <? foreach($v['specs'] as $vv):
                                                        $bDisabled = false;
                                                        if (isset($list['spec'][ $vv['id'] ])) {
                                                            $bDisabled = true;
                                                        }
                                                        ?>
                                                    <li>
                                                        <span>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="spec[]" value="<?= $vv['id'] ?>" <?= $bDisabled ? ' checked="checked" disabled="disabled" ' : '' ?> />
                                                                    <span class="checkbox__text">
                                                                        <?= $vv['title'] ?>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </span>
                                                    </li>
                                                    <? endforeach; ?>
                                                </ul>
                                            <? endif; ?>
                                            </li>
                                            <? endforeach; ?>
                                        </ul>
                                    </div>
                                </aside>

                                <div class="col-md-8">
                                    <h5><?= _t('svc', 'Выбранные разделы'); ?></h5>
                                    <form action="" method="post" >
                                        <? foreach($cats as $v) :
                                            if ($v['id'] < 0 ){
                                                if(isset($list['spec'][ $v['id'] ])) {
                                                    $d = $aData;
                                                    $d['it'] = $list['spec'][ $v['id'] ];
                                                    $d['it']['spec_title'] = $v['title'];
                                                    echo $this->viewPHP($d, 'svc.stairs.section');
                                                    unset($list['spec'][ $v['id'] ]);
                                                }
                                            } else {
                                                if (isset($list['cat'][ $v['id'] ])) {
                                                    $d = $aData;
                                                    $d['it'] = $list['cat'][ $v['id'] ];
                                                    $d['it']['spec_title'] = $v['title'];
                                                    echo $this->viewPHP($d, 'svc.stairs.section');
                                                    unset($list['cat'][ $v['id'] ]);
                                                }
                                                if( ! empty($v['specs'])){
                                                    foreach($v['specs'] as $vv){
                                                        if(isset($list['spec'][ $vv['id'] ])){
                                                            $d = $aData;
                                                            $d['it'] = $list['spec'][ $vv['id'] ];
                                                            $d['it']['spec_title'] = $vv['title'];
                                                            echo $this->viewPHP($d, 'svc.stairs.section');
                                                            unset($list['spec'][ $vv['id'] ]);
                                                        }
                                                    }
                                                }
                                            }
                                        endforeach; ?>

                                    <div id="j-add-block">
                                    </div>

                                    <div class="c-formSubmit">
                                        <button class="btn btn-primary c-formSuccess j-submit hidden"></button>
                                        <a class="c-formCancel j-cancel" href="#"><?= _t('form', 'Отмена'); ?></a>
                                    </div>
                                    </form>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="j-add-form" class="hidden">
                <div class="se-service-section j-stairs j-add-__counter__">
                    <input type="hidden" name="spec[__counter__]" class="j-spec" value="0" />
                    <input type="hidden" name="cat[__counter__]"  class="j-cat" value="0" />

                    <div class="se-service-close"><a href="#" class="j-close"><i class="fa icon-cancel-music"></i></a></div>
                    <h5 class="j-title bold"></h5>
                    <div class="clearfix"></div>

                    <div class="user-box">
                        <a class="user-box__avatar user-box__avatar_sq" href="<?= Users::url('my.profile'); ?>">
                            <img src="<?= UsersAvatar::url($user['id'], $user['avatar'], UsersAvatar::szSmall, $user['sex']); ?>" alt="<?= tpl::avatarAlt($user); ?>" />
                            <?= tpl::userOnline($user) ?>
                        </a>
                        <div class="user-box__text mrgl10">

                            <div class="rating">
                                <?= Rating::getViewTotalAverage($user['user_id'], 'partial.total.average') ?>
                            </div>
                            <div class="user-box__text-s">
                                <?= isset($user['spec_data']['title'])?$user['spec_data']['title']:''?>
                            </div>
                            <span class="nowrap">
                                <?= tpl::userLink($user, 'no-login, no-verified', '', array('class'=>'f-freelancer-name')) ?>
                            </span>
                            <div class="user-box__text-s">
                                <?= isset($user['region_data']['title'])? $user['region_data']['title'].', ':''?>
                                <?= $user['district_title']?>
                                <br>
                                <?= $user['addr_addr'];?>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="mrgt10">
                        <p>
                            <?= _t('svc', 'На сколько недель Вы хотите заказать услугу?'); ?>
                        </p>

                        <div class="flex flex_center">
                            <div class="se-service-section-input">
                                <div class="number-count">
                                        <span class="number-count__btn j-minus">
                                            <i class="icon-minus-symbol"></i>
                                        </span>
                                    <input type="text" name="weeks[__counter__]" class="form-control short j-weeks" value="1" min="1" max="10" />
                                    <span class="number-count__btn j-plus">
                                        <i class="icon-cancel-music"></i>
                                    </span>
                                </div>
                            </div>
                            <span class="bold mrgl10">
                                <strong class="j-sum">0 <?= $cur ?></strong>
                            </span>
                        </div>

                    </div>

                </div>
            </div>

        </section>

    </div>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jSvcStairsSelect.init(<?= func::php2js(array(
            'lang' => array(
                'cur' => $cur,
                'buy' => _t('svc', 'Пополнить на'),
                'publish' => _t('svc', 'Оплатить'),
                'success' => _t('svc', 'Услуга успешно активирована'),
                'maxlength_left' => _t('users','[symbols] осталось'),
                'maxlength_symbols' => explode(';', _t('users', 'знак;знака;знаков')),
            ),
            'balance' => $user['balance'],
            'i' => $i,
            'price' => array(
                'main' => $svc['price_main'],
                'user' => $svc['price_user'],
                'spec' => $svc['price_spec'],
                'cat'  => (isset($svc['price_cat'])?$svc['price_cat']:0),
            ),
            'spec_main' => Users::SVC_STAIRS_MAIN,
            'spec_user' => Users::SVC_STAIRS_USER,
            'cat_id'    => $cat_id,
            'spec_id'   => $spec_id,
            'message_length' => 80,
            'descr_length'   => 400,
        )) ?>);
    });
    (function(){
        $(function(){
            window.ratingViewTotalAverage = new Rating();
            window.ratingViewTotalAverage.initViewTotalAverage(
                <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
            );
        });
    })();
    <? js::stop(); ?>
</script>