<?php
?>
<div class="form-group mrgt10">
    <label class="control-label">
        <?= ! empty($priceSett['price_title']) ? $priceSett['price_title'] : _t('users', 'Цена') ?>
    </label>
    <div class="row">
        <div class="col-md-4">
            <input type="text" class="form-control input-sm" name="<?= $prefix?>[<?= $spec ?>][price]" value="<?= ! empty($values['price']) ? $values['price'] : '' ?>" />
        </div>

        <? if( ! empty($priceSett['rates'])): ?>
        <div class="col-md-4 select-custom_global">
            <select  class="form-control j-select input-sm" name="<?= $prefix?>[<?= $spec ?>][price_rate]"><?= HTML::selectOptions($priceSett['rates'], ( ! empty($values['price_rate']) ? $values['price_rate'] : 0) ) ?></select>
        </div>
        <? endif; ?>
        <div class="col-md-4 select-custom_global">
            <select  class="form-control j-select input-sm" name="<?= $prefix?>[<?= $spec ?>][price_curr]"><?= Site::currencyOptions( ! empty($values['price_curr']) ? $values['price_curr'] : ( ! empty($priceSett['curr']) ? $priceSett['curr'] : Site::currencyDefault('id'))) ?></select>
        </div>
    </div>
</div>
<div class="form-group mrgt10">
    <label class="control-label">
        <?= _t('users', 'Бюджет от:') ?>
    </label>
    <div class="row">
        <div class="col-md-4">
            <input type="text" class="form-control input-sm" name="<?= $prefix?>[<?= $spec ?>][budget]" value="<?= ! empty($values['budget']) ? $values['budget'] : '' ?>" />
        </div>
        <div class="col-md-4 select-custom_global">
            <select class="form-control j-select" name="<?= $prefix?>[<?= $spec ?>][budget_curr]">
                <?= Site::currencyOptions( ! empty($values['budget_curr']) ? $values['budget_curr'] : ( ! empty($priceSett['curr']) ? $priceSett['curr'] : Site::currencyDefault('id'))) ?>
            </select>
        </div>
    </div>
</div>