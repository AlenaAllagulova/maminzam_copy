<?php
tpl::includeJS('svc', false, 3);
$bPro = $user['pro'];
$cur = Site::currencyDefault();
?>
<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-12">

                <h1 class="small text-center mrgt50"><?= $svc['title_view'][LNG] ?></h1>

                <? if ($bPro): ?><?= _t('svc', 'Ваш аккаунт <span class="pro">pro</span> действителен до [date]', array('date' => tpl::date_format_pub($user['pro_expire'], 'd.m.Y'))); ?><? endif; ?>

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="hidden-xs">
                            <div class="table se-table">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th><?= _t('svc', 'Возможности'); ?></th>
                                        <th class="se-table-pro-column text-center"><?= _t('svc', 'Аккаунт'); ?> <span
                                                    class="pro">pro</span></th>
                                        <th class="text-center"><?= _t('svc', 'Базовый аккаунт'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><?= _t('svc', 'Количество предложений на заказы <small>(в день)</small>'); ?></td>
                                        <td class="se-table-pro-column text-center"><?= Orders::offersLimit(true) > 999 ? _t('svc', 'Неограничено') : Orders::offersLimit(true) ?></td>
                                        <td class="text-center"><?= Orders::offersLimit() > 999 ? _t('svc', 'Неограничено') : Orders::offersLimit() ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= _t('svc', 'Возможность отвечать на заказы с пометкой "Только для [pro]"', array('pro' => '<span class="pro">pro</span>')); ?></td>
                                        <td class="se-table-pro-column text-center"><i class=" icon-verify-sign "></i></td>
                                        <td class="text-center"><i class="icon-cancel-music"></i></td>
                                    </tr>
                                    <tr>
                                        <td><?= _t('svc', 'Размещение в каталоге выше остальных'); ?></td>
                                        <td class="se-table-pro-column text-center"><i class=" icon-verify-sign "></i></td>
                                        <td class="text-center"><i class="icon-cancel-music"></i></td>
                                    </tr>
                                    <tr>
                                        <td><?= _t('svc', 'Множитель рейтинга'); ?></td>
                                        <td class="se-table-pro-column text-center">x <?= Users::ratingMultiplier() ?></td>
                                        <td class="text-center"><i class="icon-cancel-music"></i></td>
                                    </tr>
                                    <? $nSpecLimit = Users::specializationsLimit();
                                    if ($nSpecLimit): ?>
                                        <tr>
                                            <td><?= _t('svc', 'Количество специализаций, по которым размещаетесь в каталоге'); ?></td>
                                            <td class="se-table-pro-column text-center"><?= Users::specializationsLimit(true) ?></td>
                                            <td class="text-center"><?= $nSpecLimit ?></td>
                                        </tr>
                                    <? endif;
                                    if (Portfolio::previewOnlyPro()): ?>
                                        <tr>
                                            <td><?= _t('svc', 'Превью работ в портфолио'); ?></td>
                                            <td class="se-table-pro-column text-center"><i class=" icon-verify-sign "></i></td>
                                            <td class="text-center"><i class="icon-cancel-music"></i></td>
                                        </tr>
                                    <? endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="visible-xs">
                    <h6><?= _t('svc', 'Преимущества аккаунта Pro'); ?></h6>
                    <ul class="list-unstyled">
                        <li>
                            <i class=" icon-verify-sign  text-primary"></i> <?= Orders::offersLimit(true) > 999 ? _t('svc', 'Неограниченное количество предложений на заказы (в день)') : _t('svc', '[N_decl] на заказы (в день)', array('N_decl' => tpl::declension(Orders::offersLimit(true), _t('svc', 'предложение;предложения;предложений')))) ?>
                        </li>
                        <li>
                            <i class=" icon-verify-sign  text-primary"></i> <?= _t('svc', 'Возможность отвечать на заказы с пометкой "Только для [pro]"', array('pro' => '<span class="pro">pro</span>')); ?>
                        </li>
                        <li>
                            <i class=" icon-verify-sign  text-primary"></i> <?= _t('svc', 'Размещение в каталоге выше остальных'); ?>
                        </li>
                        <li><i class=" icon-verify-sign  text-primary"></i> <?= _t('svc', 'Множитель рейтинга'); ?> <span
                                    class="text-primary">x <?= Users::ratingMultiplier() ?></span></li>
                        <? if ($nSpecLimit): ?>
                            <li>
                                <i class=" icon-verify-sign  text-primary"></i> <?= _t('svc', '[n_decl], по которым размещаетесь в каталоге', array('n_decl' => tpl::declension(Users::specializationsLimit(true), _t('svc', 'специализация;специализации;специализаций')))); ?>
                            </li>
                        <? endif;
                        if (Portfolio::previewOnlyPro()): ?>
                            <li><i class=" icon-verify-sign  text-primary"></i> <?= _t('svc', 'Превью работ в портфолио'); ?>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <h6 class="text-center"><?= $bPro ? _t('svc', 'Продлить аккаунт') : _t('svc', 'Купить аккаунт') ?></h6>
                        <form class="form" method="post" id="j-svc-pro">
                            <div class="services-list ">
                                <? foreach ($svc['mass'] as $k => $v): ?>
                                    <div class="services-list__item-label">
                                        <label class="">
                                            <input type="radio" name="m" value="<?= $k ?>" data-sum="<?= $v ?>">
                                            <span class="services-list__label">
                                                <?= tpl::declension($k, _t('svc', 'месяц;месяца;месяцев')) ?>
                                                <?= _t('','продвинутого пользования ресурсом.') ?>
                                                <span class="services-list__price mrgt10"> <?= $v ?> <?= $cur ?></span>
                                            </span>
                                        </label>
                                    </div>
                                <? endforeach; ?>
                            </div>
                            <div class="c-formSubmit text-center">
                                <button class="min-w-200px btn btn-primary c-formSuccess j-submit"><?= _t('svc', 'Купить'); ?></button>
                                <a class="c-formCancel j-cancel" href="#"><?= _t('form', 'Отмена'); ?></a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    <? js::start(); ?>
    $(function () {
        jSvcPro.init(<?= func::php2js(array(
            'lang' => array(
                'select_month' => _t('svc', 'Выберите период'),
                'svc' => $bPro ? _t('svc', 'Продлить') : _t('svc', 'Купить'),
                'buy' => _t('svc', 'Пополнить'),
                'success' => _t('svc', 'Услуга успешно активирована'),
            ),
            'balance' => $user['balance'],
        )) ?>);
    });
    <? js::stop(); ?>
</script>