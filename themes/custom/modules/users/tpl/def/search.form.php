<?php
    tpl::includeJS(array('autocomplete'), true);
    $lng_all = _t('', 'Все');
    $lng_back = _t('form', 'Вернуться назад');
?>
<div class="h6-text visible-md visible-lg">
    <?= _t('','Фильтр')?>
</div>

<div class="mrgt20">
    <div class="">
        <a href="javascript:volid(0);" class="btn-filter-md flex mrgb10" data-toggle="collapse" data-target="#j-users-search-form-block">
            <span>
                <?= _t('', 'Фильтр'); ?>
                (<?= _t('users', 'Найдено'); ?>
                    <span class="j-users-count"><?= $count ?></span>
                )
            </span>
            <i class="icon-arrow-point-to-down mrgl10"></i>
        </a>
    </div>

    <div class="form-sidebar collapse" id="j-users-search-form-block">
        <form action="" method="get">
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <input type="hidden" name="m" value="<?= $f['m']?>" />

        <a href="#j-users-specs" data-toggle="collapse" data-parent="#accordion" class="form-sidebar__title active">
            <?= _t('', 'Специализации') ?>
            <i class=" icon-arrow-point-to-down"></i>
        </a>
        <div class="collapse in j-collapse" id="j-users-specs">
            <? if (Specializations::catsOn()): ?>
                <ul class="l-left-categories l-inside ">
                    <? foreach($specs as &$v): if(empty($v['specs'])) continue; ?>
                        <li class=" <? if($v['a']){ ?> opened<? } ?>">

                            <ul class="list-def ">
                                <li class="mrgb5 <? if($v['a'] && ! $spec_id){ ?>  checked <? } ?>">
                                    <a class="filter-checkbox " href="<?= $v['url'] ?>">
                                        <i class="mrgr5 fa <?= (($v['a'] && ! $spec_id)?'ico-square-checked' :'ico-square')?>"></i>
                                        <?= $lng_all; ?>
                                    </a>
                                </li>
                                <? foreach($v['specs'] as &$vv): ?>
                                    <li class="mrgb5 <? if($vv['a']){ ?> checked<? } ?>">
                                        <a href="<?= $vv['url'] ?>" class="filter-checkbox">
                                            <i class="mrgr5 fa <?= (($vv['a'])?'ico-circle-checked' :'ico-circle')?>"></i>
                                            <?= $vv['title'] ?>
                                        </a>
                                    </li>
                                <? endforeach; unset($vv); ?>
                            </ul>
                        </li>
                    <? endforeach; unset($v); ?>
                </ul>
            <? else: ?>

                <ul class="list-def">
                    <? foreach($specs as &$v): ?>
                        <li class="mrgb5">
                            <a href="<?= $v['url'] ?>" class="filter-checkbox">
                                <i class="mrgr5 fa <?= (($v['a']) ? 'ico-circle-checked' :'ico-circle') ?>"></i>
                                <?= $v['title'] ?>
                            </a>
                        </li>
                    <? endforeach; unset($v); ?>
                </ul>

                <ul class="l-left-categories l-left-categories_mobile visible-sm visible-xs">
                    <? foreach($specs as &$v): ?>
                        <li<? if($v['a']){ ?> class="active"<? } ?>><a href="<?= $v['url'] ?>"><?= $v['title'] ?></a></li>
                    <? endforeach; unset($v); ?>
                </ul>

            <? endif; ?>
        </div>

        <div id="j-users-search-form-dp"><?= ! empty($dp) ? $dp : '' ?></div>

        <? if(Users::profileMap()): ?>
            <? if( ! Geo::filterEnabled()): ?>
            <? $bCountry = Geo::countrySelect(); $bOpenRegion = $f['ct'] || ($bCountry && $f['c']); ?>
            <a href="#j-left-region" data-toggle="collapse" data-parent="#accordion" class="form-sidebar__title active">
                <?= _t('users', 'Регион'); ?>
                <i class=" icon-arrow-point-to-down"></i>
            </a>
            <div class="collapse in j-collapse" id="j-left-region">
                <div class="l-inside">
                    <div class="form">
                        <? if($bCountry): ?>
                            <div class="form-group">
                                <select name="c" class="form-control" id="j-left-region-country" autocomplete="off">
                                    <?= HTML::selectOptions(Geo::countryList(), $f['c'], _t('', 'Все страны'), 'id', 'title') ?>
                                </select>
                            </div>
                        <? endif; ?>
                        <div class="relative">
                            <input type="hidden" name="r" value="<?= $f['r'] ?>" id="j-region-pid-value" />
                            <input type="hidden" name="ct" value="<?= $f['ct'] ?>" id="j-region-city-value" />
                            <input type="text" class="form-control input-sm<?= $bCountry && empty($f['c']) ? ' hidden' : '' ?>" id="j-region-city-select" value="<?= $region_title ?>" placeholder="<?= _t('users', 'Или введите название региона'); ?>" />
                        </div>
                        <div class="relative mrgt20">
                            <input type="hidden" name="me" value="<?= $f['me'] ?>" id="j-region-metro-value" />
                            <input type="text" class="form-control input-sm<?= empty($metro_exists) ? ' hidden' : '' ?>" id="j-region-metro-select" value="<?= $metro_title ?>" placeholder="<?= _t('users', 'Или введите название станции метро'); ?>" />
                        </div>
                    </div>
                </div>
            </div>
            <? else: if( ! empty($metro_exists)): $openMetro = $f['me']; ?>
                <a href="#j-left-region" data-toggle="collapse" data-parent="#accordion" class="form-sidebar__title active">
                    <?= _t('users', 'Метро'); ?>
                    <i class=" icon-arrow-point-to-down"></i>
                </a>
                <div class="collapse in j-collapse" id="j-left-region">
                    <div class="form">
                        <div class="relative">
                            <input type="hidden" name="me" value="<?= $f['me'] ?>" id="j-region-metro-value" />
                            <input type="text" class="form-control input-sm" id="j-region-metro-select" value="<?= $metro_title ?>" placeholder="<?= _t('users', 'Введите название станции метро'); ?>" />
                        </div>
                    </div>
                </div>
            <? endif; endif; ?>
        <? endif; ?>

        <? $bOpen = ! empty($f['exf']) || ! empty($f['ext']); ?>
        <a href="#j-left-experience" data-toggle="collapse" data-parent="#accordion" class="form-sidebar__title active">
            <?= _t('users', 'Опыт работы (лет)'); ?>
            <i class=" icon-arrow-point-to-down"></i>

        </a>
        <div class="collapse in j-collapse" id="j-left-experience">
            <div class=" l-budget">
                <div class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control input-sm j-input-change" placeholder="<?= _t('', 'От'); ?>" name="exf" value="<?= ! empty($f['exf']) ? $f['exf'] : '' ?>" />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-sm j-input-change" placeholder="<?= _t('', 'До'); ?>" name="ext"  value="<?= ! empty($f['ext']) ? $f['ext'] : '' ?>"/>
                    </div>
                </div>
            </div>
        </div>

        <? $aFlags = array(
            'fv'  => _t('users', 'Мои избранные'),
            'ex'  => _t('users', 'Только с примерами работ'),
            'op'  => _t('users', 'Только с отзывами'),
            'neg' => _t('users', 'Без отрицательных отзывов'),
            'fr'  => _t('users', 'Только со статусом "свободен"'),
            'vr'  => _t('users', 'Только подтвержденные [icon]', array('icon' => '')),
            'pro' => _t('users', 'Только [pro]', array('pro' => '<div class="mrgl5"><span class="pro">pro</span></div>')),
        );
        if( ! Users::verifiedEnabled()){
            unset($aFlags['vr']);
        }
        if( ! bff::servicesEnabled()){
            unset($aFlags['pro']);
        }
        if( ! User::id() || Users::useClient() && ! User::isClient()){
            unset($aFlags['fv']);
        }
        ?>
        <? $bOpen = false; foreach($aFlags as $k => $v){ $bOpen |= $f[$k] ? 1 : 0; } ?>
        <a href="#left-more" data-toggle="collapse" data-parent="#accordion" class="form-sidebar__title active">
            <?= _t('users', 'Дополнительно'); ?>
            <i class=" icon-arrow-point-to-down"></i>
        </a>
        <div class="collapse in j-collapse" id="left-more">
            <ul class="list-def">
                <? foreach($aFlags as $k => $v): ?>
                    <input type="hidden" name="<?= $k ?>" value="<?= $f[$k] ?>" />
                    <li class="mrgb5 filter-checkbox <?= $f[$k] ? 'checked ' : '' ?> j-checkbox-flag" data-name="<?= $k ?>">
                        <i class="mrgr5 fa <?= $f[$k] ? 'ico-square-checked' : 'ico-square' ?>"></i>
                        <?= $v ?>
                    </li>
                <? endforeach; ?>
            </ul>
        </div>

        <div class="l-inside text-center">
            <a href="#" class="ajax-link j-clear-filter btn btn-clear-filter btn-block">
                <?= _t('', 'Сбросить фильтры'); ?>
            </a>
        </div><!-- /.l-inside -->

        </form>

    </div>

    <div class="visible-md visible-lg">

        <?= ! empty($stairs) ? $stairs : '' ?>

        <? # Баннер: Исполнители: список ?>
        <?= Banners::view('users_list', array('pos'=>'left', 'spec'=>$spec_id)) ?>

    </div>
</div>