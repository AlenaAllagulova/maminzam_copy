<?php
/** @var $this Users */
$isOwner = User::isCurrent($id);
$aCurrentUserData = Users::model()->userData($userID, 'type');
?>
<? if( ! empty($contacts)): # контакты и кнопка их открытия
    $bOpenedContacts = Users::isOpenedContacts($id);
    # показывать кнопку в чужом профиле для противоположного типа пользователя
    $bShowContactsBtn = !$isOwner && !$bOpenedContacts && ($aCurrentUserData['type'] != $type);
    if( ! User::id() || $bShowContactsBtn):?>
        <div class="l-inside text-center j-show-btn-block">
            <button onclick="<?= ( ! User::id())? '$(\'.j-show-btn\').hide();$(\'#j-login-link\').toggle();' :
                "Custom.open_contacts({$id})"?>"
                    class="btn btn-success j-show-btn">
                <?=_t('contacts', 'Показать контакты')?>
            </button>
            <a style="display: none"
               href="<?=Users::url('login')?>"
               class="btn btn-warning"
               id="j-login-link">
                <i class="fa fa-lock"></i>
                <?=_t('contacts', 'Авторизируйтесь')?>
            </a>
        </div>
    <? endif;?>
    <? if($isOwner || $bOpenedContacts):?>
    <?= $this->viewPHP($aData, 'contacts.block');?>
<? else:?>
    <div class="j-contacts-block"></div>
<? endif;?>
<? endif; ?>