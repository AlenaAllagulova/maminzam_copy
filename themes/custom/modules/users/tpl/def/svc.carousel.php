<?php
    tpl::includeCSS(array('owl.carousel'), true);
    tpl::includeJS(array('owl.carousel.min'), false);
    tpl::includeJS('svc', false, 3);
    tpl::includeJS(['dist/rating']);
    $cur = Site::currencyDefault();
?>
    <div class="container">
        <section class="l-mainContent">
            <div class="row">
                <div class="col-md-12">

                    <?= tpl::getBreadcrumbs($breadcrumbs); ?>
                    <div class="text-center">
                        <h1 class=""><?= $svc['title_view'][LNG] ?></h1>
                        <p class="max-w400 m0-a mrgb40">
                            <?= $svc['description_full'][LNG] ?>
                        </p>
                    </div>
                    <div class="l-carousel-container">
                        <div id="j-carousel" class="owl-carousel l-carousel l-freelancers-carousel mrgb50">
                            <? foreach($list as $v): ?>
                                <div>
                                    <div class="media l-freelancers-carousel-item">
                                        <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="pull-left l-freelancers-carousel__avatar">
                                            <?= tpl::userAvatar($v) ?>
                                        </a>
                                        <div class="media-body">
                                            <div class="rating">
                                                <?= Rating::getViewTotalAverage($v['user_id'], 'partial.total.average') ?>
                                            </div>
                                            <div class="l-freelancers-carousel__text">
                                                <?= isset($v['spec_data']['title'])?$v['spec_data']['title']:''?>
                                            </div>
                                            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="l-freelancers-carousel__name">
                                                <?= $v['name'] ?>
                                                <? if(!empty($v['surname'])): ?>
                                                    <?= mb_strimwidth($v['surname'], 0, 2, "."); ?>
                                                <? endif; ?>
                                            </a>
                                            <div class="l-freelancers-carousel__text l-freelancers-carousel__text_second">
                                                <?= isset($v['region_data']['title'])? $v['region_data']['title'].', ':''?>
                                                <?= $v['district_title']?>
                                                <br>
                                                <?= $v['addr_addr'];?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>

                        <div class="text-center">

                            <form class="mrgt20" action="" method="post" id="j-svc-carousel">
                                <div>

                                    <div class="media-body c-formSubmit">

                                        <div class="form-group hidden">
                                            <textarea name="message" rows="2"  class="form-control input-sm" placeholder="<?= _t('svc', 'Текст сюда'); ?>"><?= ! empty($message) ? $message : 'Default value' ?></textarea>
                                        </div>
                                        <button class="min-w-200px btn btn-primary c-formSuccess">
                                            <?= ($user['balance'] >= $svc['price'] ?
                                            _t('svc', 'Купить за [price] [cur]', array('price' => $svc['price'], 'cur' => $cur)) :
                                            _t('svc', 'Пополнить счет на [price] [cur]', array('price' => $svc['price'], 'cur' => $cur)) ) ?>
                                        </button>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="j-pay-form-request" class="hidden"></div>
    </div>

<? if( ! empty($list)): ?>
    <script type="text/javascript">
        <? js::start() ?>
        $(function(){
            var $carousel = $('#j-carousel');
            if($carousel.length){
                $carousel.owlCarousel({
                    items : 4, //10 items above 1000px browser width
                    itemsDesktop : [991,3], //5 items between 1000px and 901px
                    itemsDesktopSmall : [768,3], // betweem 900px and 601px
                    itemsTablet: [600,1], //2 items between 600 and 0
                    itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                    navigation : true,
                    navigationText : ["<i class=' icon-arrow-point-to-left'></i>", "<i class=' icon-arrow-point-to-right '></i>"],
                    pagination : true,
                    autoPlay : true
                });
            }

        });
        <? js::stop() ?>
    </script>
<? endif; ?>

<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jSvcCarousel.init(<?= func::php2js(array(
            'lang' => array(),
        )) ?>);
    });
    (function(){
        $(function(){
            window.ratingViewTotalAverage = new Rating();
            window.ratingViewTotalAverage.initViewTotalAverage(
                <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
            );
        });
    })();
    <? js::stop(); ?>
</script>