<?php
    if( ! empty($list)):
        tpl::includeJS(['dist/custom']);
        $aStatus = Users::aWorkerStatus();
        $bProEnabled = (bff::servicesEnabled() && Portfolio::previewOnlyPro());
        Portfolio::i();
        $lng_spec = _t('users', 'Специализация:');
        $lng_opinions = _t('opinions', 'отзывов');
        $lng_rating = _t('users', 'Рейтинг:');
        $lng_budget = _t('users', 'Бюджет от:');
        $bUseServices = Specializations::useServices();
        $tagsLimit = Users::searchTagsLimit();
        $nUserID = User::id();
        $bAllowFav = ($nUserID > 0 && Users::useClient());
        if ($bAllowFav) tpl::includeJS('users.list.fav', false, 2);
        $userID = User::id();
        $aCurrentUserData = Users::model()->userData($userID, 'type');
        $inviteEnabled = false;
        if (Orders::invitesEnabled()) {
            do {
                if ( ! $userID ) break;
                if (Users::useClient() && ! User::isClient()) break;
                tpl::includeJS('orders.invite.list', false, 2);
                tpl::includeJS('specs.select', false, 5);
                $inviteEnabled = true;
            } while(false);
        }

?>
    <div class="user-list mrgt25i">
        <? foreach($list as &$v): ?>
            <div class="row user-list__box user-list__box_now-wrap j-user-block">
                <div class="col-md-9 col-sm-12 w100p-sm">
                    <div class="user-list__item">

                        <div class="user-list__wrap">
                            <div class="user-list__avatar">
                                <div class="relative">

                                    <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="user-avatar user-avatar_xs">
                                        <img src="<?= UsersAvatar::url($v['user_id'], $v['avatar'], UsersAvatar::szBig, $v['sex']); ?>" alt="">
                                        <? if (($v['pro'] == 1)): ?>
                                            <span class="pro">pro</span>
                                        <? endif; ?>
                                        <? if($bAllowFav): $bIsFav = Users::model()->isUserFav($nUserID, $v['user_id']); ?>
                                            <div class="j-usr-block-fav">
                                                <span href="javascript:void(0)"
                                                   class="user-avatar__fav j-add-fav<?= $bIsFav ? ' hidden' : '' ?>"
                                                   data-id="<?=  $v['user_id'] ?>">
                                                    <i class="icon-heart"></i>
                                                </span>
                                                <span href="javascript:void(0)"
                                                   class="user-avatar__fav j-del-fav<?= $bIsFav ? '' : ' hidden' ?>"
                                                   data-id="<?=  $v['user_id'] ?>">
                                                    <i class="icon-heart-full"></i>
                                                </span>
                                            </div>
                                        <? endif; ?>
                                    </a>
                                </div>

                                <div class="text-center rating mrgt5">
                                    <?= Rating::getViewTotalAverage($v['user_id'], 'partial.total.average') ?>
                                </div>

                                <div class="user-list__opinions">
                                    <?= tpl::opinions($v['opinions_cache'], array('login' => $v['login'])); ?>
                                    <?= $lng_opinions; ?>
                                    <?// todo clazion count opinions  and link opinions  ?>
                                </div>

                            </div>
                            <div class="user-list__body">
                                <div class="user-list__header">
                                    <div class="flex flex_column">
                                        <span class="user-list__text-light"><?= $v['main_spec_title'] ?></span>
                                        <a class="user-list__link" href="<?= Users::url('profile', array('login' => $v['login'])); ?>">
                                            <?= $v['name'] ?>
                                            <? if(!empty($v['surname'])): ?>
                                                <?= mb_strimwidth($v['surname'], 0, 2, "."); ?>
                                            <? endif; ?>
                                        </a>
                                        <? if( ! empty($v['city_data']['title'])): ?>
                                            <span class="user-list__text-light user-list__text-light_sec">
                                                <?= $v['city_data']['title'] ?><? if(!empty($v['district_id'])):?>, <?= Geo::districtTitle($v['district_id'])?><?endif;?>
                                            </span>
                                        <? endif; ?>
                                    </div>
                                    <div class="user-list__price">
                                        <? if( ! empty($v['main_price'])): ?>
                                            <div>
                                                <?= ! empty($v['main_spec_price_title']) ? $v['main_spec_price_title'] : _t('users', 'Цена'); ?><?=
                                                ! empty($v['main_price_rate_text'][LNG]) ? ' '.$v['main_price_rate_text'][LNG] : '' ?>:
                                                <?= $v['main_price'] ?>
                                                <?= Site::currencyData($v['main_price_curr'], 'title_short'); ?>
                                            </div>
                                        <? endif; ?>
                                        <? if( ! empty($v['main_budget'])): ?>
                                            <div>
                                                 <?= $lng_budget; ?>
                                                <?= $v['main_budget'] ?>
                                                <?= Site::currencyData($v['main_budget_curr'], 'title_short'); ?>
                                            </div>
                                        <? endif; ?>
                                    </div>


                                </div>
                                <div class="user-list__conditions mrgt10">
                                    <? if(Users::profileBirthdate()):?>
                                        <div class="user-list__conditions-item" style="min-width: 85px;">
                                            <div class="bold">
                                                <?= ($v['birthdate'] == '1901-01-01' ||  $v['birthdate'] == '0000-00-00')?
                                                    _t('', 'не указан'):
                                                    tpl::date_format_spent( $v['birthdate'], false, false);?>
                                            </div>
                                            <span class="user-list__text">
                                                <?= _t('','Возраст')?>
                                            </span>
                                        </div>
                                    <? endif;?>
                                    <? if($v['type'] == Users::TYPE_WORKER || ! Users::useClient()): ?>
                                        <? $aExperience = Users::aExperience();
                                        ?>
                                        <div class="user-list__conditions-item" style="min-width: 85px;">
                                            <div class="bold">
                                                <?= $aExperience[$v['experience']]['t'] ?>
                                            </div>
                                            <span class="user-list__text">
                                                <?= _t('','Опыт')?>
                                            </span>
                                        </div>
                                    <? endif;?>
                                    <div class="user-list__conditions-item" style="min-width: 85px;">
                                        <?  if (!empty($v['dynprops_simple'])): # краткий вывод динсвойствa ?>
                                            <? foreach ($v['dynprops_simple'] as $dp_item): ?>
                                                <? if ($dp_item['cache_key'] == Users::DP_KEY_QUANTITY_CHILDREN ): ?>
                                                    <? if($dp_item['value'] !== ''):?>
                                                        <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                        <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                                            <? foreach ($dp_item['multi'] as $val): ?>
                                                                <? if ($dp_item['value'] == $val['value']): ?>
                                                                    <div class="bold"><?= $val['name'] ?></div>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? else: ?>
                                                            <div class="bold">
                                                                <? foreach ($dp_item['value'] as $dp_val): ?>
                                                                    <? foreach ($dp_item['multi'] as $val): ?>
                                                                        <? if ($dp_val == $val['value']): ?>
                                                                            <?= $val['name'] ?> <br>
                                                                        <? endif; ?>
                                                                    <? endforeach; ?>
                                                                <? endforeach; ?>
                                                            </div>
                                                        <? endif; ?>
                                                    <? else: ?>
                                                        <div class="bold">
                                                            <?= _t('dp_user','не указано') ?>
                                                        </div>
                                                    <? endif;?>
                                                    <span class="user-list__text">
                                                         <?= !empty($dp_item['description'])? $dp_item['description'] : _t('dp_user_qc', 'К-во детей'); ?>
                                                    </span>
                                                <? endif; ?>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                    </div>
                                </div>
                                <? if(!empty($v['status_text'])):?>
                                    <div class="user-list__text mrgt10">
                                        <?= tpl::truncate($v['status_text'], 250, '...', true) ?>
                                    </div>
                                <? endif;?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-5  user-list__notif">
                <? if(bff::security()->isLogined() ) : ?>
                    <? if( ! empty($v['contacts']['contacts'])): # контакты и кнопка их открытия
                        $open_user_id = $v['open_user_id'] = $v['user_id'];
                        $open_user_type = $v['open_user_type'] = $v['type'];
                        $is_profile = Site::i()->isProfilePage();
                        $bOpenedContacts = Users::isOpenedContacts($open_user_id);
                        $isOwner = User::isCurrent($open_user_id);
                        # показывать кнопку в чужом профиле для противоположного типа пользователя
                        $bShowContactsBtn = !$isOwner && !$bOpenedContacts && ($aCurrentUserData['type'] != $open_user_type);
                        if( ! User::id() || $bShowContactsBtn):?>
                            <div class=" text-center mrgb10 j-show-btn-block " data-btn-opend-user-id="<?=$open_user_id;?>">
                                <button onclick="<?= ( ! User::id())? '$(\'.j-show-btn\').hide();$(\'#j-login-link\').toggle();' :
                                    "Custom.open_contacts({$open_user_id},{$is_profile})"?>"
                                        class="btn btn-primary btn-block j-show-btn">
                                    <?=_t('contacts', 'Купить контакт')?>
                                </button>
                                <a style="display: none"
                                   href="<?=Users::url('login')?>"
                                   class="btn btn-primary btn-block"
                                   id="j-login-link">
                                    <?=_t('contacts', 'Авторизируйтесь')?>
                                </a>
                            </div>
                        <? endif;?>
                        <? if($inviteEnabled): ?>
                            <div class="text-center mrgb10">
                                <a href="javascript:void(0)" class="link-bold link-bold_fz15 j-order-invite" data-toggle="modal" data-id="<?=  $v['user_id'] ?>">
                                    <?= _t('orders', 'Предложить работу'); ?>
                                </a>
                            </div>
                        <? endif; ?>
                        <? if($isOwner || $bOpenedContacts):?>
                            <?= $this->viewPHP($v, 'contacts.block');?>
                        <? else:?>
                            <div class="j-contacts-block" data-opend-user-id="<?=$open_user_id;?>"></div>
                        <? endif;?>
                    <? endif; ?>

                    <div class="mrgt20">
                        <?  $aNote = [
                                'id'      => $v['note_id'],
                                'user_id' => $v['user_id'],
                                'note'    => $v['note'],
                                'bFav'    => true,
                        ];
                        echo($this->viewPhp($aNote, 'my.note.block'));
                        ?>
                    </div>
                <? endif; ?>
                </div>
            </div>
        <? endforeach; unset($v); ?>

    </div>
    <? else: ?>
        <div class="alert alert-info"><?= _t('users', 'Исполнители не найдены'); ?></div>
    <? endif;?>

    <? if(isset($bAllowFav) && $bAllowFav):?>
        <script type="text/javascript">
            <? js::start() ?>
            jUsersListFav.init(<?= func::php2js(array(
                'lang' => array(),
            )) ?>);
            <? js::stop() ?>
        </script>
    <? endif; ?>
    
    <script type="text/javascript">
    (function(){
        if (typeof window.ratingViewTotalAverage === 'object') {
            window.ratingViewTotalAverage.initViewTotalAverage(
                <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
            );
        }
    })();
    </script>
    <? if (isset($inviteEnabled) && !empty($inviteEnabled)):
        $aTerms = Orders::aTerms();
        $aTermsText = array();
        foreach ($aTerms as $v) {
            if ($v['days']) {
                $aTermsText[$v['id']] = tpl::date_format2(time() + $v['days'] * 24 * 60 * 60, false, true);
            } else {
                $aTermsText[$v['id']] = false;
            }
        } ?>
        <script type="text/javascript">
            <? js::start() ?>
            jOrdersInviteList.init(<?= func::php2js(array(
                'lang' => array(
                    'spec_wrong' => _t('orders', 'Укажите специализацию'),
                    'invite_success' => _t('orders', 'Предложение было успешно отправлено'),
                ),
                'user_id' => false,
                'terms' => $aTermsText,
            )) ?>);
            <? js::stop() ?>
        </script>
    <? endif; ?>