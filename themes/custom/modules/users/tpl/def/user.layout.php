<div class="container">

    <section class="l-mainContent">
        <div class="row">
            <aside class="col-md-3">
                <?= $profile ?>
            </aside>
            <div class="col-md-9 l-content-column">
                <div class="dropdown dropdown-share">
                    <button class="btn btn-share" type="button" data-toggle="dropdown">
                        <i class="icon-share"></i>
                    </button>
                    <div class="dropdown-menu ">

                        <script type="text/javascript">(function() {
                                if (window.pluso)if (typeof window.pluso.start == "function") return;
                                if (window.ifpluso==undefined) { window.ifpluso = 1;
                                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                                    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                                    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                                    var h=d[g]('body')[0];
                                    h.appendChild(s);
                                }})();</script>
                        <div class="pluso" data-background="transparent" data-options="small,round,line,vertical,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>

                    </div>
                </div>
                <div class="user-tab">
                    <?= tpl::getTabs(array('tabs' => $tabs, 'a' => $tab, 'class' => 'j-user-cabinet-tab', 'noAjax' => 1)); ?>
                </div>
                <?= $content ?>
            </div>
        </div>
    </section>
</div>
