<?php
if( ! empty($favs)): ?>
<? tpl::includeJS(['dist/rating']); ?>
<div class="fav-users">
    <? foreach($favs as $v): ?>
        <div class="fav-users__item">
            <div class="user-box">
                <?if(User::id()):?>
                    <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="user-box__avatar user-box__avatar_sq-big">
                        <?= tpl::userAvatar($v) ?>
                    </a>
                <? else:?>
                    <div class="user-box__avatar user-box__avatar_sq-big">
                        <?= tpl::userAvatar($v) ?>
                    </div>
                <? endif;?>
                <div class="mrgl10">
                    <div class="user-box__text">
                        <div class="rating">
                            <?= Rating::getViewTotalAverage($v['user_id'], 'partial.total.average') ?>
                        </div>
                        <a class="" <?= User::id()? 'href="'.Users::url('profile', array('login' => $v['login'])).'"':''?>>
                            <?= $v['name'] ?>
                            <? if(!empty($v['surname'])): ?>
                                <?= mb_strimwidth($v['surname'], 0, 2, "."); ?>
                            <? endif; ?>
                            <?= (($v['pro'] == 1)? '<span class="pro mrgb5">pro</span>' : '')?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <? endforeach; ?>
    </div>

    <script type="text/javascript">
        <? js::start() ?>
        (function(){
            $(function(){
                window.ratingViewTotalAverage = new Rating();
                window.ratingViewTotalAverage.initViewTotalAverage(
                    <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
                );
            });
        })();
        <? js::stop() ?>
    </script>

<? endif;
