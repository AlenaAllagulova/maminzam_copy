<?php
/**
 * @var $it array
 */
$days = strtotime($it['expire']) - time();
$days = round($days / 86400);
?>
<div class="se-service-section j-stairs" data-sum="0">
    <input type="hidden" name="spec[<?= $i ?>]" class="j-spec" value="<?= $it['spec_id'] ?>"/>
    <input type="hidden" name="cat[<?= $i ?>]" class="j-cat" value="<?= $it['cat_id'] ?>"/>

    <h5 class="bold"><?= $it['spec_title'] ?></h5>
    <div class="clearfix"></div>
    <table class="table se-service-section-table">
        <tr>
            <td><?= _t('svc', 'Дата оплаты:'); ?></td>
            <td><?= tpl::date_format2($it['payed'], true) ?></td>
        </tr>
        <tr>
            <td><?= _t('svc', 'Срок окончание:'); ?></td>
            <td><?= tpl::date_format2($it['expire'], true) ?></td>
        </tr>
        <tr>
            <td><?= _t('svc', 'Осталось:'); ?></td>
            <td><strong class="text-success"><?= tpl::declension($days, _t('', 'день;дня;дней'), true); ?></strong></td>
        </tr>
    </table>
    <table class="table se-service-section-table">
        <tr>
            <td><?= _t('svc', 'Продлить на:'); ?></td>
            <td>
                <div class="se-service-section-input flex flex_center mrgb10">

                    <div class="number-count">
                            <span class="number-count__btn j-minus">
                                <i class="icon-minus-symbol"></i>
                            </span>
                        <input type="text" name="weeks[__counter__]" class="form-control short j-weeks" value="0" min="0" max="10" />
                        <span class="number-count__btn j-plus">
                            <i class="icon-cancel-music"></i>
                        </span>
                    </div>
                    <span class="bold mrgl10">
                        <?= _t('svc', 'недель'); ?>
                    </span>
                </div>
            </td>
        </tr>
        <tr>
            <td class=""><?= _t('svc', 'Сумма:'); ?></td>
            <td><span><strong class="j-sum bold">0 <?= $cur ?></strong></span></td>
        </tr>
    </table>
</div>