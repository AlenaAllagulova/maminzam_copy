<?php
$nUserID = User::id();
$bAdd = true;

tpl::includeJS(['dist/rating']);

if (empty($list) && ( ! $nUserID || ! User::isWorker())) return;
?>
    <h5 class="mrgb5"><?= _t('svc', 'Вам могут понравится'); ?></h5>

    <ul class="list-def" id="j-svc-stairs-block">

        <? foreach($list as $v): $bEdit = $v['user_id'] == $nUserID; ?>
            <li class="media">
                <div class="user-box mrgb10">
                    <div class="user-box__avatar user-box__avatar_sq">
                        <a class="" href="<?= Users::url('profile', array('login' => $v['login'])); ?>">
                            <?= tpl::userAvatar($v) ?>
                        </a>
                    </div>
                    <div class="user-box__text mrgl10">
                        <div class="rating">
                            <?= Rating::getViewTotalAverage($v['user_id'], 'partial.total.average') ?>
                        </div>
                        <?if(isset($v['spec_data']) && !empty($v['spec_data'])):?>
                        <span class="user-box__text-s"><?= $v['spec_data']['title']?></span>
                        <? endif;?>
                        <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $v['login'])); ?>">
                            <?= $v['name'] ?>
                            <? if(!empty($v['surname'])): ?>
                                <?= mb_strimwidth($v['surname'], 0, 2, "."); ?>
                            <? endif; ?>
                            <? if($v['pro']): ?><span class="pro">pro</span><? endif; ?>
                        </a>
                        <? if(isset($v['contacts_phones']) && !empty($v['contacts_phones'])):?>
                              <span>
                                  <?= $v['contacts_phones'][0]['v']?>
                              </span>
                         <?endif;?>
                        <? if($v['user_id'] == $nUserID): $bAdd = false; ?>
                            <a class="link-def link-def_fz13" target="_blank" href="<?= Svc::url('view', array('keyword' => 'stairs', 'up' => 1, 'cat_id' => $v['cat_id'], 'spec_id' => $v['spec_id'])) ?>">
                                <?= _t('svc', 'Изменить положение'); ?>
                            </a>
                        <? endif; ?>
                    </div>
                </div>
            </li>
        <? endforeach; ?>
        <? if($bAdd && User::id() && (Users::useClient() && User::isWorker() || ! Users::useClient())): ?>
        <? $url = Svc::url('view', array('keyword' => 'stairs', 'cat_id' => $cat_id, 'spec_id' => $spec_id)); ?>
        <li class="media">
            <a href="<?= $url ?>" class="pull-left">
                <img src="<?= UsersAvatar::url(0, '', UsersAvatar::szSmall); ?>" class="img-circle" alt="<?= tpl::avatarAlt(); ?>" />
            </a>
            <div class="media-body l-leftCol-wannahere">
                <a href="<?= $url ?>"><?= _t('svc', 'Как сюда попасть?'); ?></a>
            </div>
            <div class="clearfix"></div>
        </li>
        <? endif ; ?>
    </ul>

<script type="text/javascript">
<? js::start() ?>
(function(){
    $(function(){
        window.ratingViewTotalAverage = new Rating();
        window.ratingViewTotalAverage.initViewTotalAverage(
            <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
        );
    });
})();
<? js::stop() ?>
</script>