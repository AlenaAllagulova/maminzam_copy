<?php
/**
 * Регистрация пользователя. Подтвержнение email
 * @var $this Users
 */
 tpl::includeJS('users.auth', false, 5);
?>

<div class="s-signBlock-form">

    <div class="s-signBlock-div">

        <p>
            <?= _t('users', 'На указанный Вами e-mail отправлено письмо.') ?><br />
            <?= _t('users', 'Пожалуйста, перейдите по ссылке из письма для подтверждения указанного электронного адреса.') ?><br />
            <?= _t('users', 'На этом регистрация будет завершена.') ?>
        </p>
        <? if($retry_allowed) { ?>
        <p id="j-u-register-emailed-retry">
            <?= _t('users', 'Не получили письмо? <a [link_retry]>Отправить повторно</a>', array('link_retry'=>'href="#" class=""')) ?>
        </p>
        <script type="text/javascript">
        <? js::start(); ?>
        $(function(){
            jUserAuth.registerEmailed(<?= func::php2js(array(
                'lang' => array(
                'success' => _t('users', 'Письмо было успешно отправлено повторно'),
                ),
            )) ?>);
        });
        <? js::stop(); ?>
        </script>
        <? } ?>
    </div><!-- /.s-signBlock-div -->

</div><!-- /.signBlock-form -->