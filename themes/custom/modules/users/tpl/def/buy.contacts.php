<?php
tpl::includeJS(['dist/custom']);

?>
<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-12">

                <?= tpl::getBreadcrumbs($breadcrumbs); ?>
                <div class="mrgt50">

                    <h1 class="text-center">
                        <?=_t('', 'Разовая покупка контактов [user]', ['user' => ($bClient)?  'исполнителя': 'заказчика'])?>
                    </h1>
                    <div class="text-center mrgb40  ">
                        <span>
                            <?= _t('','Выберите количество контактов, которое хотите купить')?>
                        </span>
                    </div>
                    <div class="text-center">
                        <span>
                            <?=_t('buy_contacts', 'Вам доступно <strong class="bold">[cnt]</strong> контактов [user]',
                                ['cnt'  => tpl::declension($nPayedOpeningCnt, 'открытие;открытия;открытий', true),
                                    'user' => ($bClient)?  'исполнителя': 'заказчика'])?>
                        </span>
                    </div>
                    <div class="text-center mrgt20">
                        <form method="post" action="" onsubmit="return false;">
                            <div class="flex flex_center flex_jcc">
                                <div class="number-count mrgr10">
                                    <span class="j-minus-count number-count__btn">
                                        <i class="icon-minus-symbol"></i>
                                    </span>
                                    <input class="form-control short"
                                           type="text"
                                           min="1"
                                           step="1"
                                           name="contact_cnt"
                                           value="1"
                                           href="javascript:void(0)"
                                           onchange="Custom.total_sum(<?=$nContactAmount?>)">
                                    <span class="j-plus-count number-count__btn">
                                        <i class="icon-cancel-music"></i>
                                    </span>
                                </div>
                                <span class="bold">
                                    x
                                    <?= $nContactAmount.' '.Site::currencyDefault()?>
                                </span>
                            </div>

                            <div class="mrgt40">
                                <button class="min-w-200px btn btn-primary" onclick="Custom.buy_contacts()">
                                    <?= _t('svc', 'Оплатить'); ?>
                                    <span id="total_sum"><?=$nContactAmount?></span>
                                    <?= Site::currencyDefault()?>
                                </button>

                            </div>
                        </form>
                    </div>
                    <div class="text-center mrgt25">
                        <div class="mrgb20">
                            или
                        </div>
                        <a href="<?= Packages::url('packages_buy')?>">
                            купите пакет контактов
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>