<?php
?>
<div class="calendar-work <?= (isset($bScheduleViewOnly) && !empty($bScheduleViewOnly)) ? 'calendar-work_dis' : ''; ?>">
    <table >
        <tr>
            <th></th>
            <? foreach (Users::SCHEDULE_DAYS as $days): ?>
                <th class="text-center calendar-work__title">
                    <?= $days ?>
                </th>
            <? endforeach; ?>
        </tr>
        <? foreach (Users::SCHEDULE_PERIODS as $id_p => $period): ?>
            <tr>
                <th class="text-right calendar-work__title">
                    <?= $period['from'] ?>-<?= $period['to'] ?>
                </th>
                <? foreach (Users::SCHEDULE_DAYS as $id_d => $days): ?>
                    <th class="">
                        <label class="calendar-work__label">
                            <input type="checkbox"
                                   name="schedule[]"
                                <?= (isset($bScheduleViewOnly) && !empty($bScheduleViewOnly)) ? 'disabled="disabled"' : ''; ?>
                                   value="<?= $id_p . '-' . $id_d ?>" <?= (isset($schedule[$id_d]) && in_array($id_p, $schedule[$id_d])) ? 'checked="checked"' : '' ?>>
                            <span class="calendar-work__checbox"></span>
                        </label>
                    </th>
                <? endforeach; ?>
            </tr>
        <? endforeach; ?>
    </table>
</div>
