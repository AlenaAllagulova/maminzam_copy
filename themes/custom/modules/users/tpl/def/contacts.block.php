<?php
/** @var $this Users */
$isOwner = User::isCurrent($open_user_id);
$aContactsData = (key_exists('contacts', $contacts)) ? $contacts['contacts']: $contacts;
$isProfilePage = Site::i()->isProfilePage() || (isset($from_profile) && !empty($from_profile));
?>
<div class=" j-contacts-block">
    <span class="bold"><?= _t('users', 'Контакты'); ?></span>
    <? if(Users::isContactsView($open_user_id, isset($type)?$type:$open_user_type, $pro, $contactsHideReason, 'mrgb0')): ?>
        <!--noindex-->
        <ul class="p-profile-info-list">
            <? foreach($aContactsData as $v): ?>
                <li><?= tpl::linkContact($v); ?></li>
            <? endforeach; ?>
        </ul>
        <!--/noindex-->
    <? else: ?>
        <?= $contactsHideReason ?>
    <? endif; ?>
    <? if(!$isOwner && $isProfilePage): ?>
        <a href="<?= InternalMail::url('my.chat', array('user' => $login)) ?>" class="btn btn-primary btn-block mrgt10">
            <?= _t('users', 'Написать сообщение'); ?>
        </a>
    <? endif; ?>
</div>
