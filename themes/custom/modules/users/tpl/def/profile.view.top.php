<?php
if (!empty($list)):
    tpl::includeJS(['dist/custom']); ?>
    <h4><?= _t('', 'Вам могут понравиться');?></h4>
    <ul class="list-def">
        <? foreach ($list as &$v): ?>
            <li class="media">
                <div class="user-box mrgb10">
                    <div class="user-box__avatar user-box__avatar_sq">
                        <a class="" href="<?= Users::url('profile', array('login' => $v['login'])); ?>">
                            <?= tpl::userAvatar($v) ?>
                        </a>
                    </div>
                    <div class="user-box__text mrgl10">
                        <div class="rating">
                            <?= Rating::getViewTotalAverage($v['user_id'], 'partial.total.average') ?>
                        </div>
                        <?if(isset($v['main_spec_title']) && !empty($v['main_spec_title'])):?>
                            <span class="user-box__text-s mrgt5">
                                <?= $v['main_spec_title']?>
                            </span>
                        <? endif;?>
                        <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $v['login'])); ?>">
                            <?= $v['name'] ?>
                            <? if(!empty($v['surname'])): ?>
                                <?= mb_strimwidth($v['surname'], 0, 2, "."); ?>
                            <? endif; ?>
                            <? if($v['pro']): ?><span class="pro">pro</span><? endif; ?>
                        </a>


                    </div>
                </div>
            </li>

        <? endforeach;
        unset($v); ?>
    </ul>
<? endif; ?>

<script type="text/javascript">
    (function () {
        if (typeof window.ratingViewTotalAverage === 'object') {
            window.ratingViewTotalAverage.initViewTotalAverage(
                <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
            );
        }
    })();
</script>
