<?php
/** @var $this Users */
if( ! empty($favs)): ?>
<? tpl::includeJS(['dist/rating']); ?>
<div class="fav-users">
    <?  foreach($favs as $v): ?>
        <div class="fav-users__item">
            <div class="user-box">
                <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="user-box__avatar user-box__avatar_sq-big">
                    <?= tpl::userAvatar($v) ?>
                </a>
                <div class="mrgl10">
                    <div class="user-box__text">
                        <div class="rating">
                            <?= Rating::getViewTotalAverage($v['user_id'], 'partial.total.average') ?>
                        </div>
                        <a class="" href="<?= Users::url('profile', array('login' => $v['login'])); ?>">
                            <?= $v['name'] ?>
                            <? if(!empty($v['surname'])): ?>
                                <?= mb_strimwidth($v['surname'], 0, 2, "."); ?>
                            <? endif; ?>
                            <?= (($v['pro'] == 1)? '<span class="pro mrgb5">pro</span>' : '')?>
                        </a>
                    </div>
                    <div class="mrgt5">
                        <?  $aNote = array('id' => $v['note_id'], 'user_id' => $v['user_id'], 'note' => $v['note'], 'bFav' => 1);
                        echo($this->viewPhp($aNote, 'my.note.block'));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>

    <script type="text/javascript">
        <? js::start() ?>
        (function(){
            $(function(){
                window.ratingViewTotalAverage = new Rating();
                window.ratingViewTotalAverage.initViewTotalAverage(
                    <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
                );
            });
        })();
        <? js::stop() ?>
    </script>

<? endif;

