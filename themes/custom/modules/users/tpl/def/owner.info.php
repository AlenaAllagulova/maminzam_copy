<?php
    tpl::includeJS('users.info', false, 3);

    $aData['bMy'] = $bMy = User::isCurrent($id);
    if ($bMy) {
        tpl::includeJS('users.note', false, 2);
    }

    $bResume = false;
    if (Users::useClient()) {
        if ($type == Users::TYPE_WORKER) {
            $sPlural = _t('users', 'заказчика;заказчиков;заказчиков;');
            $bResume = true;
        } else {
            $sPlural = _t('users', 'исполнителя;исполнителей;исполнителей;');
        }
    } else {
        $sPlural = _t('users', 'пользователя;пользователей;пользователей;');
        $bResume = true;
    }
?>
    <div class="p-profileContent" id="j-owner-info-block">
        <div class="p-favorites-list">
            <h5><?= _t('users', 'В избранных у'); ?> <?= tpl::declension($fav_cnt, $sPlural); ?></h5>
            <div id="j-favs"><?= $favs ?></div>
            <? if($fav_cnt && ( $favs_cnt < $fav_cnt)): ?><a href="#" id="j-favs-all" data-id="<?= $id ?>"><?= _t('users', 'Показать всех'); ?> <?= tpl::declension($fav_cnt, $sPlural); ?></a><? endif; ?>
        </div>

        <? if($bMy): ?>
        <div class="p-favorites-list">
            <h5><?= _t('users', 'Мои избранные'); ?></h5>
            <div id="j-favs-my"><?= $favs_my ?></div>
            <? if($fav_my_cnt && ( $favs_my_cnt < $fav_my_cnt)): ?><a href="#" id="j-favs-my-all" data-id="<?= $id ?>"><?= _t('users', 'Показать всех [n] избранных', array('n' => $fav_my_cnt)); ?></a><? endif; ?>
        </div>
        <? endif; ?>
    </div>

<script type="text/javascript">
<? js::start() ?>
    jUsersInfo.init(<?= func::php2js(array(
        'lang' => array(
            //
        ),
        'my' => $bMy,
        'url_info' => Users::url('user.info', array('login'=>$login)),
    )) ?>);
<? js::stop() ?>
</script>