<?php
/** @var $this Users */
    $userID = User::id();
    $isOwner = User::isCurrent($id);
    $aCurrentUserData = Users::model()->userData($userID, 'type');
    $isWorker = ($type == Users::TYPE_WORKER);
    if ($isOwner && $isWorker) {
        tpl::includeJS('users.status', false, 1);
    }
    $bAllowFav = ($userID > 0);
    if ($bAllowFav) {
        if (Users::useClient()) {
            $bAllowFav = !User::type($type);
        }
        if ($bAllowFav) tpl::includeJS('users.fav', false, 2);
    }
    $aMainSpec = false;
    if ($isWorker && ! empty($specs)) {
        $aMainSpec = reset($specs);
        if ( ! empty($aMainSpec['price_rate_text'])) {
            $aMainSpec['price_rate_text'] = func::unserialize($aMainSpec['price_rate_text']);
        }
    }
    $bShowStat = ($isOwner);
    if ($bShowStat) {
        tpl::includeJS('user.views', false, 2);
    }
    $rolesEnabled = Users::rolesEnabled();
    $inviteEnabled = false;
    if (Orders::invitesEnabled()) {
        do {
            if ( ! $userID || ! $isWorker) break;
            if (Users::useClient() && ! User::isClient()) break;
            tpl::includeJS('orders.invite', false, 2);
            tpl::includeJS('specs.select', false, 5);
            $inviteEnabled = true;
        } while(false);
    }
    $rolePrivate = true;
    tpl::includeJS(['dist/custom']);
    tpl::includeJS(['qquploader'], true);
    tpl::includeJS(['dist/rating']);

?>

<div class="user-sidebar" id="j-user-status">
    <div class="user-sidebar__container">
        <div class="avatar-load mrgb10" id="j-my-profile-avatar">
            <img src="<?= $avatar ?>" alt="<?= tpl::avatarAlt($aData) ?>" id="j-my-avatar-img"/>
            <?= tpl::userOnline($aData, 'c-status-lg') ?>
            <?if($pro): ?>
                <span class="pro"><?= _t('', 'pro'); ?></span>
            <? endif; ?>
            <? if($bAllowFav): $bIsFav = Users::model()->isUserFav($userID, $id); ?>
                <span href="javascript:void(0)"
                      class="user-avatar__fav j-add-fav <?= $bIsFav ? ' hidden' : '' ?>"
                      data-id="<?= $id ?>">
                    <i class="icon-heart"></i>
                </span>
                <span href="javascript:void(0)"
                      class="user-avatar__fav j-del-fav <?= $bIsFav ? '' : ' hidden' ?>"
                      data-id="<?= $id ?>">
                    <i class="icon-heart-full"></i>
                </span>

                <script type="text/javascript">
                    <? js::start() ?>
                    jUsersFav.init(<?= func::php2js(array(
                        'lang' => array(),
                    )) ?>);
                    <? js::stop() ?>
                </script>
            <? endif; ?>

            <? if($isOwner):?>
                <div class="avatar-load__btn">
                    <a id="j-my-avatar-upload"><?= _t('users', 'Загрузить фото профиля') ?></a>
                </div>
            <? endif;?>
        </div>
        <?
        $sNameClose = '';
        if (empty($name) && empty($surname)) {
            $sName = '['.$login.']';
        } else {
            if(empty($surname)){
                $sName = $name;
            }else {
                $sName = $name . ' <span class="nowrap">' . $surname;
                $sNameClose = '</span>';
            }
        }
        ?>
        <? if($isWorker && ! empty($specs)): ?>
            <div class="">
                <? $aSpecs = array(); $fst = true;
                foreach ($specs as $v) { if($v['disabled']) continue;
                    $aUrl = array('main_spec_keyword' => $v['spec_keyword']);
                    if(Specializations::catsOn()){
                        $aUrl['main_cat_keyword'] = $v['cat_keyword'];
                    }
                    $aSpecs[] = '<span>'.$v['spec_title'].'</span>';
                }
                ?>
                <?= join(', ', $aSpecs)?>
            </div>
        <? endif; ?>

        <h6 class="user-sidebar__name">
            <?= $sName ?>
            <?= $sNameClose ?>
        </h6>
        <? if( ! empty($reg3_city)): ?>
            <span>
                <?= ! empty($city_data['title']) ? $city_data['title'] : '' ?>
                <?= $bCountry && ! empty($country_data['title']) ? ', '.$country_data['title'] : ''?>
            </span>
        <? endif; ?>

        <div>
            <? if($isOwner):
                if($isWorker):
                    $aStatus = Users::aWorkerStatus(); ?>
                <div class="p-status-text hidden">

                    <div id="j-user-status-show">
                        <div class="j-text"><?= nl2br($status_text) ?></div>
                        <div class="p-status-edit">
                            <a href="#" class="ajax-link j-edit">
                                <i class="fa fa-pencil c-link-icon"></i>
                                <span>
                                    <?= _t('form', 'Редактировать'); ?>
                                </span>
                            </a>
                        </div>
                    </div>

                    <div id="j-user-status-form-block" style="display:none;">
                        <form action="" method="post">
                            <textarea name="status_text" class="form-control" rows="3"><?= $status_text ?></textarea>
                            <div class="c-formSubmit">
                                <div id="j-status-message" class="help-block"></div>
                                <button class="btn btn-primary btn-sm c-formSuccess j-submit"><?= _t('form', 'Сохранить'); ?></button>
                                <a href="#" class="ajax-link c-formCancel j-cancel"><span><?= _t('form', 'Отмена'); ?></span></a>
                            </div>
                        </form>
                    </div>
                </div>
                <script type="text/javascript">
                <? js::start() ?>
                    jUsersWorkerStatus.init(<?= func::php2js(array(
                        'lang' => array(
                            'left'    => _t('users','[symbols] осталось'),
                            'symbols' => explode(';', _t('users', 'знак;знака;знаков')),
                        ),
                    )) ?>);
                <? js::stop() ?>
                </script>
            <? endif; else: ?>
            <? endif; ?>
        </div>
        <? if($userID == $isWorker): ?>
            <div class="user-sidebar__conditions">
            <? if($rolePrivate && Users::profileBirthdate() && $birthdate != '1901-01-01' && $birthdate != '0000-00-00'): ?>
                <div class="user-sidebar__conditions-item">
                    <div class="bold">
                        <?= tpl::date_format_spent($birthdate, false, false) ?>
                    </div>
                    <span class="user-sidebar__text">
                        <?= _t('','Возраст')?>
                    </span>
                </div>
            <? endif; ?>
            <? if($isWorker || ! Users::useClient()): $aExperience = Users::aExperience() ?>
                <div class="user-sidebar__conditions-item">
                <div class="bold">
                    <?= $aExperience[$experience]['t'] ?>
                </div>
                <span class="user-sidebar__text">
                    <?= _t('','Опыт')?>
                </span>
            </div>
            <? endif; ?>
            <div class="user-sidebar__conditions-item">
                <? if (!empty($dynprops_simple)): # краткий вывод динсвойствa ?>
                    <? foreach ($dynprops_simple as $dp_item): ?>
                        <? if ($dp_item['cache_key'] == Users::DP_KEY_QUANTITY_CHILDREN ): ?>
                                <? if($dp_item['value'] !== ''):?>
                                    <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                    <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                        <? foreach ($dp_item['multi'] as $val): ?>
                                            <? if ($dp_item['value'] == $val['value']): ?>
                                                <div class="bold"><?= $val['name'] ?></div>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    <? else: ?>
                                        <div class="bold">
                                            <? foreach ($dp_item['value'] as $dp_val): ?>
                                                <? foreach ($dp_item['multi'] as $val): ?>
                                                    <? if ($dp_val == $val['value']): ?>
                                                        <?= $val['name'] ?> <br>
                                                    <? endif; ?>
                                                <? endforeach; ?>
                                            <? endforeach; ?>
                                        </div>
                                    <? endif; ?>
                                <? else: ?>
                                    <div class="bold">
                                        <?= _t('dp_user','не указано') ?>
                                    </div>
                                <? endif;?>
                                <span class="user-sidebar__text">
                                    <?= !empty($dp_item['description'])? $dp_item['description'] : _t('dp_user_qc', 'К-во детей'); ?>
                                </span>
                        <? endif; ?>
                    <? endforeach; ?>
                <? endif; ?>
            </div>
        </div>
        <?  endif; ?>
        <? if($aMainSpec): ?>
            <div class="user-sidebar__price mrgb10">
                <? if( ! empty($aMainSpec['price'])): ?>
                    <div class="bold">
                        <?= tpl::formatPrice($aMainSpec['price']) ?>
                        <?= Site::currencyData($aMainSpec['price_curr'], 'title_short'); ?>
                        <?= ! empty($aMainSpec['price_rate_text'][LNG]) ? '/'.$aMainSpec['price_rate_text'][LNG] : '' ?>
                    </div>
                <? endif; ?>
                <? if( ! empty($aMainSpec['budget'])): ?>
                    <div>
                        <?= _t('users', 'Бюджет от'); ?>
                        <?= tpl::formatPrice($aMainSpec['budget']) ?>
                        <?= Site::currencyData($aMainSpec['budget_curr'], 'title_short'); ?>
                    </div>
                <? endif; ?>
            </div>
        <? endif; ?>

        <div class="text-center mrgb10 s-rating-white">
            <div>
                <?= Rating::getViewTotalAverage($id, 'partial.total.average') ?>
            </div>
            <div class="o-feedbacks-inf">
                <span id="j-user-opinions-cache">
                    <?= tpl::opinions($opinions_cache, array('login' => $login)) ?>
                </span>
            </div>
        </div>

        <div class="mrgt10" id="profile-collapse">

            <? if( ! empty($contacts)): # контакты и кнопка их открытия
                $open_user_id = $aData['open_user_id'] = $id;
                $open_user_type = $aData['open_user_type'] = $type;
                $is_profile = Site::i()->isProfilePage();
                $bOpenedContacts = Users::isOpenedContacts($open_user_id);
                # показывать кнопку в чужом профиле для противоположного типа пользователя
                $bShowContactsBtn = !$isOwner && !$bOpenedContacts && ($aCurrentUserData['type'] != $open_user_type);
                if( ! User::id() || $bShowContactsBtn):?>
                    <div class=" text-center mrgb10 j-show-btn-block ">
                        <button onclick="<?= ( ! User::id())? '$(\'.j-show-btn\').hide();$(\'#j-login-link\').toggle();' :
                            "Custom.open_contacts({$open_user_id},{$is_profile})"?>"
                                class="btn btn-primary btn-block j-show-btn">
                            <?=_t('contacts', 'Купить контакты')?>
                        </button>
                        <a style="display: none"
                           href="<?=Users::url('login')?>"
                           class="btn btn-primary btn-block"
                           id="j-login-link">
                            <?=_t('contacts', 'Авторизируйтесь')?>
                        </a>
                    </div>
                <? endif;?>
                <? if($inviteEnabled): ?>
                <div class="text-center mrgb10">
                    <a href="#" class="link-bold link-bold_fz15 j-order-invite" data-toggle="modal">
                        <?= _t('orders', 'Предложить работу'); ?>
                    </a>
                </div>
                <? endif; ?>
                <? if($isOwner || $bOpenedContacts):?>
                    <?= $this->viewPHP($aData, 'contacts.block');?>
                <? else:?>
                    <div class="j-contacts-block"></div>
                <? endif;?>
            <? endif; ?>

            <? if( ! empty($tags)): ?>
                <div class="l-inside">
                    <h6><?= _t('', 'Навыки'); ?></h6>
                    <div class="p-profile-info-list">
                        <? foreach($tags as $v): ?>
                            <a href="<?= Users::url('search-tag', $v) ?>" class="l-tag"><?= $v['tag'] ?></a>
                        <? endforeach; ?>
                    </div>
                </div>
            <? endif; ?>

        </div>
        <? if( ! Users::useClient() || (Users::useClient() && User::isClient() && $userID == $isOwner)): ?>
            <a href="<?= Orders::url('add')?>" class="btn btn-primary btn-block mrgt10 mrgb10">
                <?= _t('orders', 'Добавить объявление'); ?>
            </a>
        <? endif; ?>

        <? if($isOwner): ?>
            <div class="text-center">
                <a href="<?= Users::url('my.settings')?>" class="link-bold link-bold_fz15">
                    <?= _t('','Редактировать профиль')?>
                </a>
            </div>
        <? endif; ?>
    </div>
</div>

<?= $this->note($id, array('bFav' => 1, 'classes' => 'mrgt10 mrgb20')); ?>
<? if(DEVICE_DESKTOP): ?>
    <? if(Users::useClient() && !User::isWorker() && User::id()): ?>
        <?= Users::getTopPositionsList($id); ?>
    <? endif; ?>
<? endif; ?>
<script type="text/javascript">
<? js::start() ?>
<?  if($bShowStat): ?>
jUserViews.init(<?= func::php2js(array(
    'lang' => array(
        'ava_upload_messages' => array(
            'typeError' => _t('form', 'Допустимы только следующие типы файлов: {extensions}'),
            'sizeError' => _t('form', 'Файл {file} слишком большой, максимально допустимый размер {sizeLimit}'),
            'minSizeError' => _t('form', 'Файл {file} имеет некорректный размер'),
            'emptyError' => _t('form', 'Файл {file} имеет некорректный размер'),
            'onLeave' => _t('form', 'Происходит загрузка изображения, если вы покинете эту страницу, загрузка будет прекращена'),
        ),
        'ava_upload' => _t('users', 'Загрузка фотографии'),
    ),
    'id' => $id,
    //avatar
    'avatarMaxsize' => $avatar_maxsize,
    'avatarSzSmall' => UsersAvatar::szSmall,
    'avatarSzNormal' => UsersAvatar::szNormal,
    'avatarSzBig' => UsersAvatar::szBig,
    'avatarUploadProgress'  => '<div class="avatar-load__loader p-img-loading j-progress"> <img alt="" src="'.bff::url('/img/loader.gif').'"> </div>',
)) ?>);
<?  endif;
if ($inviteEnabled):
        $aTerms = Orders::aTerms(); $aTermsText = array();
        foreach ($aTerms as $v) {
            if ($v['days']) {
                $aTermsText[ $v['id'] ] = tpl::date_format2(time() + $v['days'] * 24*60*60, false, true);
            } else {
                $aTermsText[ $v['id'] ] = false;
            }
        }
    ?>
    jOrdersInvite.init(<?= func::php2js(array(
        'lang' => array(
            'spec_wrong'     => _t('orders', 'Укажите специализацию'),
            'invite_success' => _t('orders', 'Предложение было успешно отправлено'),
        ),
        'user_id' => $id,
        'terms' => $aTermsText,
    )) ?>);
<? endif; ?>

(function(){
    $(function(){
        window.ratingViewTotalAverage = new Rating();
        window.ratingViewTotalAverage.initViewTotalAverage(
            <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
        );
    });
})();
<? js::stop() ?>
</script>
