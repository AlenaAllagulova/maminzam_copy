<?php
/**
 * Регистрация пользователя
 * @var $this Users
 */
tpl::includeJS('users.auth', false, 5);
$rolesEnabled = Users::rolesEnabled();
$registerPhone = Users::registerPhone();
$showPassword = true;
$showType = true;
$forceRole = false;
$md5 = '-5';
$md7 = '-7';
$isEmbedded = ! empty($embedded);
if ($isEmbedded) {
    $md5 = '-3';
    $md7 = '-4';
    $forceRole = isset($embedded['force_role']);
    $showPassword = empty($embedded['generate_password']);
    if ( ! $showPassword) {
        $pass_confirm_on = false;
    }
    if ( ! empty($embedded['no_captcha'])) {
        $captcha_on = false;
    }
    $showType = ! isset($embedded['force_type']);
}
?>
<? if( ! $isEmbedded): ?>
<div class="s-signBlock-form">
    <div class="row">
        <div class="form-sm pdt0 mrgb30">

            <div class="form-sm__double mrgb20">
                <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_CLIENT]); ?>" class="form-sm__double-btn <?= $user_type == Users::TYPE_WORKER ? '' : 'active';?>">
                    <?= _t('register','Я родитель')?>
                </a>
                <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_WORKER])?>" class="form-sm__double-btn <?= $user_type == Users::TYPE_WORKER ? 'active' : '';?>">
                    <?= _t('register','Я маминзам')?>
                </a>
            </div>

            <? if($showType): ?>
            <div class="form-sm__caption mrgb20">
                <? if($user_type == Users::TYPE_WORKER): ?>
                    <?= _t('','Зарегистрируйтесь, чтобы найти работу')?>
                <? else: ?>
                    <?= _t('','Зарегистрируйтесь, чтобы найти идеальную няню')?>
                <? endif; ?>
            </div>
            <? endif; ?>

            <? if(isset($providers)): ?>
                <div class="social-buttons mrgb25">
                    <? foreach($providers as $v) { ?>
                        <a href="#" class="social-buttons__items social-buttons_<?= $v['class'] ?> j-u-login-social-btn" data="{provider:'<?= $v['key'] ?>',w:<?= $v['w'] ?>,h:<?= $v['h'] ?>}"></a>
                    <? } ?>
                </div>
            <? endif; ?>

            <div class="or-text mrgb25">
                <span>
                    <?= _t('','Или')?>
                </span>
            </div>

            <button class="j-open-reg-form btn btn-primary btn-primary_empty btn-block " >
               <?= _t('','Электронная почта')?>
            </button>

            <div class="collapse j-reg-form">
                <form action="" class="g-form" role="form" id="j-u-register-form">
                    <input type="hidden" name="back" value="<?= HTML::escape($back) ?>" />
                    <? endif; # ==begin embedded block ?>
                    <? if($rolesEnabled): ?>
                        <? if( ! $forceRole): ?>
                        <div class="form-group">
                            <label class=" control-label"><?= _t('users', 'Тип аккаунта'); ?></label>
                            <div class="">
                                <div class="btn-group" data-toggle="buttons">
                                    <? foreach(Users::roles() as $v): ?>
                                        <label class="btn btn-default <?= $v['default'] ? 'active' : '' ?>">
                                            <input type="radio" name="role_id" value="<?= $v['id'] ?>" <?= $v['default'] ? 'checked="checked"' : '' ?>> <?= $v['t'] ?>
                                        </label>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <? endif; ?>
                    <? endif; ?>
                    <div class="form-group">
                        <label for="inputFname" class="control-label j-name">
                            <?= _t('users', 'Имя'); ?>
                        </label>
                        <div class="">
                            <input type="text" name="name" class="form-control j-required" placeholder="<?= _t('', 'Ваше имя'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputFname" class="control-label j-surname">
                            <?= _t('users', 'Фамилия'); ?>
                        </label>
                        <div class="">
                            <input type="text" name="surname" class="form-control j-required" placeholder="<?= _t('', 'Ваша фамилия'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="j-u-register-email" class=" control-label">
                            <?= _t('users', 'Электронная почта') ?>
                        </label>
                        <div class="">
                            <input type="email" name="email" class="form-control j-required" id="j-u-register-email" autocomplete="off" placeholder="<?= _t('users', 'Введите ваш email') ?>" maxlength="100" />
                        </div>
                    </div>
                    <? if(Users::profilePhoneRequired()): ?>
                        <div class="form-group">
                            <label for="j-u-register-phone" class="control-label">
                                <?= _t('users', 'Телефон') ?>
                            </label>
                            <div class="col-md<?= $md7 ?>">
                                <input type="text" name="phone" class="form-control j-required" id="j-u-register-phone" autocomplete="off" placeholder="<?= _t('users', 'Введите ваш телефон') ?>" maxlength="16" />
                            </div>
                        </div>
                    <? endif; ?>
                    <? if($phone_on): ?>
                        <div class="form-group">
                            <label for="inputPhone" class="control-label">
                                <?= _t('users', 'Телефон') ?>
                            </label>
                            <div class="">
                                <?= $this->registerPhoneInput(array('id'=>'j-u-register-phone','name'=>'phone')) ?>
                            </div>
                        </div>
                    <? else: ?>
                        <div class="form-group">
                            <label for="j-u-register-contact-phone" class="control-label">
                                <?= _t('users', 'Телефон для связи') ?>
                            </label>
                            <div class="">
                                <input
                                        type="text"
                                        name="contact_phone"
                                        class="form-control j-required"
                                        id="j-u-register-contact-phone"
                                        placeholder="<?= _t('users', 'Введите контактный телефон') ?>"
                                        maxlength="30"
                                        required
                                />
                            </div>
                        </div>
                    <? endif; ?>
                    <? if($showPassword): ?>
                    <div class="form-group">
                        <label for="j-u-register-pass" class=" control-label">
                            <?= _t('users', 'Пароль') ?>
                        </label>
                        <div class="">
                            <input type="password" name="pass" class="form-control j-required" id="j-u-register-pass" autocomplete="off" placeholder="<?= _t('users', 'Введите ваш пароль') ?>" maxlength="100" />
                        </div>
                    </div>
                    <? if($pass_confirm_on): ?>
                        <div class="form-group">
                            <label for="j-u-register-pass2" class="control-label">
                                <?= _t('users', 'Пароль еще раз') ?>
                            </label>
                            <div class="">
                                <input type="password" name="pass2" class="form-control j-required" id="j-u-register-pass2" autocomplete="off" placeholder="<?= _t('users', 'Повторите пароль') ?>" maxlength="100" />
                            </div>
                        </div>
                    <? endif;
                    endif;
                    if($captcha_on): ?>
                        <div class="form-group">
                            <label for="j-u-register-captcha" class="control-label">
                                <?= _t('users', 'Результат с картинки') ?>
                            </label>
                            <div class="flex">
                                <div class="">
                                    <input type="text" name="captcha" id="j-u-register-captcha" autocomplete="off" class="form-control j-required" value="" />
                                </div>
                                <div class="">
                                    <div class="s-captcha">
                                        <img src="<?= tpl::captchaURL() ?>" class="j-captcha" onclick="$(this).attr('src', '<?= tpl::captchaURL() ?>&rnd='+Math.random())" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endif; ?>

                    <? if($showType): ?>
                        <? $usersTypes = Users::aTypes();?>
                        <? if (sizeof($usersTypes) > 1): ?>
                            <div class="hidden ">
                                <div class="">
                                    <? foreach($usersTypes as $v):?>
                                        <div class="radio-inline">
                                            <label>
                                                <input type="radio"
                                                       name="type"
                                                       value="<?= $v['id'] ?>"
                                                       <?=(!empty($user_type) && $user_type == $v['id']) ? 'checked="checked"' : '' ?>
                                                       autocomplete="off" />
                                                <?= $v['t'] ?>
                                            </label>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        <? else: ?>
                            <input type="radio" name="type" value="<?= key($usersTypes) ?>" checked="checked" style="display: none;" />
                        <? endif;?>
                    <? endif; ?>

                    <div class="form-group mrgt20 mrgb0">
                        <button type="submit" class="btn btn-primary btn-block j-submit">
                            <?= _t('users', 'Зарегистрироваться') ?>
                        </button>
                    </div>

                    <? if($agreement_on): ?>
                    <div class="form-group hidden">
                        <div class="checkbox checkbox-none">
                            <label>
                                <input type="checkbox" checked name="agreement" id="j-u-register-agreement" autocomplete="off" />
                                <?= _t('users', 'Регистрируясь, Вы соглашаетесь с
                                    <a href="[link_agreement]" target="_blank">правилами использования сервиса,</a>
                                    а также с передачей и обработкой Ваших данных.',
                                    array('link_agreement'=>Users::url('agreement'))) ?>
                                <i class="text-danger">*</i>
                            </label>
                        </div>
                    </div>
                    <? endif; ?>

                    <? if( ! $isEmbedded): # ==end embedded block ?>
                </form>
            </div>

            <div class="checkbox checkbox-none mrgt20">
                <label>
                    <input type="checkbox" checked name="agreement" id="j-u-register-agreement" autocomplete="off" />
                    <?= _t('users', 'Регистрируясь, Вы соглашаетесь с
                                <a href="[link_agreement]" target="_blank">правилами использования сервиса,</a>
                                а также с передачей и обработкой Ваших данных.',
                        array('link_agreement'=>Users::url('agreement'))) ?>
                </label>
            </div>

            <div class="secondary-text flex flex_center flex_jcc text-center mrgt25">
                <h6 class="mrgr5"><?= _t('users', 'Уже зарегистрированы?') ?></h6>
                <a class="link-bold" style="margin-top: 3px;" href="<?= Users::url('login') ?>"><?= _t('users', 'Войти') ?></a>
            </div>

        </div>

    </div>


</div>
<? endif; ?>
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    jUserAuth.register(<?= func::php2js(array(
        'embedded' => $isEmbedded,
        'embedded_form' => ($isEmbedded ? $embedded['form'] : ''),
        'captcha' => $captcha_on,
        'agreement' => $agreement_on,
        'pass_confirm' => $pass_confirm_on,
        'phone' => $phone_on,
        'login_social_url' => Users::url('login.social'),
        'login_social_return' => ! empty($back) ? $back : '',
        'lang' => array(
            'email' => _t('users', 'E-mail адрес указан некорректно'),
            'pass' => _t('users', 'Укажите пароль'),
            'pass2' => _t('users', 'Пароли должны совпадать'),
            'captcha' => _t('users', 'Введите результат с картинки'),
            'agreement' => _t('users', 'Пожалуйста подтвердите, что Вы согласны с пользовательским соглашением'),
            'type' => _t('users', 'Пожалуйста укажите тип пользователя'),
            'phone' => _t('users', 'Номер телефона указан некорректно'),
            'name'  => array(
                'r'.Users::ROLE_PRIVATE => array('n' => _t('users', 'Имя'),      'p' => _t('', 'Ваше имя')),
                'r'.Users::ROLE_COMPANY => array('n' => _t('users', 'Компания'), 'p' => _t('', 'Название вашей компании')),
            ),
        ),
    )) ?>);
});
<? js::stop(); ?>
</script>