<?php
tpl::includeJS('users.auth', false, 5);
?>
    <div class="s-signBlock-form">
        <div class="form-sm">
            <div class="form-sm__caption mrgb10">
                <?= _t('','Подтвердите номер телефона') ?>
            </div>
            <p>
                <?= _t('users', 'На номер [phone] отправлен код подтверждения. На этом регистрация будет завершена.
                    ', array(
                    'phone' => '<strong>+'.$phone.'</strong>',
                ));
                ?>
            </p>
            <div id="j-u-register-phone-block-code">
                <form class="g-form mrgb15" role="form" action="">
                    <div class="form-group mrgb20">
                        <label for="inputCode" class="control-label">
                            <?= _t('users', 'Код подтверждения') ?>
                        </label>
                        <div class="">
                            <input type="text" class="form-control j-u-register-phone-code-input" id="inputCode" placeholder="<?= _t('users', 'Введите код из смс'); ?>">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-block btn-primary j-u-register-phone-code-validate-btn">
                        <?= _t('users', 'Подтвердить') ?>
                    </button>
                </form>
                <div class="mrgb10 text-center">
                    <a href="#" class=" j-u-register-phone-change-step1-btn">
                        <span>
                            <?= _t('users', 'Изменить номер телефона'); ?>
                        </span>
                    </a>
                </div>
                <div class=" text-center">
                    <a href="#" class=" j-u-register-phone-code-resend-btn">
                        <span>
                            <?= _t('users', 'Выслать код повторно'); ?>
                        </span>
                    </a>
                </div>
            </div>

            <div id="j-u-register-phone-block-phone" class="displaynone">
                <form class="g-form mrgb15" role="form" action="">
                    <div class="form-group mrgb10">
                        <label for="j-u-register-phone"><?= _t('users', 'Номер телефона') ?></label>
                        <?= $this->registerPhoneInput(array('name'=>'phone', 'id'=>'j-u-register-phone-input')) ?>
                    </div>
                    <button type="button" class="btn btn-block btn-primary j-u-register-phone-change-step2-btn">
                        <?= _t('users', 'Выслать код') ?>
                    </button>
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jUserAuth.registerPhone(<?= func::php2js(array(
            'lang' => array(
                'resend_success' => _t('users', 'Код подтверждения был успешно отправлен повторно'),
                'change_success' => _t('users', 'Код подтверждения был отправлен на указанный вами номер'),
            ),
            'url' => (isset($url) ? $url : ''),
        )) ?>);
    });
    <? js::stop(); ?>
</script>