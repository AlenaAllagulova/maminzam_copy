<?php
tpl::includeCSS(array('owl.carousel'), true);
tpl::includeJS(array('owl.carousel.min'), false);
tpl::includeJS(['dist/rating']);
$ordersPlus = Site::indexView(Site::INDEX_VIEW_ORDERS_PLUS);
?>
    <? if($ordersPlus): ?>
    <div class="l-carousel-container in-carousel">
		<div class="container">
    <? else: ?>
    <div class="container mrgt30">
		<div class="l-carousel-container">
    <? endif; ?>
            <div id="j-carousel" class="owl-carousel l-carousel l-freelancers-carousel">
                <? foreach($list as $v): ?>
                <div>
                    <div class="media l-freelancers-carousel-item">
                        <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="pull-left l-freelancers-carousel__avatar">
                            <?= tpl::userAvatar($v) ?>
                        </a>
                        <div class="media-body">
                            <div class="rating">
                                <?= Rating::getViewTotalAverage($v['user_id'], 'partial.total.average') ?>
                            </div>
                            <div class="l-freelancers-carousel__text">
                                <?= isset($v['spec_data']['title'])?$v['spec_data']['title']:''?>
                            </div>
                            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="l-freelancers-carousel__name">
                                <?= $v['name'] ?>
                                <? if(!empty($v['surname'])): ?>
                                    <?= mb_strimwidth($v['surname'], 0, 2, "."); ?>
                                <? endif; ?>
                            </a>
                            <div class="l-freelancers-carousel__text l-freelancers-carousel__text_second">
                                <?= isset($v['region_data']['title'])? $v['region_data']['title'].', ':''?>
                                <?= $v['district_title']?>
                                <br>
                                <?= $v['addr_addr'];?>
                            </div>
                        </div>
                    </div>
                </div>
                <? endforeach; ?>
            </div>
            <? if(User::id() && (Users::useClient() && User::isWorker() || ! Users::useClient())): ?>
        	<a href="<?= Svc::url('view', array('keyword' => $svc['keyword'])) ?>" class="l-carousel-how_link"><small><?= _t('svc', 'Как сюда попасть?'); ?></small></a>
            <? endif; ?>
        </div>
    </div>
<? if( ! empty($list)): ?>
<script type="text/javascript">
    <? js::start() ?>
    $(function(){
        var $carousel = $('#j-carousel');
        if($carousel.length){
            $carousel.owlCarousel({
                items : 4, //10 items above 1000px browser width
                    itemsDesktop : [991,3], //5 items between 1000px and 901px
                    itemsDesktopSmall : [768,3], // betweem 900px and 601px
                    itemsTablet: [600,1], //2 items between 600 and 0
                    itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                    navigation : true,
                    navigationText : ["<i class=' icon-arrow-point-to-left'></i>", "<i class=' icon-arrow-point-to-right '></i>"],
                    pagination : true,
                    autoPlay : true
            });
        }

    });
    (function(){
        $(function(){
            window.ratingViewTotalAverage = new Rating();
            window.ratingViewTotalAverage.initViewTotalAverage(
                <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
            );
        });
    })();
    <? js::stop() ?>
</script>
<? endif; ?>