<?php

class Theme_Custom_Users_Adm extends Theme_Custom_Users_Adm_Base
{

    public function settings()
    {
        if (!$this->haveAccessTo('settings')) {
            return $this->showAccessDenied();
        }

        $aLang = array(
            'verified_requirement' => TYPE_STR,
        );

        if (Request::isAJAX()) {
            $this->input->postm(array(
                'specialization_limit'     => TYPE_UINT,
                'specialization_limit_pro' => TYPE_UINT,
                'contacts_worker_display'  => TYPE_UINT,
                'contacts_worker_view'     => TYPE_UINT,
                'contacts_client_display'  => TYPE_UINT,
                'contacts_client_view'     => TYPE_UINT,
                'opinions_delay'           => TYPE_UINT,
                'pro_contacts_cnt'         => TYPE_UINT,
                'worker_contact_amount'    => TYPE_UINT,
                'client_contact_amount'    => TYPE_UINT,
            ), $aData);

            $aData['specialization_limit'];
            $aData['specialization_limit_pro'];
            if($aData['specialization_limit_pro'] < $aData['specialization_limit']){
                $this->errors->set(_t('users', 'Кол-во специализаций для PRO не может быть меньше чем у обычного аккаунта'));
                $this->ajaxResponseForm();
            }

            $aDisplay = array(static::CONTACTS_DISPLAY_ALL, static::CONTACTS_DISPLAY_PRO);
            if( ! in_array($aData['contacts_worker_display'], $aDisplay)){
                $aData['contacts_worker_display'] = reset($aDisplay);
            }
            if( ! in_array($aData['contacts_client_display'], $aDisplay)){
                $aData['contacts_client_display'] = reset($aDisplay);
            }

            $aView = array(static::CONTACTS_VIEW_ALL, static::CONTACTS_VIEW_AUTH, static::CONTACTS_VIEW_PRO);
            if( ! in_array($aData['contacts_worker_view'], $aView)){
                $aData['contacts_worker_view'] = reset($aView);
            }
            if( ! in_array($aData['contacts_client_view'], $aView)){
                $aData['contacts_client_view'] = reset($aView);
            }

            $this->input->postm_lang($aLang, $aData);
            $this->db->langFieldsModify($aData, $aLang, $aData);

            $this->configSave($aData);
            $this->ajaxResponseForm();
        }

        $aData = $this->configLoad(array(
            'specialization_limit_pro' => static::specializationsLimit(true),
            'specialization_limit'     => static::specializationsLimit(false),
            'contacts_worker_display'  => static::CONTACTS_DISPLAY_ALL,
            'contacts_worker_view'     => static::CONTACTS_VIEW_ALL,
            'contacts_client_display'  => static::CONTACTS_DISPLAY_ALL,
            'contacts_client_view'     => static::CONTACTS_VIEW_ALL,
        ));
        if( ! is_array($aData)) $aData = array();

        return $this->viewPHP($aData, 'admin.settings');
    }
}

