<?php
    tpl::includeJS('bills.my', false, 2);
    $urlPay = Bills::url('my.pay');
?>
        <div class="container">

            <section class="l-mainContent">

                <div class="row">

                    <div class="col-md-12">

                        <?= tpl::getBreadcrumbs($breadcrumbs); ?>

                        <form method="post" action="<?= $urlPay ?>" id="j-my-pay-form">

                        	<div class="text-center">
                                <h1><?=_t('','Пополнить счет')?></h1>
                                <p class="mrgt30"><?= _t('bills', 'На какую сумму вы хотите пополнить счет?'); ?></p>
                                <div class="">
                                    <input type="text" name="amount" autocomplete="off" class="form-control w200i m0-a" placeholder="<?= _t('bills', 'Сумма пополнения'); ?>" value="<?= $amount ?>">
                                </div>

                            </div>
                            <h6 class="mrgt40 text-center"><?= _t('bills', 'Выберите способ оплаты'); ?></h6>

                            <?= $payMethodsList ?>

                            <div class="c-formSubmit mrgt20 text-center">
                                <button class="btn btn-primary c-formSuccess j-submit min-w-200px">
                                    <?= _t('bills', 'Пополнить'); ?>
                                </button>
                            </div>

                        </form>

					</div>

                </div>
            </section>
        </div>
    <script type="text/javascript">
        <? js::start(); ?>
        $(function(){
            jBillsMyPay.init(<?= func::php2js(array(
                'lang'       => array(),
                'url_submit' => $urlPay,
                'url_back'   => Bills::url('my.history'),
            )) ?>);
        });
        <? js::stop(); ?>
    </script>