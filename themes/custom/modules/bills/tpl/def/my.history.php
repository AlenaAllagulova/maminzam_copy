<?php
    tpl::includeJS('bills.my', false, 2);
    tpl::includeCSS('../libs/bootstrap-datepicker-master/bootstrap-datepicker');
    tpl::includeJS('../libs/bootstrap-datepicker-master/bootstrap-datepicker.min');
?>
<div class="container">

    <section class="l-mainContent mrgt50">

        <div class="row">
            <div class="col-md-12" id="j-bills-history">
                <div class="text-center">
                    <h1><?= _t('','Ваш счет') ?></h1>
                    <div class="">
                        <?= _t('bills', 'На вашем счету:'); ?>
                        <strong class="bold"><?= $balance ?> <?= Site::currencyDefault(); ?></strong>
                    </div>
                    <div class="mrgt20 w200 m0-a">
                        <a href="<?= Bills::url('my.pay') ?>" class="btn btn-block btn-primary">
                            <?= _t('bills', 'Пополнить счет'); ?>
                        </a>
                    </div>
                </div>

                <div class="text-center mrgt40">
                    <h3><?= _t('','История операций')?></h3>
                </div>
                <form class="text-right form-inline mrgt10 mrgb10" role="form" method="get" action="" id="j-bills-history-form">
                    <input type="hidden" name="page" value="<?= $f['page'] ?>" />
                    <div class="form-group">
                        <input type="text" name="fr" class="form-control input-sm j-datepicker" placeholder="<?= _t('', 'дд-мм-гггг'); ?>" value="<?= $f['fr'] ?>" autocomplete="off" />
                    </div>
                    <span class="hidden-xs">-</span>
                    <div class="form-group">
                        <input type="text" name="to" class="form-control input-sm j-datepicker" placeholder="<?= _t('', 'дд-мм-гггг'); ?>" value="<?= $f['to'] ?>" autocomplete="off" />
                    </div>
                    <button type="submit" class="btn-block-sm btn btn-sm btn-clear-filter j-submit">
                        <?= _t('bills', 'Показать'); ?>
                    </button>
                </form>
                <div class="j-list"><?= $list ?></div>
                <div class="j-pagination"><?= $pgn ?></div>

            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        !function(a){a.fn.datepicker.dates.ru= {
            days: ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],
            daysShort:["Вск","Пнд","Втр","Срд","Чтв","Птн","Суб"],
            daysMin:["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
            months:["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],
            monthsShort:["Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек"],
            today:"Сегодня",
            clear:"Очистить",
            format:"dd.mm.yyyy",
            weekStart:1,
            monthsTitle:"Месяцы"}
        }(jQuery);
        $('.j-datepicker').datepicker({
            format: 'dd-mm-yyyy',
            language: 'ru',
            orientation: 'bottom auto',
            todayHighlight: true,
            leftArrow: '<i class="icon-arrow-point-to-left"></i>',
            rightArrow: '<i class="icon-arrow-point-to-right"></i>'
        });


        jBillsMyHistory.init(<?= func::php2js(array(
            'lang' => array(),
            'ajax' => true,
        )) ?>);
    });
    <? js::stop(); ?>
</script>