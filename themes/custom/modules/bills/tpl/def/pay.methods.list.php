<?php
tpl::includeJS('bills.my', false, 2);
?>
<div id="j-bills-select-pay-system">
    <div class="services-list">

        <? $i = 1; foreach($psystems as $k => $v): ?>
            <div class="services-list__item-label ">
                <label class="j-ps-item w100p h100p <?= $i == 1 ? ' active' : '' ?>">
                    <input type="radio" name="ps" value="<?= $k ?>" class="j-ps-radio" <?= $i == 1 ? 'checked="checked"' : '' ?> >
                    <span class="services-list__label">
                        <span class="services-list__title">
                            <?= $v['title'] ?>
                        </span>
                        <span class="services-list__img">
                            <img src="<?= $v['logo_desktop'] ?>" alt="<?= HTML::escape($v['title']); ?>" />
                        </span>
                    </span>
                </label>
            </div>
            <? $i++; ?>
        <? endforeach; ?>

    </div>
</div>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jBillsSelectPay.init(<?= func::php2js(array(
            'lang' => array(),
        )) ?>);
    });
    <? js::stop(); ?>
</script>