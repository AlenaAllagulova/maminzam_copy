<?php
$langDescription = array(
    Bills::TYPE_IN_PAY => _t('bills', 'Пополнение счета'),
    Bills::TYPE_IN_GIFT => _t('bills', 'Пополнение счета'),
    Bills::TYPE_OUT_SERVICE => _t('bills', 'Оплата усуги'),
);
$curr = Site::currencyDefault();
if( ! empty($list)): ?>
    <table class="table table-hover p-bill-table">
        <thead>
        <tr>
            <th style="max-width: 100px" class="hidden-xs"><?= _t('','Дата')?></th>
            <th><?= _t('','Описание')?></th>
            <th style="min-width: 110px;" class="hidden-xs text-center"><?= _t('','№ операции')?></th>
            <th style="min-width: 80px;" class="text-right"><?= _t('','Сумма')?></th>
        </tr>
        </thead>
        <tbody>
        <?  $dateLast = 0;
            foreach($list as $v):
                $in = ($v['type'] != Bills::TYPE_OUT_SERVICE);?>
                <tr>
                    <td class="hidden-xs"><?= tpl::date_format2($v['created_date']) ?></td>
                    <td>
                        <?= $v['description'] ?>
                    </td>
                    <td class="hidden-xs text-center"><?= $v['id'] ?></td>
                    <td class="text-right"><?= ( ! $in ? '- ' : '').$v['amount'] ?> <?= $curr ?></td>
                </tr>
        <? endforeach; ?>
        </tbody>
    </table>
<? else: ?>
    <div class="text-center"><?= _t('bills', 'Список операций по счету пустой'); ?></div>
<? endif; ?>

