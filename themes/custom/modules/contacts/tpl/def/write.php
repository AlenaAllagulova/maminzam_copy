<?php
    $captchaUrl = tpl::captchaURL();
?>

    <div class="container">
        <section class="l-mainContent">
            <?= tpl::getBreadcrumbs($breadCrumbs); ?>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 form-def">
                    <div class="text-center mrgb20">
                        <h2>
                            <?= ( ! empty($titleh1) ? $titleh1 : _t('contacts', 'Задать вопрос') ); ?>
                        </h2>
                    </div>
                    <div class="form-def__box">

                        <form class="" role="form" id="contactForm" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="subject" class="control-label">
                                    <?= _t('contacts', 'Тип вопроса'); ?>
                                    <i class="text-danger">*</i>
                                </label>

                                <div class="select-custom_global form-def__select">
                                    <select name="ctype" class="form-control j-required j-select" id="subject"><?= $aData['ctype_options'] ?></select>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="message" class=" control-label"><?= _t('contacts', 'Вопрос'); ?> <i class="text-danger">*</i></label>
                                <div class="">
                                    <textarea name="message" class="form-control j-required" id="message" rows="5" placeholder="<?= _t('contacts', 'Подробно опишите свой вопрос'); ?>"></textarea>
                                </div>
                            </div>

                            <? if(Contacts::attachmentsEnabled()): ?>
                            <div class="form-group">
                                <label for="inputSpec" class="control-label"><?= _t('contacts', 'Прикрепить файл'); ?></label>
                                <div class="">
                                    <input type="hidden" name="MAX_FILE_SIZE" value="<?= $this->attach()->getMaxSize() ?>" />
                                    <input type="file" name="attach" class="j-upload-file hidden" />
                                    <ul class="p-addProject_files j-file-name hidden">
                                        <li>
                                            <a href="#" class="ajax-link j-upload-delete">
                                                <i class="fa fa-times"></i>
                                            </a>
                                            <a href="javascript:;" class="j-name">
                                            </a>
                                        </li>
                                    </ul>
                                    <button type="button" class="link-bold j-upload-file-btn">
                                        <?= _t('contacts', 'Загрузить файл с компьютера'); ?>
                                    </button>
                                </div>
                            </div>
                            <? endif; ?>

                            <div class="form-group">
                                <label for="email" class=" control-label"><?= _t('contacts', 'Email'); ?> <i class="text-danger">*</i></label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control j-required" name="email" id="email" value="<?= $aData['user']['email'] ?>" maxlength="70" placeholder="<?= _t('contacts', 'Ваш Email для ответа'); ?>" />
                                    </div>
                                </div>
                            </div>

                            <? if( ! User::id() ): ?>
                                <div class="form-group">
                                    <label for="captcha" class="control-label"><?= _t('contacts', 'Защита'); ?> <i class="text-danger">*</i></label>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <input type="text" name="captcha" class="form-control j-required" id="captcha" placeholder="<?= _t('contacts', 'Результат с картинки'); ?>"/>
                                        </div>
                                        <div class="col-sm-3 mrgt5">
                                            <img src="<?= $captchaUrl ?>" style="cursor: pointer;" onclick="jContactForm.refreshCaptha();" id="captchaCode"  />
                                        </div>
                                    </div>

                                </div>
                            <? endif; ?>

                            <div class="form-group row">
                                <div class="col-sm-9 col-md-offset-3 c-formSubmit mrgt35">
                                    <button class="btn btn-primary c-formSuccess j-submit"><?= _t('contacts', 'Задать вопрос'); ?></button>
                                    <a class="c-formCancel j-cancel" href="#"><?= _t('form', 'Отмена'); ?></a>
                                </div>
                            </div>

                        </form>

                    </div>

                </div>
            </div>

        </section>

    </div>


<script type="text/javascript">
<? js::start() ?>
var jContactForm = (function(){
    var $form, f, captchaUrl = '<?= $captchaUrl ?>';
    var lng = {
        alert_title: '<?= _t('contacts','Отправка сообщения') ?>',
        email_wrong: '<?= _t('contacts', 'E-mail адрес указан некорректно') ?>',
        no_message: '<?= _t('contacts', 'Текст сообщения слишком короткий') ?>',
        enter_captcha: '<?= _t('contacts', 'Укажите результат с картинки') ?>',
        success: '<?= _t('contacts', 'Ваше сообщение успешно отправлено.<br/>Мы постараемся ответить на него как можно скорее.'); ?>'
    };

    $(function()
    {
        $form = $('#contactForm');
        f = app.form($form, false, {noEnterSubmit: true});

        var $f = f.getForm();
        bff.iframeSubmit($f, function(data, errors){
            if(data.success) {
                f.buttonSuccess(false,{revert:true,reset:true});
                app.alert.success(lng.success);
            } else {
                f.fieldsError(data.fields, errors, {title:lng.alert_title});
            }
            formReset();
            refreshCaptha();
        }, {
            beforeSubmit: function(){
                if(!f.checkRequired()) return false;
                var errMsg = [], errFields = [];
                if( ! bff.isEmail( f.fieldStr('email') ) ) {
                    errFields.push('email');
                    errMsg.push(lng.email_wrong);
                }
                if( f.fieldStr('message').length < 10 ) {
                    errFields.push('message');
                    errMsg.push(lng.no_message);
                }
                if( ! app.user.logined() ) {
                    if( ! f.fieldStr('captcha').length ) {
                        errFields.push('captcha');
                        errMsg.push(lng.enter_captcha);
                    }
                }
                if(errMsg.length > 0) {
                    f.fieldsError(errFields, errMsg, {title:lng.alert_title}); return false;
                }
                return true;
            }
        });

        var $btn = $form.find('.j-upload-file-btn');
        var $inp = $form.find('[name="attach"]');
        $btn.click(function(){
            $inp.trigger('click');
            return false;
        });

        var $fname = $form.find('.j-name');
        var $filename = $form.find('.j-file-name');
        $inp.on('change', function(){
            $fname.text($inp.val());
            $filename.removeClass('hidden');
            $btn.addClass('hidden');
        });

        $form.find('.j-upload-delete').on('click', function(){
            $inp.val('');
            $filename.addClass('hidden');
            $btn.removeClass('hidden');
            return false;
        });

        $form.on('click', '.j-cancel', function(){
            history.back();
            return false;
        });
    });

    function formReset() 
    {
        $form.find('input:text, input:password, select, textarea').val('');
        $form.find('input:radio, input:checkbox').prop('checked', false);
        $form.find('.j-upload-delete').trigger('click');
    }
    
    function refreshCaptha() {
        $('#captchaCode').attr('src', captchaUrl+'&r='+Math.random(1));
    }

    return {
        refreshCaptha: refreshCaptha
    }
}());
<? js::stop() ?>
</script>