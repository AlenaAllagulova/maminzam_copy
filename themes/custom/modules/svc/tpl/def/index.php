<?php
    /**
     * @var $this Svc
     */
   $isUser = User::id();
   $useClient = Users::useClient();
   $isWorker = $isUser && User::isWorker();
   $sMessage = '';
   if ($bill_success && User::id()) {
        $sMessage .= _t('bills', 'Вы пополнили счет');
        if (!empty($lastPayment)){
            $sMessage .= _t('bills', ' на <a href="[link]">[last_amount] [last_amount_curr]</a>',
                array(
                    'link'    => Bills::url('my.history'),
                    'last_amount' => $lastPayment['amount'],
                    'last_amount_curr' => $lastPayment['currency_data']['title_short'],
                    'balance' => $this->security->getUserBalance(),
                    'curr'    => Site::currencyDefault(),
                )
            );
        }
   }
?>
<?if($bill_success && User::id()):?>
    <div id="modalShowPrice" class="modal fade modal-success" role="dialog">
        <div class="modal-dialog">
            <img src="<?= bff::url('/img/money-bg.png')?>" alt="">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <p class=" modal-success__article"><?= $sMessage;?></p>
                    <? if(Users::useClient() && User::isClient()): ?>
                        <span>
                            <?= _t('bills', 'Теперь вы можете рекламировать свои объявления и покупать контакты нянь. ');?>
                        </span>
                    <?else:?>
                        <span>
                            <?= _t('bills', 'Теперь вы можете рекламировать свой профиль и покупать контакты родителей. ');?>
                        </span>
                    <? endif;?>
                </div>
                <div class="text-center pdb30">
                    <a href="<?= Svc::url('list') ?>" class="btn btn-primary pdr30 pdl30">
                        <?= _t('', 'Начать покупки') ?>
                    </a>
                </div>
            </div>
          </div>
    </div>
<?endif;?>
    <div class="container">

        <section class="l-mainContent">

            <h1 class="text-center mrgb50"><?= $titleh1; ?></h1>

            <?= config::get('svc_description_'.LNG, '') ?>

            <?if(!empty($packages_unlim)):?>
                <div class="text-center">
                    <h4>
                        <?=_t('buy_contacts', 'Открывайте контакты безлимитно')?>
                    </h4>
                </div>
                <div class="services-list mrgb50">
                    <? foreach($packages_unlim as $k => $pack): if( ! $pack['enabled']) continue; ?>
                        <div class="services-list__item">
                            <a href="<?=Packages::url('packages_buy', ['anchor' => '#unlim-pack-'.$pack['id']])?>" class="services-list__box">
                                <span class="services-list__title">
                                    <?= _t('','Пакет') ?>
                                    <?= _t('package','"[name]" ', ['name' => $pack['title']])?>
                                </span>

                                <span>
                                    <?= _t('','Неограниченное количество контактов на') ?>
                                    <?= tpl::declension($pack['period'], _t('package', 'день;дня;дней')) ?>

                                </span>
                                <span class="services-list__price mrgt10">
                                    <?= (int)$pack['price_per_one'].' '.Site::currencyDefault()?>
                                </span>
                            </a>
                        </div>
                    <? endforeach; ?>
                </div>
            <?endif;?>

            <?if($bClient && !empty($packages)):?>
                <div class="text-center">
                    <h4>
                        <?=_t('buy_contacts', 'Покупайте нужное количество контактов на месяц')?>
                    </h4>
                </div>

                <div class="services-list mrgb50">
                    <? foreach($packages as $k => $pack): if( ! $pack['enabled']) continue; ?>
                        <div class="services-list__item">
                            <a class="services-list__box" href="<?=Packages::url('packages_buy', ['anchor' => '#days-pack-'.$pack['id']])?>">
                                <span class="services-list__title">
                                    <?= _t('','Пакет ')?>"<?= $pack['title'] ?>"
                                </span>

                                <span>
                                     <?= tpl::declension($pack['quantity'], _t('package', 'контакт;контакта;контактов')) ?>
                                    x
                                    <?= tpl::declension(PACKAGES_PERIOD_DAYS, _t('package', 'день;дня;дней')) ?>
                                    <?if(!empty($pack['saving'])):?>
                                        <br>
                                        <?= _t('package','экономия <strong>[saving]</strong> ', ['saving' => $pack['saving']])?>
                                    <?endif;?>
                                </span>
                                <span class="services-list__price mrgt10">
                                    <?= (int)$pack['quantity']*(int)$pack['price_per_one'].' '.Site::currencyDefault()?>
                                </span>
                            </a>
                        </div>
                    <? endforeach; ?>
                </div>
            <?endif;?>

            <? if($useClient && !$bClient): ?>
                <h4 class="text-center">
                    <?= _t('svc', 'Продвигайте свой профиль'); ?>
                </h4>

                <? if( ! empty($svc['users'])): $oIcon = Users::i()->svcIcon(); ?>
                    <div class="services-list mrgb50">
                        <? foreach($svc['users'] as $k => $v): if( ! $v['on']) continue;
                            $url = ( $isWorker ? Svc::url('view', array('keyword' => $v['keyword'])) : false); ?>
                            <div class="services-list__item">
                            <? if($url) : ?>
                                <a <? if($url) { ?> href="<?= $url ?>"<? } ?> class="services-list__box" >
                            <? else: ?>
                                <div class="services-list__wrap">
                            <? endif;  ?>
                                <div class="mrgb20">
                                    <img src="<?= $oIcon->url($v['id'], $v['icon_s']) ?>" alt="<?= tpl::imageAlt(array('t' => $v['title_view'][LNG])); ?>" />
                                </div>
                                <span class="services-list__title"><?= $v['title_view'][LNG] ?></span>
                                <?= $v['description'][LNG] ?>
                            <? if($url) : ?>
                                </a>
                            <? else: ?>
                                </div>
                                <? endif;  ?>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>

                <? if( ! empty($svc['shop'])): $oIcon = Shop::i()->svcIcon(); ?>
                    <h6><?= _t('svc', 'Продвижение товара в магазине'); ?></h6>
                    <div class="se-services-list">
                        <div class="row">
                            <? foreach($svc['shop'] as $k => $v): if( ! $v['on']) continue; ?>
                                <div class="col-sm-4 se-services-item">
                                    <a class="se-service-icon">
                                        <img src="<?= $oIcon->url($v['id'], $v['icon_s']) ?>" alt="<?= tpl::imageAlt(array('t' => $v['title_view'][LNG])); ?>" />
                                    </a>
                                    <div class="media-body">
                                        <span class="se-service-name"><?= $v['title_view'][LNG] ?></span>
                                        <?= $v['description'][LNG] ?>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif; ?>
           <? endif; ?>


           <? if($useClient && $bClient): ?>
                <? if( ! empty($svc['orders'])): $oIcon = Orders::i()->svcIcon(); ?>

                    <div class="text-center">
                        <h4><?= _t('svc', 'Рекламируйте свои объявления'); ?></h4>
                        <span><?= _t('svc','Услуги доступны при публикации объявления')?></span>
                    </div>

                    <div class="services-list ">
                        <?  foreach($svc['orders'] as $k => $v): if( ! $v['on']) continue; ?>
                            <div class="services-list__item">
                                <div class="services-list__wrap ">
                                    <div class=" mrgb20">
                                        <img src="<?= $oIcon->url($v['id'], $v['icon_s']) ?>" alt="<?= tpl::imageAlt(array('t' => $v['title_view'][LNG])); ?>" />
                                    </div>
                                    <span class="services-list__title"><?= $v['title_view'][LNG] ?></span>
                                    <?= $v['description'][LNG] ?>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                 <? endif; ?>
           <? endif; ?>


        </section>

    </div>
<?if($bill_success && User::id()):?>
    <script type="text/javascript">
        <? js::start() ?>
        $(window).on('load',function(){
            $('#modalShowPrice').modal('show');
        });
        <? js::stop() ?>
    </script>
<? endif; ?>