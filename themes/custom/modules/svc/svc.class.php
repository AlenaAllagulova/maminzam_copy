<?php

class Theme_Custom_Svc extends Theme_Custom_Svc_Base
{
    /**
     * Страница с описанием всех услуг
     * добавлено:
     *   - информация о пакетах открытия контактов,
     *   - редирект с успешной оплаты счета
     */
    public function index()
    {
        if ( ! bff::servicesEnabled()) {
            $this->errors->error404();
        }

        $bill_success = $this->input->get('bill_success', TYPE_BOOL);

        $aData = [
            'svc' => [],
            'bill_success' => $bill_success
        ];

        $aSvc = Svc::model()->svcListing(Svc::TYPE_SERVICE);
        foreach ($aSvc as $v) {
            if ($v['on'] && bff::moduleExists($v['module'])) {
                $aData['svc'][ $v['module'] ][ $v['keyword'] ] = $v;
            }
        }

        if ($bill_success && User::id()) {
            $aFilter = array(
                'user_id' => User::id(),
                'status' => Bills::STATUS_COMPLETED,
                'type' => Bills::TYPE_IN_PAY,
                ':psystem' => 'psystem > 0'
            );
            $aListPayment = Bills::model()->billsList($aFilter, false, ' LIMIT 1');
            if (is_array($aListPayment) && !empty($aListPayment)) {
                $aData['lastPayment'] = reset($aListPayment);

                $aData['lastPayment']['currency_data'] = Site::currencyData($aData['lastPayment']['currency_id'], ['title_short']);
            }
        }
        # пакеты открытия контактов
        $aData['bClient'] = (Users::useClient() && User::isClient());
        if($aData['bClient'] ){
            # пакеты с ограничениями по контактам на месяц доступные для покупки
            $aData['packages'] = Packages::model()->packagesContactsListing(['quantity > 0', 'enabled'   => true], false, '', 'P.quantity');
        }
        # пакеты без ограничения на открытие контактов на Nдней доступные для покупки
        $nCurrentUserType = User::isClient()? Users::TYPE_CLIENT : Users::TYPE_WORKER;
        $aFilter = [
            'quantity'  => PACKAGES_UNLIM_QUANTITY,
            'user_type' => $nCurrentUserType,
            'enabled'   => true,
        ];
        $aData['packages_unlim'] = Packages::model()->packagesContactsListing($aFilter, false, '', 'P.period');

        # SEO: Услуги
        $url = static::url('list', array(), true);
        $this->urlCorrection(static::urlDynamic($url));
        $this->seo()->canonicalUrl($url);
        $this->seo()->setPageMeta(Site::i(), 'services', array(), $aData);
        if (empty($aData['titleh1'])) $aData['titleh1'] = _t('svc', 'Платные сервисы');

        $aData['breadcrumbs'] = array();
        if (User::id()) {
            $aData['breadcrumbs'][] = array('title' => _t('','Profile'), 'link' => Users::url('my.profile'));
        }
        $aData['breadcrumbs'][] = array('title' => $aData['titleh1'], 'active' => true);

        return $this->viewPHP($aData, 'index');
    }
}