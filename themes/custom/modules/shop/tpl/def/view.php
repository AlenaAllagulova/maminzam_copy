<?php
if( ! empty($regions)){
    $aFirstRegions = reset($regions);
}
tpl::includeJS(['dist/custom']);
?>
<div class="container">

    <?= tpl::getBreadcrumbs($breadcrumbs); ?>

    <section class="l-mainContent">
        <div class="row">

            <!-- Project-->
            <div class="col-md-9">

                <div class="l-borderedBlock">

                    <div class="l-inside">

                        <h1 class="small"><?= $title ?></h1>

                        <div>
                            <?= Shop::priceBlock($aData, $cat, false) ?>
                            <? if( ! empty($tags)): foreach($tags as $v):?>
                                <a href="<?= Shop::url('search-tag', $v) ?>" class="l-tag"><?= $v['tag'] ?></a>
                            <? endforeach; endif; ?>
                        </div>

                        <article class="sh-shop-item-text">
                            <p><?= nl2br($descr); ?></p>
                        </article>

                    </div>

                    <? if($imgcnt): ?>
                    <div class="l-inside">
                        <div class="sh-item-gallery">
                            <? foreach($images as $k => $v): ?>
                            <a href="<?= $v['i'][ ShopProductImages::szView ]; ?>" class="fancyzoom" rel="fancy-gallery-<?= $id ?>">
                                <img src="<?= $v['i'][ ShopProductImages::szThumbnail ]; ?>" alt="<?= tpl::imageAlt(array('t' => $title, 'k' => $k)); ?>" />
                            </a>
                            <? endforeach ?>
                        </div>
                    </div>
                    <script type="text/javascript">
                        <?
                            tpl::includeJS('fancybox2', true);
                            js::start(); ?>
                            $(function(){
                                $('.fancyzoom').fancybox({
                                    openEffect	: 'none',
                                    closeEffect	: 'none',
                                    nextEffect  : 'fade',
                                    prevEffect : 'fade',
                                    fitToView: false,
                                    helpers: {
                                        overlay: {locked: false}
                                    }
                                });

                            });
                        <? js::stop(); ?>
                    </script>
                    <? endif; ?>

                    <? if( ! empty($dynprops)): ?><div class="l-inside l-dymanic-features"><?= $dynprops ?></div><? endif; ?>

                    <div class="l-inside o-clientInfo">

                        <div class="row">
                            <div class="col-sm-7">
                                <a href="<?= Users::url('profile', array('login' => $login)) ?>" class="o-client-avatar">
                                    <img src="<?= $avatar_small ?>" alt="<?= tpl::avatarAlt($aData); ?>" />
                                    <?= tpl::userOnline($aData) ?>
                                </a>
                                <div class="o-client-about">
                                    <div>
                                        <?= tpl::userLink($aData, 'no-login'); ?>
                                    </div>
                                    <div class="visible-lg visible-md visible-sm">
                                        <?= _t('shop', 'Зарегистрирован на сайте') ?> <?= tpl::date_format_spent($user_created, false, false)?>
                                    </div>
                                    <div class="o-feedbacks-inf">
                                        <?= _t('shop', 'Отзывы исполнителей:') ?> <?= tpl::opinions($opinions_cache, array('login' => $login)) ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-5 o-client-more-works">
                                <a href="<?= Shop::url('user.listing', array('login' => $login)) ?>" class="sh-showAllWorks"><span><?= _t('shop', 'Другие товары продавца'); ?></span><i class="fa fa-angle-right c-link-icon"></i></a>
                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <? if( ! empty($contacts)): # контакты и кнопка их открытия
                            $aTypes = Users::aContactsTypes();
                            $open_user_id = $aData['open_user_id'] = $user_id;
                            $open_user_type = $aData['open_user_type'] = $type;
                            $is_profile = Site::i()->isProfilePage();
                            $bOpenedContacts = Users::isOpenedContacts($open_user_id);
                            $aCurrentUserData = Users::model()->userData(User::id(), 'type');
                            # показывать кнопку в чужом профиле для противоположного типа пользователя
                            $isOwner = $isOwner = User::isCurrent($user_id);
                            $bShowContactsBtn = !$isOwner && !$bOpenedContacts && ($aCurrentUserData['type'] != $open_user_type);
                            if( ! User::id() || $bShowContactsBtn):?>
                                <div class="text-center j-show-btn-block">
                                    <button onclick="<?= ( ! User::id())? '$(\'.j-show-btn\').hide();$(\'#j-login-link\').toggle();' :
                                        "Custom.open_contacts({$open_user_id},{$is_profile})"?>"
                                            class="btn btn-success j-show-btn">
                                        <?=_t('contacts', 'Показать контакты')?>
                                    </button>
                                    <a style="display: none"
                                       href="<?=Users::url('login')?>"
                                       class="btn btn-primary btn-block"
                                       id="j-login-link">
                                        <?=_t('contacts', 'Авторизируйтесь')?>
                                    </a>
                                </div>
                            <? endif;?>
                            <? if($isOwner || $bOpenedContacts):?>
                                 <?= Users::i()->viewPHP($aData, 'contacts.block');?>
                            <? else:?>
                                <div class="j-contacts-block"></div>
                            <? endif;?>
                        <? endif; ?>

                    </div>

                    <?= User::isCurrent($user_id) ? $this->viewPHP($aData, 'owner.view.controls') : '' ?>

                    <div class="l-inside">
                        <ul class="l-item-features">
                            <? if( ! empty($aFirstRegions['title'])): ?><li><i class="fa fa-map-marker"></i> <?= $aFirstRegions['title']?></li><? endif; ?>
                            <li><span><i class="fa fa-eye"></i> <?= _t('', 'Просмотров: [total]', array('total'=>$views_total)); ?></span></li>
                        </ul>
                    </div>

                </div>

            </div>

            <div class="col-md-3 visible-lg visible-md">
                <? # Баннер: Магазин: просмотр ?>
                <?= Banners::view('shop_view', array('pos'=>'right', 'cat'=>$cat_id)) ?>
            </div>

        </div>
    </section>

</div>