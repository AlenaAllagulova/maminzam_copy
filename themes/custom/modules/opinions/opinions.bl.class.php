<?php

class Theme_Custom_OpinionsBase extends Theme_Custom_OpinionsBase_Base
{
    /**
     * Обрабатываем параметры запроса, добавлена проверка отсрочки отзыва
     * @param integer $nOpinionID ID отзыва или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    public function validateOpinionData($nOpinionID, $bSubmit)
    {
        $aData = array();
        $aParams = array(
            'message' => array(TYPE_NOTAGS, 'len' => 3000, 'len.sys' => 'opinions.message.limit'), # Текст отзыва
            'type'    => TYPE_UINT,   # Тип отзыва
        );

        if( ! $nOpinionID){
            if (Orders::ordersOpinions()) {
                $aParams['order_id'] = TYPE_UINT; # ID заказа на основании которого добавляется отзыв
            } else {
                $aParams['user_id'] = TYPE_UINT; # ID пользователя которому оставляют отзыв
            }
        }

        $this->input->postm($aParams, $aData);

        if ($bSubmit)
        {
            if(empty($aData['message'])){
                $this->errors->set(_t('opinions', 'Не указан текст отзыва'));
            }

            $aTypes = Opinions::aTypes();
            $aTypes = array_keys($aTypes);
            if( ! in_array($aData['type'], $aTypes)){
                $this->errors->set(_t('opinions', 'Не указан тип отзыва'));
            }

            if (!empty($aData['order_id'])) {
                $aOrderData = Orders::model()->orderData($aData['order_id'], ['user_id', 'status', 'performer_created']);

                # проверка отсрочки отзыва
                if (Orders::ordersOpinions() && $aOrderData['status'] == Orders::STATUS_CLOSED) {

                    $d = time() - strtotime($aOrderData['performer_created']);
                    $daysOpinionsDelay = config::get('users_opinions_delay');

                    if ($d <= (int)$daysOpinionsDelay * (24 * 60 * 60)) {

                        $sOpinionsStartDate = tpl::date_format2(strtotime($aOrderData['performer_created']) + ($daysOpinionsDelay * (24 * 60 * 60)), true);
                        $this->errors->set(_t('opinions', 'Отзыв можно будет добавить начиная с [date]',
                            ['date' => $sOpinionsStartDate]));
                    }
                }
            }
        }
        return $aData;
    }

}