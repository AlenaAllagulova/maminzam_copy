<?php
?>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="mrgt40">
                    <h4 class="modal-title text-center " id="myModalLabel">
                        <? if(User::isWorker()): ?>
                            <?= _t('opinions', 'Оставьте отзыв о семье'); ?>
                        <? else: ?>
                            <?= _t('opinions', 'Оставьте отзыв'); ?>
                        <? endif; ?>
                    </h4>
                </div>
            </div>
            <div class="modal-body">
                <form method="post" class="<?= Rating::JS_CLASS_RATING_STARS_BOX_MODAL ?>">
                    <input type="hidden" name="order_id" value="0" />
                    <div class="hidden form-group <?= Rating::JS_CLASS_RATING_STARS_TYPES_BOX ?>">
                        <? foreach(Opinions::aTypes() as $v): ?>
                            <label class="radio-inline radio-inline-sm radio-<?= $v['cf'] ?>">
                                <input type="radio" name="type" <?= $v['ck']; ?> value="<?= $v['id'] ?>"> <?= $v['t'] ?>
                            </label>
                        <? endforeach; ?>
                    </div>
                    <?= Rating::getViewStars(); ?>
                    <div class="form-group j-required">
                        <textarea rows="5" class="form-control" style="resize: vertical" name="message" placeholder="<?= _t('opinions', 'Ваш отзыв...'); ?>"></textarea>
                        <div class="help-block j-help-block"></div>
                    </div>
                    <div class="c-formSubmit flex flex_center">
                        <button type="submit" class="btn btn-primary w200 c-formSuccess">
                            <?= _t('opinions', 'Отправить'); ?>
                        </button>
                        <a class="link-bold_fz15 link-bold mrgl20" href="#" data-dismiss="modal"><span><?= _t('form', 'Отмена'); ?></span></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>