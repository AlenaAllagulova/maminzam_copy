<?php
/**
 * @var $this Opinions
 */

tpl::includeJS('opinions.list', false, 2);

?>
    <div class="p-profileContent pdt0" id="j-opinions-list">

        <form method="get" action="" class="j-filter">
            <input type="hidden" name="t" value="<?= $f['t'] ?>" />
            <input type="hidden" name="tm" value="<?= $f['tm'] ?>" />
            <input type="hidden" name="d"  value="<?= $f['d'] ?>" />
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />

            <div class="p-profile-title bbn pdb0 mrgb0  ">
                <ul class="p-profile-submenu pull-left hidden-xs">
                    <? foreach($types as $v): ?>
                        <li<?= $v['id'] == $f['t'] ? ' class="active"' : '' ?>><a href="#" class="j-f-t" data-id="<?= $v['id'] ?>"><?= $v['plural'] ?> <small class="j-cnt-<?= $v['id'] ?>">(<?= $counters[ $v['id'] ]['cnt'] ?>)</small></a></li>
                    <? endforeach; ?>
                </ul>

                <ul class="dropdown visible-xs">
                    <li>
                        <a href="#" class="drop-link drop-link_fz13 flex flex_center  flex_0-0-a" data-toggle="dropdown">
                            <span class="j-f-title"><?= $types[ $f['t'] ]['plural'] ?>
                                <small class="j-f-title-counter">(<?= $counters[ $f['t'] ]['cnt'] ?>)</small>
                            </span>
                            <i class="mrgl5 icon-arrow-point-to-down"></i>
                        </a>
                        <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                            <? foreach($types as $v): ?>
                                <li<?= $v['id'] == $f['t'] ? ' class="active"' : '' ?>><a href="#" class="j-f-t" data-id="<?= $v['id'] ?>"><?= $v['plural'] ?> <small class="j-cnt-<?= $v['id'] ?>">(<?= $counters[ $v['id'] ]['cnt'] ?>)</small></a></li>
                            <? endforeach; ?>
                        </ul>
                    </li>
                </ul>

                <div class="p-profile-submenu-dropdowns pull-right">

                    <ul class="dropdown pull-right visible-lg">
                        <li>
                            <a href="#" class="drop-link drop-link_fz13 flex flex_center  flex_0-0-a" data-toggle="dropdown">
                                <span class="j-tm-title"><?= $terms[ $f['tm'] ]['t'] ?></span>
                                <i class="mrgl5 icon-arrow-point-to-down"></i>
                            </a>
                            <ul class="dropdown-menu c-dropdown-caret_right pull-right" role="menu">
                                <? foreach($terms as $v): ?>
                                    <li<?= $v['id'] == $f['tm'] ? ' class="active"' : '' ?>><a href="#" class="j-f-tm" data-id="<?= $v['id'] ?>"><?= $v['t'] ?></a></li>
                                <? endforeach; ?>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="clearfix"></div>

            </div><!-- /.p-profile-title -->
        </form>


        <? if($bAllowAdd): ?>
        <div class="l-borderedBlock no-color" id="j-opinion-add-block">
            <header class="l-inside">
                <a href="#leaveOpinion" class="btn btn-link ajax-link o-btn-propose" data-toggle="collapse">
                    <span>
                        <?= _t('opinions', 'Оставить отзыв'); ?>
                    </span>
                </a>
            </header>

            <div class="collapse" id="leaveOpinion">

            <form class="form-horizontal" role="form" method="post" id="j-opinion-add-form">
                <input type="hidden" name="user_id" value="<?= $user['id'] ?>" />

                <div class="l-inside">

                    <div class="row j-required">
                        <label for="text" class="col-sm-2 control-label"><?= _t('opinions', 'Текст отзыва:'); ?></label>
                        <div class="col-sm-10">
                            <textarea rows="5" id="text" class="form-control" name="message"></textarea>
                        </div>
                    </div>

                    <div class="form-group mrgt20">
                        <div class="col-sm-10 col-sm-offset-2">
                            <? foreach(Opinions::aTypes() as $v): ?>
                            <label class="radio-inline radio-inline-sm radio-<?= $v['cf'] ?>">
                                <input type="radio" name="type" value="<?= $v['id'] ?>"> <?= $v['t'] ?>
                            </label>
                            <? endforeach; ?>
                        </div>
                    </div>

                </div>

                <div class="l-inside">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-2 o-publicate-button">
                            <button class="btn btn-success mrgr10 j-submit"><?= _t('opinions', 'Опубликовать отзыв'); ?></button>
                            <a href="#leaveOpinion" class="ajax-link j-cancel" data-toggle="collapse"><span><?= _t('form', 'Отмена'); ?></span></a>
                        </div>
                    </div>
                </div><!-- /.l-inside -->

            </form>

            </div><!-- /.collapse -->

        </div><!-- /.bordered-block no-color -->
        <? else: if( ! empty($reasonMessage)): ?>
            <div class="alert alert-warning">
                <?= $reasonMessage ?>
            </div>
        <? endif; endif; ?>

        <div class="j-list"><?= $list ?></div>

        <div class="j-pagination"><?= $pgn ?></div>

    </div><!-- /.p-profile-content -->

<script type="text/javascript">
    <? js::start() ?>
    jOpinionsList.init(<?= func::php2js(array(
        'lang'   => array(
            'opinion_delete' => _t('opinions', 'Удалить отзыв?'),
            'answer_delete'  => _t('opinions', 'Удалить ответ?'),
            'check_type'     => _t('opinions', 'Укажите тип отзыва'),
        ),
        'types' => $types,
        'terms' => $terms,
        'directions' => $directions,
        'ajax'  => 1,
        'add'   => $bAllowAdd,
    )) ?>);
    <? js::stop() ?>
</script>