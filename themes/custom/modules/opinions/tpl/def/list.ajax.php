<?php
/**
 * @var $this Opinions
 */

tpl::includeJS(['dist/rating']);

    $aUserTypes = Users::aTypes();
    $aTypes = Opinions::aTypes();
    $ordersOpinions = Orders::ordersOpinions();

    if( ! empty($list)): ?>
        <div class="opinions-list">
            <? foreach($list as $v):
                $id  = $v['id'];
                $isAuthor = User::isCurrent($v['author_id']);
            ?>
                <div class="opinions-list__item">
                    <div class="opinions-list__box">
                        <div class="user-box">
                            <div class="user-box__avatar user-box__avatar_sq-big">
                                <?if(User::id()):?>
                                    <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="">
                                        <img src="<?= UsersAvatar::url($v['opponent_id'], $v['avatar'], UsersAvatar::szNormal, $v['sex']); ?>" class="" alt="<?= tpl::avatarAlt($v); ?>" />
                                        <?= tpl::userOnline($v) ?>
                                    </a>
                                <?else:?>
                                    <img src="<?= UsersAvatar::url($v['opponent_id'], $v['avatar'], UsersAvatar::szNormal, $v['sex']); ?>" class="" alt="" />
                                <?endif;?>
                            </div>
                            <div class="mrgl10 flex_1 <?= Rating::JS_CLASS_RATING_STARS_BOX ?>">
                                <div class="user-box__text">
                                    <div class="flex flex_center">
                                        <a <?= User::id() ? 'href="' . Users::url('profile', ['login' => $v['login']]) . '"' : '' ?> class="user-box__name">
                                            <?= ($v['name']) ?> <?= ($v['surname']) ?>
                                        </a>
                                        <? if($v['pro']): ?><span><span class="pro mrgl5">pro</span></span><? endif; ?>
                                    </div>
                                </div>
                                <div class="user-box__text-s">
                                    <?= _t('','Объявление:')?>
                                    <? if($ordersOpinions && $v['keyword']):
                                        $link = '<a class="user-box__link-second" href="'.Orders::url('view', array('id' => $v['order_id'], 'keyword' => $v['keyword'])).'">'.tpl::truncate($v['title'], config::sysAdmin('opinions.list.ajax.title.truncate', 50, TYPE_UINT)).'</a>';
                                        $text = $v['author_id'] == $v['performer_id']
                                            ? _t('opinions', '[link]', array('link' => $link))
                                            : _t('opinions', '[link]', array('link' => $link));
                                        ?>
                                        <?= User::id() ? $text : tpl::truncate($v['title'], config::sysAdmin('opinions.list.ajax.title.truncate', 50, TYPE_UINT)); ?>
                                    <? endif; ?>
                                </div>
                                <div class="flex flex_center mrgt10">
                                    <div class="rating">
                                        <i class="icon-star-color"></i>
                                        <i class="icon-star-color"></i>
                                        <i class="icon-star-color"></i>
                                        <i class="icon-star-color"></i>
                                        <i class="icon-star-color"></i>
                                    </div>
                                    <div class="user-box__text-s mrgl10">
                                        <?= tpl::dateFormat($v['created']); ?>
                                    </div>
                                </div>
                                <div class="j-opinion">
                                    <div class="opinions-list__text">
                                        <?= nl2br($v['message']); ?>
                                        <?= Rating::getViewStars(empty($f['d']) ? $user['id'] : $v['opponent_id'], $v['id']); ?>
                                    </div>
                                    <? if($isAuthor): ?>
                                        <div class="p-feedback-answer-controls">
                                            <a href="#" class="link-def link-def_main link-def_tdn link-def_fz13 j-opinion-edit" data-id="<?= $id ?>">
                                                <?= _t('form', 'Редактировать'); ?>
                                            </a>
                                            <a href="#" class="link-def link-def_main link-def_tdn link-def_fz13 j-opinion-delete" data-id="<?= $id ?>">
                                                <?= _t('form', 'Удалить'); ?>
                                            </a>
                                        </div>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>

                        <? if(User::isCurrent($v['user_id'])): if( empty($v['answer'])): ?>
                            <div class="pd10 text-center">
                                <div class="mrgb5"><?= _t('','У Вас новый отзыв.')?></div>
                                <a href="#j-answer-add-<?= $id ?>" data-toggle="collapse" class="btn min-w-200px btn-primary j-answer-add" data-id="<?= $id ?>">
                                    <span>
                                        <?= _t('opinions', 'Ответить'); ?>
                                    </span>
                                </a>
                            </div>
                        <? endif;  endif; ?>

                    </div>
                    <div class="opinions-list__my">
                        <? if($isAuthor): ?>
                            <div class="j-my-opinion-edit-<?= $id ?>" style="display:none;">
                                <form action="" method="post">
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                    <input type="hidden" name="id" value="<?= $v['order_id'] ?>" />
                                    <div class="form-group j-required">
                                        <textarea name="message" class="form-control" rows="5"><?= $v['message'] ?></textarea>
                                    </div>
                                    <div class="form-group <?= Rating::JS_CLASS_RATING_STARS_TYPES_BOX ?>">
                                        <? foreach(Opinions::aTypes() as $vv): ?>
                                            <label class="radio-inline radio-inline-sm radio-<?= $vv['cf'] ?>">
                                                <input type="radio" name="type" value="<?= $vv['id'] ?>" <?= $vv['id'] == $v['type'] ? 'checked="checked"' : '' ?> /> <?= $vv['t'] ?>
                                            </label>
                                        <? endforeach; ?>
                                    </div>
                                    <?= Rating::getViewStars(empty($f['d']) ? $user['id'] : $v['opponent_id'], $v['id'], !$isAuthor); ?>
                                    <div class="c-formSubmit">
                                        <button class="btn btn-primary btn-sm c-formSuccess j-submit"><?= _t('form', 'Сохранить'); ?></button>
                                        <a href="#" class="link-bold c-formCancel j-cancel">
                                            <?= _t('form', 'Отмена'); ?>
                                        </a>
                                    </div>
                                </form>
                            </div>
                        <? endif; ?>

                        <? if(User::isCurrent($v['user_id'])):
                            if( ! empty($v['answer'])): ?>
                                <div class="user-box mrgt10">
                                    <div class="user-box__avatar user-box__avatar_sq-big">
                                        <a href="<?= Users::url('profile', array('login' => $user['login'])); ?>" class="">
                                            <img src="<?= UsersAvatar::url($v['user_id'], $v['answerer_avatar'], UsersAvatar::szNormal, $v['answerer_sex']); ?>" class="" alt="<?= tpl::avatarAlt($v); ?>" />
                                            <?= tpl::userOnline($v) ?>
                                        </a>
                                    </div>
                                    <div class="mrgl10 flex_1">
                                        <div class="user-box__text">
                                            <div class="flex flex_center">
                                                <a href="<?= Users::url('profile', array('login' => $user['login'])); ?>" class="user-box__name">
                                                    <?= ($user['name']) ?> <?= ($user['surname']) ?>
                                                </a>
                                                <div class="">
                                                    <? if($user['pro']): ?><span class="pro mrgl5">pro</span><? endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex flex_center mrgt10">
                                            <div class="rating">
                                                <i class="icon-star-color"></i>
                                                <i class="icon-star-color"></i>
                                                <i class="icon-star-color"></i>
                                                <i class="icon-star-color"></i>
                                                <i class="icon-star-color"></i>
                                            </div>
                                            <div class="user-box__text-s mrgl10">
                                                <?= tpl::dateFormat($v['answer_modified']); ?>

                                            </div>
                                        </div>
                                        <div class="j-answer flex_0-0-a">
                                            <div class="opinions-list__text">
                                                <?= nl2br($v['answer']); ?>
                                            </div>
                                            <div class="p-feedback-answer-controls">
                                                <a href="#" class="link-def link-def_main link-def_tdn link-def_fz13  j-answer-edit" data-id="<?= $id ?>">
                                                    <?= _t('form', 'Редактировать'); ?>
                                                </a>
                                                <a href="#" class="link-def link-def_main link-def_tdn link-def_fz13  j-answer-delete" data-id="<?= $id ?>">
                                                    <?= _t('form', 'Удалить'); ?>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="w100p mrgt10 flex_0-0-a">

                                            <div class="j-answer-edit-<?= $id ?>" style="display:none;">
                                                <form role="form" method="post" action="">
                                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                                    <div class="form-group j-required">
                                                        <textarea class="form-control" name="answer" rows="5"><?= $v['answer'] ?></textarea>
                                                    </div>
                                                    <div class="c-formSubmit">
                                                        <button class="btn btn-primary btn-sm c-formSuccess j-submit"><?= _t('form', 'Сохранить'); ?></button>
                                                        <a href="#" class="link-bold c-formCancel j-cancel">
                                                            <?= _t('form', 'Отмена'); ?>
                                                        </a>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? else: ?>
                                <div class="collapse" id="j-answer-add-<?= $id ?>">
                                    <div class="p-add-comment mrgt10">
                                        <form role="form" method="post" action="">
                                            <input type="hidden" name="id" value="<?= $id ?>" />
                                            <div class="form-group j-required">
                                                <textarea rows="4" name="answer" class="form-control" placeholder="<?= _t('', 'Комментарий'); ?>"></textarea>
                                            </div>
                                            <div class="c-formSubmit">
                                                <button class="btn btn-primary btn-sm c-formSuccess j-submit">
                                                    <?= _t('opinions', 'Ответить'); ?>
                                                </button>
                                                <a href="#j-answer-add-<?= $id ?>" data-toggle="collapse" class="link-bold c-formCancel">
                                                    <?= _t('form', 'Отмена'); ?>
                                                </a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            <? endif; ?>
                        <? else: ?>
                           <? if( ! empty($v['answer'])): ?>
                                <div class="user-box mrgt10">
                                    <div class="user-box__avatar user-box__avatar_sq-big">
                                        <?if(User::id()):?>
                                            <a href="<?= Users::url('profile', array('login' => $user['login'])); ?>" class="">
                                                <img src="<?= UsersAvatar::url($v['user_id'], $v['answerer_avatar'], UsersAvatar::szNormal, $v['answerer_sex']); ?>" class="" alt="<?= tpl::avatarAlt($v); ?>" />
                                                <?= tpl::userOnline($v) ?>
                                            </a>
                                        <?else:?>
                                            <img src="<?= UsersAvatar::url($v['user_id'], $v['answerer_avatar'], UsersAvatar::szNormal, $v['answerer_sex']); ?>" class="" alt="" />
                                        <?endif;?>
                                    </div>
                                    <div class="mrgl10 flex_1">
                                        <div class="user-box__text">
                                            <div class="flex flex_center">
                                                <a <?= User::id() ? 'href="' . Users::url('profile', ['login' => $user['login']]) . '"' : '';?> class="user-box__name">
                                                    <?= ($user['name']) ?> <?= ($user['surname']) ?>
                                                </a>
                                                <div class="">
                                                    <? if($user['pro']): ?><span class="pro mrgl5">pro</span><? endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex flex_center mrgt10">
                                            <div class="rating">
                                                <i class="icon-star-color"></i>
                                                <i class="icon-star-color"></i>
                                                <i class="icon-star-color"></i>
                                                <i class="icon-star-color"></i>
                                                <i class="icon-star-color"></i>
                                            </div>
                                            <div class="user-box__text-s mrgl10">
                                                <?= tpl::dateFormat($v['answer_modified']); ?>
                                            </div>
                                        </div>
                                        <div class="opinions-list__text">
                                            <?= nl2br($v['answer']); ?>
                                        </div>
                                    </div>
                                </div>
                            <? endif; ?>
                       <? endif; ?>
                    </div>
                </div>


                <div class="media-body ">

                    <div class="">
                        <? if($isAuthor && Opinions::premoderation() && ! $v['moderated']): ?>
                            <div class="alert alert-warning" role="alert">
                                <?= _t('opinions', 'Ожидает проверки модератора') ?>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
<? else: ?>
        <div class="text-center">
            <img src="<?= bff::url('/img/sad.svg')?>" width="40" alt="">
            <br>
            <?= _t('opinions', 'Отзывов пока что нет'); ?>
        </div>
<? endif; ?>

<? if (Rating::isActive()): ?>
    <script type="text/javascript">
        (function () {
            if (typeof window.ratingView === 'object') {
                if (typeof window.ratingView.initView === 'function') {
                    window.ratingView.initView(<?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>);
                }
            }
            if (typeof window.ratingEdit === 'object') {
                if (typeof window.ratingEdit.initEdit === 'function') {
                    window.ratingEdit.starsData = {};
                    window.ratingEdit.initEdit(<?= Rating::JS_PARAMETERS_EDIT('.j-opinion-edit') ?>);
                }
            }
        })();

        <? js::start() ?>
        (function() {
            $(function() {
                window.ratingView = new Rating();
                window.ratingView.initView(<?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>);
                window.ratingEdit = new Rating();
                window.ratingEdit.initEdit(<?= Rating::JS_PARAMETERS_EDIT('.j-opinion-edit') ?>);
            });
        }());
        <? js::stop(); ?>
    </script>
<? endif;