<?php
    $types = Opinions::aTypes();
    $type = $types[$type];
?>
<div class="modal fade" id="j-modal-opinion-<?= $id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?= $my ? _t('opinions', 'Ваш отзыв') : ( $author_id == $order['user_id'] ?  _t('opinions', 'Отзыв заказчика') : _t('opinions', 'Отзыв исполнителя')) ?></h4>
            </div>
            <div class="modal-body">
                <p><?= $my ? _t('opinions', 'Вы оставили') : tpl::userLink($author, 'no-login').' '.($author['sex'] == Users::SEX_MALE ? _t('opinions','оставил') : _t('opinions','оставила')) ?> <strong class="<?= $type['ct'] ?>"><?= $type['t'] ?></strong> <?= _t('opinions', 'отзыв'); ?>:</p>
                <?= nl2br($message); ?>
                <?= Rating::getViewStars($user_id, $id); ?>
            </div>
        </div>
    </div>
</div>