<?php

View::setLayout('index');

?>
<div class="general-wrap">
    <div class="container">
        <p class="general-wrap__article">
            <?= _t('','Как это работает')?>
        </p>
        <div class="how-work mrgt35">
            <div class="how-work__col how-work__col_none-xs flex flex_center flex_jcc">
                <img class="how-work__img" src="<?= bff::url('/img/mobile-general.png') ?>" alt="">
            </div>
            <div class="how-work__col flex flex_center">
                <ul class="how-list">
                    <li class="how-list__item">
                        <span class="how-list__numb flex flex_center flex_jcc flex_0-0-a">1</span>
                        <span class="how-list__text">
                            <?= _t('','Расскажите кого Вы ищите - няню или бебиситтера.')?>
                        </span>
                    </li>
                    <li class="how-list__item">
                        <span class="how-list__numb flex flex_center flex_jcc flex_0-0-a">2</span>
                        <span class="how-list__text">
                            <?= _t('','Просматривайте профили и отзывы.')?>
                        </span>
                    </li>
                    <li class="how-list__item">
                        <span class="how-list__numb flex flex_center flex_jcc flex_0-0-a">3</span>
                        <span class="how-list__text">
                            <?= _t('','Связывайтесь с подходящей няней и приглашайте ее на работу.')?>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="general-wrap general-wrap_bg">
    <div class="container">
        <p class="general-wrap__article">
            <?= _t('','Это безопасно')?>
        </p>
        <div class="row">
            <div class="col-sm-4 flex flex_center flex_column save-info">
                <i class="icon-tap save-info__icon"></i>
                <p class="save-info__title">
                    <?= _t('','Все профили проверяются')?>
                </p>
                <span class="save-info__text">
                    <?= _t('','Няня не попадает в поиск, пока ее страница не одобреня модератором.')?>
                </span>
            </div>
            <div class="col-sm-4 flex flex_center flex_column save-info">
                <i class="icon-telephone1 save-info__icon"></i>
                <p class="save-info__title">
                    <?= _t('','Номера подтверждены')?>
                </p>
                <span class="save-info__text">
                    <?= _t('','Вы можете связаться с няней, прежде чем пригласить ее в семью.')?>
                </span>
            </div>
            <div class="col-sm-4 flex flex_center flex_column save-info">
                <i class="icon-communication save-info__icon"></i>
                <p class="save-info__title">
                    <?= _t('','Правдивые отзывы')?>
                </p>
                <span class="save-info__text">
                    <?= _t('','Все отзывы и оценки от реальных родителей о нянях, которых уже приглашали.')?>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="general-wrap hidden">
    <div class="container">
        <p class="general-wrap__article">
            <?= _t('','Что говорят родители')?>
        </p>
        <div class="j-slider-general slider-general">
            <div class="">
                <div class="slider-general__item flex flex_center flex_jcc flex_column">
                    <img src="<?= bff::url('/img/woman-slider-g.jpg')?>" class="slider-general__avatar" alt="">
                    <span class="slider-general__name">
                        <?= _t('','Виктория')?>
                    </span>
                    <span class="slider-general__rating">
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                    </span>
                    <span class="slider-general__text">
                        <?= _t('','Нашла прекрасную няню на сайте. Она стала настоящим другом нашему ребенку. Всегда можем на нее положиться.')?>
                    </span>
                </div>
            </div>
            <div class="">
                <div class="slider-general__item flex flex_center flex_jcc flex_column">
                    <img src="<?= bff::url('/img/woman-slider-g.jpg')?>" class="slider-general__avatar" alt="">
                    <span class="slider-general__name">
                        <?= _t('','Виктория')?>
                    </span>
                    <span class="slider-general__rating">
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                    </span>
                    <span class="slider-general__text">
                        <?= _t('','Нашла прекрасную няню на сайте. Она стала настоящим другом нашему ребенку. Всегда можем на нее положиться.')?>
                    </span>
                </div>
            </div>
            <div class="">
                <div class="slider-general__item flex flex_center flex_jcc flex_column">
                    <img src="<?= bff::url('/img/woman-slider-g.jpg')?>" class="slider-general__avatar" alt="">
                    <span class="slider-general__name">
                        <?= _t('','Виктория')?>
                    </span>
                    <span class="slider-general__rating">
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                        <i class="icon-star-color"></i>
                    </span>
                    <span class="slider-general__text">
                        <?= _t('','Нашла прекрасную няню на сайте. Она стала настоящим другом нашему ребенку. Всегда можем на нее положиться.')?>
                    </span>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="general-wrap general-wrap_bg-accent">
    <div class="container">
        <p class="general-wrap__article general-wrap__article_white">
            <?= _t('','Найдите то, что ищите')?>
        </p>
        <div class="text-center btn-box">
            <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_CLIENT]) ?>" class="general-btn">
                <?=_t('','Я родитель')?>
            </a>
            <a href="<?= Users::url('register', ['user_type'=> Users::TYPE_WORKER]) ?>" class="general-btn">
                <?=_t('','Я няня')?>
            </a>
        </div>
    </div>
</div>