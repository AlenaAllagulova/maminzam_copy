<?php

tpl::includeJS('autocomplete', true);
tpl::includeJS('filter', false, 2);

if($detect = config::get('geo.ip.location.detect', 0, TYPE_UINT)):
$detect = Geo::regionData($detect); ?>
    <div class="c-dropdown navbar-header-dropdown displaynone" id="j-geo-region-geoip-confirm">
        <div class="c-dropdown-heading">
            <a href="#" class="close j-close"><i class="fa fa-times"></i></a>
            <h3><?= _t('geo', 'Ваш регион [title]?', array('title' => $detect['title'])); ?></h3>
        </div>
        <div class="c-dropdown-inner">
            <button type="button" class="btn btn-primary j-confirm" data-id="<?= $detect['id'] ?>"><?= _t('geo', 'Да'); ?></button>
            <button type="button" class="btn btn-default j-cancel"><?= _t('geo', 'Нет, выбрать другой'); ?></button>
        </div>
    </div>
<? endif; ?>
<div class="c-dropdown navbar-header-dropdown displaynone" id="j-geo-region-dropdown">
    <? if($countrySelect): ?>
        <? $countries = Geo::countryList(); ?>
        <div class="j-select-country<?= $numlevel != 0 ? ' displaynone' : ''?>">
        <div class="c-dropdown-heading">
            <a href="#" class="close"><i class="fa fa-times"></i></a>
            <h3><?= _t('geo', 'Выберите страну'); ?></h3>
            <div class="c-spacer10"></div>
            <?= _t('geo', 'Искать <a[attr]>по всем странам</a>', array('attr' => ' href="#" class="j-all" data-id="0"')); ?>
        </div>
        <div class="c-dropdown-inner">
            <ul class="navbar-header-dropdown-countries">
                <? foreach($countries as $v): ?>
                <li><a href="#" class="j-select" data-id="<?= $v['id'] ?>" data-noregions="<?= $v['filter_noregions'] ?>"><?= $v['title'] ?></a></li>
                <? endforeach; ?>
            </ul>
        </div>
    </div>
    <? endif; ?>

    <div class="j-select-region<?= $numlevel != Geo::lvlCountry || $cityNoRegion ? ' displaynone' : '' ?>">
        <div class="c-dropdown-heading navbar-header-dropdown-heading">
            <a href="#" class="close visible-xs"><i class="fa fa-times"></i></a>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="input-group">
                        <input type="text" class="form-control j-search-q" placeholder="<?= _t('geo', 'Начните вводить название региона...'); ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default j-search" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
                <div class="col-md-8 col-sm-6 mrgt5">
                    <? if($countrySelect): ?>
                    <div class="pull-right-lg pull-right-md">
                        <a href="#" class="ajax-link j-change"><span><?= _t('geo', 'Изменить страну'); ?></span></a>
                    </div>
                    <? endif; ?>
                    <?= _t('geo', 'Искать <a[attr]>по всей стране</a>', array('attr' => ' href="#" class="j-all" data-id="'.($countrySelect ? $countryID : 0).'"')); ?>
                </div>
            </div>
        </div>
        <div class="c-dropdown-inner j-list"><?= $regionBlock ?></div>
    </div>

    <div class="j-select-city-noregion<?= ! $cityNoRegion ? ' displaynone' : '' ?>">
        <div class="c-dropdown-heading navbar-header-dropdown-heading">
            <a href="#" class="close visible-xs"><i class="fa fa-times"></i></a>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="input-group">
                        <input type="hidden" class="j-city-value" value="<?= $cityID ?>" />
                        <input type="text" class="form-control j-city-ac" placeholder="<?= _t('geo', 'Введите название города...'); ?>" value="<?= $cityTitle ?>" />
                    </div>
                </div>
                <div class="col-md-8 col-sm-6 mrgt5">
                    <? if($countrySelect): ?>
                        <div class="pull-right-lg pull-right-md">
                            <a href="#" class="ajax-link j-change"><span><?= _t('geo', 'Изменить страну'); ?></span></a>
                        </div>
                    <? endif; ?>
                    <?= _t('geo', 'Искать <a[attr]>по всей стране</a>', array('attr' => ' href="#" class="j-all" data-id="'.($countrySelect ? $countryID : 0).'"')); ?>
                </div>
            </div>
        </div>
        <div class="c-dropdown-inner j-list"><?= $regionBlock ?></div>
    </div>

    <div class="j-select-city<?= ($numlevel < Geo::lvlRegion || $cityNoRegion || !empty($districtsBlock)) ? ' displaynone ' : '' ?>">
        <div class="c-dropdown-heading navbar-header-dropdown-heading">
            <a href="#" class="close visible-xs"><i class="fa fa-times"></i></a>
            <h3 class="j-region-title"><?= ! empty($regionTitle) ? $regionTitle : '' ?></h3>
            <div class="c-spacer10"></div>
            <?= _t('geo', 'Искать <a[attr]>по всему региону</a>', array('attr' => ' href="#" class="j-all" data-id="'.$regionID.'"')); ?>
            <div class="pull-right-lg pull-right-md">
                <a href="#" class="ajax-link j-change"><span><?= _t('geo', 'Изменить регион'); ?></span></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="c-dropdown-inner j-list"><?= $cityBlock ?></div>
    </div>
    <?/* City district Block */?>
    <div class="j-select-city<?= (!empty($districtsBlock) && Geo::defaultCity()) ? '' : 'displaynone' ?>">
        <div class="c-dropdown-heading navbar-header-dropdown-heading">
            <a href="#" class="close visible-xs">
                <i class="fa fa-times"></i>
            </a>
            <h3 class="j-region-title">
                <?= ! empty($title) ? $title : '' ?>
            </h3>
            <div class="clearfix"></div>
        </div>
        <div class="c-dropdown-inner j-list"><?= $districtsBlock ?></div>
    </div>
</div>

<script type="text/javascript">
<? js::start();
    $changeLocation = 0;
    if (Geo::filterEnabled()) {
        $urlType = Geo::urlType();
        $changeLocation = in_array($urlType, array(Geo::URL_SUBDIR, Geo::URL_SUBDOMAIN));
    }
?>
    jFilterRegion.init(<?= func::php2js(array(
        'lang' => array(),
        'country' => $countryID,
        'cityNoRegion' => $cityNoRegion,
        'changeLocation' => $changeLocation,
    )) ?>);
<? js::stop(); ?>
</script>