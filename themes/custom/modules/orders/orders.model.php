<?php


class Theme_Custom_OrdersModel extends Theme_Custom_OrdersModel_Base
{
    public function makeEnotifyAboutOpinionsAllowed()
    {
        $aEmailData = $this->db->select('SELECT O.user_id, O.performer_id, O.id, O.keyword, O.title
                                            FROM '.TABLE_ORDERS.' O
                                            WHERE O.status = :status 
                                            AND O.performer_created != \'0000-00-00 00:00:00\'
                                            AND DATE_ADD(O.performer_created, Interval :n DAY) < NOW()
                                            AND DATE_ADD(O.performer_created, Interval :n+1 DAY) > NOW()
                                          ',
            [':status' => Orders::STATUS_CLOSED,
             ':n' => config::get('users_opinions_delay')]);

        foreach ($aEmailData as $item){
            Users::sendMailTemplateToUser(
                (int)$item['user_id'],
                'order_opinions_delay',
                [
                    'order_title' => $item['title'],
                    'order_url'    => Orders::url('view', ['id' => $item['id'], 'keyword' => $item['keyword']]),
                ],
                Users::ENOTIFY_GENERAL);

            Users::sendMailTemplateToUser(
                (int)$item['performer_id'],
                'order_opinions_delay',
                [
                    'order_title' => $item['title'],
                    'order_url'    => Orders::url('view', ['id' => $item['id'], 'keyword' => $item['keyword']]),
                ],
                Users::ENOTIFY_GENERAL);
        }
    }

    /**
     * Список заказов. Поиск на главной
     * @param array $aFilter фильтр списка заказов
     * @param bool $bCount только подсчет кол-ва заказов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function ordersList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        $sFrom = '';
        $bSelectSpec = false;
        if (array_key_exists('spec_id', $aFilter)) {
            $sFrom .= ', ' . TABLE_ORDERS_SPECS . ' S ';
            $aFilter[':jspec'] = 'O.id = S.order_id ';
            $aFilter[':spec_id'] = $this->db->prepareIN('S.spec_id', $aFilter['spec_id']);
            unset($aFilter['spec_id']);
        }
        if( ! empty($aFilter['SelectSpec'])){
            $bSelectSpec = true;
            if( ! isset($aFilter[':jspec'])){
                $sFrom .= ', ' . TABLE_ORDERS_SPECS . ' S ';
                $aFilter[':jspec'] = 'O.id = S.order_id ';
            }
        }

        $bSelectCat = false;
        if (array_key_exists('cat_id', $aFilter)) {
            $sFrom .= ', ' . TABLE_ORDERS_CATS . ' C ';
            $aFilter[':jcat'] = 'O.id = C.order_id ';
            $aFilter[':cat_id'] = $this->db->prepareIN('C.cat_id', $aFilter['cat_id']);
            unset($aFilter['cat_id']);
        }
        if( ! empty($aFilter['SelectCat'])){
            $bSelectCat = true;
            if( ! isset($aFilter[':jcat'])){
                $sFrom .= ', ' . TABLE_ORDERS_CATS . ' C ';
                $aFilter[':jcat'] = 'O.id = C.order_id ';
            }
        }

        if (array_key_exists('userSpecs', $aFilter)) {
            $sFrom .= ', ' . TABLE_USERS_SPECIALIZATIONS . ' US ';
            $aFilter[':jus'] = array('US.spec_id = S.spec_id AND US.user_id = :ususer', ':ususer' => $aFilter['userSpecs']);
            if( ! isset($aFilter[':jspec'])){
                $sFrom .= ', ' . TABLE_ORDERS_SPECS . ' S ';
                $aFilter[':jspec'] = 'O.id = S.order_id ';
            }
            unset($aFilter['userSpecs']);
        }

        unset($aFilter['SelectSpec'], $aFilter['SelectCat']);

        # Фильтр по специализациям пользователя
        if (array_key_exists('userSpecs', $aFilter)) {
            $sFrom .= ', ' . TABLE_USERS_SPECIALIZATIONS . ' US ';
            $aFilter[':jus'] = array('US.spec_id = S.spec_id AND US.user_id = :ususer', ':ususer' => $aFilter['userSpecs']);
            if ( ! isset($aFilter[':jspec'])) {
                $sFrom .= ', ' . TABLE_ORDERS_SPECS . ' S ';
                $aFilter[':jspec'] = 'O.id = S.order_id ';
            }
            unset($aFilter['userSpecs']);
        }

        # фильтр по региону (если есть)
        $bLjRegion = true;
        foreach (array('reg1_country', 'reg2_region', 'reg3_city') as $v) {
            if (!isset($aFilter[$v])) {
                continue;
            }
            if (!isset($aFilter[':jregions'])) {
                $sFrom .= ', ' . TABLE_ORDERS_REGIONS . ' R ';
                $aFilter[':jregions'] = 'O.id = R.order_id ';
                $bLjRegion = false;
            }
            $aFilter[':' . $v] = array('R.' . $v . ' = :v' . $v, ':v' . $v => $aFilter[$v]);
            unset($aFilter[$v]);
        }

        $bTagTT = false;
        # поиск:
        if (isset($aFilter['text'])) {
            $sText = trim($aFilter['text']);
            if ( ! empty($sText)) {
                # поиск по заголовку, описанию
                $aFilter[':text'] = '( '.$this->db->prepareFulltextQuery($sText, 'O.title, O.descr');
                # поиск по тегу (при совпадении с названием тега)
                $aTagIDs = $this->db->select_one_column('SELECT id FROM '.TABLE_TAGS.' WHERE tag LIKE :tag', array(':tag' => $sText));
                if( ! empty($aTagIDs)){
                    $aFilter[':text'] .= ' OR '.$this->db->prepareIN('TT.tag_id', $aTagIDs);
                    $bTagTT = true;
                }
                # поиск по специализации (при совпадении с названием специализации)
                $nSpecID = $this->db->one_data('SELECT id FROM '.TABLE_SPECIALIZATIONS_LANG.' WHERE title = :title', array(':title' => $sText));
                if($nSpecID){
                    $sFrom = ' LEFT JOIN ' . TABLE_ORDERS_SPECS . ' LjS ON LjS.order_id = O.id AND LjS.spec_id = '.$nSpecID.' '.$sFrom;
                    $aFilter[':text'] .=  ' OR LjS.order_id IS NOT NULL ';
                }
                $aFilter[':text'] .= ' )';
            }
            unset($aFilter['text']);
        }

        # фильтр по тегу
        if (array_key_exists('tag_id', $aFilter)) {
            $aFilter[':tag_id'] = array('TT.tag_id = :tag', ':tag' => $aFilter['tag_id']);
            $bTagTT = true;
            unset($aFilter['tag_id']);
        }
        if ($bTagTT) {
            $sFrom .= ', ' . TABLE_ORDERS_IN_TAGS . ' TT ';
            $aFilter[':jtags'] = 'O.id = TT.order_id';
        }

        # скрываем заказы с активированной услугой "Скрытый заказ"
        if ( ! User::id() || isset($aFilter['exclude-svc-hidden'])) {
            $aFilter[':hidden'] = '(O.svc & '.Orders::SVC_HIDDEN.') = 0';
            if (isset($aFilter['exclude-svc-hidden'])) {
                unset($aFilter['exclude-svc-hidden']);
            }
        }
        if( ! isset($aFilter[':moderated']) && $aFilter['status'] != Orders::STATUS_ONACTIVATION) {
            if (Orders::premoderation()) {
                $aFilter[':moderated'] = 'O.moderated > 0';
            }
        }
        $aFilter['visibility'] = Orders::VISIBILITY_ALL;

        $aFilter = $this->prepareFilter($aFilter, 'O');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(id) FROM
                ( SELECT O.id FROM ' . TABLE_ORDERS . ' O ' . $sFrom . $aFilter['where'] . ' GROUP BY O.id ) sl', $aFilter['bind']);
        }

        $aData = $this->db->select('
            SELECT O.id, 
                   O.user_id, 
                   O.type, 
                   O.service_type, 
                   O.status, 
                   O.moderated,
                   O.title, 
                   O.descr, 
                   O.keyword, 
                   O.pro, 
                   O.fairplay, 
                   O.created, 
                   O.offers_cnt, 
                   O.price_vacancy,
                   O.price, 
                   O.price_curr, 
                   O.price_ex, 
                   O.price_rate_text, 
                   O.addr_lat, 
                   O.addr_lng, 
                   O.views_total, 
                   O.expire, 
                   O.district_id,
                   O.start_date,
                   O.is_immediate,
                   ((O.svc & ' . Orders::SVC_MARK . ') > 0) as svc_marked,
                   ((O.svc & ' . Orders::SVC_FIX . ') > 0) as svc_fixed,
                   SP.spec_id,
                   GROUP_CONCAT(T.tag_id) AS tags, MAX(R.reg3_city) AS reg3_city
                   '.($bSelectSpec ? ', GROUP_CONCAT(S.spec_id) AS specs, GROUP_CONCAT(S.cat_id) AS cats' : '').'
                   '.($bSelectCat ? ', GROUP_CONCAT(C.cat_id1) AS cats1, GROUP_CONCAT(C.cat_id2) AS cats2 ' : '').'
            FROM ' . TABLE_ORDERS . ' O
                LEFT JOIN ' . TABLE_ORDERS_IN_TAGS . ' T ON O.id = T.order_id 
                INNER JOIN '.TABLE_ORDERS_SPECS.' SP ON O.id = SP.order_id  ' .
            ($bLjRegion ? ' LEFT JOIN ' . TABLE_ORDERS_REGIONS . ' R ON O.id = R.order_id ' : '') .
            $sFrom . $aFilter['where'] . '
            GROUP BY O.id
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, $aFilter['bind']
        );

        if ( ! empty($aData)) {
            $this->ordersListPrepare($aData);
        } else {
            $aData = array();
        }

        return $aData;
    }

    public function isOpenedContacts($nId)
    {
        $nUserID = User::id();
        if (!$nUserID || !$nId) return false;

        $bResult = $this->db->exec('SELECT EXISTS 
                                          ( SELECT * 
                                                FROM ' . TABLE_USERS_OPENED_CONTACTS . ' 
                                                WHERE user_id = :user_id AND opened_user_id = :id 
                                           ) SUB',
            [':user_id' => $nUserID, ':id' => $nId]);

        if (is_array($bResult) && !empty($bResult)) {
            $bResult = reset($bResult);
        }
        return (boolean)$bResult['SUB'];

    }

    public function changeStatusOnActivation()
    {
        $res = $this->db->exec('UPDATE '.TABLE_ORDERS.' O,
                            (SELECT id
                            FROM '.TABLE_ORDERS.'
                            WHERE `vacancy_activated_to` != \'0000-00-00 00:00:00\' AND `vacancy_activated_to` < NOW() AND status = :st_opend
                            ) SUB
                        SET O.status = :st_on_activation
                        WHERE O.id = SUB.id',
                        [':st_opend'=> Orders::STATUS_OPENED, ':st_on_activation'=> Orders::STATUS_ONACTIVATION]);

    }
}