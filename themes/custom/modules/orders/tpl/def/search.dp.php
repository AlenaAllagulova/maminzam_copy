<?php
use bff\db\Dynprops;
/**
 * @var $this Dynprops
 */
$prefix = 'd';
$prefix_child = 'dc';
$drawControl = function($id, $title, $value, $class = array(), $bOpened = false, $sCustom = ''){
    if( ! is_array($class)) $class = array($class);
    ?>
    <a href="#left-more<?= $id ?>" data-toggle="collapse" data-parent="#accordion" class="form-sidebar__title active">
        <?= $title ?>
        <i class="icon-arrow-point-to-down"></i>
    </a>

    <div class="collapse in j-dp j-collapse" id="left-more<?= $id ?>">
        <? if( ! empty($sCustom)): echo($sCustom); else: ?>
            <div class="row">
                <div class=" <?= join(' ', $class) ?>">
                    <?= $value ?>
                </div>
            </div>
        <? endif; ?>
    </div>
    <?
};

$lng = array(
    'range_from' => _t('','от'),
    'range_to'   => _t('','до'),
);

$bOnlyChild = ! empty($extra['aOnlyChild']['dp_id']);

# ---------------------------------------------------------------------------------------
# Дин. свойства:
foreach($dynprops as $d)
{
    $ID = $d['id'];
    if($bOnlyChild && $ID != $extra['aOnlyChild']['dp_id']) continue;

    $name = $prefix.'['.$d['data_field'].']';
    $d['value'] = ( isset($extra['filter']['d'][ $d['data_field'] ]) ? $extra['filter']['d'][ $d['data_field'] ] : '');

    switch($d['type'])
    {
        case Dynprops::typeSelect: # Выпадающий список
        case Dynprops::typeSelectMulti: # Список с мультивыбором
        case Dynprops::typeRadioGroup: # Группа св-в с единичным выбором
        case Dynprops::typeCheckboxGroup: # Группа св-в с множественным выбором
        {
            $values = (isset($d['value']) && $d['value'] ? $d['value'] : array());
            if($bOnlyChild && empty($values)){
                $values = array($extra['aOnlyChild']['dp_id'] => $extra['aOnlyChild']['dp_value']);
            }
            $this->input->clean($values, TYPE_ARRAY_UINT);

            if( ! empty($d['multi']) ) {
                if( isset($d['multi'][0]) && empty($d['multi'][0]['value']) ) {
                    unset($d['multi'][0]); # удаляем вариант "-- выберите --"
                }
                $html = '';
                $bOpen = false;
                foreach($d['multi'] as $k => $v){
                    $bChecked = false;
                    if(in_array($v['value'], $values)){
                        $bChecked = true;
                        $bOpen = true;
                    }
                    $html .= '<div class="checkbox"> 
                                <label> 
                                    <input type="checkbox" name="'.$name.'['.$k.']" '.($d['parent'] ? 'class="j-dp-parent" data-id="'.$ID.'"' : '').' autocomplete="off"
                                    '.($bChecked ? ' checked="checked"':'').' value="'.$v['value'].'" data-num="'.$v['num'].'" />
                                    <span class="checkbox__text">'.$v['name'].'</span>
                                    </label>
                            </div>';
                }
                if( ! $bOnlyChild){
                    $drawControl($ID, $d['title_'.LNG], $html, 'col-sm-12', $bOpen);
                }
            }
            if( $d['parent'] )
            {
                if( ! empty($d['multi']) ) {
                    $aPairs = array();
                    foreach($values as $v) $aPairs[] = array('parent_id'=>$ID,'parent_value'=>$v);
                    $aChildren = $this->getByParentIDValuePairs($aPairs, true);
                    $aChildren = ( ! empty($aChildren[$ID]) ? $aChildren[$ID] : array() );
                    foreach($d['multi'] as $k=>$m)
                    {
                        if( empty($aChildren[$m['value']]) ) continue;

                        $dd = $aChildren[$m['value']];
                        # CHILD: реализуем поддержку типов, формирующих вывод в виде checkbox-списков
                        if( ! in_array($dd['type'], array(Dynprops::typeSelect, Dynprops::typeSelectMulti, Dynprops::typeRadioGroup, Dynprops::typeCheckboxGroup)) )
                            continue;
                        # CHILD: находим отмеченные(active=1), формируем "текст отмеченных"($btn_value)
                        if( isset($extra['filter']['dc'][ $dd['data_field'] ][ $dd['id'] ]) ) {
                            foreach($dd['multi'] as $kk=>$mm) {
                                if( in_array($mm['value'], $extra['filter']['dc'][ $dd['data_field'] ][ $dd['id'] ]) ) {
                                    $dd['multi'][$kk]['active'] = 1;
                                }
                            }
                        }
                        # CHILD: выводим checkbox-списки с заголовками
                        $name_child = $prefix_child.'['.$dd['data_field'].']['.$dd['id'].']';
                        $html = '';
                        $bOpen = false;
                        foreach($dd['multi'] as $kk => $vv){
                            $bChecked = false;
                            if( ! empty($vv['active'])){
                                $bChecked = true;
                                $bOpen = true;
                            }

                            $html .= '<div class="checkbox"> 
                                            <label> 
                                                <input type="checkbox" name="'.$name_child.'[]" autocomplete="off"
                                                '.($bChecked ? ' checked="checked"':'').' value="'.$vv['value'].'" data-num="'.$vv['num'].'" />
                                                <span class="checkbox__text">'.$vv['name'].'</span>
                                            </label>
                            </div>';
                        }
                        ?><div id="j-dp-child-<?= $ID.'-'.$m['value'] ?>" class="j-dp-child-<?= $ID ?>" data-num="<?= $m['num']?>"><? $drawControl($ID.'-'.$m['value'], $m['name'], $html, 'col-sm-12', $bOpen); ?></div><?
                    }
                }
            }
        } break;
        case Dynprops::typeRadioYesNo: # Выбор Да/Нет
        {
            $html = '<div class="checkbox-inline"> <label> <input type="checkbox" name="'.$name.'" value="2" '.( ! empty($d['value']) ? 'checked="checked"':'').' autocomplete="off" /> '.$this->langText['yes'].' </label> </div>';
            $drawControl($ID, $d['title_'.LNG], $html, 'col-sm-12');
        } break;
        case Dynprops::typeCheckbox: # Флаг
        {
            $html = '<div class="checkbox-inline"> <label> <input type="checkbox" name="'.$name.'" value="1" '.( ! empty($d['value']) ? 'checked="checked"':'').' autocomplete="off" /> '.$this->langText['yes'].' </label> </div>';
            $drawControl($ID, $d['title_'.LNG], $html, 'col-sm-12');
        } break;

        case Dynprops::typeNumber: # Число
        case Dynprops::typeRange: # Диапазон
        {
            $value = ( is_array($d['value']) ? $d['value'] : array() );
            $this->input->clean_array($value, array(
                'f' => TYPE_UNUM, # от (пользовательский вариант)
                't' => TYPE_UNUM, # до (пользовательский вариант)
                'r' => TYPE_ARRAY_UINT, # отмеченные диапазоны (ranges)
            )); $from = $value['f']; $to = $value['t']; if($from && $to && $from>=$to) $from = $value['f'] = 0;

            $sel = false;
            $custom = ! empty($d['search_range_user']) || empty($d['search_ranges']);
            if ($this->searchRanges) {
                foreach($d['search_ranges'] as $k=>$i){
                    $d['search_ranges'][$k]['title'] = $i['title'] = ($i['from'] && $i['to'] ? $i['from'].'...'.$i['to'] : ($i['from'] ? '> '.$i['from'] : '< '.$i['to']));
                    if($sel === false && in_array($k, $value['r'])) {
                        $sel = $i['title'];
                    }
                }
            }
            $html = '';
            $bOpen = false;
            if ($custom)
            {
                $bOpen = (bool)($from || $to);
                $html .= '<div class="l-inside l-range"> <div class="form-inline"> '.
                    '<div class="form-group"><input type="text" class="form-control input-sm j-from" name="'.$name.'[f]" value="'.($from ?: '').'" placeholder="'.$lng['range_from'].'"/></div>'.
                    '<div class="form-group"><input type="text" class="form-control input-sm j-from" name="'.$name.'[t]" value="'.($to ?: '').'"  placeholder="'.$lng['range_to'].'"/></div>'.
                        '</div></div>';

                    /*
                    if($d['parent'] && isset($aData['children'][$ID])) {
                        $childForm = $self->formChild($aData['children'][$ID], array('name'=>$prefix_child), true);
                        ?><span><?= $childForm; ?></span><?
                    } */
            }
            if ($this->searchRanges)
            {
                $html .= '<div class="l-inside"> <div class="row"> <div class="col-sm-12"> ';
                foreach($d['search_ranges'] as $k => $v){
                    $bChecked = false;
                    if(in_array($k, $value['r'])){
                        $bChecked = true;
                        $bOpen = true;
                    }
                    $html .= '<div class="checkbox"> <label> <input type="checkbox" name="'.$name.'[r][]" autocomplete="off"
                             '.($bChecked ? ' checked="checked"':'').' value="'.$k.'" /><span class="checkbox__text">'.$v['title'].'</span></label></div>';
                }
                $html .= '</div></div></div>';
            }
            $drawControl($ID, $d['title_'.LNG], '', '', $bOpen, $html);

        } break;

    }

}