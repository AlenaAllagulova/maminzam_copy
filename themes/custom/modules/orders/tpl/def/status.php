<?php
    /**
     * @var $this Orders
     */
    $nOrderId = $this->input->getpost('id');
?>
<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
            <? if ($status == Orders::STATUS_NOTACTIVATED) { ?>
                <h1 class="small text-center"><?= _t('orders', 'Необходима активация аккаунта') ?></h1>
                <? if( ! empty($phone_form)): ?>
                    <?= $phone_form ?>
                <? else: ?>
                    <p><?= _t('orders', 'На указанный e-mail отправлено письмо. Для активации аккаунта перейдите по ссылке указанной в письме.') ?></p>
                <? endif; ?>
            <? } else if($status == Orders::STATUS_ONACTIVATION){ ?>
                <? if (Orders::premoderation()) { # включена премодерация ?>
                    <h1 class="small"><?= _t('orders', 'Ваш заказ будет доступен для активации после модерации') ?></h1>
                    <p><?= _t('orders', 'Ваш заказ сохранен, но еще не опубликован.<br> Вы можете увидеть и отредактировать его в <a [link_cabinet]>вашем кабинете</a>.<br> Как только наш модератор проверит корректность введенных вами данных, заказ появится в списке.',
                            array('link_cabinet' => 'href="'.Orders::url('my.orders').'"')) ?><br /></p>
                    <p><a href="<?= Orders::url('add') ?>"><?= _t('orders', 'Добавить еще один заказ') ?></a></p>
                    <p><a href="<?= Orders::url('my.orders') ?>"><?= _t('orders','Перейти на страницу списка заказов') ?></a></p>
                <? } else { ?>
                    <h1 class="small"><?= _t('orders', 'Ваш заказ будет доступен исполнителям после активации') ?></h1>
                    <p><?= _t('orders', 'Ваш заказ сохранен, но еще не опубликован.<br> Вы можете увидеть и отредактировать его в <a [link_cabinet]>вашем кабинете</a>.<br>',
                            array('link_cabinet' => 'href="'.Orders::url('my.orders').'"')) ?><br /></p>
                    <p><a href="<?= Orders::url('activation', ['id' => $nOrderId]) ?>"><?= _t('orders', 'Активировать заказ') ?></a></p>
                    <p><a href="<?= Orders::url('add') ?>"><?= _t('orders', 'Добавить еще один заказ') ?></a></p>
                    <p><a href="<?= Orders::url('my.orders') ?>"><?= _t('orders','Перейти на страницу списка заказов') ?></a></p>

                <?}?>
            <? } else { ?>
                <? if (Orders::premoderation()) { # включена премодерация ?>
                    <h1 class="small"><?= _t('orders', 'Ваш заказ будет опубликован после проверки модератором') ?></h1>
                    <p><?= _t('orders', 'Ваш заказ сохранен, но еще не опубликован.<br> Вы можете увидеть и отредактировать его в <a [link_cabinet]>вашем кабинете</a>.<br> Как только наш модератор проверит корректность введенных вами данных, заказ появится в списке.',
                            array('link_cabinet' => 'href="'.Orders::url('my.orders').'"')) ?><br /></p>
                    <p><a href="<?= Orders::url('add') ?>"><?= _t('orders', 'Добавить еще один заказ') ?></a></p>
                    <p><a href="<?= Orders::url('my.orders') ?>"><?= _t('orders','Перейти на страницу списка заказов') ?></a></p>
                <? } else { ?>
                    <h1 class="small"><?= _t('orders', 'Ваш заказ успешно опубликован.') ?></h1>
                    <p><?= _t('orders', 'Чтобы увидеть его в общем списке заказов перейдите на <a [link_list]>страницу заказов</a>.',
                            array('link_list' => 'href="'.Orders::url('list').'"')) ?><br />
                        <?= _t('orders', 'Вы также можете отредактировать его в <a [link_cabinet]>вашем кабинете</a>.',
                            array('link_cabinet' => 'href="'.Orders::url('my.orders').'"')) ?><br />
                    </p>
                    <p><a href="<?= Orders::url('add') ?>"><?= _t('orders', 'Добавить еще один заказ') ?></a></p>
                <? }
            } ?>
            </div>
        </div>
    </section>
</div>