<?php

/**
 * Список заказов пользователя (только список)
 * @var $this Orders
 * @var $user array
 */

$aServiceTypes = Orders::aServiceTypes();
$aOfferStatuses = array(
    Orders::OFFER_STATUS_CANDIDATE => array(
        'id' => Orders::OFFER_STATUS_CANDIDATE,
        't'  => _t('orders-offers', 'Вас выбрали кандидатом. У Вас больше шансов стать няней в этой семье.'),
        'c'  => 'alert-candidate',
        'fa' => '/img/stars.svg'
    ),
    Orders::OFFER_STATUS_PERFORMER => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER,
        't'  => _t('orders-offers', 'Вас выбрали исполнителем.'),
        'c'  => 'alert-chosen',
        'fa' => 'fa-trophy text-orange'
    ),
    Orders::OFFER_STATUS_CANCELED  => array(
        'id'  => Orders::OFFER_STATUS_CANCELED,
        't'  => _t('orders', 'Вам отказали по этому объявлению. Пора искать новые предложения.'),
        'c'  => 'alert-decline',
        'fa' => '/img/sad.svg'
    ),
    Orders::OFFER_STATUS_PERFORMER_START => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER_START,
        't'  => _t('orders', 'Вам предложили стать исполнителем.'),
        'c'  => 'alert-chosen',
        'fa' => 'fa-trophy text-orange'
    ),
    Orders::OFFER_STATUS_PERFORMER_DECLINE => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER_DECLINE,
        't'  => _t('orders-offers', 'Вы отказались от заказа.'),
        'c'  => 'alert-decline',
        'fa' => 'fa-times'
    ),
);
$userID = user::id();
$tagsLimit = Orders::searchTagsLimit();
$ordersOpinions = Orders::ordersOpinions();
$fairplayEnabled = bff::fairplayEnabled();
if( ! empty($list)): ?>
    <ul class="order">
        <? foreach($list as $v): ?>
        <li class="j-order" data-id="<?= $v['id'] ?>">

            <div class="row order__box">
                <div class="col-md-9 col-sm-12">
                    <div class="order__item">
                        <div class="order__header">
                            <div class="flex flex_column flex_center-sm">
                                <div class="order__status">
                                    <?if(isset($aOrderMainStatuses[$v['status']])):?>
                                        <span class="<?= $aOrderMainStatuses[$v['status']]['class']?>">
                                                    <i></i>
                                            <?= $aOrderMainStatuses[$v['status']]['order_t']?>
                                                </span>
                                    <? endif;?>
                                </div>
                                <div class="flex flex_center flex_wrap">
                                    <? if($v['visibility'] == Orders::VISIBILITY_PRIVATE): ?>
                                        <i class="fa fa-eye-slash show-tooltip mrgr5"
                                           data-toggle="tooltip" data-placement="top"
                                           title="<?= _t('orders', 'Приватный заказ'); ?>"></i>
                                    <? endif; ?>
                                    <? if($fairplayEnabled && $v['fairplay']): ?>
                                        <i class="fa fa-shield c-safe-color show-tooltip mrgr5"
                                           data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>"
                                           title="" data-placement="top" data-toggle="tooltip"></i>
                                    <? endif; ?>
                                    <a href="<?= $v['url_view'] ?>" class="order__title"><?= $v['title'] ?></a>
                                    <? if($v['pro']): ?>
                                        <div class="">
                                            <span class="pro mrgl5">pro</span>
                                        </div>
                                    <? endif; ?>
                                </div>
                                <? if( ! empty($v['city_data']['title'])): ?>
                                    <div class="order__text">
                                        <?= $v['city_data']['title']?>
                                    </div>
                                <? endif; ?>
                                <? if(!empty($v['district_id'])):?>
                                    <div class="order__text">
                                        <?= Geo::districtTitle($v['district_id'])?>
                                    </div>
                                <?endif;?>
                            </div>
                            <div class="order__price">
                                <? if($v['type'] == Orders::TYPE_SERVICE): ?>
                                    <? if($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                                        <?= ! empty($v['price_rate_text'][LNG]) ? $v['price_rate_text'][LNG] : _t('orders', 'По договоренности'); ?>
                                    <? else: ?>
                                        <?= tpl::formatPrice($v['price']) ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?>
                                        <? if( ! empty($v['price_rate_text'][LNG])): ?>
                                            <?= $v['price_rate_text'][LNG] ?>
                                        <? endif; ?>
                                    <? endif; ?>
                                <? endif; ?>
                            </div>
                        </div>
                        <div class="order__conditions mrgt10">
                            <? if (!empty($v['dynprops_simple'])): # краткий вывод динсвойств patr 2 c обработкой результата ?>
                                <? foreach ($v['dynprops_simple'] as $dp_item): ?>
                                    <? if ($dp_item['group_id'] == Orders::DP_ORDER_VIEW_PART_SECOND): ?>
                                        <div class="order__conditions-item">
                                            <? if ($dp_item['value'] !== ''): ?>
                                                <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                                    <? foreach ($dp_item['multi'] as $val): ?>
                                                        <? if ($dp_item['value'] == $val['value']):
                                                            if (strpos($val['name'], '(')) {
                                                                $val['name'] = substr($val['name'], strpos($val['name'], '(') + strlen('('));
                                                                $val['name'] = strstr($val['name'], ')', true);
                                                            } else {
                                                                $val['name'] = (int)$val['name'];
                                                            }
                                                            ?>
                                                            <div class="bold"><?= $val['name'] ?></div>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                <? else: ?>
                                                    <div class="bold">
                                                        <? foreach ($dp_item['value'] as $dp_val): ?>
                                                            <? foreach ($dp_item['multi'] as $val): ?>
                                                                <? if ($dp_val == $val['value']):
                                                                    if (strpos($val['name'], '(')) {
                                                                        $val['name'] = substr($val['name'], strpos($val['name'], '(') + strlen('('));
                                                                        $val['name'] = strstr($val['name'], ')', true);
                                                                    } else {
                                                                        $val['name'] = (int)$val['name'];
                                                                    }
                                                                    ?>
                                                                    <?= $val['name'] ?> <br>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? endforeach; ?>
                                                    </div>
                                                <? endif; ?>
                                            <? else: ?>
                                                <th><?= _t('dp_orders', 'не указано') ?></th>
                                            <? endif; ?>
                                            <span class="order__second-text"><?= $dp_item['description_' . LNG] ?></span>
                                        </div>
                                    <? endif; ?>
                                <? endforeach; ?>
                            <? endif; ?>
                            <div class="order__conditions-item">
                                <div class="bold">
                                    <?= ( $v['start_date'] != '0000-00-00 00:00:00')? tpl::date_format2( $v['start_date'], false, true): _t('', 'не указана'); ?>
                                </div>
                                <span class="order__second-text">
                                    <?= _t('','Дата начала')?>
                                </span>
                            </div>
                        </div>
                        <p class="order__text mrgt10 order__desc">
                            <?= tpl::truncate($v['descr'], config::sysAdmin('orders.search.list.descr.truncate', 250, TYPE_UINT)); ?>
                        </p>
                        <ul class="order-info">
                            <li>
                                <?= _t('Orders','Опубликовано:')?>
                                <?= tpl::date_format_spent($v['created'], false, true) ?>
                            </li>
                            <li>
                                <?=_t('', 'Просмотров [cnt]', ['cnt' => $v['views_total']])?>
                            </li>
                            <li>
                                <a href="<?= $v['url_view'] ?>#offers">
                                    <span>
                                        <?= tpl::declension($v['offers_cnt'], _t('orders', 'отклик;отклика;откликов'))?>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-5  order__notif">

                    <? if( ! empty($aOfferStatuses[ $v['offer_status'] ])): $aStatus = $aOfferStatuses[ $v['offer_status'] ]; ?>

                        <div class="text-center mrgb10">
                            <? if( ! empty($aStatus['fa'])): ?>
                                <div class="">
                                    <img src="<?= bff::url($aStatus['fa']) ?>" alt="" width="35">
                                </div>
                            <? endif; ?>
                            <span class="order__text">
                                <?= $aStatus['t'] ?>
                            </span>
                            <? if($ordersOpinions && $v['offer_status'] == Orders::OFFER_STATUS_PERFORMER_START): ?>
                                <div class="p-profile-alert-controls">
                                    <a href="#" class="btn btn-success btn-sm j-performer-start-agree" data-id="<?= $v['offer_id'] ?>">
                                        <?= _t('orders', 'Подтвердить заказ'); ?>
                                    </a>
                                </div>
                            <? endif; ?>
                        </div>
                    <? endif; ?>
                    <? if($ordersOpinions && $v['my'] && $v['offer_status'] == Orders::OFFER_STATUS_PERFORMER_AGREE):
                        if($fairplayEnabled): ?>
                            <div class="alert alert-chosen o-freelancer-defined p-profile-alert">
                                <span>
                                    <?= _t('orders', 'Вас выбрали исполнителем'); ?>
                                </span>
                                <? if(isset($v['workflow']['url'])): ?>
                                    <div class="p-profile-alert-controls">
                                        <a href="<?= $v['workflow']['url'] ?>" class="ajax-link">
                                            <i class="fa fa-rocket c-link-icon"></i>
                                            <span><?= _t('fp', 'Ход работ'); ?></span>
                                        </a>
                                    </div>
                                <? endif; ?>
                            </div>
                        <? else: ?>
                            <div class="text-center mrgb10">
                                <?  $meEnd = isset($v['opinions'][ $userID ]); if($meEnd) { $myOpinion = $v['opinions'][ $userID ]; }
                                $clientEnd = isset($v['opinions'][ $v['order_user_id'] ]); if($clientEnd) { $clientOpinion = $v['opinions'][ $v['order_user_id'] ]; }
                                ?>
                                <div class="">
                                    <? if (!$meEnd && !$clientEnd): ?>
                                        <img src="<?= bff::url('/img/confetti.svg')?>" alt="" width="35">
                                    <? endif; ?>

                                </div>
                                <span class="order__text">
                                    <?= $meEnd ? _t('orders', 'Заказ завершон')
                                        : ( $clientEnd ? _t('orders', 'Заказчик [link] завершил(а) заказ.', array('link' => tpl::userLink($clientOpinion, 'no-login no-verified no-pro')))
                                            : _t('orders', 'Вы приняты на работу. Поздравляем!')) ?>
                                </span>
                                <div class="">
                                    <? if($clientEnd): ?>
                                        <a href="#" class="link-bold j-opinion-show" data-id="<?= $clientOpinion['id'] ?>">
                                            <?= _t('orders', 'Посмотртеть отзыв'); ?>
                                        </a>
                                    <? endif; ?>
                                    <? if($meEnd): ?>
                                        <div class="">
                                            <a href="#" class="link-bold  mrgt10 j-opinion-show" data-id="<?= $myOpinion['id'] ?>">
                                                <?= _t('orders', 'Ваш отзыв'); ?>
                                            </a>
                                        </div>
                                    <? else: ?>
                                        <? if ($clientEnd):?>
                                            <button type="button" class="btn mrgt10 btn-block btn-primary mrgb10 j-opinion-add" data-id="<?= $v['id'] ?>" data-order_id="<?= $v['id'] ?>"><?=  _t('orders', 'Оставить отзыв'); ?></button>
                                        <? endif; ?>
                                    <? endif; ?>
                                </div>
                            </div>

                        <? endif; ?>
                    <? endif; ?>

                    <? if ($v['my'] && $v['offer_status'] != Orders::OFFER_STATUS_PERFORMER_AGREE
                        && $v['offer_status'] != Orders::OFFER_STATUS_PERFORMER
                        && $v['offer_status'] != Orders::OFFER_STATUS_CANDIDATE
                        && $v['offer_status'] != Orders::OFFER_STATUS_CANCELED
                        && $v['offer_status'] != Orders::OFFER_STATUS_PERFORMER_START
                        && $v['offer_status'] != Orders::OFFER_STATUS_PERFORMER_DECLINE
                        && $v['offer_status'] != Orders::OFFER_STATUS_INVITE_DECLINE
                    ): ?>
                        <div class="text-center mrgb10">
                            <div class="">
                                <img src="<?= bff::url('/img/waving-hand.svg') ?>" alt="" width="35">
                            </div>
                            <span class="order__text">
                                <?= _t('','Вы отправили отклик. Теперь родитель рассмотрит Вашу кандидатуру.')?>
                            </span>
                        </div>
                    <? endif; ?>

                    <? if($v['my']): ?>
                        <?= $this->getOrderNote($v['id']);?>
                    <? endif; ?>
                </div>
            </div>

        </li>
        <? endforeach; ?>
    </ul>
<? else: ?>

    <div class="mrgt10 text-center">
        <div class="max-w250 m0-a">
            <?= _t('','У Вас еще нет заказов. Воспользуйтесь поиском, чтобы найти подходящую семью.')?>
            <a href="<?= Users::url('my.settings', array('tab' => 'questionary')); ?>" class="mrgt10 btn btn-primary btn-block">
                <?= _t('users', 'Заполнить анкету'); ?>
            </a>
        </div>
    </div>
<? endif; ?>