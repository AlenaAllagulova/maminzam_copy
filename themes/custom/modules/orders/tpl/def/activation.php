<?php
$cur = Site::currencyDefault();
$isPayable = (Users::model()->userBalance() - $price_vacancy) >= 0;
?>
<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <?= tpl::getBreadcrumbs($breadcrumbs); ?>
                <div class="p-profileContent" id="j-order-block">
                    <?= $list ?>
                </div>
                <form method="post" action="" id="j-order-activation">
                    <input type="hidden" name="id" value="<?=$id?>">
                    <div class="panel-group o-advertise-accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="j-accordion">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="" value="" disabled="disabled" checked="checked"/>
                                            <i class="fa fa-dollar"></i>
                                            <span>
                                                <?=_t('vacancy','Активация вакансии на')?>
                                                <?= tpl::declension($spec_data['period_vacancy'],  _t('', 'день;дня;дней'), true);?>
                                            </span>
                                            <span class="j-vacancy-days"></span>
                                            <div class="o-advertise-price"><span class="j-vacancy-price"><?=$price_vacancy?></span> <?= Site::currencyDefault(); ?></div>
                                        </label>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <h4><?= _t('svc', 'Выберите способ оплаты'); ?></h4>
                    <?= Bills::i()->payMethodsList(); ?>

                    <p>
                    <h5 class="mrgt0 text-center-mobile"><?= _t('svc', 'Всего к оплате:'); ?> <span class="j-sum"><?= $price_vacancy?></span> <?= $cur ?></h5>
                    </p>

                    <div class="c-formSubmit">
                        <button type="submit" class="min-w-200px btn btn-primary j-submit <?= $price_vacancy<0 ? 'hidden' : '' ?>"><?= _t('svc', 'Оплатить'); ?></button>
                        <a class="j-cancel" href="<?=Orders::url('status', ['id' => $id])?>"><?= _t('form', 'Отмена'); ?></a>
                    </div>

                </form>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    <? js::start() ?>
        $(function()
        {
            app.form($('#j-order-activation'), function(){
                if (! Boolean('<?= $isPayable ?>')) {
                    app.alert.error('<?= _t('','На Вашем счету недостаточно средств, пополните и вернитесь к активации заказа ') ?>');
                    return false;
                }

                var f = this;

                f.ajax(bff.ajaxURL('orders&ev=activation'),{},function(resp, errors){

                    if(resp && resp.success) {
                        if(resp.form){
                            jBillsSelectPay.pay(resp.form);
                        }else{
                            app.alert.success('<?= _t('','Активация заказа прошла успешно') ?>');
                            window.setTimeout(function(){
                                console.log(resp.redirect);
                                if(resp.redirect){
                                    bff.redirect(resp.redirect);
                                }else{
                                    location.reload();
                                }
                            }, 1000);
                        }

                    } else {
                        f.alertError(errors);
                        if(resp.redirect){
                            bff.redirect(resp.redirect);
                        }
                    }
                });
            }, {noEnterSubmit: true});


        });

    <? js::stop() ?>
</script>
