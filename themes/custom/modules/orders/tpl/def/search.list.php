<?php
tpl::includeJS(['dist/custom']);

$bPromote = ! empty($bPromote);
$isCurrentUserWorker = User::isWorker();
if( ! empty($list)):
    $lng_offers = _t('orders', 'отклик;отклика;откликов');
    $aServiceTypes = Orders::aServiceTypes();
    $tagsLimit = Orders::searchTagsLimit();
    $fairplayEnabled = bff::fairplayEnabled();
    ?>
    <? if($bPromote): ?>
    <div class="order mrgt25i">
<? else: ?>
    <ul class="order mrgt25i">
<? endif; ?>
    <? foreach($list as $v): ?>
        <<?= $bPromote ? 'div' : 'li' ?> class=" j-order">

            <div class="row order__box ">
                <div class="<?= $bPromote ? 'col-md-8 col-md-offset-2' : 'col-md-9' ?> col-sm-12">
                    <div class=" order__item <?= $v['svc_marked'] ? ' select' : '' ?>">
                        <div class="order__header">
                            <div class="flex flex_column flex_center-sm">
                                <div class="flex flex_center flex_wrap">
                                    <? if($v['status'] == Orders::STATUS_CLOSED): ?>
                                        <i class="fa fa-lock show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Закрытый заказ'); ?>"></i>
                                    <? endif; ?>
                                    <? if($v['svc_fixed'] || $bPromote): ?>
                                        <i class="fa fa-thumb-tack c-icon-fixed<?= ! $v['svc_fixed'] ? ' j-fixed hidden' : '' ?>"></i>
                                    <? endif; ?>
                                    <? if($fairplayEnabled && $v['fairplay']): ?>
                                        <i class="fa fa-shield c-safe-color show-tooltip" data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip"></i>
                                    <? endif; ?>
                                    <? if($v['is_immediate']): ?>
                                        <i class="fa fa-fire" aria-hidden="true"></i>
                                    <? endif; ?>
                                    &nbsp;<a class="order__title" href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a>
                                    <? if($v['pro']): ?>
                                        <div class="">
                                            <span class="pro mrgl5">pro</span>
                                        </div>
                                    <? endif; ?>
                                </div>
                                <? if(!empty($v['district_id'])):?>
                                    <div class="order__text">
                                        <?= Geo::districtTitle($v['district_id'])?>
                                    </div>
                                <?endif;?>
                            </div>
                            <div class="order__price">
                                <? if($v['type'] == Orders::TYPE_SERVICE): ?>
                                    <? if($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                                        <?= ! empty($v['price_rate_text'][LNG]) ? $v['price_rate_text'][LNG] : _t('orders', 'По договоренности'); ?>
                                    <? else: ?>
                                        <?= tpl::formatPrice($v['price']) ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?>
                                        <? if( ! empty($v['price_rate_text'][LNG])): ?>
                                            <?= $v['price_rate_text'][LNG] ?>
                                        <? endif; ?>
                                    <? endif; ?>
                                <? endif; ?>
                            </div>
                        </div>

                        <div class="order__conditions mrgt10">
                            <? if (!empty($v['dynprops_simple'])): # краткий вывод динсвойств patr 2 c обработкой результата ?>
                                <? foreach ($v['dynprops_simple'] as $dp_item): ?>
                                    <? if ($dp_item['group_id'] == Orders::DP_ORDER_VIEW_PART_SECOND ): ?>
                                        <div class="order__conditions-item">
                                            <? if($dp_item['value'] !== ''):?>
                                                <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                                    <? foreach ($dp_item['multi'] as $val): ?>
                                                        <? if ($dp_item['value'] == $val['value']):
                                                            if(strpos($val['name'],'(')){
                                                                $val['name'] = substr( $val['name'] , strpos($val['name'],'(')+strlen('('));
                                                                $val['name'] = strstr($val['name'], ')', true);
                                                            }else{
                                                                $val['name'] = (int)$val['name'];
                                                            }
                                                            ?>
                                                            <div class="bold"><?= $val['name'] ?></div>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                <? else: ?>
                                                    <div class="bold">
                                                        <? foreach ($dp_item['value'] as $dp_val): ?>
                                                            <? foreach ($dp_item['multi'] as $val): ?>
                                                                <? if ($dp_val == $val['value']):
                                                                    if(strpos($val['name'],'(')){
                                                                        $val['name'] = substr( $val['name'] , strpos($val['name'],'(')+strlen('('));
                                                                        $val['name'] = strstr($val['name'], ')', true);
                                                                    }else{
                                                                        $val['name'] = (int)$val['name'];
                                                                    }
                                                                    ?>
                                                                    <?= $val['name'] ?> <br>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? endforeach; ?>
                                                    </div>
                                                <? endif; ?>
                                            <? else: ?>
                                                <th><?= _t('dp_orders','не указано') ?></th>
                                            <? endif;?>
                                            <span class="order__second-text"><?= $dp_item['description_'.LNG] ?></span>
                                        </div>
                                    <? endif; ?>
                                <? endforeach; ?>
                            <? endif; ?>
                            <div class="order__conditions-item">
                                <div class="bold">
                                    <?= ( $v['start_date'] != '0000-00-00 00:00:00')? tpl::date_format2( $v['start_date'], false, true): _t('', 'не указана'); ?>
                                </div>
                                <span class="order__second-text">
                                    <?= _t('orders','Дата начала')?>
                                </span>
                            </div>
                        </div>
                        <p class="order__text mrgt10 order__desc">
                            <?= tpl::truncate($v['descr'], config::sysAdmin('orders.search.list.descr.truncate', 250, TYPE_UINT)); ?>
                        </p>
                        <ul class="order-info">

                            <li>
                                <?= _t('Orders','Опубликовано:')?>
                                <?= tpl::date_format_spent($v['created'], false, true) ?>
                            </li>

                            <li>
                                <?= _t('Orders','Просмотров:')?>
                                <?= $v['views_total']?>
                            </li>

                            <li>
                                <a href="<?= $v['url_view'] ?>#offers">
                                    <span><?= tpl::declension($v['offers_cnt'], $lng_offers)?></span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <? if(!$bPromote):?>
                <div class="col-md-3 col-sm-5  order__notif">
                    <? if($isCurrentUserWorker && $v['moderated'] && isset($v['offer_status'])):?>
                        <? if(!is_array($v['offer_status']) && empty($v['offer_status'])):?>
                            <div class="" id="j-offer-btn-block-<?=$v['id']?>">
                                <a href="javascript:void(0)"
                                   class="btn btn-primary btn-block"
                                   onclick="Custom.simple_offer_add(<?=$v['id']?>)">
                                    <?= _t('','Отправить отклик')?>
                                </a>
                            </div>
                            <div class="text-center hidden" id="j-offer-add-success-<?=$v['id']?>">
                                <img width="35" src="<?= bff::url('/img/waving-hand.svg')?>" alt="">
                                <div class="simple-text mrgt10">
                                    <?= _t('','Вы отправили отклик. Теперь родитель рассмотрит Вашу кандидатуру.')?>
                                </div>
                            </div>
                        <? endif;?>
                        <? if( is_array($v['offer_status']) && $v['offer_status']['status'] == Orders::OFFER_STATUS_ADD):?>
                            <div class="text-center">
                                <img width="35" src="<?= bff::url('/img/waving-hand.svg')?>" alt="">
                                <div class="simple-text mrgt10">
                                    <?= _t('','Вы отправили отклик. Теперь родитель рассмотрит Вашу кандидатуру.')?>
                                </div>
                            </div>
                        <? endif;?>
                        <? if(is_array($v['offer_status']) && $v['offer_status']['status'] == Orders::OFFER_STATUS_CANDIDATE):?>
                            <div class="text-center ">
                                <img width="35" src="<?= bff::url('/img/stars.svg')?>" alt="">
                                <div class="simple-text mrgt10">
                                    <?= _t('','Вас выбрали кандидатом. У Вас больше шансов стать няней в этой семье.')?>
                                </div>
                            </div>
                        <? endif;?>
                        <? if(is_array($v['offer_status']) && $v['offer_status']['status'] == Orders::OFFER_STATUS_CANCELED):?>
                            <div class="text-center ">
                                <img width="35" src="<?= bff::url('/img/sad.svg')?>" alt="">
                                <div class="simple-text mrgt10">
                                    <?= _t('','Вам отказали по этому объявлению. Пора искать новые предложения.')?>
                                </div>
                            </div>
                        <? endif;?>
                        <? if(is_array($v['offer_status']) && $v['offer_status']['status'] == Orders::OFFER_STATUS_PERFORMER_DECLINE):?>
                            <div class="text-center ">
                                <img width="35" src="<?= bff::url('/img/forbidden.svg')?>" alt="">
                                <?//todo clazion icon ?>
                                <div class="simple-text mrgt10">
                                    <?= _t('','Вы отказались от этого предложения. Найдите новые объявления.')?>
                                </div>
                            </div>
                        <? endif;?>
                    <?endif;?>
                    <div class=" mrgt20">
                        <?= $this->getOrderNote($v['id']);?>
                    </div>
                </div>
                <? endif; ?>
            </div>

        </<?= $bPromote ? 'div' : 'li' ?>>
    <? endforeach; ?>
    </<?= $bPromote ? 'div' : 'ul' ?>>
<? else: ?>
    <div class="alert alert-info"><?= _t('orders', 'Заказы не найдены'); ?></div>
<? endif;