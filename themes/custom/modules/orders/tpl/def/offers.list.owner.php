<?php

/**
 * Список заявок к заказу: владелец заказа
 * @var $this Orders
 */

tpl::includeJS('orders.offers', false, 3);
if (User::id()) {
    tpl::includeJS('users.note', false, 2);
}

tpl::includeJS(['dist/rating']);

$statusList = array(
    Orders::OFFER_STATUS_CANDIDATE => array(
        'id'  => Orders::OFFER_STATUS_CANDIDATE,
        'tl'  => _t('orders-offers', 'В кандидаты'),
        'tb'  => _t('orders-offers', 'В кандидаты'),
        'cla' => 'label-candidate',
        'cli' => '',
        'fa'  => 'fa-check text-green'
    ),
    Orders::OFFER_STATUS_PERFORMER => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER,
        'tl'  => _t('orders-offers', 'Взять на работу'),
        'tb'  => _t('orders-offers', 'Взять на работу'),
        'cla' => 'label-chosen',
        'cli' => '',
        'fa'  => 'fa-trophy text-orange',
    ),
    Orders::OFFER_STATUS_PERFORMER_START => array(),
    Orders::OFFER_STATUS_CANCELED  => array(
        'id'  => Orders::OFFER_STATUS_CANCELED,
        'tl'  => _t('orders-offers', 'Отказано'),
        'tb'  => _t('orders-offers', 'Отказать'),
        'cla' => 'label-decline',
        'cli' => 'o-declined-worker',
        'fa'  => 'fa-times text-red'
    ),
);
$ordersOpinions = Orders::ordersOpinions();
if($ordersOpinions){
    foreach(array(
        0, Orders::OFFER_STATUS_CANDIDATE,       Orders::OFFER_STATUS_PERFORMER,        Orders::OFFER_STATUS_CANCELED,
           Orders::OFFER_STATUS_PERFORMER_START, Orders::OFFER_STATUS_PERFORMER_AGREE,  Orders::OFFER_STATUS_PERFORMER_DECLINE,
           Orders::OFFER_STATUS_INVITE_DECLINE
        ) as $v){
            if( ! isset($statuses[$v])){
                $statuses[$v] = 0;
            }
    }
    $statuses[0] += $statuses[Orders::OFFER_STATUS_PERFORMER_START];
    $statuses[0] += $statuses[Orders::OFFER_STATUS_PERFORMER_AGREE];
    $statuses[Orders::OFFER_STATUS_PERFORMER_DECLINE] += $statuses[Orders::OFFER_STATUS_INVITE_DECLINE];
    $statusList[Orders::OFFER_STATUS_PERFORMER_START] = $statusList[Orders::OFFER_STATUS_PERFORMER];
    $statusList[Orders::OFFER_STATUS_PERFORMER_START]['id'] = Orders::OFFER_STATUS_PERFORMER_START;
    $statusList[Orders::OFFER_STATUS_PERFORMER_AGREE] = $statusList[Orders::OFFER_STATUS_PERFORMER];
    $statusList[Orders::OFFER_STATUS_PERFORMER_AGREE]['id'] = Orders::OFFER_STATUS_PERFORMER_AGREE;
    unset($statusList[Orders::OFFER_STATUS_PERFORMER]);
}else{
    unset($statusList[Orders::OFFER_STATUS_PERFORMER_START]);
}
$fairplayEnabled = bff::fairplayEnabled();
$showButtons = true;
if($ordersOpinions){
    if( ! empty($performer_id) && ! empty($performer_status) && $performer_status == Orders::OFFER_STATUS_PERFORMER_AGREE){
        $showButtons = false;
    }
}
?>
<? if( ! empty($offers)): ?>
    <? if($ordersOpinions): ?>
    <div class="">
        <ul class="p-profile-submenu p-profile-submenu_mobile" id="j-status-filter">
            <li class="active">
                <a href="#" class="j-status-filter" data-status="0">
                    <span class="j-title">
                        <?= _t('orders', 'Отклики'); ?>
                    </span>
                    <small class="label-count label-count-def">
                        <?= $statuses[0] ?>
                    </small>
                </a>
            </li>
            <li>
                <a href="#" class="j-status-filter" data-status="<?= Orders::OFFER_STATUS_CANDIDATE ?>">
                    <span class="j-title">
                        <?= _t('orders', 'Кандидаты'); ?>
                    </span>
                    <small class="label-count label-count-def">
                        <?= $statuses[Orders::OFFER_STATUS_CANDIDATE] ?>
                    </small>
                </a>
            </li>
            <li>
                <a href="#" class="j-status-filter" data-status="<?= Orders::OFFER_STATUS_CANCELED ?>">
                    <span class="j-title">
                        <?= _t('orders', 'Отказано'); ?>
                    </span>
                    <small class="label-count label-count-def">
                        <?= $statuses[Orders::OFFER_STATUS_CANCELED] ?>
                    </small>
                </a>
            </li>
            <li>
                <a href="#" class="j-status-filter" data-status="<?= Orders::OFFER_STATUS_PERFORMER_DECLINE ?>">
                    <span class="j-title">
                        <?= _t('orders', 'Отказались'); ?>
                    </span>
                    <small class="label-count label-count-def">
                        <?= $statuses[Orders::OFFER_STATUS_PERFORMER_DECLINE] ?>
                    </small>
                </a>
            </li>
        </ul>
    </div>
    <? endif; ?>
    <ul class="proposal-list" id="j-offers">

        <? foreach($offers as $v):
            $avatar =  UsersAvatar::url($v['user_id'], $v['avatar'], UsersAvatar::szNormal, $v['sex']);
            $statusSet = array_key_exists($v['status'], $statusList);
            if($statusSet){
                $status = $statusList[ $v['status'] ];
            }
            if($v['user_blocked']): ?>
                <li class="media">
                    <div class="alert alert-danger">
                        <?= _t('', 'Пользователь заблокирован')?>
                    </div>
                </li>
                <? continue;
            endif; ?>
            <?
            # Стоимость:
            $sPrice = '';
            if( ! empty($v['price_from']) &&  ! empty($v['price_to'])){
                $sPrice = $v['price_from'].' - '.$v['price_to'];
            } else {
                if( ! empty($v['price_from'])){
                    $sPrice = _t('', 'от ').$v['price_from'];
                }
                if( ! empty($v['price_to'])){
                    $sPrice = _t('', 'до ').$v['price_to'];
                }
            }
            $sPrice .= ! empty($sPrice) ? ' '.Site::currencyData($v['price_curr'], 'title_short') : '';
            $aPriceRate = ( $v['price_rate_text'] ? func::unserialize($v['price_rate_text']) : array(LNG=>'') );
            if ( ! empty($aPriceRate[LNG]) && ! empty($sPrice)) { $sPrice .= ' '.$aPriceRate[LNG]; }

            # Сроки:
            $sTerms = '';$term = 0;
            if( ! empty($v['terms_from']) && ! empty($v['terms_to'])){
                $sTerms = $v['terms_from'].' - '.$v['terms_to'];
                $term = $v['terms_to'];
            } else {
                if( ! empty($v['terms_from'])){
                    $sTerms = _t('', 'от ').$v['terms_from'];
                    $term = $v['terms_from'];
                }
                if( ! empty($v['terms_to'])){
                    $sTerms = _t('', 'до ').$v['terms_to'];
                    $term = $v['terms_to'];
                }
            }
            if ( ! empty($sTerms)) {
                $sTerms = ' / '.$sTerms.' '.Specializations::aTerms($v['terms_type'], 'title');
            }
            ?>

            <li class="proposal-list__item <?= $ordersOpinions && ! in_array($v['status'], array(0, Orders::OFFER_STATUS_PERFORMER_START)) ? ' hidden' : '' ?> j-offer" data-status="<?= $v['status'] ?>" data-id="<?= $v['id'] ?>">


                <div class="proposal-list__body">
                    <div class="proposal-list__inside">
                        <div class="">
                            <div class="user-box">
                                <a href="<?= Users::url('profile', array('login' => $v['login'])) ?>" class="user-box__avatar user-box__avatar_sq-big ">
                                    <img src="<?= $avatar ?>" class="" alt="<?= tpl::avatarAlt($v); ?>" />
                                    <?= tpl::userOnline($v) ?>
                                </a>
                                <div class="mrgl10">
                                    <? if($v['spec_id']): ?>
                                        <div class="user-box__text-s">
                                            <a href="<?= Users::url('search-spec', $v) ?>" class="user-box__text-s">
                                                <?= $v['spec_title'] ?>
                                            </a>
                                        </div>
                                    <? endif; ?>
                                    <div class="user-box__text">
                                        <div class="nowrap">
                                            <?= tpl::userLink($v, 'no-login, no-verified') ?>
                                            <?= Rating::getViewTotalAverage($v['user_id']) ?>
                                        </div>
                                    </div>

                                    <? if($v['contacts']  && !empty(func::unserialize($v['contacts']))):# контакты и кнопка их открытия
                                        $open_user_id = $v['open_user_id'] = $v['user_id'];
                                        $open_user_type = $v['open_user_type'] = $v['user_type'];
                                        $is_profile = Site::i()->isProfilePage();
                                        $bOpenedContacts = Users::isOpenedContacts($open_user_id);
                                        $aCurrentUserData = Users::model()->userData(User::id(), 'type');
                                        # показывать кнопку в чужом профиле для противоположного типа пользователя
                                        $bShowContactsBtn = !$bOpenedContacts && ($aCurrentUserData['type'] != $v['user_type']);
                                        if( ! User::id() || $bShowContactsBtn):?>
                                            <div class=" j-show-btn-block" data-btn-opend-user-id="<?=$open_user_id;?>">
                                                <button onclick="<?= ( ! User::id())? '$(\'.j-show-btn\').hide();$(\'#j-login-link\').toggle();' :
                                                    "Custom.open_contacts({$open_user_id},{$is_profile})"?>"
                                                        class="link-bold pd0 j-show-btn">
                                                    <?=_t('contacts', 'Купить контакт')?>
                                                </button>
                                                <a style="display: none"
                                                   href="<?=Users::url('login')?>"
                                                   class="btn btn-warning"
                                                   id="j-login-link">
                                                    <i class="fa fa-lock"></i>
                                                    <?=_t('contacts', 'Авторизируйтесь')?>
                                                </a>
                                            </div>
                                        <? endif;?>
                                        <? $contacts = $v['contacts'] = func::unserialize($v['contacts']);?>
                                        <? if($bOpenedContacts):?>
                                            <div class="proposal-list__contact">
                                                <?= Users::i()->viewPHP($v, 'contacts.block');?>
                                            </div>
                                        <? else:?>
                                            <div class="j-contacts-block" data-opend-user-id="<?=$open_user_id;?>"></div>
                                    <? endif;?>

                                    <? endif; ?>
                                </div>
                            </div>
                            <div class="media-body">
                                <div class="o-freelancer-info">
                                    <div class="j-worker-info">
                                        <? if($v['status'] == Orders::OFFER_STATUS_INVITE_DECLINE): ?>
                                            <span class="label label-count">
                                        <?= _t('orders', 'Отказался от предложения') ?>
                                    </span>
                                        <? endif; ?>
                                    </div>
                                    <? if($fairplayEnabled && ! $fairplay && $v['fairplay']): ?>
                                        <div>
                                            <?= _t('fp', 'Предпочитаю оплату работы через <a [link]>Безопасную Сделку</a>', array('link' => 'href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-shield c-link-icon c-safe-color"></i')); ?>
                                        </div>
                                    <? endif; ?>
                                </div>
                            </div>
                            <div class="j-allet-performer">
                                <? if($ordersOpinions && $v['status'] == Orders::OFFER_STATUS_PERFORMER_START):
                                    if( ! empty($v['workflow'])){ $aData['workflow'] = func::unserialize($v['workflow']); }
                                    $aData['offerID'] = $v['id']; ?><?= $this->viewPHP($aData, 'offers.list.alert.performer.start'); ?>
                                <? endif; ?>
                            </div>
                        </div>
                        <div class="">

                            <div class="mrgt5">
                                <?  $aNote = array('id' => $v['note_id'], 'user_id' => $v['user_id'], 'note' => $v['note'], 'bFav' => 1);
                                echo(Users::i()->viewPhp($aNote, 'my.note.block'));
                                ?>
                            </div>
                            
                            <span class="proposal-list__add ">
                                <?= _t('', '[date]', array('date' => tpl::date_format3($v['created'])))?>
                            </span>

                        </div>
                    </div>
                </div>

                <? if($showButtons): ?>
                    <div class="proposal-list__btn">
                        <? foreach($statusList as $vv):
                            if($vv['id'] == Orders::OFFER_STATUS_PERFORMER_AGREE) continue;
                            $bActive = $v['status'] == $vv['id'];
                            $bDisabled = $ordersOpinions
                                ? ( $performer_id && $v['user_id'] == $performer_id ) || ($performer_id && $vv['id'] == Orders::OFFER_STATUS_PERFORMER_START)
                                : $vv['id'] == (Orders::OFFER_STATUS_PERFORMER && $performer_id && $v['user_id'] != $performer_id);
                            $start = '';
                            if($vv['id'] == Orders::OFFER_STATUS_PERFORMER_START){
                                $start = ' data-fp="'.$v['fairplay'].'" data-term="'.$term.'" ';
                            }
                            ?>
                            <div class="">
                                <button type="button" class="proposal-btn <?= $bActive ? 'active' : '' ?> j-status j-status-<?= $vv['id'] ?>" <?= $bDisabled ? 'disabled="disabled"' : '' ?> data-id="<?= $v['id'] ?>" data-s="<?= $vv['id'] ?>"<?= $start ?>>
                                    <?= $vv['tb'] ?>
                                </button>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
            </li>
        <? endforeach; ?>
    </ul>
<? endif; ?>
<? if($ordersOpinions): ?>
<div class="modal fade" id="j-modal-worker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?= _t('', 'Выбрать исполнителя'); ?></h4>
            </div>
            <div class="modal-body">
                <ul class="list-unstyled list-unstyled-morespace">
                    <li><strong><?= $order_title ?></strong></li>
                    <li><?= _t('orders', 'Исполнитель:'); ?> <span class="nowrap j-worker-name"></span></li>
                </ul>
                <form method="post">
                    <input type="hidden" name="id" value="0" />
                    <? if($fairplayEnabled): ?>
                        <div class="form-horizontal">
                            <div class="form-group o-propose-inputs">
                                <label for="j-workflow-period" class="col-sm-3 control-label o-control-label"><?= _t('fp', 'Срок'); ?></label>
                                <div class="col-sm-9">
                                    <div class="input-group j-required">
                                        <input type="text" name="term" id="j-workflow-period" class="form-control">
                                        <div class="o-propose-inputs-txt"><?= _t('fp', 'дней'); ?></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group o-propose-inputs">
                                <label for="j-workflow-price" class="col-sm-3 control-label o-control-label"><?= _t('fp', 'Бюджет'); ?></label>
                                <div class="col-sm-9">
                                    <div class="input-group j-required">
                                        <input type="text" name="price" id="j-workflow-price" class="form-control input-sm" value="<?= $price ?>">
                                        <div class="o-propose-inputs-txt"><?= Site::currencyDefault('title_short'); ?></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label o-control-label"><?= _t('fp', 'Способ оплаты'); ?></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="fairplay" value="<?= Orders::FAIRPLAY_USE ?>">
                                            <?= _t('fp', 'Безопасная сделка (с резервированием бюджета) [link]', array('link' => '<a href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-question-circle"></i> </a>')); ?>
                                        </label>
                                        <div class="help-block mrgt0 mrgb0"><?= _t('fp', 'Безопасное сотрудничество с гарантией возврата денег. Вы резервируете бюджет заказа, а мы гарантируем вам возврат суммы, если работа выполнена некачественно или не в срок.'); ?></div>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="fairplay" value="<?= Orders::FAIRPLAY_NONE ?>">
                                            <?= _t('fp', 'Прямая оплата исполнителю на его кошелек/счет'); ?>
                                        </label>
                                        <div class="help-block mrgt0 mrgb0"><?= _t('fp', 'Сотрудничество без участия сайта в процессе оплаты. Вы сами договариваетесь с исполнителем о способе и порядке оплаты. И самостоятельно регулируете все претензии, связанные с качеством и сроками выполнения работы.'); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="form-group">
                        <textarea rows="5" class="form-control" name="message" placeholder="<?= _t('orders', 'Сообщение исполнителю (не обязательно)'); ?>"></textarea>
                        <div class="help-block j-help-block"></div>
                    </div>
                    <div class="c-formSubmit">
                        <button type="submit" class="btn btn-success c-formSuccess j-submit"><?= _t('orders', 'Выбрать исполнителем'); ?></button>
                        <a class="c-formCancel ajax-link j-cancel" href="#" data-dismiss="modal"><span><?= _t('form', 'Отмена'); ?></span></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<? endif; ?>
<script type="text/javascript">
<? js::start() ?>
jOrdersOffersPerformer.init(<?= func::php2js(array(
    'lang'   => array(
        'left'    => _t('users','[symbols] осталось'),
        'symbols' => explode(';', _t('users', 'знак;знака;знаков')),
        'candidate' => _t('orders', 'Исполнитель был успешно назначен кандидатом'),
    ),
    'statuses' => $statusList,
    'ordersOpinions' => $ordersOpinions,
    'fairplayEnabled' => $fairplayEnabled ? 1 : 0,
    'fairplay'        => $fairplay,
    'fairplay_use'    => Orders::FAIRPLAY_USE,
    'fairplay_none'   => Orders::FAIRPLAY_NONE,
    'st' => array(
        'canceled'  => Orders::OFFER_STATUS_CANCELED,
        'performer' => Orders::OFFER_STATUS_PERFORMER,
        'candidate' => Orders::OFFER_STATUS_CANDIDATE,
        'start'     => Orders::OFFER_STATUS_PERFORMER_START,
        'agree'     => Orders::OFFER_STATUS_PERFORMER_AGREE,
        'decline'   => Orders::OFFER_STATUS_PERFORMER_DECLINE,
        'invite_no' => Orders::OFFER_STATUS_INVITE_DECLINE,
        ),
)) ?>);

jUsersNote.init();

$(function () {
    var ratingTotalAverage = new Rating();
    ratingTotalAverage.initViewTotalAverage(
        <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
    );
});

<? js::stop() ?>
</script>