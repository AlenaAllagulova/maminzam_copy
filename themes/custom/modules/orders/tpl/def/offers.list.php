<?php

/**
 * Список заявок к заказу: зарегистрированный пользователь
 * @var $this Orders
 */

$statusList = array(
    Orders::OFFER_STATUS_CANDIDATE => array(
        'id' => Orders::OFFER_STATUS_CANDIDATE,
        't'  => _t('orders-offers', 'Вы кандидат'),
        'd'  => _t('orders-offers', 'Работодатель определил Вас как кандидата на этот заказ. Это значит, что Вы прошли предварительный отбор и, возможно, будете выбраны исполнителем.'),
        'c'  => 'alert-chosen',
    ),
    Orders::OFFER_STATUS_PERFORMER => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER,
        't'  => _t('orders-offers', 'Вы исполнитель'),
        'd'  => _t('orders-offers', 'Работодатель определил Вас как исполнителя на этот заказ.'),
        'c'  => 'alert-candidate',
    ),
    Orders::OFFER_STATUS_CANCELED  => array(
        'id'  => Orders::OFFER_STATUS_CANCELED,
        't'  => _t('orders-offers', 'Вам отказанно'),
        'd'  => _t('orders-offers', 'Работодатель отказал Вам.'),
        'c'  => 'alert-decline',
    ),
    Orders::OFFER_STATUS_PERFORMER_DECLINE => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER_DECLINE,
        't'  => _t('orders-offers', 'Отказались'),
        'd'  => _t('orders-offers', 'Вы отказались от заказа.'),
        'c'  => 'alert-decline',
    ),
    Orders::OFFER_STATUS_PERFORMER_AGREE => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER_AGREE,
        't'  => _t('orders-offers', 'Вы исполнитель'),
        'd'  => _t('orders-offers', 'Вы согласились стать исполнителем на этот заказ.'),
        'c'  => 'alert-candidate',
    ),
);
$fairplayEnabled = bff::fairplayEnabled();
?>

<? if( ! empty($offers)): ?>
    <ul class="proposal-list" id="j-offers-performer-start">

        <? foreach($offers as $v):
            $avatar =  UsersAvatar::url($v['user_id'], $v['avatar'], UsersAvatar::szSmall, $v['sex']);
            $is_my = ! empty($v['bMy']);
            if($v['user_blocked']): ?>
                <li class="media">
                    <div class="alert alert-danger">
                        <?= _t('', 'Пользователь заблокирован')?>
                    </div>
                </li>
                <? continue;
            endif; ?>
            <li class="proposal-list__item <? if($is_my): ?> <? endif; ?>">
                <div class="proposal-list__body">
                    <div class="proposal-list__inside">
                        <div class="">
                            <? if($is_my && array_key_exists($v['status'], $statusList)): $status = $statusList[ $v['status'] ];
                                if($fairplayEnabled && $v['status'] == Orders::OFFER_STATUS_PERFORMER_AGREE && ! empty($fairplay) && ! empty($order_id)){
                                    Fairplay::i()->checkReserved($order_id, $keyword, $status['d']);
                                }
                                ?>
                                <div class="alert <?= $status['c'] ?>">
                                    <div><strong><?= $status['t'] ?></strong></div>
                                    <?= $status['d'] ?>
                                </div>
                            <? endif; ?>
                            <?= ! empty($v['performer_start']) && $v['show_erformer_start']? $v['performer_start'] : '' ?>

                            <div class="user-box">
                                <a href="<?= Users::url('profile', array('login' => $v['login'])) ?>" class="user-box__avatar user-box__avatar_sq-big ">
                                    <img src="<?= $avatar ?>" class="" alt="<?= tpl::avatarAlt($v); ?>" />
                                    <?= tpl::userOnline($v) ?>
                                </a>
                                <div class="mrgl10">
                                    <? if($v['spec_id']): ?>
                                        <div class="user-box__text-s">
                                            <a href="<?= Users::url('search-spec', $v) ?>" class="user-box__text-s">
                                                <?= $v['spec_title'] ?>
                                            </a>
                                        </div>
                                    <? endif; ?>
                                    <div class="user-box__text">
                                        <div class="nowrap">
                                            <?= tpl::userLink($v, 'no-login, no-verified') ?>
                                        </div>
                                    </div>
                                    <? if($v['contacts'] && !empty(func::unserialize($v['contacts']))): # контакты и кнопка их открытия
                                        $open_user_id = $v['open_user_id'] = $v['user_id'];
                                        $open_user_type = $v['open_user_type'] = $v['user_type'];
                                        $is_profile = Site::i()->isProfilePage();
                                        $bOpenedContacts = Users::isOpenedContacts($open_user_id);
                                        $aCurrentUserData = Users::model()->userData(User::id(), 'type');
                                        # показывать кнопку в чужом профиле для противоположного типа пользователя
                                        $bShowContactsBtn = !$is_my && !$bOpenedContacts && ($aCurrentUserData['type'] != $open_user_type);
                                        if( ! User::id() || $bShowContactsBtn):?>
                                            <div class="l-inside text-center j-show-btn-block" data-btn-opend-user-id="<?=$open_user_id;?>">
                                                <button onclick="<?= ( ! User::id())? '$(\'.j-show-btn\').hide();$(\'#j-login-link\').toggle();' :
                                                    "Custom.open_contacts({$open_user_id},{$is_profile})"?>"
                                                        class="btn btn-success j-show-btn">
                                                    <?=_t('contacts', 'Показать контакты')?>
                                                </button>
                                                <a style="display: none"
                                                   href="<?=Users::url('login')?>"
                                                   class="btn btn-warning"
                                                   id="j-login-link">
                                                    <i class="fa fa-lock"></i>
                                                    <?=_t('contacts', 'Авторизируйтесь')?>
                                                </a>
                                            </div>
                                        <? endif;?>
                                        <? $contacts = $v['contacts'] = func::unserialize($v['contacts']);?>
                                        <? if($is_my || $bOpenedContacts):?>
                                            <div class="proposal-list__contact">
                                                <?= Users::i()->viewPHP($v, 'contacts.block');?>
                                            </div>
                                        <? else:?>
                                            <div class="j-contacts-block" data-opend-user-id="<?=$open_user_id;?>"></div>
                                        <? endif;?>
                                    <? endif; ?>
                                </div>
                            </div>

                            <? if($v['status'] == Orders::OFFER_STATUS_INVITE_DECLINE): ?>
                                <span class="label label-count">
                                    <?= _t('orders', 'Отказался от предложения') ?>
                                </span>
                            <? endif; ?>

                        </div>
                    </div>
                    <div class="">
                        <span class="proposal-list__add ">
                            <?= _t('', '[date]', array('date' => tpl::date_format3($v['created'])))?>
                        </span>
                    </div>
                </div>
            </li>
        <? endforeach; ?>
    </ul>
<? endif;
if($offers_cnt > count($offers)): ?>
    <div class="alert alert-info mrgt20">
        <?= _t('orders-offers', 'Остальные предложения скрыты и видны только заказчику'); ?>
    </div>
<? endif;