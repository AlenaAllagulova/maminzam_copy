<?php
    $nUserID = User::id();
    $aServiceTypes = Orders::aServiceTypes();
    $bDeleteAllow = Orders::deleteAllow();
    $tagsLimit = Orders::searchTagsLimit();
    $ordersOpinions = Orders::ordersOpinions();
    $fairplayEnabled = bff::fairplayEnabled();

$aOfferStatuses = array(
    Orders::OFFER_STATUS_CANDIDATE => array(
        'id' => Orders::OFFER_STATUS_CANDIDATE,
        't'  => _t('orders', 'Вас выбрали кандидатом. У Вас больше шансов стать няней в этой семье.'),
        'c'  => 'alert-candidate',
        'fa' => '/img/stars.svg'
    ),
    Orders::OFFER_STATUS_PERFORMER => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER,
        't'  => _t('orders', 'Вас выбрали исполнителем.'),
        'c'  => 'alert-chosen',
        'fa' => 'fa-trophy text-orange'
    ),
    Orders::OFFER_STATUS_CANCELED  => array(
        'id'  => Orders::OFFER_STATUS_CANCELED,
        't'  => _t('orders', 'Вам отказали по этому объявлению. Пора искать новые предложения.'),
        'c'  => 'alert-decline',
        'fa' => '/img/sad.svg'
    ),
    Orders::OFFER_STATUS_PERFORMER_START => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER_START,
        't'  => _t('orders', 'Вам предложили стать исполнителем.'),
        'c'  => 'alert-chosen',
        'fa' => 'fa-trophy text-orange'
    ),
    Orders::OFFER_STATUS_PERFORMER_DECLINE => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER_DECLINE,
        't'  => _t('orders', 'Вы отказались от заказа.'),
        'c'  => 'alert-decline',
        'fa' => 'fa-times'
    ),
);
$aOrderMainStatuses = Orders::getOrderMainStatusesContent();

    if( ! empty($list)): ?>
        <ul class="order">
            <? foreach($list as $v):
                $my = $v['user_id'] == $nUserID;
                $urlView = Orders::url('view', array('id' => $v['id'], 'keyword' => $v['keyword']));
                $openClose = in_array($v['status'], array(Orders::STATUS_OPENED, Orders::STATUS_CLOSED));
                $delete = $bDeleteAllow;
                if($openClose && $ordersOpinions
                    && $v['performer_id']
                    && ! empty($v['offer_status'])
                    && $v['offer_status'] == Orders::OFFER_STATUS_PERFORMER_AGREE){
                    $openClose = false;
                    $delete = false;
                } ?>

                <li class="j-order">
                    <div class="row order__box">
                        <div class="col-md-9 col-sm-12">
                            <div class="order__item <?= $v['svc_marked'] ? 'select' : '' ?>">
                                <div class="order__header">
                                    <div class="flex flex_column flex_center-sm">
                                        <div class="order__status">
                                            <?if(isset($aOrderMainStatuses[$v['status']])):?>
                                                <span class="<?= $aOrderMainStatuses[$v['status']]['class']?>">
                                                    <i></i>
                                                    <?= $aOrderMainStatuses[$v['status']]['order_t']?>
                                                </span>
                                            <? endif;?>
                                        </div>
                                        <div class="flex flex_center flex_wrap">
                                            <? if($v['visibility'] == Orders::VISIBILITY_PRIVATE): ?>
                                                <i class="fa fa-eye-slash show-tooltip mrgr5"
                                                   data-toggle="tooltip" data-placement="top"
                                                   title="<?= _t('orders', 'Приватный заказ'); ?>"></i>
                                            <? endif; ?>
                                            <? if($fairplayEnabled && $v['fairplay']): ?>
                                                <i class="fa fa-shield c-safe-color show-tooltip mrgr5"
                                                   data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>"
                                                   title="" data-placement="top" data-toggle="tooltip"></i>
                                            <? endif; ?>
                                            <a href="<?= $urlView ?>" class="order__title"><?= $v['title'] ?></a>
                                            <? if($v['pro']): ?>
                                                <div class="">
                                                    <span class="pro mrgl5"><?=_t('', 'pro')?></span>
                                                </div>
                                            <? endif; ?>
                                        </div>
                                        <? if( ! empty($v['city_data']['title'])): ?>
                                            <div class="order__text">
                                                <?= $v['city_data']['title']?>
                                            </div>
                                        <? endif; ?>
                                        <? if(!empty($v['district_id'])):?>
                                            <div class="order__text">
                                                <?= Geo::districtTitle($v['district_id'])?>
                                            </div>
                                        <?endif;?>
                                    </div>
                                    <div class="order__price">
                                        <? if($v['type'] == Orders::TYPE_SERVICE): ?>
                                            <? if($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                                                <?= ! empty($v['price_rate_text'][LNG]) ? $v['price_rate_text'][LNG] : _t('orders', 'По договоренности'); ?>
                                            <? else: ?>
                                                <?= tpl::formatPrice($v['price']) ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?>
                                                <? if( ! empty($v['price_rate_text'][LNG])): ?>
                                                    <?= $v['price_rate_text'][LNG] ?>
                                                <? endif; ?>
                                            <? endif; ?>
                                        <? endif; ?>
                                    </div>
                                </div>
                                <div class="order__conditions mrgt10">
                                    <? if (!empty($v['dynprops_simple'])): # краткий вывод динсвойств patr 2 c обработкой результата ?>
                                        <? foreach ($v['dynprops_simple'] as $dp_item): ?>
                                            <? if ($dp_item['group_id'] == Orders::DP_ORDER_VIEW_PART_SECOND ): ?>
                                                <div class="order__conditions-item">
                                                    <? if($dp_item['value'] !== ''):?>
                                                        <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                        <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                                            <? foreach ($dp_item['multi'] as $val): ?>
                                                                <? if ($dp_item['value'] == $val['value']):
                                                                    if(strpos($val['name'],'(')){
                                                                        $val['name'] = substr( $val['name'] , strpos($val['name'],'(')+strlen('('));
                                                                        $val['name'] = strstr($val['name'], ')', true);
                                                                    }else{
                                                                        $val['name'] = (int)$val['name'];
                                                                    }
                                                                    ?>
                                                                    <div class="bold"><?= $val['name'] ?></div>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? else: ?>
                                                            <div class="bold">
                                                                <? foreach ($dp_item['value'] as $dp_val): ?>
                                                                    <? foreach ($dp_item['multi'] as $val): ?>
                                                                        <? if ($dp_val == $val['value']):
                                                                            if (strpos($val['name'], '(')) {
                                                                                $val['name'] = substr($val['name'], strpos($val['name'], '(') + strlen('('));
                                                                                $val['name'] = strstr($val['name'], ')', true);
                                                                            } else {
                                                                                $val['name'] = (int)$val['name'];
                                                                            }
                                                                            ?>
                                                                            <?= $val['name'] ?> <br>
                                                                        <? endif; ?>
                                                                    <? endforeach; ?>
                                                                <? endforeach; ?>
                                                            </div>
                                                        <? endif; ?>
                                                    <? else: ?>
                                                        <th><?= _t('dp_orders','не указано') ?></th>
                                                    <? endif;?>
                                                    <span class="order__second-text"><?= $dp_item['description_'.LNG] ?></span>
                                                </div>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    <? endif; ?>
                                    <div class="order__conditions-item">
                                        <div class="bold">
                                            <?= ( $v['start_date'] != '0000-00-00 00:00:00')? tpl::date_format2( $v['start_date'], false, true): _t('', 'не указана'); ?>
                                        </div>
                                        <span class="order__second-text">
                                            <?= _t('orders','Дата начала')?>
                                        </span>
                                    </div>
                                </div>
                                <p class="order__text mrgt10 order__desc">
                                    <?= tpl::truncate($v['descr'], config::sysAdmin('orders.search.list.descr.truncate', 250, TYPE_UINT)); ?>
                                </p>
                                <ul class="order-info">
                                    <li>
                                        <?= _t('Orders','Опубликовано: [date]',
                                                [ 'date' => tpl::date_format_spent($v['created'], false, true)])?>
                                    </li>
                                    <li>
                                        <?= _t('Orders', 'Просмотров: [total]', array('total'=>$v['views_total'])); ?>
                                    </li>
                                    <li>
                                        <a href="<?= $urlView ?>#offers">
                                            <span>
                                                <?= tpl::declension($v['offers_cnt'], _t('orders', 'отклик;отклика;откликов'))?>
                                            </span>
                                        </a>
                                        <?= ! empty($v['offers_new']) ? ' <span class="color-accent bold">+'.$v['offers_new'].'</span>' : '' ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-5  order__notif">
                        <? if($my): ?>
                            <?= $this->orderStatusBlock($v); ?>
                            <? if($v['status'] == Orders::STATUS_ONACTIVATION):?>
                                <div class="text-center order__text mrgb10">
                                    <?= _t('','Объявление не активно')?>
                                    <a href="<?= Orders::url('activation', array('id'=>$v['id'])); ?>" class="btn btn-primary btn-block mrgt10">
                                        <?= _t('form', 'Активировать'); ?>
                                    </a>
                                </div>
                            <? endif;?>
                            <div class="">
                                <? if(bff::servicesEnabled() && $v['visibility'] == Orders::VISIBILITY_ALL && $v['status'] == Orders::STATUS_OPENED): ?>
                                    <div class="order__text text-center mrgb10">
                                        <?= _t('','Хотите, чтобы ваше объявление увидело больше бебиситтеров или нянь?')?>
                                        <? // todo clazion вместо 'бебиситтеров или нянь' подставлять нужную специализацию ?>
                                    </div>
                                    <a href="<?= Orders::url('promote', array('id' => $v['id'])); ?>" class="btn btn-primary btn-block">
                                        <?= _t('orders', 'Рекламировать'); ?>
                                    </a>
                                <? endif; ?>
                                <? if($v['performer_id']):
                                    $performerLink = array(
                                        'name'     => $v['performer_name'],
                                        'surname'  => $v['performer_surname'],
                                        'pro'      => $v['performer_pro'],
                                        'login'    => $v['performer_login'],
                                        'verified' => $v['performer_verified'],
                                    ); ?>
                                    <? if($ordersOpinions && ! $fairplayEnabled && ! empty($v['offer_status'])):
                                    if($v['offer_status'] == Orders::OFFER_STATUS_PERFORMER_START): ?>
                                        <div class="text-left mrgt10">
                                            <div class="bold"><?= _t('','Вы выбрали')?></div>
                                            <div class="user-box mrgt5">
                                                <div class="user-box__avatar user-box__avatar_sq">
                                                    <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $performerLink['login'])); ?>">
                                                        <img src="<?= $v['performer_avatar_url']?>" alt="">
                                                    </a>
                                                </div>
                                                <div class="user-box__text mrgl10">
                                                    <? if(isset($v['performer_spec_data']) && !empty($v['performer_spec_data'])):?>
                                                        <span class="user-box__text-s">
                                                            <?=$v['performer_spec_data']['title'];?>
                                                        </span>
                                                    <?endif;?>
                                                    <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $performerLink['login'])); ?>">
                                                        <? if(!empty($performerLink['name'])): ?>
                                                            <?= $performerLink['name'] ?>
                                                        <? endif; ?>
                                                        <? if(!empty($performerLink['surname'])): ?>
                                                            <?= mb_strimwidth($performerLink['surname'], 0, 2, "."); ?>
                                                        <? endif; ?>
                                                    </a>
                                                    <? if (Users::isOpenedContacts($v['performer_id'])): # телефон исполнителя и проверка его открытия ?>
                                                        <? if(isset($v['performer_contacts_phones']) && !empty($v['performer_contacts_phones'])):?>
                                                            <span class="">
                                                                <?=$v['performer_contacts_phones'][0]['v']?>
                                                            </span>
                                                        <?endif;?>
                                                    <? endif;?>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:volid(0);"
                                           type="button"
                                           class="link-bold link-bold_fz15 j-performer-start-cancel mrgt10"
                                           data-id="<?= $v['performer_offer_id'] ?>">
                                            <?= _t('orders', 'Отменить предложение'); ?>
                                        </a>

                                    <? else: if($v['offer_status'] == Orders::OFFER_STATUS_PERFORMER_AGREE): ?>
                                        <div class="">
                                            <?  $meEnd = isset($v['opinions'][ $nUserID ]); if($meEnd) { $myOpinion = $v['opinions'][ $nUserID ]; }
                                            $workerEnd = isset($v['opinions'][ $v['performer_id'] ]); if($workerEnd) { $workerOpinion = $v['opinions'][ $v['performer_id'] ]; }
                                            ?>
                                            <div class="text-center order__text">
                                                <? if($meEnd): ?>
                                                    <?=  _t('orders', 'Заказ завершен.')?>
                                                <? endif; ?>
                                                <? if(!($meEnd)) :?>
                                                    <div class="">
                                                        <?= _t('','Работа выполнена? Завершите объявление.')?>
                                                    </div>
                                                <? endif; ?>
                                            </div>
                                            <div class="">
                                                <? if($workerEnd): ?>
                                                    <div class="text-center">
                                                        <a href="#" class="link-bold j-opinion-show"
                                                           data-id="<?= $workerOpinion['id'] ?>">
                                                            <?= _t('orders', 'Посмотреть отзыв'); ?>
                                                        </a>
                                                    </div>
                                                <? endif; ?>
                                                <? if($meEnd): ?>
                                                   <div class="text-center">
                                                       <a href="#" class="link-bold j-opinion-show" data-id="<?= $myOpinion['id'] ?>">
                                                           <?= _t('orders', 'Ваш отзыв'); ?>
                                                       </a>
                                                   </div>
                                                <? else: ?>
                                                    <button type="button" class="btn btn-primary btn-block j-opinion-add mrgt10" data-id="<?= $v['id'] ?>" data-order_id="<?= $v['id'] ?>">
                                                        <?= $workerEnd ? _t('orders', 'Оставить отзыв') : _t('orders', 'Завершить'); ?>
                                                    </button>
                                                    <div class="text-left mrgt10">
                                                        <div class="bold"><?= _t('','Вы наняли')?></div>
                                                        <div class="user-box mrgt5">
                                                            <div class="user-box__avatar user-box__avatar_sq">
                                                                <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $performerLink['login'])); ?>">
                                                                    <img src="<?= $v['performer_avatar_url']?>" alt="">
                                                                </a>
                                                            </div>
                                                            <div class="user-box__text mrgl10">
                                                                <? if(isset($v['performer_spec_data']) && !empty($v['performer_spec_data'])):?>
                                                                    <span class="user-box__text-s">
                                                                        <?=$v['performer_spec_data']['title'];?>
                                                                    </span>
                                                                <?endif;?>
                                                                <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $performerLink['login'])); ?>">
                                                                    <? if(!empty($performerLink['name'])): ?>
                                                                        <?= $performerLink['name'] ?>
                                                                    <? endif; ?>
                                                                    <? if(!empty($performerLink['surname'])): ?>
                                                                        <?= mb_strimwidth($performerLink['surname'], 0, 2, "."); ?>
                                                                    <? endif; ?>
                                                                </a>
                                                                <? if(isset($v['performer_contacts_phones']) && !empty($v['performer_contacts_phones'])):?>
                                                                    <span class="">
                                                                        <?=$v['performer_contacts_phones'][0]['v']?>
                                                                    </span>
                                                                <?endif;?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endif; ?>
                                            </div>
                                        </div>
                                    <? endif; endif; ?>
                                <? else: ?>
                                    <div class="alert alert-chosen o-freelancer-defined">
                                        <span><?= _t('orders', 'Исполнитель определен:'); ?></span>
                                        <?= tpl::userLink($performerLink, 'no-login') ?>
                                    </div>
                                <? endif; ?>
                                <? endif; ?>

                                <? if(empty($v['performer_id']) && ! empty($v['declined'])): ?>
                                    <div class="alert alert-decline p-profile-alert p-profile-alert">
                                        <i class="fa fa-times pull-right"></i>
                                        <span><?= _t('', 'Исполнитель'); ?> <?= tpl::userLink($v['declined'], 'no-login no-verified no-pro') ?></span>
                                        <strong><?= _t('orders', 'отказался от предложения'); ?></strong>.
                                        <? if( ! empty($v['declined']['message'])): ?>
                                            <div class="alert-in mrgt10">
                                                <strong><?= _t('orders', 'Сообщение от исполнителя:'); ?></strong>
                                                <div><?= nl2br($v['declined']['message']); ?></div>
                                            </div>
                                        <? endif; ?>
                                    </div>
                                <? endif; ?>


                                <div class="text-center mrgt10">
                                    <div class="relative">
                                        <a href="javascript:volid(0);" data-toggle="dropdown" class="drop-link drop-link_fz13 flex flex_center flex_jcc flex_0-0-a">
                                            <?= _t('','еще')?>
                                            <i class="mrgl5 icon-arrow-point-to-down"></i>
                                        </a>
                                        <ul class="dropdown-menu m0-a text-left w100p" >
                                            <li>
                                                <? if($fairplayEnabled && ! empty($v['workflow']['url'])): ?>
                                                    <a href="<?= $v['workflow']['url'] ?>">
                                                        <i class="fa fa-rocket c-link-icon"></i>
                                                        <?= _t('fp', 'Ход работ'); ?>
                                                    </a>
                                                <? endif; ?>
                                            </li>
                                            <li>
                                                <a href="<?= Orders::url('edit', array('id'=>$v['id'])); ?>">
                                                    <?= _t('form', 'Редактировать'); ?>
                                                </a>
                                            </li>
                                            <? if($v['status'] == Orders::STATUS_ONACTIVATION):?>
                                                <li>
                                                    <a href="<?= Orders::url('activation', array('id'=>$v['id'])); ?>">
                                                        <?= _t('form', 'Активировать'); ?>
                                                    </a>
                                                </li>
                                            <? endif;?>
                                            <? if($openClose): ?>
                                                <li>
                                                    <a href="#" class="<?= $v['status'] != Orders::STATUS_OPENED ? 'hidden' : '' ?> j-hide" data-id="<?= $v['id'] ?>">
                                                        <?= _t('orders', 'Снять с публикации'); ?>
                                                    </a>
                                                    <a href="#" class="<?= $v['status'] != Orders::STATUS_CLOSED ? 'hidden' : '' ?> j-show" data-id="<?= $v['id'] ?>">
                                                        <?= _t('orders', 'Публиковать заново'); ?>

                                                    </a>
                                                </li>
                                            <? endif; ?>
                                            <? if($delete): ?>
                                                <li>
                                                    <a href="#" class="link-delete j-delete" data-id="<?= $v['id'] ?>">
                                                        <?= _t('form', 'Удалить'); ?>
                                                    </a>
                                                </li>
                                            <? endif; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if (User::isWorker()): ?>
                            <div class="">
                                <a href="" class="btn btn-primary btn-block">
                                    <?= _t('','Отправить отклик')?>
                                </a>
                            </div>
                            <div class="mrgt10">
                                <?= $this->getOrderNote($v['id']);?>
                            </div>

                        <? endif ?>
                        </div>
                    </div>
                </li>
            <? endforeach; ?>
        </ul>
<? else: ?>
        <div class="mrgt10 text-center">
            <div class="max-w400 m0-a">
                <? if( ! Users::useClient() || (Users::useClient() && User::isClient())): ?>
                    <div class="mrgt20 mrgb20">
                        <?= _t('','У Вас еще нет объявлений. Создайте объявление, чтобы найти няню или бебиситтера.')?>
                    </div>
                    <div class=" flex flex_sb flex_wrap">
                        <a href="<?= Orders::url('add')?>" class="btn btn-primary btn-block-sm mrgt10">
                            <?= _t('orders', 'Создать объявление'); ?>
                        </a>

                        <a href="<?= Users::url('list')?>" class="btn btn-primary btn-primary_empty  btn-block-sm mrgt10">
                            <?= _t('orders', 'Искать маминзама'); ?>
                        </a>
                    </div>
                <? else: ?>
                    <?= _t('','У родителя еще нет объявлений')?>

                <? endif; ?>
            </div>
        </div>
<? endif; ?>