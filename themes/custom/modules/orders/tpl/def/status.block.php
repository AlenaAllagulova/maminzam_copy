<?php
/**
 * @var $this Orders
 */
?>
<? if(!$moderated && Orders::premoderation()): ?>
    <div class="text-center order__text mrgb10" role="alert"><?= _t('orders', 'Ожидает проверки модератора') ?></div>
<? endif; ?>
<? if($status == Orders::STATUS_BLOCKED): ?>
    <div class="text-center order__text mrgb10" role="alert">
        <img src="<?= bff::url('/img/sad.svg')?>" width="40" alt="" class="mrgb5">
        <br />
        <?= _t('orders', 'Заблокирован модератором.') ?>
        <? if( ! empty($blocked_reason)): ?>
            <br />
            <?= _t('', 'Причина блокировки:'); ?>
            <br />
            <i>
                <?= tpl::blockedReason($blocked_reason) ?>
            </i>
        <? endif; ?>
    </div>
<? endif; ?>