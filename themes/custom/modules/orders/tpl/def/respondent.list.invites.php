<?php
if( ! empty($ordersInvites)): ?>
<div id="j-orders-invites">
    <? foreach($ordersInvites as $v): ?>
        <div class="j-invite" data-id="<?= $v['id'] ?>">
            <div class="row order__box">
                <div class="col-md-9 col-sm-12">
                    <div class="order__item">
                        <div class="order__header">
                            <div class="flex flex_column flex_center-sm">
                                <div class="order__status">
                                    <?if(isset($aOrderMainStatuses[$v['status']])):?>
                                        <span class="<?= $aOrderMainStatuses[$v['status']]['class']?>">
                                                    <i></i>
                                            <?= $aOrderMainStatuses[$v['status']]['order_t']?>
                                                </span>
                                    <? endif;?>
                                </div>
                                <div class="flex flex_center flex_wrap">
                                    <a href="<?= $v['link'] ?>" target="_blank" class="order__title"><?= $v['title'] ?></a>
                                </div>
                                <? if( ! empty($v['city_data']['title'])): ?>
                                    <div class="order__text">
                                        <?= $v['city_data']['title']?>
                                    </div>
                                <? endif; ?>
                                <? if(!empty($v['district_id'])):?>
                                    <div class="order__text">
                                        <?= Geo::districtTitle($v['district_id'])?>
                                    </div>
                                <?endif;?>
                            </div>
                            <div class="order__price">
                                <? if ($v['type'] == Orders::TYPE_SERVICE): ?>
                                    <? if ($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                                        <?= !empty($v['price_rate_text'][LNG]) ? $v['price_rate_text'][LNG] : _t('orders', 'По договоренности'); ?>
                                    <? else: ?>
                                        <?= tpl::formatPrice($v['price']) ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?>
                                        <? if (!empty($v['price_rate_text'][LNG])): ?>
                                            <?= $v['price_rate_text'][LNG] ?>
                                        <? endif; ?>
                                    <? endif; ?>
                                <? endif; ?>
                            </div>
                        </div>
                        <div class="order__conditions mrgt10">
                            <? if (!empty($v['dynprops_simple'])): # краткий вывод динсвойств patr 2 c обработкой результата ?>
                                <? foreach ($v['dynprops_simple'] as $dp_item): ?>
                                    <? if ($dp_item['group_id'] == Orders::DP_ORDER_VIEW_PART_SECOND): ?>
                                        <div class="order__conditions-item">
                                            <? if ($dp_item['value'] !== ''): ?>
                                                <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                                    <? foreach ($dp_item['multi'] as $val): ?>
                                                        <? if ($dp_item['value'] == $val['value']):
                                                            if (strpos($val['name'], '(')) {
                                                                $val['name'] = substr($val['name'], strpos($val['name'], '(') + strlen('('));
                                                                $val['name'] = strstr($val['name'], ')', true);
                                                            } else {
                                                                $val['name'] = (int)$val['name'];
                                                            }
                                                            ?>
                                                            <div class="bold"><?= $val['name'] ?></div>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                <? else: ?>
                                                    <div class="bold">
                                                        <? foreach ($dp_item['value'] as $dp_val): ?>
                                                            <? foreach ($dp_item['multi'] as $val): ?>
                                                                <? if ($dp_val == $val['value']):
                                                                    if (strpos($val['name'], '(')) {
                                                                        $val['name'] = substr($val['name'], strpos($val['name'], '(') + strlen('('));
                                                                        $val['name'] = strstr($val['name'], ')', true);
                                                                    } else {
                                                                        $val['name'] = (int)$val['name'];
                                                                    }
                                                                    ?>
                                                                    <?= $val['name'] ?> <br>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? endforeach; ?>
                                                    </div>
                                                <? endif; ?>
                                            <? else: ?>
                                                <th><?= _t('dp_orders', 'не указано') ?></th>
                                            <? endif; ?>
                                            <span class="order__second-text"><?= $dp_item['description_' . LNG] ?></span>
                                        </div>
                                    <? endif; ?>
                                <? endforeach; ?>
                            <? endif; ?>
                            <div class="order__conditions-item">
                                <div class="bold">
                                    <?= ( $v['start_date'] != '0000-00-00 00:00:00')? tpl::date_format2( $v['start_date'], false, true): _t('', 'не указана'); ?>
                                </div>
                                <span class="order__second-text">
                                   <?= _t('orders','Дата начала')?>
                                </span>
                            </div>
                        </div>
                        <p class="order__text mrgt10 order__desc">
                            <?= tpl::truncate($v['descr'], config::sysAdmin('orders.search.list.descr.truncate', 250, TYPE_UINT)); ?>
                        </p>
                        <ul class="order-info">
                            <li>
                                <?= tpl::date_format_spent($v['created'], false, true) ?>
                            </li>
                            <li>
                                <?=_t('', 'Просмотров [cnt]', ['cnt' => $v['views_total']])?>
                            </li>
                            <li>
                                <a href="<?= $v['link'].'#offers'?>">
                                    <span>
                                        <?= tpl::declension($v['offers_cnt'], _t('orders', 'отклик;отклика;откликов'))?>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-5  order__notif">
                    <div class="text-center order__text mrgb10">
                        <?= _t('','Вам прделожили работу. Отправьте отклик, если Вы заинтересованы.')?>
                    </div>

                    <button class="btn btn-block btn-primary j-agree mrgb10 j-add-offer" data-id="<?= $v['id'] ?>">
                        <?= _t('orders', 'Отправить отклик'); ?>
                    </button>
                    <div class="text-center">
                        <a href="#j-modal-decline-<?= $v['id'] ?>" data-toggle="modal" class="bold link-bold link-bold_fz15">
                            <?= _t('orders', 'Отклонить'); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <? endforeach; ?>
    <? foreach($ordersInvites as $v): ?>
        <div class="modal fade" id="j-modal-decline-<?= $v['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?= _t('orders', 'Отказаться от предложения'); ?></h4>
                    </div>
                    <form method="post">
                        <div class="modal-body">
                            <p><?= _t('orders', 'Вы уверены, что хотите отказаться от заказа [link]?', array('link' => '<a href="'.$v['link'].'" class="nowrap">'.$v['title'].'</a>')); ?></p>
                            <div class="form-group">
                                <label for="modal-decline-reason-<?= $v['id'] ?>"><?= _t('orders', 'Причина отказа'); ?></label>
                                <textarea rows="4" class="form-control" id="modal-decline-reason-<?= $v['id'] ?>" placeholder="<?= _t('orders', 'Укажите причину отказа'); ?>" name="message"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer flex flex_center">
                            <button type="button" class="btn btn-primary j-decline" data-id="<?= $v['id'] ?>">
                                <?= _t('orders', 'Отказаться'); ?>
                            </button>
                            <button type="button" class=" mrgl10 bold link-bold link-bold_fz15" data-dismiss="modal">
                                <?= _t('form', 'Отмена'); ?>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>
<? endif;