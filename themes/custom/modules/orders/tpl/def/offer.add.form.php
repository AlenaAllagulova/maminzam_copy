<?php
    tpl::includeJS(array('qquploader', 'ui.sortable'), true);
    tpl::includeJS('orders.offer.add', false, 3);
    $nDefCurr = config::sys('currency.default');
    $nSpecID = 0;
    $type_service = ($type == Orders::TYPE_SERVICE);
    if ($type_service) {
        $spec = array();
        if( ! empty($specs)){ $spec = reset($specs); }
        if( ! empty($spec['spec_id'])){  $nSpecID = $spec['spec_id']; }
        $aPriceSett = Specializations::i()->aPriceSett($nSpecID);
    }
    $fairplayEnabled = bff::fairplayEnabled();
?>
<div class="">
    <div class="" id="offer-add">

        <form action="" method="post" id="j-offer-add-form" class="form-horizontal" role="form">
        <input type="hidden" name="order_id" value="<?= $id ?>" />
            <div class="l-inside hidden">

                <div class="row ">
                    <label for="text" class="col-sm-2 control-label"><?= _t('orders-offers', 'Описание'); ?></label>
                    <div class="col-sm-10">
                        <textarea name="descr" rows="5" id="text" class="form-control"><?= _t('offer', Orders::OFFER_TEMPLATE);?></textarea>
                    </div>
                </div>
            </div>

            <a class="btn btn-primary btn-block j-submit ">
                <?= _t('orders-offers', 'Отправить отклик'); ?>
            </a>


        </form>

    </div><!-- /.collapse -->
</div><!-- /.bordered-block no-color -->
<script type="text/javascript">
<? js::start() ?>
jOrdersOfferAddForm.init(<?= func::php2js(array(
    'lang' => array(
        'upload_typeError'    => _t('form', 'Допустимы только следующие типы файлов: {extensions}'),
        'upload_sizeError'    => _t('form', '"Файл {file} слишком большой, максимально допустимый размер {sizeLimit}'),
        'upload_minSizeError' => _t('form', 'Файл {file} имеет некорректный размер'),
        'upload_emptyError'   => _t('form', 'Файл {file} имеет некорректный размер'),
        'upload_limitError'   => _t('form', 'Вы можете загрузить не более {limit} файлов'),
        'upload_onLeave'      => _t('form', 'Происходит загрузка изображения, если вы покинете эту страницу, загрузка будет прекращена'),
    ),
    'examplesLimit' => Orders::offerExamplesLimit(),
    'imgMaxSize'    => $img->getMaxSize(),
    'imgUploaded'   => 0,
    'imgData'       => array(),
)) ?>);
<? js::stop() ?>
</script>