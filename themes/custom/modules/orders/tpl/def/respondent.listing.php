<?php
/**
 * Список заказов пользователя (layout)
 * @var $this Orders
 */
tpl::includeJS('orders.respondent.list', false, 4);
tpl::includeJS('orders.opinions', false, 2);

tpl::includeJS(['dist/rating']);
?>
<? if( ! empty($performerStart) ||  ! empty($ordersInvites)): ?>
<div class="mrgt20">
    <?= ! empty($ordersInvites) ? $ordersInvites : '' ?>
    <?= ! empty($performerStart) ? $performerStart : '' ?>
</div>
<? endif; ?>
<div class="p-profileContent pdt0" id="j-orders-respondent-list">

    <div class="pdb10 mrgb10">
        <form method="get" action="" id="j-orders-respondent-list-form" class="mrgt10">
            <input type="hidden" name="st" value="<?= $f['st'] ?>" />
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />


        <div class="dropdown hidden-lg">
            <div class=" display-inline">
                <a href="#" class="link-def_tdn link-bold flex flex_center flex_0-0-a" data-toggle="dropdown">
                    <span class="j-f-status-title"><?= $types[ $f['st'] ]['t'] ?></span>
                    <small class="mrgl5 j-f-status-cnt">(<?= $types[ $f['st'] ]['c'] ?>)</small>
                    <i class="mrgl5 icon-arrow-point-to-down"></i>
                </a>
                <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                    <? foreach($types as $v): ?>
                        <li<?= $v['id'] == $f['st'] ? ' class="active"' : '' ?>><a href="#" class="j-f-status" data-id="<?= $v['id']?>"><?= $v['t'] ?> <small class="j-cnt-status-<?= $v['id'] ?>">(<?= $v['c'] ?>)</small></a></li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>

        <ul class="p-profile-submenu visible-lg">
            <? foreach($types as $v): ?>
                <li<?= $v['id'] == $f['st'] ? ' class="active"' : '' ?>><a href="#" class="j-f-status" data-id="<?= $v['id']?>"><?= $v['t'] ?> <small class="j-cnt-status-<?= $v['id'] ?>">(<?= $v['c'] ?>)</small></a></li>
            <? endforeach; ?>
        </ul>

        <div class="clearfix"></div>
        </form>


    </div><!-- /.p-portfolioSection-dropdown -->

    <div class="j-list"><?= $list ?></div>

    <div class="j-pagination"><?= $pgn ?></div>

</div><!-- /.p-profile-content -->
<script type="text/javascript">
<? js::start() ?>
jOrdersRespondentList.init(<?= func::php2js(array(
    'lang'  => array(
        'decline_short' => _t('orders', 'Укажите причину отказа'),
    ),
    'types' => $types,
    'ordersOpinions' => Orders::ordersOpinions(),
    'invitesEnabled' => Orders::invitesEnabled(),
    'ajax'  => false,
)) ?>);
jOrdersOpinions.init(<?= func::php2js(array(
    'lang'   => array(
        'left'        => _t('users','[symbols] осталось'),
        'symbols'     => explode(';', _t('users', 'знак;знака;знаков')),
        'you_opinion' => _t('orders', 'Ваш отзыв'),
        'check_type'  => _t('opinions', 'Укажите тип отзыва'),
    ),
    'block' => 'j-orders-respondent-list',
)) ?>);
<? if (Rating::isActive()): ?>
(function() {
    $(function() {
        if (typeof Rating === 'function') {
            var ratingModal = new Rating();
            ratingModal.initModalAdd(<?= Rating::JS_PARAMETERS_EDIT(
                '.j-opinion-add',
                Rating::JS_CLASS_RATING_STARS_BOX_MODAL
            ) ?>);

            var ratingModalView = new Rating();
            ratingModalView.initModalView(<?= Rating::JS_PARAMETERS_VIEW(
                Rating::JS_CLASS_RATING_STARS_ITEM_READONLY,
                '#j-modal-opinion-',
                '.j-opinion-show'
            ) ?>);
        }
    });
})($);
<? endif; ?>
<? js::stop() ?>
</script>