<?php
/**
 * Список заказов: Поиск на главной
 * @var $this Orders
 */
    $bMapsEnabled = Orders::mapEnabled();
    if ($bMapsEnabled) {
        Geo::mapsAPI(true);
        if (Geo::mapsType() == Geo::MAPS_TYPE_GOOGLE) {
            tpl::includeJS('markerclusterer/markerclusterer', false);
        }
    }

    tpl::includeJS('orders.search', false, 6);
    if (Site::indexView(Site::INDEX_VIEW_ORDERS_PLUS)) {
        echo Site::i()->index_header_slider();
    }

    $nUserID = User::id();
    $bShowButton = ( ! $nUserID || ($nUserID && (!Users::useClient() || (Users::useClient() && User::isClient()))) );
    /* согласно интерфейсу отсутствует карусель пользователей
    echo Users::i()->svc_carousel_block(); */
?>
    <div class="container">


        <section class="l-mainContent" id="j-orders-search">
            <div class="row">
                <aside class="col-md-3">

                    <?= $form ?>

                    <div class="visible-md visible-lg">
                    <?
                        echo Users::i()->svc_stairs_block(Users::SVC_STAIRS_MAIN);
                    ?>
                    <? # Баннер: Заказы: список ?>
                    <?= Banners::view('orders_list', array('pos'=>'left', 'spec'=>(!empty($nSpecID) && $nSpecID>1? $nSpecID : 0))) ?>
                    </div>

                </aside>

                <div class="col-md-9" id="j-orders-search-list">
                    <? if( ! Orders::useProducts()):
                            $aServiceTypes = array(
                                Orders::SERVICE_TYPE_NONE    => array('id' => Orders::SERVICE_TYPE_NONE,    't' => _t('orders','Все')),
                                Orders::SERVICE_TYPE_ONE     => array('id' => Orders::SERVICE_TYPE_ONE,     't' => _t('orders','Разовые заказы')),
                                Orders::SERVICE_TYPE_JOB     => array('id' => Orders::SERVICE_TYPE_JOB,     't' => _t('orders','Постоянная работа')),
                                Orders::SERVICE_TYPE_CONTEST => array('id' => Orders::SERVICE_TYPE_CONTEST, 't' => _t('orders','Конкурсы')),
                            );
                            $aServiceType = isset($aServiceTypes[ $f['st'] ]) ? $aServiceTypes[ $f['st'] ] : reset($aServiceTypes);
                    ?>


                    <? else:
                        $aTypes = Orders::aTypes();
                        $aType = isset($aTypes[ $f['t'] ]) ? $aTypes[ $f['t'] ] : $aTypes[ Orders::TYPE_SERVICE ];
                        ?>
                        <div class="l-menu-filter hidden-sm">
                            <button type="button" class="l-tabs-toggle btn btn-default" data-toggle="collapse" data-target="#j-f-type-filter"><i class="fa fa-chevron-down pull-right"></i><span><?= $aTypes[ $f['t'] ]['plural'] ?></span></button>
                            <ul class="nav nav-tabs collapse" id="j-f-type-filter">
                                <? foreach($aTypes as $v): ?>
                                    <li<?= $f['t'] == $v['id'] ? ' class="active"' : '' ?>><a href="<?= Orders::url('list', $v['id'] == Orders::TYPE_SERVICE ? array('clear' => 1) : array('t' => $v['id'], 'clear' => 1)); ?>"><?= $v['plural']; ?></a></li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    <? endif; ?>

                    <h6 class="h6-text visible-lg visible-md"><?= _t('orders', 'Найдено'); ?>
                        <span class="j-orders-count"><?= $count ?></span>
                    </h6>

                    <div class="l-mapView <?= ($f['m'] != 1 ? 'hidden' : '') ?>">
                        <div id="map-desktop" class="map-google"></div>
                    </div>

                    <? if($f['m'] == 1): # карта ?>
                        <div class="j-pagination"><?= $pgn ?></div>
                        <div class="j-list"><?= $list ?></div>
                    <? else: # список ?>
                        <div class="j-list">
                            <?= $list ?>
                        </div>
                        <? if( ! empty($rss)): ?>
                        <div class="dropdown j-rss">
                            <a href="#" class="ajax-link" id="j-rss-popup-link"><i class="fa fa-rss c-link-icon"></i><span><?= _t('', 'Подписка через RSS'); ?></span></a>
                            <div id="j-rss-popup" class="c-rss-subscribe" style="display: none;">
                                <div class="form-group">
                                    <i class="fa fa-rss c-link-icon"></i><?= _t('', 'Подписка через RSS'); ?>
                                </div>
                                <form class="form" method="get" action="<?= Orders::url('rss'); ?>">
                                    <input type="hidden" name="t" value="<?= $rss['t'] ?>">
                                    <div class="form-group<?=  empty($rss['cat']) ? ' hidden' : '' ?>">
                                        <select name="cat_id" class="form-control input-sm" autocomplete="off"><?= HTML::selectOptions($rss['cat'], $rss['cat_id'], false) ?></select>
                                    </div>
                                    <div class="form-group<?=  empty($rss['sub']) ? ' hidden' : '' ?>">
                                        <select name="sub_id" class="form-control input-sm" autocomplete="off"><?= HTML::selectOptions($rss['sub'], $rss['sub_id'], false) ?></select>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-xs mrgr10"><?= _t('', 'Подписаться'); ?></button>
                                    <a href="#" class="ajax-link j-close"><span><?= _t('form', 'Отмена'); ?></span></a>
                                </form>
                            </div>
                        </div>
                        <? endif; ?>
                        <div class="j-pagination"><?= $pgn ?></div>
                    <? endif; ?>

                </div><!-- /.col-md-9 -->

            </div>
        </section>

    </div>

<script type="text/javascript">
<? js::start() ?>
jOrdersSearch.init(<?= func::php2js(array(
    'lang'          => array(),
    'ajax'          => true,
    'type'          => $f['t'],
    'typeService'   => Orders::TYPE_SERVICE,
    'typeProduct'   => (Orders::useProducts() ? Orders::TYPE_PRODUCT : 0),
    'aServiceTypes' => (isset($aServiceTypes) ? $aServiceTypes : array()),
    'oneSpec'       => Orders::searchOneSpec(),
    'locationDefault' => Orders::url('list'),
    'defCountry'    => Geo::defaultCountry(),
    'rootSpec'      => Specializations::ROOT_SPEC,
    'currSpec'      => (! empty($nSpecID) ? $nSpecID : 0),
    'currCat'       => (! empty($nCatID) ? $nCatID : 0),
    'points'        => (! empty($points) ? $points : array()),
    'coordorder'    => Geo::$ymapsCoordOrder,
    'coordDefaults' => Geo::$ymapsDefaultCoords,
    'preSuggest'    => (Geo::countrySelect() && $f['c'] ? Geo::regionPreSuggest($f['c'], 2) : ''),
)) ?>);
<? js::stop() ?>
</script>