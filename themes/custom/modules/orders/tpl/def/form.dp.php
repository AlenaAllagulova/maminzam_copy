<?php

use bff\db\Dynprops;

/**
 * @var $this Dynprops
 */
$prefix = 'd';

$drawControl = function($title, $value, $required, $class = array()){
    if( ! is_array($class)) $class = array($class);
    ?>
    <div class="form-def__box">
        <div class="form-group<? if($required) { ?> j-required<? } ?>">
            <label class=" control-label o-control-label">
                <?= $title ?>
                <?= $required ? ' <span class="text-danger">*</span>' : ''?>
            </label>
            <div class="form-def__flex <?= join(' ', $class) ?>">
                <?= $value ?>
            </div>
        </div>
    </div>
<?

};

# ---------------------------------------------------------------------------------------
# Дин. свойства:
foreach($dynprops as $d)
{
    $ID = $d['id'];
    $ownerID = $d[$this->ownerColumn];
    $name = $prefix.'['.$ownerID.']'.'['.$ID.']';
    $nameChild = $prefix.'['.$ownerID.']';
    $isRequred = $d['req'];
    $html = '';
    $class = array('j-dp');


    switch($d['type'])
    {
        # Группа св-в с единичным выбором
        case Dynprops::typeRadioGroup:
        {
            $value = (isset($d['value'])? $d['value'] : $d['default_value']);
            $class = ! empty($d['group_one_row']) ? 'radio-inline' : 'radio';
            $html = HTML::renderList($d['multi'], array($value), function($k,$i,$values) use ($name, $extra) {
                    $v = &$i['value'];
                    return '<div class="radio">
                                <label>
                                    <input type="radio" name="'.$name.'" '
                                    .(in_array($v,$values)?' checked="checked"':'').' value="'.$v.'" />
                                    '.$i['name'].'
                                </label>
                            </div>';
                },
                array(2=>4),
                array('class'=>''),
                'div'
            );
            $drawControl($d['title_'.LNG], $html, $isRequred, '');
        } break;

        # Группа св-в с множественным выбором
        case Dynprops::typeCheckboxGroup:
        {
            $value = ( isset($d['value']) && $d['value'] ? explode(';', $d['value']) : explode(';', $d['default_value']) );
            $class = ! empty($d['group_one_row']) ? 'checkbox-inline' : 'checkbox';
            $html = HTML::renderList($d['multi'], $value, function($k,$i,$values) use ($name, $extra) {
                    $v = &$i['value'];
                    return '<div class="checkbox">
                                <label>
                                    <input type="checkbox" name="'.$name.'[]" '
                                        .(in_array($v,$values)?' checked="checked"':'').' value="'.$v.'" />
                                    <span class="checkbox__text checkbox__text_grey">'.$i['name'].'</span>
                                </label>
                            </div>';
                },
                array(2=>4),
                array('class'=>'form-def__row-5 '),
                'div'
            );
            $drawControl($d['title_'.LNG], $html, $isRequred, '');
        } break;

        # Выбор Да/Нет
        case Dynprops::typeRadioYesNo:
        {
            $value = (isset($d['value'])? $d['value'] : $d['default_value']);
                $html = '<div class="radio-inline">
                            <label>
                                <input type="radio" name="'.$name.'" value="2" '.($value == 2?'checked="checked"':'').' />
                                    '.$this->langText['yes'].' 
                            </label> 
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="'.$name.'" value="1" '.($value == 1?'checked="checked"':'').' />
                                    '.$this->langText['no'].' 
                            </label>
                         </div>';
            $drawControl($d['title_'.LNG], $html, $isRequred, '');
        } break;

        # Флаг
        case Dynprops::typeCheckbox:
        {
            $value = (isset($d['value'])? $d['value'] : $d['default_value']);
            $html = '<input type="hidden" name="'.$name.'" value="0" />
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="'.$name.'" value="1" '.($value?'checked="checked"':'').' />
                                 '.$this->langText['yes'].' 
                            </label>
                        </div>';
            $drawControl($d['title_'.LNG], $html, $isRequred, '');
        } break;

        # Выпадающий список
        case Dynprops::typeSelect:
        {
            $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
            if($d['parent'])
            {
                $html = '<select class="form-control j-dp-child" name="'.$name.'" data-id="'.$d['id'].'" data-name="'.$nameChild.'">';
                $html .= HTML::selectOptions($d['multi'], $value, false, 'value', 'name');
                $html .= '</select>';
                if( ! empty($d['description']) ) {
                    $html .= '';
                }
                $htmlChild = '';
                if( ! empty($value) && isset($children[$ID])) {
                    $htmlChild .= $this->formChild($children[$ID], array('name'=>$nameChild,'class'=>'form-control'));
                }
                ?>
                <div class="form-group<? if($isRequred) { ?> j-required<? } ?>">
                    <label class=" control-label o-control-label">
                        <?= $d['title_'.LNG] ?><?= $isRequred ? ' <span class="text-danger">*</span>' : ''?>
                    </label>
                    <div class="">
                        <?= $html ?>
                    </div>
                    <div class="j-dp-child-<?= $ID ?><?= empty($value) ? ' hidden' : '' ?>">
                        <?= $htmlChild ?>
                    </div>
                </div>
                <?
                continue 2;
            }
            else
            {
                $html = '<div class="select-custom_global form-def__select">
                            <select class="j-select form-control" name="'.$name.'">
                                '.HTML::selectOptions($d['multi'], $value, false, 'value', 'name').'
                            </select>
                        </div>'
                ;
                if( ! empty($d['description']) ) {
                    $html .= '';
                }
                $drawControl($d['title_'.LNG], $html, $isRequred, '');
            }
        } break;

        # Однострочное текстовое поле
        case Dynprops::typeInputText:
        {
            $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
            $html = '<input class="form-control" type="text" name="'.$name.'" value="'.HTML::escape($value).'" />';
            if( ! empty($d['description']) ) {
                $html .= '<p class="help-block">'.$d['description'].'</p>';
            }
            $drawControl($d['title_'.LNG], $html, $isRequred, '');
        } break;

        # Многострочное текстовое поле
        case Dynprops::typeTextarea:
        {
            $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
            $html = '<textarea name="'.$name.'" rows="5" class="form-control">'.HTML::escape($value).'</textarea>';
            # уточнение к названию
            if( ! empty($d['description']) ) {
                $html .= '<p class="help-block">'.$d['description'].'</p>';
            }
            $drawControl($d['title_'.LNG], $html, $isRequred, '');
        } break;

        # Число
        case Dynprops::typeNumber:
        {
            $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
            $html = '<div class="input-group"> <input type="text" class="form-control" name="'.$name.'" value="'.$value.'" />';
            $sClass = '';
            if( ! empty($d['description']) ) {
                if( mb_strlen(strip_tags($d['description'])) <=5 ) {
                    $html .= '<span class="input-group-addon">'.$d['description'].'</span>';
                } else {
                    $html .= '<p class="help-block">'.$d['description'].'</p>';
                }
            }
            $html .= '</div>';
            $drawControl($d['title_'.LNG], $html, $isRequred, array('', $sClass));
        } break;

        # Диапазон
        case Dynprops::typeRange:
        {
            $value = (isset($d['value']) && $d['value'] ? $d['value'] : $d['default_value']);

            $html = '<select name="'.$name.'" class="form-control">';
            if( ! empty($value) && ! intval($value)) {
                $html .= '<option value="0">'.$value.'</option>';
            }
            if($d['start'] <= $d['end']) {
                for($i = $d['start']; $i <= $d['end'];$i += $d['step']) {
                    $html .= '<option value="'.$i.'"'.($value == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
                }
            } else {
                for($i = $d['start']; $i >= $d['end'];$i -= $d['step']) {
                    $html .= '<option value="'.$i.'"'.($value == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
                }
            }
            $html .= '</select>';

            # уточнение к названию
            if( ! empty($d['description']) ) {
                $html .= '<p class="help-block">'.$d['description'].'</p>';
            }
            $drawControl($d['title_'.LNG], $html, $isRequred, '');
        } break;
    }

}
