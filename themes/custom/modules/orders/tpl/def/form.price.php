<?php
$aPriceSett = array(
    'rates' => array(),
    'price_title' => _t('orders', 'Бюджет'),
    'price_title_mod' => _t('orders', 'По договоренности'),
    'curr' => 0,
);
if( ! empty($spec['spec_id'])){
    $aPriceSett = Specializations::i()->aPriceSett($spec['spec_id']);
}
$bShowPriceEx = ! empty($aPriceSett['ex']);
?>
<div class="form-def__box">
    <div class="form-group o-propose-inputs">
        <label for="price" class=" control-label o-control-label"><?= $aPriceSett['price_title'] ?></label>
        <div class="">
            <div class="input-group <?= $bShowPriceEx ? 'radio' : '' ?> j-price-ex-radio">
                <input class="<?= $bShowPriceEx ? '' : 'hidden' ?> j-price-ex j-price-ex-<?= Specializations::PRICE_EX_PRICE ?>" type="radio" name="price_ex" value="<?= Specializations::PRICE_EX_PRICE ?>" <?= $price_ex == Specializations::PRICE_EX_PRICE ? ' checked="checked"' : '' ?> />
                <input type="text" name="price" value="<?= $price ?>" maxlength="9" class="form-control input-sm" />
                <span class="select-custom_global mrgl10">
                    <select name="price_curr" class="j-select form-control input-sm"><?= Site::currencyOptions( ! empty($price_curr) ? $price_curr : $aPriceSett['curr']) ?></select>
                </span>
                <span class="select-custom_global mrgl10">
                    <select name="price_rate"  class="j-select form-control input-sm <?= empty($aPriceSett['rates']) ? 'hidden' : '' ?>"><?= ! empty($aPriceSett['rates']) ? HTML::selectOptions($aPriceSett['rates'], $price_rate) : '' ?></select>
                </span>
            </div>
            <div class="radio <?= $bShowPriceEx && ($aPriceSett['ex'] & Specializations::PRICE_EX_AGREE)  ? '' : 'hidden' ?> j-price-ex j-price-ex-<?= Specializations::PRICE_EX_AGREE ?>">
                <label>
                    <input type="radio" name="price_ex" value="<?= Specializations::PRICE_EX_AGREE ?>" <?= $price_ex == Specializations::PRICE_EX_AGREE ? ' checked="checked"' : '' ?>> <?= $aPriceSett['price_title_mod'] ?>
                </label>
            </div>
        </div>
    </div>
</div>