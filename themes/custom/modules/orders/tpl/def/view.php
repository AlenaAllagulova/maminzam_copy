<?php
    /**
     * Просмотр заказа
     * @var $this Orders
     */
    tpl::includeJS('orders.order.view', false, 4);
    tpl::includeJS(['dist/custom']);

    tpl::includeJS(['dist/rating']);

    $bMapEnabled = Orders::mapEnabled() && ($addr_lat != 0) && ($addr_lng != 0);
    if ($bMapEnabled) {
        Geo::mapsAPI(true);
    }

    if( ! empty($regions)){
        $aFirstRegions = reset($regions);
    }

    $bOwner = User::isCurrent($user_id);

    $bAnonymous = true;
    switch(Orders::ordersContactsView()){
        case Orders::ORDERS_CONTACTS_VIEW_ALL:
            $bAnonymous = false;
            break;
        case Orders::ORDERS_CONTACTS_VIEW_AUTH:
            if(User::id()){
                $bAnonymous = false;
            }
            break;
        case Orders::ORDERS_CONTACTS_VIEW_PRO:
            if(User::isPro()){
                $bAnonymous = false;
            }
            break;
    }
    $bDeleteAllow = Orders::deleteAllow();
    $ordersOpinions = Orders::ordersOpinions();
    $invitesEnabled = Orders::invitesEnabled();
    $fairplayEnabled = bff::fairplayEnabled();
?>
    <div class="container">

        <?= tpl::getBreadcrumbs($breadcrumbs); ?>

        <section class="l-mainContent">
            <div class="row">

            <!-- Project-->
            <div class="col-md-9" id="j-order-view">
                <div class="control-vue control-vue_flex-end mrgb10">
                    <? if($visibility == Orders::VISIBILITY_ALL):
                    $aConfig = $this->configLoad();
                    if( ! empty($aConfig['share_code'])): ?>
                        <div class="dropdown">
                            <a href="" data-toggle="dropdown" class="control-vue__item drop-link drop-link_fz13">
                                <i class="control-vue__ icon-share"></i>
                            </a>
                            <div class="dropdown-menu c-dropdown-caret_right pull-right">

                                <?= $aConfig['share_code'] ?>

                            </div>
                        </div>
                    <?  endif; endif; ?>
                    <a href="" class="j-print-window control-vue__item">
                        <i class="icon-printer"></i>
                    </a>
                </div>
                <div class="order-vue">

                    <div class="order-vue__box mrgb10">
                        <div class="row">
                            <div class="col-md-9">
                                <? if($bAnonymous): ?>
                                    <div class="user-box">
                                        <div class="user-box__avatar">
                                            <img src="<?= UsersAvatar::url(0, '', UsersAvatar::szNormal); ?>" alt="<?= tpl::avatarAlt(); ?>" />
                                        </div>
                                    </div>
                                    <div class="mrgl10">
                                        <div class="user-box__text">
                                            <?= _t('orders', 'Заказчик')?>
                                        </div>
                                    </div>
                                <? else: ?>
                                    <div class="user-box">
                                        <div class="user-box__avatar user-box__avatar_sq-big">
                                            <a href="<?= Users::url('profile', array('login' => $login)) ?>" class="">
                                                <img src="<?= UsersAvatar::url($aData['user_id'], $aData['avatar'], UsersAvatar::szNormal, $aData['sex']); ?>" class="" alt="" />
                                                <?= tpl::userOnline($aData) ?>
                                            </a>
                                        </div>
                                        <div class="mrgl10">
                                            <div>
                                                <div class="user-box__text">
                                                    <?= tpl::userLink($aData, 'no-login, no-verified') ?>
                                                </div>
                                                <?= Rating::getViewTotalAverage($user_id) ?>
                                            </div>
                                            <div class="order__conditions mrgt10">
                                                <? if (!empty($dynprops_simple)): # краткий вывод динсвойств patr 2 c обработкой результата ?>
                                                    <? foreach ($dynprops_simple as $dp_item): ?>
                                                        <? if ($dp_item['group_id'] == Orders::DP_ORDER_VIEW_PART_SECOND ): ?>
                                                            <div class="order__conditions-item">
                                                                <? if($dp_item['value'] !== ''):?>
                                                                    <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                                    <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                                                        <? foreach ($dp_item['multi'] as $val): ?>
                                                                            <? if ($dp_item['value'] == $val['value']):
                                                                                if(strpos($val['name'],'(')){
                                                                                    $val['name'] = substr( $val['name'] , strpos($val['name'],'(')+strlen('('));
                                                                                    $val['name'] = strstr($val['name'], ')', true);
                                                                                }else{
                                                                                    $val['name'] = (int)$val['name'];
                                                                                }
                                                                                ?>
                                                                                <div class="bold"><?= $val['name'] ?></div>
                                                                            <? endif; ?>
                                                                        <? endforeach; ?>
                                                                    <? else: ?>
                                                                        <div class="bold">
                                                                            <? foreach ($dp_item['value'] as $dp_val): ?>
                                                                                <? foreach ($dp_item['multi'] as $val): ?>
                                                                                    <? if ($dp_val == $val['value']):
                                                                                        if(strpos($val['name'],'(')){
                                                                                            $val['name'] = substr( $val['name'] , strpos($val['name'],'(')+strlen('('));
                                                                                            $val['name'] = strstr($val['name'], ')', true);
                                                                                        }else{
                                                                                            $val['name'] = (int)$val['name'];
                                                                                        }
                                                                                        ?>
                                                                                        <?= $val['name'] ?> <br>
                                                                                    <? endif; ?>
                                                                                <? endforeach; ?>
                                                                            <? endforeach; ?>
                                                                        </div>
                                                                    <? endif; ?>
                                                                <? else: ?>
                                                                    <th><?= _t('dp_orders','не указано') ?></th>
                                                                <? endif;?>
                                                                <span class="order__second-text"><?= $dp_item['description_'.LNG] ?></span>
                                                            </div>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                <? endif; ?>
                                                <div class="order__conditions-item">
                                                    <div class="bold">
                                                        <?= ( $start_date != '0000-00-00 00:00:00') ? tpl::date_format2($start_date, false, true): _t('', 'не указана'); ?>
                                                    </div>
                                                    <span class="order__second-text">
                                                        <?= _t('orders', 'Дата начала'); ?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>

                                <div class="mrgt20">

                                    <header class="j-title">
                                        <? if($price_ex == Specializations::PRICE_EX_AGREE): ?>
                                            <div class="order-vue__price">
                                                <?= ! empty($price_rate_text[LNG]) ? $price_rate_text[LNG] : _t('orders', 'Договорная цена'); ?>
                                            </div>
                                        <? else: ?>
                                            <div class="order-vue__price">
                                                <?= tpl::formatPrice($price) ?>
                                                <?= Site::currencyData($price_curr, 'title_short'); ?>
                                                <? if( ! empty($price_rate_text[LNG])): ?>
                                                    <?= $price_rate_text[LNG] ?>
                                                <? endif; ?>
                                            </div>
                                        <? endif; ?>
                                        <div class="mrgb10">
                                            <h1 class="order-vue__title">
                                                <? if($fairplayEnabled && $fairplay == Orders::FAIRPLAY_USE): ?>
                                                    <i data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip" class="fa fa-shield c-safe-color show-tooltip"></i>
                                                <? endif; ?>
                                                <?= $title ?>
                                                <? if($order_pro): ?>
                                                    <span class="pro">pro</span>
                                                <? endif; ?>

                                            </h1>
                                            <? if(!empty($district_id)):?>
                                                <div class="order__text">
                                                    <?= Geo::districtTitle($district_id)?>
                                                </div>
                                            <?endif;?>
                                        </div>

                                        <? if($expire != '0000-00-00 00:00:00'): ?>
                                            <div class="mrgb10">
                                                <i class="fa fa-calendar"></i> <strong><?= _t('orders', 'Предложения принимаются до [date]', array('date' => tpl::date_format3($expire, 'd.m.Y'))); ?></strong>
                                            </div>
                                        <? endif; ?>
                                    </header>

                                    <article class="order-vue__text">
                                        <?= nl2br($descr) ?>
                                    </article>

                                    <? if($bMapEnabled): ?>
                                        <div class="l-mapView">
                                            <div id="map-desktop" class="map-google"></div>
                                        </div>
                                    <? endif; ?>

                                    <? if(Orders::imagesLimit() && $aData['imgcnt']): ?>
                                        <div id="j-order-view-carousel" class="owl-carousel l-carousel l-images-carousel">
                                            <? foreach($images as $k => $v): ?>
                                                <div class="item">
                                                    <a href="<?= $v['i'][ OrdersOrderImages::szView ]; ?>" class="fancyzoom" rel="fancy-gallery-<?= $id ?>">
                                                        <img src="<?= $v['i'][ OrdersOrderImages::szSmall ]; ?>" alt="<?= tpl::imageAlt(array('t' => $title, 'k' => $k)); ?>" />
                                                    </a>
                                                </div>
                                            <? endforeach; ?>
                                        </div>
                                    <? endif; ?>

                                    <? if(Orders::attachmentsLimit() && $aData['attachcnt']): ?>
                                        <ul class="o-order-files">
                                            <? foreach($attachments as $v): ?>
                                                <li><a href="<?= $v['i'] ?>" target="_blank"><i class="fa fa-file-o c-li c-link-icon"></i><span><?= $v['origin'] ?></span></a> (<?= $v['size'] ?>)</li>
                                            <? endforeach; ?>
                                        </ul>
                                    <? endif; ?>

                                    <ul class="order-info mrgt10">
                                        <li>
                                            <?= _t('','Опубликовано: ')?>
                                            <?= tpl::date_format_spent($created, false, true) ?>

                                        </li>

                                        <li>
                                            <?= _t('', '[total] просмотров', array('total'=>$views_total)); ?>
                                        </li>

                                        <li>
                                            <?= tpl::declension($aData['offers_cnt'], _t('orders', 'отклик;отклика;откликов'))?>
                                        </li>

                                    </ul>
                                </div>

                                <? if( ! empty($tags)): ?>
                                    <div class="l-inside">
                                        <? foreach($tags as $v): ?>
                                            <a href="<?= Orders::url('search-tag', $v) ?>" class="l-tag"><?= $v['tag'] ?></a>
                                        <? endforeach; ?>
                                    </div>
                                <?  endif; ?>
                            </div>
                            <div class="col-md-3">
                                <? if($status == Orders::STATUS_OPENED): ?>
                                    <div class="text-right">
                                        <span class="order__status-opened">
                                            <i></i>
                                            <?= _t('','Активно')?>
                                        </span>
                                    </div>
                                <? endif; ?>
                                <? if($status == Orders::STATUS_CLOSED): ?>
                                    <div class="text-right">
                                        <span class="order__status-close">
                                            <i></i>
                                            <?= _t('','Закрыто')?>
                                        </span>
                                    </div>
                                <? endif; ?>
    
                                <? if($status == Orders::STATUS_IN_WORK): ?>
                                    <span class="order__status-work">
                                        <i></i>
                                        <?= _t('orders', 'В работе'); ?>
                                    </span>
                                <? endif; ?>
                                
                                <? if($performer_id):?>

                                    <? if($bOwner && $ordersOpinions && ! empty($performer_offer['status']) && $performer_offer['status'] == Orders::OFFER_STATUS_PERFORMER_START):
                                        if( ! empty($performer_offer['workflow'])){ $aData['workflow'] = func::unserialize($performer_offer['workflow']); }
                                        $aData['offerID'] = $performer_offer['id']; ?>
                                        <?= $this->viewPHP($aData, 'offers.list.alert.performer.start'); ?>
                                    <? elseif ($bOwner): ?>
                                        <div class="l-project-author mrgt0">
                                            <div class="bold">
                                                <?= _t('orders', 'Вы наняли:'); ?>
                                            </div>
                                            <div class="user-box mrgb10 mrgt5">
                                                <div class="user-box__avatar user-box__avatar_sq">
                                                    <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $performer['login'])); ?>">
                                                        <img src="<?= UsersAvatar::url($performer_id, $performer['avatar'], UsersAvatar::szNormal, $performer['sex']); ?>" alt="">
                                                    </a>
                                                </div>
                                                <div class="user-box__text mrgl10">
                                                    <div class="user-box__text">
                                                        <? if(isset($performer['specs']) && !empty($performer['specs'])):?>
                                                            <span class="user-box__text-s">
                                                            <? foreach ($performer['specs'] as $spec):?>
                                                                <?if($spec['main']):?>
                                                                    <?=$spec['spec_title'];?>
                                                                <?endif;?>
                                                            <? endforeach;?>
                                                            </span>
                                                        <?endif;?>
                                                        <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $performer['login'])); ?>">
                                                            <?= $performer['name'] ?>
                                                            <? if(!empty($performer['surname'])): ?>
                                                                <?= mb_strimwidth($performer['surname'], 0, 2, "."); ?>
                                                            <? endif; ?>
                                                        </a>
                                                        <div class="user-box__text">
                                                            <? if (Users::isOpenedContacts($performer_id)): # телефон исполнителя и проверка его открытия ?>
                                                                <? if(isset($performer['contacts_phones']) && !empty($performer['contacts_phones'])):?>
                                                                    <span class="">
                                                                <?=$performer['contacts_phones'][0]['v']?>
                                                            </span>
                                                                <?endif;?>
                                                            <? endif;?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="order-vue__row mrgb10">
                        <div class="order-vue__row-big">
                            <div class="order-vue__box mrgb10">
                                <div class="bold mrgb10">
                                    <?= _t('','Детали работы')?>
                                </div>
                                <table class="work-info">
                                    <? if(!empty($district_id)):?>
                                    <tr>
                                        <th><?=_t('','Район')?></th>
                                        <th>
                                            <?= Geo::districtTitle($district_id)?>
                                        </th>

                                    </tr>
                                    <?endif;?>
                                    <? if (!empty($dynprops_simple)): # краткий вывод динсвойств patr 1 ?>
                                        <? foreach ($dynprops_simple as $dp_item): ?>
                                            <? if ($dp_item['group_id'] == Orders::DP_ORDER_VIEW_PART_FIRST ): ?>
                                                <tr>
                                                    <th><?= $dp_item['title'] ?></th>
                                                    <? if($dp_item['value'] !== ''):?>
                                                        <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                        <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                                            <? foreach ($dp_item['multi'] as $val): ?>
                                                                <? if ($dp_item['value'] == $val['value']): ?>
                                                                    <th><?= $val['name'] ?></th>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? else: ?>
                                                            <th>
                                                                <? foreach ($dp_item['value'] as $dp_val): ?>
                                                                    <? foreach ($dp_item['multi'] as $val): ?>
                                                                        <? if ($dp_val == $val['value']): ?>
                                                                            <?= $val['name'] ?> <br>
                                                                        <? endif; ?>
                                                                    <? endforeach; ?>
                                                                <? endforeach; ?>
                                                            </th>
                                                        <? endif; ?>
                                                    <? else: ?>
                                                        <th><?= _t('dp_orders','не указано') ?></th>
                                                    <? endif;?>
                                                    </tr>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    <? endif; ?>
                                    <tr>
                                        <th><?= _t('','Зарплата')?></th>
                                        <th>
                                            <? if($price_ex == Specializations::PRICE_EX_AGREE): ?>
                                                <div class="">
                                                    <?= ! empty($price_rate_text[LNG]) ? $price_rate_text[LNG] : _t('orders', 'Договорная цена'); ?>
                                                </div>
                                            <? else: ?>
                                                <div class="">
                                                    <?= tpl::formatPrice($price) ?>
                                                    <?= Site::currencyData($price_curr, 'title_short'); ?>
                                                    <? if( ! empty($price_rate_text[LNG])): ?>
                                                        <?= $price_rate_text[LNG] ?>
                                                    <? endif; ?>
                                                </div>
                                            <? endif; ?>
                                        </th>
                                    </tr>
                                    <? if (!empty($dynprops_simple)): # краткий вывод динсвойств patr 2 ?>
                                        <? foreach ($dynprops_simple as $dp_item): ?>
                                            <? if ($dp_item['group_id'] == Orders::DP_ORDER_VIEW_PART_SECOND ): ?>
                                                <tr>
                                                    <th><?= $dp_item['title'] ?></th>
                                                    <? if($dp_item['value'] !== ''):?>
                                                        <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                        <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                                            <? foreach ($dp_item['multi'] as $val): ?>
                                                                <? if ($dp_item['value'] == $val['value']): ?>
                                                                    <th><?= $val['name'] ?></th>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? else: ?>
                                                            <th>
                                                                <? foreach ($dp_item['value'] as $dp_val): ?>
                                                                    <? foreach ($dp_item['multi'] as $val): ?>
                                                                        <? if ($dp_val == $val['value']): ?>
                                                                            <?= $val['name'] ?> <br>
                                                                        <? endif; ?>
                                                                    <? endforeach; ?>
                                                                <? endforeach; ?>
                                                            </th>
                                                        <? endif; ?>
                                                    <? else: ?>
                                                        <th><?= _t('dp_orders','не указано') ?></th>
                                                    <? endif;?>
                                                </tr>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    <? endif; ?>
                                </table>
                            </div>
                            <div class="order-vue__box">
                                <div class="bold mrgb10">
                                    <?= _t('','График работы')?>
                                </div>
                                <?$aData['bScheduleViewOnly'] = true;?>
                                <?= Users::i()->viewPHP($aData, 'schedule.block')?>
                            </div>
                        </div>
                        <? if (!empty($dynprops_simple)): # краткий вывод динсвойств patr 3 "Доп помощь" ?>
                            <? foreach ($dynprops_simple as $dp_item): ?>
                                <? if ($dp_item['group_id'] == Orders::DP_ORDER_VIEW_PART_THIRD ): ?>
                                    <? if($dp_item['value'] !== ''):?>
                                        <div class="order-vue__row-small">
                                            <div class="order-vue__box h100p">
                                                <div class="bold"><?= $dp_item['title'] ?></div>
                                                <ul class="list-dp">
                                                    <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                    <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                                        <? foreach ($dp_item['multi'] as $val): ?>
                                                            <? if ($dp_item['value'] == $val['value']): ?>
                                                                <li><?= $val['name'] ?></li>
                                                            <? endif; ?>
                                                        <? endforeach; ?>
                                                    <? else: ?>
                                                        <? foreach ($dp_item['value'] as $dp_val): ?>
                                                            <? foreach ($dp_item['multi'] as $val): ?>
                                                                <? if ($dp_val == $val['value']): ?>
                                                                    <li><?= $val['name'] ?></li>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? endforeach; ?>
                                                    <? endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    <? endif;?>
                                <? endif; ?>
                            <? endforeach; ?>
                        <? endif; ?>
                    </div>

                    <div class=" mrgb10">
                        <div class="order-vue__row">
                            <? if (!empty($dynprops_simple)): # краткий вывод динсвойств patr 4 "Требования" ?>
                                <? foreach ($dynprops_simple as $dp_item): ?>
                                    <? if ($dp_item['group_id'] == Orders::DP_ORDER_VIEW_PART_FOURTH ): ?>
                                        <? if($dp_item['value'] !== ''):?>
                                            <div class="order-vue__row-auto">
                                                <div class="order-vue__box h100p">
                                                    <div class="bold"><?= $dp_item['title'] ?></div>
                                                    <ul class="list-dp">
                                                        <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                        <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                                            <? foreach ($dp_item['multi'] as $val): ?>
                                                                <? if ($dp_item['value'] == $val['value']): ?>
                                                                    <li><?= $val['name'] ?></li>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? else: ?>
                                                            <? foreach ($dp_item['value'] as $dp_val): ?>
                                                                <? foreach ($dp_item['multi'] as $val): ?>
                                                                    <? if ($dp_val == $val['value']): ?>
                                                                        <li><?= $val['name'] ?></li>
                                                                    <? endif; ?>
                                                                <? endforeach; ?>
                                                            <? endforeach; ?>
                                                        <? endif; ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        <? endif;?>
                                    <? endif; ?>
                                <? endforeach; ?>
                            <? endif; ?>
                            <? if (!empty($dynprops_simple)): # краткий вывод динсвойств patr 5 "Навыки" ?>
                                <? foreach ($dynprops_simple as $dp_item): ?>
                                    <? if ($dp_item['group_id'] == Orders::DP_ORDER_VIEW_PART_FIFTH ): ?>
                                        <? if($dp_item['value'] !== ''):?>
                                            <div class="order-vue__row-auto">
                                                <div class="order-vue__box h100p">
                                                    <div class="bold"><?= $dp_item['title'] ?></div>
                                                    <ul class="list-dp">
                                                        <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                        <? if (count($dp_item['value']) == 1): $dp_item['value'] = reset($dp_item['value']); ?>
                                                            <? foreach ($dp_item['multi'] as $val): ?>
                                                                <? if ($dp_item['value'] == $val['value']): ?>
                                                                    <li><?= $val['name'] ?></li>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? else: ?>
                                                            <? foreach ($dp_item['value'] as $dp_val): ?>
                                                                <? foreach ($dp_item['multi'] as $val): ?>
                                                                    <? if ($dp_val == $val['value']): ?>
                                                                        <li><?= $val['name'] ?></li>
                                                                    <? endif; ?>
                                                                <? endforeach; ?>
                                                            <? endforeach; ?>
                                                        <? endif; ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        <? endif;?>
                                    <? endif; ?>
                                <? endforeach; ?>
                            <? endif; ?>
                        </div>
                    </div>

                    <? /**if( ! empty($dynprops)):  не выводим дин свойства в стандартном виде?>
                        <div class=" mrgb10"><?= $dynprops ?></div>
                    <? endif; **/?>

                </div>

                <div class="svc_carousel_block_orders">
                    <?= Users::i()->svc_carousel_block_orders(); ?>
                </div>

                <? if($visibility == Orders::VISIBILITY_ALL):
                    $aConfig = $this->configLoad();
                    if( ! empty($aConfig['share_code'])): ?>
                        <?= $aConfig['share_code'] ?>
                    <?  endif;
                endif; ?>

                <a name="offers"></a>

                <?= $offers_list ?>

            </div>

            <div class="col-md-3 visible-lg visible-md">

                <? if($bOwner) echo $this->orderStatusBlock($aData); ?>

                <div class="mrgb10">
                    <?= $offerAddForm ?>
                </div>

                <?# Блок текущего исполнителя: Вас выбрали исполнителем / Вы приняли предложение  ?>
                <?= (isset($currentUserPerformerState) && ! empty($currentUserPerformerState)) ? $currentUserPerformerState : '' ?>
                <?= $this->getOrderNote($aData['id']);?>

                <? if($bOwner):
                    $openClose = in_array($status, array(Orders::STATUS_OPENED, Orders::STATUS_CLOSED));
                    if($openClose && $ordersOpinions && ! empty($performer_offer['status']) && $performer_offer['status'] == Orders::OFFER_STATUS_PERFORMER_AGREE){
                        $openClose = false;
                    }
                    ?>
                    <? if(bff::servicesEnabled() && $visibility == Orders::VISIBILITY_ALL && $status == Orders::STATUS_OPENED): ?>
                        <div class="order__text text-center mrgb10">
                            <?= _t('','Хотите, чтобы ваше объявление увидело больше бебиситтеров или нянь?')?>
                            <? // todo clazion вместо 'бебиситтеров или нянь' подставлять нужную специализацию ?>
                        </div>
                        <a href="<?= Orders::url('promote', array('id' => $id))?>"" class="btn btn-primary btn-block">
                            <?= _t('orders', 'Рекламировать'); ?>
                        </a>
                    <? endif; ?>

                    <? if($performer_id): ?>
                    <? if($bOwner && $ordersOpinions && ! empty($performer_offer['status']) && $performer_offer['status'] == Orders::OFFER_STATUS_PERFORMER_START):
                        if( ! empty($performer_offer['workflow'])){ $aData['workflow'] = func::unserialize($performer_offer['workflow']); }
                        $aData['offerID'] = $performer_offer['id']; ?>
                    <? else: ?>
                        <div class="text-center">
                            <img src="<?= bff::url('/img/confetti.svg');?>" alt="" width="35">
                            <div class="order__text text-center mrgb10 mrgt10">
                                <?= _t('','Няня согласилась работать в Вашей семье. Поздравляем!')?>
                            </div>
                        </div>
                    <? endif; ?>
                <? endif; ?>

                    <div class="text-center mrgt10 mrgb10">
                        <div class="relative">
                            <a href="javascript:volid(0);" data-toggle="dropdown" class="drop-link drop-link_fz13 flex flex_center flex_jcc flex_0-0-a">
                                <?= _t('','еще')?>
                                <i class="mrgl5 icon-arrow-point-to-down"></i>
                            </a>
                            <ul class="dropdown-menu m0-a text-left w100p" >
                                <li>
                                    <? if($fairplayEnabled && ! empty($workfow['url'])): ?>
                                        <a href="<?= $workfow['url'] ?>">
                                            <i class="fa fa-rocket  c-link-icon"></i>
                                            <?= _t('fp', 'Ход работ'); ?>
                                        </a>
                                    <? endif; ?>
                                </li>
                                <li>
                                    <a href="<?= Orders::url('edit', array('id' => $id))?>">
                                        <?= _t('form', 'Редактировать'); ?>
                                    </a>
                                </li>
                                <li>
                                    <? if($status == Orders::STATUS_ONACTIVATION):?>
                                        <a href="<?= Orders::url('activation', array('id' => $id))?>">
                                            <?= _t('form', 'Активировать'); ?>
                                        </a>
                                    <? endif;?>
                                </li>
                                <? if($openClose): ?>
                                    <li>
                                        <a href="#" class="<?= $status != Orders::STATUS_OPENED ? 'hidden' : '' ?> j-hide">
                                            <?= _t('orders', 'Снять с публикации'); ?>
                                        </a>
                                        <a href="#" class="<?= $status != Orders::STATUS_CLOSED ? 'hidden' : '' ?> j-show">
                                            <?= _t('orders', 'Публиковать заново'); ?>
                                        </a>
                                    </li>
                                <? endif; ?>
                                <? if($openClose): ?>
                                    <? if($bDeleteAllow): ?>
                                        <li>
                                            <a href="#" class=" j-delete">
                                                <?= _t('form', 'Удалить'); ?>
                                            </a>
                                        </li>
                                    <? endif; ?>
                                <? endif; ?>
                            </ul>
                        </div>
                    </div>

                    <? if( ! empty($invites)): ?>
                    <h5 class="mrgt0 mrgb0 pd0"><?= _t('orders', 'Вы предложили заказ:'); ?></h5>
                    <ul class="media-list mrgb15" id="j-order-invites-list">

                        <? $cnt = 0; $limit = 3; foreach($invites as $v): $cnt++; ?>
                            <li class="mrgt10 <?= $cnt > $limit ? ' hidden' : '' ?>">
                                <div class="user-box mrgb10 mrgt5">
                                    <div class="user-box__avatar user-box__avatar_sq-big">
                                        <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $v['login'])); ?>">
                                            <?= tpl::userAvatar($v) ?>
                                        </a>
                                    </div>
                                    <div class="user-box__text mrgl10">
                                        <div class="user-box__text">
                                            <? if( ! empty($v['main_spec_title'])): ?>
                                                <span class="user-box__text-s"><?= $v['main_spec_title'] ?></span>
                                            <? endif; ?>
                                            <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $v['login'])); ?>">
                                                <?= $v['name'] ?>
                                                <? if(!empty($v['surname'])): ?>
                                                    <?= mb_strimwidth($v['surname'], 0, 2, "."); ?>
                                                <? endif; ?>
                                                <? if($v['pro']):?>
                                                    <span class="pro">pro</span>
                                                <? endif; ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <? endforeach; ?>
                    </ul>
                    <? if($cnt > $limit): ?>
                        <div class="text-center mrgb30">
                            <a href="#" class="ajax-link" id="j-more-invites"><span><?= _t('', 'Показать еще'); ?></span></a>
                        </div>
                    <? endif; endif; ?>

                <? else: if( ! empty($workfow['url'])): ?>
                    <div class="l-inside p-profileOrder-controls-mobile">
                        <a href="<?= $workfow['url'] ?>"><i class="fa fa-rocket c-link-icon"></i></a>
                    </div>
                    <div class="l-inside p-profileOrder-controls o-controls">
                        <a href="<?= $workfow['url'] ?>"><i class="fa fa-rocket  c-link-icon"></i><?= _t('fp', 'Ход работ'); ?></a>
                    </div>
                <? endif; endif; ?>


                <? # Баннер: Заказы: просмотр ?>
                <div class="mrgt10">
                    <?= Banners::view('orders_view', array('pos'=>'right', 'spec'=>(!empty($specs[0]['spec_id']) ? $specs[0]['spec_id'] : 0))) ?>
                </div>
            </div>

            </div>
        </section>

    </div>
<script type="text/javascript">
<? js::start() ?>
jOrdersOrderView.init(<?= func::php2js(array(
    'lang' => array(
        'order_delete' => _t('orders', 'Удалить заказ'),
    ),
    'id' => $id,
    'lat' => $addr_lat,
    'lng' => $addr_lng,
    'balloon' => ! empty($addr_addr) ? '<div><i class="fa fa-map-marker"></i> '.( ! empty($aFirstRegions['title']) ? $aFirstRegions['title'].', ' : '') .$addr_addr.'</div>' : false,
)) ?>);
<? if(Orders::imagesLimit() && $aData['imgcnt']):
    tpl::includeCSS(array('owl.carousel'), true);
    tpl::includeJS('owl.carousel.min', false);
    tpl::includeJS('fancybox2', true);
?>
$(function(){
    var $carousel = $('#j-order-view-carousel');
    if($carousel.length){
        $carousel.owlCarousel({
            items : 5, //10 items above 1000px browser width
            itemsDesktop : [1000,4], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,3], // betweem 900px and 601px
            itemsTablet: [600,1], //2 items between 600 and 0
            itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
            navigation : true,
            navigationText : ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            pagination : true,
            autoPlay : false
        });
    }
    $('.fancyzoom').fancybox({
        openEffect	: 'none',
        closeEffect	: 'none',
        nextEffect  : 'fade',
        prevEffect : 'fade',
        fitToView: false,
        helpers: {
            overlay: {locked: false}
        }
    });
});
<? endif; ?>

var ratingTotalAverage = new Rating();
    ratingTotalAverage.initViewTotalAverage(
        <?= Rating::JS_PARAMETERS_VIEW(Rating::JS_CLASS_RATING_STARS_ITEM_READONLY) ?>
    );
<? js::stop() ?>
</script>