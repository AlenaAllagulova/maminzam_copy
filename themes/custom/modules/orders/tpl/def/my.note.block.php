<?php

?>
<div class="note <?= ! empty($classes) ? ' '.$classes.' ' : '' ?> j-order-note">

    <p class="note__title">
        <?= _t('users', 'Ваша заметка:'); ?>
        <span class="note__tip show-tooltip"
              data-placement="top"
              title="<?= _t('users', 'Ваша заметка будет видна только вам.'); ?>"
              data-original-title="<?= _t('users', 'Ваша заметка будет видна только вам.'); ?>"
        >
            ?
        </span>
    </p>

    <div class="j-notes-show" <?= ! $id ? 'style="display:none;"' : '' ?> data-id="<?= $id ?>">
        <div class="j-note-text">
            <?= nl2br($note)?>
        </div>
        <div class="">
            <a class=" note__btn-edit j-note-edit" href="javascript:void(0)" >
                <?= _t('form', 'Редактировать'); ?>
            </a>
            <a class="note__btn-delete j-note-delete" href="javascript:void(0)">

                <?= _t('form', 'Удалить'); ?>
            </a>
        </div>
    </div>

    <div class="mrgt5 j-notes-form-block <?= $id ? 'hidden' : '' ?>">
        <form method="post" action="">
            <input type="hidden" name="id" value="<?= $id ?>" />
            <input type="hidden" name="order_id" value="<?= $order_id ?>" />
            <div class="form-group">
                <input name="note" class="form-control form-control_sm j-notes-input" value="<?= $note ? $note : ''; ?>"
                       placeholder="<?= _t('users', 'Введите текст заметки'); ?>">
            </div>
        </form>
    </div>
</div>
