<?php
$isPublic = ! empty($public);
$isPrivate = ! empty($private);
?>
<div class="modal fade" id="j-modal-offer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="j-listing ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="modal__title text-center">
                        <?= _t('','Выберите объявление, которое хотите предложить')?>
                    </div>
                </div>
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane " id="modal-offer-tabs-new">
                        <div class="modal-body">
                            <?= $form ?>
                        </div>
                    </div>

                    <? if($isPublic): ?>
                    <div role="tabpanel" class="tab-pane active" id="modal-offer-tabs-public">
                        <div class="modal-body pd20">
                            <?= $public ?>
                        </div>
                    </div>
                    <? else:?>
                        <div class="modal-body">
                            <span>
                                <?=_t('', 'У вас еще нет ни одного заказа, который можно предложить, 
                                            хотите <a href="[link]" target="_blank">создать новый</a>?',
                                        ['link' => Orders::url('add')])?>
                            </span>
                        </div>
                    <? endif; ?>
                </div>
            </div>
            <div class="hidden j-invite">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <a href="#" class="link-def link-def_tdn flex flex_center link-def_fz13 j-another">
                        <i class="icon-arrow-point-to-left mrgr5"></i>
                        <span><?= _t('orders', 'Выбрать другой заказ'); ?></span>
                    </a>
                <div class="modal-body">
                    <div class="bold">
                        <?= _t('','Предложить заказ') ?>
                    </div>
                    <ul class="o-orderSelect mrgb10 mrgt10">
                        <li class="active j-order">
                        </li>
                    </ul>
                </div>
                <div class="pdl10 pdr10 pdb20 text-center">
                    <button type="button" class="btn btn-primary  j-submit"><?= _t('orders', 'Предложить'); ?></button>
                    <a href="javascript:volid(0);" type="button" class="mrgl10 link-bold link-bold_fz15" data-dismiss="modal"><?= _t('form', 'Отмена'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>