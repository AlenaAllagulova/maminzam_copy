<?php
tpl::includeJS('orders.respondent.list', false, 4);
?>
<? if (isset($performerState['status'])): ?>
    <? if($performerState['status'] == Orders::OFFER_STATUS_PERFORMER_START):?>
        <div id="j-order-view-performer-start" style="padding: 10px">
            <?= ! empty($performerStartBlock) ? $performerStartBlock : '' ?>
        </div>
    <? else: ?>
        <div class="text-center mrgb10"  style="padding: 20px">
            <div class="mrgb10">
                <img src="<?= bff::url($statusList[$performerState['status']]['img'])?>" alt="" width="35">
            </div>
            <span class="order__text">
                <?//= $statusList[$performerState['status']]['t']?>
            </span>
        </div>
    <?endif;?>
<? endif; ?>

<script type="text/javascript">
    <? js::start() ?>
    jOrdersRespondentList.init(<?= func::php2js(array(
        'lang'  => array(
            'decline_short' => _t('orders', 'Укажите причину отказа'),
        ),
        'types' => [],
        'ordersOpinions' => Orders::ordersOpinions(),
        'invitesEnabled' => Orders::invitesEnabled(),
        'ajax'  => false,
    )) ?>);
    <? js::stop() ?>
</script>
