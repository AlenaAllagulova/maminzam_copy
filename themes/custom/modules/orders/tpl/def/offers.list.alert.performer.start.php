<?php
$fairplayEnabled = bff::fairplayEnabled();
$orderView = ! empty($performer);
$offerID = ! empty($offerID) ? $offerID : 0;
?>
<div class=" <?= $orderView ? 'j-performer-start-cancel-'.$offerID : 'j-performer' ?>">
    <? if($orderView): ?>
        <span class="bold"><?= _t('orders', 'Вы выбрали:'); ?></span>
        <div class="user-box mrgb10 mrgt5">
            <div class="user-box__avatar user-box__avatar_sq">
                <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $performer['login'])); ?>">
                    <img src="<?= UsersAvatar::url($performer_id, $performer['avatar'], UsersAvatar::szNormal, $performer['sex']) ?>" alt="">
                </a>
            </div>
            <div class="user-box__text mrgl10">
                <div class="user-box__text">
                    <span class="user-box__text-s">
                    <?if(!empty($performer['specs'])):?>
                        <? foreach ($performer['specs'] as $spec):?>
                            <?if($spec['main']):?>
                                <?= $spec['spec_title']?>
                            <? endif;?>
                        <? endforeach;?>
                    <?endif;?>
                    </span>
                    <a class="user-box__link-sm" href="<?= Users::url('profile', array('login' => $performer['login'])); ?>">
                        <?= $performer['name'] ?>
                        <? if(!empty($performer['surname'])): ?>
                            <?= mb_strimwidth($performer['surname'], 0, 2, "."); ?>
                        <? endif; ?>
                    </a>
                </div>
            </div>
        </div>
    <? else: ?>
        <strong><?= _t('orders', 'Выбран исполнителем:'); ?></strong>
    <? endif; ?>
    <? if($fairplayEnabled && ! empty($workflow)): ?>
        <div class="l-project-terms">
            <span class="l-project-terms-budget"><?= _t('fp', 'Бюджет:'); ?> <strong><?= tpl::formatPrice($workflow['price']) ?> <?= Site::currencyDefault('title_short'); ?></strong></span>,
            <span class="l-project-terms-time"><?= _t('fp', 'Срок:'); ?> <?= Fairplay::tplTerm($workflow['term']) ?></span>
        </div>
        <div class="l-project-deal">
            <? if($workflow['fairplay'] == Orders::FAIRPLAY_USE): ?><?= _t('fp', 'Работа через <a [link]>Безопасную Сделку</a>', array(
                'link' => 'href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-shield c-link-icon c-safe-color"></i')); ?><?
            else: ?><?= _t('fp', 'Работа через прямую оплату</a>'); ?><? endif; ?>
        </div>
    <? endif; ?>
    <button type="button" class="link-bold <?= $orderView ? 'j-performer-start-cancel-general' : 'j-performer-start-cancel' ?>" data-id="<?= $offerID ?>"><?= _t('orders', 'Отменить предложение'); ?></button>
    <? if($fairplayEnabled): ?>
        <?= _t('', 'или'); ?> <a class="ajax-link j-workflow-change" href="#" data-id="<?= $offerID ?>"><span><?= _t('fp', 'изменить срок, сумму или тип оплаты'); ?></span></a>
    <? endif; ?>
</div>