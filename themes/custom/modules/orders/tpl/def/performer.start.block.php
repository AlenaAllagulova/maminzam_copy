<?php
$isOpenedOrderOwnerContacts = Users::isOpenedContacts($user_id);
?>

<div class="text-center order__text mrgb10">
    <?= ( isset($performerState) && isset($statusList[$performerState['status']]['t'])) ? $statusList[$performerState['status']]['t'] : _t('', 'Родитель хочет взять Вас на работу.') ?>
</div>
<button class="btn btn-block btn-primary j-agree mrgb10" data-id="<?= $offer_id?>">
    <?= _t('orders', 'Принять предложение'); ?>
</button>
<a href="<?= $isOpenedOrderOwnerContacts? InternalMail::url('my.chat', ['user'=> $login]): 'javascript:void(0)' ?>"
    <?= !$isOpenedOrderOwnerContacts ?
        'onclick="app.alert.error('._t('','\'Переписка доступна после покупки контактов\'').')" '
        : ''?>
   class="btn btn-block mrgb10 btn-primary btn-primary_empty">
    <?= _t('orders', 'Продолжить общение'); ?>
</a>
<div class="text-center">
    <a href="#" data-id="<?= $offer_id ?>" class="bold link-bold link-bold_fz15 j-decline">
        <?= _t('orders', 'Отказать'); ?>
    </a>
</div>
