<?php
    /**
     * @var $this Orders
     */
    tpl::includeJS(array('autocomplete'), true);

    $isOneSpec = Orders::searchOneSpec();
    $isSpecCatsOn = Specializations::catsOn();

    $specClassLiCat = function($cat, $aSelected){
        if( ! empty($aSelected[ $cat['id'] ])){
            if(count($cat['specs']) == count($aSelected[ $cat['id'] ])){
                return ' class="opened checked" ';
            }
            return ' class="opened subchecked" ';
        }
        return '';
    };
    $specClassLiCatI = function($cat, $aSelected){
        if( ! empty($aSelected[ $cat['id'] ])){
            if(count($cat['specs']) == count($aSelected[ $cat['id'] ])){
                return 'fa-check-circle';
            }
            return 'fa-circle';
        }
        return 'fa-circle-o';
    };
    $catsClassLiCat = function($cat, $aSelected){
        if( ! empty($aSelected[ $cat['id'] ])){
            if(is_array($aSelected[ $cat['id'] ])){
                if(count($cat['sub']) == count($aSelected[ $cat['id'] ])){
                    return ' class="opened checked" ';
                }
                return ' class="opened subchecked" ';
            } else {
                return ' class="checked" ';
            }
        }
        return '';
    };
    $catsClassLiCatI = function($cat, $aSelected){
        if( ! empty($aSelected[ $cat['id'] ])){
            if(is_array($aSelected[ $cat['id'] ])){
                if(count($cat['sub']) == count($aSelected[ $cat['id'] ])){
                    return 'fa-check-circle';
                }
                return 'fa-circle';
            } else {
                return 'fa-check-circle';
            }
        }
        return 'fa-circle-o';
    };
?>

<div class="h6-text visible-md visible-lg">
    <?= _t('','Фильтр')?>
</div>

<div class="mrgt25">
    <div class="">
        <a href="javascript:volid(0);" type="button" class="btn-filter-md flex mrgb10" data-toggle="collapse" data-target="#j-orders-search-form-block">
        <span class="">
            <?= _t('', 'Фильтр'); ?>
            (<?= _t('orders', 'Найдено'); ?>
            <span class="j-orders-count">
                <?= $count ?>
            </span>
            )
        </span>
            <i class="icon-arrow-point-to-down mrgl10"></i>
        </a>
    </div>

    <div class="form-sidebar collapse" id="j-orders-search-form-block">
        <form action="" method="get">
            <input type="hidden" name="orders" value="<?= $f['orders'] ?>" />
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
            <input type="hidden" name="m" value="<?= $f['m'] ?>" />
            <input type="hidden" name="4me" value="<?= $f['4me'] ?>" />

            <? if(bff::servicesEnabled()): ?>
                <input type="hidden" name="pro" value="<?= $f['pro'] ?>" />
                <ul class="list-def">
                    <li class="filter-checkbox <?= $f['pro'] ? 'checked ' : '' ?> j-checkbox-pro">
                        <i class="mrgr5 fa <?= $f['pro'] ? 'ico-square-checked' : 'ico-square' ?>"></i>
                        <?= _t('orders', 'Только для'); ?>
                        <div class="mrgl5">
                            <span class="pro">pro</span>
                        </div>
                    </li>
                </ul>
            <? endif; ?>

            <? if($f['t'] == Orders::TYPE_SERVICE): ?>
                <a href="#j-left-categories" data-toggle="collapse" data-parent="#accordion" class="form-sidebar__title active">
                    <?= _t('', 'Специализации'); ?>
                    <i class=" icon-arrow-point-to-down"></i>
                </a>
                <div class="collapse in j-collapse" id="j-left-categories">
                    <ul class="l-left-categories l-inside <?= $isSpecCatsOn ? '' : '' ?>">
                        <?  if($isSpecCatsOn):
                            foreach($specs as &$v): ?>
                                <li<?= $specClassLiCat($v, $fspecs) ?> class="" data-id="<?= $v['id'] ?>">

                                    <ul class="list-def <?= empty($fspecs[ $v['id'] ]) ? '' : '' ?>">
                                        <? foreach($v['specs'] as &$vv): ?>
                                            <li class="filter-checkbox mrgb5 <?= ! empty($fspecs[ $v['id'] ][ $vv['id'] ]) ? ' checked' : '' ?>">
                                                <span class="filter-checkbox__text flex flex_center j-spec" data-id="<?= $vv['id'] ?>" data-cat="<?= $v['id'] ?>"
                                                      <? if($isOneSpec): ?>
                                                          data-url="<?= $vv['url'] ?>"
                                                      <? else: ?>
                                                          data-keyword="<?= $vv['keyword'] ?>"
                                                      <? endif; ?>
                                                >
                                                    <i class="fa mrgr5 <?= ! empty($fspecs[ $v['id'] ][ $vv['id'] ]) ? 'ico-square-checked' : 'ico-square' ?>"></i>
                                                    <?= $vv['title'] ?>
                                                </span>
                                            </li>
                                        <? endforeach; unset($vv); ?>
                                    </ul>
                                </li>
                            <? endforeach; unset($v);
                        else:
                            if($isOneSpec):
                                foreach($specs as &$v): ?>
                                    <li<?= ! empty($fspecs[ $v['id'] ]) ? ' class="active"' : '' ?>>
                                        <a href="<?= $v['url']; ?>">
                                            <i class="fa fa-caret-right"></i>
                                            <?= $v['title'] ?>
                                        </a>
                                    </li>
                                <?  endforeach; unset($v);
                            else:
                                foreach($specs as &$v): ?>
                                    <li class="filter-checkbox mrgb5 <?= ! empty($fspecs[ $v['id'] ]) ? 'checked' : '' ?>" >
                                        <span class="filter-checkbox__text flex flex_center j-spec-nocat" data-id="<?= $v['id'] ?>" data-keyword="<?= $v['keyword'] ?>">
                                            <i class="fa mrgr5 <?= ! empty($fspecs[ $v['id'] ]) ? 'ico-square-checked' : 'ico-square' ?>"></i>
                                            <?= $v['title'] ?>
                                        </span>
                                    </li>
                                <?  endforeach; unset($v);
                            endif;
                        endif; ?>
                    </ul>
                </div><!-- /#left-categories -->

                <? if(Orders::useProducts()):
                    $aServiceTypes = array(
                        Orders::SERVICE_TYPE_NONE    => array('id' => Orders::SERVICE_TYPE_NONE,    't' => _t('orders','Все')),
                        Orders::SERVICE_TYPE_ONE     => array('id' => Orders::SERVICE_TYPE_ONE,     't' => _t('orders','Разовые заказы')),
                        Orders::SERVICE_TYPE_JOB     => array('id' => Orders::SERVICE_TYPE_JOB,     't' => _t('orders','Постоянная работа')),
                        Orders::SERVICE_TYPE_CONTEST => array('id' => Orders::SERVICE_TYPE_CONTEST, 't' => _t('orders','Конкурсы')),
                    );
                    $aServiceType = isset($aServiceTypes[ $f['st'] ]) ? $aServiceTypes[ $f['st'] ] : reset($aServiceTypes);
                    $bOpenSt = $f['st'] != Orders::SERVICE_TYPE_NONE;
                    ?>
                    <a href="#j-left-service-type" data-toggle="collapse" data-parent="#accordion" class="form-sidebar__title <?= $bOpenSt ? ' active' : '' ?>">
                        <?= _t('orders', 'Тип заказа'); ?>
                        <i class="icon-arrow-point-to-down"></i>
                    </a>
                    <div class="collapse in <?= $bOpenSt ? 'in' : '' ?> j-collapse" id="j-left-service-type">
                        <div class="l-inside">
                            <div class="row">
                                <div class="col-sm-12">
                                    <? foreach($aServiceTypes as $v): ?>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="j-f-st" <?= $f['st'] == $v['id'] ? 'checked="checked" ' : '' ?>value="<?= $v['id'] ?>" autocomplete="off" name="st" /><?= $v['t'] ?>
                                            </label>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <? else: ?>
                    <input type="hidden" name="st" value="<?= $f['st'] ?>" />
                <? endif; ?>
            <? endif; ?>
            <? if($f['t'] == Orders::TYPE_PRODUCT): ?>
                <input type="hidden" name="t" value="<?= $f['t'] ?>" />
                <a href="#j-left-categories" data-toggle="collapse" data-parent="#accordion" class="form-sidebar__title active">
                    <?= _t('orders', 'Категории'); ?>
                    <i class="icon-arrow-point-to-down"></i>
                </a>
                <div class="collapse in j-collapse" id="j-left-categories">
                    <ul class="l-left-categories l-inside <?= $isSpecCatsOn ? 'visible-md visible-lg' : '' ?>">
                        <? foreach($cats as &$v):
                            $bSub = ! empty($v['sub']);
                            if( ! $bSub){  $sData = ($isOneSpec ? 'data-url="'.$v['url'].'"' : 'data-keyword="'.$v['keyword'].'"'); } ?>
                            <li<?= $catsClassLiCat($v, $fcats) ?> data-id="<?= $v['id'] ?>"><span<?= ! $bSub ? ' class="j-cat-parent" '.$sData : '' ?> data-id="<?= $v['id'] ?>"><i class="fa <?= $catsClassLiCatI($v, $fcats) ?> <?= $bSub ? 'j-cat-check' : '' ?>"></i> <span class="<?= $bSub ? 'j-cat-title' : '' ?>"><?= $v['title'] ?></span></span>
                                <? if($bSub): ?>
                                    <ul class="<?= empty($fcats[ $v['id'] ]) ? 'hidden' : '' ?>">
                                        <? foreach($v['sub'] as &$vv): $sData = ($isOneSpec ? 'data-url="'.$vv['url'].'"' : 'data-keyword="'.$vv['keyword'].'"'); ?>
                                            <li<?= ! empty($fcats[ $v['id'] ][ $vv['id'] ]) ? ' class="checked"' : '' ?>>
                                                <span class="j-cat" data-id="<?= $vv['id'] ?>" data-cat="<?= $v['id'] ?>" <?= $sData ?>>
                                                    <i class="fa <?= ! empty($fcats[ $v['id'] ][ $vv['id'] ]) ? 'ico-square-checked' : 'ico-square' ?>"></i>
                                                    <?= $vv['title'] ?>
                                                </span>
                                            </li>
                                        <? endforeach; unset($vv); ?>
                                    </ul>
                                <? endif; ?>
                            </li>
                        <? endforeach; unset($v); ?>
                    </ul>

                </div>
            <? endif; ?>

            <div id="j-orders-search-form-dp">
                <?= ! empty($dp) ? $dp : '' ?>
            </div>

            <? if( ! Geo::filterEnabled()): $bCountry = Geo::countrySelect(); $bOpenRegion = $f['ct'] || ($bCountry && $f['c']); ?>
                <a href="#j-left-region" data-toggle="collapse" data-parent="#accordion" class="form-sidebar__title <?= $bOpenRegion ? ' active' : '' ?>">
                    <?= _t('orders', 'Регион'); ?>
                    <i class="icon-arrow-point-to-down"></i>
                </a>
                <div class="collapse <?= $bOpenRegion ? 'in' : '' ?> j-collapse" id="j-left-region">
                    <div class="l-inside">
                        <div class="form">
                            <? if($bCountry): ?>
                                <div class="form-group">
                                    <select name="c" class="form-control" id="j-left-region-country" autocomplete="off"><?= HTML::selectOptions(Geo::countryList(), $f['c'], _t('', 'Все'), 'id', 'title') ?></select>
                                </div>
                            <? endif; ?>
                            <div class="relative">
                                <input type="hidden" name="r" value="<?= $f['r'] ?>" id="j-region-pid-value" />
                                <input type="hidden" name="ct" value="<?= $f['ct'] ?>" id="j-region-city-value" />
                                <input type="text" class="form-control input-sm<?= $bCountry && empty($f['c']) ? ' hidden' : '' ?>" id="j-region-city-select" value="<?= $region_title ?>" placeholder="<?= _t('orders', 'Или введите название региона'); ?>" />
                            </div>
                        </div>
                    </div>
                </div>
            <? endif; ?>

            <div id="j-order-price-block">
                <?= $this->viewPHP($aData, 'search.form.price.ex'); ?>
            </div>


            <div class="l-inside text-center">

                <a href="#" class="ajax-link j-clear-filter btn btn-clear-filter btn-block">
                    <?= _t('', 'Сбросить фильтры'); ?>
                </a>
            </div>

        </form>

    </div>

</div>
