<?php

class Theme_Custom_OrdersBase extends Theme_Custom_OrdersBase_Base
{

    const STATUS_ONACTIVATION = 10; # заказ на активации


    /**
     * Добавлен шаблон письма о том, что можно публиковать отзыв в заказе
     * @return array
     */
    public function sendmailTemplates()
    {
        $aTemplates = array(
            'orders_order_activate' => array(
                'title'       => 'Заказы: Активация заказа',
                'description' => 'Уведомление, отправляемое <u>незарегистрированному пользователю</u> после добавления заказа',
                'vars'        => array(
                    '{name}'          => 'Имя пользователя',
                    '{email}'         => 'Email',
                    '{activate_link}' => 'Ссылка активации заказа',
                ),
                'impl'        => true,
                'priority'    => 20,
                'enotify'     => 0, # всегда
                'group'       => 'orders',
            ),
            'orders_order_on_activation' => array(
                'title'       => 'Заказы: Активация вакансии',
                'description' => 'Уведомление, отправляемое <u>активированному пользователю</u> при необходимости активации заказа',
                'vars'        => array(
                    '{name}'          => 'Имя пользователя',
                    '{email}'         => 'Email',
                    '{order_title}'   => 'Заголовок заказа',
                    '{order_url}'     => 'Ссылка на заказа',
                    '{order_activation_link}' => 'Ссылка на активацию заказа',
                ),
                'impl'        => true,
                'priority'    => 21,
                'enotify'     => 0, # всегда
                'group'       => 'orders',
            ),
            'order_offer_change_status' => array(
                'title'       => 'Заказы: Статус заявки',
                'description' => 'Уведомление, отправляемое <u>исполнителю</u> при изменении статуса заявки исполнителя',
                'vars'        => array(
                    '{name}'      => 'Имя пользователя',
                    '{email}'     => 'Email',
                    '{title}'     => 'Заголовок заказа',
                    '{order_url}' => 'Ссылка для просмотра заказа',
                    '{status}'    => 'Статус:<br /> - Вас выбрали кандидатом.<br />- Вас выбрали исполнителем.<br />- Вы получили отказ.',
                ),
                'impl'        => true,
                'priority'    => 21,
                'enotify'     => 0, # всегда
                'group'       => 'orders',
            ),
            'order_offer_add' => array(
                'title'       => 'Заказы: Добавление заявки',
                'description' => 'Уведомление, отправляемое <u>заказчику</u> при добавлении заявки к заказу',
                'vars'        => array(
                    '{name}'          => 'Имя заказчика',
                    '{worker_name}'   => 'Имя исполнителя',
                    '{worker_login}'  => 'Логин исполнителя',
                    '{worker_url}'    => 'Ссылка на профиль исполнителя',
                    '{order_title}'   => 'Заголовок заказа',
                    '{order_url}'     => 'Ссылка для просмотра заказа',
                    '{offer_descr}'   => 'Текст заявки (150 символов)',
                ),
                'impl'        => true,
                'priority'    => 22,
                'enotify'     => 0, # всегда
                'group'       => 'orders',
            ),
            'orders_offer_chat' => array(
                'title'       => 'Заявки: Переписка',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при добавлении ответа в переписке к заявке',
                'vars'        => array(
                    '{name}'          => 'Имя адресата (заказчика или исполнителя)',
                    '{author_name}'   => 'Имя автора ответа',
                    '{order_id}'      => 'ID заказа',
                    '{order_title}'   => 'Заголовок заказа',
                    '{order_url}'     => 'Ссылка для просмотра заказа',
                    '{message}'       => 'Текст ответа (150 символов)',
                ),
                'impl'        => true,
                'priority'    => 31,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'orders',
            ),
            'orders_order_new_now' => array(
                'title'       => 'Заказы: Новый заказ (мгновенная отправка)',
                'description' => 'Уведомление, отправляемое <u>исполнителю</u> при создании нового заказа в одну из его специализаций',
                'vars'        => array(
                    '{fio}'           => 'ФИО исполнителя',
                    '{name}'          => 'Имя исполнителя',
                    '{order_id}'      => 'ID заказа',
                    '{order_title}'   => 'Заголовок заказа',
                    '{order_url}'     => 'Ссылка для просмотра заказа',
                    '{order_descr}'   => 'Текст заказа (150 символов)',
                    '{spec_title}'    => 'Название специализации',
                ),
                'impl'        => true,
                'priority'    => 23,
                'enotify'     => Users::ENOTIFY_ORDERS_NEW,
                'group'       => 'orders',
            ),
            'orders_order_new_group' => array(
                'title'       => 'Заказы: Новый заказ (групповая отправка)',
                'description' => 'Уведомление, отправляемое <u>исполнителю</u> при создании нового заказа в одну из его специализаций',
                'vars'        => array(
                    '{fio}'           => 'ФИО исполнителя',
                    '{name}'          => 'Имя исполнителя',
                    '{orders_block}'  => 'Блок заказов (заголовок, ссылка для просмотра)',
                ),
                'impl'        => true,
                'priority'    => 24,
                'enotify'     => Users::ENOTIFY_ORDERS_NEW,
                'group'       => 'orders',
            ),
            'orders_order_blocked' => array(
                'title'       => 'Заказы: Заблокирован модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> (автору заказа) при блокировке заказа модератором',
                'vars'        => array(
                    '{fio}'             => 'ФИО заказчика',
                    '{name}'            => 'Имя заказчика',
                    '{order_id}'        => 'ID заказа',
                    '{order_title}'     => 'Заголовок заказа',
                    '{order_url}'       => 'Ссылка для просмотра заказа',
                    '{blocked_reason}'  => 'Причина блокировки',
                ),
                'impl'        => true,
                'priority'    => 25,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'orders',
            ),
            'orders_order_approved' => array(
                'title'       => 'Заказы: Одобрен модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> (автору заказа) при одобрении заказа модератором',
                'vars'        => array(
                    '{fio}'         => 'ФИО заказчика',
                    '{name}'        => 'Имя заказчика',
                    '{order_id}'    => 'ID заказа',
                    '{order_title}' => 'Заголовок заказа',
                    '{order_url}'   => 'Ссылка для просмотра заказа',
                ),
                'impl'        => true,
                'priority'    => 26,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'orders',
            ),
        );
        if (static::ordersOpinions()) {
            $aTemplates += array(
                'order_offer_performer_start' => array(
                    'title'       => 'Заявки: Предложение стать исполнителем',
                    'description' => 'Уведомление, отправляемое <u>исполнителю</u> с предложением стать исполнителем',
                    'vars'        => array(
                        '{fio}'          => 'ФИО исполнителя',
                        '{name}'         => 'Имя исполнителя',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_fio}'   => 'ФИО заказчика',
                        '{client_login}' => 'Логин заказчика',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                    ),
                    'impl'        => true,
                    'priority'    => 27,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
                'order_offer_performer_stop' => array(
                    'title'       => 'Заявки: Отмена предложения стать исполнителем',
                    'description' => 'Уведомление, отправляемое <u>исполнителю</u> в случае отмены заказчиком его предложения.',
                    'vars'        => array(
                        '{fio}'          => 'ФИО исполнителя',
                        '{name}'         => 'Имя исполнителя',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_fio}'   => 'ФИО заказчика',
                        '{client_login}' => 'Логин заказчика',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                    ),
                    'impl'        => true,
                    'priority'    => 28,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
                'order_offer_performer_agree' => array(
                    'title'       => 'Заявки: Подтверждение работы над заказом',
                    'description' => 'Уведомление, отправляемое <u>заказчику</u> при подтверждении исполнителем предложения.',
                    'vars'        => array(
                        '{fio}'          => 'ФИО заказчика',
                        '{name}'         => 'Имя заказчика',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_fio}'   => 'ФИО исполнителя',
                        '{worker_login}' => 'Логин исполнителя',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                    ),
                    'impl'        => true,
                    'priority'    => 29,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
                'order_offer_performer_decline' => array(
                    'title'       => 'Заявки: Отказ от работы над заказом',
                    'description' => 'Уведомление, отправляемое <u>заказчику</u> в случае отказа исполнителя от предложения.',
                    'vars'        => array(
                        '{fio}'          => 'ФИО заказчика',
                        '{name}'         => 'Имя заказчика',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{message}'      => 'Текст сообщения',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_fio}'   => 'ФИО исполнителя',
                        '{worker_login}' => 'Логин исполнителя',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                    ),
                    'impl'        => true,
                    'priority'    => 30,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
                # шаблон письма для исполнителя или заказчика о том, что можно публиковать отзыв в заказе
                'order_opinions_delay' => array(
                    'title'       => 'Отзывы: Возможность публиковать отзывы',
                    'description' => 'Уведомление, отправляемое <u>заказчику</u> и <u>исполнителю</u> о возможности опубликовать отзыв.',
                    'vars'        => array(
                        '{name}'         => 'Имя заказчика/исполнителя',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                    ),
                    'impl'        => true,
                    'priority'    => 35,
                    'enotify'     => Users::ENOTIFY_GENERAL, # всегда
                    'group'       => 'orders',
                ),
            );
        }
        if (static::invitesEnabled()) {
            $aTemplates += array(
                'order_invite_start' => array(
                    'title'       => 'Заказы: Предложение заказа исполнителю (Предложить заказ)',
                    'description' => 'Уведомление, отправляемое <u>исполнителю</u> при получении предложения заказа от заказчика',
                    'vars'        => array(
                        '{fio}'          => 'ФИО исполнителя',
                        '{name}'         => 'Имя исполнителя',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_fio}'   => 'ФИО заказчика',
                        '{client_login}' => 'Логин заказчика',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                        //'{message}'      => 'Текст сообщения (до 150 символов)',
                    ),
                    'impl'        => true,
                    'priority'    => 32,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
                'order_invite_agree' => array(
                    'title'       => 'Заказы: Согласие на предложенный заказ исполнителем (Предложить заказ)',
                    'description' => 'Уведомление, отправляемое <u>заказчику</u> в случае согласия исполнителя с заказом',
                    'vars'        => array(
                        '{fio}'          => 'ФИО заказчика',
                        '{name}'         => 'Имя заказчика',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_fio}'   => 'ФИО исполнителя',
                        '{worker_login}' => 'Логин исполнителя',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                        '{message}'      => 'Текст сообщения (до 150 символов)',
                    ),
                    'impl'        => true,
                    'priority'    => 33,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
                'order_invite_decline' => array(
                    'title'       => 'Заказы: Отказ от предложенного заказа исполнителем (Предложить заказ)',
                    'description' => 'Уведомление, отправляемое <u>заказчику</u> в случае отказа исполнителя от заказа',
                    'vars'        => array(
                        '{fio}'          => 'ФИО заказчика',
                        '{name}'         => 'Имя заказчика',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_fio}'   => 'ФИО исполнителя',
                        '{worker_login}' => 'Логин исполнителя',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                        '{message}'      => 'Текст сообщения (до 150 символов)',
                    ),
                    'impl'        => true,
                    'priority'    => 34,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
            );
        }

        Sendmail::i()->addTemplateGroup('orders', 'Заказы', 1);

        return $aTemplates;
    }


    public static function bOpenedContacts($nId)
    {
        return self::model()->isOpenedContacts($nId);

    }

    /**
     *
     * @return array ('t' => title in order list filter, 'order_t' => title for order item in list, 'class' => view style )
     */
    public static function getOrderMainStatusesContent()
    {
        return [
            self::STATUS_IN_WORK => [
                'id'      => self::STATUS_IN_WORK,
                't'       => _t('orders', 'В работе'),
                'order_t' => _t('orders', 'В работе'),
                'class'   => 'order__status-work',
            ],
            self::STATUS_OPENED         => [
                'id'      => self::STATUS_OPENED,
                't'       => _t('orders', 'Открытые'),
                'order_t' => _t('orders', 'Активно'),
                'class'   => 'order__status-opened',
            ],
            self::STATUS_CLOSED         => [
                'id'      => self::STATUS_CLOSED,
                't'       => _t('orders', 'Закрытые'),
                'order_t' => _t('orders', 'Завершено'),
                'class'   => 'order__status-close',
            ],
        ];
    }

}