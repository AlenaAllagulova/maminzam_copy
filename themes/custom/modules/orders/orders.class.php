<?php

class Theme_Custom_Orders extends Theme_Custom_Orders_Base
{
    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {

        return array(
            'cronOfferStatus'        => array('period' => '*/15 * * * *'),
            'cronOrdersClose'        => array('period' => '0 0 * * *'),
            'cronNewOrders'          => array('period' => '*/5 * * * *'),
            'cronOpinionsDelay'      => array('period' => '0 0 * * *'),
            'cronOrdersOnActivation' => array('period' => '0 0 * * *'),
        );
    }

    /**
     * cronOpinionsDelay: уведомление заказчику и исполнителю о том что можно публиковать отзыв с назначенным исполнителем через X дней
     * Рекомендуемый интервал: раз в сутки в 00:00:00
     */
    public function cronOpinionsDelay()
    {
        if (!bff::cron()) {
            return;
        }

        $this->model->makeEnotifyAboutOpinionsAllowed();

    }

    /**
     * cronOrdersOnActivation: для заказов с платной активацией меняем статус при истечении срока публикации
     * Рекомендуемый интервал: раз в сутки в 00:00:00
     */
    public function cronOrdersOnActivation()
    {
        if (!bff::cron()) {
            return;
        }

        $this->model->changeStatusOnActivation();
    }


    /**
     * Добавление заказа
     */
    public function add()
    {
        $this->security->setTokenPrefix('order-item-form');

        $aData = $this->validateOrderData(0, Request::isPOST());
        $nUserID = User::id();

        $invitesEnabled = static::invitesEnabled();
        $registerPhone = Users::registerPhone(); # задействовать: номер телефона

        $workerInvite = 0;
        if($invitesEnabled){
            $workerInvite = $this->input->postget('worker_invite', TYPE_UINT);
        }

        if (Request::isPOST() && ! $workerInvite) {
            do {
                $aResponse = array();
                $bNeedActivation = false;

                if (!$nUserID) {
                    # проверяем IP для неавторизованных
                    $mBanned = Users::i()->checkBan(true);
                    if ($mBanned) {
                        $this->errors->set(_t('orders', 'В доступе отказано по причине: [reason]', array('reason' => $mBanned)));
                        break;
                    }

                    # Обязательный контактный номер телефона (без подтверждениея)
                    $sContactPhone = $this->input->post('contact_phone', TYPE_NOTAGS);
                    $objUsers = Users::i();
    
                    if (!empty($sContactPhone)) {
                        
                        if (!$objUsers->isValidTelephone($sContactPhone)) {
                            $this->errors->set(_t('users', 'Контактный телефон указан некорректно'));
                            break;
                        }
    
                        $aContactPhone = func::unserialize($objUsers->prepareTelephoneContacts($sContactPhone));
                    }
                    
                    if ($registerPhone) {
                        $phone = $this->input->post('phone', TYPE_NOTAGS, array('len' => 30, 'len.sys' => 'orders.phone.limit'));
                        if ( ! $this->input->isPhoneNumber($phone)) {
                            $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                            break;
                        }
    
                        $aContactPhone = func::unserialize($objUsers->prepareTelephoneContacts($phone));
                    }

                    # регистрируем нового или задействуем существующего пользователя по E-mail или Телефону
                    $sEmail = $this->input->post('email', TYPE_NOTAGS, array('len' => 100, 'len.sys' => 'orders.email.limit')); # E-mail
                    $sName = $this->input->post('name', TYPE_NOTAGS, array('len' => 75, 'len.sys' => 'orders.name.limit'));
                    if (!$this->input->isEmail($sEmail)) {
                        $this->errors->set(_t('orders', 'E-mail адрес указан некорректно'));
                        break;
                    }
                    # антиспам фильтр: временные ящики
                    if(Site::i()->spamEmailTemporary($sEmail)){
                        $this->errors->set(_t('', 'Указанный вами email адрес находится в списке запрещенных, используйте например @gmail.com'));
                        break;
                    }

                    $aUserData = Users::model()->userDataByFilter(($registerPhone ? array('phone_number'=>$phone) :  array('email' => $sEmail)),
                        array('user_id', 'activated', 'blocked', 'blocked_reason', 'type', 'activate_key', 'email')
                    );
                    if (empty($aUserData)) {
                        # проверяем уникальность email адреса
                        if ($registerPhone && Users::model()->userEmailExists($sEmail, $aUserData['user_id'])) {
                            $this->errors->set(_t('users', 'Пользователь с таким e-mail адресом уже зарегистрирован. <a [link_forgot]>Забыли пароль?</a>',
                                array('link_forgot' => 'href="' . Users::url('forgot') . '"')
                            ), 'email');
                            break;
                        }

                        if (!$this->errors->no('orders.order.add.step1.guest',array('id'=>0,'data'=>&$aData,'email'=>&$sEmail,'name'=>&$sName))) {
                            break;
                        }

                        # Создаем аккаунт пользователя
                        $aRegisterData = array(
                            'email' => $sEmail,
                            'name'  => $sName,
                        );
                        
                        if (!empty($aContactPhone)) {
                            $aRegisterData = array_merge($aRegisterData, $aContactPhone);
                        }
                        
                        if ($registerPhone) $aRegisterData['phone_number'] = $phone;
                        if (Users::rolesEnabled()) {
                            $roleID = $this->input->post('role_id', TYPE_UINT);
                            if (!array_key_exists($roleID, Users::roles())) {
                                $roleID = Users::ROLE_PRIVATE;
                            }
                            $aRegisterData['role_id'] = $roleID;
                        }
                        if (Users::useClient()) {
                            $aRegisterData['type'] = Users::TYPE_CLIENT;
                        }
                        $aUserData = Users::i()->userRegister($aRegisterData);

                        if (empty($aUserData['user_id'])) {
                            $this->errors->set(_t('orders', 'При публикации заказа возникла ошибка, обратитесь к администратору'));
                            break;
                        }
                    } else {
                        # пользователь существует
                        # его аккаунт заблокирован
                        if ($aUserData['blocked']) {
                            $this->errors->set(_t('orders', 'В доступе отказано по причине: [reason]', array('reason' => $aUserData['blocked_reason'])));
                            break;
                        }
                        # исполнитель
                        if (Users::useClient() && $aUserData['type'] == Users::TYPE_WORKER) {
                            $this->errors->set(_t('orders', 'Добавлять заказы могут только заказчики'));
                            break;
                        }
                        $sEmail = $aUserData['email'];
                    }
                    $nUserID = $aUserData['user_id'];
                    $aData['user_id'] = $nUserID;
                    $bNeedActivation = true;
                } else {
                    if (!User::isClient() && Users::useClient()) {
                        $this->errors->set(_t('orders', 'Добавлять заказы могут только заказчики'));
                        break;
                    }
                    # если пользователь авторизован и при этом не вводил номер телефона ранее
                    if ($registerPhone){
                        $aUserData = User::data(array('user_id', 'email', 'name', 'phone_number', 'phone_number_verified'), true);
                        if (empty($aUserData['phone_number']) || !$aUserData['phone_number_verified']) {
                            $phone = $this->input->post('phone', TYPE_NOTAGS, array('len'=>30, 'len.sys' => 'orders.phone.limit'));
                            if (!$this->input->isPhoneNumber($phone)) {
                                $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                                break;
                            }
                            if (Users::model()->userPhoneExists($phone, $nUserID)) {
                                $this->errors->set(_t('users', 'Пользователь с таким номером телефона уже зарегистрирован.'), 'phone');
                                break;
                            }
                            $aActivation = Users::i()->getActivationInfo();
                            Users::model()->userSave($nUserID, array(
                                'activate_key'    => $aActivation['key'],
                                'activate_expire' => $aActivation['expire'],
                                'phone_number'    => $phone,
                            ));
                            $aUserData['activate_key'] = $aActivation['key'];
                            $sName = $aUserData['name'];
                            $sEmail = $aUserData['email'];
                            $bNeedActivation = true;
                        }
                    }
                }

                if (!$this->errors->no('orders.order.add.step2',array('id'=>0,'data'=>&$aData,'activation'=>$bNeedActivation))) {
                    break;
                }

                $trusted = User::isTrusted($this->module_name);
                if ($bNeedActivation) {
                    $aData['status'] = self::STATUS_NOTACTIVATED;
                    $aActivation = $this->getActivationInfo();
                    $aData['activate_key'] = $aActivation['key'];
                    $aData['activate_expire'] = $aActivation['expire'];
                } else {
                    $aData['status'] = self::STATUS_OPENED;
                    if($aData['price_vacancy']){
                        $aData['status'] = Orders::STATUS_ONACTIVATION;
                    }
                    $aData['moderated'] = $trusted ? 1 : 0; # помечаем на модерацию
                    if($invitesEnabled && ! $aData['moderated'] && $aData['visibility'] == static::VISIBILITY_PRIVATE){
                        $aData['moderated'] = 2;    # для приватных заказов - постмодерация
                    }
                }

                if (!$this->errors->no('orders.order.add.step3',array('id'=>0,'data'=>&$aData,'activation'=>$bNeedActivation))) {
                    break;
                }

                # создаем заказ
                $nOrderID = $this->model->orderSave(0, $aData, 'd');
                if (!$nOrderID) {
                    $this->errors->reloadPage();
                    break;
                } else {
                    if (!$bNeedActivation) {
                        # накручиваем счетчик кол-ва заказов пользователя
                        $this->security->userCounter('orders', 1, $nUserID);
                        if ($trusted || !static::premoderation()) {
                            # уведомление исполнителей о новом заказе
                            $this->enotifyNewOrder($nOrderID);
                        }
                        $aResponse['id'] = $nOrderID;
                    }
                }

                # загружаем изображения
                if (static::imagesLimit()) {
                    # сохраняем / загружаем изображения
                    $oImages = $this->orderImages($nOrderID);
                    $oImages->setAssignErrors(false);
                    if ($this->input->post('images_type', TYPE_STR) == 'simple') {
                        # загружаем
                        if (!empty($_FILES)) {
                            for ($i = 1; $i <= $oImages->getLimit(); $i++) {
                                $oImages->uploadFILES('images_simple_' . $i);
                            }
                            # удаляем загруженные через "удобный способ"
                            $aImages = $this->input->post('images', TYPE_ARRAY_STR);
                            $oImages->deleteImages($aImages);
                        }
                    } else {
                        # перемещаем из tmp-директории в постоянную
                        $oImages->saveTmp('images');
                    }
                }

                # загружаем файлы
                if (static::attachmentsLimit()) {
                    $this->orderAttachments($nOrderID)->saveTmp('attach');
                }

                # теги
                $this->orderTags()->tagsSave($nOrderID);

                # обновляем счетчик заказов "на модерации"
                if (!$trusted) {
                    $this->moderationCounterUpdate(1);
                }

                # отправляем письмо cо ссылкой на активацию заказа
                if ($bNeedActivation) {
                    if ($registerPhone) {
                        # отправляем sms c кодом активации
                        Users::i()->sms(false)->sendActivationCode($phone, $aUserData['activate_key']);
                        $aMailData = array(
                            'id'            => $aUserData['user_id'],
                            'name'          => $sName,
                            'password'      => isset($aUserData['password']) ? $aUserData['password'] : '',
                            'phone'         => $phone,
                            'email'         => $sEmail,
                            'activate_link' => isset($aUserData['activate_link']) ? $aUserData['activate_link'] : ''
                        );
                        # Сохраняем данные для повторной отправки кода
                        $this->security->sessionStart();
                        $this->security->setSESSION('users-register-data', $aMailData);
                    } else {
                        $aMailData = array(
                            'name'          => $sName,
                            'email'         => $sEmail,
                            'activate_link' => $aActivation['link'] . '_' . $nOrderID
                        );
                        bff::sendMailTemplate($aMailData, 'orders_order_activate', $sEmail);
                    }
                }

                $svc = $this->input->post('svc', TYPE_ARRAY_UINT);
                $svc = array_sum($svc);
                if ($svc) {
                    $params = array('id' => $nOrderID, 'preset' => $svc, 'status' => 1);
                    $fixedDays = $this->input->post('svc_fixed_days', TYPE_UINT);
                    if ($fixedDays) {
                        $params['svc_fixed_days'] = $fixedDays;
                    }
                    $aResponse['redirect'] = static::url('promote', $params);
                } else {
                    $aResponse['redirect'] = static::url('status', array('id' => $nOrderID));
                }

                if( ($aData['status'] == Orders::STATUS_ONACTIVATION)){
                    $aResponse['redirect'] = static::url('activation', ['id' => $nOrderID]);
                }
                # создадим приглашение для исполнителя, если необходимо
                $inviteUserID = $this->input->getpost('create_invite', TYPE_UINT);
                if ($inviteUserID) {
                    $this->createInvite($nOrderID, $inviteUserID);
                }

            } while (false);
            $this->iframeResponseForm($aResponse);
        }

        if ($nUserID && Users::useClient() && !User::isClient()) {
            return $this->viewPHP($aData, 'add.forbidden');
        }

        if($workerInvite){
            $aData['workerInvite'] = Users::model()->userData($workerInvite, array('user_id', 'type', 'login', 'name', 'surname', 'avatar', 'sex', 'pro', 'verified'));
            if(empty($aData['workerInvite']) || $aData['workerInvite']['type'] != Users::TYPE_WORKER){
                unset($aData['workerInvite']);
            }else {
                $aData['type'] = static::TYPE_SERVICE;
            }
        }

        if ($registerPhone && $nUserID) {
            $aUserData = User::data(array('phone_number', 'phone_number_verified'), true);
            if (empty($aUserData['phone_number']) || !$aUserData['phone_number_verified']) {
                $aData['phone_on'] = true;
            }
        }


        # SEO: Добавление заказа
        $url = static::url('add', array(), true);
        $this->urlCorrection(static::urlDynamic($url));
        $this->seo()->canonicalUrl($url);
        $this->setMeta('add', array(), $aData);

        return $this->form(0, $aData);
    }

    public function activation(){
        $aData = [];
        $nOrderID = $this->input->getpost('id', TYPE_UINT);
        if ( ! $nOrderID) {
            $this->errors->error404();
        }

        if (Request::isAJAX()){
            $aResponse = array();

            if (!$this->security->validateToken()) {
                $this->errors->reloadPage();

            }

            $aData = $this->input->postm(array(
                'ps' => TYPE_NOTAGS,
                'id' => TYPE_UINT,
            ));

            extract($aData);

            $aOrderData = $this->model->orderData($id, [], true);

            if(empty($aOrderData['specs'][0])){
                $this->errors->set('Выберите специализацию для активации заказа');
                $aResponse['redirect'] = static::url('edit', array('id' => $nOrderID));
                $this->ajaxResponseForm($aResponse);
            }

            $aOrderData['specs'] = reset($aOrderData['specs']);
            $aOrderData['specs']['vacancy'] = Specializations::model()->specializationData($aOrderData['specs']['spec_id']);

            if(! $aOrderData['specs']['vacancy']['price_vacancy_enabled']){
                $this->errors->set('В выбранной специализации не требуется активация заказа');
            }

            $nActivationCost = $aOrderData['specs']['vacancy']['price_vacancy'];
            if ($ps == 'balance') {
                $nBalance = User::balance();
                if ($nBalance >= $nActivationCost) {
                    $nUserBalance = Users::model()->userBalance(User::id());

                    $sBillDescr = 'Активация заказа  <br /><a href="'.static::url('view', array('id' => $id, 'keyword' => $aOrderData['keyword']), false).'">'.$aOrderData['title'].'</a>';

                    # 1) создаем закрытый счет активации услуги
                    $nBillID = $this->bills()->createBill_OutService(0, $id, User::id(), ($nUserBalance - $nActivationCost),
                        $nActivationCost, $nActivationCost, Bills::STATUS_COMPLETED, $sBillDescr,
                        []
                    );

                    # 2) снимаем деньги со счета пользователя
                    $bSuccess = $this->bills()->updateUserBalance(User::id(), $nActivationCost, false);

                    # 3) обновляем статус заказа
                    if ($nBillID && $bSuccess){
                        $aDataChanged['status'] = self::STATUS_OPENED;
                        $days = $aOrderData['specs']['vacancy']['period_vacancy'];
                        $aDataChanged['vacancy_activated_to']  = date('Y-m-d H:i:s', strtotime("+{$days}day"));

                        $this->model->orderSave($id, $aDataChanged);
                    }


                } else {
                    $this->errors->set(_t('svc', 'Недостаточно средств. Пополните счет или выберите другой способ оплаты.'));
                }
            } else {
                $aResponse['form'] = Bills::i()->createBill($ps, $nActivationCost, [], 0, $id);
            }
            $aResponse['redirect'] = static::url('status', array('id'=>$nOrderID));

            $this->ajaxResponseForm($aResponse);
        }

        $aData = $this->model->ordersList(array('id'=>$nOrderID, 'status'=>Orders::STATUS_ONACTIVATION));
        if (empty($aData)) {
            $this->redirect(static::url('status', array('id' => $nOrderID)));
        }

        $aData = reset($aData);
        $aData['spec_data'] = Specializations::model()->specializationData($aData['spec_id']);
        $aData['url_view'] = static::url('view', array('id'=>$nOrderID, 'keyword'=>$aData['keyword']));

        $aData['list'] = [$aData];
        $aData['list'] = $this->viewPHP($aData, 'search.list');

        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => $aData['title'], 'link' => static::url('view', array('id' => $aData['id'], 'keyword' => $aData['keyword']))),
            array('title' => _t('orders', 'Активация заказа'), 'active' => true),
        );

        return $this->viewPHP($aData, 'activation');
    }

    /**
     * Дополнительные ajax запросы
     */
    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR)) {
            case 'tags-autocomplete':
            {
                $sQuery = $this->input->post('q', TYPE_NOTAGS);
                $this->orderTags()->tagsAutocomplete($sQuery);
            }
                break;
            case 'order-delete': # удаление заказа
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                if( ! static::deleteAllow()){
                    $this->errors->impossible();
                    break;
                }

                $nOrderID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->orderData($nOrderID, array('user_id', 'status', 'performer_id'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if (!$this->isOrderOwner($nOrderID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if (static::ordersOpinions()) {
                    if ($aData['performer_id']) {
                        $offer = $this->model->offersDataByFilter(array('order_id' => $nOrderID, 'user_id' => $aData['performer_id']), array('status'));
                        if ($offer['status'] == static::OFFER_STATUS_PERFORMER_AGREE) {
                            $this->errors->set(_t('orders', 'Невозможно удалить завершенный заказ'));
                            break;
                        }
                    }
                }

                if (!$this->orderDelete($nOrderID)) {
                    $this->errors->reloadPage();
                    break;
                }
            }
                break;
            case 'order-hide': # закрыть заказ
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nOrderID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->orderData($nOrderID, array('user_id', 'status'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if ( ! $this->isOrderOwner($nOrderID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if($aData['status'] != static::STATUS_OPENED) {
                    $this->errors->reloadPage();
                    break;
                }

                $aUpdate = array();
                $aUpdate['status_prev'] = $aData['status'];
                $aUpdate['status'] = static::STATUS_CLOSED;
                $aUpdate['status_changed'] = $this->db->now();

                $this->model->orderSave($nOrderID, $aUpdate);
            } break;
            case 'order-show': # опубликовать заказ
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nOrderID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->orderData($nOrderID, array('user_id', 'status', 'term', 'performer_id'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if ( ! $this->isOrderOwner($nOrderID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if($aData['status'] != static::STATUS_CLOSED) {
                    $this->errors->reloadPage();
                    break;
                }

                if(static::ordersOpinions()){
                    if($aData['performer_id']){
                        $offer = $this->model->offersDataByFilter(array('order_id' => $nOrderID, 'user_id' => $aData['performer_id']), array('status'));
                        if($offer['status'] == static::OFFER_STATUS_PERFORMER_AGREE){
                            $this->errors->set(_t('orders', 'Невозможно опубликовать завершенный заказ'));
                            break;
                        }
                    }
                }

                # Срок приема заявок
                if(static::termsEnabled()){
                    $aData['expire'] = $this->termExpire($aData['term']);
                }

                $aUpdate = array();
                $aUpdate['status_prev'] = $aData['status'];
                $aUpdate['status'] = static::STATUS_OPENED;
                $aUpdate['status_changed'] = $this->db->now();

                $this->model->orderSave($nOrderID, $aUpdate);
            } break;
            case 'order-trash': # перемещение заказа в корзину
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nOrderID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->orderData($nOrderID, array('user_id'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if ( ! $this->isOrderOwner($nOrderID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                $this->model->orderSave($nOrderID, array('removed = 1 - removed'));
            } break;
            case 'offer-trash': # удаление заявки
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nOfferID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->offerData($nOfferID, array('user_id'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if ($aData['user_id'] != User::id()) {
                    $this->errors->impossible();
                    break;
                }

                $this->model->offerSave($nOfferID, array('user_removed = ! user_removed'), array('id' => $nOfferID));
            }
                break;
            case 'search-price-ex':
            {
                $nSpecID = $this->input->post('id', TYPE_UINT);
                if (!$nSpecID) {
                    break;
                }
                $aData = array(
                    'nSpecID' => $nSpecID,
                    'f' => array(
                        'pf'  => 0,
                        'pt'  => 0,
                        'pc'  => 0,
                        'pex' => 0,
                        't' => static::TYPE_SERVICE
                    ));
                $aResponse['html'] = $this->viewPHP($aData, 'search.form.price.ex');
            }
                break;
            case 'price-form-sett':
            {
                $nSpecID = $this->input->post('spec_id', TYPE_UINT);
                if (!$nSpecID) {
                    break;
                }
                $aData = array(
                    'spec'       => array('spec_id' => $nSpecID),
                    'price'      => 0,
                    'price_ex'   => 0,
                    'price_rate' => 0,
                    'price_curr' => 0,
                );
                $aResponse['html'] = $this->viewPHP($aData, 'form.price');
            }
                break;
            case 'dp-form':
            {
                $nType = $this->input->post('type', TYPE_UINT);
                $nID = $this->input->post('id', TYPE_UINT);
                $bSearch = $this->input->post('search', TYPE_BOOL);
                $sFormName = $bSearch ? 'search.dp' : 'form.dp';
                if ($nType == static::TYPE_SERVICE) {
                    if (static::dynpropsEnabled()) {
                        $aResponse['dp'] = Specializations::i()->dpOrdersForm($nID, $bSearch, false, 'd', $sFormName, $this->module_dir_tpl);
                    } else {
                        $aResponse['dp'] = Specializations::i()->dpForm($nID, $bSearch, false, 'd', $sFormName, $this->module_dir_tpl);
                    }
                }
                if ($nType == static::TYPE_PRODUCT && static::useProducts()) {
                    $aResponse['dp'] = Shop::i()->dpForm($nID, $bSearch, false, 'd', $sFormName, $this->module_dir_tpl);
                }

                $aResponse['spec_vacancy_data'] = Specializations::model()->specializationData($nID);
                if($aResponse['spec_vacancy_data']['price_vacancy_enabled']){
                    $aResponse['spec_vacancy_data']['period_vacancy_days'] = tpl::declension($aResponse['spec_vacancy_data']['period_vacancy'],  _t('', 'день;дня;дней'), true);
                }

            }
                break;
            # дин. свойства: child-свойства
            case 'dp-child':
            {
                $p = $this->input->postm(array(
                        'dp_id'       => TYPE_UINT, # ID parent-дин.свойства
                        'dp_value'    => TYPE_UINT, # ID выбранного значения parent-дин.свойства
                        'name_prefix' => TYPE_NOTAGS, # Префикс для name
                        'type'        => TYPE_UINT, # тип заказа
                    )
                );

                if (empty($p['dp_id']) && empty($p['dp_value'])) {
                    $this->errors->impossible();
                } else {
                    if ($p['type'] == static::TYPE_SERVICE) {
                        if (static::dynpropsEnabled()) {
                            $aData = Specializations::i()->dpOrders()->formChildByParentIDValue($p['dp_id'], $p['dp_value'], array(
                                'name' => $p['name_prefix'],
                                'class' => 'form-control'
                            ), false
                            );
                        } else {
                            $aData = Specializations::i()->dp()->formChildByParentIDValue($p['dp_id'], $p['dp_value'], array(
                                'name' => $p['name_prefix'],
                                'class' => 'form-control'
                            ), false
                            );
                        }
                    }
                    if ($p['type'] == static::TYPE_PRODUCT && static::useProducts()) {
                        $aData = Shop::i()->dp()->formChildByParentIDValue($p['dp_id'], $p['dp_value'], array(
                            'name'  => $p['name_prefix'],
                            'class' => 'form-control'
                        ), false
                        );
                    }
                    $aResponse['form'] = $aData;
                }
            }
                break;
            case 'dp-child-search':
            {
                $p = $this->input->postm(array(
                        'dp_id'    => TYPE_UINT, # ID parent-дин.свойства
                        'dp_value' => TYPE_UINT, # ID выбранного значения parent-дин.свойства
                        'type'     => TYPE_UINT, # тип заказа
                        'id'       => TYPE_UINT, # ID специализации или категории
                    )
                );

                if (empty($p['dp_id']) && empty($p['dp_value'])) {
                    $this->errors->impossible();
                } else {
                    if ($p['type'] == static::TYPE_SERVICE) {
                        if (static::dynpropsEnabled()) {
                            $aResponse['form'] = Specializations::i()->dpOrdersForm($p['id'], true, false, 'd', 'search.dp', $this->module_dir_tpl, $p);
                        } else {
                            $aResponse['form'] = Specializations::i()->dpForm($p['id'], true, false, 'd', 'search.dp', $this->module_dir_tpl, $p);
                        }
                    }
                    if ($p['type'] == static::TYPE_PRODUCT && static::useProducts()) {
                        $aResponse['form'] = Shop::i()->dpForm($p['id'], true, false, 'd', 'search.dp', $this->module_dir_tpl, $p);
                    }
                }
            }
                break;
            case 'rss-sub':
            {
                $p = $this->input->postm(array(
                    'cat_id' => TYPE_UINT, # id категории
                    't'      => TYPE_UINT # id типа заказов
                ));
                $aData = array('f' => array('t' => $p['t']));
                if ($p['t'] == static::TYPE_SERVICE) {
                    $aData['fspecs'] = array($p['cat_id'] => 1);
                } else if ($p['t'] == static::TYPE_PRODUCT) {
                    $aData['fcats'] = array($p['cat_id'] => 1);
                }
                $this->searchRssData($aData);
                $aResponse['sub'] = $aData['rss']['sub'];
            }
                break;
            # стоимость услуг для формы добавления/редактирования
            case 'order-form-svc-prices':
            {
                if (!bff::servicesEnabled()) {
                    $aResponse['prices'] = array();
                    break;
                }
                $nCityID = $this->input->post('city', TYPE_UINT);

                $aSvcData = Svc::model()->svcListing(Svc::TYPE_SERVICE, $this->module_name);
                $ids = array();
                foreach ($aSvcData as $v) $ids[] = $v['id'];
                $aSvcPrices = $this->model->svcPricesEx($ids, $nCityID);
                foreach ($aSvcData as $v) {
                    if (empty($aSvcPrices[ $v['id'] ]) || $aSvcPrices[ $v['id'] ] <= 0) {
                        $aSvcPrices[ $v['id'] ] = $v['price'];
                    }
                }
                $aResponse['prices'] = $aSvcPrices;
            }
                break;
            case 'invite-modal': # модальное окно предложения заказа исполнителю
            {
                if ( ! static::invitesEnabled() ||
                    ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->validateOrderData(0, false, static::TYPE_SERVICE);
                $this->security->setTokenPrefix('order-item-form');
                $workerID = $this->input->post('id', TYPE_UINT);
                if ( ! $workerID) {
                    $this->errors->reloadPage();
                    break;
                }

                $filter = array(
                    'status'         => static::STATUS_OPENED,
                    'user_id'        => User::id(),
                    'performer_id'   => 0,
                    'visibility'     => static::VISIBILITY_PRIVATE,
                    'exclude_invite' => $workerID,
                    'exclude_offer'  => $workerID,
                );
                # список Приватных заказов
                $data['list'] = $this->model->ordersListInvite($filter);
                if ( ! empty($data['list'])) {
                    $data['private'] = $this->viewPHP($data, 'invite.modal.list');
                }
                # список Публичных заказов
                $filter['visibility'] = static::VISIBILITY_ALL;
                if (static::premoderation()) {
                    $filter[':moderated'] = 'O.moderated > 0';
                }
                $data['list'] = $this->model->ordersListInvite($filter);
                if ( ! empty($data['list'])) {
                    $data['public'] = $this->viewPHP($data, 'invite.modal.list');
                }
                # форма предложения нового заказа
                $workerData = Users::model()->userData($workerID, array('specs'));
                if( ! empty($workerData['specs'])){
                    foreach($workerData['specs'] as $v){
                        if($v['main']){
                            $data['spec'] = $v;
                            break;
                        }
                    }
                    if(empty($data['spec'])){
                        $data['spec'] = reset($workerData['specs']);
                    }
                }
                $data['form'] = $this->viewPHP($data, 'invite.modal.form');
                $aResponse['html'] = $this->viewPHP($data, 'invite.modal');
            }
                break;
            case 'invite': # предложение заказа исполнителю
            {
                if ( ! static::invitesEnabled()) {
                    break;
                }

                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $orderID = $this->input->post('order', TYPE_UINT);
                if ( ! $orderID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->orderData($orderID, array('user_id', 'status', 'performer_id'));
                if (empty($data['user_id']) || ! User::isCurrent($data['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['status'] != static::STATUS_OPENED || $data['performer_id']) {
                    $this->errors->reloadPage();
                    break;
                }
                $userID = $this->input->post('user', TYPE_UINT);
                if ( ! $userID) {
                    $this->errors->reloadPage();
                    break;
                }
                $user = Users::model()->userData($userID, array('type', 'blocked', 'deleted', 'activated'));
                if (empty($user['type']) || $user['type'] != Users::TYPE_WORKER) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($user['activated'] == 0 || $user['blocked'] || $user['deleted']) {
                    $this->errors->reloadPage();
                    break;
                }
                $cnt = $this->model->invitesDataByFilter(array(
                    'order_id' => $orderID,
                    'user_id' => $userID,
                ));
                if ($cnt) {
                    $this->errors->set(_t('orders', 'Вы уже предлагали исполнителю данный заказ'));
                    break;
                }
                $offers = $this->model->offersDataByFilter(array('order_id' => $orderID, 'user_id' => $userID));
                if ($offers) {
                    $this->errors->set(_t('orders', 'Вы уже предлагали исполнителю данный заказ'));
                    break;
                }

                # не чаще чем раз в {X} секунд с одного IP (для одного пользователя)
                if (Site::i()->preventSpam('order-invite', config::sysAdmin('orders.invite.prevent.spam', 10, TYPE_UINT))) {
                    $this->errors->reloadPage();
                    break;
                }

                $message = $this->input->post('message', TYPE_NOTAGS);
                $this->createInvite($orderID, $userID, $message);
            }
                break;
            case 'invite-decline': # отклонение предложения исполнителем
            {
                if( ! static::invitesEnabled()){
                    break;
                }

                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $message = $this->input->post('message', TYPE_TEXT, array('len' => 3000, 'len.sys' => 'orders.invite.message.limit'));
                if (strlen($message) < config::sys('orders.invite.message.min', 5, TYPE_UINT)) {
                    $this->errors->set(_t('orders', 'Укажите причину отказа'));
                    break;
                }

                $orderID = $this->input->post('id', TYPE_UINT);
                if ( ! $orderID) {
                    $this->errors->reloadPage();
                    break;
                }
                $order = $this->model->orderData($orderID, array('user_id', 'title', 'keyword'));
                if (empty($order)) {
                    $this->errors->reloadPage();
                    break;
                }
                $userID = User::id();
                $cnt = $this->model->invitesDataByFilter(array(
                    'user_id'  => $userID,
                    'order_id' => $orderID,
                ));

                if ( ! $cnt) {
                    $this->errors->reloadPage();
                    break;
                }

                $offerID = $this->model->offerSave(0, array(
                    'order_id'       => $orderID,
                    'user_id'        => $userID,
                    'status'         => static::OFFER_STATUS_INVITE_DECLINE,
                    'status_created' => $this->db->now(),
                    'user_ip'        => Request::remoteAddress(),
                    'client_only'    => true,
                    'descr'          => $message,
                    'from_invite'    => 1,
                ));
                if ($offerID) {
                    $this->model->orderSave($orderID, array('offers_cnt = offers_cnt + 1'));

                    $this->model->inviteDelete(array(
                        'user_id'  => $userID,
                        'order_id' => $orderID,
                    ));
                    $this->model->calcUserCounters($userID);

                    # Отправим уведомление заказчику
                    $user = User::data(array('email','name','surname','login'));
                    $client = Users::model()->userDataEnotify($order['user_id']);
                    $fio = array();
                    if (!empty($user['name'])) $fio[] = $user['name'];
                    if (!empty($user['surname'])) $fio[] = $user['surname'];
                    $user['fio'] = ( ! empty($fio) ? join(' ', $fio) : $user['login']);
                    Users::sendMailTemplateToUser($client, 'order_invite_decline', array(
                        'client_url'   => Users::url('profile', array('login' => $client['login'])),
                        'worker_fio'   => $user['fio'],
                        'worker_name'  => $user['name'],
                        'worker_login' => $user['login'],
                        'worker_url'   => Users::url('profile', array('login' => $user['login'])),
                        'order_title'  => $order['title'],
                        'order_url'    => static::url('view', array('id' => $orderID, 'keyword' => $order['keyword'])),
                        'message'      => tpl::truncate($message, config::sysAdmin('orders.email.invite.message.truncate', 150, TYPE_UINT)),
                    ));
                }
            }
                break;
            case 'offer-modal': # модальное окно для принятие приглашения, заявка в упрощеной форме
            {
                if ( ! static::invitesEnabled()) {
                    break;
                }

                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $orderID = $this->input->post('id', TYPE_UINT);
                if ( ! $orderID) {
                    $this->errors->reloadPage();
                    break;
                }
                $order = $this->model->orderData($orderID, array(), true);
                if (empty($order)) {
                    $this->errors->reloadPage();
                    break;
                }

                $userID = User::id();
                $cnt = $this->model->invitesDataByFilter(array(
                    'user_id'  => $userID,
                    'order_id' => $orderID,
                ));
                if ( ! $cnt) {
                    $this->errors->reloadPage();
                    break;
                }

                $offers = $this->model->offersDataByFilter(array(
                    'user_id'  => $userID,
                    'order_id' => $orderID,
                ));
                if ($offers) {
                    $this->errors->reloadPage();
                    break;
                }
                $aResponse['html'] = $this->viewPHP($order, 'offer.modal');
            }
                break;
            case 'note-save': # заметка: сохранение (добавление / редактирование)
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nNoteID = $this->input->post('id', TYPE_UINT);
                $nOrderID = $this->input->post('order_id', TYPE_UINT);

                $aData = $this->input->postm(array(
                        'note' => array(TYPE_NOTAGS, 'len' => 1000, 'len.sys' => 'users.note.limit'),
                    )
                );
                if (!$nOrderID) {
                    break;
                }
                if (!strlen($aData['note'])) {
                    $this->errors->set(_t('orders_note', 'Укажите текст заметки'));
                    break;
                }
                if (!$nNoteID) {
                    $aData['order_id'] = $nOrderID;
                    $aData['author_id'] = User::id();
                }
                if ($this->model->noteSave($nNoteID, $aData)) {
                    $aResponse['data'] = $this->model->noteData(User::id(), $nOrderID);
                    $aResponse['data']['note'] = nl2br($aResponse['data']['note']);
                }
            }
            break;
            case 'note-delete': # заметка: удаление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nNoteID = $this->input->post('id', TYPE_UINT);
                $aData = $this->model->noteData($nNoteID);
                if (empty($aData)) {
                    break;
                }

                if ($aData['author_id'] != User::id()) {
                    break;
                }
                if (!$this->model->noteDelete($nNoteID)) {
                    $this->errors->impossible();
                }

            }
            break;
            default:
                $this->errors->impossible();
        }
        $this->ajaxResponseForm($aResponse);
    }


    /**
     * Список заказов пользователя (владельца заказов).
     * @param integer $nUserID ID пользователя
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    protected function orders_owner_listing($nUserID, array $userData)
    {
        $aFilter = array(
            'user_id' => $nUserID,
            'removed' => 0,
        );

        $seoFilter = 0;
        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT, # № страницы
            't'    => TYPE_UINT, # Тип
            'st'   => TYPE_UINT, # Статус
        ));

        if ($f['t']) {
            $aFilter['type'] = $f['t'];
            $seoFilter++;
        }

        if ($f['st']) {
            switch ($f['st']) {
                case static::STATUS_OPENED:
                case static::STATUS_CLOSED:
                case static::STATUS_BLOCKED:
                case  Orders::STATUS_ONACTIVATION:
                    $aFilter['status'] = $f['st'];
                    break;
                case static::STATUS_NOTMODERATED:
                    $aFilter['moderated'] = 0;
                    break;
                case static::STATUS_IN_WORK:
                    $aFilter['in_work'] = 1;
                    break;
                case static::STATUS_REMOVED:
                    $aFilter['removed'] = 1;
                    break;
            }
            $seoFilter++;
        }




        $aData['aStatus'] = array(
            0                           => array( 'id' => 0,                            't' => _t('orders', 'Любой статус')),
            static::STATUS_IN_WORK      => array( 'id' => static::STATUS_IN_WORK,       't' => _t('orders', 'В работе') ),
            static::STATUS_OPENED       => array( 'id' => static::STATUS_OPENED,        't' => _t('orders', 'Открытые') ),
            static::STATUS_CLOSED       => array( 'id' => static::STATUS_CLOSED,        't' => _t('orders', 'Закрытые') ),
            static::STATUS_BLOCKED      => array( 'id' => static::STATUS_BLOCKED,       't' => _t('orders', 'Заблокированные') ),
            static::STATUS_NOTMODERATED => array( 'id' => static::STATUS_NOTMODERATED,  't' => _t('orders', 'На модерации') ),
            static::STATUS_REMOVED      => array( 'id' => static::STATUS_REMOVED,       't' => _t('orders', 'Корзина') ),
            Orders::STATUS_ONACTIVATION => array( 'id' => Orders::STATUS_ONACTIVATION,       't' => _t('orders', 'На активации') ),
        );
        if ( ! $userData['my']) {
            unset(
                $aData['aStatus'][static::STATUS_BLOCKED],
                $aData['aStatus'][static::STATUS_NOTMODERATED],
                $aData['aStatus'][static::STATUS_REMOVED],
                $aData['aStatus'][Orders::STATUS_ONACTIVATION]
            );
        }
        $aData['pgn'] = '';
        $aData['counts'] = $this->model->ordersListOwnerCounts($aFilter);
        $nCount = $this->model->ordersListOwner($aFilter, true);
        if ($nCount) {
            $pgn = new Pagination($nCount, config::sysAdmin('orders.profile.pagesize', 10, TYPE_UINT), array(
                'link'  => static::url('user.listing', array('login'=>$userData['login'])),
                'query' => $f,
            ));
            $aData['list'] = $this->model->ordersListOwner($aFilter, false, $pgn->getLimitOffset(), 'O.created DESC');
            foreach ($aData['list'] as &$v) {
                if(!empty($v['performer_id'])){
                    $v['performer_contacts_phones'] = Users::i()->getUserContacts($v['performer_id'], true);
                }
                if(!empty($v['performer_spec_id'])){
                    $v['performer_spec_data'] = Specializations::model()->specializationData($v['performer_spec_id']);
                }
                if(!empty($v['performer_id']) && isset($v['performer_sex'])){
                    $v['performer_avatar_url'] = UsersAvatar::url($v['performer_id'], $v['performer_avatar'], UsersAvatar::szSmall, $v['performer_sex']);
                }
                $v['specs'] = $this->model->orderSpecs($v['id']);
                # Дин. свойства
                if ($v['type'] == static::TYPE_SERVICE) {
                    $aFirstSpec = reset($v['specs']);
                    if (static::dynpropsEnabled()) {
                        $v['dynprops_simple'] = Specializations::i()->dpOrdersView($aFirstSpec['spec_id'], $v, 'd', 'view.dp.simple');
                        $v['dynprops_simple'] = json_decode($v['dynprops_simple'], true);
                    }
                }
                } unset($v);
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }
        $aData['list'] = $this->viewPHP($aData, 'owner.list.ajax');
        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'    => $aData['pgn'],
                'list'   => $aData['list'],
                'counts' => $aData['counts'],
            ));
        }

        # SEO:
        $this->seo()->robotsIndex(!$seoFilter);
        $seoURL = static::url('user.listing', array('login'=>$userData['login']), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL, array('page'=>$f['page']));
        $this->seo()->setPageMeta($this->users(), 'profile-orders', array(
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'region'  => $userData['region'],
            'country' => $userData['country'],
            'page'    => $f['page'],
        ), $aData);

        $aData['f'] = &$f;
        $aData['user'] = &$userData;
        return $this->viewPHP($aData, 'owner.listing');
    }

    /**
     * Активация заказа
     * @param get ::string 'c' - ключ активации
     * @return string HTML
     */
    public function activate()
    {
        $langActivateTitle = _t('orders', 'Активация заказа');

        $sCode = $this->input->get('c', TYPE_STR); # ключ активации + ID заказа
        list($sCode, $nItemID) = explode('_', (!empty($sCode) && (strpos($sCode, '_') !== false) ? $sCode : '_'), 2);
        $nItemID = $this->input->clean($nItemID, TYPE_UINT);

        # 1. Получаем данные о заказе:
        $aData = $this->model->orderData($nItemID, array(
                'user_id',
                'status',
                'activate_key',
                'activate_expire',
                'blocked_reason',
                'price_vacancy',
                'vacancy_activated_to',
                'title',
                'keyword'
            )
        );
        if (empty($aData)) {
            # не нашли такого заказа
            return $this->showForbidden($langActivateTitle,
                _t('orders', 'Заказ не найден. Возможно период действия ссылки активации вашего заказа истек.')
            );
        }
        if ($aData['activate_key'] != $sCode || strtotime($aData['activate_expire']) < BFF_NOW) {
            # код неверный
            #  или
            # срок действия кода активации устек
            return $this->showForbidden($langActivateTitle,
                _t('orders', 'Срок действия ссылки активации истек либо она некорректна. Пожалуйста, <a [link_add]>добавьте новый заказ</a>.', array('link_add' => 'href="' . self::url('add') . '"'))
            );
        }
        if ($aData['status'] == self::STATUS_BLOCKED) {
            # заказ был заблокирован (модератором?!)
            return $this->showForbidden($langActivateTitle,
                _t('orders', 'Заказ был заблокирован модератором, причина: [reason]', array(
                        'reason' => $aData['blocked_reason']
                    )
                )
            );
        }

        # 2. Получаем данные о пользователе:
        $aUserData = Users::model()->userData($aData['user_id'], array(
            'user_id', 'email', 'name', 'password', 'password_salt',
            'activated', 'blocked', 'blocked_reason'
        ));
        if (empty($aUserData)) {
            return $this->showForbidden($langActivateTitle, _t('orders', 'Ошибка активации, обратитесь в службу поддержки.'));
        } else {
            $nUserID = $aUserData['user_id'];
            # аккаунт заблокирован
            if ($aUserData['blocked']) {
                return $this->showForbidden($langActivateTitle,
                    _t('orders', 'Ваш аккаунт заблокирован. За детальной информацией обращайтесь в службу поддержки.')
                );
            }
            # активируем аккаунт
            if (!$aUserData['activated']) {
                $sPassword = func::generator(12); # генерируем новый пароль
                $aUserData['password'] = $this->security->getUserPasswordMD5($sPassword, $aUserData['password_salt']);
                $bSuccess = Users::model()->userSave($nUserID, array(
                        'activated'    => 1,
                        'activate_key' => '',
                        'password'     => $aUserData['password'],
                    )
                );
                if ($bSuccess) {
                    # триггер активации аккаунта
                    bff::i()->callModules('onUserActivated', array($nUserID));
                    # отправляем письмо об успешной автоматической регистрации
                    bff::sendMailTemplate(array(
                        'name'     => $aUserData['name'],
                        'email'    => $aUserData['email'],
                        'password' => $sPassword
                    ),
                        'users_register_auto', $aUserData['email']
                    );
                }
            }
            # авторизуем, если текущий пользователь неавторизован
            if (!User::id()) {
                Users::i()->userAuth($nUserID, 'user_id', $aUserData['password'], true);
            }
        }

        # 3. Публикуем заказ:
        $nStatus = ($aData['price_vacancy'] > 0 && $aData['vacancy_activated_to'] == '0000-00-00 00:00:00')?
            Orders::STATUS_ONACTIVATION :
            self::STATUS_OPENED;
        $bSuccess = $this->model->orderSave($nItemID, array(
                'activate_key' => '', # чистим ключ активации
                'status_prev'  => self::STATUS_NOTACTIVATED,
                'status'       => $nStatus,
                'moderated'    => 0, # помечаем на модерацию
            )
        );

        # обновляем счетчик заказов "на модерации"
        $this->moderationCounterUpdate();

        if (!$bSuccess) {
            return $this->showForbidden($langActivateTitle,
                _t('orders', 'Ошибка активации, обратитесь в службу поддержки.')
            );
        }

        if ($bSuccess && $nStatus == Orders::STATUS_ONACTIVATION){
            Users::sendMailTemplateToUser(
                (int)$aData['user_id'],
                'orders_order_on_activation',
                [
                    'order_title'           => $aData['title'],
                    'order_url'             => Orders::url('view', ['id' => $nItemID, 'keyword' => $aData['keyword']]),
                    'order_activation_link' => Orders::url('activation', ['id' => $nItemID]),
                ],
                Users::ENOTIFY_GENERAL);
        }

        if (!static::premoderation()) {
            # уведомление исполнителей о новом заказе
            $this->enotifyNewOrder($nItemID);
        }

        # накручиваем счетчик кол-ва заказов авторизованного пользователя
        $this->security->userCounter('orders', 1, $nUserID); # +1

        $this->redirect(static::url('status', array('id' => $nItemID)));
        return '';
    }


}