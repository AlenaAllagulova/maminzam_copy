<?php

class M_Svc_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        # Настройки
        if ($security->haveAccessToModuleToMethod('svc','settings')) {
            $menu->assign('Счет и услуги', _t('','Settings'), 'svc', 'settings', true, 50);
        }
    }
}