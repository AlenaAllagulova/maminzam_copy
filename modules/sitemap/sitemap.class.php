<?php

class Sitemap_ extends SitemapBase
{
    /**
     * Построение меню
     * @param string $sKey ключ меню, например: 'main', 'footer'
     */
    public static function view($sKey)
    {
        static $cache;
        if (!isset($cache)) {
            $self = static::i();
            $self->buildMenu(true, '/main/index');
            $cache = $self->menu;
        }

        return (!empty($cache[$sKey]['sub']) ? $cache[$sKey]['sub'] : array());
    }

}