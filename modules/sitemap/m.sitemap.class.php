<?php

class M_Sitemap_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        if ($security->haveAccessToModuleToMethod('sitemap', 'listing')) {
            $menu->assign('Карта сайта и меню', 'Управление меню', 'sitemap', 'listing', true, 1);
            $menu->assign('Карта сайта и меню', 'Добавить меню', 'sitemap', 'add', false, 2);
            $menu->assign('Карта сайта и меню', 'Редактирование меню', 'sitemap', 'edit', false, 3);
        }
    }
}