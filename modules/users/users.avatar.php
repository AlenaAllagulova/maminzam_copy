<?php

class UsersAvatar_ extends CImageUploader
{
    /**
     * Константы размеров
     */
    const szSmall  = 's'; // small
    const szNormal = 'n'; // normal
    const szBig = 'b'; // big

    public function initSettings()
    {
        $this->path = bff::path('avatars', 'images');
        $this->pathTmp = bff::path('tmp', 'images');

        $this->url = bff::url('avatars', 'images');
        $this->urlTmp = bff::url('tmp', 'images');

        $this->table = TABLE_USERS;
        $this->fieldID = 'user_id';
        $this->fieldImage = 'avatar';
        $this->filenameLetters = config::sysAdmin('users.avatar.filename.letters', 6, TYPE_UINT);
        $this->folderByID = config::sysAdmin('users.avatar.folderbyid', true, TYPE_BOOL);
        $this->maxSize = config::sysAdmin('users.avatar.max.size', 5242880, TYPE_UINT); # 2мб (2мб: 2097152, 5мб: 5242880)

        $this->minWidth = config::sysAdmin('users.avatar.min.width', 100, TYPE_UINT);
        $this->minHeight = config::sysAdmin('users.avatar.min.height', 100, TYPE_UINT);
        $this->maxWidth = config::sysAdmin('users.avatar.max.width', 2000, TYPE_UINT);
        $this->maxHeight = config::sysAdmin('users.avatar.max.height', 2000, TYPE_UINT);

        $this->sizes = bff::filter('users.avatar.sizes', array(
            self::szSmall  => array('width' => 64, 'height' => 64),
            self::szNormal => array('width' => 120, 'height' => 120),
            self::szBig => array('width' => 245, 'height' => 245),
        ));
    }

    public static function url($nUserID, $sFilename, $sSizePrefix, $nSex = 0, $bTmp = false)
    {
        static $i;
        if (!isset($i)) {
            $i = new self();
        }
        $i->setRecordID($nUserID);

        if (empty($sFilename)) {
            return $i->url . $sSizePrefix . '.png';
        }

        return $i->getURL($sFilename, $sSizePrefix, $bTmp);
    }

    public static function urlDefault($sSizePrefix, $nSex = 0)
    {
        return self::url(0, 0, $sSizePrefix, $nSex);
    }

    /**
     * Загрузка аватара из соц. сети
     * @param integer $nSocialProviderID ID провайдера
     * @param string $sProviderAvatarURL URL аватара в соц. сети
     * @param boolean $bNewUserAccount true - вновь созданный аккаунт пользователя
     */
    public function uploadSocial($nSocialProviderID, $sProviderAvatarURL, $bNewUserAccount = true)
    {
        if ($nSocialProviderID == UsersSocial::PROVIDER_ODNOKLASSNIKI && stripos($sProviderAvatarURL, 'stub_') !== false) {
            # Ссылка на аватар у Одноклассников.ру
            # пустой - http://usd12.odnoklassniki.ru/res/stub_128x96.gif
            # загруженный - http://usd5.odnoklassniki.ru/getImage?photoId=000000000000&photoType=2
        } else {
            # сбрасываем ограничение по ширине/высоте
            $this->setDimensions(0, 0, 0, 0);
            # загружаем
            $mResult = $this->uploadURL($sProviderAvatarURL, !$bNewUserAccount, true);
            if ($mResult !== false) {
                # обновляем аватар в сессии
                if (!$bNewUserAccount) {
                    $this->security->updateUserInfo(array('avatar' => $mResult['filename']));
                }
            }
        }
    }
}