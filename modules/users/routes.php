<?php

return [
    # авторизация соц сетей
    'users-loginsocial' => [
        'pattern'  => 'user/loginsocial/(.*)',
        'callback' => 'users/loginSocial/provider=$1',
        'priority' => 130,
    ],    
    # авторизация из админ панели
    'users-login-admin' => [
        'pattern'  => 'user/login_admin(.*)',
        'callback' => 'users/login_admin/',
        'priority' => 140,
    ],
    # список действия пользователя
    'user-action' => [
        'pattern'  => 'user/(login|logout|register|forgot|activate|settings)',
        'callback' => 'users/$1/',
        'priority' => 160,
    ],
    # список действия для кабинета авторизованного пользователя
    'user-profile-tab-action' => [
        'pattern'  => 'user/(.+)/(shop|portfolio|qa|blog)/([\d]+)\-(.*)(/|)',
        'callback' => 'users/profile/login=$1&tab=$2&id=$3',
        'priority' => 170,
    ],
    # настройки кабинета пользователя для других модулей
    'user-profile-settings' => [
        'pattern'  => 'user/(.+)/(shop|portfolio)/settings(/|)',
        'callback' => '$2/my_settings',
        'priority' => 180,
    ],
    # просмотр профиля пользователя для других модулей
    'user-profile-tab' => [
        'pattern'  => 'user/(.+)/(orders|offers|shop|portfolio|qa|opinions|info|common|blog|price|workflows)(/|)',
        'callback' => 'users/profile/login=$1&tab=$2',
        'priority' => 190,
    ],
    # просмотр профиля пользователя
    'user-profile' => [
        'pattern'  => 'user/(.+)(/|)',
        'callback' => 'users/profile/login=$1',
        'priority' => 200,
    ],
    # поиск пользователей
    'users-search' => [
        'pattern'  => 'users/(.*)',
        'callback' => 'users/search/spec=$1',
        'priority' => 210,
    ],
    # прайс главная страница
    'price' => [
        'pattern'  => 'price(/|)',
        'callback' => 'users/price/',
        'priority' => 220,
    ],
    # прайс исполнителя
    'price-users' => [
        'pattern'  => 'price/users/(.+)(/|)',
        'callback' => 'users/price_users/service=$1',
        'priority' => 230,
    ],
    # прайс специализации
    'price-spec' => [
        'pattern'  => 'price/(.+)(/|)',
        'callback' => 'users/price/spec=$1',
        'priority' => 240,
    ],
    # кабинет пользователя
    'cabinet' => [
        'pattern'  => 'cabinet/settings(/|)',
        'callback' => 'users/my_settings/',
        'priority' => 250,
    ],
];