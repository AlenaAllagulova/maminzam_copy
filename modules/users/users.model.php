<?php

class UsersModel_ extends UsersModelBase
{
    public $langSvcServices = array(
        'title_view'       => TYPE_NOTAGS, # название
        'description'      => TYPE_STR, # описание (краткое)
        'description_full' => TYPE_STR, # описание (подробное)
    );

    public $langSvcPacks = array(
        'title_view'       => TYPE_NOTAGS, # название
        'description'      => TYPE_STR, # описание (краткое)
        'description_full' => TYPE_STR, # описание (подробное)
    );

    public function init()
    {
        parent::init();

        # список полей данных о пользователе запрашиваемых для сохранения в сессии
        $this->userSessionDataKeys = array(
            'user_id as id',
            'member',
            'login',
            'password',
            'password_salt',
            'email',
            'name',
            'surname',
            'avatar',
            'sex',
            'birthdate',
            'activated',
            'blocked',
            'blocked_reason',
            'trusted',
            'admin',
            'last_login',
            'reg3_city', 'reg1_country',
            'social_id',
            'type',
            'enotify',
            'pro',
            'pro_expire',
            'role_id',
            'workflow',
        );

        # список полей счетчиков
        $this->userStatCounters = array(
            'internalmail_new',
            'items',
            'items_fav',
            'orders_offers_client',
            'orders_offers_worker',
            'fairplay_workflows'
        );
    }

    /**
     * Получаем данные о пользователе по ID
     * @param int $nUserID ID пользователя
     * @param mixed $aDataKeys ключи необходимых данных
     * @return array|mixed
     */
    public function userData($nUserID, $aDataKeys = array())
    {
        $bSpecs = false;
        $bSpecsServices = false;
        $bSchedule = false;
        if (is_array($aDataKeys)) {
            if (in_array('specs', $aDataKeys)) {
                $bSpecs = true;
            }
            if (in_array('specsServices', $aDataKeys)) {
                $bSpecsServices = true;
            }
            if (in_array('schedule', $aDataKeys)) {
                $bSchedule = true;
            }
            foreach ($aDataKeys as $k => $v) {
                if (in_array($v, array('specs', 'specsServices', 'schedule'))) {
                    unset($aDataKeys[$k]);
                }
            }
        }
        if ($aDataKeys == '*') {
            $bSpecs = true;
            $bSpecsServices = true;
        }
        if ( ! Specializations::useServices()) {
            $bSpecsServices = false;
        }
        $aData = $this->userDataByFilter(array('user_id' => $nUserID), $aDataKeys);
        if (isset($aData['contacts'])) {
            $aData['contacts'] = (!empty($aData['contacts']) ? func::unserialize($aData['contacts']) : array());
        }
        if ($bSpecs) {
            $aData['specs'] = $this->userSpecs($nUserID, false);
        }
        if ($bSpecsServices) {
            $aData['specsServices'] = $this->userSpecsServices($nUserID);
        }
        if ($bSchedule) {
            $aData['schedule'] = $this->userShedule($nUserID);
        }

        return $aData;
    }
    /**
    * Получение данных графика занятости пользователя
    * @param integer $nUserID ID заказа
    * @return mixed
    */
    public function userShedule($nUserID)
    {
        $aRes = $this->db->select('
            SELECT *
            FROM ' . TABLE_USERS_SCHEDULE . ' OS
            WHERE OS.user_id = :user_id ',
            array(':user_id' => $nUserID)
        );

        if(empty($aRes)){
            return $aRes;
        }

        foreach ($aRes as $item){
            $aSchedule[$item['day_id']][] = (int)$item['period_id'];
        }
        return $aSchedule;
    }


    /**
     * Получаем данные о пользователе - для отправки ему email-уведомления.
     * Проверяем возможно ли отправить письмо пользователю и если нет возвращаем FALSE.
     * @param integer $nUserID ID пользователя
     * @param integer $nEnotifyID ID настройки email-уведомления, 0 - не выполнять проверку настроек уведомлений
     * @param array $aDataKeys требуемые данные
     * @return array|boolean данные или FALSE
     */
    public function userDataEnotify($nUserID, $nEnotifyID = 0, array $aDataKeys = array())
    {
        $aDataKeys = array_merge($aDataKeys, array('user_id','name','surname','email','login','blocked','enotify'));
        $aData = $this->userData($nUserID, $aDataKeys);

        do {
            # не нашли такого пользователя
            if (empty($aData)) {
                break;
            }
            # заблокирован
            if ($aData['blocked']) {
                break;
            }
            # запретил email-уведомления
            if ($nEnotifyID > 0) {
                if (empty($aData['enotify']) || !($aData['enotify'] & $nEnotifyID)) {
                    break;
                }
            }
            # email адрес некорректный
            if (!$this->input->isEmail($aData['email'])) {
                break;
            }

            return $aData;
        } while (false);

        return false;
    }

    /**
     * Сохраняем данные пользователя
     * @param int $nUserID ID пользователя
     * @param array|bool $aData данные
     * @param array $aDataStat динамические данные
     * @param array $aBind доп. параметры запросы для bind'a
     * @return bool
     */
    public function userSave($nUserID, $aData, $aDataStat = array(), array $aBind = array())
    {
        # выделяем данные требующие отдельного сохранения
        $aDataEx = array();
        foreach (array('specs','specsServices', 'schedule') as $k) {
            if (isset($aData[$k])) {
                $aDataEx[$k] = $aData[$k];
                unset($aData[$k]);
            }
        }

        $res = parent::userSave($nUserID, $aData, $aDataStat, $aBind);

        # связь со специализациями
        if (!empty($aDataEx['specs'])) {
            $aOldSpecs = $this->userSpecs($nUserID, false);
            # удаляем предыдущие настройки
            $this->db->delete(TABLE_USERS_SPECIALIZATIONS, array('user_id'=>$nUserID));
            foreach ($aDataEx['specs'] as &$v) {
                # сохраняем просчитанную позицию пользователя в специализации
                if ( ! empty($aOldSpecs[ $v['spec_id'] ]['position'])) {
                    $v['position'] = $aOldSpecs[ $v['spec_id'] ]['position'];
                }
                $v['user_id'] = $nUserID;
                $this->db->insert(TABLE_USERS_SPECIALIZATIONS, $v);
            } unset($v);
        }
        # настройки услуг специализаций
        if (!empty($aDataEx['specsServices'])) {
            $this->userSaveSpecsServices($nUserID, $aDataEx['specsServices']);
            $this->generateUserSpecsServicesCache($nUserID);
        }

        if(!empty($aDataEx['schedule'])){
            $this->userSaveSchedule($nUserID, $aDataEx);
        }
        return $res;
    }

    /**
     * Сохранение данных графика занятости пользователя
     * @param $nUserID int заказа
     * @param array $aData ['schedule'] данные графика работ
     */
    public function userSaveSchedule($nUserID, array $aData)
    {
        if (!empty($aData['schedule'])){
            $this->db->delete(TABLE_USERS_SCHEDULE, array('user_id' => $nUserID));
            $aInsert = array();
            foreach ($aData['schedule'] as $day_id => $periods) {
                # разворачиваем данные графика
                foreach ($periods as $period_id){
                    $aInsert[] = [
                        'user_id'  => $nUserID,
                        'day_id'    => $day_id,
                        'period_id' => $period_id
                    ];
                }
            }
            $this->db->multiInsert(TABLE_USERS_SCHEDULE, $aInsert);
        }
    }

    /**
     * Создаем пользователя
     * @param array $aUserData данные пользователя
     * @param array|integer|null $aGroupsID ID групп, в которые входит пользователь
     * @return int ID пользователя
     */
    public function userCreate($aUserData, $aGroupsID = null)
    {
        if (isset($aUserData['specs'])) {
            $aSpecs = $aUserData['specs'];
            unset($aUserData['specs']);
        }

        $mSpecsServices = false;
        if (isset($aUserData['specsServices'])) {
            $mSpecsServices = $aUserData['specsServices'];
            unset($aUserData['specsServices']);
        }

        $nUserID = parent::userCreate($aUserData, $aGroupsID);
        if($nUserID){
            $this->db->insert(TABLE_USERS_TABS, array('user_id' => $nUserID));
        }

        # связь со специализациями
        if ($nUserID > 0 && ! empty($aSpecs)) {
            foreach ($aSpecs as $k => $v) {
                $aSpecs[$k]['user_id'] = $nUserID;
                $this->db->insert(TABLE_USERS_SPECIALIZATIONS, $aSpecs[$k]);
            }
            # и услугами
            if ($mSpecsServices !== false) {
                $this->userSaveSpecsServices($nUserID, $mSpecsServices);
                $this->generateUserSpecsServicesCache($nUserID);
            }
        }

        return $nUserID;
    }

    /**
     * Сохраняем данные об услугах специализаций пользователя
     * @param int $nUserID ID пользователя
     * @param array $aData данные
     */
    protected function userSaveSpecsServices($nUserID, $aData)
    {
        $aOld = $this->userSpecsServices($nUserID);
        $aInsert = array();
        $aUpdate = array();
        foreach ($aData as $v) {
            if (isset($aOld[ $v['service_id'] ])) {
                $updateChanged = array();
                foreach (array('price','price_curr','disabled') as $k) {
                    if ($aOld[ $v['service_id'] ][$k] != $v[$k]) {
                        $updateChanged[$k] = $v[$k];
                    }
                }
                if (!empty($updateChanged)) {
                    $aUpdate[$v['service_id']] = $updateChanged;
                }
                unset($aOld[$v['service_id']]);
            } else {
                $v['user_id'] = $nUserID;
                $v['modified'] = $this->db->now();
                $aInsert[] = $v;
            }
        }
        if ( ! empty($aOld)) {
            foreach ($aOld as $v) {
                $this->db->delete(TABLE_USERS_SPECIALIZATIONS_SERVICES, array('user_id' => $nUserID, 'service_id' => $v['service_id']));
            }
        }
        if ( ! empty($aUpdate)) {
            foreach ($aUpdate as $k => $v) {
                $v['modified'] = $this->db->now();
                $this->db->update(TABLE_USERS_SPECIALIZATIONS_SERVICES, $v, array('user_id' => $nUserID, 'service_id' => $k));
            }
        }
        if ( ! empty($aInsert)) {
            $this->db->multiInsert(TABLE_USERS_SPECIALIZATIONS_SERVICES, $aInsert);
        }
    }

    /**
     * Данные о заметке одного пользователя о другом
     * @param integer $nID id заметки или id автора заметки
     * @param integer $nUserID о ком заметка (0 - если указан id заметки)
     * @return array|mixed
     */
    public function noteData($nID, $nUserID = 0)
    {
        if ($nUserID) {
            $aFilter = array(
                'author_id' => $nID,
                'user_id'   => $nUserID,
            );
        } else {
            $aFilter = array(
                'id' => $nID,
            );
        }

        $aFilter = $this->prepareFilter($aFilter);

        return $this->db->one_array('SELECT * FROM ' . TABLE_USERS_NOTES . $aFilter['where'], $aFilter['bind']);
    }

    /**
     * Сохранение заметки
     * @param integer $nNoteID id заметки
     * @param array $aData данных заметки
     * @return bool|int
     */
    public function noteSave($nNoteID, array $aData)
    {
        if ($nNoteID) {
            return $this->db->update(TABLE_USERS_NOTES, $aData, array('id' => $nNoteID));
        } else {
            return $this->db->insert(TABLE_USERS_NOTES, $aData);
        }
    }

    /**
     * Удаление заметки
     * @param $nNoteID - id заметки
     * @return bool
     */
    public function noteDelete($nNoteID)
    {
        return $this->db->delete(TABLE_USERS_NOTES, array('id' => $nNoteID));
    }

    /**
     * Список фрилансеров
     * @param array $aFilter фильтр списка фрилансеров
     * @param bool $bCount только подсчет кол-ва фрилансеров
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function searchList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        $sFrom = '';
        # регион: страна, регион, город, метро
        # статус пользователей
        foreach (array('reg1_country','reg2_region','reg3_city','metro_id','status', 'district_id') as $key) {
            if (array_key_exists($key, $aFilter)) {
                $aFilter[':'.$key] = array('U.'.$key.' = :'.$key, ':'.$key => $aFilter[$key]);
                unset($aFilter[$key]);
            }
        }

        # PRO-аккаунт
        if (array_key_exists('pro', $aFilter)) {
            $aFilter[':pro'] = 'U.pro != 0';
            unset($aFilter['pro']);
        }

        # проверенный
        if (array_key_exists('verified', $aFilter)) {
            $aFilter[':verified'] = array('U.verified = :verified', ':verified' => $aFilter['verified']);
            unset($aFilter['verified']);
        }

        # с примерами работ
        if (array_key_exists('ex', $aFilter)) {
            if ($bCount) $sFrom .= ', ' . TABLE_USERS_STAT . ' ST ';
            $aFilter[':ex'] = 'U.user_id = ST.user_id AND ST.portfolio > 0';
            unset($aFilter['ex']);
        }

        # с отзывами
        if (array_key_exists('op', $aFilter)) {
            $aFilter[':op'] = 'U.opinions_cache != "" AND U.opinions_cache != "0;0;0"';
            unset($aFilter['op']);
        }

        # без отрицательных отзывов
        if (array_key_exists('neg', $aFilter)) {
            # MySQL only
            $aFilter[':neg'] = '( U.opinions_cache = "" OR U.opinions_cache = "0;0;0" OR SUBSTRING_INDEX(U.opinions_cache, ";", -1) = "0" ) ';
            unset($aFilter['neg']);
        }

        # только мои избранные
        if (array_key_exists('fv', $aFilter)) {
            $sFrom .= ', ' . TABLE_USERS_FAVS . ' F ';
            $aFilter[':fv'] = array('U.user_id = F.fav_user_id AND F.user_id = :me', ':me' => User::id());
            unset($aFilter['fv']);
        }

        $bTagTT = false;
        if (array_key_exists('text', $aFilter)) {
            $sText = trim($aFilter['text']);
            # поиск по имени, фамилии, логину, описанию портфолио, резюме
            $sql = ' ( U.name LIKE :q OR U.surname LIKE :q OR U.login LIKE :q OR U.portfolio_intro LIKE :q OR U.resume_text LIKE :q ';
            # поиск по тегу (при совпадении с названием тега)
            $aTagIDs = $this->db->select_one_column('SELECT id FROM '.TABLE_TAGS.' WHERE tag LIKE :tag', array(':tag' => $sText));
            if ( ! empty($aTagIDs)) {
                $sql .= ' OR '.$this->db->prepareIN('TT.tag_id', $aTagIDs);
                $bTagTT = true;
            }
            # поиск по специализации (при совпадении с названием специализации)
            $nSpecID = $this->db->one_data('SELECT id FROM '.TABLE_SPECIALIZATIONS_LANG.' WHERE title = :title', array(':title' => $sText));
            if ($nSpecID) {
                $sql .= ' OR S.spec_id = '.$nSpecID;
            }
            $sql .= ') ';
            $aFilter[':text'] = array($sql, ':q' => '%'.$sText.'%');
            unset($aFilter['text']);
        }

        # фильтр по тегу
        if (array_key_exists('tag_id', $aFilter)) {
            $bTagTT = true;
            if (is_array($aFilter['tag_id'])){
                $aFilter[':tag_id'] = $this->db->prepareIN('TT.tag_id', $aFilter['tag_id']);
            } else {
                $aFilter[':tag_id'] = array('TT.tag_id = :tag', ':tag' => $aFilter['tag_id']);
            }
            unset($aFilter['tag_id']);
        }
        if ($bTagTT) {
            $sFrom .= ', ' . TABLE_USERS_IN_TAGS . ' TT ';
            $aFilter[':jtags'] = 'U.user_id = TT.user_id ';
        }

        $aFilter[':blocked'] = 'U.blocked = 0 AND U.deleted = 0';

        $aFilter[':specs'] = 'U.user_id = S.user_id AND S.disabled = 0';
        $sFrom .= ', '.TABLE_USERS_SPECIALIZATIONS.' S ';

        if ($bCount) {
            $aFilter = $this->prepareFilter($aFilter, 'S');
            return $this->db->one_data('SELECT COUNT(user_id) FROM
                ( SELECT U.user_id FROM ' . TABLE_USERS . ' U ' . $sFrom . $aFilter['where'] . '
                  GROUP BY U.user_id ) sl', $aFilter['bind']);
        }

        $aFilter[':stat'] = 'U.user_id = ST.user_id';
        $aFilter[':tab'] = 'U.user_id = TB.user_id';
        $aFilter = $this->prepareFilter($aFilter, 'S');
        $nUserID = User::id();
        if($nUserID){
            $aFilter['bind'][':current_user'] = $nUserID;
        }
        $aData = $this->db->select('
            SELECT U.user_id, U.login, U.name, U.surname, U.pro, U.created, U.avatar, U.verified, U.sex, U.status,
                   U.addr_lat, U.addr_lng, U.reg1_country, U.reg2_region, U.reg3_city, U.district_id, U.rating, U.opinions_cache,
                   U.portfolio_intro, U.portfolio_works, 
                   U.birthdate, U.type, U.experience, U.status_text, U.position,
                   TB.portfolio AS portfolio_tab, TB.price AS price_tab,  
                   GROUP_CONCAT(T.tag_id) AS tags,
                   MS.cat_id AS main_cat_id,  MS.spec_id AS main_spec_id, MS.price AS main_price, MS.price_curr AS main_price_curr,
                   MS.price_rate AS main_price_rate, MS.price_rate_text AS main_price_rate_text,
                   MS.budget AS main_budget, MS.budget_curr AS main_budget_curr, MS.services_cache AS main_services_cache,
                   S.services_cache, S.spec_id AS spec_id, ST.specs_services, ST.last_activity '.
                   ($nUserID ? ', N.id AS note_id, N.note ' : '').
            'FROM '.TABLE_USERS_STAT.' ST, ' . TABLE_USERS . ' U
                '.($nUserID ? ' LEFT JOIN '.TABLE_USERS_NOTES.' N ON U.user_id = N.user_id AND N.author_id = :current_user' : '').'
                LEFT JOIN ' . TABLE_USERS_IN_TAGS . ' T ON U.user_id = T.user_id
                LEFT JOIN '.TABLE_USERS_SPECIALIZATIONS.' MS ON U.user_id = MS.user_id AND MS.main = 1 
                , '.TABLE_USERS_TABS.' TB '.
            $sFrom . $aFilter['where'] . '
            GROUP BY U.user_id
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, $aFilter['bind']
        );
        if ( ! empty($aData)) {
            $this->usersListPrepare($aData);
        } else {
            $aData = array();
        }

        return $aData;
    }

    /**
     * Прайс услуг специализаций пользователей
     * @param array $aFilter фильтр
     * @param bool $bCount только подсчет кол-ва
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return array
     */
    public function priceList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        $bSpecCatsOn = Specializations::catsOn();

        $bCatOn = false;
        if (isset($aFilter['cat_id'])) {
            $aFilter[':cat'] = array('SIN.cat_id = :cat', ':cat' => $aFilter['cat_id']);
            $bCatOn = true;
            unset($aFilter['cat_id']);
        }

        $aFilter[':spec'] = ' S.spec_id = SS.id AND SS.enabled = 1 ';
        if ($bSpecCatsOn) {
            $aFilter[':in'] = ' SS.id = SIN.spec_id ';
        }

        # регион: страна, регион, город
        $bRegion = false;
        foreach (array('reg1_country','reg2_region','reg3_city') as $key) {
            if (array_key_exists($key, $aFilter)) {
                $bRegion = true;
                $aFilter[':'.$key] = array('R.'.$key.' = :'.$key, ':'.$key => $aFilter[$key]);
                unset($aFilter[$key]);
            }
        }
        if ($bRegion) {
            $aFilter[':jreg'] = ' R.service_id = S.id ';
            if (isset($aFilter[':reg3_city'])) {
                unset($aFilter[':reg1_country']);
                unset($aFilter[':reg2_region']);
            } elseif (isset($aFilter[':reg2_region'])) {
                $aFilter[':reg3_city'] = 'R.reg3_city = 0';
                unset($aFilter[':reg1_country']);
            } else {
                $aFilter[':reg2_region'] = 'R.reg2_region = 0';
                $aFilter[':reg3_city'] = 'R.reg3_city = 0';
            }
        }

        $bPriceAdmin = ! empty($aFilter['bPriceAdmin']);
        unset($aFilter['bPriceAdmin']);

        if ( ! $bPriceAdmin && ! $bRegion) {
            $aFilter[':avg'] = 'S.price_avg > 0';
        }

        $sFrom = ' FROM '.TABLE_SPECIALIZATIONS_SERVICES.' S,
                        '.TABLE_SPECIALIZATIONS.' SS
                        '.($bSpecCatsOn ? ', '.TABLE_SPECIALIZATIONS_IN_CATS.' SIN ' : '')
                         .($bRegion ? ', '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' R ' : '');

        if ($bCount) {
            $aFilter = $this->prepareFilter($aFilter, 'S');
            return (int)$this->db->one_data('SELECT COUNT(*) FROM ( SELECT S.id '.$sFrom.$aFilter['where'].' GROUP BY S.id ) sub', $aFilter['bind']);
        }

        $aFilter[':lang'] = $this->db->langAnd(false, 'S', 'SL');
        $aFilter = $this->prepareFilter($aFilter, 'S');

        if (empty($sqlOrder)) {
            if ($bCatOn) {
                $sqlOrder = 'SIN.num,';
            } else {
                $sqlOrder = 'SS.num,';
            }
            $sqlOrder .= 'S.num';
        }
        $aData = $this->db->select('
            SELECT S.id, S.spec_id, S.price, S.price_curr, SL.title, SL.measure, S.keyword '.
            ($bRegion ? ', R.price_min, R.price_avg, R.price_max' : ', S.price_min, S.price_avg, S.price_max').'
            '.$sFrom.', '.TABLE_SPECIALIZATIONS_SERVICES_LANG.' SL
            '.$aFilter['where'].'
            GROUP BY S.id
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, $aFilter['bind']
        );
        return $aData;
    }

    /**
     * Специализации пользователя
     * @param integer $nUserID ID пользователя
     * @param boolean $bExcludeDisabled исключая выключенные
     * @return array
     */
    public function userSpecs($nUserID, $bExcludeDisabled = true)
    {
        $bCatON = Specializations::catsOn();
        return $this->db->select_key('
                SELECT U.*, S.keyword AS spec_keyword, SL.title AS spec_title ' . ($bCatON ? ', C.keyword AS cat_keyword, CL.title AS cat_title, C.numlevel AS lvl, C.pid ' : '') . '
                FROM ' . TABLE_USERS_SPECIALIZATIONS . ' U,
                     ' . TABLE_SPECIALIZATIONS . ' S, ' . TABLE_SPECIALIZATIONS_LANG . ' SL
                     ' . ($bCatON ? ', ' . TABLE_SPECIALIZATIONS_CATS . ' C, ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL ' : '') . '
                WHERE U.user_id = :user_id AND U.spec_id = S.id ' .($bExcludeDisabled ? ' AND U.disabled = 0 ':''). $this->db->langAnd(true, 'S', 'SL') .
            ($bCatON ? $this->db->langAnd(true, 'C', 'CL') . ' AND U.cat_id = C.id ' : '') . '
                ORDER BY U.num
            ', 'spec_id', array(':user_id' => $nUserID)
        );
    }

    /**
     * Выключение специлизаций пользователя, превышающих допустимый лимит
     * @param integer $nUserID ID пользователя
     * @param integer $nLimit допустимый лимит
     * @return array ID выключенных в результате действия специализаций
     */
    public function userSpecsDisableByLimit($nUserID, $nLimit)
    {
        $aUserSpecs = $this->db->select_one_column('
                SELECT S.spec_id
                FROM ' . TABLE_USERS_SPECIALIZATIONS . ' S
                WHERE S.user_id = :user_id AND S.disabled = 0
                ORDER BY S.num
            ', array(':user_id' => $nUserID)
        );
        if (!empty($aUserSpecs) && sizeof($aUserSpecs) > $nLimit) {
            $aDisableSpecs = array_slice($aUserSpecs, $nLimit);
            if (!empty($aDisableSpecs)) {
                # специализации
                $this->db->update(TABLE_USERS_SPECIALIZATIONS, array('disabled'=>1), array(
                    'user_id' => $nUserID, 'spec_id' => $aDisableSpecs,
                ));
                # услуги специализаций
                $this->db->update(TABLE_USERS_SPECIALIZATIONS_SERVICES, array('disabled'=>1), array(
                    'user_id' => $nUserID, 'spec_id' => $aDisableSpecs,
                ));
                return $aDisableSpecs;
            }
        } else {
            return array();
        }
    }

    /**
     * Включение специлизаций пользователя, не превышающих допустимый лимит
     * @param integer $nUserID ID пользователя
     * @param integer $nLimit допустимый лимит
     * @return array ID включенных в результате действия специализаций
     */
    public function userSpecsEnableByLimit($nUserID, $nLimit)
    {
        $aUserSpecs = $this->db->select('
                SELECT S.spec_id, S.disabled
                FROM ' . TABLE_USERS_SPECIALIZATIONS . ' S
                WHERE S.user_id = :user_id
                ORDER BY S.num
                '.$this->db->prepareLimit(0, $nLimit).'
            ', array(':user_id' => $nUserID)
        );
        if (!empty($aUserSpecs)) {
            $aEnableSpecs = array();
            foreach ($aUserSpecs as $v) {
                if ($v['disabled']) $aEnableSpecs[] = $v['spec_id'];
            }
            if (!empty($aEnableSpecs)) {
                # специализации
                $this->db->update(TABLE_USERS_SPECIALIZATIONS, array('disabled'=>0), array(
                    'user_id' => $nUserID, 'spec_id' => $aEnableSpecs, 'disabled'=>1,
                ));
                # услуги специализаций
                $this->db->update(TABLE_USERS_SPECIALIZATIONS_SERVICES, array('disabled'=>0), array(
                    'user_id' => $nUserID, 'spec_id' => $aEnableSpecs, 'disabled'=>1,
                ));
                return $aEnableSpecs;
            }
        } else {
            return array();
        }
    }

    /**
     * Услуги специализаций пользователя
     * @param integer $nUserID ID пользователя
     * @return mixed
     */
    public function userSpecsServices($nUserID)
    {
        return $this->db->select_key('
            SELECT U.*, S.price AS serv_price, S.price_curr AS serv_price_curr, S.price AS serv_price, S.price_free, SL.title, SL.measure
            FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' U,
                 '.TABLE_SPECIALIZATIONS_SERVICES . ' S,
                 '.TABLE_SPECIALIZATIONS_SERVICES_LANG . ' SL
            WHERE U.user_id = :user_id AND U.service_id = S.id ' . $this->db->langAnd(true, 'S', 'SL').'
            ORDER BY S.num
            ', 'service_id', array(':user_id' => $nUserID)
        );
    }

    /**
     * Список табов пользователя
     * @param integer $userID
     * @return array
     */
    public function userProfileTabs($userID)
    {
        if( ! $userID) return array();
        return $this->db->one_array('SELECT orders, portfolio, shop, price, qa, blog FROM '.TABLE_USERS_TABS.' WHERE user_id = :id', array(':id' => $userID));
    }

    /**
     * Сохранение списка табов пользователя
     * @param integer $userID
     * @param array $data
     * @return array
     */
    public function userProfileTabsSave($userID, $data)
    {
        return $this->db->update(TABLE_USERS_TABS, $data, array('user_id' => $userID));
    }


    /**
     * Построение кеша услуг специализаций пользователя
     * @param integer|array $mFilter ID пользователя или фильтр для выборки пользователей
     * @return integer кол-во затронутых пользователей
     */
    public function generateUserSpecsServicesCache($mFilter = array())
    {
        $nSummaryCount = 0;
        if (is_integer($mFilter)) {
            $mFilter = array('user_id' => $mFilter);
        } else {
            if (!isset($mFilter['type'])) $mFilter['type'] = Users::TYPE_WORKER;
            if (!isset($mFilter['blocked'])) $mFilter['blocked'] = 0;
            if (!isset($mFilter['deleted'])) $mFilter['deleted'] = 0;
            if (!isset($mFilter['activated'])) $mFilter['activated'] = 1;
        }

        $nLimit = Users::searchServicesLimit();

        $db = $this->db;
        $this->db->select_rows_chunked(TABLE_USERS, array('user_id'), $mFilter, 'user_id', array(), function($rows) use ($nLimit, &$nSummaryCount, $db) {
            $aUsers = array();
            foreach ($rows as $v) {
                $aUsers[] = $v['user_id'];
            }
            # данные о специализациях
            $aTmp = $db->select_rows(TABLE_USERS_SPECIALIZATIONS, array('user_id', 'spec_id'), array('user_id'=>$aUsers, 'disabled'=>0));
            $aSpecs = array();
            foreach ($aTmp as $v) {
                $aSpecs[ $v['user_id'] ][ $v['spec_id'] ] = $v;
            }
            unset($aTmp);

            # данные об услугах специализаций
            $aTmp = $db->select('
                SELECT U.*, S.price_free
                FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' U,
                     '.TABLE_SPECIALIZATIONS_SERVICES.' S
                WHERE U.service_id = S.id AND U.disabled = 0 AND '.$db->prepareIN('U.user_id', $aUsers).'
                ORDER BY S.num');
            $aServices = array();
            foreach ($aTmp as $v) {
                if( ! $v['price'] && ! $v['price_free']) continue;
                $aServices[ $v['user_id'] ][ $v['spec_id'] ][ $v['service_id'] ] = $v;
            }
            unset($aTmp);

            $sUpdateStat = '';
            $sUpdateSpecs = '';
            foreach ($rows as $u) {
                $nUserID = $u['user_id'];
                if (empty($aSpecs[$nUserID])) continue;
                $nCount = 0;
                foreach ($aSpecs[$nUserID] as $k => $v) {
                    $aCache = array();
                    $nCountCache = 0;
                    if ( ! empty($aServices[$nUserID][$k])) {
                        foreach ($aServices[$nUserID][$k] as $vv) {
                            if (count($aCache) < $nLimit) {
                                $aCache[$vv['service_id']] = array(
                                    'id' => $vv['service_id'],
                                    'price' => $vv['price'],
                                    'price_curr' => $vv['price_curr'],
                                );
                            }
                            $nCountCache++;
                            $nCount++;
                            $nSummaryCount++;
                        }
                    }
                    if ($nCountCache) {
                        $aCache['cnt'] = $nCountCache;
                        $sUpdateSpecs .= ' WHEN user_id = '.$nUserID.' AND spec_id = '.$k.' THEN '.$db->str2sql(serialize($aCache));
                    }
                }
                if ($nCount) {
                    $sUpdateStat .= ' WHEN user_id = '.$nUserID.' THEN '.$nCount;
                }
            }
            if ($sUpdateSpecs) {
                $db->exec('
                    UPDATE '.TABLE_USERS_SPECIALIZATIONS.'
                    SET `services_cache` = CASE '.$sUpdateSpecs.' ELSE `services_cache` END
                    WHERE '.$db->prepareIN('user_id', $aUsers));
            }
            if ($sUpdateStat) {
                $db->exec('
                    UPDATE '.TABLE_USERS_STAT.'
                    SET `specs_services` = CASE '.$sUpdateStat.' ELSE `specs_services` END
                    WHERE '.$db->prepareIN('user_id', $aUsers));
            }
        }, 50);
        return $nSummaryCount;
    }

    /**
     * Список пользователей, предоставляющих услугу из прайса
     * @param array $filter фильтр
     * @param bool|false только подсчет кол-ва
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return array
     */
    public function priceServiceUsers(array $filter = array(), $count = false, $sqlLimit = '', $sqlOrder = '')
    {
        $from = ' FROM '.TABLE_USERS.' U 
                    , '.TABLE_USERS_TABS.' T 
                    , '.TABLE_USERS_STAT.' US
                    , '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' S ';
        $filter[':j'] = 'U.user_id = S.user_id AND U.user_id = US.user_id AND U.blocked = 0 AND U.deleted = 0';
        $filter[':tab'] = ' U.user_id = T.user_id AND T.price = 1 ';

        if (isset($filter['service_id'])) {
            $filter[':s'] = array('S.service_id = :service', ':service' => $filter['service_id']);
            unset($filter['service_id']);
        }

        # исключаем исполнителей со стоимостью услуги указанной в 0
        if (isset($filter['price'])) {
            $filter[':price'] = 'S.price > 0';
            unset($filter['price']);
        }

        $filter = $this->prepareFilter($filter, 'U');
        if ($count) {
            return $this->db->one_data('SELECT COUNT(U.user_id) '.$from.$filter['where'], $filter['bind']);
        }

        $data = $this->db->select('
            SELECT U.user_id, U.login, U.name, U.surname, U.pro, U.avatar, U.verified, U.sex,
                   U.opinions_cache, U.reg1_country, U.reg2_region, U.reg3_city, US.last_activity,
                   S.price, S.price_curr
            '.$from.$filter['where'].'
        ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
        ' . $sqlLimit, $filter['bind']);

        foreach ($data as & $v) {
            if (Geo::countrySelect()) {
                $v['country_data'] = Geo::regionData($v['reg1_country']);
            }
            if ($v['reg3_city']) {
                $v['city_data'] = Geo::regionData($v['reg3_city']);
            }
        } unset($v);

        return $data;
    }

    /**
     * Подготовка данных списка исполнителей
     * @param array $aData @ref данные о исполнителях
     */
    protected function usersListPrepare(array &$aData = array())
    {
        if (empty($aData)) return;

        $aTags = array(); # теги для пользователей в списке
        $aSpecs = array(); # основные специализации для пользователей в списке
        $aSpecsServices = array(); # специализации для услуг, попавших в кеш
        $aCats = array(); # категории специализации для пользователей в списке
        $bUseServices = Specializations::useServices();
        foreach ($aData as &$v) {
            # собираем теги
            if ($v['tags']) {
                $tags = array_unique(explode(',', $v['tags']));
                if (!empty($tags)) {
                    $v['aTags'] = $tags;
                    foreach ($tags as $t) {
                        if (!in_array($t, $aTags)) {
                            $aTags[] = $t;
                        }
                    }
                }
            }
            if (Geo::countrySelect()) {
                $v['country_data'] = Geo::regionData($v['reg1_country']);
            }
            if ($v['reg3_city']) {
                $v['city_data'] = Geo::regionData($v['reg3_city']);
            }
            # собираем основные специализации
            if ($v['main_spec_id']) {
                if( ! in_array($v['main_spec_id'], $aSpecs)){
                    $aSpecs[] = $v['main_spec_id'];
                }
            }
            # собираем специализации для услуг
            if($bUseServices && $v['services_cache']){
                if( ! in_array($v['spec_id'], $aSpecsServices)){
                    $aSpecsServices[] = $v['spec_id'];
                }
            }
            # собираем категории
            if ($v['main_cat_id']) {
                if ( ! in_array($v['main_cat_id'], $aCats)) {
                    $aCats[] = $v['main_cat_id'];
                }
            }
            if ($v['main_price_rate_text']) {
                $v['main_price_rate_text'] = func::unserialize($v['main_price_rate_text']);
            }
            if($bUseServices && $v['main_services_cache']){
                $v['main_services_cache'] = func::unserialize($v['main_services_cache']);
            }
            if($bUseServices && $v['services_cache']){
                $v['services_cache'] = func::unserialize($v['services_cache']);
            }

        } unset($v);

        if (!empty($aTags)) {
            # заполним данные для тегов
            $aTags = $this->db->select_key('SELECT id, tag FROM ' . TABLE_TAGS . ' WHERE ' . $this->db->prepareIN('id', $aTags), 'id');
            foreach ($aData as &$v) {
                if (!empty($v['aTags'])) {
                    foreach ($v['aTags'] as $i => $tag_id) {
                        if (!empty($aTags[$tag_id])) {
                            $v['aTags'][$i] = $aTags[$tag_id];
                        }
                    }
                }
            } unset($v);
        }
        if ( ! empty($aSpecs)) {
            # заполним данные для специализаций
            if($bUseServices){
                # добавим специализации для услуг, если еще нет
                $aSpecsServices = array_merge($aSpecsServices, $aSpecs);
                $aSpecsServices = array_unique($aSpecsServices);
                $aSpecsServices = Specializations::model()->specializationsServicesListingInArray($aSpecsServices);
            }
            # список необходимых специализаций
            $aSpecs = Specializations::model()->specializationsListingInArray($aSpecs);
            foreach ($aData as &$v) {
                # заполним данные для основной специализации
                if ($v['main_spec_id']) {
                    $id = $v['main_spec_id'];
                    list($v['main_spec_keyword'], $v['main_spec_title'], $v['main_spec_price_title']) = (
                        isset($aSpecs[$id]) ? array($aSpecs[$id]['keyword'], $aSpecs[$id]['title'], $aSpecs[$id]['price_title']) :
                        array('','','') );
                    if($bUseServices && ! empty($v['main_services_cache'])){
                        foreach ($v['main_services_cache'] as $k => &$vv){
                            if( ! empty($aSpecsServices[ $vv['id'] ])){
                                $vv['title'] = $aSpecsServices[ $vv['id'] ]['title'];
                                $vv['measure'] = $aSpecsServices[ $vv['id'] ]['measure'];
                                if( empty($vv['price']) && ! $aSpecsServices[ $vv['id'] ]['price_free']){
                                    unset($v['main_services_cache'][$k]);
                                    continue;
                                }
                            }
                        } unset($vv);
                    }
                }
                # заполним данные для услуг из кеша
                if($bUseServices && $v['spec_id']){
                    if( ! empty($v['services_cache'])){
                        foreach ($v['services_cache'] as &$vv){
                            if( ! empty($aSpecsServices[ $vv['id'] ])){
                                $vv['title'] = $aSpecsServices[ $vv['id'] ]['title'];
                                $vv['measure'] = $aSpecsServices[ $vv['id'] ]['measure'];
                                if( ! $vv['price']){
                                    $vv['price'] = $aSpecsServices[ $vv['id'] ]['price'];
                                    $vv['price_curr'] = $aSpecsServices[ $vv['id'] ]['price_curr'];
                                }
                            }
                        } unset($vv);
                    }
                }
            } unset($v);
        }
        if ( ! empty($aCats) && Specializations::catsOn()) {
            # заполним данные для категорий
            $aCats = Specializations::model()->categoriesListingInArray($aCats);
            foreach ($aData as &$v) {
                if ($v['main_cat_id']) {
                    $id = $v['main_cat_id'];
                    list($v['main_cat_keyword'], $v['main_cat_title']) = (
                        isset($aCats[$id]) ? array($aCats[$id]['keyword'], $aCats[$id]['title']) :
                        array('','')
                    );
                }
            } unset($v);
        }

    }

    /**
     * Добавлен ли пользователь в избранные
     * @param int $nUserID ID пользователя, проверяющего
     * @param int $nFavUserID ID пользователя
     * @return bool|mixed
     */
    public function isUserFav($nUserID, $nFavUserID)
    {
        if (!$nUserID || !$nFavUserID) return false;
        return $this->db->one_data('SELECT COUNT(*) FROM '.TABLE_USERS_FAVS.'
            WHERE user_id = :user AND fav_user_id = :fav ', array(
                ':user' => $nUserID,
                ':fav'  => $nFavUserID,
            ));
    }

    /**
     * Добавление пользователя в избранные
     * @param int $nUserID ID пользователя, кто добавляет
     * @param int $nFavUserID ID пользователя, кого добавляют
     */
    public function favAdd($nUserID, $nFavUserID)
    {
        $this->db->insert(TABLE_USERS_FAVS, array(
            'user_id'     => $nUserID,
            'fav_user_id' => $nFavUserID,
        ));

        # обновляем счетчик избранных
        $this->db->exec('
            UPDATE '.TABLE_USERS_STAT.'
            SET fav_cnt = (
                SELECT COUNT(*) FROM '.TABLE_USERS_FAVS.' WHERE fav_user_id = :user
            )
            WHERE user_id = :user', array(':user' => $nFavUserID));
    }

    /**
     * Удаление пользователя из избранных
     * @param int $nUserID ID пользователя, кто удаляет
     * @param int $nFavUserID ID пользователя, кого удаляют
     */
    public function favDel($nUserID, $nFavUserID)
    {
        $this->db->delete(TABLE_USERS_FAVS, array(
            'user_id'     => $nUserID,
            'fav_user_id' => $nFavUserID,
        ));

        # обновляем счетчик избранных
        $this->db->exec('
            UPDATE '.TABLE_USERS_STAT.'
            SET fav_cnt = (
                SELECT COUNT(*) FROM '.TABLE_USERS_FAVS.' WHERE fav_user_id = :user
            )
            WHERE user_id = :user', array(':user' => $nFavUserID));
    }

    /**
     * Список у кого пользователь в избранных
     * @param int $nUserID ID пользователя
     * @param bool $bOwner true - список просматривает владелец профиля
     * @param int $nLimit
     * @return array
     */
    public function favList($nUserID, $bOwner, $nLimit = 12)
    {
        return $this->db->select('
            SELECT U.user_id, U.name, U.surname, U.login, U.avatar, U.sex, U.verified, U.pro, U.opinions_cache '.($bOwner ? ', N.id AS note_id, N.note ' : '').'
            FROM '.TABLE_USERS_FAVS.' F, '.TABLE_USERS.' U
            '.($bOwner ? ' LEFT JOIN '.TABLE_USERS_NOTES.' N ON U.user_id = N.user_id AND N.author_id = :fav' : '').'
            WHERE F.user_id = U.user_id AND F.fav_user_id = :fav
            ORDER BY U.name, U.surname, U.login
            '.($nLimit ? $this->db->prepareLimit(0, $nLimit) : ''),
            array(':fav' => $nUserID));
    }

    /**
     * Список моих избранных (текущий авторизованный пользователь)
     * @param bool $bCount только подсчет кол-ва
     * @param integer $nLimit
     * @return array
     */
    public function favListMy($bCount = false, $nLimit = 12)
    {
        $nUserID = User::id();
        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(*) FROM '.TABLE_USERS_FAVS.'
                WHERE user_id = :user', array(':user'=>$nUserID));
        }

        return $this->db->select('
            SELECT U.user_id, U.name, U.surname, U.login, U.avatar, U.sex, U.verified, U.pro, N.id AS note_id, N.note
            FROM '.TABLE_USERS_FAVS.' F, '.TABLE_USERS.' U
                 LEFT JOIN '.TABLE_USERS_NOTES.' N ON U.user_id = N.user_id AND N.author_id = :user
            WHERE F.user_id = :user AND F.fav_user_id = U.user_id
            ORDER BY U.name, U.surname, U.login
            '.($nLimit ? $this->db->prepareLimit(0, $nLimit) : ''),
            array(':user'=>$nUserID));
    }

    /**
     * Удаляем неактивированные аккаунты с просроченным периодом активации
     */
    public function cronDeleteUnactivated()
    {
        $this->db->exec('DELETE U, US FROM ' . TABLE_USERS . ' U, ' . TABLE_USERS_STAT . ' US
                    WHERE U.activated = 0 AND U.activate_expire < :expire
                        AND U.user_id = US.user_id
                        AND US.last_login = :last
                ', array(':expire' => $this->db->now(), ':last' => '0000-00-00 00:00:00')
        );

        # формируем уведомление о скором удалении неактивированного аккаунта
        $days = config::sysAdmin('users.activation.days.notify', 0, TYPE_UINT);
        if (!$days) return;

        $expire = strtotime('+ '.$days.' days');

        $where = 'WHERE U.activated = 0 AND U.activate_expire >= :from AND U.activate_expire <= :to ';
        $bind = array(':from' => date('Y-m-d 00:00:00', $expire), ':to' => date('Y-m-d 23:59:59', $expire));
        $cnt =  $this->db->one_data('SELECT COUNT(*) FROM '.TABLE_USERS.' U '.$where, $bind);
        if (!$cnt) return;

        $aTplData = Sendmail::i()->getMailTemplate('users_activation_expire', array());
        # Формируем рассылку
        $massendID = $this->db->insert(TABLE_MASSEND, array(
            'type'     => Sendmail::TYPE_AUTO,
            'total'    => $cnt,
            'started'  => $this->db->now(),
            'settings' => serialize(array(
                    'subject'    => $aTplData['subject'],
                    'body'       => $aTplData['body'],
                    'is_html'    => $aTplData['is_html'],
                    'time_total' => 0,
                    'time_avg'   => 0
                )
            ),
        ));
        if (empty($massendID)) {
            bff::log('Ошибка инициализации рассылки');
            return;
        }

        # Формируем список получателей рассылки
        $insert = array();
        $this->db->select_iterator('SELECT user_id, activate_key FROM '.TABLE_USERS.' U '.$where, $bind,
            function($row) use( & $insert, $massendID){
                $insert[] = array(
                    'massend_id' => $massendID,
                    'user_id'    => $row['user_id'],
                    'tpl_data'   => serialize(array(
                        '{activation_link}' => Users::url('activate', array('key' => $row['activate_key']))
                    )),
                );
                if(count($insert) > 100){
                    $this->db->multiInsert(TABLE_MASSEND_RECEIVERS, $insert);
                    $insert = array();
                }
            }
        );
        if( ! empty($insert)){
            $this->db->multiInsert(TABLE_MASSEND_RECEIVERS, $insert);
        }
    }

    /**
     * Пересчет позиций пользователей в специализациях
     */
    public function cronCalcSpecsPositions()
    {
        $aFilter = array(
            'deleted'   => 0,
            'activated' => 1,
            'blocked'   => 0,
        );
        if (Users::useClient()) {
            $aFilter['type'] = Users::TYPE_WORKER;
        }
        $aFilter = $this->prepareFilter($aFilter, 'U');

        $aData = $this->db->select('
            SELECT U.user_id, U.position, GROUP_CONCAT(S.spec_id) as specs
            FROM '.TABLE_USERS.' U
                LEFT JOIN '.TABLE_USERS_SPECIALIZATIONS.' S ON U.user_id = S.user_id
            '.$aFilter['where'].'
            GROUP BY U.user_id
            ORDER BY U.pro DESC, U.rating DESC ', $aFilter['bind']);

        $aUpdateUsers = array();
        $aUpdateSpecs = array();
        $aSpecs = array();

        $nCnt = 1;
        foreach ($aData as $v) {
            if ($v['specs']) {
                $specs = explode(',', $v['specs']);
                foreach ($specs as $s) {
                    if ( ! isset($aSpecs[$s])) $aSpecs[$s] = 1;
                    $aUpdateSpecs[] = 'WHEN user_id = '.$v['user_id'].' AND spec_id = '.$s.' THEN '.$aSpecs[$s];
                    $aSpecs[$s]++;
                }
                if ($v['position'] != $nCnt) {
                    $aUpdateUsers[] = 'WHEN user_id = '.$v['user_id'].' THEN '.$nCnt;
                }
                $nCnt++;
            }
        }
        if ( ! empty($aUpdateSpecs)) {
            $aUpdateSpecs = array_chunk($aUpdateSpecs, 1000);
            foreach ($aUpdateSpecs as $v) {
                $this->db->exec('UPDATE '.TABLE_USERS_SPECIALIZATIONS.' SET position = CASE '.join(' ', $v).' ELSE position END;');
            }
        }
        if ( ! empty($aUpdateUsers)) {
            $aUpdateUsers = array_chunk($aUpdateUsers, 1000);
            foreach ($aUpdateUsers as $v) {
                $this->db->exec('UPDATE '.TABLE_USERS.' SET position = CASE '.join(' ', $v).' ELSE position END;');
            }
        }

    }

    /**
     * Пересчет прайса услуг пользователей
     */
    public function cronCalcPrice()
    {
        $aConfig = Specializations::servicesConfig();
        if ($aConfig['specs_services_price'] != Specializations::SERVICES_PRICE_USERS) return;
        if ($aConfig['specs_services_price_author'] != Specializations::SERVICES_PRICE_AUTHOR_USERS) return;
        $bMin = ($aConfig['specs_services_price_author_rows'] & Specializations::SERVICES_PRICE_AUTHOR_ROWS_MIN);
        $bMax = ($aConfig['specs_services_price_author_rows'] & Specializations::SERVICES_PRICE_AUTHOR_ROWS_MAX);

        $this->db->update(TABLE_SPECIALIZATIONS_SERVICES, array(
            'price_avg' => 0,
            'price_min' => 0,
            'price_max' => 0,
        ));

        // price_avg
        $this->db->exec('
            UPDATE '.TABLE_SPECIALIZATIONS_SERVICES.' S, (
                SELECT service_id, AVG(price_main) AS price_main
                FROM (
                    SELECT US.service_id, ROUND(US.price * C.rate) AS price_main
                    FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U, '.TABLE_CURRENCIES.' C
                    WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                      AND US.price_curr = C.id AND US.price > 0
                ) sub
                GROUP BY service_id
            ) GR, '.TABLE_CURRENCIES.' R
            SET S.price_avg = ROUND(GR.price_main / R.rate)
            WHERE S.id = GR.service_id AND S.price_curr = R.id
        ');

        // price_max - средняя из 10% наибольших
        if ($bMax) { $this->db->exec('
            UPDATE '.TABLE_SPECIALIZATIONS_SERVICES.' S, (
                SELECT service_id, AVG(price_main) AS price_main
                FROM (
                    SELECT US.service_id, ROUND(US.price * C.rate) AS price_main
                    FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U, '.TABLE_CURRENCIES.' C,
                        (
                            SELECT Ss.service_id, MIN(Ss.price_main) AS price_max
                            FROM (
                                    SELECT service_id, price_main, IF(service_id != @s, @i := 1, @i := @i + 1) AS num, IF(service_id != @s, @s := service_id, 0) AS k
                                    FROM (
                                            SELECT US.service_id, ROUND(US.price * C.rate) AS price_main
                                            FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U, '.TABLE_CURRENCIES.' C
                                            WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                                              AND US.price_curr = C.id AND US.price > 0
                                            ORDER BY US.service_id, price_main DESC
                                        ) sub, (SELECT @i := 0) Z, (SELECT @s := 0) zZ
                                ) Ss,
                                (
                                    SELECT service_id, CEIL(cnt * 0.1) AS cnt
                                    FROM (
                                        SELECT US.service_id, COUNT(*) AS cnt
                                        FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U
                                        WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                                        GROUP BY service_id
                                        ) N
                                ) nN
                            WHERE Ss.service_id = nN.service_id AND Ss.num <= nN.cnt
                            GROUP BY service_id
                        ) AS Nn
                    WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                      AND US.price_curr = C.id AND US.service_id = Nn.service_id AND ROUND(US.price * C.rate) >= Nn.price_max
                ) sub
                GROUP BY service_id
            ) GR, '.TABLE_CURRENCIES.' R
            SET S.price_max = ROUND(GR.price_main / R.rate)
            WHERE S.id = GR.service_id AND S.price_curr = R.id
        ');}

        // price_min - средняя из 10% наименьших
        if ($bMin) { $this->db->exec('
            UPDATE '.TABLE_SPECIALIZATIONS_SERVICES.' S, (
                SELECT service_id, AVG(price_main) AS price_main
                FROM (
                    SELECT US.service_id, ROUND(US.price * C.rate) AS price_main
                    FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U, '.TABLE_CURRENCIES.' C,
                        (
                            SELECT Ss.service_id, MAX(Ss.price_main) AS price_min
                            FROM (
                                    SELECT service_id, price_main, IF(service_id != @s, @i := 1, @i := @i + 1) AS num, IF(service_id != @s, @s := service_id, 0) AS k
                                    FROM (
                                            SELECT US.service_id, ROUND(US.price * C.rate) AS price_main
                                            FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U, '.TABLE_CURRENCIES.' C
                                            WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                                              AND US.price_curr = C.id AND US.price > 0
                                            ORDER BY US.service_id, price_main ASC
                                        ) sub, (SELECT @i := 0) Z, (SELECT @s := 0) zZ
                                ) Ss,
                                (
                                    SELECT service_id, CEIL(cnt * 0.1) AS cnt
                                    FROM (
                                        SELECT US.service_id, COUNT(*) AS cnt
                                        FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U
                                        WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                                        GROUP BY service_id
                                        ) N
                                ) nN
                            WHERE Ss.service_id = nN.service_id AND Ss.num <= nN.cnt
                            GROUP BY service_id
                        ) AS Nn
                    WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                      AND US.price_curr = C.id AND US.price > 0
                      AND US.service_id = Nn.service_id AND ROUND(US.price * C.rate) <= Nn.price_min
                ) sub
                GROUP BY service_id
            ) GR, '.TABLE_CURRENCIES.' R
            SET S.price_min = ROUND(GR.price_main / R.rate)
            WHERE S.id = GR.service_id AND S.price_curr = R.id
        ');}

        // прайс по регионам
        $this->db->exec('DELETE FROM '.TABLE_SPECIALIZATIONS_SERVICES_REGION);

        $aReg = array(
            'country' => array('reg1_country'),
            'region'  => array('reg2_region'),
            'city'    => array('reg3_city'),
        );
        $alias = function($data, $alias = '') {
            if($alias) $alias .= '.';
            foreach ($data as &$v){
                $v = $alias.$v;
            }unset($v);
            return join(', ', $data);
        };
        $condition = function($data, $cond, $alias = '') {
            if($alias) $alias .= '.';
            foreach ($data as &$v){
                $v = $alias.$v.$cond;
            }unset($v);
            return join(' AND ', $data);
        };
        $equal = function($data, $alias1, $alias2) {
            $alias1 .= '.';
            $alias2 .= '.';
            foreach ($data as &$v){
                $v = $alias1.$v.' = '.$alias2.$v;
            }unset($v);
            return join(' AND ', $data);
        };
        // price_avg
        foreach ($aReg as $v) {
            $this->db->exec('
                INSERT INTO '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' (service_id, '.$alias($v).', price_avg)
                SELECT service_id, '.$alias($v).', ROUND(GR.price_main / R.rate) AS price_avg
                FROM (
                    SELECT service_id, '.$alias($v).', AVG(price_main) AS price_main
                    FROM (
                        SELECT US.service_id, '.$alias($v, 'U').', ROUND(US.price * C.rate) AS price_main
                        FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U, '.TABLE_CURRENCIES.' C
                                WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                                  AND '.$condition($v, ' > 0', 'U').'
                                  AND US.price_curr = C.id AND US.price > 0
                    ) sub
                    GROUP BY service_id, '.$alias($v).'
                ) GR, '.TABLE_SPECIALIZATIONS_SERVICES.' S, '.TABLE_CURRENCIES.' R
                WHERE S.id = GR.service_id AND S.price_curr = R.id
            ');
        }

        // price_max - средняя из 10% наибольших
        if ($bMax) { foreach ($aReg as $v){
            $this->db->exec('
                UPDATE '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' SR, (
                        SELECT US.service_id, '.$alias($v, 'U').', COUNT(*) AS cnt
                        FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U
                        WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                          AND '.$condition($v, ' > 0', 'U').'
                        GROUP BY service_id, '.$alias($v).'
                    ) N
                SET SR.t = CEIL(N.cnt * 0.1)
                WHERE SR.service_id = N.service_id AND '.$equal($v, 'SR', 'N'));

            $this->db->exec('
                UPDATE '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' SR, (
                    SELECT T.service_id, '.$alias($v, 'T').', MIN(Ss.price_main) AS price_max
                    FROM (
                        SELECT service_id, '.$alias($v).', price_main,
                               IF(service_id != @s OR '.$alias($v).' != @c, @i := 1, @i := @i + 1) AS num,
                               IF(service_id != @s, @s := service_id, 0) AS s, IF('.$alias($v).' != @c, @c := '.$alias($v).', 0) AS c
                        FROM (
                                SELECT US.service_id, '.$alias($v, 'U').', ROUND(US.price * C.rate) AS price_main
                                FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U, '.TABLE_CURRENCIES.' C
                                WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                                  AND '.$condition($v, ' > 0', 'U').'
                                  AND US.price_curr = C.id AND US.price > 0
                                ORDER BY '.$alias($v, 'U').', US.service_id, price_main DESC
                            ) sub, (SELECT @i := 0) Z, (SELECT @s := 0) zZ, (SELECT @c := 0) zzZ
                        ) Ss,
                        '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' T
                        WHERE Ss.service_id = T.service_id AND Ss.num <= T.t AND '.$equal($v, 'Ss', 'T').'
                        GROUP BY service_id, '.$alias($v).'
                    ) N
                SET SR.tmp = N.price_max
                WHERE SR.service_id = N.service_id AND '.$equal($v, 'SR', 'N'));

            $this->db->exec('
                UPDATE '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' SR, (
                        SELECT service_id, '.$alias($v).', AVG(price_main) AS price_main
                        FROM (
                            SELECT US.service_id, '.$alias($v, 'U').', ROUND(US.price * C.rate) AS price_main
                            FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U, '.TABLE_CURRENCIES.' C,
                                 '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' T
                            WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                              AND '.$condition($v, ' > 0', 'U').' AND '.$equal($v, 'U', 'T').'
                              AND US.price_curr = C.id AND US.service_id = T.service_id AND ROUND(US.price * C.rate) >= T.tmp
                        ) sub
                        GROUP BY service_id, '.$alias($v).'
                    ) GR, '.TABLE_SPECIALIZATIONS_SERVICES.' S, '.TABLE_CURRENCIES.' R
                    SET SR.price_max = ROUND(GR.price_main / R.rate)
                    WHERE S.id = GR.service_id AND S.price_curr = R.id AND SR.service_id = GR.service_id AND '.$equal($v, 'SR', 'GR'));
        }}

        if ($bMax && $bMin) {
            $this->db->update(TABLE_SPECIALIZATIONS_SERVICES_REGION, array('tmp' => 0));
        }

        // price_min - средняя из 10% наименьших
        if($bMin) { foreach ($aReg as $v){
            if( ! $bMax){ $this->db->exec('
                UPDATE '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' SR, (
                        SELECT US.service_id, '.$alias($v, 'U').', COUNT(*) AS cnt
                        FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U
                        WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                          AND '.$condition($v, ' > 0', 'U').'
                        GROUP BY service_id, '.$alias($v).'
                    ) N
                SET SR.t = CEIL(N.cnt * 0.1)
                WHERE SR.service_id = N.service_id AND '.$equal($v, 'SR', 'N'));
            }

            $this->db->exec('
                UPDATE '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' SR, (
                    SELECT T.service_id, '.$alias($v, 'T').', MAX(Ss.price_main) AS price_min
                    FROM (
                        SELECT service_id, '.$alias($v).', price_main,
                               IF(service_id != @s OR '.$alias($v).' != @c, @i := 1, @i := @i + 1) AS num,
                               IF(service_id != @s, @s := service_id, 0) AS s, IF('.$alias($v).' != @c, @c := '.$alias($v).', 0) AS c
                        FROM (
                                SELECT US.service_id, '.$alias($v, 'U').', ROUND(US.price * C.rate) AS price_main
                                FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U, '.TABLE_CURRENCIES.' C
                                WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                                  AND '.$condition($v, ' > 0', 'U').'
                                  AND US.price_curr = C.id AND US.price > 0
                                ORDER BY '.$alias($v, 'U').', US.service_id, price_main ASC
                            ) sub, (SELECT @i := 0) Z, (SELECT @s := 0) zZ, (SELECT @c := 0) zzZ
                        ) Ss,
                        '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' T
                        WHERE Ss.service_id = T.service_id AND Ss.num <= T.t AND '.$equal($v, 'Ss', 'T').'
                        GROUP BY service_id, '.$alias($v).'
                    ) N
                SET SR.tmp = N.price_min
                WHERE SR.service_id = N.service_id AND '.$equal($v, 'SR', 'N'));

            $this->db->exec('
                UPDATE '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' SR, (
                        SELECT service_id, '.$alias($v).', AVG(price_main) AS price_main
                        FROM (
                            SELECT US.service_id, '.$alias($v, 'U').', ROUND(US.price * C.rate) AS price_main
                            FROM '.TABLE_USERS_SPECIALIZATIONS_SERVICES.' US, '.TABLE_USERS.' U, '.TABLE_CURRENCIES.' C,
                                 '.TABLE_SPECIALIZATIONS_SERVICES_REGION.' T
                            WHERE US.user_id = U.user_id AND U.activated = 1 AND U.blocked = 0 AND U.deleted = 0
                              AND '.$condition($v, ' > 0', 'U').' AND '.$equal($v, 'U', 'T').' AND US.price > 0
                              AND US.price_curr = C.id AND US.service_id = T.service_id AND ROUND(US.price * C.rate) <= T.tmp
                        ) sub
                        GROUP BY service_id, '.$alias($v).'
                    ) GR, '.TABLE_SPECIALIZATIONS_SERVICES.' S, '.TABLE_CURRENCIES.' R
                    SET SR.price_min = ROUND(GR.price_main / R.rate)
                    WHERE S.id = GR.service_id AND S.price_curr = R.id AND SR.service_id = GR.service_id AND '.$equal($v, 'SR', 'GR'));
        }}
    }

    /**
     * Обновление рейтинга пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nAmount на сколько изменить рейтинг
     * @param string $sReason причина изменения рейтинга
     * @param array $aReasonExtra дополнительные данные к причине
     * @return boolean
     */
    public function ratingChange($nUserID, $nAmount, $sReason = '?', array $aReasonExtra = array())
    {
        bff::hook('users.rating.change', $nUserID, $nAmount, $sReason, $aReasonExtra);
        if (!$nUserID || !$nAmount) return false;
        # Обновляем рейтинг:
        $res = $this->userSave($nUserID, array('rating = rating + :amount'), array(), array(':amount' => $nAmount));
        if (!empty($res)) {
            # Помечаем изменение рейтинга в логе:
            $aUserData = $this->userData($nUserID, array('rating','pro','blocked','activated','deleted'));
            if (empty($aUserData)) {
                bff::log('Ошибка получения данных о рейтинге пользователя #'.$nUserID);
            } else {
                $aReasonExtra['user_state'] = $aUserData;
                $aReasonExtra['rating_state_pro'] = Users::rating($aUserData['rating'], !empty($aUserData['pro']));
                $this->db->insert(TABLE_USERS_RATING, array(
                    'user_id' => $nUserID,
                    'author_id' => User::id(),
                    'author_ip' => Request::remoteAddress(true),
                    'author_ip2' => Request::remoteAddress(true, true),
                    'rating_change' => $nAmount,
                    'rating_state' => $aUserData['rating'],
                    'reason' => $sReason,
                    'reason_extra' => serialize($aReasonExtra),
                    'created' => $this->db->now(),
                ), false);
            }
        }
        return !empty($res);
    }

    /**
     * Накручиваем счетчик просмотров
     * @param integer $nUserID ID пользователя
     * @param string $sViewType тип просмотра: 'user'=>просмотр профиля
     * @param integer $nViewsToday текущий счетчик просмотров за сегодня или 0
     * @return boolean
     */
    public function userViewsIncrement($nUserID, $sViewType, $nViewsToday = 0)
    {
        if (empty($nUserID) || !in_array($sViewType, array('user'))) {
            return false;
        }
        $sDate = date('Y-m-d');
        $sField = $sViewType . '_views';

        # TABLE_USERS_VIEWS:
        # 1. пытаемся вначале обновить статистику
        # поскольку запись о статистике за сегодня уже может быть создана
        $res = $this->db->update(TABLE_USERS_VIEWS,
            array($sField . ' = ' . $sField . ' + 1'),
            array('user_id' => $nUserID, 'period' => $sDate)
        );
        # обновить не получилось
        if (empty($res) && empty($nViewsToday)) {
            # 2. начинаем подсчет статистики за сегодня
            $res = $this->db->insert(TABLE_USERS_VIEWS, array(
                'user_id' => $nUserID,
                $sField   => 1,
                'period'  => $sDate,
            ), false
            );
        }

        # TABLE_USERS_STAT:
        # 3. накручиваем счетчик просмотров ОБ/Контактов за сегодня (+ общий)
        if (!empty($res)) {
            $this->db->update(TABLE_USERS_STAT, array(
                'views_total = views_total + 1',
                'views_today = views_today + 1',
            ), array('user_id' => $nUserID)
            );
        }
        return !empty($res);
    }

    /**
     * Получаем данные о статистике просмотров профиля пользователя
     * @param integer $nUserUD ID объявления
     * @return array
     */
    public function itemViewsData($nUserUD)
    {
        $aResult = array('data' => array(), 'from' => '', 'to' => '', 'total' => 0, 'today' => 0);

        do {
            if (empty($nUserUD)) {
                break;
            }

            $aData = $this->db->select('SELECT SUM(user_views) as total, period as date
                        FROM ' . TABLE_USERS_VIEWS . '
                        WHERE user_id = :id
                        GROUP BY period
                        ORDER BY period ASC', array(':id' => $nUserUD)
            );
            if (empty($aData)) {
                break;
            }

            $aItemData = $this->userData($nUserUD, array('views_total', 'views_today'));
            if (empty($aItemData)) {
                break;
            }
            $aResult['total'] = $aItemData['views_total'];
            $aResult['today'] = $aItemData['views_today'];

            $view = current($aData);
            $aResult['from'] = $view['date']; # от
            $nFrom = strtotime($view['date']);

            $view = end($aData);
            $aResult['to'] = $view['date']; # до
            $nTo = strtotime($view['date']);

            reset($aData);

            # дополняем днями, за которые статистика отсутствует
            $nDay = 86400;
            $nTotalDays = (($nTo - $nFrom) / $nDay) + 1;
            if ($nTotalDays > sizeof($aData)) {
                $aDataFull = array();
                foreach ($aData as $v) {
                    $aDataFull[$v['date']] = $v;
                }
                $aDataResult = array();
                for ($i = $nFrom; $i <= $nTo; $i += $nDay) {
                    $sDate = date('Y-m-d', $i);
                    if (isset($aDataFull[$sDate])) {
                        $aDataResult[$sDate] = $aDataFull[$sDate];
                    } else {
                        $aDataResult[$sDate] = array('item' => 0, 'contacts' => 0, 'total' => 0, 'date' => $sDate);
                    }
                }
                unset($aDataFull);
                $aData = array_values($aDataResult);
            }

            $aResult['data'] = $aData;

        } while (false);

        return $aResult;
    }

    /**
     * Сбрасываем статистику ежедневных просмотров
     */
    public function cronResetViews()
    {
        $this->db->exec('
            UPDATE ' . TABLE_USERS_STAT . ' US
            SET views_today = 0'
        );
        $this->db->exec('DELETE FROM '.TABLE_USERS_VIEWS.' WHERE period < :date', array('date' => date('Y-m-d', strtotime('-1month'))));
    }

    /**
     * Количество пользователей ожидающих проверки
     * @return int
     */
    public function verifiedWaitingCounter()
    {
        return (int)$this->db->one_data('
            SELECT COUNT(user_id) FROM '.TABLE_USERS.'
            WHERE verified = :wait ', array(':wait' => Users::VERIFIED_STATUS_WAIT));
    }

    # ----------------------------------------------------------------
    # Услуги / Пакеты услуг

    /**
     * Данные об услугах (frontend)
     * @param integer $nTypeID ID типа Svc::type...
     * @return array
     */
    public function svcPromoteData($nTypeID)
    {
        if ($nTypeID == Svc::TYPE_SERVICE || empty($nTypeID)) {
            $aData = $this->db->select_key('SELECT id, keyword, price, settings
                    FROM ' . TABLE_SVC . ' WHERE type = :type',
                'keyword', array(':type' => Svc::TYPE_SERVICE)
            );

            if (empty($aData)) return array();

            foreach ($aData as $k => $v) {
                $sett = func::unserialize($v['settings']);
                unset($v['settings']);
                $aData[$k] = array_merge($v, $sett);
            }

            return $aData;

        } elseif ($nTypeID == Svc::TYPE_SERVICEPACK) {
            $aData = $this->db->select('SELECT id, keyword, price, settings
                                FROM ' . TABLE_SVC . ' WHERE type = :type ORDER BY num',
                array(':type' => Svc::TYPE_SERVICEPACK)
            );

            foreach ($aData as $k => $v) {
                $sett = func::unserialize($v['settings']);
                unset($v['settings']);
                # оставляем текущую локализацию
                foreach ($this->langSvcPacks as $lngK => $lngV) {
                    $sett[$lngK] = (isset($sett[$lngK][LNG]) ? $sett[$lngK][LNG] : '');
                }
                $aData[$k] = array_merge($v, $sett);
            }

            return $aData;
        }
    }

    /**
     * Данные об услугах для формы, страницы продвижения
     * @return array
     */
    public function svcData()
    {
        $aFilter = array('module' => 'users');
        $aFilter = $this->prepareFilter($aFilter, 'S');

        $aData = $this->db->select_key('SELECT S.*
                                    FROM ' . TABLE_SVC . ' S
                                    ' . $aFilter['where']
            . ' ORDER BY S.type, S.num',
            'id', $aFilter['bind']
        );
        if (empty($aData)) return array();

        $oIcon = Users::svcIcon();
        foreach ($aData as $k => &$v) {
            $v['id'] = intval($v['id']);
            $v['disabled'] = false;
            $sett = func::unserialize($v['settings']);
            unset($v['settings']);
            if (!empty($sett)) {
                $v = array_merge($sett, $v);
            }
            $v['title_view'] = (isset($v['title_view'][LNG]) ? $v['title_view'][LNG] : '');
            $v['description'] = (isset($v['description'][LNG]) ? $v['description'][LNG] : '');
            $v['description_full'] = (isset($v['description_full'][LNG]) ? $v['description_full'][LNG] : '');
            $v['icon_s'] = $oIcon->url($v['id'], $v['icon_s'], UsersSvcIcon::SMALL);
            # исключаем выключенные услуги
            if (empty($v['on'])) unset($aData[$k]);
        }
        unset($v);

        return $aData;
    }

    /**
     * Настройки услуги пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nSvcID ID услуги
     * @return mixed
     */
    public function svcUserSettings($nUserID, $nSvcID)
    {
        return $this->db->one_array('SELECT *
            FROM '.TABLE_USERS_SVC_SETTINGS.'
            WHERE user_id = :user AND svc_id = :svc',
            array(':user' => $nUserID, ':svc' => $nSvcID));
    }

    /**
     * Сохранение настроек услуг пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nSvcID ID услуги
     * @param array $aData данные
     * @return mixed
     */
    function svcUserSettingsSave($nUserID, $nSvcID, $aData)
    {
        if (empty($aData)) return false;

        $bind = array();
        $set = '';
        $insert = '';
        $insertv = '';
        foreach ($aData as $field => $val) {
            $set .= ($set ? ',' : '') . $field.'='.':'.$field;
            $insert .= ($insert ? ',' : '').$field;
            $insertv .= ($insertv ? ',' : '').':'.$field;
            $bind[':' . $field] = array($val, $this->db->type($val));
        }
        $insert .= ', user_id, svc_id';
        $insertv .= ', :user_id, :svc_id';

        $bind[':user_id'] = array($nUserID, $this->db->type($nUserID));
        $bind[':svc_id']  = array($nSvcID, $this->db->type($nSvcID));

        return $this->db->exec('INSERT INTO '.TABLE_USERS_SVC_SETTINGS.'('.$insert.') VALUES ('.$insertv.') ON DUPLICATE KEY UPDATE '.$set, $bind);
    }

    /**
     * Список пользователей, заказавших услугу карусель
     * @param integer $nLimit
     * @return array
     */
    public function svcUsersCarousel($nLimit = 10)
    {
        $aFilter[':user'] = ' C.user_id = U.user_id ';
        $aFilter[':uf'] = ' U.blocked=0 AND U.activated=1 AND U.deleted=0 ';
        $aFilter = $this->prepareFilter($aFilter, 'C');

        return $this->db->select('
            SELECT C.*, U.type, U.name, U.surname, U.avatar, U.sex, U.verified, U.pro, U.login, U.email, 
                   U.reg1_country, U.reg2_region, U.reg3_city, U.addr_addr, U.district_id,
                   U.spec_id
            FROM '.TABLE_USERS_SVC_CAROUSEL.' C,
                 '.TABLE_USERS.' U
            '.$aFilter['where'].'
            ORDER BY C.payed DESC
            LIMIT '.$nLimit, $aFilter['bind']);
    }

    /**
     * Активирование услуги карусель
     * @param int $nItemID ид элемента
     * @param $aData
     * @return int
     */
    public function svcCarouselSave($nItemID, $aData)
    {
        if ( ! $nItemID) {
            return $this->db->insert(TABLE_USERS_SVC_CAROUSEL, $aData);
        } else {
            return $this->db->update(TABLE_USERS_SVC_CAROUSEL, $aData, array('id' => $nItemID));
        }
    }

    /**
     * Удаление услуги карусель
     * @param integer $nItemID ID элемента
     */
    public function svcCarouselDelete($nItemID)
    {
        if ( ! $nItemID) return;
        $this->db->delete(TABLE_USERS_SVC_CAROUSEL, array('id' => $nItemID));
    }

    /**
     * Данные о услуге карусель
     * @param int $nItemID ид элемента
     * @return array
     */
    public function svcCarouselData($nItemID)
    {
        if ( ! $nItemID) return array();
        return $this->db->one_array('
            SELECT C.*, U.type, U.name, U.surname, U.avatar, U.sex, U.verified, U.pro, U.login, U.email
            FROM '.TABLE_USERS_SVC_CAROUSEL.' C,
                 '.TABLE_USERS.' U
            WHERE C.id = :id AND C.user_id = U.user_id', array('id' => $nItemID));
    }

    /**
     * Список пользователей, заказывших услугу лестница
     * @param array $aFilter фильтр списка
     * @param bool $bCount только кол-во
     * @param string $sqlLimit
     * @param string $sqlOrder сортировка
     * @return mixed
     */
    public function svcUsersStairs($aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '' )
    {
        $aFilter[':user'] = 'S.user_id = U.user_id ';
        $aFilter[':ustat'] = ' U.user_id = ST.user_id ';
        if( ! Specializations::catsOn()){
            $aFilter['cat_id'] = 0;
        }

        if($bCount){
            $aFilter = $this->prepareFilter($aFilter, 'S');
            return $this->db->one_data('
                SELECT COUNT(*)
                FROM '.TABLE_USERS_SVC_STAIRS.' S,
                     '.TABLE_USERS.' U,
                     '.TABLE_USERS_STAT.' ST '.
                $aFilter['where'], $aFilter['bind']);
        }

        if(empty($sqlOrder)){
            $sqlOrder = 'S.sum DESC, S.payed ASC';
        }
        $aFilter[':tab'] = 'U.user_id = T.user_id';
        $aFilter = $this->prepareFilter($aFilter, 'S');

        $aData = $this->db->select('
            SELECT S.*, 
                   U.name, 
                   U.surname, 
                   U.verified, 
                   U.pro, 
                   U.sex, 
                   U.avatar, 
                   U.login, 
                   U.email, 
                   U.portfolio_works, 
                   U.spec_id,
                   ST.portfolio, 
                   T.portfolio AS portfolio_tab  
            FROM '.TABLE_USERS_SVC_STAIRS.' S,
                 '.TABLE_USERS.' U, 
                 '.TABLE_USERS_TABS.' T,
                 '.TABLE_USERS_STAT.' ST '.
            $aFilter['where'].'
            '.( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '').'
            '.$sqlLimit, $aFilter['bind']);
        return $aData;
    }

    /**
     * Данные о услуге лестница
     * @param int $nItemID ид элемента
     * @return array
     */
    public function svcStairsData($nItemID)
    {
        if( ! $nItemID) return array();
        return $this->db->one_array('
            SELECT S.*, U.type, U.name, U.surname, U.avatar, U.sex, U.verified, U.pro, U.login, U.email
            FROM '.TABLE_USERS_SVC_STAIRS.' S,
                 '.TABLE_USERS.' U
            WHERE S.id = :id AND S.user_id = U.user_id', array('id' => $nItemID));
    }

    /**
     * Сохранение услуги лестница
     * @param integer $nStairsID ID
     * @param array $aData данные
     * @return bool|int
     */
    public function svcStairsSave($nStairsID, $aData)
    {
        if ($nStairsID) {
            return $this->db->update(TABLE_USERS_SVC_STAIRS, $aData, array('id' => $nStairsID));
        } else {
            return $this->db->insert(TABLE_USERS_SVC_STAIRS, $aData);
        }
    }

    /**
     * Удаление услуги лестница
     * @param integer $nItemID ID элемента
     */
    public function svcStairsDelete($nItemID)
    {
        if( ! $nItemID) return;
        $this->db->delete(TABLE_USERS_SVC_STAIRS, array('id' => $nItemID));
    }

    public function svcCron()
    {
        $sNow = $this->db->now();

        # Деактивируем услугу "PRO"
        $usersDeactivate = $this->usersList(array(
            'pro' => 1,
            ':expire' => array('pro_expire < :now', ':now'=>$sNow)
        ), array('user_id','session_id'), false, '', 'user_id');
        if (!empty($usersDeactivate))
        {
            # Деактивируем
            $res = $this->db->update(TABLE_USERS, array('pro'=>0), array('user_id'=>array_keys($usersDeactivate), 'pro'=>1));
            if (!empty($res)) {
                # Сформируем уведомления об окончании действия услуги "PRO"
                $this->makeEnotifyProEnded();
                $noProLimit = Users::specializationsLimit(false);
                foreach ($usersDeactivate as $v) {
                    # Деактивируем доп. специализации, если их больше допустимого кол-ва, разрешенного для не-pro
                    $this->userSpecsDisableByLimit($v['user_id'], $noProLimit);
                    # Обновляем сессию пользователя
                    $this->security->impersonalizeSession($v['session_id'], array('pro'=>0), false, false);
                }
            }
        }

        # Деактивируем услугу "Лестница"
        $this->db->delete(TABLE_USERS_SVC_STAIRS, array('expire < :expire'), array(':expire' => $sNow));

        # Сформируем уведомления о скором окончании действия услуги "PRO"
        foreach (array(5,3,1) as $days) {
            $this->makeEnotifyProEnding($days); # за X дней
        }
    }

    /**
     * Формирование рассылки для уведомлений об окончании действия услуги "PRO"
     */
    public function makeEnotifyProEnded()
    {
        $nUsersCnt = $this->db->one_data('
            SELECT COUNT(*)
            FROM '.TABLE_USERS.' U
            WHERE U.pro = 1 AND U.pro_expire <= :now AND U.blocked = 0 AND (U.enotify & :enotify != 0)
        ', array(
            ':now'     => $this->db->now(),
            ':enotify' => Users::ENOTIFY_GENERAL,
        ));
        if (!$nUsersCnt) {
            return;
        }

        $aTplData = Sendmail::i()->getMailTemplate('users_pro_ended', array(
            'services_url' => Svc::url('list'),
        ));

        $nMassendID = $this->db->insert(TABLE_MASSEND, array(
            'type'     => Sendmail::TYPE_AUTO, # Автоматическая рассылка
            'total'    => $nUsersCnt,
            'started'  => $this->db->now(),
            'settings' => serialize(array(
                    'subject'    => $aTplData['subject'],
                    'body'       => $aTplData['body'],
                    'is_html'    => $aTplData['is_html'],
                    'time_total' => 0,
                    'time_avg'   => 0
                )
            ),
        ));
        if (empty($nMassendID)) {
            bff::log('Ошибка инициализации рассылки');
            return;
        }

        $this->db->exec('
            INSERT INTO '.TABLE_MASSEND_RECEIVERS.' (massend_id, user_id)
                SELECT :massend AS massend_id, U.user_id
                FROM '.TABLE_USERS.' U
                WHERE U.pro = 1 AND U.pro_expire <= :now AND U.blocked = 0 AND (U.enotify & :enotify != 0)
            ', array(
                ':massend' => $nMassendID,
                ':now'     => $this->db->now(),
                ':enotify' => Users::ENOTIFY_GENERAL,
        ));
    }

    /**
     * Формирование рассылки для уведомлений о скором окончании действия услуги "PRO"
     * @param integer $nDays осталось дней до окончания услуги
     */
    public function makeEnotifyProEnding($nDays)
    {
        $nDate = strtotime('+'.$nDays.' days');

        $nUsersCnt = $this->db->one_data('
            SELECT COUNT(*)
            FROM '.TABLE_USERS.' U
            WHERE U.pro = 1 AND U.pro_expire >= :begin AND U.pro_expire <= :end
              AND U.blocked = 0 AND (U.enotify & :enotify != 0)
        ', array(
            ':begin'   => date('Y-m-d 00:00:00', $nDate),
            ':end'     => date('Y-m-d 23:59:59', $nDate),
            ':enotify' => Users::ENOTIFY_GENERAL,
        ));
        if ( ! $nUsersCnt) {
            return;
        }

        $aTplData = Sendmail::i()->getMailTemplate('users_pro_ending', array(
            'days' => tpl::declension($nDays, _t('', 'день;дня;дней')),
        ));

        $nMassendID = $this->db->insert(TABLE_MASSEND, array(
            'type'     => Sendmail::TYPE_AUTO, # Автоматическая рассылка
            'total'    => $nUsersCnt,
            'started'  => $this->db->now(),
            'settings' => serialize(array(
                    'subject'    => $aTplData['subject'],
                    'body'       => $aTplData['body'],
                    'is_html'    => $aTplData['is_html'],
                    'time_total' => 0,
                    'time_avg'   => 0
                )
            ),
        ));
        if (empty($nMassendID)) {
            bff::log('Ошибка инициализации рассылки');
            return;
        }

        $this->db->exec('
            INSERT INTO '.TABLE_MASSEND_RECEIVERS.' (massend_id, user_id)
                SELECT :massend AS massend_id, U.user_id
                FROM '.TABLE_USERS.' U
                WHERE U.pro = 1 AND U.pro_expire >= :begin AND U.pro_expire <= :end
                  AND U.blocked = 0 AND (U.enotify & :enotify != 0)
            ', array(
            ':massend' => $nMassendID,
            ':begin'   => date('Y-m-d 00:00:00', $nDate),
            ':end'     => date('Y-m-d 23:59:59', $nDate),
            ':enotify' => Users::ENOTIFY_GENERAL,
        ));

    }

    /**
     * Статистика
     * @return array
     */
    public function statisticData()
    {
        $data = array();

        # исполнителей / пользователей
        $filter = array(
            'blocked'   => 0,
            'deleted'   => 0,
            'activated' => 1,
        );
        if (Users::useClient()) {
            $filter['type'] = Users::TYPE_WORKER;
        }
        $filter = $this->prepareFilter($filter, 'U');
        $data['workers'] = (int)$this->db->one_data('SELECT COUNT(U.user_id) FROM '.TABLE_USERS.' U '.$filter['where'], $filter['bind']);

        return $data;
    }

}