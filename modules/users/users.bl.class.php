<?php

abstract class UsersBase_ extends UsersModule
    implements IModuleWithSvc
{
    /** @var UsersModel */
    public $model = null;

    # Типы пользователей:
    const TYPE_WORKER = 1; # исполнитель
    const TYPE_CLIENT = 2; # заказчик

    # Типы контактов:
    const CONTACT_TYPE_PHONE = 1;
    const CONTACT_TYPE_EMAIL = 2;
    const CONTACT_TYPE_SITE  = 3;
    const CONTACT_TYPE_ICQ   = 4;
    const CONTACT_TYPE_SKYPE = 5;

    # Статус исполнителя:
    const WORKER_STATUS_NONE   = 1;
    const WORKER_STATUS_FREE   = 2;
    const WORKER_STATUS_BUSY   = 3;
    const WORKER_STATUS_ABSENT = 4;

    # Флаги email-уведомлений (битовая маска):
    const ENOTIFY_NEWS         = 1; # новости сервиса
    const ENOTIFY_INTERNALMAIL = 2; # уведомления о новых сообщениях
    const ENOTIFY_ORDERS_NEW   = 4; # уведомления о новых заказах
    const ENOTIFY_GENERAL      = 8; # уведомления системы

    # ID Услуг
    const SVC_PRO       = 1; # PRO аккаунт
    const SVC_CAROUSEL  = 2; # карусель
    const SVC_STAIRS    = 4; # лестница

    const SVC_STAIRS_MAIN = -2; # услуга лестница, главная страница
    const SVC_STAIRS_USER = -1; # услуга лестница, все исполнители

    # Отображение контактов
    const CONTACTS_DISPLAY_ALL = 1;  # во всех профилях
    const CONTACTS_DISPLAY_PRO = 2;  # только в платных профилях

    # Видимость контактов
    const CONTACTS_VIEW_ALL  = 1;   # всем
    const CONTACTS_VIEW_AUTH = 2;   # авторизованным
    const CONTACTS_VIEW_PRO  = 4;   # платным

    # Верификация пользователей
    const VERIFIED_STATUS_NONE     = 0; # проверка не начата
    const VERIFIED_STATUS_WAIT     = 1; # документы загружены, ожидает решения модератора
    const VERIFIED_STATUS_DECLINED = 2; # модератор отклонил проверку
    const VERIFIED_STATUS_APPROVED = 4; # проверенный пользователь

    const VERIFIED_REASON_OTHER = -1; # другая причина отклонения

    const SCHEDULE_DAYS = [
        1 => 'Пн',
        2 => 'Вт',
        3 => 'Ср',
        4 => 'Чт',
        5 => 'Пт',
        6 => 'Сб',
        7 => 'Вс',
    ];

    const SCHEDULE_PERIODS = [
        1 => ['from' => '6:00',  'to' => '9:00'],
        2 => ['from' => '9:00',  'to' => '12:00'],
        3 => ['from' => '12:00', 'to' => '15:00'],
        4 => ['from' => '15:00', 'to' => '18:00'],
        5 => ['from' => '18:00', 'to' => '24:00'],
        6 => ['from' => '24:00', 'to' => '06:00'],
    ];

    # Роль пользователя
    const ROLE_PRIVATE = 0; # частное лицо
    const ROLE_COMPANY = 1; # компания

    # Группы дин свойств специализации пользователя
    const DP_USER_SPEC_PART_ZERO    = 0;
    const DP_USER_SPEC_PART_FIRST   = 1;
    const DP_USER_SPEC_PART_SECOND  = 2;
    const DP_USER_SPEC_PART_THIRD   = 3;
    const DP_USER_SPEC_PART_FOURTH  = 4;
    const DP_USER_SPEC_PART_FIFTH   = 5;
    const DP_USER_SPEC_PART_SIXTH   = 6;
    const DP_USER_SPEC_PART_SEVENTH = 7;
    const SCHEDULE_PART_EIGHT       = 8;
    const STATUS_TEXT_PART_NINTH    = 9;

    # Порядок групп дин свойств специализации пользователя для формы в анкете настроек
    const DP_USER_SETTINGS_QUESTIONARY_FORM_GROUPS = [
        self::DP_USER_SPEC_PART_ZERO    => '',
        self::DP_USER_SPEC_PART_FIRST   => 'Дополнительная информация',
        self::DP_USER_SPEC_PART_SECOND  => 'Личная информация',
        self::DP_USER_SPEC_PART_THIRD   => 'Образование и курсы',
        self::DP_USER_SPEC_PART_FOURTH  => 'Опыт',
        self::DP_USER_SPEC_PART_FIFTH   => '',  # навыки
        self::DP_USER_SPEC_PART_SIXTH   => '',
        self::DP_USER_SPEC_PART_SEVENTH => '',
    ];

    const DP_KEY_QUANTITY_CHILDREN = 'quantity_children';

    static $userProfileData = [];

    public function init()
    {
        parent::init();
        bff::autoloadEx(array(
            'UsersTags'             => array('app', 'modules/users/users.tags.php'),
            'UsersResumeAttachment' => array('app', 'modules/users/users.resume.attach.php'),
            'UsersVerifiedImages'   => array('app', 'modules/users/users.verified.images.php'),
            'UsersSMS'              => array('app', 'modules/users/users.sms.php'),
            'UsersSvcIcon'          => array('app', 'modules/users/users.svc.icon.php'),
        ));
    }

    /**
     * @return Users
     */
    public static function i()
    {
        return bff::module('Users');
    }

    /**
     * @return UsersModel
     */
    public static function model()
    {
        return bff::model('Users');
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # Авторизация
            case 'login':
                $url .= '/user/login' . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Выход
            case 'logout':
                $url .= '/user/logout' . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Регистрация
            case 'register':
                $url .= '/user/register' . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Авторизация
            case 'login.social':
                $url .= '/user/loginsocial/' . (!empty($opts['provider']) ? $opts['provider'] : '');
                break;
            # Профиль пользователя
            case 'profile':
                $url .= '/user/' . $opts['login'] . '/'. ( ! empty($opts['tab']) ? $opts['tab'].'/' : '');
                break;
            # Кабинет пользователя
            case 'my.profile':
                $opts['login'] = User::data('login');
                $url = static::url('profile', $opts, $dynamic);
                break;
            # Восстановление пароля
            case 'forgot':
                $url .= '/user/forgot' . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Настройки профиля
            case 'my.settings':
                $url .= '/user/settings'. (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Ссылка активации акканута
            case 'activate':
                $url .= '/user/activate' . static::urlQuery($opts);
                break;
            # Пользовательское соглашение
            case 'agreement':
                $url .= '/agreement.html';
                break;
            # Список исполнителей
            case 'list':
                $url = Geo::url($opts, $dynamic) . 'users/' . ( ! empty($opts['keyword']) ? $opts['keyword'] . '/' : '') .
                    static::urlQuery($opts, array('keyword'));
                break;
            # Прайс услуг специализаций исполнителей
            case 'price':
                $url = Geo::url($opts, $dynamic) . 'price/' . ( ! empty($opts['keyword']) ? $opts['keyword'] . '/' : '') .
                       static::urlQuery($opts, array('keyword'));
                break;
            # список исполнителей, предаставляющих услугу в прайсе
            case 'price.users':
                $url = Geo::url($opts, $dynamic) . 'price/users/' . ( ! empty($opts['keyword']) ? $opts['keyword'] . '/' : '') .
                    static::urlQuery($opts, array('keyword'));
                break;
            # Прайс услуг исполнителя
            case 'user.price':
                $url = Users::url('profile', array('login'=>$opts['login'], 'tab'=>'price'), $dynamic)
                     . static::urlQuery($opts, array('login'));
                break;
            # Прайс услуг специализаций исполнителей
            case 'my.price':
                $opts['login'] = User::data('login');
                $url = static::url('user.price', $opts, $dynamic);
                break;
            # Поиск исполнителей по категории
            case 'search-spec':
                $url = Geo::url($opts, $dynamic, false);
                if (Specializations::catsOn()) {
                    $url .= '/users/'.(isset($opts['main_cat_keyword']) ? $opts['main_cat_keyword'] : '').'/'.
                        ( ! empty($opts['main_spec_keyword']) ? $opts['main_spec_keyword'].'/' : '' );
                } else {
                    if (!empty($opts['main_spec_keyword'])) {
                        $url .= '/users/'.$opts['main_spec_keyword'].'/';
                    } else {
                        $url .= '/users/';
                    }
                }
                break;
            # Список исполнителей по специализации
            case 'list-spec':
                $url = Geo::url($opts, $dynamic).'users/'.$opts['keyword'].'/'. static::urlQuery($opts, array('keyword'));
                break;
            # Поиск исполнителей по тегу
            case 'search-tag':
                $opts['tag'] = mb_strtolower($opts['tag']).'-'.$opts['id'];
                $url = Geo::url($opts, $dynamic) . 'users/' . static::urlQuery($opts, array('id'));
                break;
            # Кабинет: Информация
            case 'my.info':
                $opts['login'] = User::data('login');
                $url = static::url('user.info', $opts, $dynamic);
                break;
            # Кабинет: Информация
            case 'my.common':
                $opts['login'] = User::data('login');
                $url = static::url('user.common', $opts, $dynamic);
                break;
            # Информация о пользователе
            case 'user.info':
                $url = static::url('profile', array('login'=>$opts['login'], 'tab'=>'info'), $dynamic)
                     . static::urlQuery($opts, array('login'));
                break;
            # Общая информация о пользователе
            case 'user.common':
                $url = static::url('profile', array('login'=>$opts['login'], 'tab'=>'common'), $dynamic)
                     . static::urlQuery($opts, array('login'));
                break;
        }
        return bff::filter('users.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Страница просмотра профиля пользователя
     * @param string $login логин пользователя
     * @return string
     */
    public static function urlProfile($login)
    {
        return static::url('profile', array('login' => $login));
    }

    /**
     * Описание seo шаблонов страниц
     * @return array
     */
    public function seoTemplates()
    {
        $profileMacros = array(
            'name'    => array('t'=>'Имя пользователя'),
            'surname' => array('t'=>'Фамилия пользователя'),
            'login'   => array('t'=>'Логин пользователя'),
            'city'    => array('t'=>'Город пользователя'),
            'country' => array('t'=>'Страна пользователя'),
        );
        $templates = array(
            'groups' => array(
                'def' => 'Общие',
                'profile' => 'Профиль пользователя',
                'price' => 'Прайс',
            ),
            'pages'  => array(
                'search-index' => array(
                    't'      => 'Список исполнителей (главная)',
                    'list'   => true,
                    'macros' => array(
                        'region' => array('t'=>'Регион поиска: город/область'),
                        'country' => array('t'=>'Страна поиска'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'search-cat' => array(
                    't'      => 'Список исполнителей (категория)',
                    'list'   => true,
                    'inherit'=> true,
                    'macros' => array(
                        'title' => array('t'=>'Название категории'),
                        'region' => array('t'=>'Регион поиска: город/область'),
                        'country' => array('t'=>'Страна поиска'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'search-spec' => array(
                    't'      => 'Список исполнителей (специализация)',
                    'list'   => true,
                    'inherit'=> true,
                    'macros' => array(
                        'title' => array('t'=>'Название специализации'),
                        'region' => array('t'=>'Регион поиска: город/область'),
                        'country' => array('t'=>'Страна поиска'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'search-keyword' => array(
                    't'      => 'Поиск исполнителей (по ключевому слову)',
                    'list'   => true,
                    'macros' => array(
                        'query' => array('t'=>'Строка запроса'),
                    ),
                ),
                'price-index' => array(
                    't'      => 'Прайс (главная)',
                    'group'  => 'price',
                    'list'   => true,
                    'macros' => array(
                        'region' => array('t'=>'Регион поиска: город/область'),
                        'country' => array('t'=>'Страна поиска'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'price-cat' => array(
                    't'      => 'Прайс (категория)',
                    'group'  => 'price',
                    'list'   => true,
                    'inherit'=> true,
                    'macros' => array(
                        'title' => array('t'=>'Название категории'),
                        'region' => array('t'=>'Регион поиска: город/область'),
                        'country' => array('t'=>'Страна поиска'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'price-spec' => array(
                    't'      => 'Прайс (специализация)',
                    'group'  => 'price',
                    'list'   => true,
                    'inherit'=> true,
                    'macros' => array(
                        'title' => array('t'=>'Название специализации'),
                        'region' => array('t'=>'Регион поиска: город/область'),
                        'country' => array('t'=>'Страна поиска'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'price-service' => array(
                    't'      => 'Прайс (просмотр услуги)',
                    'group'  => 'price',
                    'list'   => true,
                    'inherit'=> true,
                    'macros' => array(
                        'title' => array('t'=>'Название услуги'),
                        'region' => array('t'=>'Регион поиска: город/область'),
                        'country' => array('t'=>'Страна поиска'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'login' => array(
                    't'      => 'Авторизация',
                    'macros' => array()
                ),
                'register' => array(
                    't'      => 'Регистрация',
                    'macros' => array()
                ),
                'forgot' => array(
                    't'      => 'Забыли пароль',
                    'macros' => array()
                ),
                # Профиль
                'profile' => array(
                    't'      => 'Информация',
                    'group'  => 'profile',
                    'macros' => $profileMacros,
                ),
                'profile-orders' => array(
                    't'      => 'Заказы',
                    'group'  => 'profile',
                    'list'   => true,
                    'macros' => $profileMacros,
                ),
                'profile-offers' => array(
                    't'      => 'Заявки',
                    'group'  => 'profile',
                    'list'   => true,
                    'macros' => $profileMacros,
                ),
                'profile-opinions' => array(
                    't'      => 'Отзывы',
                    'group'  => 'profile',
                    'list'   => true,
                    'macros' => $profileMacros,
                ),
            ),
        );

        if (static::useClient()) {
            unset($templates['pages']['profile-offers']);
        }

        # Профиль: Вопросы
        if (bff::moduleExists('qa')) {
            $templates['pages']['profile-qa'] = array(
                't'      => 'Вопросы',
                'group'  => 'profile',
                'macros' => $profileMacros,
            );
        }

        # Профиль: Портфолио
        if (bff::moduleExists('portfolio')) {
            $templates['pages']['profile-portfolio'] = array(
                't'      => 'Портфолио: список',
                'group'  => 'profile',
                'macros' => array_merge(array(
                    'description' => array('t' => 'Описание портфолио (до 150 символов)'),
                ), $profileMacros),
            );
            $templates['pages']['profile-portfolio-view'] = array(
                't'      => 'Портфолио: работа',
                'group'  => 'profile',
                'macros' => array_merge(array(
                    'title'       => array('t' => 'Заголовок (до 50 символов)'),
                    'title.full'  => array('t' => 'Заголовок (полный)'),
                    'description' => array('t' => 'Описание (до 150 символов)'),
                ), $profileMacros),
                'fields' => array(
                    'share_title' => array(
                        't'    => 'Заголовок (поделиться в соц. сетях)',
                        'type' => 'text',
                    ),
                    'share_description' => array(
                        't'    => 'Описание (поделиться в соц. сетях)',
                        'type' => 'textarea',
                    ),
                    'share_sitename' => array(
                        't'    => 'Название сайта (поделиться в соц. сетях)',
                        'type' => 'text',
                    ),
                ),
            );
        }

        # Профиль: Магазин
        if (bff::moduleExists('shop')) {
            $templates['pages']['profile-shop'] = array(
                't'      => 'Магазин: список',
                'group'  => 'profile',
                'macros' => array_merge(array(
                    'description' => array('t' => 'Описание магазина (до 150 символов)'),
                ), $profileMacros),
            );
            $templates['pages']['profile-shop-view'] = array(
                't'      => 'Магазин: товар',
                'group'  => 'profile',
                'macros' => array_merge(array(
                    'title'       => array('t' => 'Заголовок (до 50 символов)'),
                    'title.full'  => array('t' => 'Заголовок (полный)'),
                    'description' => array('t' => 'Описание (до 150 символов)'),
                    'tags'        => array('t' => 'Теги (перечисление)'),
                ), $profileMacros),
                'fields' => array(
                    'share_title' => array(
                        't'    => 'Заголовок (поделиться в соц. сетях)',
                        'type' => 'text',
                    ),
                    'share_description' => array(
                        't'    => 'Описание (поделиться в соц. сетях)',
                        'type' => 'textarea',
                    ),
                    'share_sitename' => array(
                        't'    => 'Название сайта (поделиться в соц. сетях)',
                        'type' => 'text',
                    ),
                ),
            );
        }

        if (Specializations::useServices(Specializations::SERVICES_PRICE_USERS)) {
            $templates['pages']['profile-price'] = array(
                't'      => 'Прайс',
                'group'  => 'profile',
                'macros' => $profileMacros,
            );
        }

        # Профиль: Блог
        if (bff::moduleExists('blog')) {
            $templates['pages']['profile-blog'] = array(
                't'      => 'Блог: список',
                'group'  => 'profile',
                'list'   => true,
                'macros' => $profileMacros,
            );
            $templates['pages']['profile-blog-view'] = array(
                't'      => 'Блог: просмотр поста',
                'group'  => 'profile',
                'macros' => array_merge(array(
                    'title'       => array('t' => 'Заголовок (до 50 символов)'),
                    'title.full'  => array('t' => 'Заголовок (полный)'),
                    'description' => array('t' => 'Краткое описание (до 150 символов)'),
                    'tags'        => array('t' => 'Теги'),
                ), $profileMacros),
                'fields' => array(
                    'share_title' => array(
                        't'    => 'Заголовок (поделиться в соц. сетях)',
                        'type' => 'text',
                    ),
                    'share_description' => array(
                        't'    => 'Описание (поделиться в соц. сетях)',
                        'type' => 'textarea',
                    ),
                    'share_sitename' => array(
                        't'    => 'Название сайта (поделиться в соц. сетях)',
                        'type' => 'text',
                    ),
                ),
            );
            if ( ! Blog::tagsOn()) {
                unset($templates['pages']['profile-blog-view']['macros']['tags']);
            }
        }

        if ( ! Specializations::catsOn() ) {
            unset($templates['pages']['search-cat']);
        }
        if ( ! Geo::countrySelect()) {
            foreach ($templates['pages'] as &$v) {
                if (isset($v['macros']['country'])) {
                    unset($v['macros']['country']);
                }
            } unset($v);
        }
        if ( ! static::profileMap()) {
            foreach ($templates['pages'] as &$v) {
                if (isset($v['macros']['city'])) {
                    unset($v['macros']['city']);
                }
            } unset($v);
        }

        return $templates;
    }

    public function sendmailTemplates()
    {
        $aTemplates = array(
            'users_register' => array(
                'title'       => 'Пользователи: Уведомление о регистрации',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> после регистрации, с указаниями об активации аккаунта',
                'vars'        => array(
                    '{email}'         => 'Email',
                    '{phone}'         => 'Телефон',
                    '{password}'      => 'Пароль',
                    '{activate_link}' => 'Ссылка активации аккаунта',
                ),
                'impl'        => true,
                'priority'    => 1,
                'enotify'     => 0, # всегда
                'group'       => 'users',
            ),
            'users_register_phone' => array(
                'title'       => 'Пользователи: уведомление о регистрации (с вводом номера телефона)',
                'description' => 'Шаблон письма, отправляемого <u>пользователю</u> после успешной регистрации с подтверждением номера телефона',
                'vars'        => array(
                    '{email}'         => 'Email',
                    '{password}'      => 'Пароль',
                    '{phone}'         => 'Номер телефона',
                ),
                'impl'        => true,
                'priority'    => 1.5,
                'enotify'     => 0, # всегда
                'group'       => 'users',
            ),
            'users_register_auto' => array(
                'title'       => 'Пользователи: Уведомление об успешной регистрации (после активации заказа)',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> в случае автоматической регистрации при активации заказа',
                'vars'        => array(
                    '{name}'  => 'Имя пользователя',
                    '{email}' => 'Email',
                    '{password}' => 'Пароль'
                ),
                'impl'        => true,
                'priority'    => 2,
                'enotify'     => 0, # всегда
                'group'       => 'users',
            ),
            'users_forgot_start' => array(
                'title'       => 'Пользователи: Восстановление пароля',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> в случае запроса на восстановление пароля',
                'vars'        => array(
                    '{name}'  => 'Имя пользователя',
                    '{email}' => 'Email пользователя',
                    '{link}'  => 'Ссылка восстановления'
                ),
                'impl'        => true,
                'priority'    => 3,
                'enotify'     => 0, # всегда
                'group'       => 'users',
            ),
            'users_blocked' => array(
                'title'       => 'Пользователи: Уведомление о блокировке аккаунта',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> в случае блокировки аккаунта',
                'vars'        => array(
                    '{name}'  => 'Имя пользователя',
                    '{email}' => 'Email',
                    '{blocked_reason}' => 'Причина блокировки'
                ),
                'impl'        => true,
                'priority'    => 4,
                'enotify'     => 0, # всегда
                'group'       => 'users',
            ),
            'users_unblocked' => array(
                'title'       => 'Пользователи: Уведомление о разблокировке аккаунта',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> в случае разблокировки аккаунта',
                'vars'        => array(
                    '{name}'  => 'Имя пользователя',
                    '{email}' => 'Email'
                ),
                'impl'        => true,
                'priority'    => 5,
                'enotify'     => 0, # всегда
                'group'       => 'users',
            ),
            'users_activation_expire' => array(
                'title'       => 'Пользователи: Уведомление о скором удалении неактивированного аккаунта',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> за [days] до возможного удаления неактивированного аккаунта',
                'vars'        => array(
                    '{fio}'   => 'ФИО пользователя',
                    '{name}'  => 'Имя пользователя',
                    '{email}' => 'Email',
                    '{activation_link}' => 'Ссылка активации аккаунта',
                ),
                'impl'        => true,
                'priority'    => 6,
                'enotify'     => 0, # всегда
                'group'       => 'users',
            ),
            'users_pro_start' => array(
                'title'       => 'Услуги: Уведомление об активации услуги PRO',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при активации услуги PRO',
                'vars'        => array(
                    '{fio}'    => 'ФИО пользователя',
                    '{name}'   => 'Имя пользователя',
                    '{months}' => 'Кол-во дней действия услуги<div class="desc">1 месяц, 2 месяца</div>',
                    '{period}' => 'Период действия услуги<div class="desc">с [дата] по [дата]</div>',
                ),
                'impl'        => true,
                'priority'    => 80,
                'enotify'     => static::ENOTIFY_GENERAL,
                'group'       => 'svc',
            ),
            'users_pro_ending' => array(
                'title'       => 'Услуги: Уведомление о скором окончании действия PRO',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> для уведомления о скором окончании действия PRO',
                'vars'        => array(
                    '{fio}'   => 'ФИО пользователя',
                    '{name}'  => 'Имя пользователя',
                    '{days}'  => 'Кол-во дней до завершения действия услуги<div class="desc">1 день, 3 дня, 5 дней</div>',
                ),
                'impl'        => true,
                'priority'    => 81,
                'enotify'     => static::ENOTIFY_GENERAL,
                'group'       => 'svc',
            ),
            'users_pro_ended' => array(
                'title'       => 'Услуги: Уведомление об окончании действия PRO',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> для уведомления об окончании действия PRO',
                'vars'        => array(
                    '{fio}'   => 'ФИО пользователя',
                    '{name}'  => 'Имя пользователя',
                    '{services_url}' => 'Ссылка на страницу услуг',
                ),
                'impl'        => true,
                'priority'    => 82,
                'enotify'     => static::ENOTIFY_GENERAL,
                'group'       => 'svc',
            ),
            'users_carousel' => array(
                'title'       => 'Услуги: Уведомление об активации услуги Карусель',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при активации услуги "Карусель"',
                'vars'        => array(
                    '{fio}'   => 'ФИО пользователя',
                    '{name}'  => 'Имя пользователя',
                ),
                'impl'        => true,
                'priority'    => 83,
                'enotify'     => static::ENOTIFY_GENERAL,
                'group'       => 'svc',
            ),
            'users_stairs' => array(
                'title'       => 'Услуги: Уведомление об активации услуги Лестница',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при активации услуги "Лестница"',
                'vars'        => array(
                    '{fio}'   => 'ФИО пользователя',
                    '{name}'  => 'Имя пользователя',
                    '{section}' => 'Раздел(ы)',
                ),
                'impl'        => true,
                'priority'    => 84,
                'enotify'     => static::ENOTIFY_GENERAL,
                'group'       => 'svc',
            ),
        );
        if (static::verifiedEnabled()) {
            $aTemplates['users_verified_approved'] = array(
                'title'       => 'Пользователи: Уведомление об одобрении верификации',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при одобрении верификации модератором',
                'vars'        => array(
                    '{fio}'   => 'ФИО пользователя',
                    '{name}'  => 'Имя пользователя',
                    '{email}' => 'Email',
                ),
                'impl'        => true,
                'priority'    => 7,
                'enotify'     => 0,
                'group'       => 'users',
            );
            $aTemplates['users_verified_declined'] = array(
                'title'       => 'Пользователи: Уведомление об отклонении верификации',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при отклонении верификации модератором',
                'vars'        => array(
                    '{fio}'    => 'ФИО пользователя',
                    '{name}'   => 'Имя пользователя',
                    '{email}'  => 'Email',
                    '{reason}' => 'Причина',
                ),
                'impl'        => true,
                'priority'    => 8,
                'enotify'     => 0,
                'group'       => 'users',
            );
        }

        $activationDaysNotify = config::sysAdmin('users.activation.days.notify', 0, TYPE_UINT);
        if ($activationDaysNotify > 0) {
            $aTemplates['users_activation_expire']['description'] = str_replace('[days]', tpl::declension($activationDaysNotify, 'день;дня;дней'), $aTemplates['users_activation_expire']['description']);
        } else {
            unset($aTemplates['users_activation_expire']);
        }

        Sendmail::i()->addTemplateGroup('users', 'Пользователи', 10);

        return $aTemplates;
    }

    /**
     * Типы пользоватетей
     * @return array
     */
    public static function aTypes()
    {
        $aData = bff::filter('users.types', array(
            static::TYPE_WORKER => array(
                'id'        => static::TYPE_WORKER,
                't'         => _t('users', 'Исполнитель'),
                'genitivus' => _t('users', 'исполнителя'),
                'dativus'   => _t('users', 'исполнителю'),
                'plural'    => _t('users', 'Исполнители'),
            ),
            static::TYPE_CLIENT => array(
                'id'        => static::TYPE_CLIENT,
                't'         => _t('users', 'Заказчик'),
                'genitivus' => _t('users', 'заказчика'),
                'dativus'   => _t('users', 'заказчику'),
                'plural'    => _t('users', 'Заказчики'),
            ),
        ));
        if (!static::useClient()) {
            unset($aData[static::TYPE_CLIENT]);
        }
        reset($aData);

        return $aData;
    }

    /**
     * Типы контактов
     * @return array
     */
    public static function aContactsTypes()
    {
        return bff::filter('users.contacts.types', array(
            static::CONTACT_TYPE_PHONE => array(
                'id' => static::CONTACT_TYPE_PHONE, # ID типа контакта
                't'  => _t('users', 'Телефон'), # Название контакта
                'c'  => 'phone', # Класс иконки
            ),
            static::CONTACT_TYPE_EMAIL => array(
                'id' => static::CONTACT_TYPE_EMAIL,
                't'  => _t('users', 'E-mail'),
                'c'  => 'envelope',
            ),
            static::CONTACT_TYPE_SITE  => array(
                'id' => static::CONTACT_TYPE_SITE,
                't'  => _t('users', 'Сайт'),
                'c'  => 'globe',
            ),
            static::CONTACT_TYPE_ICQ   => array(
                'id' => static::CONTACT_TYPE_ICQ,
                't'  => _t('users', 'ICQ'),
                'c'  => 'asterisk',
            ),
            static::CONTACT_TYPE_SKYPE => array(
                'id' => static::CONTACT_TYPE_SKYPE,
                't'  => _t('users', 'Skype'),
                'c'  => 'skype',
            ),
        ));
    }

    /**
     * Варианты поля "Опыт"
     * @return array
     */
    public static function aExperience()
    {
        $options = array(0 => array('id' => 0, 't' => _t('users', 'менее 1 года')));
        $lngYears = _t('users', 'год;года;лет');
        for ($i = 1; $i < 11; $i++) {
            $options[$i] = array('id' => $i, 't' => tpl::declension($i, $lngYears));
        }
        $options[11] = array('id' => 11, 't' => _t('users', 'более 10 лет'));
        return bff::filter('users.experience', $options);
    }

    /**
     * Cтатус исполнителей
     * @return array
     */
    public static function aWorkerStatus()
    {
        return bff::filter('users.worker.status', array(
            static::WORKER_STATUS_NONE   => array(
                'id' => static::WORKER_STATUS_NONE, # ID статуса
                't'  => _t('users', 'Без статуса'), # Название статуса
                'c'  => 'label-busy', # Класс иконки
            ),
            static::WORKER_STATUS_FREE   => array(
                'id' => static::WORKER_STATUS_FREE,
                't' => _t('users', 'Свободен'),
                'c'  => 'label-success',
            ),
            static::WORKER_STATUS_BUSY   => array(
                'id' => static::WORKER_STATUS_BUSY,
                't' => _t('users', 'Занят'),
                'c'  => 'label-danger',
            ),
            static::WORKER_STATUS_ABSENT => array(
                'id' => static::WORKER_STATUS_ABSENT,
                't'  => _t('users', 'Отсутствую'),
                'c'  => 'label-warning',
            ),
        ));
    }

    /**
     * Статусы проверки пользователей
     * @return array
     */
    public static function verifiedStatuses()
    {
        return bff::filter('users.verified.statuses', array(
            static::VERIFIED_STATUS_NONE => array(
                'id'    => static::VERIFIED_STATUS_NONE,
                'i'     => 'icon-warning-sign',
                'title' => _t('users', 'Не подтвержден'),
            ),
            static::VERIFIED_STATUS_WAIT => array(
                'id'    => static::VERIFIED_STATUS_WAIT,
                'i'     => 'icon-time',
                'title' => _t('users', 'Подал заявку'),
            ),
            static::VERIFIED_STATUS_DECLINED => array(
                'id'    => static::VERIFIED_STATUS_DECLINED,
                'i'     => 'icon-ban-circle',
                'title' => _t('users', 'Отклонен'),
            ),
            static::VERIFIED_STATUS_APPROVED => array(
                'id'    => static::VERIFIED_STATUS_APPROVED,
                'i'     => 'icon-ok',
                'title' => _t('users', 'Проверенный'),
            ),
        ));
    }

    public static function verifiedReasons()
    {
        $reasons = bff::filter('users.verified.reasons', array(
            1 => array('t' => _t('users', 'Фотография не является сканом документа')),
            array('t' => _t('users', 'Фотография плохого качества')),
            array('t' => _t('users', 'ФИО автора документа не соответствует указанному в профиле')),
            array('t' => _t('users', 'Предоставьте, пожалуйста, документы подтверждающие вашу личность')),
            static::VERIFIED_REASON_OTHER => array('t' => _t('users', 'Другая причина')),
        ));
        foreach ($reasons as $k=>&$v) {
            $v['id'] = $k;
        } unset($v);
        return $reasons;
    }

    /**
     * Учитываем коеффициент умножения рейтинга
     * @param float $nRating рейтинг
     * @param bool $bPro PRO-аккаунт
     * @return float
     */
    public static function rating($nRating, $bPro)
    {
        $result = $bPro ? round(static::ratingMultiplier() * $nRating) : $nRating;
        return bff::filter('users.rating', $result, array('nRating'=>&$nRating, 'bPro'=>&$bPro));
    }

    /**
     * Использовать тип пользователей "заказчик"
     * @return bool
     */
    public static function useClient()
    {
        return config::sysAdmin('users.use.client', true, TYPE_BOOL);
    }

    /**
     * Разрешать связывание аккаутнов исполнителя и заказчика
     * @return bool
     */
    public static function accountsJoin()
    {
        return static::useClient() && config::sysAdmin('users.account.join', false, TYPE_BOOL);
    }

    /**
     * Доступно ли редактирование "пола" в профиле
     * @return bool
     */
    public static function profileSex()
    {
        return config::sysAdmin('users.profile.sex', true, TYPE_BOOL);
    }

    /**
     * Доступно ли редактирование даты рождения в профиле
     * @return bool
     */
    public static function profileBirthdate()
    {
        return config::sysAdmin('users.profile.birthdate', true, TYPE_BOOL);
    }

    /**
     * Привязка пользователей к карте
     * @return bool
     */
    public static function profileMap()
    {
        return config::sysAdmin('users.profile.map', true, TYPE_BOOL);
    }

    /**
     * Запрашивать номер телефона пользователя при регистрации
     * @return bool
     */
    public static function registerPhone()
    {
        return config::sysAdmin('users.register.phone', false, TYPE_BOOL);
    }

    /**
     * Отображать номер телефона указанный при регистрации в контактах профиля
     * @return bool
     */
    public static function registerPhoneContacts()
    {
        return config::sysAdmin('users.register.phone.contacts', false, TYPE_BOOL) && static::registerPhone();
    }

    /**
     * Требовать обязательный ввод телефона
     * @return bool
     */
    public static function profilePhoneRequired()
    {
        return config::sysAdmin('users.profile.phone.required', false, TYPE_BOOL) && ! static::registerPhone();
    }

    /**
     * Максимальное количество контактов
     * @return integer
     */
    public static function profileContactsLimit()
    {
        return config::sysAdmin('users.profile.contacts.limit', 10, TYPE_UINT);
    }

    /**
     * Включена ли услуга "аккаунт PRO"
     */
    public static function proEnabled()
    {
        $data = Svc::model()->svcData(static::SVC_PRO);
        return !empty($data['on']);
    }

    /**
     * Максимальное количество специализаций у пользователей
     * 1 - используются (только основная)
     * 2+ - основная + доп. специализации
     * @param boolean $pro для PRO-аккаунта
     * @return integer
     */
    public static function specializationsLimit($pro = false)
    {
        if($pro) return (int)config::get('users_specialization_limit_pro', 5) + 1;
        return (int)config::get('users_specialization_limit', 0) + 1;
    }

    /**
     * Множитель рейтинга для PRO-аккаунта
     * @return float
     */
    public static function ratingMultiplier()
    {
        return abs(floatval(config::sysAdmin('users.rating.multiplier', 1.2)));
    }

    /**
     * Отображать контакты пользователя
     * @param integer $userID ID пользователя (владельца контактов)
     * @param integer $userType тип пользователя (владельца контактов)
     * @param bool $userPro платный аккаунт (владельца контактов)
     * @param string $sReason @ref причина сокрытия контактов
     * @param string $sReasonClass доп. классы для блока причины
     * @return bool true - отображать, false - скрывать
     */
    public static function isContactsView($userID, $userType = Users::TYPE_WORKER, $userPro = false, &$sReason = '', $sReasonClass = '')
    {
        $sReason = '';
        $sReasonPro = '<div class="alert alert-info '.$sReasonClass.'"> '._t('users', 'Контакты видны только пользователям с <span class="c-nowrap">аккаунтом [pro]</span>', array('pro'=>'<span class="pro">pro</span>')).' </div>';
        $sReasonProOwner = '<div class="alert alert-info '.$sReasonClass.'"> '._t('users', 'Контакты отображаются только у [pro] <span class="c-nowrap">аккаунтов</span>', array('pro'=>'<span class="pro">pro</span>')).' </div>';
        $sReasonAuth = '<div class="alert alert-info '.$sReasonClass.'">' ._t('users', 'Контакты видны только авторизованным пользователям').' </div>';

        if (static::useClient() && ($userType == static::TYPE_CLIENT)) {
            $nDisplay = config::get('users_contacts_client_display', static::CONTACTS_DISPLAY_ALL);
            $nView = config::get('users_contacts_client_view', static::CONTACTS_VIEW_ALL);
        } else {
            $nDisplay = config::get('users_contacts_worker_display', static::CONTACTS_DISPLAY_ALL);
            $nView = config::get('users_contacts_worker_view', static::CONTACTS_VIEW_ALL);
        }

        # Собственные контакты видны всегда
        if ($userID && User::isCurrent($userID)) {
            return true;
        }

        # Отображать:
        if ($nDisplay == static::CONTACTS_DISPLAY_PRO) {
            if ( ! $userPro) {
                if (User::isCurrent($userID) || User::id() || $nView == static::CONTACTS_VIEW_ALL) {
                    $sReason = $sReasonProOwner;
                } else {
                    switch ($nView) {
                        case static::CONTACTS_VIEW_AUTH: # если видны авторизованным
                            $sReason = $sReasonAuth;
                            break;
                        case static::CONTACTS_VIEW_PRO: # если видны платным аккаунтам
                            $sReason = $sReasonPro;
                            break;
                    }
                }
                return false;
            }
        }

        # Если отображать, кому видны:
        switch ($nView) {
            case static::CONTACTS_VIEW_ALL: # всем
                return true;
            case static::CONTACTS_VIEW_AUTH: # авторизованным
                if (User::id()) return true;
                $sReason = $sReasonAuth;
                return false;
            case static::CONTACTS_VIEW_PRO: # платным аккаунтам
                if (User::id() && User::isPro()) return true;
                $sReason = $sReasonPro;
                return false;
        }

        return true;
    }

    /**
     * Сколько верхних отмеченных услуг в специализации выводить в списке пользователей
     * @return int
     */
    public static function searchServicesLimit()
    {
        return config::sysAdmin('users.search.services.limit', 3, TYPE_UINT);
    }

    /**
     * Склеивать теги в "еще" когда их кол-во превышает допустимый лимит
     * @return integer
     */
    public static function searchTagsLimit()
    {
        return config::sysAdmin('users.search.tags.limit', 0, TYPE_UINT);
    }

    /**
     * Включена ли функциональность "Проверенный пользователь"
     * @return bool
     */
    public static function verifiedEnabled()
    {
        return config::sysAdmin('users.verified', false, TYPE_BOOL);
    }

    /**
     * Максимальное количество загружаемых документов для проверки пользователя
     * @return bool
     */
    public static function verifiedFilesLimit()
    {
        return config::sysAdmin('users.verified.files.limit', 5, TYPE_UINT);
    }

    /**
     * Включени ли роли пользователей (компания/частное лицо)
     * @return bool
     */
    public static function rolesEnabled()
    {
        return config::sysAdmin('users.roles', false, TYPE_BOOL);
    }

    /**
     * Список доступных ролей пользователей
     * @return array
     */
    public static function roles()
    {
        return bff::filter('users.roles', array(
            static::ROLE_PRIVATE => array('id' => static::ROLE_PRIVATE, 't' => _t('users', 'Частное лицо'), 'default' => true),
            static::ROLE_COMPANY => array('id' => static::ROLE_COMPANY, 't' => _t('users', 'Компания'), 'default' => false),
        ));
    }

    /**
     * Выполняем авторизацию пользователя
     * @param mixed $mLogin login|email|user_id пользователя
     * @param string|bool $sField поле для первого параметра - login, email, user_id
     * @param string $sPassword пароль (исходный или hash)
     * @param bool $bPasswordHashed пароль hash формате
     * @param bool $bSetErrors сохранять ошибки
     * @return mixed:
     *  0 - ошибка в логине/пароле,
     *  1 - неактивирован,
     *  true - успешная авторизация,
     *  array('reason'=>'причина блокировки')
     */
    public function userAuth($mLogin, $sField, $sPassword, $bPasswordHashed = true, $bSetErrors = false)
    {
        $aUserFilter = array('member' => 1);
        if (empty($sField) || !in_array($sField, array('user_id', 'email', 'login'))) {
            $sField = 'login';
        }
        $aUserFilter[$sField] = $mLogin;
        if ($bPasswordHashed) {
            $aUserFilter['password'] = $sPassword;
        }

        $aData = $this->model->userSessionData($aUserFilter);

        if (!$bPasswordHashed && !empty($aData)) {
            if ($aData['password'] != $this->security->getUserPasswordMD5($sPassword, $aData['password_salt'])) {
                $aData = false;
            }
        }

        if (!$aData) {
            # пользователя с таким логином и паролем не существует
            if ($bSetErrors) {
                if ($sField == 'email') {
                    $this->errors->set(_t('users', 'E-mail или пароль указаны некорректно'));
                } else {
                    $this->errors->set(_t('users', 'Логин или пароль указаны некорректно'));
                }
            }

            return 0;
        } else {
            if ($aData['blocked'] == 1) {
                # аккаунт заблокирован
                if ($bSetErrors) {
                    $this->errors->set(_t('users', 'Аккаунт заблокирован по причине: [reason]', array('reason' => '<br />'.nl2br($aData['blocked_reason']))));
                }

                return array('reason' => $aData['blocked_reason']);
            } else {
                if ($aData['activated'] == 0) {
                    # аккаунт неактивирован
                    config::set('__users_preactivate_data', $aData);

                    return 1;
                }
            }

            if ($this->security->isCurrentUser($aData['id'])) {
                # текущий пользователь уже авторизован
                # под аккаунтом под которым необходимо произвести авторизацию
                return true;
            }

            $nUserID = (integer)$aData['id'];

            # стартуем сессию пользователя
            $this->security->sessionStart();

            # обновляем статистику
            $this->model->userSave($nUserID, false, array(
                    'last_login2'   => $aData['last_login'],
                    'last_login'    => $this->db->now(),
                    'last_login_ip' => Request::remoteAddress(true),
                    'last_activity' => $this->db->now(),
                    'session_id'    => session_id()
                )
            );

            if (!empty($this->model->userStatCounters)) {
                $this->security->userCounter(null); # сохраняем счетчики в сессии
            }

            # сохраняем данные пользователя в сессию
            $this->security->setUserInfo($nUserID, array(USERS_GROUPS_MEMBER), $aData);

            # обновим рейтинг "за заход в день"
            $ratingLogin = config::sysAdmin('users.rating.login', 1, TYPE_UINT);
            if ($ratingLogin > 0 && ( ! static::useClient() || (static::useClient() && User::isWorker()))) {
                if (empty($aData['last_login']) || date('Y-m-d', strtotime($aData['last_login'])) != date('Y-m-d')) {
                    if (bff::$event !== 'login_admin') {
                        $this->model->ratingChange($nUserID, $ratingLogin, 'users-login', array('date'=>date('Y-m-d')));
                        $this->model->userSave($nUserID, array(), array('logined_counter = logined_counter + 1'));
                    }
                }
            }

            return true;
        }
    }

    /**
     * Инициализация компонента работы с вложениями для резюме
     * @return UsersResumeAttachment
     */
    public function resumeAttach()
    {
        static $i;
        if (!isset($i)) {
            # до 5 МБ
            $i = new UsersResumeAttachment(bff::path('resume'), 5242880);
        }

        return $i;
    }

    /**
     * Валидация данных пользователя
     * @param array $aData @ref данные
     * @param array|boolean $mKeys список ключей требующих валидации данных или TRUE - все
     * @param array $aExtraSettings дополнительные параметры валидации
     */
    public function cleanUserData(array &$aData, $mKeys = true, array $aExtraSettings = array())
    {
        if (!is_array($mKeys)) {
            $mKeys = array_keys($aData);
        }

        foreach ($mKeys as $key) {
            if (!isset($aData[$key])) {
                continue;
            }
            if (\bff::hooksAdded('users.clean.data.'.$key)) {
                $aData[$key] = \bff::filter('users.clean.data.'.$key, $aData[$key], array('data'=>&$aData, 'extraSettings'=>$aExtraSettings));
                continue;
            }
            switch ($key) {
                case 'name': # имя
                case 'surname': # имя
                {
                    # допустимые символы:
                    # латиница, кирилица, тире, пробелы
                    $aData[$key] = preg_replace('/[^\.\s\[\]\-\_\p{L}0-9\w\’\']+/iu', '', $aData[$key]);
                    $aData[$key] = trim(mb_substr($aData[$key], 0, 75), '- ');
                }
                break;
                case 'birthdate': # дата рождения
                {
                    if (!static::profileBirthdate()) {
                        break;
                    }

                    if (!empty($aData[$key]) && is_array($aData[$key]) && array_sum($aData[$key]) > 0) {
                        if ( ! checkdate($aData[$key]['month'], $aData[$key]['day'], $aData[$key]['year'])) {
                            $this->errors->set(_t('users', 'Дата рождения указана некорректно'));
                        }
                        $aData[$key] = join('-', array($aData[$key]['year'], $aData[$key]['month'], $aData[$key]['day']));
                    } else {
                        $aData[$key] = '0000-00-00';
                    }
                }
                break;
                case 'sex': #пол
                {
                    if (!static::profileSex()) {
                        break;
                    }
                    if(empty($aData[$key])){
                        break;
                    }
                    # пол
                    if (!in_array($aData[$key], array(self::SEX_MALE, self::SEX_FEMALE))) {
                        $aData[$key] = self::SEX_MALE;
                    }

                }
                break;
                case 'about': # о себе
                {
                    $aData[$key] = mb_substr($aData[$key], 0, 2500);
                }
                break;
                case 'reg3_city':
                {
                    if ($aData[$key] > 0) {
                        # проверяем корректность указанного города
                        if (!Geo::isCity($aData[$key])) {
                            $aData[$key] = 0;
                            $aData['reg1_country'] = 0;
                            $aData['reg2_region'] = 0;
                            $aData['metro_id'] = 0;
                        } else {
                            $aRegion = Geo::model()->regionParents($aData[$key]);
                            foreach ($aRegion['db'] as $k => $v) {
                                $aData[$k] = $v;
                            }
                            if (!Geo::hasMetro($aData[$key])) {
                                $aData['metro_id'] = 0;
                            }
                            # проверяем корректность указанного района города
                            Geo::i()->validateDistrictID($aData['district_id'], $aData[$key]);
                        }
                    }
                }
                break;
                case 'contacts':
                {
                    $aContacts = array();
                    if (is_string($aData[$key])) {
                        $aData[$key] = func::unserialize($aData[$key]);
                    }
                    foreach ($aData[$key] as $v) {
                        $this->input->clean_array($v, array(
                                't' => TYPE_UINT,   # тип
                                'v' => TYPE_NOTAGS, # значение
                            )
                        );
                        if (!array_key_exists($v['t'], static::aContactsTypes())) {
                            continue;
                        }

                        switch ($v['t']) {
                            case static::CONTACT_TYPE_SITE: # сайт
                            {
                                $v['v'] = mb_substr($v['v'], 0, 255);
                                if (mb_stripos($v['v'], 'http') !== 0) {
                                    $v['v'] = 'http://' . $v['v'];
                                }
                            }
                            break;
                            case static::CONTACT_TYPE_PHONE: # телефон
                            {
                                $v['v'] = preg_replace('/[^\s\+\-0-9]/', '', $v['v']);
                                $v['v'] = preg_replace('/\s+/', ' ', $v['v']);
                                $v['v'] = trim($v['v'], '- ');
                                if (strlen($v['v']) > 4) {
                                    $v['v'] = mb_substr($v['v'], 0, 20);
                                    $v['v'] = trim($v['v'], '- ');
                                    $v['v'] = (strpos($v['v'], '+') === 0 ? '+' : '') . str_replace('+', '', $v['v']);
                                } else {
                                    $v['v'] = false;
                                }
                            }
                            break;
                            case static::CONTACT_TYPE_SKYPE: # skype
                            {
                                $v['v'] = preg_replace('/[^\.\s\[\]\:\-\_a-zA-Z0-9]/', '', $v['v']);
                                $v['v'] = trim(mb_substr($v['v'], 0, 32), ' ');
                            }
                            break;
                            case static::CONTACT_TYPE_ICQ: # icq
                            {
                                $v['v'] = preg_replace('/[^\-\s0-9]/', '', $v['v']);
                                $v['v'] = trim($v['v'], ' .-');
                            }
                            break;
                            case static::CONTACT_TYPE_EMAIL: #email
                            {
                                if (!$this->input->isEmail($v['v'])) {
                                    $v['v'] = false;
                                }
                            }
                            break;
                            default:
                                continue;
                        }
                        if (static::profileContactsLimit() && count($aContacts) > static::profileContactsLimit()) {
                            break;
                        }
                        if (!empty($v['v'])) {
                            $aContacts[] = $v;
                        }
                    }
                    $aData[$key] = serialize($aContacts);
                }
                break;
                case 'experience':
                {
                    $aExperience = Users::aExperience();
                    if( ! array_key_exists($aData['experience'], $aExperience)){
                        unset($aData['experience']);
                    }
                }break;
            }
        }
    }

    /**
     * Валидация специализаций пользователя
     * @param array $aData @ref данные пользователя с ключем 'spec' - настройки специализаций
     * @param int $nTypeID тип пользователя
     * @param boolean $bPro пользователь с активированным аккаунтом "PRO"
     */
    public function cleanSpecializations(array & $aData, $nTypeID, $bPro)
    {
        if (!isset($aData['spec'])) {
            return;
        }
        if ($nTypeID != static::TYPE_WORKER) {
            unset($aData['spec']);
            return;
        }
        $nSpecLimit = static::specializationsLimit($bPro);
        if ( ! $nSpecLimit) {
            unset($aData['spec']);
            return;
        }

        $aSpecs = array();
        $aCats = array();
        foreach ($aData['spec'] as $v) {
            if (!isset($v['spec'])) {
                continue;
            }

            $nSpecID = $v['spec'];
            unset($v['spec']);
            if (!empty($v)) {
                $nCatID = end($v);
            } else {
                $nCatID = 0;
            }
            $aSpecs[] = array('cat_id'=>$nCatID, 'spec_id'=>$nSpecID, 'disabled'=>!empty($v['disabled']));
            if ($nCatID && !in_array($nCatID, $aCats)) {
                $aCats[] = $nCatID;
            }
        }
        unset($aData['spec']);

        $aAllowCats = Specializations::model()->specializationsInCategories($aCats, array());
        foreach ($aSpecs as $k => $v) {
            if (!isset($aAllowCats[$v['cat_id']][$v['spec_id']])) {
                unset($aSpecs[$k]);
            }
        }
        if (empty($aSpecs)) {
            return;
        }
        $aMain = reset($aSpecs);
        $aData['cat_id'] = $aMain['cat_id'];
        $aData['spec_id'] = $aMain['spec_id'];
        $nNum = 1; $nCnt = 1;
        foreach ($aSpecs as $k =>&$v) {
            if ($nCnt > 1 && $aMain['spec_id'] == $v['spec_id']) {
                unset($aSpecs[$k]); # пропускаем доп. специализации идентичные основной
                continue;
            }
            if ($nCnt > $nSpecLimit && !$v['disabled']) {
                unset($aSpecs[$k]); # отрезаем доп. специализации превышающие допустимый лимит
                continue;
            }
            $v['main'] = ($nCnt == 1 ? 1 : 0);
            $v['num'] = $nNum++;
            $v['disabled'] = ($v['disabled']? 1 : 0);
            if (!$v['disabled']) {
                $nCnt++;
            }
            # дин. свойства
            $aDataDP = Specializations::i()->dpSave($v['spec_id'], 'd'.$v['spec_id']);
            $v = array_merge($v, $aDataDP);
            # настройки цен
            $aDataPR = $this->priceSave($v['spec_id'], 'pr');
            $v = array_merge($v, $aDataPR);
        } unset($v);

        $aData['specs'] = $aSpecs;
    }

    /**
     * Валидация услуг специализаций пользователя
     * @param array $aData @ref данные
     *  'specs' - специализации
     *  'specsServices' - услуги специализаций
     */
    public function cleanSpecializationsServices(array &$aData)
    {
        if (empty($aData['specs']) || !Specializations::useServices(Specializations::SERVICES_PRICE_USERS)) {
            if (isset($aData['specsServices'])) unset($aData['specsServices']);
            return;
        }

        # Получаем настройки валют сайта
        $aCurrencies = Site::model()->currencyData(false);

        # Исходим из настроек специализаций пользователя
        $aServices = array();
        foreach ($aData['specs'] as &$v)
        {
            $specID = $v['spec_id'];
            if (empty($aData['specsServices'][ $specID ])) continue;

            # Получаем настройки услуг специализации
            $aDefault = Specializations::model()->specializationDataServices($specID);
            foreach ($aDefault as &$vv)
            {
                $userService = (!empty($aData['specsServices'][ $specID ][ $vv['id'] ]) ? $aData['specsServices'][ $specID ][ $vv['id'] ] : false);
                if (!$userService || empty($userService['id'])) continue;

                $aService = array(
                    'spec_id'    => $specID,
                    'service_id' => $vv['id'],
                    'price'      => $vv['price'],
                    'price_curr' => $vv['price_curr'],
                    'disabled'   => $v['disabled'],
                );
                if ( isset($userService['price'])) {
                    $aService['price'] = $this->input->clean($userService['price'], TYPE_PRICE);
                }
                if ( ! empty($userService['price_curr'])) {
                    $aService['price_curr'] = $this->input->clean($userService['price_curr'], TYPE_UINT);
                    if ( ! isset($aCurrencies[ $aService['price_curr'] ])) {
                        $aService['price_curr'] = 0;
                        $aService['price'] = 0;
                    }
                }
                $aServices[] = $aService;
            } unset($vv);
        } unset($v);

        $aData['specsServices'] = $aServices;
    }

    /**
     * Иницилизация компонента работы с соц. аккаунтами
     * @return UsersSocial
     */
    public function social()
    {
        static $i;
        if (!isset($i)) {
            $i = new UsersSocial();
        }

        return $i;
    }

    /**
     * SMS шлюз
     * @param boolean $userErrors фиксировать ошибки для пользователей
     * @return UsersSMS
     */
    public function sms($userErrors = true)
    {
        static $i;
        if (!isset($i)) {
            $i = new UsersSMS();
        }
        $i->userErrorsEnabled($userErrors);

        return $i;
    }
    
    /**
     * Регистрация пользователя
     * @param array $aData данные
     * @param bool $bAuth авторизовать в случае успешной регистрации
     * @return array|bool
     *  false - ошибка регистрации
     *  array - данные о вновь созданном пользователе (user_id, password, activate_link)
     */
    public function userRegister(array $aData, $bAuth = false)
    {
        # тип пользователя
        $aTypes = static::aTypes();
        if (!isset($aData['type']) || !array_key_exists($aData['type'], $aTypes)) {
            $aData['type'] = key($aTypes);
        }

        # статус исполнителя
        if (empty($aData['status'])) {
            $aData['status'] = self::WORKER_STATUS_NONE;
        }

        # генерируем логин на основе email-адреса
        if (isset($aData['email'])) {
            $login = mb_substr($aData['email'], 0, mb_strpos($aData['email'], '@'));
            $login = preg_replace('/[^a-z0-9\_]/ui', '', $login);
            $login = mb_strtolower(trim($login, '_ '));
            if (mb_strlen($login) >= $this->loginMinLength) {
                if (mb_strlen($login) > $this->loginMaxLength) {
                    $login = mb_substr($login, 0, $this->loginMaxLength);
                }
                $aData['login'] = $this->model->userLoginGenerate($login, true);
            } else {
                $aData['login'] = $this->model->userLoginGenerate();
            }
        }

        # генерируем пароль или используем переданный
        $sPassword = (isset($aData['password']) ? $aData['password'] : func::generator(10));

        # подготавливаем данные
        $this->cleanUserData($aData);
        $aData['password_salt'] = $this->security->generatePasswordSalt();
        $aData['password'] = $this->security->getUserPasswordMD5($sPassword, $aData['password_salt']);

        # данные необходимые для активации аккаунта
        $aActivation = $this->getActivationInfo();
        $aData['activated'] = 0;
        $aData['activate_key'] = $aActivation['key'];
        $aData['activate_expire'] = $aActivation['expire'];

        # по-умолчанию подписываем на все типы email-уведомлений
        $aData['enotify'] = $this->getEnotifyTypes(0, true);

        # создаем аккаунт
        $nUserID = $this->model->userCreate($aData, self::GROUPID_MEMBER);
        if (!$nUserID) {
            return false;
        }

        if ($bAuth) {
            # авторизуем
            $this->userAuth($nUserID, 'user_id', $aData['password']);
        }

        return array(
            'user_id'       => $nUserID,
            'password'      => $sPassword,
            'activate_key'  => $aActivation['key'],
            'activate_link' => $aActivation['link'],
        );
    }

    /**
     * Формируем ключ активации
     * @param array $opts дополнительные параметры ссылки активации
     * @param string $key ключ активации (если был сгенерирован ранее)
     * @return array (
     *  'key'=>ключ активации,
     *  'link'=>ссылка для активации,
     *  'expire'=>дата истечения срока действия ключа
     *  )
     */
    public function getActivationInfo(array $opts = array(), $key = '')
    {
        $aData = array();
        if (empty($key)) {
            $shortCode = static::registerPhone();
            if ($shortCode) {
                # В случае регистрации через телефон генерируем короткий ключ активации
                # Кроме ситуации с регистрацией через соц. сеть
                $key = mb_strtolower(func::generator(5, false));
            } else {
                $key = md5(substr(md5(uniqid(mt_rand() . SITEHOST . '^*RD%S&()%$#', true)), 0, 10) . BFF_NOW);
            }
        }
        $aData['key'] = $opts['key'] = $key;
        $aData['link'] = static::url('activate', $opts);
        $days = config::sysAdmin('users.activation.days', 7, TYPE_UINT);
        if($days > 365 || $days <= 0){
            $days = 365;
        }
        $aData['expire'] = date('Y-m-d H:i:s', strtotime('+'.$days.' days'));

        return $aData;
    }

    /**
     * ОБновляем ключ активации пользователя
     * @param integer $userID ID пользователя
     * @param string $currentKey ключ активации (если был сгенерирован ранее)
     * @return array|false
     */
    public function updateActivationKey($userID, $currentKey = '')
    {
        if (empty($userID) || $userID <0) return false;

        $activationData = $this->getActivationInfo(array(), $currentKey);
        $res = $this->model->userSave($userID, array(
            'activate_key'    => $activationData['key'],
            'activate_expire' => $activationData['expire'],
        ));
        if (!$res) {
            bff::log('Ошибка сохранения данных пользователя #'.$userID.' [users::updateActivationKey]');
            return false;
        }

        return $activationData;
    }

    /**
     * Получаем доступные варианты email-уведомлений
     * @param int $nSettings текущие активные настройки пользователя (битовое поле)
     * @param bool $bAllCheckedSettings получить битовое поле всех активированных настроек
     * @return array|int|number
     */
    public function getEnotifyTypes($nSettings = 0, $bAllCheckedSettings = false)
    {
        $aTypes = bff::filter('users.enotify.types',array(
            static::ENOTIFY_NEWS         => array(
                'title' => _t('users', 'Получать рассылку о новостях [site_title]', array('site_title' => Site::title('users.enotify.news'))),
                'a'     => 0
            ),
            static::ENOTIFY_INTERNALMAIL => array(
                'title' => _t('users', 'Получать уведомления о новых сообщениях'),
                'a'     => 0
            ),
            static::ENOTIFY_ORDERS_NEW => array(
                'title' => _t('users', 'Получать уведомления о новых заказах'),
                'a'     => 0
            ),
            static::ENOTIFY_GENERAL => array(
                'title' => _t('users', 'Уведомления сервиса'),
                'a'     => 0
            ),
        ));
        if( ! static::useClient() || (static::useClient() && User::isWorker()) || $bAllCheckedSettings){
            $aTypes[static::ENOTIFY_ORDERS_NEW] = array(
                'title' => _t('users', 'Получать уведомления о новых заказах'),
                'a'     => 0
            );
        }
        $aTypes = bff::filter('users.enotify.types.result', $aTypes, array('nSettings'=>&$nSettings, 'bAllCheckedSettings'=>&$bAllCheckedSettings));

        if ($bAllCheckedSettings) {
            return (!empty($aTypes) ? array_sum(array_keys($aTypes)) : 0);
        }

        if (!empty($nSettings)) {
            foreach ($aTypes as $k => $v) {
                if ($nSettings & $k) {
                    $aTypes[$k]['a'] = 1;
                }
            }
        }

        return $aTypes;
    }

    /**
     * Получаем доступные варианты настроек закладок профиля
     * @param mixed $mSettings ID пользователя или текущие активные настройки пользователя
     * @param int $nType тип пользователя
     * @return array
     */
    public function getProfileTabs($mSettings = false, $nType = 0)
    {
        if (empty($mSettings)) $mSettings = array();
        if (is_numeric($mSettings)){
            $mSettings = $this->model->userProfileTabs($mSettings);
        }
        $aSettings = array(
            'orders' => array(
                'title' => _t('orders', 'Orders'),
                'a'     => 0,
            ),
            'shop' => array(
                'title' => _t('shop', 'Shop'),
                'a'     => 0,
            ),
            'price' => array(
                'title' => _t('users', 'Прайс'),
                'a'     => 0,
            ),
            'qa' => array(
                'title' => _t('users', 'Вопрос-ответ'),
                'a'     => 0,
            ),
            'blog' => array(
                'title' => _t('blog', 'Blog'),
                'a'     => 0,
            ),
            'portfolio' => array(
                'title' => _t('portfolio', 'Portfolio'),
                'a'     => 0,
            ),
        );
        if (bff::moduleExists('shop') && ! Shop::enabled()) {
            $aSettings['shop']['hidden'] = true;
        }
        if (bff::moduleExists('qa') && ! Qa::enabled()) {
            $aSettings['qa']['hidden'] = true;
        }
        if (bff::moduleExists('blog') && ! Blog::enabled()) {
            $aSettings['blog']['hidden'] = true;
        }
        if ( ! Specializations::useServices(Specializations::SERVICES_PRICE_USERS)) {
            $aSettings['price']['hidden'] = true;
        }

        if (static::useClient()) {
            if ($nType == Users::TYPE_WORKER) {
                unset($aSettings['orders']);
            }
            else if ($nType == Users::TYPE_CLIENT) {
                unset($aSettings['shop'], $aSettings['portfolio'], $aSettings['price']);
            }
        }

        foreach ($aSettings as $k => $v) {
            $aSettings[$k]['a'] = ! empty($mSettings[$k]) ? 1 : 0;
        }
        return bff::filter('users.profile.tabs.available', $aSettings, $mSettings, $nType);
    }

    /**
     * Формирование контакта пользователя в виде изображения
     * @param string|array $text текст контакта
     * @return string base64
     */
    public static function contactAsImage($text)
    {
        if (is_array($text) && sizeof($text) == 1) {
            $text = reset($text);
        }

        # Указываем шрифт
        $font = PATH_CORE . 'fonts' . DS . 'tahoma.bold.ttf';
        $fontSize = 10;
        $fontAngle = 0;

        # Определяем необходимые размера изображения
        if (is_array($text)) {
            $textMulti = join("\n", $text);
            $textDimm = imagettfbbox($fontSize, $fontAngle, $font, $textMulti);
        } else {
            $textDimm = imagettfbbox($fontSize, $fontAngle, $font, $text);
        }
        if ($textDimm === false) {
            return '';
        }
        $width = ($textDimm[4] - $textDimm[6]) + 2;
        $height = ($textDimm[1] - $textDimm[7]) + 2;

        # Создаем холст
        $image = imagecreatetruecolor($width, $height);

        # Формируем прозрачный фон
        imagealphablending($image, false);
        $transparentColor = imagecolorallocatealpha($image, 0, 0, 0, 127);
        imagefill($image, 0, 0, $transparentColor);
        imagesavealpha($image, true);

        # Пишем текст
        $textColor = imagecolorallocate($image, 0x33, 0x33, 0x33); # цвет текста
        if (is_array($text)) {
            $i = 0;
            foreach ($text as $v) {
                $y = ($i++ * $fontSize) + (5 * $i) + 10;
                imagettftext($image, $fontSize, $fontAngle, 0, $y, $textColor, $font, $v);
            }
        } else {
            imagettftext($image, $fontSize, $fontAngle, 0, $height - 2, $textColor, $font, $text);
        }

        # Формируем base64 версию изображения
        ob_start();
        imagepng($image);
        imagedestroy($image);
        $data = ob_get_clean();
        $data = 'data:image/png;base64,' . base64_encode($data);

        return $data;
    }

    /**
     * Валидация номеров телефонов
     * @param array $aPhones номера телефонов
     * @param int $nLimit лимит
     * @return array
     */
    public static function validatePhones(array $aPhones = array(), $nLimit = 0)
    {
        $aResult = array();
        foreach ($aPhones as $v) {
            $v = preg_replace('/[^\s\+\-0-9]/', '', $v);
            $v = preg_replace('/\s+/', ' ', $v);
            $v = trim($v, '- ');
            if (strlen($v) > 4) {
                $v = mb_substr($v, 0, 20);
                $v = trim($v, '- ');
                $v = (strpos($v, '+') === 0 ? '+' : '') . str_replace('+', '', $v);
                $phone = array('v' => $v);
                $phone['m'] = mb_substr(trim($v, ' -+'), 0, 2) . 'x xxx xxxx';
                $aResult[] = $phone;
            }
        }
        if ($nLimit > 0 && sizeof($aResult) > $nLimit) {
            $aResult = array_slice($aResult, 0, $nLimit);
        }

        return $aResult;
    }

    /**
     * Инициализация компонента работы с тегами
     * @return UsersTags
     */
    public function userTags()
    {
        static $i;
        if (!isset($i)) {
            $i = new UsersTags();
            if (!bff::adminPanel()) {
                $i->module_dir_tpl = $this->module_dir_tpl;
            }
        }

        return $i;
    }

    /**
     * Форма настроек цен специализации (цена, бюджет)
     * @param integer $nSpecializationID ID специализации
     * @param array $aValues данные
     * @return string
     */
    public function priceForm($nSpecializationID, $aValues = array())
    {
        if (empty($nSpecializationID)) {
            return '';
        }

        $aData = array('spec' => $nSpecializationID, 'prefix' => 'pr');
        $aData['values'] = $aValues;
        $aData['priceSett'] = Specializations::i()->aPriceSett($nSpecializationID);
        if (bff::adminPanel()) {
            return $this->viewPHP($aData, 'admin.user.spec.price');
        } else {
            return $this->viewPHP($aData, 'my.settings.spec.price');
        }
    }

    /**
     * Формирование SQL запроса для сохранения свойств цены
     * @param integer $nSpecializationID ID специализации
     * @param string $sFieldname ключ в $_POST массиве
     * @return array
     */
    public function priceSave($nSpecializationID, $sFieldname = 'pr')
    {
        $aData = $this->input->post($sFieldname, TYPE_ARRAY);

        if( ! isset($aData[$nSpecializationID])) return array();

        $aData = $this->input->clean_array($aData[$nSpecializationID], array(
            'price'        => TYPE_PRICE, # Цена
            'price_curr'   => TYPE_UINT,  # Валюта цены
            'price_rate'   => TYPE_UINT,  # Модификатор цены (период)
            'budget'       => TYPE_PRICE, # Бюджет
            'budget_curr'  => TYPE_UINT,  # Валюта бюджета
        ));
        $aData['price_rate_text'] = '';
        $aPriceSett = Specializations::i()->aPriceSett($nSpecializationID, true);
        $nPriceRate = $aData['price_rate'];
        if (!empty($aPriceSett['rates'][$nPriceRate])) {
            $aRates = $aPriceSett['rates'][$nPriceRate];
            foreach ($aRates as $k => $v) {
                $aRates[$k] = mb_strtolower($v);
            }
            $aData['price_rate_text'] = serialize($aRates);
        }
        return $aData;
    }

    /**
     * Формирование ключа для авторизации из под пользователя (из админ. панели)
     * @param integer $userID ID пользователя
     * @param string $userEmail E-mail пользователя
     * @return string
     */
    public function frontendAuthHash($userID, $userEmail)
    {
        return $this->security->getRememberMePasswordMD5($userID.config::sys('site.title').$userEmail);
    }

    /**
     * Отправка письма пользователю
     * @param integer|array $userData ID пользователя или данные о нем
     * @param string $templateKey ключ шаблона письма
     * @param array $templateVars макросы подставляемые в шаблон письма
     * @param integer $enotifyCheck ID для проверки подписки на данный тип уведомлений или 0
     * @return boolean
     */
    public static function sendMailTemplateToUser($userData, $templateKey, $templateVars, $enotifyCheck = 0)
    {
        if (empty($userData) || empty($templateKey)) {
            return false;
        }
        if (!is_array($userData)) {
            $userData = static::model()->userDataEnotify($userData, $enotifyCheck);
        }
        if (!empty($userData) && (!$enotifyCheck || $userData['enotify'] & $enotifyCheck)) {
            $fio = array();
            if (!empty($userData['name'])) $fio[] = $userData['name'];
            if (!empty($userData['surname'])) $fio[] = $userData['surname'];
            $userData['fio'] = ( ! empty($fio) ? join(' ', $fio) : $userData['login']);
            foreach (array('name','surname','login','fio','email') as $k) {
                if ((!isset($templateVars[$k]) || empty($templateVars[$k])) && isset($userData[$k])) {
                    $templateVars[$k] = $userData[$k];
                }
            }
            # name <= fio
            if (isset($templateVars['name']) && empty($templateVars['name']) && !empty($templateVars['fio'])) {
                $templateVars['name'] = $templateVars['fio'];
            }
            $result = bff::sendMailTemplate($templateVars, $templateKey, $userData['email']);
            return ! empty($result);
        }
        return false;
    }

    /**
     * Инициализация компонента обработки изображений для проверки пользователей UsersVerifiedImages
     * @param mixed $userID ID пользователя
     * @return UsersVerifiedImages component
     */
    public static function verifiedImages($userID = false)
    {
        static $i;
        if( ! isset($i)) {
            $i = new UsersVerifiedImages();
        }
        $i->setRecordID($userID);
        return $i;
    }

    /**
     * Актуализация счетчика пользователей ожидающих проверки
     * @param integer|null $increment
     */
    public function verifiedCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->verifiedWaitingCounter();
            config::save('users_verified_waiting', $count, true);
        } else {
            config::saveCount('users_verified_waiting', $increment, true);
        }
    }

    # --------------------------------------------------------
    # Активация услуг

    /**
     * Список общих страниц для услуги лестница
     * @return array
     */
    public static function aSvcStairs(){
        return array(
            static::SVC_STAIRS_MAIN => array('id' => static::SVC_STAIRS_MAIN, 'title' => _t('svc', 'Главная страница')),
            static::SVC_STAIRS_USER => array('id' => static::SVC_STAIRS_USER, 'title' => _t('svc', 'Все исполнители'))
        );
    }

    /**
     * Активация услуги для пользователя
     * @param integer $nUserID ID пользователя
     * @param integer $nSvcID ID услуги/пакета услуг
     * @param mixed $aSvcData данные об услуге(*)/пакете услуг или FALSE
     * @param array $aSvcSettings @ref дополнительные параметры услуги / нескольких услуг / пакета услуг
     * @return boolean true - успешная активация, false - ошибка активации
     */
    public function svcActivate($nUserID, $nSvcID, $aSvcData = false, array &$aSvcSettings = array())
    {
        if (!$nSvcID) {
            $this->errors->set(_t('svc', 'Неудалось активировать услугу'));

            return false;
        }
        if (empty($aSvcData)) {
            $aSvcData = Svc::model()->svcData($nSvcID);
            if (empty($aSvcData)) {
                $this->errors->set(_t('svc', 'Неудалось активировать услугу'));

                return false;
            }
        }

        switch($nSvcID){
            case static::SVC_CAROUSEL:
            {
                $aUserSettings = $this->model->svcUserSettings($nUserID, $nSvcID);
                if( ! empty($aUserSettings['message'])){
                    $aData = array(
                        'user_id' => $nUserID,
                        'title'   => $aUserSettings['title'],
                        'message' => $aUserSettings['message'],
                        'payed'   => $this->db->now(),
                    );
                    if($this->model->svcCarouselSave(0, $aData)){
                        # Отправим уведомление пользователю
                        static::sendMailTemplateToUser($nUserID, 'users_carousel', array(
                        ), static::ENOTIFY_GENERAL);
                        return 1;
                    }
                }

            } break;
            case static::SVC_PRO:
            {
                if(empty($aSvcSettings['month']) || empty($aSvcData['mass'][ $aSvcSettings['month'] ])){
                    $this->errors->set(_t('svc', 'Неудалось активировать услугу'));
                    break;
                }

                $aUserData = Users::model()->userData($nUserID, array('pro', 'pro_expire'));
                $now = time();
                $months = $aSvcSettings['month'];
                $prolong = false;
                if ($aUserData['pro']) {
                    # продление активной услуги
                    $pro_to = strtotime($aUserData['pro_expire']);
                    $to = $pro_to < $now ? $now : $pro_to;
                    $aSvcSettings[static::SVC_PRO]['from'] = $to;
                    $from = $to;
                    $to = strtotime( date('Y-m-d H:i:s', $to).' +'.$months.' month'.($months>1?'s':''));
                    $prolong = true;
                } else {
                    # активация услуги (на текущий момент неактивированной)
                    $to = strtotime('+ '.$months.' month'.($months>1?'s':''));
                    $aSvcSettings[static::SVC_PRO]['from'] = $now;
                    $from = $now;
                }
                $aSvcSettings[static::SVC_PRO]['to'] = $to;

                if($this->model->userSave($nUserID, array(
                    'pro'        => 1,
                    'pro_expire' => date('Y-m-d H:i:s', $to),
                ))){
                    if (User::isCurrent($nUserID)) {
                        $this->security->updateUserInfo(array('pro'=>1));
                    }
                    if (!$prolong) {
                        # включаем выключенные на текущий момент специализации
                        $this->model->userSpecsEnableByLimit($nUserID, static::specializationsLimit(true));
                    }
                    # Отправим уведомление пользователю
                    static::sendMailTemplateToUser($nUserID, 'users_pro_start', array(
                        'months'   => tpl::declension($months, _t('','месяц;месяца;месяцев')),
                        'period'   => _t('svc', 'c [from] по [to]', array('from' => tpl::date_format2($from), 'to' => tpl::date_format2($to))),
                    ), static::ENOTIFY_GENERAL);
                    return 1;
                }
            } break;
            case static::SVC_STAIRS:
            {
                $spec = $aSvcSettings['spec'];
                $cat = $aSvcSettings['cat'];
                if(empty($aSvcSettings['weeks'])){
                    $sum = $aSvcSettings['sum'];
                    if( ! $sum) {
                        $this->errors->set(_t('svc', 'Неудалось активировать услугу'));
                        break;
                    }

                    $aData = $this->model->svcUsersStairs(array(
                        'user_id' => $nUserID,
                        'spec_id' => $spec,
                        'cat_id'  => $cat,
                    ));
                    if(empty($aData)){
                        $this->errors->set(_t('svc', 'Неудалось активировать услугу'));
                        break;
                    }
                    $aData = reset($aData);
                    if(empty($aData)){
                        $this->errors->set(_t('svc', 'Неудалось активировать услугу'));
                        break;
                    }
                    $nStairsID = $aData['id'];
                    $aUpdate = array(
                        'sum'   => $aData['sum'] + $sum,
                        'payed' => $this->db->now(),
                    );
                    if($this->model->svcStairsSave($nStairsID, $aUpdate)){
                        return 1;
                    }

                    $this->errors->set(_t('svc', 'Неудалось активировать услугу'));
                    break;
                }
                $weeks = $aSvcSettings['weeks'];

                $pr = $this->svcStairsPrice($spec, $cat, $aSvcData);
                if( ! $pr){
                    $this->errors->set(_t('svc', 'Неудалось активировать услугу'));
                    break;
                }
                $aData = $this->model->svcUsersStairs(array(
                    'user_id' => $nUserID,
                    'spec_id' => $spec,
                    'cat_id'  => $cat,
                    ));
                $aData = reset($aData);

                if( ! empty($aData)){
                    $nStairsID = $aData['id'];
                    $from = strtotime($aData['expire']);
                    $now = time();
                    if($from < $now){ $from = $now; }
                    $to = strtotime('+ '.$weeks.' week'.($weeks>1?'s':''), $from);
                    $aUpdate = array(
                        'expire' => date('Y-m-d H:i:s', $to),
                    );
                    $aSvcSettings[static::SVC_STAIRS]['from'] = $from;
                    $aSvcSettings[static::SVC_STAIRS]['to'] = $to;
                    if($this->model->svcStairsSave($nStairsID, $aUpdate)){
                        return 1;
                    }
                } else {
                    $to = strtotime('+ '.$weeks.' week'.($weeks>1?'s':''));

                    $aInsert = array(
                        'user_id'     => $nUserID,
                        'cat_id'      => $cat,
                        'spec_id'     => $spec,
                        'title'       => ! empty($aSvcSettings['title']) ? $aSvcSettings['title'] : '',
                        'message'     => ! empty($aSvcSettings['message']) ? $aSvcSettings['message'] : '',
                        'description' => ! empty($aSvcSettings['description']) ? $aSvcSettings['description'] : '',
                        'payed'       => $this->db->now(),
                        'expire'      => date('Y-m-d H:i:s', $to),
                    );
                    $aSvcSettings[static::SVC_STAIRS]['from'] = time();
                    $aSvcSettings[static::SVC_STAIRS]['to'] = $to;

                    if($this->model->svcStairsSave(0, $aInsert)){
                        # Уведомление пользователю отправляем в методе Users::svc_stairs
                        return 1;
                    }
                }
            } break;
            default:
                $this->errors->set(_t('svc', 'Неудалось активировать услугу'));
                break;
        }
    }

    /**
     * Формируем описание счета активации услуги (пакета услуг)
     * @param integer $nUserID ID пользователя
     * @param integer $nSvcID ID услуги
     * @param mixed $aData данные об услуге(*)/пакете услуг или FALSE
     * @param array $aSvcSettings @ref дополнительные параметры услуги / нескольких услуг / пакета услуг
     * @return string
     */
    public function svcBillDescription($nUserID, $nSvcID, $aData = false, array &$aSvcSettings = array())
    {
        $aSvc = (!empty($aData['svc']) ? $aData['svc'] :
            Svc::model()->svcData($nSvcID));

        if ($aSvc['type'] == Svc::TYPE_SERVICE) {
            switch ($nSvcID) {
                case static::SVC_CAROUSEL:
                {
                    return _t('svc', 'Платное место на верху страницы');
                } break;
                case static::SVC_PRO:
                {
                    return _t('svc', 'Аккаунт "Pro". Период активации: [from] - [to]', array(
                        'from' => tpl::date_format2($aSvcSettings[static::SVC_PRO]['from']),
                        'to'   => tpl::date_format2($aSvcSettings[static::SVC_PRO]['to'])
                    ));
                } break;
                case static::SVC_STAIRS:
                {

                    $spec = $aSvcSettings['spec'];
                    $cat = $aSvcSettings['cat'];
                    $sSpecTitle = $this->svcStairsTitle($spec, $cat);

                    $sTitle = $aSvc['title_view'][LNG];
                    if(empty($aSvcSettings['weeks'])){
                        if( ! empty($aSvcSettings['sum'])){
                            $sTitle .= '<br />'._t('svc', 'поднятие').' '.$sSpecTitle;
                        }
                        return $sTitle;
                    }
                    $sTitle .= '<br />'._t('svc', 'Период активации: [from] - [to]', array(
                            'from' => tpl::date_format2($aSvcSettings[static::SVC_STAIRS]['from']),
                            'to'   => tpl::date_format2($aSvcSettings[static::SVC_STAIRS]['to'])));
                    $sTitle .= '<br />'.$sSpecTitle;
                    return $sTitle;

                } break;
            }
        }
        return '';
    }

    /**
     * Получение стоимости для услуги лестница с учетом общих страниц
     * @param integer $spec id специализации
     * @param integer $cat id категории
     * @param integer $aSvc Данные об услуге svcData
     * @return mixed
     */
    public function svcStairsPrice($spec, $cat, $aSvc)
    {
        if ($spec != 0) {
            switch ($spec) {
                case static::SVC_STAIRS_MAIN: return $aSvc['price_main']; break;
                case static::SVC_STAIRS_USER: return $aSvc['price_user']; break;
                default: return $aSvc['price_spec']; break;
            }
        } else {
            if($cat != 0){
                return $aSvc['price_cat'];
            }
        }
        return false;
    }

    /**
     * Получение заголовка (общие, категория, специализация) для услуги лестница
     * @param integer $spec id специализации
     * @param integer $cat id категории
     * @return string
     */
    public function svcStairsTitle($spec, $cat)
    {
        static $specs, $cats;
        if($spec < 0){
            $aSvcStairs = static::aSvcStairs();
            return $aSvcStairs[$spec]['title'];
        } else {
            if($spec > 0){
                if(isset($specs[$spec])){
                    return $specs[$spec];
                }
                $aSpec = Specializations::model()->specializationData($spec);
                $specs[$spec] = $aSpec['title'];
                return $aSpec['title'];
            } else {
                if($cat > 0){
                    if(isset($cats[$cat])){
                        return $cats[$cat];
                    }
                    $aCat = Specializations::model()->categoryData($cat);
                    $cats[$cat] = $aCat['title'];
                    return $aCat['title'];
                }
            }
        }
        return '';
    }

    /**
     * Инициализация компонента обработки иконок услуг/пакетов услуг UsersSvcIcon
     * @param mixed $nSvcID ID услуги / пакета услуг
     * @return UsersSvcIcon component
     */
    public static function svcIcon($nSvcID = false)
    {
        static $i;
        if (!isset($i)) {
            $i = new UsersSvcIcon();
        }
        $i->setRecordID($nSvcID);

        return $i;
    }

    /**
     * Период: 1 раз в час
     */
    public function svcCron()
    {
        if (!bff::cron()) {
            return;
        }

        $this->model->svcCron();
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('resume') => 'dir-only', # вложения резюме
            PATH_BASE.'files'.DS.'verified'.DS => 'dir-split', # верификация
        ));
    }

    public static function getCurrentUserAvatar($sSize = UsersAvatar::szSmall)
    {
        $aData = User::data(['id','avatar', 'sex', 'login']);
        $aData['avatar_url'] = UsersAvatar::url($aData['id'], $aData['avatar'], UsersAvatar::szSmall, $aData['sex']);
        $aData['profile_url'] = self::url('profile', $aData);
        return $aData;
    }

    /**
     * Получаем контакты пользователя :
     *  - общий список, если включена регистрация по телефону, то еще и подтвержденный телефон
     *  - список телефонов из контактов
     * @param int $nUserID - id пользователя для которого необходимо получить контакты
     * @param bool $bOnlyPhones - необходимо получить только номера телефонов
     * @return array|mixed
     */
    public function getUserContacts($nUserID, $bOnlyPhones = false)
    {
        if(! $nUserID){
            return false;
        }
        $aData = $this->model->userData($nUserID, [
            'contacts',
            'phone_number',
            'phone_number_verified'
        ]);

        if (static::registerPhoneContacts() && $aData['phone_number'] && $aData['phone_number_verified']) {
            $ph = array(
                't' => static::CONTACT_TYPE_PHONE,
                'v' => $aData['phone_number'],
            );
            if (empty($aData['contacts'])) {
                $aData['contacts'] = array(0=>$ph);
            } else {
                array_unshift($aData['contacts'], $ph);
            }
        }
        $aData['contacts_only_phones'] = [];
        foreach ($aData['contacts'] as $contact) {
            if ($contact['t'] == static::CONTACT_TYPE_PHONE) {
                $aData['contacts_only_phones'][] = $contact;
            }
        }
        if ($bOnlyPhones){
            return $aData['contacts_only_phones'];
        }

        return $aData;
    }
    
    public static function getCntTopList()
    {       
        return config::sys('users.cnt.top.list', 10, TYPE_UINT);
    }
    
    public static function isEnabledTopList()
    {
        return !empty(self::getCntTopList());
    }
    
    public static function getTopPositionsList($nCurrentProfileID = false)
    {
        $nCntListTop = Users::getCntTopList();
        $sLimit = ' LIMIT 0,'. $nCntListTop.' ';
        $aFilter[':position'] = ' U.position != 0 ';
        if($nCurrentProfileID){
            $aFilter[':not_user'] = 'U.user_id != '.$nCurrentProfileID;
        }
        
        $cCntListPosition = Users::model()->searchList($aFilter,true, '');
        
        $aData = [];
        if ($cCntListPosition) {
            $aData['list'] = Users::model()->searchList($aFilter, false, $sLimit,
                'U.position ASC, ' . Rating::getOrderFormula('U.pro DESC, U.rating DESC'));
        }
        
        if( !isset($aData['list']) || (isset($aData['list']) && count($aData['list']) < (int)$nCntListTop) ){
            
            $aFilter[':position'] = ' U.position < 1 ';
            if (isset($aData['list'])) {
                $sLimit = ' LIMIT 0,' . ((int)$nCntListTop - count($aData['list'])) . ' ';
                
            }
            
            $aData['without_pos_list'] = Users::model()->searchList($aFilter, false, $sLimit,
                Rating::getOrderFormula('U.pro DESC, U.rating DESC'));
            
            $aData['list'] = array_merge($aData['list'], $aData['without_pos_list']);
            
        }
        
        return self::i()->viewPHP($aData, 'profile.view.top');
    }

    /**
     * Заполнял ли исполнитель анкету
     * @param bool $nUserID
     * @return bool
     */
    public static function isUserFilledQuestionary($nUserID = false)
    {
        if (!$nUserID) {
            $nUserID = User::id();
        }

        $aUserShedule = Users::model()->userShedule($nUserID);
        $aUserSpecs = Users::model()->userSpecs($nUserID, false);


        if (empty($aUserShedule)) { # необходимое условие сохранение анкеты
            return false;
        }

        if (empty($aUserSpecs)) { # наличие любой специализации
            return false;
        }
        return true;
    }

    /**
     * Пренадлежит ли пользователь к группе с правами администратора
     * @param bool $nUserId
     * @return bool
     */
    public static function isAdminGroup($nUserId = false)
    {
        if (!User::id()) {
            return false;
        }
        if (!$nUserId) {
            $nUserId = User::id();
        }

        $aUserData = Users::model()->userDataByFilter(array('user_id' => $nUserId));

        return !empty($aUserData['admin']);
    }

    public static function setUserProfileData($nUserId = false, $aUserData = false)
    {
        if(!$nUserId){
            $nUserId = User::id();
        }

        if(empty($aUserData)){
            $aUserData = Users::i()->viewProfile($nUserId, true);
        }
        self::$userProfileData = $aUserData;
        return $aUserData;
    }

    public static function getUserProfileData()
    {
        return self::$userProfileData;
    }

    /**
     * Оставил ли пользователь заказ
     * @param bool $nUserID
     * @return bool
     */
    public static function isUserLeaveOrder($nUserID = false)
    {
        if(!User::id() || User::isWorker()){
            return false;
        }

        if (!$nUserID) {
            $nUserID = User::id();
        }

        $nUserOrdersCnt  = Orders::model()->ordersListOwner(['user_id' => $nUserID], true);

        return !empty($nUserOrdersCnt);
    }
}