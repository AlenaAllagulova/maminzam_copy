<?php
    /**
     * Кабинет пользователя: Настройки
     * @var $this Users
     */
    Geo::mapsAPI(true);
    tpl::includeJS(array('autocomplete','qquploader'), true);
    tpl::includeJS('users.my', false, 6);
    $aData = HTML::escape($aData, 'html', array('email','name','surname','addr_addr'));
    $city_data['title'] = HTML::escape($city_data['title']);
    $countrySelect = Geo::countrySelect();

    $aTabs = bff::filter('users.my.settings.tabs', array(
        'general'  => array('t' => _t('users', 'Общие настройки')),
        'verified' => array('t' => _t('users', 'Верификация').($verified != Users::VERIFIED_STATUS_APPROVED ? '  <i class="fa fa-warning text-warning"></i>' : '')),
        'enotify'  => array('t' => _t('users', 'Уведомления')),
        'tabs'     => array('t' => _t('users', 'Закладки')),
        'access'   => array('t' => _t('users', 'Доступ')),
        'finance'  => array('t' => _t('fp', 'Финансы')),
    ));
    func::sortByPriority($aTabs, 'priority');

    $t = $this->input->get('tab', TYPE_NOTAGS);
    if( ! array_key_exists($t, $aTabs)){
        $t = array_keys($aTabs);
        $t = reset($t);
    }
    if (Users::useClient() && User::isClient()) {
        unset($aTabs['tabs']);
    }
    $verifiedEnabled = Users::verifiedEnabled();
    if( ! $verifiedEnabled){
        unset($aTabs['verified']);
    }
    $fairplayEnabled = bff::fairplayEnabled();
    if ($fairplayEnabled) {
        if(Fairplay::model()->financeVerified($id) != Fairplay::VERIFIED_STATUS_APPROVED){
            $aTabs['finance']['t'] .= '  <i class="fa fa-warning text-warning"></i>';
        }
    } else {
        unset($aTabs['finance']);
    }
    $company = (Users::rolesEnabled() && $role_id == Users::ROLE_COMPANY);
?>

    <div class="container" id="j-my-settings">

        <section class="l-mainContent">

            <div class="row">

                <div class="col-md-8 col-md-offset-2">

                    <?= tpl::getBreadcrumbs(array(
                        array('title'=>_t('','Profile'),'link'=>Users::url('my.profile')),
                        array('title'=>_t('','Settings'),'active'=>true),
                    )); ?>

                    <?= tpl::getTabs(array('tabs' => $aTabs, 'a' => $t, 'class' => 'j-settings-tab')); ?>

                    <div class="p-profileContent j-settings-tab j-settings-tab-general<?= $t != 'general' ? ' displaynone' : ''?>">

                        <form action="" class="form-horizontal j-form-contacts" role="form" method="post">
                            <input type="hidden" name="act" value="contacts" />

                            <section class="p-profileCabinet-section">
                                <div class="row">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <h5><?= _t('users', 'Личная информация'); ?></h5>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-3 control-label p-avatar-label"><?= $company ? _t('users', 'Логотип') : _t('users', 'Аватар') ?></label>
                                    <div class="col-sm-9">

                                        <div class="p-profileCabinet-avatar">
                                            <div class="p-img">
                                                <img src="<?= $avatar_small ?>" alt="<?= tpl::avatarAlt($aData); ?>" id="j-my-avatar-img" />
                                            </div>
                                            <div class="p-load">
                                                <a class="btn btn-sm btn-secondary" id="j-my-avatar-upload"><?= _t('users', 'загрузить') ?></a>
                                                <a href="#" class="p-delete" id="j-my-avatar-delete" title="<?= _t('', 'Удалить фотографию'); ?>"><i class="fa fa-trash-o"></i></a>
                                                <div class="help-block">
                                                    <?= _t('', 'Формат: jpg, gif, png. Максимальный размер файла: [size].', array('size' => $this->avatar($id)->getMaxSize(true))); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            
                                            <? if(Users::verifiedEnabled() && $verified == Users::VERIFIED_STATUS_APPROVED): ?>
                                                <br />
                                                <? if( ! $company): ?>
                                                    <div class="alert alert-warning"> <?= _t('users', 'В случае изменения Имени или Фамилии вам необходимо будет повторно пройти верификацию'); ?> </div>
                                                <? else: ?>
                                                    <div class="alert alert-warning"> <?= _t('users', 'В случае изменения Названия компании вам необходимо будет повторно пройти верификацию'); ?> </div>
                                                <? endif; ?>
                                            <? endif; ?>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group j-required">
                                    <label for="flogin" class="col-sm-3 control-label"><?= _t('', 'Логин'); ?></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="login" value="<?= $login ?>" id="flogin" placeholder="<?= _t('users', 'Введите Ваш логин'); ?>" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="fname" class="col-sm-3 control-label"><?= $company ? _t('', 'Название компании') : _t('', 'Имя'); ?></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="name" value="<?= $name ?>" id="fname" placeholder="<?= _t('users', 'Введите Ваше имя'); ?>" />
                                    </div>
                                </div>

                                <? if( ! $company): ?>
                                <div class="form-group">
                                    <label for="lname" class="col-sm-3 control-label"><?= _t('', 'Фамилия'); ?></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="surname" value="<?= $surname ?>" id="lname" placeholder="<?= _t('users', 'Введите Вашу фамилию'); ?>" />
                                    </div>
                                </div>
                                <? if(Users::profileSex()):
                                    $aSex = array(
                                        Users::SEX_FEMALE => _t('', 'Женский'),
                                        Users::SEX_MALE   => _t('', 'Мужской'),
                                    );
                                ?>
                                <div class="form-group">
                                    <label for="sex" class="col-sm-3 control-label"><?= _t('', 'Пол'); ?></label>
                                    <div class="col-sm-4">
                                        <? foreach ($aSex as $k=>$v) { ?>
                                        <div class="radio-inline">
                                            <label>
                                                <input type="radio" value="<?= $k ?>" name="sex" <?= ($sex == $k ? ' checked="checked"' : '') ?> />
                                                <?= $v ?>
                                            </label>
                                        </div>
                                        <? } ?>
                                    </div>
                                </div>
                                <? endif; ?>
                                <? if(Users::profileBirthdate()):
                                    $aData['birthdate'] = $this->getBirthdateOptions($aData['birthdate'], 1930, true); # дата рождения
                                ?>
                                <div class="form-group p-cabinet-birthdate">
                                    <label for="dd" class="col-sm-3 control-label"><?= _t('users', 'Дата рождения'); ?></label>

                                    <div class="col-sm-2">
                                        <select name="birthdate[day]" class="form-control" id="dd">
                                            <?= $birthdate['days'] ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="birthdate[month]" class="form-control" id="mm">
                                            <?= $birthdate['months'] ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="birthdate[year]" class="form-control" id="yyyy">
                                            <?= $birthdate['years'] ?>
                                        </select>
                                    </div>
                                </div>
                                <? endif; ?>
                                <? endif; ?>

                                <? if(User::isWorker() || ! Users::useClient()): ?>
                                <div class="form-group p-cabinet-birthdate">
                                    <label for="experience" class="col-sm-3 control-label"><?= _t('users', 'Опыт работы'); ?></label>

                                    <div class="col-sm-3">
                                        <select name="experience" class="form-control" id="experience">
                                            <?= HTML::selectOptions(Users::aExperience(), $experience, false, 'id', 't') ?>
                                        </select>
                                    </div>
                                </div>
                                <? endif ?>

                            </section>


                            <? $nSpecIndex = 1; $nSpecCnt = 1;
                            $nSpecLimit = Users::specializationsLimit($pro);
                            if($type == Users::TYPE_WORKER && $nSpecLimit):
                                $aMainSpec = array();
                                foreach($specs as $k => $v){
                                    if($v['main']){
                                        $aMainSpec = $v;
                                        break;
                                    }
                                }
                            ?>
                            <section class="p-profileCabinet-section" id="j-my-specs">
                                <div class="row">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <h5><?= _t('users', 'Специализация и навыки'); ?></h5>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= _t('users', 'Основная'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="j-spec-main">
                                            <?= Specializations::i()->specSelect($nSpecIndex++, $aMainSpec, array('noCatOnSelect' => 1)); $nSpecCnt++; ?>
                                            <div class="j-spec-ex">
                                                <?= ! empty($aMainSpec['dp']) ? $aMainSpec['dp'] : '' ?>
                                                <?= ! empty($aMainSpec['serv']) ? $aMainSpec['serv'] : '' ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <? if($nSpecLimit > 1): ?>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?= _t('users', 'Дополнительные'); ?></label>
                                        <div class="col-sm-9">
                                            <div class="p-specialization-add j-specs-block">
                                                <? foreach($specs as $k => $v): if($v['main']) continue; ?>
                                                    <div>
                                                        <?= Specializations::i()->specSelect($nSpecIndex++, $v, array('allowDelete' => 1, 'noCatOnSelect' => 1)); ?>
                                                        <div class="j-spec-ex<? if(!empty($v['disabled'])) { ?> hidden<? } else { $nSpecCnt++; } ?>">
                                                        <?= ! empty($v['dp']) ? $v['dp'] : '' ?>
                                                        <?= ! empty($v['serv']) ? $v['serv'] : '' ?>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                <? endforeach; ?>
                                            </div>

                                            <a href="#" class="ajax-link link-add-2 j-spec-add<?= $nSpecCnt > $nSpecLimit ? ' hide' : '' ?>"><i class="fa fa-plus-circle"></i> <span><?= _t('user', 'Добавить специализацию'); ?></span></a>
                                      </div>
                                    </div>
                                <? endif; ?>

                                <?= $this->userTags()->formFront($id, array('wightClass' => 'col-sm-9')); ?>

                            </section>
                            <? endif; ?>


                            <? if(Users::profileMap()): ?>
                            <section class="p-profileCabinet-section" id="j-my-geo">
                                <div class="row">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <h5><?= _t('', 'Местоположение'); ?></h5>
                                    </div>
                                </div>
                                <? if($countrySelect): ?>
                                <div class="form-group">
                                    <label for="j-my-geo-country" class="col-sm-3 control-label"><?= _t('', 'Страна'); ?></label>
                                    <div class="col-sm-4">
                                        <select class="form-control"
                                                name="reg1_country"
                                                <?= ($reg3_city != Geo::defaultCity()? '': 'disabled="disabled"' )?>
                                                id="j-my-geo-country">
                                            <?= HTML::selectOptions(Geo::countryList(), $reg1_country, 'Выбрать', 'id', 'title') ?>
                                        </select>
                                    </div>
                                </div>
                                <?  endif; ?>

                                <div class="form-group j-region<?= ($countrySelect && ! $reg1_country ? ' displaynone' : '') ?>">
                                    <label for="j-my-geo-city-select" class="col-sm-3 control-label"><?= _t('', 'Город'); ?></label>
                                    <div class="col-sm-4">
                                        <input type="hidden" name="reg3_city" value="<?= $reg3_city ?>" id="j-my-geo-city-value" />
                                        <input type="text"
                                               class="form-control"
                                               id="j-my-geo-city-select"
                                               value="<?= $city_data['title'] ?>"
                                               <?= ($reg3_city != Geo::defaultCity()? '': 'disabled="disabled"' )?>
                                               placeholder="<?= _t('users', 'Введите название города') ?>" />
                                    </div>
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="dropdown<?= ! $reg3_city || ! $city_data['metro'] ? ' hide' : '' ?> mrgt10 j-my-geo-metro">
                                            <input type="hidden" name="metro_id" value="<?= $metro_id ?>" class="j-my-geo-metro-value" />
                                            <a href="#" class="dropdown-toggle ajax-link" data-toggle="dropdown">
                                                <span class="j-my-geo-metro-selected<?= ! $metro_id ? ' hide': '' ?>"><span class="p-metro-color j-color" style="background-color:<?=  $metro_id ? $metro_data['sel']['branch']['color'] : '#000000;' ?>"></span> <span class="j-title"><?= $metro_id ? $metro_data['sel']['branch']['t'] : '' ?> / <?= $metro_id ? $metro_data['sel']['station']['t'] : '' ?></span></span>
                                                <span class="j-my-geo-metro-empty<?= $metro_id ? ' hide': '' ?>"> <?= _t('', 'Выбрать ветку метро')?></span>
                                                <i class="fa fa-caret-down"></i>
                                            </a>
                                            <ul class="dropdown-menu c-dropdown-caret_left pull-left j-my-geo-metro-popup-step1<?= $metro_id ? ' hide' : '' ?>" role="menu">
                                                <?= ! empty($metro_data['data']) ? Geo::i()->viewPHP($metro_data, 'form.metro.step1') : '' ?>
                                            </ul>
                                            <ul class="dropdown-menu c-dropdown-caret_left pull-left j-my-geo-metro-popup-step2<?= ! $metro_id ? ' hide' : ''?>" role="menu">
                                                <? if($metro_id){
                                                    $station_data = $metro_data['data'][ $metro_data['sel']['branch']['id'] ];
                                                    $station_data['city_id'] = $reg3_city;
                                                    echo Geo::i()->viewPHP($station_data, 'form.metro.step2');
                                                } ?>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <? if(!empty($reg3_city) || Geo::defaultCity()):?>
                                    <div class="form-group">
                                        <label for="j-my-geo-district-select" class="col-sm-3 control-label"><?= _t('', 'Район города'); ?></label>
                                        <div class="col-sm-4">
                                            <select class="form-control"
                                                    name="district_id">
                                                <?= HTML::selectOptions(Geo::districtList(!empty($reg3_city) ? $reg3_city : Geo::defaultCity()),
                                                    (!empty($district_id) ? $district_id : 0),
                                                    _t('', 'Выбрать'),
                                                    'id',
                                                    'title') ?>
                                            </select>
                                        </div>
                                    </div>
                                <? endif;?>

                                <div class="form-group j-region<?= ($countrySelect && ! $reg1_country ? ' displaynone' : '') ?>">
                                    <input type="hidden" name="addr_lat" id="j-my-geo-addr-lat" value="<?= $addr_lat ?>" />
                                    <input type="hidden" name="addr_lng" id="j-my-geo-addr-lng" value="<?= $addr_lng ?>" />

                                    <label for="j-my-geo-addr-addr" class="col-sm-3 control-label"><?= _t('', 'Адрес'); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="addr_addr" id="j-my-geo-addr-addr" value="<?= $addr_addr ?>" placeholder="<?= _t('users', 'Введите свое местоположение'); ?>" />
                                    </div>
                                </div>
                                <div class="form-group j-region<?= ($countrySelect && ! $reg1_country ? ' displaynone' : '') ?>">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <div id="j-my-geo-addr-map" style="width:100%; height:300px" class="p-addProject_map map-google"></div>
                                    </div>
                                </div>

                            </section><!-- /.p-profileCabinet-section -->
                            <? endif; # end: profileMap ?>


                            <section class="p-profileCabinet-section p-profileCabinet-contacts"  id="j-my-contacts">

                                <div class="row">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <h5><?= _t('users', 'Контакты'); ?></h5>
                                        <div class="help-block"><?= _t('users', 'Укажите ваш ID, имя пользователя или ссылку на вашу страницу или аккаунт в социальной сети.'); ?></div>

                                        <a href="#" class="ajax-link j-add"><span><?= _t('form', 'Добавить'); ?></span></a>

                                    </div><!-- /.col-sm-9 -->
                                </div><!-- /.row -->

                            </section><!-- /.p-profileCabinet-section -->

                            <? if($type == Users::TYPE_WORKER):?>
                                <?= $this->viewPHP($aData, 'schedule.block')?>
                            <? endif;?>
                            <div class="row">
                                <div class="col-sm-9 col-sm-offset-3 c-formSubmit">
                                    <a class="btn btn-primary c-formSuccess j-submit" data-loading-text="<?= _t('form', 'Подождите...') ?>"><i class="fa fa-check"></i> <?= _t('form', 'Сохранить'); ?></a>
                                    <a class="c-formCancel j-cancel" href="<?= Users::url('my.profile'); ?>"><?= _t('form', 'Отмена'); ?></a>
                                </div>
                            </div>

                        </form>

                    </div>

                    <div class="p-profileContent j-settings-tab j-settings-tab-enotify<?= $t != 'enotify' ? ' displaynone' : ''?>">
                        <form action="" role="form" class="j-form-enotify">
                            <input type="hidden" name="act" value="enotify" />
                            <div class="p-profileCabinet-section">
                                <h5><?= _t('users', 'Настройка уведомлений') ?></h5>

                                <? foreach($enotify as $k=>$v): ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="enotify[]" value="<?= $k ?>" <? if($v['a']) { ?> checked="checked"<? } ?> class="j-my-enotify-check" />
                                        <?= $v['title'] ?>
                                    </label>
                                </div>
                                <? endforeach; ?>
                            </div><!-- /.p-profileCabinet-section -->
                            <button class="btn btn-primary mrgr10 j-submit" type="submit"><i class="fa fa-check"></i><?= _t('form', 'Сохранить'); ?></button>
                            <a href="<?= Users::url('my.profile'); ?>" class="c-formCancel j-cancel"><span><?= _t('form', 'Отмена'); ?></span></a>
                        </form>
                    </div>

                    <? if(isset($aTabs['tabs'])): ?>
                    <div class="p-profileContent j-settings-tab j-settings-tab-tabs<?= $t != 'tabs' ? ' displaynone' : ''?>">
                        <form action="" role="form" class="j-form-profile-tabs">
                            <input type="hidden" name="act" value="profile-tabs" />
                            <div class="p-profileCabinet-section">
                                <h5><?= _t('users', 'Настройка закладок') ?></h5>
                                <div class="help-block"><?= _t('users', 'На странице моего профиля отображать следующие закладки:') ?></div>

                                <? foreach($profile_tabs as $k=>$v): ?>
                                    <div class="checkbox"<? if(!empty($v['hidden'])){ ?> style="display: none;"<? } ?>>
                                        <label>
                                            <input type="checkbox" name="profile_tabs[]" value="<?= $k ?>" <? if($v['a']) { ?> checked="checked"<? } ?> class="j-my-profile-tabs-check" />
                                            <?= $v['title'] ?>
                                        </label>
                                    </div>
                                <? endforeach; ?>
                            </div>
                            <button class="btn btn-primary mrgr10 j-submit" type="submit"><i class="fa fa-check"></i><?= _t('form', 'Сохранить'); ?></button>
                            <a href="<?= Users::url('my.profile'); ?>" class="c-formCancel j-cancel"><span><?= _t('form', 'Отмена'); ?></span></a>
                        </form>
                    </div>
                    <? endif; ?>

                    <div class="p-profileContent j-settings-tab j-settings-tab-access<?= $t != 'access' ? ' displaynone' : ''?>">
                        <? if( ! empty($social) || $on['join'] ): $isClient = $type == Users::TYPE_CLIENT ?>
                        <div class="p-profileContent-item">
                            <div class="row">
                                <? if($on['join']): ?>
                                <div class="col-sm-6">
                                    <h5><?= $isClient ? _t('users', 'Профиль исполнителя') : _t('users', 'Профиль заказчика') ?></h5>

                                    <form action="" role="form" class="j-form-join-on<?= $joined_id ? ' hidden' : '' ?>">
                                        <input type="hidden" name="act" value="join-profile-on" />
                                        <div class="help-block">
                                            <?= $isClient ?
                                                _t('users', 'Вы можете объединить свой профиль заказчика с профилем исполнителя, чтобы быстро между ними переключаться.') :
                                                _t('users', 'Вы можете объединить свой профиль исполнителя с профилем заказчика, чтобы быстро между ними переключаться.') ?>

                                        </div>
                                        <div class="form-group">
                                            <label for="attach-email"><?= $isClient ? _t('users', 'Email профиля исполнителя') : _t('users', 'Email профиля заказчика') ?></label>
                                            <input type="email" name="email" class="form-control j-required" id="attach-email" placeholder="<?= $isClient ? _t('users', 'Введите email профиля исполнителя') : _t('users', 'Введите email профиля заказчика') ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="attach-pass"><?= $isClient ? _t('users', 'Пароль профиля исполнителя') : _t('users', 'Пароль профиля заказчика') ?></label>
                                            <input type="password" name="pass" class="form-control j-required" id="attach-pass" placeholder="<?= $isClient ? _t('users', 'Введите пароль профиля исполнителя') : _t('users', 'Введите пароль профиля заказчика') ?>">
                                        </div>
                                        <div class="c-formSubmit">
                                            <button type="submit" class="btn btn-primary c-formSuccess j-submit"><i class="fa fa-check"></i> <?= _t('users', 'Объединить'); ?></button>
                                        </div>
                                    </form>

                                    <div action="" role="form" class="j-form-join-off<?= ! $joined_id ? ' hidden' : '' ?>">
                                        <div class="help-block">
                                            <?= $isClient ? _t('users', 'Ваш профиль объединен с профилем исполнителя, вы можете в него перейти или отсоединить его.') : _t('users', 'Ваш профиль объединен с профилем заказчика, вы можете в него перейти или отсоединить его.') ?>
                                        </div>
                                        <div class="form-group">
                                            <label><?= $isClient ? _t('users', 'Email профиля исполнителя') : _t('users', 'Email профиля заказчика') ?></label>
                                            <input type="email" class="form-control j-join-email" value="<?= ! empty($joined['email']) ? $joined['email'] : '' ?>" disabled="disabled">
                                        </div>
                                        <div class="c-formSubmit">
                                            <button type="button" class="btn btn-danger c-formSuccess j-join-off"><i class="fa fa-close"></i> <?= _t('', 'Отсоединить'); ?></button>
                                            <a href="javascript:;" class="btn btn-default" onclick="app.user.changeJoin();"><?= _t('users', 'Перейти в профиль'); ?> <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>

                                </div>
                                <? endif; ?>
                                <? if( ! empty($social) ): ?>
                                <div class="col-sm-6">
                                    <div class="p-profileCabinet-social">
                                        <h5><?= _t('users', 'Интеграция с социальными сетями') ?></h5>
                                        <? foreach($social as $k=>$v): ?>
                                            <a href="#" class="btn btn-sm s-btn-social s-<?= $v['class'] ?><? if( ! isset($v['user']) ){ ?> not-active<? } ?> j-my-social-btn" data="{provider:'<?= $v['key'] ?>',w:<?= $v['w'] ?>,h:<?= $v['h'] ?>}"><?= $v['title'] ?> <? if(isset($v['user']) ){ ?><i class="fa fa-check"></i><? } ?></a>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                                <? endif; ?>
                            </div>
                        </div>
                        <? endif; ?>

                        <div class="p-profileContent-item">
                            <div class="row">
                                <div class="col-sm-6">
                                    <form action="" role="form" class="j-form-pass">
                                        <input type="hidden" name="act" value="pass" />

                                        <h5 class="mrgt0"><?= _t('users', 'Смена пароля'); ?></h5>

                                        <div class="form-group">
                                            <label for="oldpass"><?= _t('users', 'Старый пароль') ?></label>
                                            <input type="text" class="form-control j-required" name="pass0" maxlength="100" autocomplete="off" id="oldpass" placeholder="<?= _t('users', 'Введите свой старый пароль') ?>" />
                                        </div>

                                        <div class="form-group">
                                            <label for="newpass"><?= _t('users', 'Новый пароль'); ?></label>
                                            <input type="text" class="form-control j-required" name="pass1" maxlength="100" id="newpass" placeholder="<?= _t('users', 'Введите свой новый пароль'); ?>" />
                                        </div>

                                        <div class="form-group">
                                            <label for="newpassrepeat"><?= _t('users', 'Повторите новый пароль'); ?></label>
                                            <input type="text" class="form-control j-required" name="pass2" maxlength="100" id="newpassrepeat" placeholder="<?= _t('users', 'Повторите свой новый пароль'); ?>" />
                                        </div>

                                        <div class="c-formSubmit">
                                            <button class="btn btn-primary c-formSuccess j-submit" type="submit"><i class="fa fa-check"></i> <?= _t('form', 'Сохранить'); ?></button>
                                        </div>
                                    </form>
                                </div>
                                <? if($on['email']): ?>
                                <div class="col-sm-6">
                                    <form action="" role="form" class="j-form-email">
                                        <input type="hidden" name="act" value="email" />
                                        <h5 class="mrgt0"><?= _t('users', 'Изменить email-адрес') ?></h5>

                                        <div class="form-group">
                                            <label for="oldemail"><?= _t('users', 'Ваш текущий email') ?></label>
                                            <input type="text" class="form-control" name="email0" value="<?= $email ?>" disabled="disabled" id="oldemail" />
                                        </div>

                                        <div class="form-group">
                                            <label for="newemail"><?= _t('users', 'Новый email') ?></label>
                                            <input type="text" class="form-control j-required" name="email" maxlength="100" id="newemail" placeholder="<?= _t('users', 'Введите свой новый email'); ?>" />
                                        </div>

                                        <div class="form-group">
                                            <label for="curpass"><?= _t('users', 'Текущий пароль') ?></label>
                                            <input type="text" class="form-control j-required" name="pass" maxlength="100" autocomplete="off" id="curpass" placeholder="<?= _t('users', 'Введите свой текущий пароль'); ?>" />
                                        </div>

                                        <div class="c-formSubmit">
                                            <button class="btn btn-primary c-formSuccess j-submit" type="submit"><i class="fa fa-check"></i> <?= _t('form', 'Сохранить'); ?></button>
                                        </div>

                                    </form>
                                </div>
                                <? endif; ?>
                            </div>
                        </div>

                        <? if(Users::registerPhone() || $on['destroy']): ?>
                        <div class="p-profileContent-item">
                            <div class="row">

                                <? if(Users::registerPhone()): ?>
                                <div class="col-sm-6">
                                    <form action="" role="form" class="j-form-phone">
                                        <input type="hidden" name="act" value="phone" />
                                        <input type="hidden" name="step" value="code-send" />

                                        <h5 class="mrgt0"><?= _t('users', 'Изменить номер телефона') ?></h5>

                                        <div class="form-group">
                                            <label for="oldphone"><?= _t('users', 'Ваш номер') ?></label>
                                            <input type="text" class="form-control" name="phone0" value="<?= (!empty($phone_number) ? '+'.$phone_number : _t('users', 'Не указан')) ?>" disabled="disabled" id="oldphone"/>
                                        </div>

                                        <div class="form-group j-required j-step1">
                                            <label for="j-u-register-phone"><?= _t('users', 'Новый номер') ?></label>
                                            <?= $this->registerPhoneInput(array('id'=>'j-u-register-phone','name'=>'phone')) ?>
                                        </div>

                                        <div class="form-group hidden j-step2">
                                            <label for="code"><?= _t('users', 'Введите код из sms') ?></label>
                                            <input type="text" class="form-control j-phone-change-code"  name="code"  value="" id="code"/>

                                            <div class="i-control-links">
                                                <a href="#" class="ajax j-phone-change-repeate"><?= _t('users', 'Выслать код повторно') ?></a>
                                                <a href="#" class="ajax j-phone-change-back"><?= _t('users', 'Изменить номер') ?></a>
                                            </div>
                                        </div>

                                        <div class="c-formSubmit">
                                            <button class="btn btn-primary c-formSuccess j-submit" type="submit"><i class="fa fa-check"></i> <?= _t('form', 'Сохранить'); ?></button>
                                        </div>
                                    </form>
                                </div>
                                <? endif; ?>

                                <? if($on['destroy']): ?>
                                <div class="col-sm-6">
                                    <form action="" role="form" class="j-form-destroy">
                                        <input type="hidden" name="act" value="destroy" />

                                        <h5 class="mrgt0"><?= _t('users', 'Удалить учетную запись?') ?></h5>

                                        <div class="form-group">
                                            <label for="curpass"><?= _t('users', 'Текущий пароль') ?></label>
                                            <input type="text" class="form-control j-required" name="pass" maxlength="100" autocomplete="off" id="curpass" placeholder="<?= _t('users', 'Введите свой текущий пароль'); ?>" />
                                        </div>

                                        <div class="c-formSubmit">
                                            <button class="btn btn-default btn-sm c-formSuccess j-submit" type="submit"><?= _t('form', 'Удалить запись'); ?></button>
                                        </div>
                                    </form>
                                </div>
                                <? endif; ?>
                            </div>
                        </div>
                        <? endif; ?>
                    </div>

                    <? if($verifiedEnabled): $allowEdit = $verified == Users::VERIFIED_STATUS_NONE || $verified == Users::VERIFIED_STATUS_DECLINED; ?>
                    <div class="p-profileContent j-settings-tab j-settings-tab-verified<?= $t != 'verified' ? ' displaynone' : ''?>">
                        <form class="form-horizontal j-form-verified" role="form">
                            <input type="hidden" name="act" value="verified" />

                            <section class="p-profileCabinet-section">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label o-control-label"><?= _t('users', 'Статус'); ?></label>
                                    <div class="col-sm-9">
                                        <? switch($verified){case Users::VERIFIED_STATUS_APPROVED: ?>
                                        <div class="alert alert-success">
                                            <?= _t('users', 'Вы успешно подтвердили личные данные профиля.'); ?>
                                        </div>
                                        <? break; case Users::VERIFIED_STATUS_WAIT: ?>
                                        <div class="alert alert-warning">
                                            <?= _t('users', 'Заявка находится на обработке у модератора.'); ?>
                                        </div>
                                        <? break; case Users::VERIFIED_STATUS_DECLINED: ?>
                                        <div class="alert alert-danger">
                                            <strong><?= _t('users', 'Модератор отклонил вашу заявку по причине:'); ?></strong>
                                            <div><?= $verified_reason['message'] ?></div>
                                        </div>
                                        <? break; default: ?>
                                        <div class="alert alert-default"><?= _t('users', 'Вы еще не подтвердили ваши личные данные.'); ?></div>
                                        <? break; } ?>
                                        <?= $allowEdit ? config::get('users_verified_requirement_'.LNG, '') : '' ?>
                                    </div>
                                </div>
                                <? if($allowEdit): $verifiedImages = $this->verifiedImages(User::id()) ?>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label o-control-label"><?= _t('users', 'Документы'); ?></label>
                                        <div class="col-sm-9">
                                            <div class="c-files-add">
                                                <ul class="mrgb10 j-files">
                                                    <? if( ! empty($verified_images)): foreach($verified_images as $v): ?>
                                                        <li><a href="#" class="link-red j-delete" data-fn="<?= $v['filename'] ?>"><i class="fa fa-times"></i></a> <a href="<?= $v['link'] ?>" target="_blank" class="j-fancy-box" rel="fancy-gallery-verefied" ><?= $v['filename'] ?></a> </li>
                                                    <? endforeach; endif; ?>
                                                </ul>
                                                <button type="button" class="btn btn-sm btn-default j-add"<?= count($verified_images) >= $verifiedImages->getLimit() ? ' style="display:none;"' : '' ?>><?= _t('', 'Добавить'); ?></button>
                                            </div>
                                            <div class="help-block">
                                                <?= _t('', 'Формат: jpg, gif, png. Максимальный размер файла: [size].', array('size'=>$verifiedImages->getMaxSize(true))) ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3 c-formSubmit">
                                            <button class="btn btn-primary c-formSuccess j-submit"><?= _t('', 'Отправить'); ?></button>
                                            <a class="c-formCancel j-cancel" href="<?= Users::url('my.profile'); ?>"><?= _t('form', 'Отмена'); ?></a>
                                        </div>
                                    </div>
                                <? endif; ?>

                            </section>

                        </form>
                    </div>
                    <? endif; ?>

                    <?= $fairplayEnabled ? Fairplay::i()->my_settings_finance($t == 'finance' ? 1 : 0) : '' ?>

                    <?= bff::filter('users.my.settings.tabs.html', '', array('t'=>&$t, 'aData'=>&$aData)) ?>

                </div><!-- /.col-md-8 -->

            </div><!-- /.row -->

        </section><!-- /.main-content -->

    </div>
<script type="text/javascript">
    <? $reg1_country = ( $countrySelect && $reg1_country ? $reg1_country : Geo::defaultCountry() ); ?>
    <? js::start() ?>
    jMySettings.init(<?= func::php2js(array(
        'url_settings' => Users::url('my.settings'),
        'url_social' => Users::url('login.social'),
        'lang' => array(
            'saved_success' => _t('users', 'Настройки успешно сохранены'),
            'ava_upload_messages' => array(
                'typeError' => _t('form', 'Допустимы только следующие типы файлов: {extensions}'),
                'sizeError' => _t('form', 'Файл {file} слишком большой, максимально допустимый размер {sizeLimit}'),
                'minSizeError' => _t('form', 'Файл {file} имеет некорректный размер'),
                'emptyError' => _t('form', 'Файл {file} имеет некорректный размер'),
                'onLeave' => _t('form', 'Происходит загрузка изображения, если вы покинете эту страницу, загрузка будет прекращена'),
            ),
            'ava_upload' => _t('users', 'Загрузка фотографии'),
            'phones_tip' => _t('users', 'Номер телефона'),
            'phones_plus' => _t('users', '+ ещё<span [attr]> телефон</span>', array('attr'=>'class="hidden-phone"')),
            'pass_changed' => _t('users', 'Пароль был успешно изменен'),
            'pass_confirm' => _t('users', 'Ошибка подтверждения пароля'),
            'email_wrong' => _t('users', 'E-mail адрес указан некорректно'),
            'email_diff' => _t('users', 'E-mail адрес не должен совпадать с текущим'),
            'email_changed' => _t('users', 'E-mail адрес был успешно изменен'),
            'account_destoyed' => _t('users', 'Ваш аккаунт был успешно удален'),
            'existsMessage'    => _t('users', 'Вы уже выбрали эту специализацию')
        ),
        //avatar
        'avatarMaxsize' => $avatar_maxsize,
        'avatarSzSmall' => UsersAvatar::szSmall,
        'avatarSzNormal' => UsersAvatar::szNormal,
        'avatarUploadProgress'  => '<div class="p-img-loading j-progress"> <img alt="" src="'.bff::url('/img/loader.gif').'"> </div>',
        //contacts
        'contactsLimit' => Users::profileContactsLimit(),
        'contactsData' => $contacts,
        'contactsTypesOptions' => HTML::selectOptions(Users::aContactsTypes(), 0, false, 'id', 't'),
        //geo
        'reg3_city'    => $reg3_city,
        'reg1_country' => $reg1_country,
        'geoCountry' => Geo::regionTitle($reg1_country),
        //settings
        'specCnt' => $nSpecCnt, // кол-во выбранных специализаций
        'specLimit' => $nSpecLimit, // кол-во допустимых специализаций
        'specsServices' => Specializations::useServices(Specializations::SERVICES_PRICE_USERS),
        'openSpecServices' => $this->input->get('spec_service', TYPE_UINT),
        'verified_limit' => isset($verifiedImages) ? $verifiedImages->getLimit() : 0
    )) ?>);
    <? if( ! empty($verified_images)):     tpl::includeJS('fancybox2', true); ?>
    $(function() {
        $('.j-fancy-box').fancybox({
            openEffect	: 'none',
            closeEffect	: 'none',
            nextEffect  : 'fade',
            prevEffect : 'fade',
            fitToView: true,
            helpers: {
                overlay: {locked: false}
            }
        });
    });
    <? endif; ?>
    <? js::stop() ?>
</script>