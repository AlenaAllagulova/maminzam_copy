<?php
    /**
     * Форма управления заметкой о пользователе
     * @var $this Users
     */
    tpl::includeJS('users.note', false, 2);
    if( ! isset($id)) { $aData['id'] = 0; }
    if( ! isset($note)) { $aData['note'] = ''; }
?>
<?= $this->viewPHP($aData, 'my.note.block'); ?>
<script type="text/javascript">
<? js::start() ?>
    jUsersNote.init(<?= func::php2js(array(
        'lang' => array(
    ),
)) ?>);
<? js::stop() ?>
</script>
