<?php
/** @var $this Users */
    $userID = User::id();
    $isOwner = User::isCurrent($id);
    $isWorker = ($type == Users::TYPE_WORKER);
    if ($isOwner && $isWorker) {
        tpl::includeJS('users.status', false, 1);
    }
    $bAllowFav = ($userID > 0);
    if ($bAllowFav) {
        if (Users::useClient()) {
            $bAllowFav = !User::type($type);
        }
        if ($bAllowFav) tpl::includeJS('users.fav', false, 2);
    }
    $aMainSpec = false;
    if ($isWorker && ! empty($specs)) {
        $aMainSpec = reset($specs);
        if ( ! empty($aMainSpec['price_rate_text'])) {
            $aMainSpec['price_rate_text'] = func::unserialize($aMainSpec['price_rate_text']);
        }
    }
    $bShowStat = ($isWorker && $isOwner);
    if ($bShowStat) {
        tpl::includeJS('user.views', false, 2);
    }
    $rolesEnabled = Users::rolesEnabled();
    $inviteEnabled = false;
    if (Orders::invitesEnabled()) {
        do {
            if ( ! $userID || ! $isWorker) break;
            if (Users::useClient() && ! User::isClient()) break;
            tpl::includeJS('orders.invite', false, 2);
            tpl::includeJS('specs.select', false, 5);
            $inviteEnabled = true;
        } while(false);
    }
    $rolePrivate = true;
?>
<div class="l-borderedBlock p-profileColumn">

    <div class="l-inside p-profile-top">
        <div class="p-profile-avatar">
            <img src="<?= $avatar ?>" alt="<?= tpl::avatarAlt($aData) ?>" />
            <?= tpl::userOnline($aData, 'c-status-lg') ?>
        </div>
        <?
        $sNameClose = '';
        if (empty($name) && empty($surname)) {
            $sName = '['.$login.']';
        } else {
            if(empty($surname)){
                $sName = $name;
            }else {
                $sName = $name . ' <span class="nowrap">' . $surname;
                $sNameClose = '</span>';
            }
        }
        ?>
        <h6><?= $sName ?><?
            if($pro): ?> <span class="pro"><?= _t('', 'pro'); ?></span><? endif;
            if($verified == Users::VERIFIED_STATUS_APPROVED && Users::verifiedEnabled()): ?> <i class="fa fa-check-circle show-tooltip c-verified" data-toggle="tooltip" data-placement="top" data-container="body" title="<?= _t('users', 'Подтвержденный профиль'); ?>" data-original-title="<?= _t('users', 'Подтвержденный профиль'); ?>"></i><? endif;
            ?><?= $sNameClose ?></h6>

        <div id="j-user-status">
            <? if($isOwner):
                if($isWorker):
                    $aStatus = Users::aWorkerStatus(); ?>
                <span class="label <?= $aStatus[$status]['c']?> dropdown">
                    <a href="#" data-toggle="dropdown"><span id="j-user-status-title"><?= $aStatus[$status]['t']?></span> <b class="caret"></b></a>
                    <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                        <? foreach($aStatus as $v): ?>
                        <li <?= $v['id'] == $status ? 'class="active"' : '' ?>><a href="#" class="j-status" data-id="<?= $v['id'] ?>"><?= $v['t'] ?></a></li>
                        <? endforeach; ?>
                    </ul>
                </span>

                <div class="p-status-text">

                    <div id="j-user-status-show">
                        <div class="j-text"><?= nl2br($status_text) ?></div>
                        <div class="p-status-edit">
                            <a href="#" class="ajax-link j-edit"><i class="fa fa-pencil c-link-icon"></i><span><?= _t('form', 'Редактировать'); ?></span></a>
                        </div>
                    </div>

                    <div id="j-user-status-form-block" style="display:none;">
                        <form action="" method="post">
                            <textarea name="status_text" class="form-control" rows="3"><?= $status_text ?></textarea>
                            <div class="c-formSubmit">
                                <div id="j-status-message" class="help-block"></div>
                                <button class="btn btn-primary btn-sm c-formSuccess j-submit"><?= _t('form', 'Сохранить'); ?></button>
                                <a href="#" class="ajax-link c-formCancel j-cancel"><span><?= _t('form', 'Отмена'); ?></span></a>
                            </div>
                        </form>
                    </div>
                </div>
                <script type="text/javascript">
                <? js::start() ?>
                    jUsersWorkerStatus.init(<?= func::php2js(array(
                        'lang' => array(
                            'left'    => _t('users','[symbols] осталось'),
                            'symbols' => explode(';', _t('users', 'знак;знака;знаков')),
                        ),
                    )) ?>);
                <? js::stop() ?>
                </script>
            <? endif;
            else:
                if($isWorker): $aStatus = Users::aWorkerStatus(); ?>
                    <span class="label <?= $aStatus[$status]['c']?>"><?= $aStatus[$status]['t']?></span>
                    <? if( ! empty($status_text)): ?><div class="p-status-text"><div><?= nl2br($status_text) ?></div></div><? endif; ?>
                <? endif; ?>
                <div>
                    <? if($userID): ?><a href="<?= InternalMail::url('my.chat', array('user' => $login)) ?>" class="btn btn-block btn-sm btn-secondary"><i class="fa fa-envelope-o"></i> <?= _t('users', 'Написать сообщение'); ?></a><? endif; ?>
                    <? if($inviteEnabled): ?><a href="#" class="btn btn-block btn-sm btn-default j-order-invite" data-toggle="modal"><i class="fa fa-clipboard"></i> <?= _t('orders', 'Предложить заказ'); ?></a><? endif; ?>
                </div>
                <? if($bAllowFav): $bIsFav = Users::model()->isUserFav($userID, $id); ?>
                    <a href="#" class="ajax-link j-add-fav<?= $bIsFav ? ' hidden' : '' ?>"
                       data-id="<?= $id ?>">
                        <i class="fa fa-star"></i>
                        <span>
                            <?= _t('users', 'Добавить в избранное'); ?>
                        </span>
                    </a>
                    <a href="#" class="ajax-link link-delete j-del-fav<?= $bIsFav ? '' : ' hidden' ?>" data-id="<?= $id ?>"><i class="fa fa-times"></i> <span><?= _t('users', 'Удалить из избранных'); ?></span></a>

                    <script type="text/javascript">
                        <? js::start() ?>
                        jUsersFav.init(<?= func::php2js(array(
                            'lang' => array(),
                        )) ?>);
                        <? js::stop() ?>
                    </script>
                <? endif; ?>
                <?= $this->note($id, array('bFav' => 1, 'classes' => 'mrgt10')); ?>
            <? endif; ?>
        </div>
    </div>

    <a class="p-profile-collapse-toggle text-center ajax-link" data-toggle="collapse" href="#profile-collapse"><span><?= _t('users', 'Показать информацию'); ?></span></a>
    <div class="p-profile-collapse collapse" id="profile-collapse">

        <div class="l-inside">
            <ul class="p-profile-info-list">
                <? if($isWorker): $sGend = ($sex == Users::SEX_FEMALE ? _t('','-ая') : _t('','-й')); ?>
                    <? if($position): ?><li><i class="fa fa-list-alt"></i> <?= _t('', 'На сайте:'); ?> <?= $position?><?= $sGend ?></li><? endif; ?>
                    <li><i class="fa fa-bar-chart-o"></i> <?= _t('users', 'Рейтинг:'); ?> <span class="text-success"><?= Users::rating($rating, $pro) ?></span></li>
                    <? if($aMainSpec): ?>
                        <? if( ! empty($aMainSpec['price'])): ?>
                            <li><i class="fa fa-clock-o"></i> <?= tpl::formatPrice($aMainSpec['price']) ?> <?= Site::currencyData($aMainSpec['price_curr'], 'title_short'); ?><?=
                                ! empty($aMainSpec['price_rate_text'][LNG]) ? '/'.$aMainSpec['price_rate_text'][LNG] : '' ?></li>
                        <? endif; ?>
                        <? if( ! empty($aMainSpec['budget'])): ?>
                            <li><i class="fa fa-dollar"></i> <?= _t('users', 'Бюджет от'); ?> <?= tpl::formatPrice($aMainSpec['budget']) ?>
                                <?= Site::currencyData($aMainSpec['budget_curr'], 'title_short'); ?></li>
                        <? endif; ?>
                    <? endif; ?>
                <? endif; ?>
                <li class="o-feedbacks-inf"><i class="fa fa-comments-o"></i> <?= _t('opinions', 'Opinions:'); ?>
                    <span id="j-user-opinions-cache"><?= tpl::opinions($opinions_cache, array('login' => $login)) ?></span>
                </li>
                <? if($fav_cnt > 0){ ?><li><i class="fa fa-star"></i> <?= _t('users', 'В избранных у'); ?> <a href="<?= Users::url('user.info', array('login' => $login)) ?>" id="j-fav-cnt"><?= $fav_cnt ?></a></li><? } ?>
                <? if($bShowStat): ?><li class="hidden-xs hidden-sm"><i class="fa fa-line-chart"></i> <a href="#" data-toggle="modal" class="ajax-link" id="j-show-stat"><span><?= _t('users', 'Посмотреть статистику'); ?></span></a></li><? endif; ?>
            </ul>
        </div><!-- /.l-inside -->

        <? if($isWorker && ! empty($specs)): ?>
        <div class="l-inside">
            <h6><?= _t('', 'Специализации'); ?></h6>
            <? $aSpecs = array(); $fst = true;
            foreach ($specs as $v) { if($v['disabled']) continue;
                $aUrl = array('main_spec_keyword' => $v['spec_keyword']);
                if(Specializations::catsOn()){
                    $aUrl['main_cat_keyword'] = $v['cat_keyword'];
                }
                $aSpecs[] = '<a href="'.Users::url('search-spec', $aUrl).'"><small>'.($fst ? '<b>' : '').$v['spec_title'].($fst ? '</b>' : '').'</small></a>'.($v['position'] ? ' ('.$v['position'].$sGend.')' : '');
                $fst = false;
            }
            ?>
            <?= join(', <br />', $aSpecs)?>
        </div>
        <? endif; ?>
        <? if( ! empty($reg3_city)): ?>
        <div class="l-inside">
            <h6><?= _t('', 'Местоположение'); ?></h6>
            <ul class="p-profile-info-list">
                <li><i class="fa fa-map-marker"></i> <?= ! empty($city_data['title']) ? $city_data['title'] : '' ?><?= $bCountry && ! empty($country_data['title']) ? ', '.$country_data['title'] : ''?></li>
            </ul>
        </div>
        <? endif; ?>

        <div class="l-inside">
            <? if($isWorker): if($rolesEnabled) $rolePrivate = ($role_id == Users::ROLE_PRIVATE); ?><h6><?= $rolesEnabled && ! $rolePrivate ? _t('users', 'Опыт') : _t('users', 'Возраст и опыт'); ?></h6><? endif; ?>
            <ul class="p-profile-info-list">
                <? if($rolePrivate && Users::profileBirthdate() && $birthdate != '1901-01-01' && $birthdate != '0000-00-00'){ ?><li><?= _t('users', 'Возраст:'); ?> <strong><?= tpl::date_format_spent($birthdate, false, false) ?></strong></li><? } ?>
                <? if($isWorker || ! Users::useClient()){ $aExperience = Users::aExperience(); ?><li><?= _t('users', 'Опыт:'); ?> <strong><?= $aExperience[$experience]['t'] ?></strong></li><? } ?>
                <li><?= _t('users', 'На сайте:'); ?> <strong><?= tpl::date_spent_month($created) ?></strong></li>
                <? if($last_activity != '0000-00-00 00:00:00'):?><li><?= $sex == Users::SEX_FEMALE && $rolePrivate ? _t('users', 'Заходила:') : _t('users', 'Заходил:') ?> <strong><?= tpl::date_format_spent($last_activity, true, true, true, array('lng' => array('now' => _t('users', 'сейчас на сайте')))) ?></strong></li><? endif; ?>
            </ul>
        </div>

        <? if( ! empty($contacts)): ?>
        <div class="l-inside">
            <h6><?= _t('users', 'Контакты'); ?></h6>
            <? if(Users::isContactsView($id, $type, $pro, $contactsHideReason, 'mrgb0')): ?>
            <!--noindex-->
            <ul class="p-profile-info-list">
                <? foreach($contacts as $v): ?>
                <li><?= tpl::linkContact($v); ?></li>
                <? endforeach; ?>
            </ul>
            <!--/noindex-->
            <? else: ?>
                <?= $contactsHideReason ?>
            <? endif; ?>
        </div>
        <? endif; ?>

        <? if( ! empty($tags)): ?>
        <div class="l-inside">
            <h6><?= _t('', 'Навыки'); ?></h6>
            <div class="p-profile-info-list">
                <? foreach($tags as $v): ?>
                <a href="<?= Users::url('search-tag', $v) ?>" class="l-tag"><?= $v['tag'] ?></a>
                <? endforeach; ?>
            </div>
        </div>
        <? endif; ?>

    </div>
</div>
<script type="text/javascript">
<? js::start() ?>
<?  if($bShowStat): ?>
jUserViews.init(<?= func::php2js(array(
    'lang' => array(
        //
    ),
    'id' => $id,
)) ?>);
<?  endif;
if ($inviteEnabled):
        $aTerms = Orders::aTerms(); $aTermsText = array();
        foreach ($aTerms as $v) {
            if ($v['days']) {
                $aTermsText[ $v['id'] ] = tpl::date_format2(time() + $v['days'] * 24*60*60, false, true);
            } else {
                $aTermsText[ $v['id'] ] = false;
            }
        }
    ?>
    jOrdersInvite.init(<?= func::php2js(array(
        'lang' => array(
            'spec_wrong'     => _t('orders', 'Укажите специализацию'),
            'invite_success' => _t('orders', 'Предложение было успешно отправлено'),
        ),
        'user_id' => $id,
        'terms' => $aTermsText,
    )) ?>);
<? endif; ?>
<? js::stop() ?>
</script>