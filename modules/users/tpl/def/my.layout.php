    <div class="container">

        <section class="l-mainContent">
            <div class="row">

                <aside class="col-md-3">

                    <?= $profile ?>

                </aside>

                <div class="col-md-9 l-content-column">

                    <?= tpl::getTabs(array('tabs' => $tabs, 'a' => $tab, 'class' => 'j-my-cabinet-tab', 'noAjax' => 1)); ?>

                    <?= $content ?>

                </div>

            </div>
        </section>

    </div>