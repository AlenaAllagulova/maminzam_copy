<?php
    /**
     * Регистрация пользователя через соц. сети
     * @var $this Users
     */
$phone_on = Users::registerPhone();
?>

<div class="s-signBlock-form">

    <div class="row">
        <div class="col-md-2 col-md-offset-1">
            <div class="s-signBlock-social-avatar">
                <img src="<?= $avatar ?>" alt="" />
            </div>
        </div>
        <div class="col-md-9">
            <div class="s-signBlock_hello j-social">
                <?= _t('users','Здравствуйте, <strong>[name]</strong>!', array('name'=>$name)) ?>
            </div>
            <div class="s-signBlock_hello displaynone j-social">
                <?= _t('users', 'У вас уже есть профиль на [site_name]?', array('site_name'=>Site::title('users.register.social'))) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 s-socialBlock_confirm">

            <form action="" class="form" role="form" id="j-u-register-social-form" autocomplete="off">
                <div class="form-group">
                    <input type="email" name="email" class="form-control j-required" id="j-u-register-social-email" autocomplete="off" placeholder="<?= _t('users','Введите Ваш Email') ?>" maxlength="100" />
                    <a class="link-ajax j-social" style="display:none;" id="j-u-register-social-email-change" href="#"><?= _t('users','Изменить e-mail') ?></a>
                </div>
                <? if($phone_on): ?>
                <div class="form-group">
                    <?= $this->registerPhoneInput(array('id'=>'j-u-register-phone','name'=>'phone', 'placeholder' => _t('users','Введите номер Вашего телефона'))) ?>
                </div>
                <div class="form-group j-step2 hidden">
                    <input type="text" class="form-control" name="code" autocomplete="off" placeholder="<?= _t('users', 'Введите код из sms') ?>"/>
                    <a class="link-ajax j-phone-repeate" href="#"><?= _t('users', 'Выслать код повторно') ?></a>
                </div>
                <? endif; ?>
                <? $usersTypes = Users::aTypes(); if (sizeof($usersTypes) > 1): ?>
                    <? foreach($usersTypes as $v):?>
                        <div class="radio-inline  j-social">
                            <label>
                                <input type="radio" name="type" value="<?= $v['id'] ?>" autocomplete="off" />
                                <?= $v['t'] ?>
                            </label>
                        </div>
                    <? endforeach; ?>
                <? else: ?>
                    <input type="radio" name="type" value="<?= key($usersTypes) ?>" checked="checked" style="display: none;" />
                <? endif; ?>

                <div class="form-group displaynone j-social">
                    <input type="password" name="pass" class="form-control" id="j-u-register-social-pass" placeholder="<?= _t('users','Введите ваш пароль') ?>" autocomplete="off" maxlength="100" />
                    <a class="link-ajax" href="<?= Users::url('forgot', array('social'=>1)) ?>"><?= _t('users','Забыли пароль?') ?></a>
                </div>

                <div class="form-group j-social">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="agreement" id="j-u-register-social-agreement" autocomplete="off" /><?= _t('users', 'Я соглашаюсь с <a href="[link_agreement]" target="_blank">правилами использования сервиса</a>, а также с передачей и обработкой моих данных.', array('link_agreement'=>Users::url('agreement'))) ?><i class="text-danger">*</i>
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary j-social"><?= _t('users','Завершить регистрацию') ?></button>
                <button type="submit" class="btn btn-primary displaynone j-social"><?= _t('users','Объединить профили') ?></button>
            </form>

        </div>
    </div>

</div>
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    jUserAuth.registerSocial(<?= func::php2js(array(
        'login_url' => Users::url('login'),
        'lang' => array(
            'register'=>array(
                'title' => _t('users', 'Для завершения регистрации введите Вашу электронную почту'),
                'email' => _t('users', 'E-mail адрес указан некорректно'),
                'agreement' => _t('users', 'Пожалуйста подтвердите, что Вы согласны с пользовательским соглашением'),
                'type' => _t('users', 'Пожалуйста укажите тип пользователя'),
            ),
            'login'=>array(
                'title' => _t('users', 'Пользователь с таким e-mail адресом уже зарегистрирован.'),
                'pass' => _t('users', 'Укажите пароль'),
            )
        ),
        'phone' => $phone_on,
    )) ?>);
});
<? js::stop(); ?>
</script>