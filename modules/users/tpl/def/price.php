<?php
tpl::includeJS('users.price', false);
?>
<div class="container">
    <section class="l-mainContent" id="j-users-price">
        <div class="row">
            <aside class="col-md-3">
                <?= $form ?>
            </aside>
            <div class="col-md-9 l-content-column" id="j-users-price-list">

                <header class="title-type-2">
                    <h1 class="mrgt0"><?= _t('users', 'Прайс на услуги по специализациям'); ?></h1>
                </header>

                <div class="j-list"><?= $list ?></div>
                <div class="j-pagination"><?= $pgn ?></div>

            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
<? js::start() ?>
    jUsersPrice.init(<?= func::php2js(array(
        'lang' => array(),
        'ajax' => true,
        'defCountry'    => Geo::defaultCountry(),
        'preSuggest'    => Geo::countrySelect() && $f['c'] ? Geo::regionPreSuggest($f['c'], 2) : '',
    )) ?>);
<? js::stop() ?>
</script>
<?
