<?php
/**
 * Регистрация пользователя
 * @var $this Users
 */
tpl::includeJS('users.auth', false, 5);
$rolesEnabled = Users::rolesEnabled();
$registerPhone = Users::registerPhone();
$showPassword = true;
$showType = true;
$forceRole = false;
$md5 = '-5';
$md7 = '-7';
$isEmbedded = ! empty($embedded);
if ($isEmbedded) {
    $md5 = '-3';
    $md7 = '-4';
    $forceRole = isset($embedded['force_role']);
    $showPassword = empty($embedded['generate_password']);
    if ( ! $showPassword) {
        $pass_confirm_on = false;
    }
    if ( ! empty($embedded['no_captcha'])) {
        $captcha_on = false;
    }
    $showType = ! isset($embedded['force_type']);
}
?>
<? if( ! $isEmbedded): ?>
<div class="s-signBlock-form">
    <div class="row">
        <div class="col-sm-6 s-signBlock-div">

            <form action="" class="form-horizontal" role="form" id="j-u-register-form">
                <input type="hidden" name="back" value="<?= HTML::escape($back) ?>" />
                <? endif; # ==begin embedded block ?>
                <? if($rolesEnabled): ?>
                    <? if( ! $forceRole): ?>
                    <div class="form-group">
                        <label class="col-md<?= $md5 ?> control-label"><?= _t('users', 'Тип аккаунта'); ?></label>
                        <div class="col-md<?= $md7 ?>">
                            <div class="btn-group" data-toggle="buttons">
                                <? foreach(Users::roles() as $v): ?>
                                    <label class="btn btn-default <?= $v['default'] ? 'active' : '' ?>">
                                        <input type="radio" name="role_id" value="<?= $v['id'] ?>" <?= $v['default'] ? 'checked="checked"' : '' ?>> <?= $v['t'] ?>
                                    </label>
                                <? endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <? endif; ?>
                    <div class="form-group">
                        <label for="inputFname" class="col-md<?= $md5 ?> control-label j-name"><?= _t('users', 'Имя'); ?></label>
                        <div class="col-md<?= $md7 ?>">
                            <input type="text" name="name" class="form-control j-required" placeholder="<?= _t('', 'Ваше имя'); ?>">
                        </div>
                    </div>
                <? endif; ?>
                <div class="form-group">
                    <label for="j-u-register-email" class="col-md<?= $md5 ?> control-label"><?= _t('users', 'Электронная почта') ?></label>
                    <div class="col-md<?= $md7 ?>">
                        <input type="email" name="email" class="form-control j-required" id="j-u-register-email" autocomplete="off" placeholder="<?= _t('users', 'Введите ваш email') ?>" maxlength="100" />
                    </div>
                </div>
                <? if(Users::profilePhoneRequired()): ?>
                    <div class="form-group">
                        <label for="j-u-register-phone" class="col-md<?= $md5 ?> control-label"><?= _t('users', 'Телефон') ?></label>
                        <div class="col-md<?= $md7 ?>">
                            <input type="text" name="phone" class="form-control j-required" id="j-u-register-phone" autocomplete="off" placeholder="<?= _t('users', 'Введите ваш телефон') ?>" maxlength="16" />
                        </div>
                    </div>
                <? endif; ?>
                <? if($phone_on): ?>
                    <div class="form-group">
                        <label for="inputPhone" class="col-md<?= $md5 ?> control-label"><?= _t('users', 'Телефон') ?></label>
                        <div class="col-md<?= $md7 ?>">
                            <?= $this->registerPhoneInput(array('id'=>'j-u-register-phone','name'=>'phone')) ?>
                        </div>
                    </div>
                <? endif; ?>
                <? if($showPassword): ?>
                <div class="form-group">
                    <label for="j-u-register-pass" class="col-md<?= $md5 ?> control-label"><?= _t('users', 'Пароль') ?></label>
                    <div class="col-md<?= $md7 ?>">
                        <input type="password" name="pass" class="form-control j-required" id="j-u-register-pass" autocomplete="off" placeholder="<?= _t('users', 'Введите ваш пароль') ?>" maxlength="100" />
                    </div>
                </div>
                <? if($pass_confirm_on): ?>
                    <div class="form-group">
                        <label for="j-u-register-pass2" class="col-md<?= $md5 ?> control-label"><?= _t('users', 'Пароль еще раз') ?></label>
                        <div class="col-md<?= $md7 ?>">
                            <input type="password" name="pass2" class="form-control j-required" id="j-u-register-pass2" autocomplete="off" placeholder="<?= _t('users', 'Повторите пароль') ?>" maxlength="100" />
                        </div>
                    </div>
                <? endif;
                endif;
                if($captcha_on): ?>
                    <div class="form-group">
                        <label for="j-u-register-captcha" class="col-md<?= $md5 ?> control-label"><?= _t('users', 'Результат с картинки') ?></label>
                        <div class="col-md<?= $md7 ?>">
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="text" name="captcha" id="j-u-register-captcha" autocomplete="off" class="form-control j-required" value="" />
                                </div>
                                <div class="col-xs-6">
                                    <div class="s-captcha">
                                        <img src="<?= tpl::captchaURL() ?>" class="j-captcha" onclick="$(this).attr('src', '<?= tpl::captchaURL() ?>&rnd='+Math.random())" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endif; ?>

                <? if($showType):
                $usersTypes = Users::aTypes(); if (sizeof($usersTypes) > 1): ?>
                <div class="row">
                    <div class="col-md-offset<?= $md5 ?> col-md<?= $md7 ?>">
                        <? foreach($usersTypes as $v):?>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="type" value="<?= $v['id'] ?>" autocomplete="off" />
                                <?= $v['t'] ?>
                            </label>
                        </div>
                        <? endforeach; ?>
                    </div>
                </div>
                <? else: ?>
                    <input type="radio" name="type" value="<?= key($usersTypes) ?>" checked="checked" style="display: none;" />
                <? endif;
                endif; ?>

                <? if($agreement_on): ?>
                <div class="form-group">
                    <div class="col-md-offset<?= $md5 ?> col-md-7">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="agreement" id="j-u-register-agreement" autocomplete="off" /><?= _t('users', 'Я соглашаюсь с <a href="[link_agreement]" target="_blank">правилами использования сервиса</a>, а также с передачей и обработкой моих данных.', array('link_agreement'=>Users::url('agreement'))) ?><i class="text-danger">*</i>
                            </label>
                        </div>
                    </div>
                </div>
                <? endif; ?>

                <? if( ! $isEmbedded): # ==end embedded block ?>
                <div class="row">
                    <div class="col-md-offset-5 col-md-7 help-block">
                        <?= _t('', 'Все поля обязательны для заполнения') ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-5 col-md-7">
                        <button type="submit" class="btn btn-primary j-submit"><?= _t('users', 'Зарегистрироваться') ?></button>
                    </div>
                </div>
            </form>

        </div>

        <div class="col-sm-6 s-signBlock-div s-social-buttons">
            <? foreach($providers as $v) { ?>
                <a href="#" class="btn btn-sm s-btn-social s-<?= $v['class'] ?> j-u-login-social-btn" data="{provider:'<?= $v['key'] ?>',w:<?= $v['w'] ?>,h:<?= $v['h'] ?>}"><?= $v['title'] ?></a>
            <? } ?>
            <?  ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="s-signBlock-other">
                <div class="row">
                    <div class="col-sm-6">
                        <h6><?= _t('users', 'Уже зарегистрированы?') ?></h6>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a class="btn btn-default btn-sm" href="<?= Users::url('login') ?>"><?= _t('users', 'Войти') ?></a>
                    </div>
                </div>
            </div><!-- /.s-signBlock-other -->
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="s-signBlock-other">
                <div class="row">
                    <div class="col-sm-6">
                        <h6><?= _t('users', 'Забыли пароль?') ?></h6>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a class="btn btn-default btn-sm" href="<?= Users::url('forgot') ?>"><?= _t('users', 'Восстановить пароль') ?></a>
                    </div>
                </div>
            </div><!-- /.s-signBlock-other -->
        </div>

    </div><!-- /.row -->

</div><!-- /.signBlock-form -->
<? endif; ?>
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    jUserAuth.register(<?= func::php2js(array(
        'embedded' => $isEmbedded,
        'embedded_form' => ($isEmbedded ? $embedded['form'] : ''),
        'captcha' => $captcha_on,
        'agreement' => $agreement_on,
        'pass_confirm' => $pass_confirm_on,
        'phone' => $phone_on,
        'login_social_url' => Users::url('login.social'),
        'login_social_return' => ! empty($back) ? $back : '',
        'lang' => array(
            'email' => _t('users', 'E-mail адрес указан некорректно'),
            'pass' => _t('users', 'Укажите пароль'),
            'pass2' => _t('users', 'Пароли должны совпадать'),
            'captcha' => _t('users', 'Введите результат с картинки'),
            'agreement' => _t('users', 'Пожалуйста подтвердите, что Вы согласны с пользовательским соглашением'),
            'type' => _t('users', 'Пожалуйста укажите тип пользователя'),
            'phone' => _t('users', 'Номер телефона указан некорректно'),
            'name'  => array(
                'r'.Users::ROLE_PRIVATE => array('n' => _t('users', 'Имя'),      'p' => _t('', 'Ваше имя')),
                'r'.Users::ROLE_COMPANY => array('n' => _t('users', 'Компания'), 'p' => _t('', 'Название вашей компании')),
            ),
        ),
    )) ?>);
});
<? js::stop(); ?>
</script>