<div class="container">

    <section class="l-mainContent">
        <div class="row">
            <aside class="col-md-3">
                <?= $profile ?>
            </aside>
            <div class="col-md-9 l-content-column">
                <div class="user-tab">
                    <?= tpl::getTabs(array('tabs' => $tabs, 'a' => $tab, 'class' => 'j-user-cabinet-tab', 'noAjax' => 1)); ?>
                </div>
                <?= $content ?>
            </div>
        </div>
    </section>
</div>
