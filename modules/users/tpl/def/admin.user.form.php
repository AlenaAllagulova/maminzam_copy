<?php
    /**
     * @var $this Users
     */
    tpl::includeJS(array('autocomplete'), true);

    $enotify = Users::i()->getEnotifyTypes($enotify); # уведомления
    $aData = HTML::escape($aData, 'html', array('login', 'name', 'skype', 'icq', 'site', 'addr_addr', 'addr_lat', 'addr_lng', 'status_text'));
    $edit = $user_id > 0;
    $aTypes = Users::aTypes();
    if ( ! isset($type)  || ! array_key_exists($type, $aTypes)) {
        $type = key($aTypes);
    }
    $typeClass = function($typeNeed, $forceHidden = false) use ($type) {
        return 'j-type j-type-'.$typeNeed.($type != $typeNeed || $forceHidden ? ' hidden' : '');
    };
    if ( ! isset($tab)) {
        $tab = $this->input->get('tab', TYPE_NOTAGS);
        if( ! $tab) {
            $tab = 'profile';
        }
    }
    if ($edit) {
        $aData['popup'] = false;
        echo $this->viewPHP($aData, 'admin.user.status');
    }
    $bSpecUseServices = Specializations::useServices(Specializations::SERVICES_PRICE_USERS);
    $verifiedEnabled = Users::verifiedEnabled();
    if($verifiedEnabled){
        $verifiedStatuses = Users::verifiedStatuses();
        $verifiedStatus = $verifiedStatuses[$verified];
    }
    $rolesEnabled = Users::rolesEnabled();
?>
<div class="tabsBar">
    <script type="text/javascript">
        function jUserTab(key, link)
        {
            $('.tab-form').hide();
            $(link).parent().addClass('tab-active').siblings().removeClass('tab-active');
            var $tab = $('#tab-'+key);
            if($tab.length){
                $tab.show();
                $('#j-user-form-buttons').toggle(key !== 'opinions');
            } else {
                return true;
            }
            return false;
        }
        $(function(){
            jUserTab('<?= $tab ?>');
        });
    </script>
    <span class="tab <?= $tab == 'profile' ? 'tab-active' : ''?>"><a href="#" onclick="return jUserTab('profile', this);"><?= _t('','Profile') ?></a><? if(!empty($pro)){ ?><span style="margin-left: 5px;"><span class="pro">PRO</span></span><? } ?></span>
    <span class="tab <?= $tab == 'specs' ? 'tab-active' : ''?> <?= $typeClass(Users::TYPE_WORKER) ?>"><a href="#" onclick="return jUserTab('specs', this);">Специализации и навыки</a></span>
    <? if($edit) { ?>
        <? if($verifiedEnabled): ?><span class="tab <?= $tab == 'verified' ? 'tab-active' : ''?>"><a href="#" onclick="return jUserTab('verified', this);">Верификация</a> <i class="<?= $verifiedStatus['i'] ?> disabled" title="<?= $verifiedStatus['title'] ?>"></i></span><? endif; ?>
        <span class="tab <?= $tab == 'opinions' ? 'tab-active' : ''?>"><a href="#" onclick="return jUserTab('opinions', this);"><?= _t('opinions', 'Opinions') ?></a></span>
        <? bff::hook('users.admin.user.form.tabs.extra', array('edit'=>$edit,'data'=>&$aData)) ?>
        <span class="tab"><a href="<?= $this->adminLink('listing&uid='.$user_id, 'bills'); ?>" target="_blank">Баланс <span class="desc">(<?= $balance ?> <?= Site::currencyDefault() ?>)</span></a></span>
        <? if(bff::fairplayEnabled()): ?><span class="tab"><span class="icon-fairplay"></span> <a href="<?= $this->adminLink('bills&user='.$user_id, 'fairplay'); ?>" target="_blank">Движение средств</a></span><? endif; ?>
        <div class="right">
            <div style="margin:0 0 0 10px;<? if( ! $activated) { ?> display: none;<? } ?>" class="left u_block_links">
                <a href="#" onclick="return jUserStatus.unblock(this);" class="u_unblock_lnk ajax clr-success <? if(!$blocked){ ?>hidden<? } ?>">разблокировать</a>
                <a href="#" onclick="return jUserStatus.block(this);" class="u_block_lnk ajax clr-error <? if($blocked){ ?>hidden<? } ?>">заблокировать</a>
            </div>
        </div>
    <? } ?>
    <div class="clear"></div>
</div>
<form action="" name="modifyUserForm" id="modifyUserForm" method="post" enctype="multipart/form-data">
<? if($edit || Users::profileMap()) { ?>
<input type="hidden" name="addr_lat" id="user-addr_lat" value="<?= $addr_lat ?>" />
<input type="hidden" name="addr_lng" id="user-addr_lng" value="<?= $addr_lng ?>" />
<? } ?>
<table class="admtbl tbledit relative">
<tbody id="tab-profile" class="tab-form relative">
<? if($rolesEnabled): ?>
    <tr>
        <td class="row1" ><span class="field-title">Тип<span class="required-mark">*</span>:</span></td>
        <td class="row2">
            <? if($edit): ?>
                <div>
                    <strong><? $roles= Users::roles(); echo $roles[$role_id]['t'] ?></strong>
                    <a href="#" class="ajax desc" onclick="$(this).parent().next().show(); $(this).parent().remove(); return false;">изменить</a>
                </div>
            <? endif; ?>
            <div <?= $edit ? 'style="display:none;"' : '' ?>>
            <? foreach(Users::roles() as $v): ?>
                <label class="inline radio"><input type="radio" name="role_id" value="<?= $v['id'] ?>" <?= (( $edit && $v['id'] == $role_id) || (! $edit && $v['default']) ? 'checked="checked"' : '') ?>><?= $v['t'] ?></label>
            <? endforeach; ?>
            </div>
        </td>
    </tr>
<? endif; ?>
<tr class="<?= ( sizeof($aTypes) < 2 ? 'hidden' : 'required' ) ?>">
    <td class="row1" ><span class="field-title">Роль<span class="required-mark">*</span>:</span></td>
    <td class="row2">
        <? if($edit): ?>
            <input type="hidden" name="type" value="<?= $type ?>" />
            <strong><?= $aTypes[$type]['t'] ?></strong>
        <? else: foreach($aTypes as $v): ?>
            <label class="inline radio"><input type="radio" name="type" value="<?= $v['id'] ?>" <?= ($type == $v['id'] ? 'checked="checked"' : '') ?> onchange="jUser.onType($(this))"><?= $v['t'] ?></label>
        <? endforeach; endif; ?>
    </td>
</tr>
<tr>
    <td class="row1 field-title j-name" style="width:120px;"><?= $rolesEnabled && $edit && $role_id == Users::ROLE_COMPANY ? 'Компания:' : 'Имя:' ?></td>
    <td class="row2">
        <input maxlength="200" type="text" name="name" id="user_name" value="<?= $name ?>" />
        <div style="position: absolute; right: 5px; top: 5px; text-align: center;">
            <div style="margin-bottom: 5px;">
                <img id="avatar" src="<?= UsersAvatar::url($user_id, $avatar, UsersAvatar::szNormal); ?>" class="img-polaroid" alt="" style="width: 65px;" />
            </div>
            <input type="hidden" name="avatar_del" id="avatar_delete_flag" value="0" />
            <? if($avatar && $edit) {?><a href="javascript:void(0);" id="avatar_delete_link" title="удалить текущий аватар" class="desc ajax" onclick="jUser.deleteAvatar('<?= UsersAvatar::url($user_id, '', UsersAvatar::szNormal) ?>');">удалить</a><? } ?>
        </div>
    </td>
</tr>
<tr class="<?= $rolesEnabled && $edit && $role_id == Users::ROLE_COMPANY ? 'hidden ' : ''?>j-role" data-id="<?= Users::ROLE_PRIVATE ?>">
    <td class="row1 field-title">Фамилия:</td>
    <td class="row2">
        <input maxlength="35" type="text" name="surname" id="user_surname" value="<?= $surname ?>" />
    </td>
</tr>
<? if(Users::registerPhone()) { ?>
<tr class="required">
	<td class="row1 field-title"><?= _t('users', 'Телефон'); ?><span class="required-mark">*</span>:</td>
	<td class="row2">
        <input type="text" name="phone_number" maxlength="30" pattern="[0-9+]*" value="<?= (!empty($phone_number) ? '+'.$phone_number : '') ?>" <? if(empty($phone_number)) { ?> placeholder="Не указан" <? } ?> autocomplete="off" />
        <? if(!empty($phone_number_verified)) { ?>
            <i class="icon-ok disabled" style="margin-top:-2px; opacity: 0.2;" title="<?= _te('users', 'подтвержден'); ?>"></i>
        <? } ?>
    </td>
</tr>
<? } ?>
<tr class="required check-email">
	<td class="row1 field-title">E-mail<span class="required-mark">*</span>:</td>
	<td class="row2">
        <input type="text" id="email" name="email" maxlength="100" value="<?= $email ?>" autocomplete="off" />
    </td>
</tr>
<tr class="required">
    <td class="row1 field-title">Логин<span class="required-mark">*</span>:</td>
    <td class="row2">
        <input maxlength="35" type="text" name="login" id="user_login" value="<?= $login ?>" />
    </td>
</tr>
<tr>
    <td class="row1"><span class="left field-title j-avatar"><?= $rolesEnabled && $edit && $role_id == Users::ROLE_COMPANY ? 'Логотип:' : 'Аватар:' ?></span></td>
    <td class="row2"><input type="file" name="avatar" size="17" /></td>
</tr>
<? if(!$edit){ ?>
<tr class="required check-password">
	<td class="row1 field-title">Пароль<span class="required-mark">*</span>:</td>
	<td class="row2">
        <input type="password" id="password" name="password" autocomplete="off" value="<?= $password ?>" maxlength="100" />
    </td>
</tr>
<tr class="required check-password">
	<td class="row1 field-title">Подтверждение пароля<span class="required-mark">*</span>:</td>
	<td class="row2">
        <input type="password" id="password2" name="password2" class="check-password2" maxlength="100" autocomplete="off" value="<?= $password2 ?>" />
    </td>
</tr>
<? } else { ?>
<tr class="required check-password">
    <td class="row1">
        <span class="field-title">Пароль<span class="required-mark">*</span></span>:
        <input type="hidden" name="changepass" id="changepass" value="0" />
    </td>
    <td class="row2">
        <div id="passwordCurrent">
            <a href="#" class="ajax desc" onclick="jUser.doChangePassword(1); return false;">изменить пароль</a>
        </div>
        <div id="passwordChange" style="display:none;">
            <input type="text" id="password" name="password" value="" maxlength="100" />
            &nbsp;<a href="#" class="ajax desc" onclick="jUser.doChangePassword(0); return false;">отмена</a>
        </div>
    </td>
</tr>
<tr <?= Users::useClient() && $type == Users::TYPE_CLIENT ? 'style="display:none;"' : '' ?>>
    <td class="row1 field-title">Рейтинг:</td>
    <td class="row2">
        <span>
            <span class="label" id="j-user-rating-current"><?= $rating ?></span>
            <? if(!empty($pro)) { ?>
                <span class="disabled icon-chevron-right"></span>&nbsp;<abbr id="j-user-rating-current-pro" class="pro" style="padding: 3px 4px;" data-content="<?= HTML::escape('<span class="pro">PRO</span>&nbsp;&nbsp;<span class="desc">x'.Users::ratingMultiplier().'</span>') ?>"><?= Users::rating($rating, $pro) ?></abbr>
                <span id="j-user-rating-current-pro-popover"></span>
            <? } ?>
            &nbsp;<a href="#" class="ajax desc" onclick="$(this).parent().hide().next().show(); return false;">изменить</a>
        </span>
        <span style="display: none;">
            <input type="text" class="short" id="j-user-rating-new" value="<?= $rating ?>" />
            <button class="btn btn-mini btn-success" onclick="jUser.onRatingChange($(this)); return false;">сохранить</button>
            <button class="btn btn-mini" onclick="$(this).parent().hide().prev().show(); return false;">отмена</button>
        </span>
    </td>
</tr>
<tr>
    <td class="row1 field-title">Регистрация:</td>
    <td class="row2"><?= tpl::date_format2($created, true) ?>, <a class="desc" href="<?= $this->adminLink('ban') ?>"><?= long2ip($created_ip) ?></a></td>
</tr>
<tr>
    <td class="row1 field-title">Авторизация:</td>
    <td class="row2">
        <? if($last_login == '0000-00-00 00:00:00') { ?>
            <span class="desc">&mdash;</span>
        <? } else { ?>
        <?= tpl::date_format2($last_login,true); ?><span class="desc"> - последняя, <a class="bold desc" href="<?= $this->adminLink('ban') ?>"><?= long2ip($last_login_ip); ?></a></span>
        <? if($last_login2 && $last_login2 !== '0000-00-00 00:00:00'){ ?><br /><?= tpl::date_format2($last_login2,true); ?><span class="desc"> - предпоследняя</span><? } ?>
        <? } ?>
    </td>
</tr>
<? } ?>
<? if(Users::profileMap()):
$bHideAddr = $edit && ! empty($addr_addr);
$bCountry = Geo::countrySelect();
if($bHideAddr): ?>
<tr class="j-hide-addr">
    <td class="row1 field-title">Адрес:</td>
    <td class="row2" style="height: 30px;">
        <?= $bCountry && ! empty($country_data['title']) ? $country_data['title'].', ' : '' ?>
        <?= ! empty($city_data['title']) ? $city_data['title'].', ' : '' ?>
        <?= $addr_addr ?>
        <button style="margin-left: 3px;" class="btn btn-mini" onclick="jUser.onShowAddr();">изменить</button>
    </td>
</tr>
<? endif; # end: $bHideAddr
if($bCountry): ?>
<tr class="j-addr<?= $bHideAddr ? ' hidden' : '' ?>">
    <td class="row1 field-title"><?= _t('', 'Страна:');?></td>
    <td class="row2">
        <select name="reg1_country"
                id="user-country-id"
                <?= ($reg3_city != Geo::defaultCity()? '': 'disabled="disabled"' )?>
                onchange="jUser.onCountry($(this));">
            <?= HTML::selectOptions(Geo::countryList(),
                                    $reg1_country,
                                    'Выбрать',
                                    'id',
                                    'title') ?>
        </select>
    </td>
</tr>
<? endif; # end: $bCountry ?>
<tr id="user-city-block"
    class="j-addr<?= $bHideAddr ? ' hidden' : '' ?>"
    <? if($bCountry && ! $reg1_country){ ?>style="display: none;"<? } ?>>
    <td class="row1 field-title"><?= _t('', 'Город:');?></td>
    <td class="row2">
        <input type="hidden" name="reg3_city" value="<?= $reg3_city ?>" id="user-city-id" />
        <input type="text"
               value="<?= $city_data['title'];?>"
               id="user-city-ac"
               class="autocomplete"
               <?= ($reg3_city != Geo::defaultCity()? '': 'disabled="disabled"' )?>
               placeholder="Введите название города" style="width: 212px;" />
    </td>
</tr>
<tr id="order-district-block">
    <td class="row1 field-title"><?= _t('district', 'Район города') ?></td>
    <td class="row2">
        <select class="form-control"
                id="j-order-distrit"
                name="district_id">
            <?= HTML::selectOptions(Geo::districtList($reg3_city),
                (!empty($district_id) ? $district_id : 0),
                _t('', 'Выбрать'),
                'id',
                'title') ?>
        </select>
        <div class="clearfix"></div>
    </td>
</tr>
<tr id="user-metro-block"
    <? if( empty($city_metro['data'])){ ?>style="display: none;"<? } ?>
    class="j-addr<?= $bHideAddr ? ' hidden' : '' ?>">
    <td class="row1 field-title">
        <?= _t('', 'Метро:');?>
    </td>
    <td class="row2">
        <select name="metro_id" id="user-metro-sel"><?= $city_metro['html'] ?></select>
    </td>
</tr>
<tr id="user-addr" <? if( ! $edit || empty($addr_addr)){ ?>style="display: none;"<? } ?> class="j-addr<?= $bHideAddr ? ' hidden' : '' ?>">
    <td class="row1 field-title">
        <?= _t('', 'Точный адрес:');?>
    </td>
    <td class="row2">
        <input type="text"
               name="addr_addr"
               id="user-addr_addr"
               value="<?= $addr_addr ?>"
               style="width: 300px;"
               maxlength="400" />
        <a href="#"
           class="ajax"
           onclick="jUser.onMapSearch(); return false;">
            <?= _t('', 'найти адрес');?>
        </a>
        <div id="user-addr-map" class="map-google"  style="width: 620px; height: 260px; margin-top: 5px;"></div>
    </td>
</tr>
<? endif; # end: profileMap ?>
<? if (Users::profileBirthdate()):
    $hidden = $rolesEnabled && $edit && $role_id == Users::ROLE_COMPANY;
    $aData['birthdate'] = $this->getBirthdateOptions($aData['birthdate'], 1930, true); # дата рождения
?>
<tr class="<?= $hidden ? 'hidden ' : ''?>j-role" data-id="<?= Users::ROLE_PRIVATE ?>">
    <td class="row1 field-title">Дата рождения:</td>
    <td class="row2">
        <select name="birthdate[day]" class="j-role-disable j-enable-<?= Users::ROLE_PRIVATE ?>"<?= $hidden ? ' disable="disable"' : '' ?> style="width:50px;"><?= $birthdate['days'] ?></select>
        <select name="birthdate[month]" class="j-role-disable j-enable-<?= Users::ROLE_PRIVATE ?>"<?= $hidden ? ' disable="disable"' : '' ?> style="width:105px;"><?= $birthdate['months'] ?></select>
        <select name="birthdate[year]" class="j-role-disable j-enable-<?= Users::ROLE_PRIVATE ?>"<?= $hidden ? ' disable="disable"' : '' ?> style="width:60px;"><?= $birthdate['years'] ?></select>
    </td>
</tr>
<? endif; # end: profileBirthdate
if (Users::profileSex()): ?>
<tr class="<?= $rolesEnabled && $edit && $role_id == Users::ROLE_COMPANY ? 'hidden ' : ''?>j-role" data-id="<?= Users::ROLE_PRIVATE ?>">
    <td class="row1 field-title">Пол:</td>
    <td class="row2">
        <?
            $aSex = array(
                Users::SEX_FEMALE => 'Женщина',
                Users::SEX_MALE   => 'Мужчина',
            );
            foreach($aSex as $k=>$v) { ?><label class="radio inline"><input type="radio" name="sex" value="<?= $k ?>" <? if($sex == $k){ ?>checked="checked"<? } ?> /><?= $v ?></label><? } ?>
    </td>
</tr>
<? endif; # end: profileSex  ?>
<tr>
    <td class="row1 field-title">Контакты:</td>
    <td class="row2">
        <div id="j-user-contacts"></div>
        <a class="ajax desc" id="j-add-contact" href="#">+ добавить</a>
    </td>
</tr>
<tr class="<?= $typeClass(Users::TYPE_WORKER) ?>">
    <td class="row1 field-title">Статус:</td>
    <td class="row2">
        <select name="status" style="margin-bottom: 4px;"><?= HTML::selectOptions(Users::aWorkerStatus(), $status, false, 'id', 't'); ?></select>
        <textarea class="stretch" name="status_text" rows="4"><?= $status_text ?></textarea>
        <div id="status-message" class="small desc"></div>
    </td>
</tr>
<? bff::hook('users.admin.user.form', array('edit'=>$edit,'data'=>&$aData,'typeClass'=>$typeClass)) ?>
<tr style="display: none;">
    <td class="row1 field-title">О себе:</td>
    <td class="row2">
        <textarea class="stretch" name="about"><?= $about ?></textarea>
    </td>
</tr>
<tr class="<?= $typeClass(Users::TYPE_WORKER) ?>">
    <td class="row1 field-title">Резюме:</td>
    <td class="row2">
        <?= tpl::jwysiwyg($resume_text, 'resume_text', 0, 150); ?>
    </td>
</tr>
<tr>
    <td colspan="2"><hr class="cut" /></td>
</tr>
<? if($admin) { ?>
<tr>
    <td class="row1 field-title">Внутренняя почта:</span></td>
    <td class="row2"><label class="checkbox"><input type="checkbox" name="im_noreply" <? if($im_noreply){ ?>checked="checked"<? } ?> /> пользователи не могут отвечать на его сообщения</label></td>
</tr>
<? } ?>
<tr>
    <td class="row1 field-title bold">Доверенный:</td>
    <td class="row2">
        <label class="checkbox">
            <input type="checkbox" name="trusted" value="1" <? if(!empty($trusted)) { ?>checked="checked"<? } ?> />
            <div class="help-inline">действия пользователя не будут попадать на проверку модератору</div>
        </label>
    </td>
</tr>
<tr>
    <td class="row1 field-title">Уведомления и подписка:</td>
    <td class="row2">
        <? foreach($enotify as $k=>$v){ ?>
            <label class="checkbox"><input type="checkbox" name="enotify[]" value="<?= $k ?>" <? if($v['a']){ ?>checked="checked"<? } ?> /><?= $v['title'] ?></label>
        <? } ?>
    </td>
</tr>
<tr>
    <td class="row1 field-title">Принадлежность<br />к группе:</td>
    <td class="row2">
        <table>
            <tr>
                <td width="220">
                    <strong>Группы пользователей:</strong><br />
                    <select multiple name="exists_values[]" id="exists_values" style="width:210px; height:100px !important;"><?= $exists_options ?></select>
                </td>
                <td width="40">
                     <div style="width:33px; height:12px;">&nbsp;</div>
                     
                     <input type="button" class="btn btn-mini button" style="width: 25px; margin-bottom:2px;" value="&gt;&gt;" onclick="bff.formSelects.MoveAll('exists_values', 'group_id');" />
                     <input type="button" class="btn btn-mini button" style="width: 25px; margin-bottom:2px;" value="&gt;" onclick="bff.formSelects.MoveSelect('exists_values', 'group_id');" />
                     <input type="button" class="btn btn-mini button" style="width: 25px; margin-bottom:2px;" value="&lt;" onclick="bff.formSelects.MoveSelect('group_id', 'exists_values');" />
                     <input type="button" class="btn btn-mini button" style="width: 25px; margin-bottom:2px;" value="&lt;&lt;" onclick="bff.formSelects.MoveAll('group_id', 'exists_values');" />
                </td>
                <td width="220">
                    <strong>Активные группы:</strong><br />
                    <select multiple name="group_id[]" id="group_id" style="width:210px; height:100px !important;"><?= $active_options ?></select>
                </td>
               	<td>&nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
</tbody>
<tbody id="tab-specs" class="tab-form relative hidden">
    <?  $nSpecLimit = Users::specializationsLimit($pro); ?>
    <tr class="<?= $typeClass(Users::TYPE_WORKER) ?>">
        <td class="row1 field-title" style="width: 100px;">Основная специализация:</td>
        <td class="row2" id="j-main-spec">
            <div id="j-main-specs-selected"></div>
            <div id="j-main-spec-selector"><?
                foreach($dopSpecsOptions as $lvl => $v) {
                    ?><select class="spec-select" data-lvl="<?= $lvl ?>" style="margin-right: 5px;<? if(empty($v['categories'])){ ?>display: none; <? } ?>" onchange="jUser.onSpecSelect(true,$(this))"><?= $v['categories'] ?></select><?
                }
                ?></div>
        </td>
    </tr>
    <tr class="<?= $typeClass(Users::TYPE_WORKER, ($nSpecLimit <= 1)) ?>">
        <td class="row1 field-title">Дополнительные специализации:</td>
        <td class="row2">
            <div id="j-dop-specs-selected" class="hide"></div>
            <div id="j-dop-spec-selector"><?
                foreach($dopSpecsOptions as $lvl => $v) {
                    ?><select class="spec-select" data-lvl="<?= $lvl ?>" style="margin-right: 5px;<? if(empty($v['categories'])){ ?>display: none; <? } ?>" onchange="jUser.onSpecSelect(false,$(this))"><?= $v['categories'] ?></select><?
                }
                ?></div>
        </td>
    </tr>
    <tr class="<?= $typeClass(Users::TYPE_WORKER) ?>">
        <td class="row1 field-title">Навыки:</td>
        <td class="row2">
            <?= $this->userTags()->tagsForm($user_id, $this->adminLink('ajax&act=tags-suggest', bff::$class), 675) ?>
        </td>
    </tr>
</tbody>
<? if($verifiedEnabled): $verifiedReasons = Users::verifiedReasons(); ?>
<tbody id="tab-verified" class="tab-form relative hidden">
<tr>
    <td class="row1 field-title" style="width: 100px;">Статус:</td>
    <td class="row2">
        <strong class="j-verified-status"><?= $verifiedStatus['title'] ?></strong>
        <? if ($verified == Users::VERIFIED_STATUS_WAIT && ! empty($verified_reason['id'])) { ?>
            <div class="well well-small j-verified-status-note">
                <i>Повторно, по причине отклонения:</i><br />
                <?= ($verified_reason['id'] == Users::VERIFIED_REASON_OTHER ? nl2br($verified_reason['message']) :
                    (isset($verifiedReasons[$verified_reason['id']]) ? $verifiedReasons[$verified_reason['id']]['t'] : '?')); ?>
            </div>
        <? } ?>
    </td>
</tr>
<? if( ! empty($verifiedImages)): ?>
<tr>
    <td class="row1 field-title">Документы:</td>
    <td class="row2" id="j-verified-images">
        <? foreach($verifiedImages as $v): ?>
            <div>
                <a href="<?= $v['link'] ?>" class="j-verified-image" rel="verified-images-group" target="_blank"><?= $v['filename'] ?></a>
                <a class="but cross j-verified-image-delete" href="#" title="Удалить" data-fn="<?= $v['filename'] ?>"></a>
            </div>
        <? endforeach; ?>
    </td>
</tr>
<? endif; ?>
<tr>
    <td></td>
    <td>
        <input type="hidden" name="verified" value="<?= $verified ?>" />
        <input type="hidden" name="verified_reason[cnt]" value="<?= $verified_reason['cnt']; ?>" />
        <input type="button" class="btn btn-mini green j-verified-approve" value="Одобрить" <?= $verified == Users::VERIFIED_STATUS_APPROVED ? 'style="display:none;"' : ''?> />
        <input type="button" class="btn btn-mini orange j-verified-decline" value="Отклонить" <?= $verified == Users::VERIFIED_STATUS_DECLINED ? 'style="display:none;"' : ''?> />
    </td>
</tr>
<tr class="j-verified-reasons" <?= ($verified != Users::VERIFIED_STATUS_DECLINED ? 'style="display:none;"' : '') ?>>
    <td class="row1 field-title">Причина отклонения:</td>
    <td class="row2">
        <? foreach($verifiedReasons as $v): ?>
            <label class="radio"><input type="radio" name="verified_reason[id]" value="<?= $v['id'] ?>"<?= $v['id'] == $verified_reason['id'] ? ' checked="checked"' : '' ?>><?= $v['t'] ?></label>
        <? endforeach; ?>
        <div class="j-verified-reason-message well well-small" style="display:none;">
            <textarea class="stretch" rows="3" name="verified_reason[message]"><?= HTML::escape($verified_reason['message']) ?></textarea>
        </div>
    </td>
</tr>
</tbody>
<? endif; ?>
<tbody id="tab-opinions" class="tab-form relative hidden">
    <tr>
        <td colspan="2">
            <?= Opinions::i()->user_listing($user_id); ?>
        </td>
    </tr>
</tbody>
<? bff::hook('users.admin.user.form.tabs.content', array('edit'=>$edit,'data'=>&$aData)) ?>
<tr class="footer" id="j-user-form-buttons">
    <td colspan="2">
        <hr style="margin: 0 0 10px 0;" />
        <div class="left">
            <input type="submit" class="btn btn-success button submit j-submit" value="<?= _t('', 'Save') ?>"<? if( ! $edit) { ?> onclick="jUser.back();"<? } ?> data-loading-text="<?= _t('', 'Wait...') ?>" />
            <? if($edit){ ?><input type="submit" class="btn btn-success button submit j-submit" onclick="jUser.back();" value="<?= _t('', 'Save and back') ?>" data-loading-text="<?= _t('', 'Wait...') ?>" /><? } ?>
            <? if($edit && $session_id && ! $superadmin) { ?>
            <input type="button" class="btn clr-error button" value="Разлогинить" onclick="bff.confirm('sure', {r:'<?= $this->adminLink('user_action&type=logout&rec='.$user_id.'&tuid='.$tuid) ?>'});" />
            <? } ?>
            <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="history.back();" />
        </div>
        <div class="right">
            <? if( ! empty($frontendAuthUrl)): ?>
                <a href="<?= $frontendAuthUrl ?>" class="btn" target="_blank">Авторизоваться</a>
            <? endif; ?>
        </div>
        <div class="clear"></div>
    </td>
</tr>
</table>
</form>

<script type="text/javascript">
//<![CDATA[ 
var jUser = (function(){
    var $form, $types, submitAndBack = false;
    var city = {$block:0,$ac:0, acApi:0, data:{}};
    var metro = {$block:0,$sel:0,data:{}};
    var addr = {$block:0, i:false,addr:0,lat:0,lng:0, mapEditor:0};
    var specCache = {}, dpCache = {}, prCache = {}, servCache = {};
    var specsServices = <?= ($bSpecUseServices ? 'true':'false') ?>;
    var $mainSpecSelector, $mainSpecsSelected;
    var $dopSpecSelector, $dopSpecsSelected, dopIndex = 1;

    $(function(){
        $form = $('#modifyUserForm');
        bff.iframeSubmit($form, function(data){
            if(data && data.success) {
                bff.success('Данные успешно сохранены');
                if( data.hasOwnProperty('redirect') ) {
                    bff.redirect('<?= tpl::adminLink('listing'); ?>');
                } else if(data.reload) {
                    setTimeout(function(){ location.reload(); }, 1000);
                } else if(submitAndBack) {
                    setTimeout(function(){ history.back(); }, 1000);
                }
            }
        },{
            beforeSubmit: function(){
                //check groups
                if( document.getElementById('group_id').options.length == 0 ) {
                    bff.error('укажите <strong>принадлежность к группе</strong>');
                    return false;
                }
                bff.formSelects.SelectAll('group_id');
                <? if($verifiedEnabled): ?>
                    if(intval($form.find('[name="verified"]').val()) == <?= Users::VERIFIED_STATUS_DECLINED ?>){
                        if( ! $form.find('[name="verified_reason[id]"]:checked').length){
                            bff.error('Укажите причину отклонения проверки пользователя');
                            return false;
                        }else{
                            if(intval($form.find('[name="verified_reason[id]"]:checked').val()) == -1
                            && $form.find('[name="verified_reason[message]"]').val().length < 10){
                                bff.error('Подробней опишите причину отклонения проверки пользователя');
                                return false;
                            }
                        }
                    }
                <? endif; ?>

                return true;
            },
            button: '.j-submit'
        });

        $types = $('.j-type');

        <? if(Users::profileMap()): ?>
        metro.$block = $form.find('#user-metro-block');
        metro.$sel = $form.find('#user-metro-sel');

        city.$block = $form.find('#user-city-block');
        city.$ac = $form.find('#user-city-ac').autocomplete('<?= $this->adminLink('regionSuggest', 'geo') ?>',
            {valueInput: $form.find('#user-city-id'), params:{country:<?= $reg1_country ?>}, suggest: <?= Geo::regionPreSuggest($reg1_country) ?>,
        onSelect: function(cityID, cityTitle, ex){
            if( ! ex.changed ) return;
            if(addr.addr) addr.addr.val('');
            // metro
            if( ex.data === false || intval(ex.data[2]) === 0 ) {
                metro.$block.hide();
                metro.$sel.html('').val(0);
            } else {
                geoRefreshMetro(cityID);
            }
            // map
            addr.$block.show();
            initAddr();
            if(ex.title.length > 0) {
                jUser.onMapSearch();
            }
        }}, function(){ city.acApi = this; });

        addr.$block = $form.find('#user-addr');
            <? if(Geo::defaultCity()): ?>
                geoRefreshMetro(<?=Geo::defaultCity()?>)
                // map
                addr.$block.show();
                initAddr();
            <? endif; ?>
        <? endif; ?>

        bff.maxlength($form.find('[name="status_text"]'), {limit:300, message:$form.find('#status-message')});

        initContacts(<?= Users::profileContactsLimit() ?>, <?= func::php2js($contacts)?>);

        $mainSpecSelector = $('#j-main-spec-selector');
        $mainSpecsSelected = $('#j-main-specs-selected');

        $dopSpecSelector = $('#j-dop-spec-selector');
        $dopSpecsSelected = $('#j-dop-specs-selected');
        $form.find('#tab-specs').on('click', '.j-delete', function(){
            var $block = $(this).closest('.j-spec-selected'), isMain = $block.parent().is($mainSpecsSelected);
            onSpecBlockSelected($block, false);
            toggleSpecSelector(isMain);
            return false;
        });

        <? if($edit):
            if( ! empty($specs)):
                foreach($specs as $v):
                    if($v['main']): ?>onSpecSelected(true, false, <?= $v['spec_id']?>, <?= func::php2js($v)?>, false);
                    <? else: ?>onSpecSelected(false, false, <?= $v['spec_id']?>, <?= func::php2js($v)?>, <?= (!empty($v['disabled'])?'true':'false') ?>);<?
                    endif;
                endforeach;
            endif;
            if (!empty($pro)):
                ?>
                if (bff.bootstrapJS()) {
                    $form.find('#j-user-rating-current-pro').popover({trigger:'hover',placement:'top',container:'#j-user-rating-current-pro-popover',title:'',html:true});
                }
                <?
            endif;
        endif; ?>

        <? if($verifiedEnabled): ?>
            var $images = $form.find('#j-verified-images');
            $images.find('.j-verified-image').fancybox({type:'image', autoScale:false});
            $images.on('click', '.j-verified-image-delete', function(e){
                e.preventDefault();
                var $el = $(this);
                $el.parent().remove();
                $images.append('<input type="hidden" name="verified_deleted[]" value="'+$el.data('fn')+'" />');
            });
            var $verified = $form.find('[name="verified"]');
            var $vStatus  = $form.find('.j-verified-status');
            var $vStatusNote  = $form.find('.j-verified-status-note');
            var $vApprove = $form.find('.j-verified-approve');
            var $vDecline = $form.find('.j-verified-decline');
            var $vReasons = $form.find('.j-verified-reasons');
            var $vMessage = $form.find('.j-verified-reason-message');

            $vApprove.click(function(){
                if (!bff.confirm('sure')) return;
                $verified.val(<?= Users::VERIFIED_STATUS_APPROVED ?>);
                $vStatus.text('<?= $verifiedStatuses[Users::VERIFIED_STATUS_APPROVED]['title'] ?>');
                $vStatusNote.hide();
                $vApprove.hide();
                $vDecline.show();
                $vReasons.hide();
                $vMessage.hide();
            });

            $vDecline.click(function(){
                $verified.val(<?= Users::VERIFIED_STATUS_DECLINED ?>);
                $vStatus.text('<?= $verifiedStatuses[Users::VERIFIED_STATUS_DECLINED]['title'] ?>');
                $vStatusNote.hide();
                $vApprove.show();
                $vDecline.hide();
                $vReasons.show();
                showVerifiedMessage();
            });

            function showVerifiedMessage()
            {
                $vMessage.toggle(intval($form.find('[name="verified_reason[id]"]:checked').val()) == -1);
            }

            $form.find('[name="verified_reason[id]"]').change(function(){
                showVerifiedMessage();
            });
        <? endif; ?>
        <? if($rolesEnabled && ! $edit): ?>
        $form.on('change', '[name="role_id"]', function(){
            var $roles = $form.find('.j-role').hide();
            var id = intval($(this).val());
            $roles.filter('[data-id="'+id+'"]').show();
            $form.find('.j-role-disable').prop('disabled', true);
            $form.find('.j-enable-'+id).prop('disabled', false);
            if( id == <?= Users::ROLE_COMPANY ?>){
                $form.find('.j-name').text('Компания:');
                $form.find('.j-avatar').text('Логотип:');
            }else{
                $form.find('.j-name').text('Имя:');
                $form.find('.j-avatar').text('Аватар:');
            }
        });
        <? endif; ?>

    });

    function geoRefreshMetro(cityID)
    {
        if( metro.data.hasOwnProperty(cityID) ) {
            metro.$sel.html( metro.data[cityID] ).val(0);
            metro.$block.show();
        } else {
            bff.ajax('<?= $this->adminLink('ajax&act=city-metro', 'geo'); ?>', {city:cityID}, function(data, errors){
                if(data && data.success) {
                    metro.data[cityID] = data.html;
                    geoRefreshMetro(cityID);
                } else {
                    bff.error(errors);
                }
            });
        }
    }

    function initContacts(limit, contacts)
    {
        var index  = 0, total = 0;
        var $block = $('#j-user-contacts');
        var $add = $('#j-add-contact');

        function add(contact)
        {
            contact = contact || {};
            if(limit>0 && total>=limit) return;
            index++; total++;
            var value = '';
            if(contact.hasOwnProperty('v') && contact.v) value = contact.v;
            $block.append('<div class="j-contact">'+
                                '<select class="left j-contact-type" name="contacts['+index+'][t]" style="width:85px;"><?= HTML::selectOptions(Users::aContactsTypes(), 0, false, 'id', 't'); ?></select>'+
                                '<input class="left j-value" type="text" maxlength="40" name="contacts['+index+'][v]" value="'+value.replace(/"/g, "&quot;")+'" style="margin-left:4px;"/>'+
                                '<div class="left" style="margin: 3px 0 0 4px;"><a href="#" class="but cross j-remove"></a></div>'+
                                '<div class="clear"></div>'+
                            '</div>');
            if(contact.hasOwnProperty('t')){
                $block.find('.j-contact:last').find('.j-contact-type').val(contact.t);
            }
            if(limit>0 && total>=limit) {
                $add.hide();
            }
        }

        $block.on('click', '.j-remove', function(e){ nothing(e);
            var $contact = $(this).closest('.j-contact');
            if( $contact.find('.j-value').val() != '' ) {
                if(confirm('Удалить контакт?')) {
                    $contact.remove(); total--;
                }
            } else {
                $contact.remove(); total--;
            }
            if(limit>0 && total<limit) {
                $add.show();
            }
        });

        $add.click(function(e){ nothing(e);
            add();
            return false;
        });

        contacts = contacts || {};
        for(var i in contacts) {
            if( contacts.hasOwnProperty(i) ) {
                add(contacts[i]);
            }
        }
        if( ! total ) {
            add();
        }
    }

    function initAddr()
    {
        if( addr.i === true ) {
            addr.map.refresh();
            return;
        }

        addr.addr = $form.find('#user-addr_addr');
        addr.lat  = $form.find('#user-addr_lat');
        addr.lng  = $form.find('#user-addr_lng');
        addr.i    = true;

        addr.map = bff.map.init('user-addr-map', [addr.lat.val(), addr.lng.val()], function(map){
            addr.mapEditor = bff.map.editor();
            addr.mapEditor.init({
                map: map, version: '2.1',
                coords: [addr.lat, addr.lng],
                address: addr.addr,
                addressKind: 'house',
                updateAddressIgnoreClass: 'typed'
            });

            addr.addr.bind('change keyup input', $.debounce(function(){
                if( ! $.trim(addr.addr.val()).length ) {
                    addr.addr.removeClass('typed');
                } else {
                    addr.addr.addClass('typed');
                    jUser.onMapSearch();
                }
            }, 700));
            jUser.onMapSearch();
        }, {zoom:12});
    }

    function onSpecSelected(isMain, $select, specID, oSpec, bDisabled)
    {
        if(specID <= 0) return;

        var $selectedBlock = (isMain ? $mainSpecsSelected : $dopSpecsSelected);
        bDisabled = (bDisabled === true);

        var $exist = $form.find('[data-spec-id="'+specID+'"]');
        if ($exist.length) {
            if ( ! isMain && $exist.parent().is($mainSpecsSelected) && !$exist.hasClass('hide')) {
                return; // is already main
            }
            $selectedBlock.append($exist);
            onSpecBlockSelected($exist, true);
            toggleSpecSelector(isMain);
            return;
        }

        $selectedBlock.append(getSpecBlock($select, oSpec, specID, bDisabled)).removeClass('hide');
        var $block = $selectedBlock.find('[data-spec-id="'+specID+'"]');
        var $dpBl = $block.find('.j-dp');
        var $prBl = $block.find('.j-pr');
        var $servBl = $block.find('.j-serv');

        if ($select && $select.length) {
            setDp($dpBl, specID);
            setPr($prBl, specID);
            if (specsServices) setServices($servBl, specID);
        } else {
            if (oSpec.dp) $dpBl.html(oSpec.dp);
            if (oSpec.pr) $prBl.html(oSpec.pr);
            if (specsServices && oSpec.serv) $servBl.html(oSpec.serv);
        }
        toggleSpecSelector(isMain);
    }

    function onSpecBlockSelected($block, selected)
    {
        if (intval($block.data('disabled')) == 1) {
            if (selected) {
                $block.removeClass('hide').find('.j-disabled').val(0);
            } else {
                $block.addClass('hide').find('.j-disabled').val(1);
            }
        } else {
            if (!selected) {
                $block.remove();
            }
        }
    }

    function toggleSpecSelector(main)
    {
        if (main) {
            $mainSpecSelector.toggle($mainSpecsSelected.find('.j-spec-selected:not(.hide)').length < 1);
        } else {
            var _sel = $dopSpecsSelected.find('.j-spec-selected:not(.hide)').length;
            if (_sel >= intval(<?= ($nSpecLimit-1) ?>)){
                $dopSpecSelector.hide();
            } else {
                $dopSpecSelector.show();
            }
        }
    }

    function getSpecBlock($select, oSpec, specID, bDisabled)
    {
        var title = '', inputs = '', namePrefix = 'spec['+dopIndex+']';
        inputs += '<input type="hidden" name="'+namePrefix+'[disabled]" value="'+(bDisabled?1:0)+'" class="j-disabled" />';
        if ($select && $select.length) {
            $select.parent().find('select').each(function(){
                var $el = $(this);
                var id = intval($el.val());
                inputs += '<input type="hidden" name="'+namePrefix+'['+$el.data('lvl')+']" class="j-spec-'+$el.data('lvl')+'-id" value="'+id+'" />';
                if (id) {
                    if (title.length) title += ' / ';
                    title += $el.find(':selected').text();
                }
            });
        } else {
            title = (oSpec.hasOwnProperty('cat_title') ? oSpec.cat_title + ' / ' + oSpec.spec_title : oSpec.spec_title);
            if (oSpec.hasOwnProperty('lvl')) {
                inputs += '<input type="hidden" name="'+namePrefix+'['+oSpec.lvl+']" class="j-spec-'+oSpec.lvl+'-id" value="'+oSpec.cat_id+'" />';
            }
            inputs += '<input type="hidden" name="'+namePrefix+'[spec]" class="j-spec-spec-id" value="'+specID+'" />';
        }
        dopIndex++;

        return '<div class="well well-small j-spec-selected'+(bDisabled ? ' hide':'')+'" data-spec-id="'+specID+'" data-disabled="'+(bDisabled?1:0)+'" style="margin:0 2px 10px 2px;"><b>'+title+'</b>'+
            '<a href="#" class="close j-delete" style="margin-left: 3px;"><i class="icon-remove" style="margin-top:0;"></i></a>'+inputs+
            '<table class="admtbl tbledit"><tr><th style="width:100px;"></th><th></th></tr><tbody class="j-pr"></tbody><tbody class="j-dp"></tbody>'+(specsServices?'<tbody class="j-serv"></tbody>':'')+'</table></div>';
    }

    function setDp($dp, specID)
    {
        if(specID){
            if(dpCache.hasOwnProperty(specID)){
                $dp.html(dpCache[specID]);
            } else {
                bff.ajax('<?= $this->adminLink('specializations_list&act=dp-data', 'specializations'); ?>', {spec_id: specID, key: 'd'+specID}, function(data){
                    if(data && data.success) {
                        $dp.html(dpCache[specID] = data.dp);
                    }
                });
            }
        } else {
            $dp.html('');
        }
    }

    function setPr($pr, specID)
    {
        if(specID){
            if(prCache.hasOwnProperty(specID)){
                $pr.html(prCache[specID]);
            } else {
                bff.ajax('<?= $this->adminLink('ajax&act=spec-price'); ?>', {spec_id: specID}, function(data){
                    if(data && data.success) {
                        $pr.html(prCache[specID] = data.pr);
                    }
                });
            }
        } else {
            $pr.html('');
        }
    }

    function setServices($serv, specID)
    {
        if(specID){
            if(servCache.hasOwnProperty(specID)){
                $serv.html(servCache[specID]);
            } else {
                bff.ajax('<?= $this->adminLink('specializations_list&act=services-data', 'specializations'); ?>', {spec_id: specID}, function(data){
                    if(data && data.success) {
                        $serv.html(servCache[specID] = data.serv);
                    }
                });
            }
        } else {
            $serv.html('');
        }
    }

    return {
        back: function()
        {
            submitAndBack = true;
        },
        deleteAvatar: function(defaultAvatar)
        {
            if(confirm('Удалить текущий аватар?')) {
                $('#avatar').attr('src', defaultAvatar);
                $('#avatar_delete_flag').val(1);
                $('#avatar_delete_link').remove();
            }
            return false;
        },
        doChangePassword: function(change)
        {
            $('#passwordCurrent, #passwordChange').toggle();
            $('#changepass').val( change );

            if(change)
                $('#password').focus();
                
            return false;
        },
        onType: function($type)
        {
            $types.hide();
            $types.filter('.j-type-'+$type.val()).show();
        },
        <? if(Users::profileMap()): ?>
        onCountry:function($el)
        {
            var country = intval($el.val());
            if(country){
                city.acApi.setParam('country', country);
                city.$block.show();
                if( city.data.hasOwnProperty(country) ) {
                    city.acApi.setSuggest(city.data[country], true);
                } else {
                    bff.ajax('<?= $this->adminLink('ajax&act=country-presuggest', 'geo') ?>', {country:country}, function(data){
                        city.data[country] = data;
                        city.acApi.setSuggest(data, true);
                    });
                }
            } else {
                city.$block.hide();
                addr.$block.hide();
            }
        },
        onMapSearch: function()
        {
            if( ! addr.mapEditor) return false;
            var q = [];
            <? if ( ! $bCountry) { ?>
                q.push( '<?= Geo::regionTitle(Geo::defaultCountry()) ?>' );
            <? } else { ?>
                q.push( $form.find('#user-country-id').find(':selected').text() );
            <? } ?>
            var q_city = $.trim( city.$ac.val() ); if( q_city.length ) q.push( q_city );
            var q_addr = $.trim( addr.addr.val() ); if( q_addr.length ) q.push( q_addr );
            q = q.join(', '); if( addr.lastQuery == q ) return false;
            addr.mapEditor.search( addr.lastQuery = q, false, function(){
                addr.mapEditor.centerByMarker();
            } );
            return false;
        },
        onShowAddr: function()
        {
            $form.find('.j-addr').removeClass('hidden');
            $form.find('.j-hide-addr').remove();
            initAddr();
            return false;
        },
        <? endif ; ?>
        onSpecSelect: function(isMain, $select)
        {
            var specView = function(isMain, data, $select) {
                if(data.subs>0) {
                    $select.after('<select class="spec-select" data-lvl="'+data.lvl+'" style="margin-right: 5px;" onchange="jUser.onSpecSelect('+(isMain?'true':'false')+',$(this))">'+data.cats+'</select>').show();
                }
            };

            var catID = intval($select.val());
            var lvl = $select.data('lvl');
            if (lvl == 'spec') {
                onSpecSelected(isMain, $select, catID/*specID*/);
                $select.val(0);
            } else {
                $select.nextAll().remove();

                if (specCache.hasOwnProperty(catID)) {
                    specView(isMain, specCache[catID], $select );
                } else {
                    bff.ajax('<?= $this->adminLink('specializations_list&act=category-data&noAllSpec=1', 'specializations'); ?>', {'cat_id': catID, 'lvl':$select.data('lvl')}, function(data){
                        if(data && data.success) {
                            specView(isMain, (specCache[catID] = data), $select );
                        }
                    });
                }
            }
        },
        onRatingChange: function($link)
        {
            var $current = $('#j-user-rating-current');
            var $currentEdit = $('#j-user-rating-new');
            var $currentPro = $('#j-user-rating-current-pro');
            bff.ajax('<?= $this->adminLink('ajax&act=rating-change'); ?>', {id:<?= $user_id ?>, rating_current: intval($current.html()), rating_new: $('#j-user-rating-new').val()}, function(data){
                if(data && data.success){
                    bff.success('Рейтинг был успешно изменен');
                    $current.html(data.rating_new);
                    $currentEdit.val(data.rating_new);
                    <? if(!empty($pro)) { ?>
                        $currentPro.html(data.rating_pro);
                    <? } ?>
                    $link.parent().hide().prev().show();
                }
            });
            return false;
        }
    };
}());
//]]>
</script>