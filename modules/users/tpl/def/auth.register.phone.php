<?php
tpl::includeJS('users.auth', false, 5);
?>
<div class="s-signBlock-form">
    <p><?= _t('users', 'На номер [phone] отправлен код подтверждения.<br>
            На этом регистрация будет завершена.<br>', array(
            'phone' => '<strong>+'.$phone.'</strong>',
        )); ?></p>
    <div class="s-signBlock-div text-center">
        <div id="j-u-register-phone-block-code">
            <form class="form-inline mrgb15" role="form" action="">
                <div class="form-group">
                    <label for="inputCode"><?= _t('users', 'Код подтверждения') ?></label>
                    <input type="text" class="form-control j-u-register-phone-code-input" id="inputCode" placeholder="<?= _t('users', 'Введите код из смс'); ?>">
                </div>
                <button type="submit" class="btn btn-primary j-u-register-phone-code-validate-btn"><?= _t('users', 'Подтвердить') ?></button>
            </form>
            <ul class="s-signBlock-links list-unstyled text-center">
                <li><a href="#" class="ajax-link j-u-register-phone-change-step1-btn"><span><?= _t('users', 'Изменить номер телефона'); ?></span></a></li>
                <li><a href="#" class="ajax-link j-u-register-phone-code-resend-btn"><span><?= _t('users', 'Выслать код повторно'); ?></span></a></li>
            </ul>
        </div>

        <div id="j-u-register-phone-block-phone" class="displaynone">
            <form class="form-inline mrgb15" role="form" action="">
                <div class="form-group">
                    <label for="j-u-register-phone"><?= _t('users', 'Номер телефона') ?></label>
                    <?= $this->registerPhoneInput(array('name'=>'phone', 'id'=>'j-u-register-phone-input')) ?>
                </div>
                <button type="button" class="btn btn-default j-u-register-phone-change-step2-btn"><?= _t('users', 'Выслать код') ?></button>
            </form>
        </div>
    </div>
</div>

<?= _t('users', 'Номер телефона будет использоваться:') ?>
<ul>
    <li><?= _t('users', 'Для идентификации вашей учетной записи;') ?></li>
    <li><?= _t('users', 'Для уведомления о подозрительных действиях с учетной записью;') ?></li>
    <li><?= _t('users', 'Для уведомления при получении государственных услуг.') ?></li>
</ul>
<p>
    <?= _t('users', 'Номер телефона не будет использоваться для отправки рекламных сообщений и в иных коммерческих целях.') ?>
</p>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jUserAuth.registerPhone(<?= func::php2js(array(
            'lang' => array(
                'resend_success' => _t('users', 'Код подтверждения был успешно отправлен повторно'),
                'change_success' => _t('users', 'Код подтверждения был отправлен на указанный вами номер'),
            ),
            'url' => (isset($url) ? $url : ''),
        )) ?>);
    });
    <? js::stop(); ?>
</script>