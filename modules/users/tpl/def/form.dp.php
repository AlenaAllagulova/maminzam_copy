<?php
use bff\db\Dynprops;
/**
 * @var $this Dynprops
 */

if(empty($dynprops)) return;

$drawControl = function($title, $value, $required, $class = array()){
    if( ! is_array($class)) $class = array($class);
    ?>
    <div class="p-additional-info-parameters">
        <h6><?= $title ?><?= $required ? ' <span class="text-danger">*</span>' : ''?></h6>
        <div class="row">
            <div class="<?= join(' ', $class) ?>">
                <?= $value ?>
            </div>
        </div>
    </div>
<?
};
?>

<?
# ---------------------------------------------------------------------------------------
# Дин. свойства:
foreach($dynprops as $d)
{
    $ID = $d['id'];
    $ownerID = $d[$this->ownerColumn];
    $name = $prefix.'['.$ownerID.']'.'['.$ID.']';
    $nameChild = $prefix.'['.$ownerID.']';
    $isRequred = $d['req'];
    $html = '';
    $class = array('j-dp');

    switch($d['type'])
    {
        # Группа св-в с единичным выбором
        case Dynprops::typeRadioGroup:
        {
            $value = (isset($d['value'])? $d['value'] : $d['default_value']);
            $class = ! empty($d['group_one_row']) ? 'radio-inline' : 'radio';
            $html = HTML::renderList($d['multi'], array($value), function($k,$i,$values) use ($name, $extra) {
                    $v = &$i['value'];
                    return '<div class="radio"><label><input type="radio" name="'.$name.'" '
                                .(in_array($v,$values)?' checked="checked"':'').' value="'.$v.'" />'.$i['name'].'</label></div>';
                },
                array(2=>4),
                array('class'=>'col-sm-6'),
                'div'
            );
            $drawControl($d['title_'.LNG], $html, $isRequred, 'col-sm-12');
        } break;

        # Группа св-в с множественным выбором
        case Dynprops::typeCheckboxGroup:
        {
            $value = ( isset($d['value']) && $d['value'] ? explode(';', $d['value']) : explode(';', $d['default_value']) );
            $class = ! empty($d['group_one_row']) ? 'checkbox-inline' : 'checkbox';
            $html = HTML::renderList($d['multi'], $value, function($k,$i,$values) use ($name, $extra) {
                    $v = &$i['value'];
                    return '<div class="checkbox"><label><input type="checkbox" name="'.$name.'[]" '
                                .(in_array($v,$values)?' checked="checked"':'').' value="'.$v.'" />'.$i['name'].'</label></div>';
                },
                array(2=>4),
                array('class'=>'col-sm-6'),
                'div'
            );
            $drawControl($d['title_'.LNG], $html, $isRequred, 'col-sm-12');
        } break;

        # Выбор Да/Нет
        case Dynprops::typeRadioYesNo:
        {
            $value = (isset($d['value'])? $d['value'] : $d['default_value']);
            $html = '<div class="radio-inline"> <label> <input type="radio" name="'.$name.'" value="2" '.($value == 2?'checked="checked"':'').' /> '.$this->langText['yes'].' </label> </div>
                     <div class="radio-inline"> <label> <input type="radio" name="'.$name.'" value="1" '.($value == 1?'checked="checked"':'').' /> '.$this->langText['no'].' </label> </div>';
            $drawControl($d['title_'.LNG], $html, $isRequred, 'col-sm-12');
        } break;

        # Флаг
        case Dynprops::typeCheckbox:
        {
            $value = (isset($d['value'])? $d['value'] : $d['default_value']);
            $html = '<input type="hidden" name="'.$name.'" value="0" /> <div class="checkbox"> <label> <input type="checkbox" name="'.$name.'" value="1" '.($value?'checked="checked"':'').' /> '.$this->langText['yes'].' </label> </div>';
            $drawControl($d['title_'.LNG], $html, $isRequred, 'col-sm-12');
        } break;

        # Выпадающий список
        case Dynprops::typeSelect:
        {
            $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
            if($d['parent'])
            {
                $html = '<select class="form-control input-sm j-dp-child" name="'.$name.'" data-id="'.$d['id'].'" data-name="'.$nameChild.'">';
                $html .= HTML::selectOptions($d['multi'], $value, false, 'value', 'name');
                $html .= '</select>';
                if( ! empty($d['description']) ) {
                    $html .= '<span class="input-group-addon">'.$d['description'].'</span>';
                }
                $htmlChild = '';
                if( ! empty($value) && isset($children[$ID])) {
                    $htmlChild .= $this->formChild($children[$ID], array('name'=>$nameChild, 'class'=>'input-sm form-control'));
                }
                $title = $d['title_'.LNG];
                ?>
                <div class="p-additional-info-parameters">
                    <h6><?= $title ?><?= $isRequred ? ' <span class="text-danger">*</span>' : ''?></h6>
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $html ?>
                        </div>
                        <div class="col-sm-6 j-dp-child-<?= $ID ?><?= empty($value) ? ' hidden' : '' ?>">
                            <?= $htmlChild ?>
                        </div>
                    </div>
                </div>
                <?
                continue 2;
            }
            else
            {
                $html = '<select class="form-control input-sm" name="'.$name.'">'.HTML::selectOptions($d['multi'], $value, false, 'value', 'name').'</select>';
                if( ! empty($d['description']) ) {
                    $html .= '<span class="input-group-addon">'.$d['description'].'</span>';
                }
                $drawControl($d['title_'.LNG], $html, $isRequred, 'col-sm-4');
            }
        } break;

        # Однострочное текстовое поле
        case Dynprops::typeInputText:
        {
            $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
            $html = '<input class="form-control input-sm" type="text" name="'.$name.'" value="'.HTML::escape($value).'" />';
            if ( ! empty($d['description']) ) {
                $html .= '<span class="input-group-addon">'.$d['description'].'</span>';
            }
            $drawControl($d['title_'.LNG], $html, $isRequred, 'col-sm-12');
        } break;

        # Многострочное текстовое поле
        case Dynprops::typeTextarea:
        {
            $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
            $html = '<textarea name="'.$name.'" rows="5" class="form-control">'.HTML::escape($value).'</textarea>';
            # уточнение к названию
            if ( ! empty($d['description']) ) {
                $html .= '<p class="help-block">'.$d['description'].'</p>';
            }
            $drawControl($d['title_'.LNG], $html, $isRequred, 'col-sm-12');
        } break;

        # Число
        case Dynprops::typeNumber:
        {
            $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
            $html = '<input type="text" class="form-control input-sm" name="'.$name.'" value="'.$value.'"/>';
            $sClass = '';
            if ( ! empty($d['description']) ) {
                if ( mb_strlen(strip_tags($d['description'])) <= 5 ) {
                    $html .= '<span class="input-group-addon">'.$d['description'].'</span>';
                    $sClass = ' input-group';
                } else {
                    $html .= '<p class="help-block">'.$d['description'].'</p>';
                }
            }
            $drawControl($d['title_'.LNG], $html, $isRequred, array('col-sm-12', $sClass));
        } break;

        # Диапазон
        case Dynprops::typeRange:
        {
            $value = (isset($d['value']) && $d['value'] ? $d['value'] : $d['default_value']);

            $html = '<select name="'.$name.'" class="form-control input-sm">';
            if ( ! empty($value) && ! intval($value)) {
                $html .= '<option value="0">'.$value.'</option>';
            }
            if ($d['start'] <= $d['end']) {
                for ($i = $d['start']; $i <= $d['end'];$i += $d['step']) {
                    $html .= '<option value="'.$i.'"'.($value == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
                }
            } else {
                for ($i = $d['start']; $i >= $d['end'];$i -= $d['step']) {
                    $html .= '<option value="'.$i.'"'.($value == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
                }
            }
            $html .= '</select>';

            # уточнение к названию
            if ( ! empty($d['description']) ) {
                $html .= '<p class="help-block">'.$d['description'].'</p>';
            }
            $drawControl($d['title_'.LNG], $html, $isRequred, 'col-sm-12');
        } break;
    }

}