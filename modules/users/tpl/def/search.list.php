<?php
    if( ! empty($list)):
        $aStatus = Users::aWorkerStatus();
        $bProEnabled = (bff::servicesEnabled() && Portfolio::previewOnlyPro());
        Portfolio::i();
        $lng_spec = _t('users', 'Специализация:');
        $lng_opinions = _t('opinions', 'Opinions:');
        $lng_rating = _t('users', 'Рейтинг:');
        $lng_budget = _t('users', 'Бюджет от:');
        $bUseServices = Specializations::useServices();
        $tagsLimit = Users::searchTagsLimit();
?>
    <ul class="o-freelancersList media-list">
        <? foreach($list as &$v): ?>
        <li class="media">
            <a href="<?= Users::url('profile', array('login' => $v['login'])); ?>" class="f-freelancer-avatar">
                <?= tpl::userAvatar($v) ?>
            </a>
            <div class="media-body">
                <div class="row o-freelancer-info">
                    <div class="col-sm-6">
                        <div class="f-freelancer-name">
                            <?= tpl::userLink($v); ?>
                            <? if($v['status'] != Users::WORKER_STATUS_NONE): ?>
                                <span class="label <?= $aStatus[ $v['status'] ]['c'] ?>">
                                    	<?= $aStatus[ $v['status'] ]['t'] ?>
                                </span>
                            <? endif; ?>
                        </div>
                        <div>
                            <span class="hidden-sm hidden-xs"><?= $lng_spec; ?></span> <a href="<?= Users::url('search-spec', $v) ?>"><?= $v['main_spec_title'] ?></a>
                        </div>
                        <? if( ! empty($v['city_data']['title'])): ?>
                        <div>
                            <i class="fa fa-map-marker"></i> <?= $v['city_data']['title'] ?><?= ! empty($v['country_data']['title']) ? ', '.$v['country_data']['title'] : '' ?>
                        </div>
                        <? if(!empty($v['district_id'])):?>
                                <?= _t('', 'р-н ');?>
                                <?= Geo::districtTitle($v['district_id'])?>
                        <?endif;?>
                        <? endif; ?>
                    </div>

                    <div class="col-sm-3">
                        <div class="o-feedbacks-inf">
                            <?= $lng_opinions; ?> <?= tpl::opinions($v['opinions_cache'], array('login' => $v['login'])); ?>
                        </div>
                        <div>
                            <?= $lng_rating; ?> <strong><?= Users::rating($v['rating'], $v['pro']) ?></strong>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <? if( ! empty($v['main_price'])): ?>
                        <div>
                            <i class="fa fa-clock-o"></i>
                            <?= ! empty($v['main_spec_price_title']) ? $v['main_spec_price_title'] : _t('users', 'Цена'); ?><?=
                            ! empty($v['main_price_rate_text'][LNG]) ? ' '.$v['main_price_rate_text'][LNG] : '' ?>:
                            <?= $v['main_price'] ?>
                            <?= Site::currencyData($v['main_price_curr'], 'title_short'); ?>
                        </div>
                        <? endif; ?>
                        <? if( ! empty($v['main_budget'])): ?>
                        <div>
                            <i class="fa fa-dollar"></i> <?= $lng_budget; ?>
                            <?= $v['main_budget'] ?>
                            <?= Site::currencyData($v['main_budget_curr'], 'title_short'); ?>
                        </div>
                        <? endif; ?>
                    </div>

                </div>

                <? if( ! empty($v['aTags'])): $n = 0; foreach($v['aTags'] as $vv): ?>
                    <? if($tagsLimit && $n == $tagsLimit): ?><a class="l-tag l-tag-more j-tag-more" href="#"><?= _t('', 'еще ...'); ?></a><? endif; $n++; ?>
                    <a href="<?= Users::url('search-tag', $vv) ?>" class="l-tag<?= $tagsLimit && $n > $tagsLimit ? ' hidden' : '' ?>"><?= $vv['tag'] ?></a>
                <? endforeach; endif; ?>
            </div>
            <? if( ! empty($v['portfolio_tab']) && (! $bProEnabled || ($bProEnabled && $v['pro']))): ?>
                <? if( ! empty($v['portfolio_intro'])): $v['portfolio_intro'] = htmlspecialchars(strip_tags($v['portfolio_intro'])); $v['portfolio_intro'] = tpl::truncate($v['portfolio_intro'], config::sysAdmin('users.search.list.portfolio_intro.truncate', 330, TYPE_UINT)) ?>
                <div class="f-infoText">
                    <?= $v['portfolio_intro'] ?>
                </div>
                <? endif; ?>

                <? if( ! empty($v['portfolio_works'])): $v['portfolio_works'] = func::unserialize($v['portfolio_works']);
                   if( ! empty($v['portfolio_works'])): ?>
                    <div class="f-propose-works visible-lg visible-md">
                        <? foreach($v['portfolio_works'] as $vv): ?>
                        <a href="<?= Portfolio::url('user.view', array('login' => $v['login'], 'id' => $vv['id'], 'keyword' => $vv['keyword'])) ?>" class="o-project-thumb_proposal">
                            <? if(!empty($vv['title'])){ ?><div class="o-project-thumb_proposal_title"><?= $vv['title'] ?></div><? } ?>
                            <div class="o-inner added">
                                <img src="<?= PortfolioItemPreview::url($vv['id'], $vv['preview']); ?>" alt="<?= ! empty($vv['title']) ? tpl::imageAlt(array('t' => $vv['title'])) : '' ?>" />
                            </div>
                        </a>
                        <? endforeach; ?>
                        <? for($i = count($v['portfolio_works']); $i < 3; $i++): ?>
                        <div class="o-project-thumb_proposal">
                            <div class="o-inner added">
                                <img src="<?= PortfolioItemPreview::url(0, '') ?>" alt="" />
                            </div>
                        </div>
                       <? endfor; ?>
                        <div class="clearfix"></div>
                    </div>
                <? endif; endif;?>
            <? endif; ?>

            <? $sCache = $spec_id == $v['spec_id'] ? 'services_cache' : 'main_services_cache';
                if( $bUseServices && ! empty($v['price_tab']) && ! empty($v[$sCache])): $cnt = $v['specs_services']; unset($v[$sCache]['cnt']); $show = 0;?>
                <div class="se-services-propose se-services-propose-mrg">
                    <? foreach($v[$sCache] as $vv): if( ! isset($vv['title']) || ! isset($vv['measure'])) continue; $show++; ?>
                    <div class="se-services-propose-row">
                        <div class="se-services-propose-title">
                            <span><?= $vv['title'] ?></span>
                        </div>
                        <div class="se-services-propose-price">
                            <div class="se-services-propose-price-in">
                                <? if($vv['price']): ?>
                                <strong><?= tpl::formatPrice($vv['price']); ?> <?= Site::currencyData($vv['price_curr'],'title_short')?></strong> <span>/ <?= $vv['measure'] ?></span>
                                <? else: ?>
                                    <?= _t('users', 'бесплатно') ?>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                    <? endforeach; ?>
                </div>
                <? if($show && $cnt > count($v[$sCache])): $cnt -= count($v[$sCache]); ?>
                    <div class="se-services-propose-btn">
                        <a href="<?= Users::url('user.price', array('login' => $v['login'])); ?>" class="btn btn-default btn-sm"><?= _t('', 'Еще'); ?> <?= tpl::declension($cnt, _t('','услуга;услуги;услуг')) ?></a> &nbsp;<?= _t('users', 'от исполнителя'); ?> <span><?= tpl::userLink($v, 'text no-login'); ?></span>
                    </div>
                <? endif; ?>
            <? endif; ?>
            </li>
        <? endforeach; unset($v); ?>

    </ul>
    <? else: ?>
        <div class="alert alert-info"><?= _t('users', 'Исполнители не найдены'); ?></div>
    <? endif;
