<?php

$aData['bMy'] = $bMy = User::isCurrent($id);
$isOwner = User::isCurrent($id);
# Порядок групп дин свойств специализации пользователя
$aDrawGroups = [
    Users::STATUS_TEXT_PART_NINTH =>
        [
            'title' => 'О себе',
            'class' => 'order-vue__row-big',
        ],
    Users::DP_USER_SPEC_PART_SECOND =>
        [
            'title' => 'Личная информация',
            'class' => 'order-vue__row-small',
        ],
    Users::DP_USER_SPEC_PART_FOURTH =>
        [
            'title' => 'Опыт',
            'class' => '',
        ],
    # дин св-во навыки
    Users::DP_USER_SPEC_PART_FIFTH =>
        [
            'title' => '',
            'class' => '',
        ],
    Users::DP_USER_SPEC_PART_THIRD =>
        [
            'title' => 'Образование и курсы',
            'class' => '',
        ],
    Users::DP_USER_SPEC_PART_ZERO =>
        [
            'title' => 'Предпочтения по работе',
            'class' => '',
        ],
    # дин св-во доп помощь
    Users::DP_USER_SPEC_PART_SIXTH =>
        [
            'title' => '',
            'class' => '',
        ],
    Users::SCHEDULE_PART_EIGHT =>
        [
            'title' => 'График работы',
            'class' => '',
        ],
    Users::DP_USER_SPEC_PART_FIRST =>
        [
            'title' => 'Полезная информация',
            'class' => '',
        ],
    # дин св-во pекомендации
    Users::DP_USER_SPEC_PART_SEVENTH =>
        [
            'title' => '',
            'class' => '',
        ],
];
?>

<? if (!empty($dynprops_simple)):?>
<div class="p-profileContent" id="j-owner-info-block">
    <div class="order-vue__row flex_wrap">
        <?// О себе start  ?>
        <? foreach ($aDrawGroups as $num_group => $group): ?>
            <? if( $num_group == Users::STATUS_TEXT_PART_NINTH): # О себе ?>
                <div class="order-vue__row-auto <?= !empty($group['class']? ''.$group['class'].'':'')?>">
                    <div class="order-vue__box h100p">
                        <? if(!empty($group['title'])){ ?>
                            <div class="settings-box__article mrgb10">
                                <?= _t('',$group['title']);  # заголовок группы дин свойств?>
                            </div>
                        <? } ?>
                        <div class="">
                            <?= $status_text ? $status_text : 'Не указано'?>
                        </div>
                    </div>
                </div>
            <? endif;?>
        <? endforeach; ?>
        <?// О себе finish  ?>

        <?//  Личная информация  start  ?>
        <div class="order-vue__row-auto <?= !empty($group['class']? ''.$group['class'].'':'')?>">
            <div class="order-vue__box h100p">
                <? foreach ($aDrawGroups as $num_group => $group): ?>
                    <? if($num_group == Users::DP_USER_SPEC_PART_SECOND):  ?>
                        <? if(!empty($group['title'])){ ?>
                            <div class="settings-box__article mrgb10">
                                <?= _t('',$group['title']);  # заголовок группы дин свойств?>
                            </div>
                        <? } ?>
                        <ul class="list-dp">
                            <? foreach ($dynprops_simple as $dp_item): ?>
                                <? if ($dp_item['group_id'] == $num_group ): # $num_group  ?>
                                    <li>
                                        <? if($dp_item['value'] !== ''): ?>
                                            <? if(empty($group['title'])) :  # название дин свойства ?>
                                                <div class="settings-box__article mrgb10">
                                                    <?= $dp_item['title_'.LNG]; ?>
                                                </div>
                                            <? else:?>
                                                <?= $dp_item['title_'.LNG].':'; ?>
                                            <?endif; ?>
                                            <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                            <? if (count($dp_item['value']) == 1):
                                                $dp_item['value'] = reset($dp_item['value']); ?>
                                                <? if(!is_array($dp_item['multi'])): ?>
                                                    <?= $dp_item['value'] . !empty($dp_item['description'])? $dp_item['description']: '';?>
                                                <? else:
                                                    foreach ($dp_item['multi'] as $val):?>
                                                        <? if ($dp_item['value'] == $val['value']):?>
                                                            <?= $val['name']; # значение дин свойства  ?>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                <? endif; ?>
                                            <? else: ?>
                                                <span class="comma">
                                                    <? foreach ($dp_item['value'] as $dp_val): ?>
                                                        <? foreach ($dp_item['multi'] as $val): ?>
                                                            <? if ($dp_val == $val['value']):?>
                                                                <span>
                                                                    <?= $val['name']; # значение дин свойства ?>
                                                                </span>
                                                            <? endif; ?>
                                                        <? endforeach; ?>
                                                    <? endforeach; ?>
                                                </span>
                                            <? endif; ?>
                                        <? else: ?>
                                            <span><?= _t('dp_orders','Не указано') ?></span>
                                        <? endif;?>
                                    </li>
                                <? endif; ?>
                            <? endforeach; ?>
                        </ul>
                    <? endif;?>
                <? endforeach; ?>
            </div>
        </div>
        <?//  Личная информация  finish  ?>
    </div>

    <div class="order-vue__row flex_wrap">

        <?//  Опыт и образование  start  ?>
        <div class="order-vue__row-auto <?= !empty($group['class']? ''.$group['class'].'':'')?>">
            <div class="order-vue__box h100p">
                <? foreach ($aDrawGroups as $num_group => $group): ?>
                    <? if($num_group == Users::DP_USER_SPEC_PART_THIRD):  ?>
                        <? if(!empty($group['title'])){ ?>
                            <div class="settings-box__article mrgb10">
                                <?= _t('',$group['title']);  # заголовок группы дин свойств?>
                            </div>
                        <? } ?>
                        <ul class="list-dp">
                            <? foreach ($dynprops_simple as $dp_item): ?>
                                <? if ($dp_item['group_id'] == $num_group ): # $num_group  ?>
                                    <li>
                                        <? if($dp_item['value'] !== ''): ?>
                                            <? if(empty($group['title'])) :  # название дин свойства ?>
                                                <div class="settings-box__article mrgb10">
                                                    <?= $dp_item['title_'.LNG]; ?>
                                                </div>
                                            <? else:?>
                                                <?= $dp_item['title_'.LNG].':'; ?>
                                            <?endif; ?>
                                            <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                            <? if (count($dp_item['value']) == 1):
                                                $dp_item['value'] = reset($dp_item['value']); ?>
                                                <? if(!is_array($dp_item['multi'])): ?>
                                                <?= $dp_item['value'] . !empty($dp_item['description'])? $dp_item['description']: '';?>
                                            <? else:
                                                foreach ($dp_item['multi'] as $val):?>
                                                    <? if ($dp_item['value'] == $val['value']):?>
                                                        <?= $val['name']; # значение дин свойства  ?>
                                                    <? endif; ?>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                            <? else: ?>
                                                <span class="comma">
                                                    <? foreach ($dp_item['value'] as $dp_val): ?>
                                                        <? foreach ($dp_item['multi'] as $val): ?>
                                                            <? if ($dp_val == $val['value']):?>
                                                                <span>
                                                                    <?= $val['name']; # значение дин свойства ?>
                                                                </span>
                                                            <? endif; ?>
                                                        <? endforeach; ?>
                                                    <? endforeach; ?>
                                                </span>
                                            <? endif; ?>
                                        <? else: ?>
                                            <span><?= _t('dp_orders','Не указано') ?></span>
                                        <? endif;?>
                                    </li>
                                <? endif; ?>
                            <? endforeach; ?>
                        </ul>
                    <? endif;?>
                <? endforeach; ?>
                <? foreach ($aDrawGroups as $num_group => $group): ?>
                    <? if($num_group == Users::DP_USER_SPEC_PART_FOURTH):  ?>
                        <? if(!empty($group['title'])){ ?>
                            <div class="settings-box__article mrgt20 mrgb10">
                                <?= _t('',$group['title']);  # заголовок группы дин свойств?>
                            </div>
                        <? } ?>
                        <ul class="list-dp">
                            <? foreach ($dynprops_simple as $dp_item): ?>
                                <? if ($dp_item['group_id'] == $num_group ): # $num_group  ?>
                                    <li>
                                        <? if($dp_item['value'] !== ''): ?>
                                            <? if(empty($group['title'])) :  # название дин свойства ?>
                                                <div class="settings-box__article mrgb10">
                                                    <?= $dp_item['title_'.LNG]; ?>
                                                </div>
                                            <? else:?>
                                                <?= $dp_item['title_'.LNG].':'; ?>
                                            <?endif; ?>
                                            <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                            <? if (count($dp_item['value']) == 1):
                                                $dp_item['value'] = reset($dp_item['value']); ?>
                                                <? if(!is_array($dp_item['multi'])): ?>
                                                <?= $dp_item['value'] . !empty($dp_item['description'])? $dp_item['description']: '';?>
                                            <? else:
                                                foreach ($dp_item['multi'] as $val):?>
                                                    <? if ($dp_item['value'] == $val['value']):?>
                                                        <?= $val['name']; # значение дин свойства  ?>
                                                    <? endif; ?>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                            <? else: ?>
                                                <span class="comma">
                                                    <? foreach ($dp_item['value'] as $dp_val): ?>
                                                        <? foreach ($dp_item['multi'] as $val): ?>
                                                            <? if ($dp_val == $val['value']):?>
                                                                <span>
                                                                    <?= $val['name']; # значение дин свойства ?>
                                                                </span>
                                                            <? endif; ?>
                                                        <? endforeach; ?>
                                                    <? endforeach; ?>
                                                </span>
                                            <? endif; ?>
                                        <? else: ?>
                                            <span><?= _t('dp_orders','Не указано') ?></span>
                                        <? endif;?>
                                    </li>
                                <? endif; ?>
                            <? endforeach; ?>
                        </ul>
                    <? endif;?>
                <? endforeach; ?>
            </div>
        </div>
        <?//  Опыт  finish  ?>

        <?//  Навыки start  ?>
        <div class="order-vue__row-auto <?= !empty($group['class']? ''.$group['class'].'':'')?>">
            <div class="order-vue__box h100p">
                <? foreach ($aDrawGroups as $num_group => $group): ?>
                    <? if($num_group == Users::DP_USER_SPEC_PART_SIXTH):  ?>
                        <? if(!empty($group['title'])){ ?>
                            <div class="settings-box__article mrgb10">
                                <?= _t('',$group['title']);  # заголовок группы дин свойств?>
                            </div>
                        <? } ?>
                        <ul class="list-dp">
                            <? foreach ($dynprops_simple as $dp_item): ?>
                                <? if ($dp_item['group_id'] == $num_group ): # $num_group  ?>
                                    <? if($dp_item['value'] !== ''): ?>
                                        <? if(empty($group['title'])) :  # название дин свойства ?>
                                            <div class="settings-box__article mrgb10">
                                                <?= $dp_item['title_'.LNG]; ?>
                                            </div>
                                        <? else:?>
                                            <?= $dp_item['title_'.LNG].':'; ?>
                                        <?endif; ?>
                                        <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                        <? if (count($dp_item['value']) == 1):
                                            $dp_item['value'] = reset($dp_item['value']); ?>
                                            <? if(!is_array($dp_item['multi'])): ?>
                                            <?= $dp_item['value'] . !empty($dp_item['description'])? $dp_item['description']: '';?>
                                        <? else:
                                            foreach ($dp_item['multi'] as $val):?>
                                                <? if ($dp_item['value'] == $val['value']):?>
                                                    <?= $val['name']; # значение дин свойства  ?>
                                                <? endif; ?>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                        <? else: ?>
                                            <? foreach ($dp_item['value'] as $dp_val): ?>
                                                <? foreach ($dp_item['multi'] as $val): ?>
                                                    <? if ($dp_val == $val['value']):?>
                                                        <li>
                                                            <?= $val['name']; # значение дин свойства ?>
                                                        </li>
                                                    <? endif; ?>
                                                <? endforeach; ?>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                    <? else: ?>
                                        <div class="settings-box__article mrgb10">
                                            <?= _t('','Навыки')  # заголовок группы дин свойств?>
                                        </div>
                                        <span><?= _t('dp_orders','Не указано') ?></span>
                                    <? endif;?>
                                <? endif; ?>
                            <? endforeach; ?>
                        </ul>
                    <? endif;?>
                <? endforeach; ?>
            </div>
        </div>
        <?//  Навыки  finish  ?>

    </div>

    <div class="order-vue__row mrgb10">
        <div class="order-vue__row-big">
            <?// Предпочтения по работе start ?>
            <div class="order-vue__box mrgb10">
                <? foreach ($aDrawGroups as $num_group => $group): ?>
                    <? if ($num_group == Users::DP_USER_SPEC_PART_ZERO): # Дополнительные поля для блока "Предпочтения по работе"?>
                        <? if(!empty($group['title'])){ ?>
                            <div class="settings-box__article mrgb10">
                                <?= _t('',$group['title']);  # заголовок группы дин свойств?>
                            </div>
                        <? } ?>

                        <table class="work-info">
                            <tbody>
                                <? if(!empty($district_id)):?>
                                    <tr>
                                        <th><?= _t('users', 'Район '); ?></th>
                                        <th><?= Geo::districtTitle($district_id)?></th>
                                    </tr>
                                <? endif; ?>
                                <? if( ! empty($specs['price'])): # Цена  ?>
                                    <? if(!empty($specs['price_rate_text'])){ $specs['price_rate_text'] = unserialize($specs['price_rate_text']);}?>
                                    <tr>
                                        <th><?= _t('users', 'Желаемая зарплата:'); ?></th>
                                        <th>
                                            <?= tpl::formatPrice($specs['price']) ?>
                                            <?= Site::currencyData($specs['price_curr'], 'title_short'); ?>
                                            <?= ! empty($specs['price_rate_text'][LNG]) ? '/'.$specs['price_rate_text'][LNG] : '' ;?>
                                        </th>
                                    </tr>
                                <? endif; ?>

                                <? foreach ($dynprops_simple as $dp_item): ?>
                                    <tr>
                                        <? if ($dp_item['group_id'] == $num_group ): # $num_group  ?>
                                            <? if($dp_item['value'] !== ''): ?>
                                                <? if(empty($group['title'])) :  # название дин свойства ?>
                                                    <th>
                                                        <?= $dp_item['title_'.LNG]; ?>
                                                    </th>
                                                <? else:?>
                                                    <th style="vertical-align: top">
                                                        <?= $dp_item['title_'.LNG].':'; ?>
                                                    </th>
                                                <?endif; ?>
                                                <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                                <? if (count($dp_item['value']) == 1):
                                                    $dp_item['value'] = reset($dp_item['value']); ?>
                                                    <? if(!is_array($dp_item['multi'])): ?>
                                                    <th>
                                                        <?= $dp_item['value'] . !empty($dp_item['description'])? $dp_item['description']: '';?>
                                                        <?= _t('','от зарплаты') ?>
                                                    </th>
                                                <? else:
                                                    foreach ($dp_item['multi'] as $val):?>
                                                        <? if ($dp_item['value'] == $val['value']):?>
                                                            <th>
                                                                <?= $val['name']; # значение дин свойства  ?>
                                                            </th>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                <? endif; ?>
                                                <? else: ?>
                                                    <th>
                                                        <? foreach ($dp_item['value'] as $dp_val): ?>
                                                            <? foreach ($dp_item['multi'] as $val): ?>
                                                                <? if ($dp_val == $val['value']):?>
                                                                    <div>
                                                                        <?= $val['name']; # значение дин свойства ?>
                                                                    </div>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? endforeach; ?>
                                                    <tr>
                                                <? endif; ?>

                                            <? endif;?>
                                        <? endif; ?>
                                    </tr>
                                <? endforeach; ?>

                            </tbody>
                        </table>



                    <? endif;?>
                <? endforeach;?>
            </div>
            <?// Предпочтения по работе finish ?>

            <?//График работы start ?>
            <? foreach ($aDrawGroups as $num_group => $group): ?>
                <? if( $num_group == Users::SCHEDULE_PART_EIGHT ): #График работы ?>
                    <div class="order-vue__box">
                        <? if(!empty($group['title'])){ ?>
                            <div class="settings-box__article mrgb10">
                                <?= _t('',$group['title']);  # заголовок группы дин свойств?>
                            </div>
                        <? } ?>
                        <? if($type == Users::TYPE_WORKER):?>
                            <?$aData['bScheduleViewOnly'] = true;?>
                            <?= $this->viewPHP($aData, 'schedule.block')?>
                        <? endif;?>
                    </div>
                <? endif;?>
            <? endforeach; ?>
            <?//График работы finish ?>
        </div>
        <div class="order-vue__row-small">
            <?//  Дополнительная помощь  start  ?>
                <div class="order-vue__box h100p">
                    <? foreach ($aDrawGroups as $num_group => $group): ?>
                        <? if($num_group == Users::DP_USER_SPEC_PART_SEVENTH):  ?>
                            <? if(!empty($group['title'])){ ?>
                                <div class="settings-box__article mrgb10">
                                    <?= _t('',$group['title']);  # заголовок группы дин свойств?>
                                </div>
                            <? } ?>
                            <ul class="list-dp">
                                <? foreach ($dynprops_simple as $dp_item): ?>
                                    <? if ($dp_item['group_id'] == $num_group ): # $num_group  ?>
                                        <? if($dp_item['value'] !== ''): ?>
                                            <? if(empty($group['title'])) :  # название дин свойства ?>
                                                <div class="settings-box__article mrgb10">
                                                    <?= $dp_item['title_'.LNG]; ?>
                                                </div>
                                            <? else:?>
                                                <?= $dp_item['title_'.LNG].':'; ?>
                                            <?endif; ?>
                                            <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                            <? if (count($dp_item['value']) == 1):
                                                $dp_item['value'] = reset($dp_item['value']); ?>
                                                <? if(!is_array($dp_item['multi'])): ?>
                                                <li>
                                                    <?= $dp_item['value'] . !empty($dp_item['description'])? $dp_item['description']: '';?>
                                                </li>
                                            <? else:
                                                foreach ($dp_item['multi'] as $val):?>
                                                    <? if ($dp_item['value'] == $val['value']):?>
                                                        <li>
                                                            <?= $val['name']; # значение дин свойства  ?>
                                                        </li>
                                                    <? endif; ?>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                            <? else: ?>
                                                <? foreach ($dp_item['value'] as $dp_val): ?>
                                                    <? foreach ($dp_item['multi'] as $val): ?>
                                                        <? if ($dp_val == $val['value']):?>
                                                            <li>
                                                                <?= $val['name']; # значение дин свойства ?>
                                                            </li>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                        <? else: ?>
                                            <div class="settings-box__article mrgb10">
                                                <?= _t('',' Дополнительная помощь ');  # заголовок группы дин свойств?>
                                            </div>
                                            <span><?= _t('dp_orders','Не указано') ?></span>
                                        <? endif;?>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </ul>
                        <? endif;?>
                    <? endforeach; ?>
                </div>
            <?//  Дополнительная помощь   finish  ?>
        </div>
    </div>


    <div class="order-vue__row flex_wrap">

        <?//  Полезная информация start  ?>
        <div class="order-vue__row-auto <?= !empty($group['class']? ''.$group['class'].'':'')?>">
            <div class="order-vue__box h100p">
                <? foreach ($aDrawGroups as $num_group => $group): ?>
                    <? if($num_group == Users::DP_USER_SPEC_PART_FIRST):  ?>
                        <? if(!empty($group['title'])){ ?>
                            <div class="settings-box__article mrgb10">
                                <?= _t('',$group['title']);  # заголовок группы дин свойств?>
                            </div>
                        <? } ?>
                        <ul class="list-dp">
                            <? foreach ($dynprops_simple as $dp_item): ?>
                                <? if ($dp_item['group_id'] == $num_group ): # $num_group  ?>
                                    <li>
                                        <? if($dp_item['value'] !== ''): ?>
                                            <? if(empty($group['title'])) :  # название дин свойства ?>
                                                <div class="settings-box__article mrgb10">
                                                    <?= $dp_item['title_'.LNG]; ?>
                                                </div>
                                            <? else:?>
                                                <?= $dp_item['title_'.LNG].':'; ?>
                                            <?endif; ?>
                                            <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                            <? if (count($dp_item['value']) == 1):
                                                $dp_item['value'] = reset($dp_item['value']); ?>
                                                <? if(!is_array($dp_item['multi'])): ?>
                                                <?= $dp_item['value'] . !empty($dp_item['description'])? $dp_item['description']: '';?>
                                            <? else:
                                                foreach ($dp_item['multi'] as $val):?>
                                                    <? if ($dp_item['value'] == $val['value']):?>
                                                        <?= $val['name']; # значение дин свойства  ?>
                                                    <? endif; ?>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                            <? else: ?>
                                                <span class="comma">
                                                    <? foreach ($dp_item['value'] as $dp_val): ?>
                                                        <? foreach ($dp_item['multi'] as $val): ?>
                                                            <? if ($dp_val == $val['value']):?>
                                                                <span>
                                                                    <?= $val['name']; # значение дин свойства ?>
                                                                </span>
                                                            <? endif; ?>
                                                        <? endforeach; ?>
                                                    <? endforeach; ?>
                                                </span>
                                            <? endif; ?>
                                        <? else: ?>
                                                <span><?= _t('dp_orders','Не указано') ?></span>
                                        <? endif;?>
                                    </li>
                                <? endif; ?>
                            <? endforeach; ?>
                        </ul>
                    <? endif;?>
                <? endforeach; ?>
            </div>
        </div>
        <?//  Полезная информация  finish  ?>

        <?//  Рекомендации start  ?>
        <div class="order-vue__row-auto <?= !empty($group['class']? ''.$group['class'].'':'')?>">
            <div class="order-vue__box h100p">
                <? foreach ($aDrawGroups as $num_group => $group): ?>
                    <? if($num_group == Users::DP_USER_SPEC_PART_FIFTH):  ?>
                        <? if(!empty($group['title'])){ ?>
                            <div class="settings-box__article mrgb10">
                                <?= _t('',$group['title']);  # заголовок группы дин свойств?>
                            </div>
                        <? } ?>
                        <ul class="list-dp">
                            <? foreach ($dynprops_simple as $dp_item): ?>
                                <? if ($dp_item['group_id'] == $num_group ): # $num_group  ?>
                                    <? if($dp_item['value'] !== ''): ?>
                                        <? if(empty($group['title'])) :  # название дин свойства ?>
                                            <div class="settings-box__article mrgb10">
                                                <?= $dp_item['title_'.LNG]; ?>
                                            </div>
                                        <? else:?>
                                            <?= $dp_item['title_'.LNG].':'; ?>
                                        <?endif; ?>
                                        <? $dp_item['value'] = explode(';', $dp_item['value']); ?>
                                        <? if (count($dp_item['value']) == 1):
                                            $dp_item['value'] = reset($dp_item['value']); ?>
                                            <? if(!is_array($dp_item['multi'])): ?>
                                            <?= $dp_item['value'] . !empty($dp_item['description'])? $dp_item['description']: '';?>
                                        <? else:
                                            foreach ($dp_item['multi'] as $val):?>
                                                <? if ($dp_item['value'] == $val['value']):?>
                                                    <li>
                                                        <?= $val['name']; # значение дин свойства  ?>
                                                    </li>
                                                <? endif; ?>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                        <? else: ?>
                                            <? foreach ($dp_item['value'] as $dp_val): ?>
                                                <? foreach ($dp_item['multi'] as $val): ?>
                                                    <? if ($dp_val == $val['value']):?>
                                                        <li>
                                                            <?= $val['name']; # значение дин свойства ?>
                                                        </li>
                                                    <? endif; ?>
                                                <? endforeach; ?>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                    <? else: ?>
                                        <div class="settings-box__article mrgb10">
                                            <?= _t('','Рекомендации'); ?>
                                        </div>
                                        <span><?= _t('dp_orders','Не указано') ?></span>
                                    <? endif;?>
                                <? endif; ?>
                            <? endforeach; ?>
                        </ul>
                    <? endif;?>
                <? endforeach; ?>
            </div>
        </div>
        <?//  Рекомендации  finish  ?>

    </div>

    <? else: ?>
        <? if($isOwner): ?>
            <div class="mrgt10 text-center">
                <div class="max-w250 m0-a">
                    <?= _t('','Ваш профиль не заполнен. Добавьте информацию о себе, чтобы Вас нашли работодатели.')?>
                    <a href="<?= Users::url('my.settings', array('tab' => 'questionary')); ?>" class="mrgt10 btn btn-primary btn-block">
                        <?= _t('users', 'Заполнить анкету'); ?>
                    </a>
                </div>
            </div>
            <? else: ?>
            <div class=" mrgt20 text-center">
                <img src="<?= bff::url('/img/sad.svg')?>" width="40" alt="">
                <br>
                <br>
                <?= _t('opinions', 'Профиль пока что не заполнен'); ?>
            </div>
        <? endif; ?>
    </div>
<? endif; ?>

<script type="text/javascript">
<? js::start() ?>
<? js::stop() ?>
</script>