<?php
/**
 * Прайс пользователя
 * @var $this Users
 * @var $specs array
 * @var $services_cnt integer
 */
?>
<div class="p-profileContent">
    <? if( ! empty($services_cnt)): ?>
    <ul class="se-price-list">
        <? foreach($specs as $v): if(empty($v['services'])) continue; ?>
        <li>
            <h2><?= $v['spec_title'] ?></h2>
            <table class="table se-price-list-table">
                <thead class="hidden-xs">
                <tr>
                    <th><?= _t('', 'Услуга'); ?></th>
                    <th class="se-price-list-table-price text-left"><?= _t('', 'Стоимость'); ?></th>
                </tr>
                </thead>
                <tbody>
                <? foreach($v['services'] as $vv): if($vv['price'] == 0 && ! $vv['price_free']) continue; ?>
                <tr>
                    <td class="se-price-list-table-name">
                        <div><?= $vv['title'] ?></div>
                    </td>
                    <td class="se-price-list-table-price text-left">
                        <? if($vv['price']): ?>
                        <strong><?= tpl::formatPrice($vv['price']); ?> <?= Site::currencyData($vv['price_curr'],'title_short')?></strong> <span>/ <?= $vv['measure'] ?></span>
                        <? else: ?>
                            <?= _t('users', 'бесплатно') ?>
                        <? endif; ?>
                    </td>
                </tr>
                <? endforeach; ?>
                </tbody>
            </table>
        </li>
        <? endforeach; ?>
    </ul>
<? else: ?>
    <div class="alert alert-info"><?= _t('users', 'Услуги не найдены'); ?></div>
<? endif; ?>
</div>