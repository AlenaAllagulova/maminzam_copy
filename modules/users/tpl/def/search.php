<?php
/**
 * Список фрилансеров.
 * @var $this Users
 */
if(Users::profileMap()){
    Geo::mapsAPI(true);
    if(Geo::mapsType() == Geo::MAPS_TYPE_GOOGLE){
        tpl::includeJS('markerclusterer/markerclusterer', false);
    }
}
tpl::includeJS('users.search', false, 5);
?>
    <div class="container">

        <section class="l-searchAdd">
            <div class="row">
                <div class="col-sm-12">
                    <div class="l-search-bar">
                        <form role="search" method="get" action="<?= Users::url('list') ?>">
                            <input type="search" name="q" class="form-control" placeholder="<?= _t('users', 'Найти исполнителя'); ?>">
                            <button class="l-search-button"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section class="l-mainContent" id="j-users-search">
            <div class="row">

                <!-- Left Column -->
                <aside class="col-md-3">
                    <?= $form ?>
                </aside>

                <div class="col-md-9 l-content-column" id="j-users-search-list">

                    <div class="l-list-heading">
                        <h6 class="hidden-xs pull-left"><?= _t('users', 'Найдено'); ?> <span class="j-users-count"><?= $count ?></span></h6>
                        <? if(Users::profileMap()): ?>
                        <ul class="pull-right">
                            <li<?= $f['m'] == 0 ? ' class="active"' : '' ?>><a href="#" class="j-f-view-type" data-id="0"><i class="fa fa-list"></i> <?= _t('users', 'Списком'); ?></a></li>
                            <li<?= $f['m'] == 1 ? ' class="active"' : '' ?>><a href="#" class="j-f-view-type" data-id="1"><i class="fa fa-map-marker"></i> <?= _t('users', 'На карте'); ?></a></li>
                        </ul>
                        <? endif; ?>

                        <div class="clearfix"></div>
                    </div>

                    <div class="l-mapView <?= ! $f['m'] ? 'hidden' : '' ?>">
                        <div id="map-desktop" class="map-google"></div>
                    </div>

                    <? if($f['m']): ?>
                        <div class="j-pagination"><?= $pgn ?></div>
                        <div class="j-list"><?= $list ?></div>
                    <? else: ?>
                        <div class="j-list"><?= $list ?></div>
                        <div class="j-pagination"><?= $pgn ?></div>
                    <? endif; ?>

                </div>

            </div>
        </section>

    </div>
<script type="text/javascript">
<? js::start() ?>
jUsersSearch.init(<?= func::php2js(array(
    'lang' => array(),
    'ajax' => true,
    'locationDefault' => Users::url('list'),
    'defCountry'    => Geo::defaultCountry(),
    'rootSpec'      => Specializations::ROOT_SPEC,
    'currSpec'      => $spec_id,
    'currCat'       => $cat_id,
    'points'        => ! empty($points) ? $points : array(),
    'coordorder'    => Geo::$ymapsCoordOrder,
    'coordDefaults' => Geo::$ymapsDefaultCoords,
    'preSuggest'    => Geo::countrySelect() && $f['c'] ? Geo::regionPreSuggest($f['c'], 2) : '',
    'cityID'        => Geo::filterEnabled() ? Geo::filter('id-city') : 0,
)) ?>);
<? js::stop() ?>
</script>