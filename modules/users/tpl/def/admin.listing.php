<?php
    tpl::includeJS(array('datepicker','autocomplete'), true);
    $f = HTML::escape($f, 'html', array('r_from','r_to','a_from','a_to','q'));
    tplAdmin::adminPageSettings(array('link'=>array(
        'title'=>'+ добавить пользователя','href'=>$this->adminLink('user_add')
    )));

    $aTabs = array(
        0 => array('t'=>'Все'),
        1 => array('t'=>'Активированные'),
        2 => array('t'=>'Неактивированные'),
        3 => array('t'=>'Заблокированные'),
        4 => array('t'=>'Подписавшиеся'),
        5 => array('t'=>'Доверенные'),
        6 => array('t'=>'Верификация', 'counter'=>config::get('users_verified_waiting', 0)),
    );
    $verified = Users::verifiedEnabled();
    $verifiedStatuses = Users::verifiedStatuses();
    if( ! $verified){
        unset($aTabs[6]);
    }
?>
<script type="text/javascript">
$(function(){
    var $filter = $('#j-users-listing-filter-form');
    bff.datepicker('.bff-datepicker', {yearRange: '-3:+3'});
    $('#j-users-listing-filter-region').autocomplete('<?= $this->adminLink('regionSuggest','geo') ?>',
        {valueInput: '#j-users-listing-filter-region-id', suggest: <?= Geo::regionPreSuggest($f['country']) ?>, params:{reg:1},
            onSelect: function() {
                $filter.submit();
            }
        });

    $('#j-users-listing-filter-cancel').on('click', function(e){ nothing(e);
        var filter = $filter.get(0);
        filter.elements.page.value = 1;
        filter.elements.r_from.value = '';
        filter.elements.r_to.value = '';
        filter.elements.a_from.value = '';
        filter.elements.a_to.value = '';
        filter.elements.q.value = '';
        <? if(Users::profileMap()): ?>
            <? if(Geo::countrySelect()): ?>
            filter.elements.country.value = 0;
            <? endif; ?>
        filter.elements.region.value = 0;
        filter.elements.region_t.value = '';
        <? endif; ?>
        <? if(Users::useClient()): ?>
        filter.elements.type.value = 0;
        <? endif; ?>
        filter.submit();
    });

    $('.j-users-listing-filter-status-tab').on('click', function(e){ nothing(e);
        var statusID = $(this).data('id');
        $filter.find('.j-users-listing-filter-status').val(statusID);
        if(intval(statusID) == 6){
            $filter.find('[name="order"]').val('verified<?= tpl::ORDER_SEPARATOR ?>desc');
        }
        $filter.get(0).elements.page.value = 1;
        $filter.submit();
    });

    $('.j-change').change(function(){
        $filter.get(0).elements.page.value = 1;
        $filter.submit();
    });
});
</script>

<div class="tabsBar" id="items-status-tabs">
    <? foreach($aTabs as $k=>$v) { ?>
    <span class="tab<?= $k==$f['status'] ? ' tab-active' : '' ?>"><a href="#" data-id="<?= $k ?>" class="j-users-listing-filter-status-tab"><?= $v['t'] ?><?= !empty($v['counter']) ? ' ('.$v['counter'].')' : '' ?></a></span>
    <? } ?>
</div>

<div class="actionBar">
<form action="" method="get" name="filters" id="j-users-listing-filter-form" class="form-inline">
    <input type="hidden" name="s" value="users" />
    <input type="hidden" name="ev" value="listing" />
    <input type="hidden" name="status" value="<?= $f['status'] ?>" class="j-users-listing-filter-status" />
    <input type="hidden" name="order" value="<?= $order_by.tpl::ORDER_SEPARATOR.$order_dir ?>" />
    <input type="hidden" name="page" value="1" />
    <? if(Users::useClient()): ?><select name="type" class="j-change" style="width: 100px;"><?= HTML::selectOptions(Users::aTypes(), $f['type'], _t('users', '- Тип -'), 'id', 'plural') ?></select><? endif; ?>
    <label>регистрация: <input type="text" name="r_from" value="<?= $f['r_from'] ?>" placeholder="от" style="width: 70px;" class="bff-datepicker" /></label><label>&nbsp;<input type="text" name="r_to" value="<?= $f['r_to'] ?>" placeholder="до" style="width: 70px;" class="bff-datepicker" />&nbsp;</label>
    <label>был: <input type="text" name="a_from" value="<?= $f['a_from'] ?>" placeholder="от" style="width: 70px;" class="bff-datepicker" /></label><label>&nbsp;<input type="text" name="a_to" value="<?= $f['a_to'] ?>" placeholder="до" style="width: 70px;" class="bff-datepicker" />&nbsp;</label>
    <label><input type="text" placeholder="ID / логин / e-mail" name="q" value="<?= $f['q'] ?>" style="width: 155px;" /></label>
    <? if(Users::profileMap()): ?><br />
        <? if(Geo::countrySelect()): ?><select name="country" class="j-change" style="width: 100px;"> <?= HTML::selectOptions(Geo::countryList(), $f['country'], 'Страна', 'id', 'title') ?></select><? endif; ?>
        <label>
            <input type="hidden" name="region" value="<?= $f['region'] ?>" id="j-users-listing-filter-region-id" />
            <input type="text" value="<?= Geo::regionTitle($f['region']) ?>" id="j-users-listing-filter-region" name="region_t" class="autocomplete" placeholder="Город" style="width: 175px;" />
        </label>
    <? endif; ?>
    <input type="submit" value="<?= _t('', 'search') ?>" class="btn btn-small button submit" />
    <a class="cancel" id="j-users-listing-filter-cancel"><?= _t('', 'reset') ?></a>
</form>
</div>

<table class="table table-condensed table-hover admtbl tblhover">
<thead>
    <tr class="header">
        <?
            $aHeaderCols = array(
                'user_id' => array('t'=>'ID','w'=>60,'order'=>'desc'),
                'email' => array('t'=>'E-mail','w'=>165,'order'=>'asc','align'=>'left'),
                'name' => array('t'=>'Имя','align'=>'left'),
                'verified' => array('t' => '<i class="icon-ok"></i>', 'order'=>'desc', 'title' => 'Верификация пользователей'),
                'pro' => array('t'=>'PRO', 'w' => 40, 'order'=>'desc'),
                'last_login' => array('t'=>'Был','w'=>125,'order'=>'desc'),
                'action' => array('t'=>'Действие','w'=>85),
            );
            if( ! $verified){
                unset($aHeaderCols['verified']);
            }
            $urlOrderBy = $this->adminLink('listing'.$filter.'&page=1&order=');
            foreach($aHeaderCols as $k=>$v) {
                ?><th<? if( ! empty($v['w']) ) { ?> width="<?= $v['w'] ?>"<? } if( ! empty($v['align']) ) { ?>  class="<?= $v['align'] ?>"<? } ?>><?
                if( ! empty($v['order'])) {
                    if( $order_by == $k ) {
                        ?><a href="<?= $urlOrderBy.$k.tpl::ORDER_SEPARATOR.$order_dir_needed ?>"<?= ! empty($v['title']) ? ' title="'.$v['title'].'"' : '' ?>><?= $v['t'] ?><div class="order-<?= $order_dir ?>"></div></a><?
                    } else {
                        ?><a href="<?= $urlOrderBy.$k.tpl::ORDER_SEPARATOR.$v['order'] ?>"<?= ! empty($v['title']) ? ' title="'.$v['title'].'"' : '' ?>><?= $v['t'] ?></a><?
                    }
                } else {
                    echo $v['t'];
                }
                ?></th><?
            }
        ?>
    </tr>
</thead>
<? foreach($users as $k=>$v) { $id = $v['user_id']; ?>
<tr class="row<?= $k%2 ?><? if( ! $v['activated'] ) { ?> disabled<? } ?>">
    <td><?= $id ?></td>
    <td class="left"><a href="#" onclick="return bff.userinfo(<?= $id ?>);"><?= $v['email'] ?></a></td>
    <td class="left"><?= $v['name'] ?></td>
    <? if($verified): ?><td><? if($v['verified']): $status = $verifiedStatuses[ $v['verified'] ]; ?>
        <a href="<?= $this->adminLink('user_edit&members=1&tuid='.$v['tuid'].'&rec='.$id.'&tab=verified') ?>">
            <i class="<?= $status['i'] ?> disabled" title="<?= $status['title'] ?>"></i>
        </a>
    <? endif; ?></td><? endif; ?>
    <td><?= $v['pro'] ? '<span class="pro">PRO</span>' : '' ?></td>
    <td><?= ( $v['last_login'] != '0000-00-00 00:00:00' ? tpl::date_format3($v['last_login'], 'd.m.Y H:i') : ' - ') ?></td>
    <td>
        <a class="but <? if( ! $v['blocked'] ) { ?>un<? } ?>block" href="#" onclick="return bff.userinfo(<?= $id ?>);" id="u<?= $id ?>"></a>
        <a class="but edit" href="<?= $this->adminLink('user_edit&members=1&tuid='.$v['tuid'].'&rec='.$id) ?>"></a>
    </td>
</tr>
<? } if( empty($users) ) { ?>
<tr class="norecords">
    <td colspan="<?= $verified ? 7 : 6 ?>">нет пользователей</td>
</tr>
<? } ?>
</table>
<? echo $pgn;