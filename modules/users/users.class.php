<?php

# Класс пользователей

class Users_ extends UsersBase
{
    public function authPage($sTemplate, $sTitle = '', array $aData = array())
    {
        tpl::includeJS('users.auth', false, 5);
        $aData['back'] = self::url('my.profile');

        return $this->showShortPage($sTitle, $this->viewPHP($aData, $sTemplate));
    }

    /**
     * Авторизация
     */
    public function login()
    {
        switch ($this->input->getpost('step', TYPE_STR))
        {
            case 'resend-activation': # Повторная отправка письма "активации"
            {
                $aResponse = array();
                do {
                    $aData = $this->input->postgetm(array(
                        'email' => array(TYPE_NOTAGS, 'len' => 100, 'len.sys' => 'users.user.email.limit'), # E-mail
                        'pass'  => TYPE_NOTRIM, # Пароль (в открытом виде)
                    ));
                    extract($aData);

                    if (!$this->security->validateReferer()) {
                        $this->errors->reloadPage(); break;
                    }
                    # не чаще чем раз в {X} секунд с одного IP (для одного пользователя)
                    if (Site::i()->preventSpam('users-login-resend-activation', config::sysAdmin('users.login.prevent.spam', 5, TYPE_UINT), false)) {
                        $this->errors->set(_t('', 'Повторите попытку через несколько секунд'));
                        break;
                    }
                    # уже авторизован
                    if (User::id()) {
                        $this->errors->reloadPage();
                        break;
                    }
                    # проверяем email
                    if (!$this->input->isEmail($email)) {
                        $this->errors->set(_t('users', 'E-mail адрес указан некорректно'), 'email');
                        break;
                    }
                    $userData = $this->model->userDataByFilter(array('email'=>$email),
                        array('user_id', 'email', 'name', 'password', 'password_salt', 'activated', 'activate_key', 'phone_number'));
                    if (empty($userData) || $userData['activated']) {
                        $this->errors->reloadPage();
                        break;
                    }
                    # телефон
                    if (static::registerPhone() && !empty($userData['phone_number'])) {
                        $this->errors->reloadPage();
                        break;
                    }
                    # проверяем пароль
                    if ($userData['password'] != $this->security->getUserPasswordMD5($pass, $userData['password_salt'])) {
                        $this->errors->set(_t('users', 'E-mail или пароль указаны некорректно'), 'email');
                        break;
                    }
                    # генерируем ссылку активации
                    $activationData = $this->updateActivationKey($userData['user_id'], $userData['activate_key']);
                    if ( ! $activationData) {
                        $this->errors->reloadPage();
                        break;
                    }

                    # отправляем письмо для активации аккаунта
                    $mailData = array(
                        'id'            => $userData['user_id'],
                        'name'          => '',
                        'password'      => $pass,
                        'email'         => $email,
                        'activate_link' => $activationData['link']
                    );
                    bff::sendMailTemplate($mailData, 'users_register', $email);

                    # сохраняем данные для повторной отправки письма
                    $this->security->sessionStart();
                    $this->security->setSESSION('users-register-data', $mailData);

                    $aResponse['redirect'] = static::url('register', array('step'=>'emailed', 'resend'=>1));
                } while(false);
                $this->ajaxResponseForm($aResponse);
            }
            break;
            case 'blocked':
            {
                $userID = $this->security->getSESSION('users-social-auth-blocked');
                $aUser['blocked_reason'] = '';
                if ($userID) {
                    $aUser = $this->model->userData($userID, array('blocked_reason'));
                }
                return $this->authPage('auth.message', _t('users', 'Аккаунт заблокирован!'), array(
                    'message' => _t('users', 'Причина блокировки: [reason]', array('reason' => $aUser['blocked_reason'])),
                ));
            }
            break;
        }

        if (Request::isPOST()) {
            $aData = $this->input->postm(array(
                    'email'    => array(TYPE_NOTAGS, 'len' => 100, 'len.sys' => 'users.user.email.limit'), # E-mail
                    'pass'     => array(TYPE_NOTRIM, 'len' => 100, 'len.sys' => 'users.user.password.limit'), # Пароль
                    'social'   => TYPE_BOOL, # Авторизация через соц. сеть
                    'remember' => TYPE_BOOL, # Запомнить меня
                    'back'     => TYPE_NOTAGS, # Ссылка возврата
                )
            );
            extract($aData);

            $aResponse = array('success' => false, 'status' => 0);
            do {
                # уже авторизован
                if (User::id()) {
                    $aResponse['success'] = true;
                    break;
                }

                # проверяем корректность email
                $sField = 'login';
                if ($this->input->isEmail($email)) {
                    $sField = 'email';
                }

                # при авторизации проверяем блокировку по IP
                if ($mBlocked = $this->checkBan(true)) {
                    $this->errors->set(_t('users', 'Доступ заблокирован по причине: [reason]', array('reason' => $mBlocked)));
                    break;
                }

                $mResult = $this->userAuth($email, $sField, $pass, false, true);
                if ($mResult === true) {
                    $nUserID = User::id();

                    # привязываем соц. аккаунт
                    if ($social) {
                        $this->social()->authFinish($nUserID);
                    }

                    $aResponse['status'] = 2; # успешная авторизация
                    $aResponse['success'] = true;
                    if ($remember) {
                        $this->security->setRememberMe($this->security->getUserLogin(), $this->security->getUserPasswordMD5($pass));
                    }
                } elseif ($mResult === 1) {
                    $aResponse['status'] = 1; # необходимо активировать аккаунт
                    $userData = config::get('__users_preactivate_data');
                    $userData = $this->model->userData($userData['id'], array('user_id as id','email','name','activate_key','phone_number'));
                    if (static::registerPhone() && !empty($userData['phone_number'])) {
                        if (empty($userData['activate_key'])) {
                            # Обновляем ключ активации
                            $activationData = $this->updateActivationKey($userData['id']);
                            if (!$activationData) {
                                $this->errors->set(_t('users', 'Ошибка регистрации, обратитесь к администратору'));
                                break;
                            } else {
                                $userData['activate_key'] = $activationData['key'];
                            }
                        }

                        # Отправляем SMS с кодом активации для подтверждения номера телефона
                        $this->sms(false)->sendActivationCode($userData['phone_number'], $userData['activate_key']);
                        $this->security->sessionStart();
                        $this->security->setSESSION('users-register-data', array(
                            'id'            => $userData['id'],
                            'name'          => $userData['name'],
                            'password'      => $pass,
                            'phone'         => $userData['phone_number'],
                            'email'         => $email,
                        ));

                        $this->errors->set(_t('users', 'Данный аккаунт неактивирован. <a [link_activate]>Активировать</a>',
                            array('link_activate'=>'href="'.static::url('register', array('step' => 'phone')).'"')));
                    } else {
                        $this->errors->set(_t('users', 'Данный аккаунт неактивирован, перейдите по ссылке отправленной вам в письме.<br /><a [link_resend]>Получить письмо повторно</a>',
                            array('link_resend'=>'href="#" class="ajax j-resend-activation"')));
                    }
                    break;
                }

            } while (false);

            if ($aResponse['success']) {
                if (empty($back) || strpos($back, '/user/') !== false) {
                    $back = self::url('my.profile');
                }
                $aResponse['redirect'] = $back;
            }

            $this->ajaxResponseForm($aResponse);
        } else {
            if (User::id()) {
                $this->redirectToProfilePage();
            }
        }

        # SEO: Авторизация
        $this->urlCorrection(static::url('login'));
        $this->seo()->canonicalUrl(static::url('login', array(), true));
        $this->setMeta('login');

        return $this->authPage('auth.login', _t('users', 'Войдите на сайт с помощью электронной почты или через социальную сеть'), array(
                'providers' => $this->social()->getProvidersEnabled()
            )
        );
    }

    /**
     * Авторизация через соц. сети
     */
    public function loginSocial()
    {
        $this->social()->auth($this->input->get('provider', TYPE_NOTAGS));
    }

    /**
     * Авторизация на фронтенде из админки
     */
    public function login_admin()
    {
        $nUserID = $this->input->get('user_id', TYPE_UINT);
        do {
            if (!$this->security->validateReferer()) break;
            if (!$nUserID || $this->model->userIsAdministrator($nUserID)) break;
            $aData = $this->model->userData($nUserID, array('email','password'));
            if (empty($aData)) break;
            $sHash = $this->input->get('hash', TYPE_STR);
            if (empty($sHash)) break;
            if ($sHash != $this->frontendAuthHash($nUserID, $aData['email'])) break;
            if ($this->userAuth($nUserID, 'user_id', $aData['password']) !== true) break;

            $this->redirectToProfilePage();

        } while(false);

        $this->errors->error404();
    }

    /**
     * Регистрация
     * @var bool|array $embedded параметры для встраиваемой формы:
     *   - 'force_type' - предопределить тип пользователя
     *   - 'force_role' - предопределить роль пользователя
     *   - 'generate_password' - сгенерировать пользователю пароль
     *   - 'no_captcha' - не использовать каптчу
     *   - 'form' - JQuery form selector
     *   - 'step' - переопределить шаг
     * @return string HTML
     */
    public function register($embedded = false)
    {
        $rolesEnabled = static::rolesEnabled();
        $bPhone = static::registerPhone(); # задействовать: номер телефона
        $generatePassword = ! empty($embedded['generate_password']);

        $step = $this->input->getpost('step', TYPE_STR);
        if ( ! empty($embedded['step']) && $embedded['step'] == 'confirm') {
            $step = $bPhone ? 'phone':'emailed';
        }

        switch ($step) {
            case 'phone': # Подтверждение номера телефона
            {
                if (User::id()) {
                    if (Request::isPOST()) {
                        $this->errors->reloadPage();
                        $this->ajaxResponseForm();
                    }
                    $this->redirectToProfile();
                }
                $registerData = $this->security->getSESSION('users-register-data');
                if (empty($registerData['id']) || empty($registerData['phone'])) {
                    $this->errors->error404();
                }
                $userID = $registerData['id'];
                $userData = $this->model->userData($userID, array('password','activated','activate_key','blocked','blocked_reason'));
                if (empty($userData) || $userData['blocked']) {
                    $this->errors->error404();
                }
                if ($userData['activated']) {
                    $this->userAuth($userID, 'user_id', $userData['password']);
                    $this->redirectToProfile();
                }
                if (Request::isPOST()) {
                    $this->register_phone($userID, static::url('register', array('step' => 'finished')));
                }

                if ( ! empty($embedded)) {
                    $registerData['embedded'] = $embedded;
                    return $this->viewPHP($registerData, 'auth.register.phone');
                }
                return $this->authPage('auth.register.phone', _t('users', 'Подтверждение номера мобильного телефона'), $registerData);
            } break;
            case 'emailed': # Уведомление о письме "активации"
            {
                $aData = $this->security->getSESSION('users-register-data');
                if (!empty($aData['id'])) {
                    $aUser = $this->model->userData($aData['id'], array('activated', 'blocked'));
                    if (empty($aUser) || $aUser['activated'] || $aUser['blocked']) {
                        $aData = false;
                    }
                } else {
                    $userID = $this->security->getSESSION('users-social-auth-notactivated');
                    if ($userID) {
                        $aUser = $this->model->userData($userID, array('email', 'activated', 'activate_key', 'activate_expire'));
                        if ( ! empty($aUser['email']) && ! $aUser['activated']) {
                            if ( ! $aUser['activate_key'] || (strtotime($aUser['activate_expire']) < time())) {
                                $activationData = $this->getActivationInfo();
                                $this->model->userSave($userID, array(
                                    'activate_key'    => $activationData['key'],
                                    'activate_expire' => $activationData['expire'],
                                ));
                                $aData = array(
                                    'id'            => $userID,
                                    'name'          => '',
                                    'password'      => '',
                                    'email'         => $aUser['email'],
                                    'activate_link' => $activationData['link'],
                                );
                                bff::sendMailTemplate($aData, 'users_register', $aData['email']);
                            } else {
                                $aData = array(
                                    'id'            => $userID,
                                    'name'          => '',
                                    'password'      => '',
                                    'email'         => $aUser['email'],
                                    'activate_link' => static::url('activate', array('key' => $aUser['activate_key'])),
                                );
                            }
                        }
                        $this->security->setSESSION('users-register-data', $aData);
                    }
                }
                if (Request::isPOST()) {
                    if (!$this->security->validateReferer()) {
                        $this->errors->reloadPage();
                    } else {
                        if (!User::id() && !empty($aData)) {
                            # Повторная отправка письма об успешной регистрации
                            bff::sendMailTemplate($aData, 'users_register', $aData['email']);
                            $this->security->setSESSION('users-register-data', null);
                        }
                    }
                    $this->ajaxResponseForm();
                }

                $bResend = $this->input->get('resend', TYPE_BOOL);
                $sTitle = ( $bResend ? _t('users', 'Письмо отправлено') : _t('users', 'Регистрация завершена') );

                $aData = array('retry_allowed' => !empty($aData));
                if ( ! empty($embedded)) {
                    $aData['embedded'] = $embedded;
                    return $this->viewPHP($aData, 'auth.register.emailed');
                }

                return $this->authPage('auth.register.emailed', $sTitle, $aData);
            }
            break;
            case 'social': # Регистрация через аккаунт в соц. сети
            {
                if (User::id()) {
                    if (Request::isPOST()) {
                        $this->errors->reloadPage();
                        $this->ajaxResponseForm();
                    }
                    $this->redirectToProfile();
                }

                $aSocialData = $this->social()->authData();

                if (Request::isPOST()) {
                    $aResponse = array('exists' => false);
                    $p = $this->input->postm(array(
                            'email'     => array(TYPE_NOTAGS, 'len' => 100, 'len.sys' => 'users.user.email.limit'), # E-mail
                            'agreement' => TYPE_BOOL, # Пользовательское соглашение
                            'type'      => TYPE_UINT, # Тип пользователя
                            'phone'     => TYPE_NOTAGS, # Телефон
                            'code'      => TYPE_STR,    # Код подтверждения
                        )
                    );
                    extract($p);
                    do {
                        if (!$this->security->validateReferer() || empty($aSocialData)) {
                            $this->errors->reloadPage();
                            break;
                        }
                        if (!array_key_exists($type, static::aTypes())) {
                            $this->errors->set(_t('users', 'Тип пользователя указан некорректно'), 'type');
                            break;
                        }
                        if (!$this->input->isEmail($email)) {
                            $this->errors->set(_t('users', 'E-mail адрес указан некорректно'), 'email');
                            break;
                        }
                        if ($bPhone) {
                            if ( ! $this->input->isPhoneNumber($phone)) {
                                $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                                break;
                            }
                            if ($this->model->userPhoneExists($phone)) {
                                $this->errors->set(_t('users', 'Пользователь с таким номером телефона уже зарегистрирован.',
                                    array('link_forgot' => 'href="' . static::url('forgot') . '"')
                                ), 'phone'
                                );
                                break;
                            }
                            $saveCode = $this->security->getSESSION('user-phone-confirm');
                            if (empty($code) || empty($saveCode)) {
                                $activationData = $this->getActivationInfo();
                                $res = $this->sms()->sendActivationCode($phone, $activationData['key']);
                                if ($res) {
                                    $this->security->setSESSION('user-phone-confirm', $activationData['key']);
                                    $aResponse['msg'] = _t('users', 'Код подтверждения был отправлен на указанный вами номер');
                                    $aResponse['confirm'] = 1;
                                } else {
                                    $this->errors->reloadPage();
                                }
                                break;
                            }
                            if (mb_strtolower($code) != mb_strtolower($saveCode)) {
                                $this->errors->set(_t('users', 'Код подтверждения указан некорректно'), 'code');
                                $aResponse['confirm'] = 1;
                                break;
                            }
                        }


                        # антиспам фильтр: временные ящики
                        if (Site::i()->spamEmailTemporary($email)) {
                            $this->errors->set(_t('', 'Указанный вами email адрес находится в списке запрещенных, используйте например @gmail.com'));
                            break;
                        }
                        if ($mBanned = $this->checkBan(true, $email)) {
                            $this->errors->set(_t('users', 'Доступ заблокирован по причине: [reason]', array('reason' => $mBanned)));
                            break;
                        }
                        if ($this->model->userEmailExists($email)) {
                            $aResponse['exists'] = true;
                            break;
                        }
                        if (!$agreement) {
                            $this->errors->set(_t('users', 'Пожалуйста подтвердите, что Вы согласны с пользовательским соглашением'), 'agreement');
                        }
                        if (!$this->errors->no()) {
                            break;
                        }


                        # Создаем аккаунт пользователя
                        $aUserData = $this->userRegister(array(
                            'name'  => $aSocialData['name'], # ФИО из соц. сети
                            'email' => $email, # E-mail
                            'type'  => $type, # Тип пользователя
                        ));
                        if (empty($aUserData)) {
                            $this->errors->set(_t('users', 'Ошибка регистрации, обратитесь к администратору'));
                            break;
                        }
                        $nUserID = $aUserData['user_id'];

                        # Загружаем аватар из соц. сети
                        if (!empty($aSocialData['avatar'])) {
                            $this->avatar($nUserID)->uploadSocial($aSocialData['provider_id'], $aSocialData['avatar'], true);
                        }

                        # Закрепляем соц. аккаунт за пользователем
                        $this->social()->authFinish($nUserID);

                        # Активируем аккаунт пользователя без подтверждения email адреса
                        if (!config::sysAdmin('users.register.social.email.activation', true, TYPE_BOOL)) {
                            $update = array(
                                'activated' => 1
                            );
                            if ($bPhone) {
                                $update['phone_number'] = $phone;
                                $update['phone_number_verified'] = 1;
                            }

                            $res = $this->model->userSave($nUserID, $update);
                            if (!$res) {
                                $this->errors->reloadPage(); break;
                            }

                            $res = $this->userAuth($nUserID, 'user_id', $aUserData['password'], false);
                            if ($res!==true) {
                                $this->errors->reloadPage(); break;
                            }

                            # Отправляем письмо об успешной регистрации
                            $aMailData = array(
                                'name'     => $aSocialData['name'],
                                'password' => $aUserData['password'],
                                'email'    => $email,
                            );
                            bff::sendMailTemplate($aMailData, 'users_register_auto', $email);

                            $aResponse['success'] = true;
                            $aResponse['redirect'] = static::url('register', array('step' => 'finished'));
                            break;
                        }


                        # Отправляем письмо для активации аккаунта
                        $aMailData = array('id'            => $nUserID,
                                           'name'          => $aSocialData['name'],
                                           'password'      => $aUserData['password'],
                                           'email'         => $email,
                                           'activate_link' => $aUserData['activate_link']
                        );
                        bff::sendMailTemplate($aMailData, 'users_register', $email);

                        # Сохраняем данные для повторной отправки письма
                        $this->security->sessionStart();
                        $this->security->setSESSION('users-register-data', $aMailData);

                        $aResponse['success'] = true;
                        $aResponse['redirect'] = static::url('register', array('step' => 'emailed')); # url результирующей страницы

                    } while (false);

                    $this->ajaxResponseForm($aResponse);
                }

                # Данные о процессе регистрации через соц.сеть некорректны, причины:
                # 1) неудалось сохранить в сессии
                # 2) повторная попытка, вслед за успешной (случайный переход по ссылке)
                if (empty($aSocialData)) {
                    $this->redirect(static::url('register'));
                }

                # Аватар по-умолчанию
                if (empty($aSocialData['avatar'])) {
                    $aSocialData['avatar'] = UsersAvatar::url(0, '', UsersAvatar::szNormal);
                }

                return $this->authPage('auth.register.social', _t('users', 'Для завершения регистрации введите Вашу электронную почту'), $aSocialData);
            }
            break;
            case 'finished':
            {
                return $this->authPage('auth.message', _t('users', 'Вы успешно зарегистрировались!'), array(
                        'message' => _t('users', 'Теперь вы можете <a [link_home]><span>перейти на главную страницу</span></a> или <a [link_profile]><span>в настройки своего профиля</span></a>.',
                            array('link_home'    => 'href="' . bff::urlBase() . '"',
                                  'link_profile' => 'href="' . self::url('my.settings') . '"'
                            )
                        )
                    )
                );
            }
            break;
        }

        $bCaptcha = config::sysAdmin('users.register.captcha', true, TYPE_BOOL); # задействовать: капчу
        $bPasswordConfirm = config::sysAdmin('users.register.passconfirm', true, TYPE_BOOL); # задействовать: подтверждение пароля
        $bAgreement = config::sysAdmin('users.register.agreement', true, TYPE_BOOL); # задействовать: подтверждение соглашения

        if (Request::isPOST()) {
            $aResponse = array('captcha' => false);

            if (User::id()) {
                $this->ajaxResponseForm($aResponse);
            }

            $params = array(
                'email'     => array(TYPE_NOTAGS, 'len' => 100, 'len.sys' => 'users.user.email.limit'), # E-mail
                'pass'      => TYPE_NOTRIM, # Пароль
                'pass2'     => TYPE_NOTRIM, # Подтверждение пароля
                'back'      => TYPE_NOTAGS, # Ссылка возврата
                'captcha'   => TYPE_NOTAGS, # Капча
                'agreement' => TYPE_BOOL, # Пользовательское соглашение
                'type'      => TYPE_UINT, # Тип пользователя
                'phone'     => TYPE_NOTAGS, # Телефон
            );
            $name = '';

            if ($rolesEnabled) {
                $params['role_id'] = TYPE_UINT; # роль
                $params['name']    = TYPE_NOTAGS; # имя / название
            }
            $aData = $this->input->postm($params);
            extract($aData);
            $aResponse['back'] = $back;

            do {
                if (!$this->security->validateReferer()) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($bPhone && ! $this->input->isPhoneNumber($phone)) {
                    $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                    break;
                }

                if (isset($embedded['force_type'])) {
                    $type = $embedded['force_type'];
                }
                if (!array_key_exists($type, static::aTypes())) {
                    $this->errors->set(_t('users', 'Тип пользователя указан некорректно'), 'type');
                    break;
                }

                if ($rolesEnabled) {
                    if (isset($embedded['force_role'])) {
                        $role_id = $embedded['force_role'];
                    }
                    if (!array_key_exists($role_id, static::roles())) {
                        $this->errors->set(_t('users', 'Роль указана некорректно'), 'role_id');
                        break;
                    }
                    if (empty($name)) {
                        if ($role_id == Users::ROLE_COMPANY) {
                            $this->errors->set(_t('users', 'Укажите название компании'), 'role_id');
                        } else {
                            $this->errors->set(_t('users', 'Укажите имя'), 'role_id');
                        }
                        break;
                    }
                }

                if ($bPhone && $this->model->userPhoneExists($phone)) {
                    $this->errors->set(_t('users', 'Пользователь с таким номером телефона уже зарегистрирован. <a [link_forgot]>Забыли пароль?</a>',
                        array('link_forgot' => 'href="' . static::url('forgot') . '"')
                    ), 'phone'
                    );
                    break;
                }

                if (!$this->input->isEmail($email)) {
                    $this->errors->set(_t('users', 'E-mail адрес указан некорректно'), 'email');
                    break;
                }
                # антиспам фильтр: временные ящики
                if (Site::i()->spamEmailTemporary($email)) {
                    $this->errors->set(_t('', 'Указанный вами email адрес находится в списке запрещенных, используйте например @gmail.com'));
                    break;
                }

                if ($mBanned = $this->checkBan(true, $email)) {
                    $this->errors->set(_t('users', 'Доступ заблокирован по причине: [reason]', array('reason' => $mBanned)));
                    break;
                }

                if ($this->model->userEmailExists($email)) {
                    $this->errors->set(_t('users', 'Пользователь с таким e-mail адресом уже зарегистрирован. <a [link_forgot]>Забыли пароль?</a>',
                            array('link_forgot' => 'href="' . static::url('forgot') . '"')
                        ), 'email'
                    );
                    break;
                }

                if ( ! $generatePassword) {
                    if (empty($pass)) {
                        $this->errors->set(_t('users', 'Укажите пароль'), 'pass');
                    } elseif (mb_strlen($pass) < $this->passwordMinLength) {
                        $this->errors->set(_t('users', 'Пароль не должен быть короче [min] символов', array('min' => $this->passwordMinLength)), 'pass');
                    } else {
                        if($bPasswordConfirm && $pass != $pass2) {
                            $this->errors->set(_t('users', 'Подтверждение пароля указано неверно'), 'pass2');
                        }
                    }
                }

                if ($bCaptcha && empty($embedded['no_captcha'])) {
                    if (empty($captcha) || !CCaptchaProtection::isCorrect($captcha)) {
                        $this->errors->set(_t('users', 'Результат с картинки указан некорректно'), 'captcha');
                        $aResponse['captcha'] = true;
                        CCaptchaProtection::reset();
                    }
                }

                if ($bAgreement && ! $agreement) {
                    $this->errors->set(_t('users', 'Пожалуйста подтвердите, что Вы согласны с пользовательским соглашением'), 'agreement');
                }

                $aUserCreate = array(
                    'email'    => $email,
                    'type'     => $type,
                );
                if ( ! $generatePassword) {
                    $aUserCreate['password'] = $pass;
                }
                if ($rolesEnabled) {
                    $aUserCreate['role_id'] = $role_id;
                    $aUserCreate['name'] = $name;
                }
                if ($bPhone) {
                    $aUserCreate['phone_number'] = $phone;
                }

                # Обязательный номер телефона
                if (static::profilePhoneRequired()) {
                    $aContacts = array(
                        'contacts' => array(
                            array(
                               't' => static::CONTACT_TYPE_PHONE,
                               'v' => $phone,
                            )
                        ));
                    $this->cleanUserData($aContacts);
                    $aPhones = func::unserialize($aContacts['contacts']);
                    if (empty($aPhones)) {
                        $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                        break;
                    }
                    $aUserCreate['contacts'] = $aPhones;
                }

                if (!$this->errors->no('users.register.submit',array('data'=>&$aUserCreate))) {
                    break;
                }

                # Создаем аккаунт пользователя
                $aUserData = $this->userRegister($aUserCreate);
                if (empty($aUserData)) {
                    $this->errors->set(_t('users', 'Ошибка регистрации, обратитесь к администратору'));
                    break;
                }
                $aResponse['success'] = true;
                $aResponse['redirect'] = static::url('register', array('step' => ($bPhone?'phone':'emailed'))); # url результирующей страницы

                # Отправляем письмо для активации аккаунта
                $aMailData = array('id'            => $aUserData['user_id'],
                                   'name'          => $name,
                                   'password'      => $generatePassword ? $aUserData['password'] : $pass,
                                   'phone'         => $phone,
                                   'email'         => $email,
                                   'activate_link' => $aUserData['activate_link']
                );
                if ($bPhone) {
                    # Отправляем SMS с кодом активации для подтверждения номера телефона
                    # Письмо отправим после успешного подтверждения
                    $this->sms(false)->sendActivationCode($phone, $aUserData['activate_key']);
                } else {
                    bff::sendMailTemplate($aMailData, 'users_register', $email);
                }

                # Сохраняем данные для повторной отправки письма
                $this->security->sessionStart();
                $this->security->setSESSION('users-register-data', $aMailData);

            } while (false);

            if ( ! empty($embedded)) {
                if ( ! empty($aResponse['success'])) {
                    return $aUserData;
                } else {
                    return false;
                }
            }

            $this->ajaxResponseForm($aResponse);
        } else {
            if (User::id()) {
                $this->redirectToProfile();
            }
        }

        if ( ! $embedded) {
            # SEO: Регистрация
            $this->urlCorrection(static::url('register'));
            $this->seo()->canonicalUrl(static::url('register', array(), true));
            $this->setMeta('register');
        }

        $data = array(
            'captcha_on'      => $bCaptcha,
            'agreement_on'    => $bAgreement,
            'pass_confirm_on' => $bPasswordConfirm,
            'phone_on'        => $bPhone,
            'providers'       => $this->social()->getProvidersEnabled()
        );
        if ( ! empty($embedded)) {
            $data['embedded'] = $embedded;
            return $this->viewPHP($data, 'auth.register');
        }

        return $this->authPage('auth.register', _t('users', 'Зарегистрируйтесь на сайте с помощью электронной почты или через социальную сеть'), $data);
    }

    /**
     * Подтверждение номера телефона
     * @param integer $userID ID пользователя
     * @param string $redirect url для редиректа в случае успеха
     */
    public function register_phone($userID, $redirect)
    {
        if ( ! Request::isPOST()) {
            $this->redirectToProfile();
        }
        $act = $this->input->postget('act');
        $response = array();
        if (!$this->security->validateReferer() || !static::registerPhone()) {
            $this->errors->reloadPage(); $act = '';
        }
        $userData = $this->model->userData($userID, array('password','activated','activate_key','blocked','blocked_reason', 'phone_number'));
        $registerData = $this->security->getSESSION('users-register-data');
        $userPhone = $userData['phone_number'];

        switch($act)
        {
            # Проверка кода подтверждения
            case 'code-validate':
            {
                $code = $this->input->postget('code', TYPE_NOTAGS);
                if (mb_strtolower($code) !== $userData['activate_key']) {
                    $this->errors->set(_t('users', 'Код подтверждения указан некорректно'), 'phone');
                    break;
                }
                # Активируем аккаунт
                $res = $this->model->userSave($userID, array('phone_number_verified'=>1, 'activated' => 1, 'activate_key' => ''));
                if ($res) {
                    bff::i()->callModules('onUserActivated', array($userID));
                    if( ! User::id()) {
                        # Авторизуем
                        $this->userAuth($userID, 'user_id', $userData['password']);
                        # Отправляем письмо об успешной регистрации
                        bff::sendMailTemplate($registerData, 'users_register_phone', $registerData['email']);
                        InternalMail::i()->sendRegInfo($userID);
                        $this->security->setSESSION('users-register-data', null);
                    }
                    $response['redirect'] = $redirect;
                } else {
                    bff::log('users: Ошибка активации аккаунта пользователя по коду подтверждения [user-id="'.$userID.'"]');
                    $this->errors->set(_t('users', 'Ошибка регистрации, обратитесь к администратору'));
                    break;
                }
            } break;
            # Повторная отправка кода подтверждения
            case 'code-resend':
            {
                $activationData = $this->getActivationInfo();

                $res = $this->sms()->sendActivationCode($userPhone, $activationData['key']);
                if ($res) {
                    $activationData = $this->updateActivationKey($userID, $activationData['key']);
                    if (!$activationData) {
                        $this->errors->reloadPage();
                        break;
                    }
                }
            } break;
            # Смена номера телефона
            case 'phone-change':
            {
                $phone = $this->input->postget('phone', TYPE_NOTAGS, array('len'=>30, 'len.sys' => 'users.phone.limit'));
                if (!$this->input->isPhoneNumber($phone)) {
                    $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                    break;
                }
                if ($phone === $userPhone) {
                    break;
                }
                if ($this->model->userPhoneExists($phone, $userID)) {
                    $this->errors->set(_t('users', 'Пользователь с таким номером телефона уже зарегистрирован. <a [link_forgot]>Забыли пароль?</a>',
                        array('link_forgot' => 'href="' . static::url('forgot') . '"')
                    ), 'phone'
                    );
                    break;
                }
                $res = $this->model->userSave($userID, array(
                    'phone_number' => $phone,
                    'phone_number_verified' => 0,
                ));
                if (!$res) {
                    bff::log('users: Ошибка обновления номера телефона [user-id="'.$userID.'"]');
                    $this->errors->reloadPage();
                } else {
                    $registerData['phone'] = $phone;
                    $response['phone'] = '+'.$phone;
                    $this->sms()->sendActivationCode($phone, $userData['activate_key']);
                    $this->security->setSESSION('users-register-data', $registerData);
                }
            } break;
        }

        $this->ajaxResponseForm($response);
    }

    /**
     * Активация пользователя
     */
    public function activate()
    {
        if (User::id()) {
            $this->redirectToProfile();
        }
        $sKey = $this->input->get('key', TYPE_STR); # ключ активации

        $aData = $this->model->userDataByFilter(array(
                'activated'    => 0,
                'blocked'      => 0,
                array('activate_expire > :expire', ':expire' => $this->db->now()),
                'activate_key' => $sKey,
            ), array('user_id', 'email', 'password', 'name', 'activated')
        );
        if (empty($aData)) {
            # Не нашли пользователя по ключу:
            # 1) Срок ключа истек / ключ некорректный
            # 2) Пользователь активирован / заблокирован
            bff::setMeta(_t('users', 'Активация аккаунта'));
            return $this->authPage('auth.message', _t('users', 'Активация аккаунта'), array(
                    'message' => _t('users', 'Срок действия ключа активации истек.')
                )
            );
        }

        $nUserID = $aData['user_id'];

        # Активируем
        $bSuccess = $this->model->userSave($nUserID, array('activated' => 1, 'activate_key' => ''));
        if (!empty($bSuccess)) {
            # Триггер активации аккаунта
            bff::i()->callModules('onUserActivated', array($nUserID));
        }

        # Авторизуем
        $bAuthorized = $this->userAuth($nUserID, 'user_id', $aData['password']);
        if ($bAuthorized === true) {
            # ...
        }

        $this->redirect(static::url('register', array('step' => 'finished')));
    }

    /**
     * Восстановление пароля пользователя
     */
    public function forgot()
    {
        # Уже авторизован
        if (User::id()) {
            if (Request::isAJAX()) {
                $this->errors->impossible();
                $this->ajaxResponseForm();
            }
            $this->redirectToProfilePage();
        }

        $sKey = $this->input->getpost('key', TYPE_STR, array('len' => 100, 'len.sys' => 'users.key.limit'));
        $bSocial = $this->input->getpost('social', TYPE_BOOL);
        if (!empty($sKey)) {
            # Шаг2: Смена пароля
            if (Request::isAJAX()) {
                do {
                    if (!$this->security->validateReferer()) {
                        $this->errors->reloadPage();
                        break;
                    }
                    # Ищем по "ключу восстановления"
                    $aData = $this->model->userDataByFilter(array(
                            'blocked'      => 0, # незаблокированные аккаунты
                            'activate_key' => $sKey,
                            array('activate_expire > :expire', ':expire' => $this->db->now()),
                        ), array('user_id', 'email', 'password', 'password_salt', 'activated')
                    );

                    # Не нашли, возможные причины:
                    # 1) Истек срок действия ссылки восстановления / неверная ссылка восстановления
                    # 2) Аккаунт заблокирован
                    # 3) Запрещаем смену пароля администраторам
                    if (empty($aData) || $this->model->userIsAdministrator($aData['user_id'])) {
                        $this->errors->set(_t('users', 'Срок действия ссылки восстановления пароля истек или ссылка некорректна, <a href="[link_fogot]">повторите попытку</a>.',
                                array('link_fogot' => static::url('forgot'))
                            )
                        );
                        break;
                    }

                    # Проверяем новый пароль
                    $password = $this->input->post('pass', TYPE_NOTRIM, array('len' => 100, 'len.sys' => 'users.pass.limit'));
                    if (mb_strlen($password) < $this->passwordMinLength) {
                        $this->errors->set(_t('users', 'Пароль не должен быть короче [min] символов', array('min' => $this->passwordMinLength)), 'pass');
                        break;
                    }

                    $nUserID = $aData['user_id'];

                    # Cохраняем новый пароль, сбрасываем ключ
                    $this->model->userSave($nUserID, array(
                            'password'     => $this->security->getUserPasswordMD5($password, $aData['password_salt']),
                            'activated'    => 1, # активируем, если аккаунт еще НЕ активирован
                            'activate_key' => ''
                        )
                    );

                    # Закрепляем соц. аккаунт за профилем
                    if ($bSocial) {
                        $this->social()->authFinish($nUserID);
                    }

                    if ($this->errors->no()) {
                        $mResult = $this->userAuth($aData['email'], 'email', $password, false, false);
                        if ($mResult === true) {
                            $this->ajaxResponseForm(array('redirect' => static::url('my.profile')));
                        }
                    }

                } while (false);

                $this->ajaxResponseForm();
            }

            # SEO: Забыли пароль
            $this->urlCorrection(static::url('forgot'));
            $this->seo()->canonicalUrl(static::url('forgot', array(), true));
            $this->setMeta('forgot');

            return $this->authPage('auth.forgot.finish', _t('users', 'Введите новый пароль'), array(
                    'key'    => $sKey,
                    'social' => $bSocial,
                )
            );
        } else {
            # Шаг1: Инициация восстановления пароля по E-mail адресу / Логину
            if (Request::isAJAX()) {
                $email = $this->input->post('email', TYPE_NOTAGS);
                do {
                    if (!$this->security->validateReferer()) {
                        $this->errors->reloadPage();
                        break;
                    }
                    # Проверяем E-mail или логин
                    $this->isLoginOrEmail($email, $isEmail);

                    # Получаем данные пользователя
                    # - восстановление пароля для неактивированных аккаунтов допустимо
                    $aData = $this->model->userDataByFilter(array(($isEmail ? 'email' : 'login') => $email, 'blocked' => 0),
                        array('user_id', 'email', 'name', 'activated', 'activate_expire')
                    );
                    if (empty($aData) || $this->model->userIsAdministrator($aData['user_id'])) {
                        $this->errors->set(_t('users', 'Указанный e-mail или логин в базе не найден'), 'email');
                        break;
                    }

                    /**
                     * Генерируем "ключ восстановления", помечаем период его действия.
                     * В случае если аккаунт неактивирован, период действия ключа восстановления пароля будет равен
                     * периоду действия ссылки активации аккаунта, поскольку задействуется
                     * одно и тоже поле "activate_expire"
                     */
                    $sKey = func::generator(20);
                    $bSaved = $this->model->userSave($aData['user_id'], array(
                            'activate_key'    => $sKey,
                            'activate_expire' => (!$aData['activated'] ? $aData['activate_expire'] :
                                    date('Y-m-d H:i:s', strtotime('+4 hours'))),
                        )
                    );

                    if (!$bSaved) {
                        $this->errors->reloadPage();
                    } else {
                        # Отправляем письмо с инcтрукцией о смене пароля
                        bff::sendMailTemplate(array(
                                'link'  => static::url('forgot', array('key' => $sKey, 'social' => $bSocial)),
                                'email' => $aData['email'],
                                'name'  => $aData['name']
                            ), 'users_forgot_start', $aData['email']
                        );
                    }
                } while (false);

                $this->ajaxResponseForm();
            }

            # SEO: Забыли пароль
            $this->urlCorrection(static::url('forgot'));
            $this->seo()->canonicalUrl(static::url('forgot', array(), true));
            $this->setMeta('forgot');

            return $this->authPage('auth.forgot.start', _t('users', 'Введите электронную почту, которую вы указывали при регистрации'), array(
                    'social' => $bSocial,
                )
            );
        }
    }

    /**
     * Выход
     */
    public function logout()
    {
        $sRedirect = bff::urlBase();

        if (User::id()) {
            $sReferer = Request::referer();

            # оставляем пользователя на текущей странице,
            # за исключением следующих:
            $aWrongReferers = array(
                '/user/',
                '/orders/',
                '/workflows/',
            );

            if (!empty($sReferer)) {
                foreach ($aWrongReferers as $v) {
                    if (strpos($sReferer, $v) !== false) {
                        $sReferer = false;
                        break;
                    }
                }
                if (!empty($sReferer)) {
                    $sRedirect = $sReferer;
                }
            }

            if ($this->security->validateReferer()) {
                $this->security->sessionDestroy(-1, true);

            }
        }

        $this->redirect($sRedirect);
    }

    /**
     * Профиль пользователя
     */
    public function profile()
    {
        $login = $this->input->get('login', TYPE_STR);
        $login = trim($login, '/ ');
        if (empty($login)) {
            $this->errors->error404();
        }

        $userData = $this->model->userDataByFilter(array('login' => $login), array(
            'user_id as id', 'name', 'surname', 'login', 'type', 'activated', 'blocked', 'pro', 'verified',
            'reg1_country', 'reg3_city',
        ));
        if (empty($userData)) {
            $this->errors->error404();
        } else {
            if(!User::id() && $userData['type'] == Users::TYPE_CLIENT){
                $this->errors->error404();
            }
            $userData['city'] = $userData['region'] = $userData['country'] = '';
            if (static::profileMap()) {
                if ($userData['reg3_city']) {
                    $userData['city'] = Geo::regionTitle($userData['reg3_city']);
                    $userData['region'] = Geo::regionData($userData['reg3_city']);
                }
                if ($userData['reg1_country'] && Geo::countrySelect()) {
                    $userData['country'] = Geo::regionData($userData['reg1_country']);
                }
            }
        }

        if ($userData['type'] == Users::TYPE_WORKER) {
            bff::setActiveMenu('//users');
        }

        if ($userData['my'] = User::isCurrent($userData['id'])) {
            return $this->my($userData);
        }

        if ($userData['blocked']) {
            return $this->showForbidden(_t('users', 'Аккаунт заблокирован'), _t('users', 'Аккаунт был заблокирован'));
        }

        $tab = $this->input->get('tab', TYPE_NOTAGS);
        $tabs = array(
            'common' => array(
                't'   => _t('users', 'Общая информация'),
                'm'   => 'users',
                'url' => Users::url('user.common',  array('login'=>$login)),
            ),
            'orders'   => array(
                't'   => _t('orders', 'Заказы'),
                'm'   => 'Orders',
                'url' => Orders::url('user.listing', array('login'=>$login)),
            ),
            'info'     => array(
                't'   => _t('users', 'Избранные'),
                'm'   => 'Users',
                'url' => Users::url('user.info', array('login'=>$login)),
            ),
            'opinions'   => array(
                't'   => _t('opinions', 'Отзывы'),
                'm'   => 'Opinions',
                'url' => Opinions::url('user.listing', array('login'=>$login)),
            ),

        );

        if(!bff::moduleExists('shop') || ! Shop::enabled()){
            unset($tabs['shop']);
        }
        if(!bff::moduleExists('qa') || ! Qa::enabled()){
            unset($tabs['qa']);
        }
        if(!bff::moduleExists('blog') || ! Blog::enabled()){
            unset($tabs['blog']);
        }
        if( ! Specializations::useServices(Specializations::SERVICES_PRICE_USERS)){
            unset($tabs['price']);
        }

        if ($userData['type'] == static::TYPE_WORKER){
            unset($tabs['orders']);
        } else if ($userData['type'] == static::TYPE_CLIENT){
            unset($tabs['shop']);
            unset($tabs['portfolio']);
            unset($tabs['price']);
            unset($tabs['common']);
        }

        $aTabs = $this->getProfileTabs($userData['id'], $userData['type']);
        foreach ($aTabs as $k => $v) {
            if( ! $v['a']){
                unset($tabs[$k]);
            }
        }
        # Расширяем
        $tabs = bff::filter('users.profile.tabs', $tabs, array('tab'=>&$tab, 'login'=>$login, 'userData'=>&$userData));
        # Сортируем
        func::sortByPriority($tabs, 'priority');


        if (!isset($tabs[$tab])) {
            if (Request::isAJAX()) {
                $this->errors->impossible();
                $this->ajaxResponseForm();
            } else {
                if (!empty($tab)) {
                    $this->errors->error404();
                }
                $tab = array_keys($tabs);
                $tab = reset($tab);
            }
        }
        $tabs[$tab]['active'] = true;
        $aData = array(
            'tabs'    => &$tabs,
            'tab'     => $tab,
            'profile' => $this->viewProfile($userData['id']),
        );

        $callable = ! empty($tabs[$tab]['m']) ? array(bff::module($tabs[$tab]['m']), 'user_' . $tab) : 'user_info';
        if (isset($tabs[$tab]['ev'])) {
            if (is_array($tabs[$tab]['ev'])) {
                $callable = $tabs[$tab]['ev'];
            }
        }
        $aData['content'] = call_user_func($callable, $userData);


        # SEO: Профиль пользователя
        if (false) {
            $this->urlCorrection(static::url('profile', array('login'=>$userData['login'], 'tab'=>$tab)));
            $this->seo()->canonicalUrl(static::url('profile', array('login'=>$userData['login'], 'tab'=>$tab), true));
            $this->setMeta('profile', array(
                'name'    => $userData['name'],
                'surname' => $userData['surname'],
                'login'   => $userData['login'],
                'city'    => $userData['city'],
                'region'  => $userData['region'],
                'country' => $userData['country'],
                'tab'     => $tabs[$tab]['t'],
            ));
        }

        return $this->viewPHP($aData, 'user.layout');
    }

    /**
     * Кабинет пользователя (layout)
     * @param array $userData данные пользователя
     */
    protected function my(array $userData)
    {
        $tab = $this->input->get('tab', TYPE_NOTAGS);
        $tabs = array(
            'common' => array(
                't'   => _t('users', 'Общая информация'),
                'm'   => 'users',
                'url' => Users::url('my.common'),
            ),
            'orders' => array(
                't'   => _t('orders', 'Мои заказы'),
                'm'   => 'Orders',
                'url' => Orders::url('my.orders'),
            ),
            'offers' => array(
                't'   => _t('orders', 'Заявки'),
                'm'   => 'Orders',
                'url' => Orders::url('my.offers'),
            ),
            'info' => array(
                't'   => _t('users', 'Избранные'),
                'm'   => 'users',
                'url' => Users::url('my.info'),
            ),
            'opinions' => array(
                't'   => _t('opinions', 'Отзывы'),
                'm'   => 'Opinions',
                'url' => Opinions::url('my.opinions'),
            ),
        );
        if (!bff::moduleExists('shop') || ! Shop::enabled()) {
            unset($tabs['shop']);
        }
        if (!bff::moduleExists('qa') || ! Qa::enabled()) {
            unset($tabs['qa']);
        }
        if (!bff::moduleExists('blog') || ! Blog::enabled()) {
            unset($tabs['blog']);
        }
        if ( ! Specializations::useServices(Specializations::SERVICES_PRICE_USERS)) {
            unset($tabs['price']);
        }
        if ( ! bff::fairplayEnabled()) {
            unset($tabs['workflows']);
        } else {
            $force = false;
            if ($tab == 'workflows' && ! User::data('workflow')) {
                $this->security->expire();
                $force = true;
            }
            if ( ! User::data('workflow', $force)) {
                unset($tabs['workflows']);
            } else {
                $tabs['workflows']['url'] = Fairplay::url('my.workflow');
            }
        }

        if (static::useClient()) {
            if (User::isClient()) {
                unset($tabs['portfolio'], $tabs['shop'], $tabs['price'], $tabs['common']);
            }
            unset($tabs['offers']);
        }

        $aTabs = $this->getProfileTabs(User::id(), User::type());
        foreach ($aTabs as $k=>$v) {
            if ( ! $v['a']) {
                unset($tabs[$k]);
            }
        }
        # Расширяем
        $tabs = bff::filter('users.my.tabs', $tabs, array('tab'=>&$tab));
        # Сортируем
        func::sortByPriority($tabs, 'priority');

        if (!isset($tabs[$tab])) {
            if (!empty($tab)) {
                $this->errors->error404();
            }
            $tab = array_keys($tabs);
            $tab = reset($tab);
        }

        if (!User::id()) {
            if (Request::isAJAX()) {
                $this->errors->reloadPage();
                $this->ajaxResponseForm();
            } else {
                return $this->showForbidden('', _t('users', 'Для доступа в кабинет необходимо авторизоваться'), true);
            }
        }

        $tabs[$tab]['active'] = true;
        $aData = array(
            'tabs'    => &$tabs,
            'tab'     => $tab,
            'user'    => User::data(array('name')),
            'profile' => $this->viewProfile(User::id()),
        );

        $callable = ! empty($tabs[$tab]['m']) ? array(bff::module($tabs[$tab]['m']), 'my_' . $tab) : 'my_info';
        if (isset($tabs[$tab]['ev'])) {
            if (is_array($tabs[$tab]['ev'])) {
                $callable = $tabs[$tab]['ev'];
            }
        }
        $aData['content'] = call_user_func($callable, $userData);

        return $this->viewPHP($aData, 'my.layout');
    }

    /**
     * Левая часть с информацией о пользователе
     */
    public function viewProfile($nUserID, $OnlyData = false)
    {
        $this->security->setTokenPrefix('');
        $aData = $this->model->userData($nUserID, array(
                'user_id as id', 'type', 'login', 'avatar', 'name', 'surname',
                'pro', 'last_activity', 'reg1_country', 'reg3_city', 'contacts', 'verified',
                'sex', 'status', 'status_text', 'created', 'opinions_cache',
                'position', 'rating', 'specs', 'fav_cnt', 'birthdate', 'experience',
                'views_today', 'views_total', 'role_id', 'phone_number', 'phone_number_verified'
        ));
        $aData['avatar'] = UsersAvatar::url($nUserID, $aData['avatar'], UsersAvatar::szBig, $aData['sex']);
        $aData['avatar_maxsize'] = $this->avatar($nUserID)->getMaxSize();
        $aData['online'] = Users::isOnline($aData['last_activity']);
        $aData['last'] = $aData['online'] ? _t('users', 'онлайн') : _t('users', 'был') . ' ' . tpl::date_format_spent($aData['last_activity'], true, true);
        $aData['bCountry'] = Geo::countrySelect();
        $aData['city_data'] = Geo::regionData($aData['reg3_city']);
        if ($aData['bCountry']) {
            $aData['country_data'] = Geo::regionData($aData['reg1_country']);
        }
        $aData['tags'] = $this->userTags()->tagsGet($nUserID);
        if (static::registerPhoneContacts() && $aData['phone_number'] && $aData['phone_number_verified']) {
            $ph = array(
                't' => static::CONTACT_TYPE_PHONE,
                'v' => $aData['phone_number'],
            );
            if (empty($aData['contacts'])) {
                $aData['contacts'] = array(0=>$ph);
            } else {
                array_unshift($aData['contacts'], $ph);
            }
        }

        # накручиваем счетчик просмотров если:
        # - не владелец
        # - не перешли по закладкам профиля
        if (!User::isCurrent($nUserID)) {
            $sReferer = Request::referer();
            if (empty($sReferer) || mb_strpos($sReferer, $aData['login']) === false) {
                if ($this->model->userViewsIncrement($nUserID, 'user', $aData['views_today'])) {
                    $aData['views_total']++;
                    $aData['views_today']++;
                }
            }
        }

        $aFirstSpec =  reset($aData['specs']);
        $aData['dynprops_simple'] = Specializations::i()->dpView($aFirstSpec['spec_id'], $aFirstSpec, 'd', 'view.dp.simple');
        $aData['dynprops_simple'] = json_decode($aData['dynprops_simple'], true);
        foreach ($aData['specs'] as $spec){
            if($spec['main']){
                $aData['main_specs_title'] = $spec['spec_title'];
            }
        }
        if($OnlyData){
            return $aData;
        }
        Users::setUserProfileData($nUserID);
        return $this->viewPHP($aData, 'profile.view');
    }

    /**
     * Управление главной фотографией в личном кабинете пользователя
     */
    public function profileAvatar()
    {
        $nUserID = User::id();

        if (!$nUserID) {
            if (Request::isAJAX()) {
                $this->showAccessDenied();
            }
        }

        $sAction = $this->input->getpost('act', TYPE_STR);
        if ($sAction) {
            if (!$this->security->validateToken('') && ! in_array($sAction, array('avatar-upload', 'avatar-delete'))) {
                $this->errors->reloadPage();
                $this->ajaxResponseForm();
            }

            $aResponse = array();
            switch ($sAction) {
                case 'avatar-upload':
                {
                    if (!$this->security->validateToken('')) {
                        $this->errors->reloadPage();
                        $mResult = false;
                    } else {
                        $mResult = $this->avatar($nUserID)->uploadQQ(true, true);
                    }

                    $aResponse = array(
                        'success' => ($mResult !== false && $this->errors->no()),
                        'errors'  => $this->errors->get(),
                    );
                    if ($mResult !== false) {
                        $this->security->updateUserInfo(array('avatar' => $mResult['filename']));
                        $nSex = User::data('sex');
                        $aResponse = array_merge($aResponse, $mResult);
                        foreach (array(UsersAvatar::szNormal, UsersAvatar::szSmall, UsersAvatar::szBig) as $size) {
                            $aResponse[$size] = UsersAvatar::url($nUserID, $mResult['filename'], $size, $nSex);
                        }
                    }

                    $this->ajaxResponse($aResponse, true, false);
                }
                    break;
                case 'avatar-delete':
                {

                    $bDeleted = $this->avatar($nUserID)->delete(true);
                    if ($bDeleted) {
                        $nSex = User::data('sex');
                        $aResponse[UsersAvatar::szBig] = UsersAvatar::url(0, false, UsersAvatar::szBig, $nSex);
                        $aResponse[UsersAvatar::szNormal] = UsersAvatar::url(0, false, UsersAvatar::szNormal, $nSex);
                        $aResponse[UsersAvatar::szSmall] = UsersAvatar::url(0, false, UsersAvatar::szSmall, $nSex);
                        $this->security->updateUserInfo(array('avatar' => ''));
                    }
                }
                    break;
                default:
                {
                    $this->errors->impossible();
                }
                    break;
            }
            if( ! empty($aResponse) || Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }


    }

    /**
     * Настройки профиля
     */
    public function settings()
    {
        $nUserID = User::id();

        if (!$nUserID) {
            if (Request::isAJAX()) {
                $this->showAccessDenied();
            }
            $this->redirect(bff::urlBase());
        }

        $this->security->setTokenPrefix('my-settings');
        # доступность настроек:
        $on_email = true; # смена email-адреса
        $on_destroy = false; # удаление аккаунта
        $bUseServices = Specializations::useServices(Specializations::SERVICES_PRICE_USERS);
        $on_join = static::accountsJoin();

        $sAction = $this->input->getpost('act', TYPE_STR);
        if ($sAction) {
            if (!$this->security->validateToken() && ! in_array($sAction, array('avatar-upload', 'verified-view', 'finance-view'))) {
                $this->errors->reloadPage();
                $this->ajaxResponseForm();
            }

            $aResponse = array();
            switch ($sAction) {
                case 'contacts':
                {

                    $aParams = array(
                        'login'        => array(TYPE_NOTAGS, 'len' => 75, 'len.sys' => 'users.user.login.limit'),
                        'name'         => array(TYPE_NOTAGS, 'len' => 75, 'len.sys' => 'users.user.name.limit'),
                        'surname'      => array(TYPE_NOTAGS, 'len' => 75, 'len.sys' => 'users.user.surname.limit'),
                        'spec'         => TYPE_ARRAY, # специализации
                        'contacts'     => TYPE_ARRAY, # контакты
                        'experience'   => TYPE_UINT,  # опыт работы
                    );
                    if ($bUseServices) {
                        $aParams['specsServices'] = TYPE_ARRAY; # услуги специализаций
                    }
                    # обработка специализацй перенесена в отдельный case specs
                    unset($aParams['spec']);
                    if(isset($aParams['specsServices'])){
                        unset($aParams['specsServices']);
                    }

                    if (static::profileMap()) {
                        $aParams['reg1_country'] = TYPE_UINT; # страна
                        $aParams['reg3_city'] = TYPE_UINT; # город
                        $aParams['metro_id']  = TYPE_UINT; # станция метро
                        $aParams['district_id']  = TYPE_UINT; # район города
                        $aParams['addr_addr'] = array(TYPE_NOTAGS, 'len' => 400, 'len.sys' => 'users.user.addr.limit');
                        $aParams['addr_lat']  = TYPE_NUM; # адрес, координата LAT
                        $aParams['addr_lng']  = TYPE_NUM; # адрес, координата LNG
                    }
                    if (static::profileBirthdate()) {
                        $aParams['birthdate'] = TYPE_ARRAY_UINT;
                    }
                    if (static::profileSex()) {
                        $aParams['sex'] = TYPE_UINT;
                    }

                    $aData = $this->input->postm($aParams);
                    $this->cleanUserData($aData);

                    # антиспам фильтр
                    Site::i()->spamFilter(array(
                        array('text' => & $aData['name'],    'error'=>_t('users', 'В указанном вами имени присутствует запрещенное слово "[word]"')),
                        array('text' => & $aData['surname'], 'error'=>_t('users', 'В указанной вами фамилии присутствует запрещенное слово "[word]"')),
                    ));

                    if (mb_strlen($aData['login']) < $this->loginMinLength) {
                        $this->errors->set(_t('users', 'Длина логина должна быть более 2 символов'), 'login');
                        break;
                    }
                    if ( ! $this->isLoginCorrect($aData['login'])) {
                        $this->errors->set(_t('users', 'Логин указан некорректно, допустимые символы: a-z, 0-9, _'), 'login');
                        break;
                    }
                    $aOldData = $this->model->userData($nUserID, array('login', 'name', 'surname', 'verified', 'type'));
                    if ($aOldData['login'] != $aData['login']) {
                        if ($this->model->userLoginExists($aData['login'], $nUserID)) {
                            $this->errors->set(_t('users', 'Логин занят. Укажите другой.'), 'login');
                            break;
                        }
                    } else {
                        unset($aData['login']);
                    }

                    # обязательный номер телефона
                    if (Users::profilePhoneRequired()) {
                        $bPhoneError = true;
                        do {
                            if (empty($aData['contacts'])) { break; }
                            $aContacts = func::unserialize($aData['contacts']);
                            foreach ($aContacts as $v) {
                                if ($v['t'] == static::CONTACT_TYPE_PHONE) {
                                    $bPhoneError = false;
                                    break;
                                }
                            }
                        } while(false);
                        if ($bPhoneError) {
                            $this->errors->set(_t('users', 'Необходимо указать номер телефона в контактах'));
                            break;
                        }
                    }
                    if ( ! $this->errors->no()) break;

                    # валидация специализаций + услуг специализаций
                    $this->cleanSpecializations($aData, User::type(), User::isPro());
                    $this->cleanSpecializationsServices($aData);

                    $aResponse['name'] = $aData['name'];

                    $updateVerifiedCounter = false;
                    if (static::verifiedEnabled()) {
                        if ($aOldData['verified']) {
                            if ($aOldData['name'] != $aData['name'] || $aOldData['surname'] != $aData['surname']) {
                                $aData['verified'] = static::VERIFIED_STATUS_NONE;
                                $updateVerifiedCounter = true;
                                $aResponse['reload'] = 1;
                            } else {
                                unset($aData['name'], $aData['surname']);
                            }
                        }
                    }

                    $this->model->userSave($nUserID, $aData);

                    # обновляем счетчик ожидающих верификации
                    if ($updateVerifiedCounter) {
                        $this->verifiedCounterUpdate();
                    }

                    $this->security->updateUserInfo($aData);

                    if (User::isWorker()) {
                        $this->userTags()->tagsSave($nUserID); # теги
                    }
                }
                break;
                case 'specs':
                {

                    $aParams = array(
                        'spec'         => TYPE_ARRAY, # специализации
                        'schedule'     => TYPE_ARRAY,  # график занятости
                        'status_text'  => TYPE_STR, # статус текст - о себе
                    );
                    if ($bUseServices) {
                        $aParams['specsServices'] = TYPE_ARRAY; # услуги специализаций
                    }

                    $aData = $this->input->postm($aParams);

                    # валидация специализаций + услуг специализаций
                    $this->cleanSpecializations($aData, User::type(), User::isPro());
                    $this->cleanSpecializationsServices($aData);

                    # Prepare and validate schedule
                    if (isset($aData['schedule']) && !empty($aData['schedule'])){
                        Orders::i()->prepareValidateSchedule($aData);
                    } else {
                        if(User::isWorker()) {
                            $this->errors->set(_t('schedule','Ошибка заполнения графика работ: один временной период в один день должен быть выбран'));
                        }
                    }
                    if ( ! $this->errors->no()) break;
                    $this->model->userSave($nUserID, $aData);
                    $this->security->updateUserInfo($aData);
                }
                break;
                case 'avatar-upload':
                {

                    if (!$this->security->validateToken()) {
                        $this->errors->reloadPage();
                        $mResult = false;
                    } else {
                        $mResult = $this->avatar($nUserID)->uploadQQ(true, true);
                    }

                    $aResponse = array(
                        'success' => ($mResult !== false && $this->errors->no()),
                        'errors'  => $this->errors->get(),
                    );
                    if ($mResult !== false) {
                        $this->security->updateUserInfo(array('avatar' => $mResult['filename']));
                        $nSex = User::data('sex');
                        $aResponse = array_merge($aResponse, $mResult);
                        foreach (array(UsersAvatar::szNormal, UsersAvatar::szSmall, UsersAvatar::szBig) as $size) {
                            $aResponse[$size] = UsersAvatar::url($nUserID, $mResult['filename'], $size, $nSex);
                        }
                    }

                    $this->ajaxResponse($aResponse, true, false);
                }
                break;
                case 'avatar-delete':
                {

                    $bDeleted = $this->avatar($nUserID)->delete(true);
                    if ($bDeleted) {
                        $nSex = User::data('sex');
                        $aResponse[UsersAvatar::szNormal] = UsersAvatar::url(0, false, UsersAvatar::szNormal, $nSex);
                        $aResponse[UsersAvatar::szSmall] = UsersAvatar::url(0, false, UsersAvatar::szSmall, $nSex);
                        $aResponse[UsersAvatar::szBig] = UsersAvatar::url(0, false, UsersAvatar::szBig, $nSex);
                        $this->security->updateUserInfo(array('avatar' => ''));
                    }
                }
                break;
                case 'social-unlink':
                {

                    $oSocial = $this->social();
                    $providerKey = $this->input->post('provider', TYPE_STR);
                    $providerID = $oSocial->getProviderID($providerKey);
                    if ($providerID) {
                        $res = $oSocial->unlinkSocialAccountFromUser($providerID, $nUserID);
                        if (!$res) {
                            $this->errors->reloadPage();
                        }
                    }
                }
                break;
                case 'enotify':
                {

                    $aUserEnotify = $this->input->post('enotify', TYPE_ARRAY_UINT);
                    $res = $this->model->userSave($nUserID, array('enotify' => array_sum($aUserEnotify)));
                    if (empty($res)) {
                        $this->errors->reloadPage();
                    }
                }
                break;
                case 'profile-tabs':
                {

                    if (static::useClient() && User::isClient()) {
                        break;
                    }
                    $aProfileTabs = $this->input->post('profile_tabs', TYPE_ARRAY_NOTAGS);
                    $aTabs = $this->getProfileTabs(false, User::type());
                    $aSave = array();
                    foreach ($aTabs as $k => $v) {
                        $aSave[$k] = in_array($k, $aProfileTabs, true) ? 1 : 0;
                    }

                    $this->model->userProfileTabsSave($nUserID, $aSave);
                }
                break;
                case 'pass':
                {
                    $this->input->postm(array(
                            'pass0' => TYPE_NOTRIM, # текущий пароль
                            'pass1' => TYPE_NOTRIM, # новый пароль
                        ), $p
                    );
                    extract($p, EXTR_REFS);

                    if (!User::isCurrentPassword($pass0)) {
                        $this->errors->set(_t('users', 'Текущий пароль указан некорректно'), 'pass0');
                        break;
                    }

                    if (empty($pass1)) {
                        $this->errors->set(_t('users', 'Укажите новый пароль'), 'pass1');
                    } elseif (mb_strlen($pass1) < $this->passwordMinLength) {
                        $this->errors->set(_t('users', 'Новый пароль не должен быть короче [symbols] символов',
                                array('symbols' => $this->passwordMinLength)
                            ), 'pass1'
                        );
                    } elseif ($pass0 == $pass1) {
                        $this->errors->set(_t('users', 'Новый пароль не должен совпадать с текущим'), 'pass1');
                    }

                    # запрещаем редактирование пароля администраторам
                    if ($this->model->userIsAdministrator($nUserID)) {
                        $this->errors->reloadPage(); break;
                    }

                    if (!$this->errors->no()) {
                        break;
                    }

                    $sNewPasswordHash = $this->security->getUserPasswordMD5($pass1, User::data('password_salt'));
                    $res = $this->model->userSave($nUserID, array('password' => $sNewPasswordHash));
                    if (!empty($res)) {
                        $this->security->updateUserInfo(array('password' => $sNewPasswordHash));
                    } else {
                        $this->errors->reloadPage();
                    }
                }
                break;
                case 'email':
                {
                    if (!$on_email) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $this->input->postm(array(
                            'email' => array(TYPE_NOTAGS, 'len' => 100, 'len.sys' => 'users.user.email.limit'), # новый email
                            'pass'  => TYPE_NOTRIM, # текущий пароль
                        ), $p
                    );
                    extract($p, EXTR_REFS);

                    if (!User::isCurrentPassword($pass)) {
                        $this->errors->set(_t('users', 'Текущий пароль указан некорректно'), 'pass');
                        break;
                    }

                    if (!$this->input->isEmail($email)) {
                        $this->errors->set(_t('users', 'E-mail адрес указан некорректно'), 'email');
                        break;
                    }
                    # антиспам фильтр: временные ящики
                    if (Site::i()->spamEmailTemporary($email)) {
                        $this->errors->set(_t('', 'Указанный вами email адрес находится в списке запрещенных, используйте например @gmail.com'));
                        break;
                    }

                    if ($this->model->userEmailExists($email)) {
                        $this->errors->set(_t('users', 'Пользователь с таким e-mail адресом уже зарегистрирован'), 'email');
                        break;
                    }

                    $res = $this->model->userSave($nUserID, array('email' => $email));
                    if (!empty($res)) {
                        $aResponse['email'] = $email;
                        $this->security->updateUserInfo(array('email' => $email));
                    } else {
                        $this->errors->reloadPage();
                    }
                }
                break;
                case 'phone': # изменение телефона
                {
                    if (!static::registerPhone()) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $this->input->postm(array(
                        'phone' => array(TYPE_NOTAGS, 'len' => 30, 'len.sys' => 'users.phone.limit'), # новый номер телефона
                        'code'  => TYPE_NOTAGS, # код активации из sms
                        'step'  => TYPE_NOTAGS, # этап
                    ), $p); extract($p, EXTR_REFS);

                    if ( ! $this->input->isPhoneNumber($phone)) {
                        $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                        break;
                    }

                    if ($this->model->userPhoneExists($phone, $nUserID)) {
                        $this->errors->set(_t('users', 'Пользователь с таким номером телефона уже зарегистрирован.'));
                        break;
                    }

                    if ($step == 'code-send') {
                        $activationData = $this->getActivationInfo();
                        $res = $this->sms()->sendActivationCode($phone, $activationData['key']);
                        if ($res) {
                            $activationData['key'] = md5($phone.$activationData['key']);
                            $activationData = $this->updateActivationKey($nUserID, $activationData['key']);
                            if ( ! $activationData) {
                                $this->errors->reloadPage();
                                break;
                            }
                            $aResponse['msg'] = _t('users', 'Код подтверждения был отправлен на указанный вами номер');
                        }
                    } else if ($step == 'finish') {
                        $aUserData = $this->model->userData($nUserID, array('activate_key'));
                        if (empty($aUserData['activate_key'])) {
                            $this->errors->reloadPage();
                            break;
                        }
                        if (mb_strtolower($aUserData['activate_key']) !== mb_strtolower(md5($phone.$code))) {
                            $this->errors->set(_t('users', 'Код подтверждения указан некорректно'), 'code');
                            break;
                        }
                        $res = $this->model->userSave($nUserID, array(
                            'phone_number' => $phone,
                            'phone_number_verified' => 1,
                            'activate_key' => '',
                        ));
                        if (!empty($res)) {
                            $aResponse['phone'] = '+'.$phone;
                            $aResponse['msg'] = _t('users', 'Номер телефона изменен.');
                        } else {
                            $this->errors->reloadPage();
                        }
                    }
                }
                break;
                case 'destroy':
                {
                    if (!$on_destroy) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $pass = $this->input->post('pass', TYPE_NOTRIM, array('len' => 100));
                    if (!User::isCurrentPassword($pass)) {
                        $this->errors->set(_t('users', 'Текущий пароль указан некорректно'), 'pass');
                        break;
                    }

                    // TODO

                    $aResponse['redirect'] = bff::urlBase();

                }
                break;
                case 'verified':
                {

                    $user = $this->model->userData($nUserID, array('verified', 'verified_reason'));
                    if ( ! in_array($user['verified'], array(static::VERIFIED_STATUS_NONE, static::VERIFIED_STATUS_DECLINED))) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $images = $this->verifiedImages($nUserID);
                    if ( ! empty($_FILES)) {
                        $i = 0; $limit = $images->getLimit();
                        foreach ($_FILES as $k => $v) {
                            if ($i >= $limit) break;
                            if (strpos($k, 'verified_') === 0) {
                                $images->uploadFILES($k);
                            }
                        }
                    }

                    $deleted = $this->input->post('verified_deleted', TYPE_ARRAY_STR);
                    if ( ! empty($deleted)) {
                        $images->deleteImages($deleted);
                    }

                    $imgData = $images->getData();
                    if ( ! empty($imgData)) {
                        $update = array('verified' => static::VERIFIED_STATUS_WAIT);
                        $reason = func::unserialize($user['verified_reason']);
                        if (isset($reason['cnt'])) {
                            $reason['cnt']++;
                            $update['verified_reason'] = serialize($reason);
                        }
                        $this->model->userSave($nUserID, $update);
                        $this->verifiedCounterUpdate(true);
                    }
                    $aResponse = array('reload' => 1);
                    $this->iframeResponseForm($aResponse);
                }
                break;
                case 'verified-view':
                {
                    $user = $this->model->userData($nUserID, array('verified', 'verified_reason'));
                    if ( ! in_array($user['verified'], array(static::VERIFIED_STATUS_NONE, static::VERIFIED_STATUS_DECLINED))) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $imageID = $this->input->get('id', TYPE_UINT);
                    $images = $this->verifiedImages($nUserID);
                    $data = $images->getImageData($imageID);
                    if (empty($data)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($data['filename'] != $this->input->get('fn', TYPE_STR)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $images->show($data);
                }
                break;
                case 'finance-view':
                {
                    if (bff::fairplayEnabled()) {
                        return Fairplay::i()->my_settings_finance();
                    }
                }
                break;
                case 'join-profile-on':
                {
                    if ( ! $on_join) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $this->input->postm(array(
                        'email' => array(TYPE_NOTAGS, 'len' => 100, 'len.sys' => 'users.user.email.limit'), # email
                        'pass'  => TYPE_NOTRIM, # пароль
                    ), $p);
                    extract($p, EXTR_REFS);

                    $user = $this->model->userData($nUserID, array('user_id', 'type', 'joined_id'));
                    if ( ! empty($user['joined_id'])) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $join = $this->model->userDataByFilter(array('email' => $email), array('user_id', 'email', 'type', 'password',
                                                            'password_salt', 'joined_id', 'blocked', 'blocked_reason', 'deleted'));
                    if (empty($join)
                        || $join['password'] != \bff::security()->getUserPasswordMD5($pass, $join['password_salt'])
                        || $join['deleted'] != 0) {
                        $this->errors->set(_t('users', 'E-mail или пароль указан некорректно'), 'pass');
                        break;
                    }

                    if ($join['blocked'] == 1) {
                        $this->errors->set(_t('users', 'Аккаунт заблокирован по причине: [reason]', array('reason' => '<br />'.nl2br($join['blocked_reason']))));
                        break;
                    }

                    if ( ! empty($join['joined_id'])) {
                        $this->errors->set(_t('users', 'Указанный профиль уже объединен с другим профилем'), 'email');
                        break;
                    }

                    if ($user['type'] == $join['type']) {
                        if ($user['type'] == static::TYPE_CLIENT) {
                            $this->errors->set(_t('users', 'Укажите e-mail профиля исполнителя'), 'email');
                        } else {
                            $this->errors->set(_t('users', 'Укажите e-mail профиля заказчика'), 'email');
                        }
                        break;
                    }
                    if ($this->model->userSave($nUserID, array('joined_id' => $join['user_id']))) {
                        $this->model->userSave($join['user_id'], array('joined_id' => $nUserID));
                        $aResponse['msg'] = _t('users', 'Профили объеденены');
                        $aResponse['email'] = $join['email'];
                    }


                }
                break;
                case 'join-profile-off':
                {
                    if ( ! $on_join) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $user = $this->model->userData($nUserID, array('user_id', 'joined_id'));
                    if (empty($user['joined_id'])) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $join = $this->model->userDataByFilter($user['joined_id'], array('user_id', 'joined_id'));
                    if ($join['joined_id'] != $nUserID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($this->model->userSave($nUserID, array('joined_id' => 0))) {
                        $this->model->userSave($join['user_id'], array('joined_id' => 0));
                        $aResponse['msg'] = _t('users', 'Профили отсоединены');
                    }
                }
                break;
                default:
                {
                    $this->errors->impossible();
                }
                break;
            }
            if( ! empty($aResponse) || Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $aFields = array(
            'user_id as id', 'type', 'role_id', 'email', 'login', 'spec_id', 'specs', 'cat_id',
            'name', 'surname', 'enotify', 'avatar', 'sex', 'pro', 'birthdate', 'verified', 'verified_reason',
            'contacts', 'addr_addr', 'addr_lat', 'addr_lng', 'joined_id',
            'reg1_country', 'reg3_city', 'metro_id', 'district_id', 'experience', 'phone_number', 'schedule',
            'status_text',
        );
        if ($bUseServices) {
            $aFields[] = 'specsServices';
        }

        $aData = $this->model->userData($nUserID, $aFields);
        if (empty($aData)) {
            # ошибка получения данных о пользователе
            bff::log('Неудалось получить данные о пользователе #' . $nUserID);
            $this->security->sessionDestroy();
        }

        if ($on_join) {
            if ($aData['joined_id']) {
                $aData['joined'] = $this->model->userData($aData['joined_id'], array('user_id as id', 'email'));
            }
        }

        $aData['avatar_small'] = UsersAvatar::url($nUserID, $aData['avatar'], UsersAvatar::szSmall, $aData['sex']);
        $aData['avatar_big'] = UsersAvatar::url($nUserID, $aData['avatar'], UsersAvatar::szBig, $aData['sex']);
        $aData['avatar_normal'] = UsersAvatar::url($nUserID, $aData['avatar'], UsersAvatar::szNormal, $aData['sex']);
        $aData['avatar_maxsize'] = $this->avatar($nUserID)->getMaxSize();

        # данные о привязанных соц. аккаунтах
        $oSocial = $this->social();
        $aSocialProviders = $oSocial->getProvidersEnabled();
        $aSocialUser = $oSocial->getUserSocialAccountsData($nUserID);
        foreach ($aSocialUser as $k => $v) {
            if (isset($aSocialProviders[$k]) && strpos($v['profile_data'], 'a:') === 0) {
                $aSocialProviders[$k]['user'] = func::unserialize($v['profile_data']);
            }
        }
        $aData['social'] = $aSocialProviders;

        # настройки уведомлений
        $aData['enotify'] = $this->getEnotifyTypes($aData['enotify']);

        # настройки настройки закладок профиля
        $aData['profile_tabs'] = $this->getProfileTabs($nUserID, $aData['type']);

        $aData['on'] = array(
            'email'   => $on_email,
            'destroy' => $on_destroy,
            'join'    => $on_join,
        );

        # город, метро
        $aData['city_data'] = Geo::regionData($aData['reg3_city']);
        $aData['metro_data'] = Geo::cityMetro($aData['reg3_city'], $aData['metro_id'], false);
        if (empty($aData['metro_data']['sel']['id'])) {
            $aData['metro_id'] = 0;
        }

        # специализации
        $oSpecs = Specializations::i();
        foreach ($aData['specs'] as &$v) {
            $aDop = array(
                'prefix' => $v['spec_id'],
                'dp'     => $oSpecs->dpForm($v['spec_id'], false, $v, 'd'.$v['spec_id'], 'form.dp', $this->module_dir_tpl), # дин. свойства
                'pr'     => $this->priceForm($v['spec_id'], $v), # цена, бюджет
            );
            $v['dp'] = $this->viewPHP($aDop, 'my.settings.spec');
            if ($bUseServices) {
                $v['serv'] = $oSpecs->servicesForm($v['spec_id'], $aData['specsServices']); # услуги + прайс
            }
        } unset($v);

        if (static::verifiedEnabled()) {
            $aData['verified_images'] = array();
            if ($aData['verified'] == Users::VERIFIED_STATUS_DECLINED) {
                $aData['verified_reason'] = func::unserialize($aData['verified_reason'], array('id' => 0, 'message' => '', 'cnt' => 0));
                $verifiedReasons = static::verifiedReasons();
                if ($aData['verified_reason']['id'] > 0 && isset($verifiedReasons[$aData['verified_reason']['id']])) {
                    $aData['verified_reason']['message'] = $verifiedReasons[$aData['verified_reason']['id']]['t'];
                } else if ($aData['verified_reason']['id'] == static::VERIFIED_REASON_OTHER) {
                    $aData['verified_reason']['message'] = nl2br($aData['verified_reason']['message']);
                }
                $images = $this->verifiedImages($nUserID);
                $aData['verified_images'] = $images->getData();
                foreach ($aData['verified_images'] as & $v) {
                    $v['link'] = static::url('my.settings', array('act' => 'verified-view', 'id' => $v['id'], 'fn' => $v['filename']));
                } unset($v);
            }
        }
        bff::setMeta(_t('', 'Settings'));
        return $this->viewPHP($aData, 'my.settings');
    }

    /**
     * Форма управления заметкой о пользователе (просмотр / добавление / редактирование)
     * @param integer $nUserID ID пользователя, о котором размещается заметка
     * @param array $aParams параметры
     * @return string
     */
    public function note($nUserID, $aParams = array())
    {
        $nAuthorID = User::id();
        if (!$nAuthorID || !$nUserID) {
            return '';
        }
        if ($nAuthorID == $nUserID) {
            return '';
        }

        $aData = $this->model->noteData($nAuthorID, $nUserID);
        if (empty($aData['user_id'])) {
            $aData['user_id'] = $nUserID;
        }
        $aData += $aParams;

        return $this->viewPHP($aData, 'my.note');
    }

    public function noteBlock($aData)
    {
        return $this->viewPHP($aData, 'my.note.block');
    }

    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR)) {
            case 'tags-autocomplete':
            {
                $sQuery = $this->input->post('q', TYPE_STR);
                $this->userTags()->tagsAutocomplete($sQuery);
            }
            break;
            case 'note-save': # заметка: сохранение (добавление / редактирование)
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nNoteID = $this->input->post('id', TYPE_UINT);
                $nUserID = $this->input->post('user_id', TYPE_UINT);

                $aData = $this->input->postm(array(
                        'note' => array(TYPE_NOTAGS, 'len' => 1000, 'len.sys' => 'users.note.limit'),
                    )
                );
                if (!$nUserID) {
                    break;
                }
                if (!strlen($aData['note'])) {
                    $this->errors->set(_t('users', 'Укажите текст заметки'));
                    break;
                }
                if (!$nNoteID) {
                    $aData['user_id'] = $nUserID;
                    $aData['author_id'] = User::id();
                }
                if ($this->model->noteSave($nNoteID, $aData)) {
                    $aResponse['data'] = $this->model->noteData(User::id(), $nUserID);
                    $aResponse['data']['note'] = nl2br($aResponse['data']['note']);
                }
            }
            break;
            case 'note-delete': # заметка: удаление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nNoteID = $this->input->post('id', TYPE_UINT);
                $aData = $this->model->noteData($nNoteID);
                if (empty($aData)) {
                    break;
                }

                if ($aData['author_id'] != User::id()) {
                    break;
                }
                if (!$this->model->noteDelete($nNoteID)) {
                    $this->errors->impossible();
                }

            }
            break;
            case 'status': # статус: тип
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                if (!User::type(static::TYPE_WORKER)) {
                    $this->errors->impossible();
                    break;
                }

                $nStatusID = $this->input->post('status', TYPE_UINT);
                $aStatus = static::aWorkerStatus();
                if (!array_key_exists($nStatusID, $aStatus)) {
                    $this->errors->impossible();
                    break;
                }

                $this->model->userSave(User::id(), array('status' => $nStatusID));

                $aResponse['title'] = $aStatus[$nStatusID]['t'];
                $aResponse['class'] = $aStatus[$nStatusID]['c'];
            }
            break;
            case 'status-text': # статус: описание
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                if (!User::type(static::TYPE_WORKER)) {
                    $this->errors->impossible();
                    break;
                }

                $sStatusText = $this->input->post('status_text', array(TYPE_NOTAGS, 'len' => 300, 'len.sys' => 'users.user.status.limit'));
                $sStatusText = mb_substr($sStatusText, 0 ,300);

                # антиспам фильтр
                if(Site::i()->spamFilter(array(
                    array('text' => & $sStatusText,   'error'=>_t('users', 'В указанном вами статусе присутствует запрещенное слово "[word]"')),
                ))) {
                    break;
                }

                $this->model->userSave(User::id(), array('status_text' => $sStatusText));
                $aResponse['status_text'] = nl2br($sStatusText);
            }
            break;
            case 'dp-form': # дин. свойства: форма
            {
                $nID = $this->input->post('id', TYPE_UINT);
                $bSearch = $this->input->post('search', TYPE_BOOL);
                $sFormName = $bSearch ? 'search.dp' : 'form.dp';
                if ($bSearch) {
                    $aResponse['dp'] = Specializations::i()->dpForm($nID, $bSearch, false, 'd'.$nID, $sFormName, $this->module_dir_tpl);
                } else {
                    $aDop = array(
                        'prefix' => $nID,
                        'dp'     => Specializations::i()->dpForm($nID, $bSearch, false, 'd'.$nID, $sFormName, $this->module_dir_tpl),
                        'pr'     => $this->priceForm($nID),
                    );
                    $aResponse['dp'] = $this->viewPHP($aDop, 'my.settings.spec');
                }

            } break;
            case 'dp-child': # дин. свойства: форма child-свойства
            {
                $p = $this->input->postm(array(
                        'dp_id'       => TYPE_UINT, # ID parent-дин.свойства
                        'dp_value'    => TYPE_UINT, # ID выбранного значения parent-дин.свойства
                        'name_prefix' => TYPE_NOTAGS, # Префикс для name
                    )
                );
                if (empty($p['dp_id']) && empty($p['dp_value'])) {
                    $this->errors->impossible();
                } else {
                    $aData = Specializations::i()->dp()->formChildByParentIDValue($p['dp_id'], $p['dp_value'], array(
                            'name'  => $p['name_prefix'],
                            'class' => 'form-control input-sm'
                        ), false
                    );
                    $aResponse['form'] = $aData;
                }
            } break;
            case 'dp-child-search': # дин. свойства: поиск child-свойств
            {
                $p = $this->input->postm(array(
                        'dp_id'    => TYPE_UINT, # ID parent-дин.свойства
                        'dp_value' => TYPE_UINT, # ID выбранного значения parent-дин.свойства
                        'id'       => TYPE_UINT, # ID специализации или категории
                    )
                );

                if (empty($p['dp_id']) && empty($p['dp_value'])) {
                    $this->errors->impossible();
                } else {
                    $aResponse['form'] = Specializations::i()->dpForm($p['id'], true, false, 'd', 'search.dp', $this->module_dir_tpl, $p);
                }
            } break;
            case 'specServices-form': # услуги специализации: форма
            {
                if ( ! Specializations::useServices()) {
                    $this->errors->reloadPage();
                    break;
                }
                $nUserID = User::id();
                if ( ! $nUserID) {
                    $this->errors->reloadPage();
                    break;
                }
                $nSpecID = $this->input->post('id', TYPE_UINT);
                if ( ! $nSpecID) {
                    $this->errors->reloadPage();
                    break;
                }
                $aData = $this->model->userData($nUserID, array('specsServices'));
                $aResponse['services'] = Specializations::i()->servicesForm($nSpecID, $aData['specsServices']);
            } break;
            case 'fav-add': # избранные: добавление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nUserID = User::id();
                if ( ! $nUserID) {
                    $this->errors->impossible();
                    break;
                }

                $nFavUserID = $this->input->post('id', TYPE_UINT);
                if (!$nFavUserID) {
                    $this->errors->reloadPage();
                    break;
                }

                $this->model->favAdd($nUserID, $nFavUserID);
                $aData = $this->model->userData($nFavUserID, 'fav_cnt');
                $aResponse['fav_cnt'] = $aData['fav_cnt'];

            } break;
            case 'fav-del': # избранные: удаление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nUserID = User::id();
                if ( ! $nUserID) {
                    $this->errors->impossible();
                    break;
                }

                $nFavUserID = $this->input->post('id', TYPE_UINT);
                if (!$nFavUserID) {
                    $this->errors->reloadPage();
                    break;
                }

                $this->model->favDel($nUserID, $nFavUserID);
                $aData = $this->model->userData($nFavUserID, 'fav_cnt');
                $aResponse['fav_cnt'] = $aData['fav_cnt'];

            } break;
            case 'views-stat': # График статистики просмотров профиля
            {
                $nUserID = $this->input->post('id', TYPE_UINT);
                if ( ! $nUserID || $nUserID != User::id()) {
                    $this->errors->impossible();
                    break;
                }

                # получаем данные
                $aStat = $this->model->itemViewsData($nUserID);

                if (empty($aStat['data'])) {
                    $this->errors->set(_t('users', 'Статистика просмотров для данного пользователя отсутствует'));
                    break;
                }

                $aResponse['popup'] = $this->viewPHP($aStat, 'profile.view.statistic');
                $aResponse['stat'] = & $aStat;
                $aResponse['lang'] = array(
                    'y_title'        => _t('view', 'Количество просмотров'),
                    'total'          => _t('view', 'Всего'),
                    'views'          => _t('view', 'Просмотры'),
                    'months'         => explode(',', _t('view', 'Января,Февраля,Марта,Апреля,Мая,Июня,Июля,Августа,Сентября,Октября,Ноября,Декабря')),
                    'shortMonths'    => explode(',', _t('view', 'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек')),
                );
            } break;
            case 'svc-stairs-save': # редактирование лестницы
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nStairsID = $this->input->post('id', TYPE_UINT);
                if ( ! $nStairsID) {
                    $this->errors->reloadPage();
                }

                $aData = $this->model->svcStairsData($nStairsID);
                if (empty($aData)) {
                    $this->errors->reloadPage();
                }
                if ($aData['user_id'] != User::id()) {
                    $this->errors->reloadPage();
                }

                $aData = $this->input->postm(array(
                    'title'       => TYPE_NOTAGS,
                    'message'     => TYPE_NOTAGS,
                ));
                $this->model->svcStairsSave($nStairsID, $aData);
                $aResponse['title'] = $aData['title'];
                $aResponse['message'] = nl2br($aData['message']);
            } break;
            case 'svc-stairs-descr': # редактирование лестницы описание
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nStairsID = $this->input->post('id', TYPE_UINT);
                if ( ! $nStairsID) {
                    $this->errors->reloadPage();
                }

                $aData = $this->model->svcStairsData($nStairsID);
                if (empty($aData)) {
                    $this->errors->reloadPage();
                }
                if ($aData['user_id'] != User::id()) {
                    $this->errors->reloadPage();
                }

                $aData = $this->input->postm(array(
                    'description'     => TYPE_NOTAGS,
                ));
                $this->model->svcStairsSave($nStairsID, $aData);
                $aResponse['description'] = nl2br($aData['description']);
            } break;
            case 'change-join':
            {
                if ( ! static::accountsJoin()) {
                    $this->errors->reloadPage();
                    break;
                }

                $userID = User::id();
                if( ! $userID) {
                    $this->errors->reloadPage();
                    break;
                }

                $data = $this->model->userData($userID, array('joined_id', 'type'));
                if (empty($data['joined_id'])) {
                    $join = '<br /> '._t('users', 'Для объединения профилей перейдите в <a [link]>настройки</a>.', array('link' => 'href="'.Users::url('my.settings', array('tab' => 'access')).'"'));
                    if (User::isClient()) {
                        $this->errors->set(_t('users', 'Ваш профиль не объединен с профилем исполнителя.').$join);
                    } else {
                        $this->errors->set(_t('users', 'Ваш профиль не объединен с профилем заказчика.').$join);
                    }
                    break;
                }

                $join = $this->model->userData($data['joined_id'], array('user_id', 'joined_id', 'password'));
                if (empty($join) || $join['joined_id'] != $userID) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($this->userAuth($join['user_id'], 'user_id', $join['password'], true, true)) {
                    $aResponse['msg'] = _t('users', 'Профиль переключен');
                }

                $aResponse['redirect'] = static::url('');
            } break;
            default:
            {
                $this->errors->impossible();
            }
        }

        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Поиск исполнителей
     */
    public function search()
    {

        if(User::id() && User::isWorker()){
           $this->redirect(Orders::url('list'));
        }
        bff::setActiveMenu('//users');

        $nPageSize = config::sysAdmin('users.search.pagesize', 10, TYPE_UINT);
        $aFilter = array();
        $aData = array('list'=>array(), 'pgn'=>'', 'cat_id' => 0, 'spec_id' => 0);

        $f = array(
            'spec' => $this->input->getpost('spec', TYPE_STR), # keyword специализации или категории/специализации
        );
        $this->input->postgetm(array(
            'page' => TYPE_UINT,  # № страницы
            'c'    => TYPE_UINT,  # регион: страна
            'r'    => TYPE_UINT,  # регион: область / регион
            'ct'   => TYPE_UINT,  # регион: город
            'me'   => TYPE_UINT,  # регион: метро
            'dt'    => TYPE_UINT, # регион: район города
            'pro'  => TYPE_UINT,  # только для PRO
            'fr'   => TYPE_UINT,  # со статусом "свободен"
            'fv'   => TYPE_UINT,  # мои избранные
            'ex'   => TYPE_UINT,  # с примерами работ
            'op'   => TYPE_UINT,  # с отзывами
            'neg'  => TYPE_UINT,  # без отрицательных отзывов
            'exf'  => TYPE_UINT,  # опыт работы: от
            'ext'  => TYPE_UINT,  # опыт работы: до
            'm'    => TYPE_UINT,  # вид списка: карта
            'q'    => TYPE_NOTAGS,# строка поиска
            'tag'  => TYPE_NOTAGS,# тег: "keyword-id"
            'd'    => TYPE_ARRAY, # дин. свойства
            'dc'   => TYPE_ARRAY, # дин. свойства child
            'vr'   => TYPE_UINT,  # подтвержденные
        ), $f);
        $aData['f'] = &$f;

        $seoFilter = 0;

        $urlData = array('keyword'=>'', 'canonical'=>'');
        if ( ! empty($f['spec']))
        {
            # анализируем URL keyword
            $urlData = Specializations::i()->parseURLKeyword($f['spec'], $this->module_name);
            if ($urlData === false) {
                if (Request::isAJAX()) {
                    $this->errors->reloadPage();
                    $this->ajaxResponseForm();
                }
                $this->errors->error404();
            }
            if ($urlData['spec']) { # специализация
                $aFilter['spec_id'] = $aData['spec_id'] = $urlData['spec']['id'];
                $aData['cat_id'] = ( $urlData['cat'] ? $urlData['cat']['id'] : $urlData['spec']['cat_id'] );
                # дин. св-ва специализации
                $dp = Specializations::i()->dp()->prepareSearchQuery($f['d'], $f['dc'], Specializations::i()->dpSettings($urlData['spec']['id']), 'S.');
                if (!empty($dp)) {
                    $aFilter[':dp'] = $dp;
                    $seoFilter++;
                }
            } else if ($urlData['cat']) { # категория
                $aFilter['spec_id'] = $urlData['cat']['specs'];
                $aData['cat_id'] = $urlData['cat']['id'];
            }
        }
        $url = static::url('list', array('keyword'=>$urlData['keyword']));
        # Регион
        if (Geo::filterEnabled() && Users::profileMap()) { # включен фильтр в шапке
            $geoFilter = Geo::filter('all');
            if (Geo::countrySelect() && ! empty($geoFilter['country'])) {
                $aFilter['reg1_country'] = $geoFilter['country'];
            }
            if ( ! empty($geoFilter['region'])) {
                $aFilter['reg2_region'] = $geoFilter['region'];
            }
            if ( ! empty($geoFilter['city'])) {
                $aFilter['reg3_city'] = $geoFilter['city'];
                if ($f['me']) {
                    $aFilter['metro_id'] = $f['me'];
                }
            }
        } else {
            if (Geo::countrySelect()) { # включен выбор страны
                if ($f['c']) {
                    $aFilter['reg1_country'] = $f['c'];
                }
                if ($f['ct']) { # указан город или регион
                    if ($f['c'] == $f['r']) { # pid совпадает со страной, значит регион
                        $aFilter['reg2_region'] = $f['ct'];
                    } else { # иначе город
                        $aFilter['reg3_city'] = $f['ct'];
                        if ($f['me']) {
                            $aFilter['metro_id'] = $f['me'];
                        }
                    }
                }
            } else {
                if ($f['ct']) { # указан город или регион
                    if (Geo::defaultCountry() == $f['r']) { # pid совпадает со страной по умолчанию, значит регион
                        $aFilter['reg2_region'] = $f['ct'];
                    } else { # иначе город
                        $aFilter['reg3_city'] = $f['ct'];
                        if ($f['me']) {
                            $aFilter['metro_id'] = $f['me'];
                        }
                    }
                }
            }
        }
        if (empty($aFilter['metro_id'])) {
            $f['me'] = 0;
        } else {
            $seoFilter++;
        }
        if($f['dt']){
            $aFilter['district_id'] = $f['dt'];
        }

        if ($f['pro']) {
            $aFilter['pro'] = 1;
            $seoFilter++;
        }

        if ($f['fr']) {
            $aFilter['status'] = static::WORKER_STATUS_FREE;
            $seoFilter++;
        }

        if ($f['m']) {
            $aFilter[':addr_lat'] = 'addr_lat != 0';
            $aFilter[':addr_lng'] = 'addr_lng != 0';
        }

        if ($f['vr'] && static::verifiedEnabled()) {
            $aFilter['verified'] = static::VERIFIED_STATUS_APPROVED;
        }

        if ($f['tag']) {
            if (preg_match('/(.*)-(\d+)/', $f['tag'], $matches) && ! empty($matches[2])) {
                $aTagData = $this->userTags()->tagData($matches[2]);
                if (empty($aTagData) || mb_strtolower($aTagData['tag']) != $matches[1]) $this->errors->error404();
                $aFilter['tag_id'] = $aData['nTagID'] = $aTagData['id'];
                $aData['sTagTitle'] = $aTagData['tag'];
            }
        }

        foreach (array('text'=>'q','ex','op','neg','fv') as $k=>$v) {
            if ($f[$v]) {
                $aFilter[(is_integer($k) ? $v : $k)] = $f[$v];
                $seoFilter++;
            }
        }

        # Опыт работы
        if ($f['exf']) {
            $aFilter[':exf'] = array('experience >= :from', ':from' => $f['exf']);
            $seoFilter++;
        }
        if ($f['ext']) {
            $aFilter[':exf'] = array('experience <= :to',   ':to' => $f['ext']);
            $seoFilter++;
        }



        $nCount = $this->model->searchList($aFilter, true);
        if ($nCount) {
            $pgnQuery = $f; unset($pgnQuery['spec']);
            $pgn = new Pagination($nCount, $nPageSize, array(
                'link'  => $url,
                'query' => $pgnQuery,
            ));
            $aData['list'] = $this->model->searchList($aFilter, false, $pgn->getLimitOffset(), Rating::getOrderFormula('U.pro DESC, U.rating DESC'));
            $bAddNumRow = ($f['m'] || $f['q'] || $f['tag']);
            foreach ($aData['list'] as $k => &$v) {
                if ($bAddNumRow) {
                    $v['n'] = ($pgn->getCurrentPage() - 1) * $nPageSize + $k + 1;
                }
                if ($f['q']) {
                    foreach (explode(' ', $f['q']) as $vv) {
                        $v['name'] = str_replace($vv, '<em>' . $vv . '</em>', $v['name']);
                        $v['surname'] = str_replace($vv, '<em>' . $vv . '</em>', $v['surname']);
                        $v['login_title'] = str_replace($vv, '<em>' . $vv . '</em>', $v['login']);
                    }
                    if ($v['main_spec_title'] == $f['q']) {
                        $v['main_spec_title'] = '<em>'.$v['main_spec_title'].'</em>';
                    }
                }
                $v['contacts'] = Users::i()->getUserContacts($v['user_id']);
                $user_specs = $this->model->userData($v['user_id'], array('specs'));
                $aFirstSpec =  reset($user_specs['specs']);
                unset($user_specs);
                $v['dynprops_simple'] = Specializations::i()->dpView($aFirstSpec['spec_id'], $aFirstSpec, 'd', 'view.dp.simple');
                $v['dynprops_simple'] = json_decode($v['dynprops_simple'], true);

            } unset($v);
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }
        if ($f['m']) {
            if ($nCount) {
                $aData['points'] = array();
                foreach ($aData['list'] as &$v) {
                    $aParam = array('v' => &$v);
                    $aData['points'][] = array(
                        'lat' => $v['addr_lat'],
                        'lng' => $v['addr_lng'],
                        'n'   => $v['n'],
                        'b'   => $this->viewPHP($aParam, 'search.map.balloon'),
                        'id'  => $v['user_id'],
                    );
                } unset($v);
            }
            $aData['list'] = $this->viewPHP($aData, 'search.map.list');
        } else {
            if ($f['q'] || $f['tag']) {
                $aData['list'] = $this->viewPHP($aData, 'search.keywords.list');
            } else {
                $aData['list'] = $this->viewPHP($aData, 'search.list');
            }
        }

        $aData['count'] = tpl::declension($nCount, _t('users', 'исполнитель;исполнителя;исполнителей'));
        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'   => $aData['pgn'],
                'list'  => $aData['list'],
                'count' => $aData['count'],
                'points'=> ( ! empty($aData['points']) ? $aData['points'] : NULL ),
            ));
        }

        if ($f['q'] || $f['tag']) {
            # SEO: Поиск исполнителей (по ключевому слову)
            $this->seo()->robotsIndex(false);
            $this->setMeta('search-keyword', array(
                'query' => ( $f['tag'] ? ( ! empty($aData['sTagTitle']) ? $aData['sTagTitle'] : '?') : tpl::truncate($f['q'], config::sysAdmin('users.search.meta.query.truncate', 50, TYPE_UINT), '') ),
                'page'  => $f['page'],
            ));
            return $this->viewPHP($aData, 'search.keywords');
        }

        # SEO:
        $seoRegion = Geo::regionData(
                ( ! empty($aFilter['reg3_city']) ? $aFilter['reg3_city'] :
                ( ! empty($aFilter['reg2_region']) ? $aFilter['reg2_region'] : 0 ) ) );
        $seoCountry = ( ! empty($seoRegion['country']) ? $seoRegion['country'] : ( ! empty($aFilter['reg1_country']) ? $aFilter['reg1_country'] : 0 ));
        $seoData = array(
            'page' => $f['page'],
            'region' => ( ! empty($seoRegion) ? $seoRegion : '' ),
            'country' => $seoCountry ? Geo::regionData($seoCountry) : '',
        );
        $this->seo()->robotsIndex(!$seoFilter);
        $this->urlCorrection($url);
        $canonical = array('page' => $f['page']);
        if ( ! Geo::filterEnabled()) {
            if($f['c'])  $canonical['c'] = $f['c'];
            if($f['ct']) $canonical['ct'] = $f['ct'];
            if($f['r'])  $canonical['r'] = $f['r'];
        }
        if ($aData['spec_id']) {
            # SEO: Список исполнителей (специализация)
            $this->seo()->canonicalUrl(static::url('list', array('keyword'=>$urlData['canonical']), true), $canonical);
            $seoData['title'] = $urlData['spec']['title'];
            $this->setMeta('search-spec', $seoData, $urlData['spec']);
            $this->seo()->setText($urlData['spec'], $f['page']);
        } else if ($aData['cat_id']) {
            # SEO: Список исполнителей (категория)
            $this->seo()->canonicalUrl(static::url('list', array('keyword'=>$urlData['canonical']), true), $canonical);
            $seoData['title'] = $urlData['cat']['title'];
            $this->setMeta('search-cat', $seoData, $urlData['cat']);
            $this->seo()->setText($urlData['cat'], $f['page']);
        } else {
            # SEO: Список исполнителей (главная)
            $this->seo()->canonicalUrl(static::url('list', array(), true), $canonical);
            $this->setMeta('search-index', $seoData, $seoData);
            $this->seo()->setText($seoData, $f['page']);
        }

        if ($aData['spec_id']) {
            $aData['dp'] = Specializations::i()->dpForm($aData['spec_id'], true, $f, 'd', 'search.dp', $this->module_dir_tpl);
        }
        $aData['form'] = $this->searchForm($aData);
        return $this->viewPHP($aData, 'search');
    }

    protected function searchForm(array &$aData)
    {
        $aData['specs'] = Specializations::model()->specializationsInAllCategories(
            array('id', 'keyword', 'title'), array('id', 'keyword', 'title'), true
        );
        if ($aData['specs']) {
            # формируем URL + помечаем активную категорию / специализацию
            $url = static::url('list');
            foreach ($aData['specs'] as &$v) {
                $v['url'] = $url.$v['keyword'].'/';
                if ( ! empty($v['specs'])) {
                    foreach ($v['specs'] as &$vv) {
                        $vv['url'] = $v['url'].$vv['keyword'].'/';
                        $vv['a'] = ( $aData['spec_id'] == $vv['id'] );
                    } unset($vv);
                    $v['a'] = ( $aData['cat_id'] == $v['id'] );
                } else {
                    $v['a'] = ( $aData['spec_id'] == $v['id'] );
                }
            } unset($v);
        }
        $aData['region_title'] = $aData['f']['ct'] ? Geo::i()->regionTitle($aData['f']['ct']) : '';

        $aData['metro_title'] = '';
        $aData['metro_exists'] = false;
        if (Geo::filterEnabled()) {
            if (Geo::filter('id-city')) {
                $aData['metro_exists'] = Geo::i()->hasMetro(Geo::filter('id-city'));
            }
        } else {
            if ($aData['f']['ct']) {
                $aData['metro_exists'] = Geo::i()->hasMetro($aData['f']['ct']);
            }
        }
        if ($aData['metro_exists'] && $aData['f']['me']) {
            $aMetro = Geo::model()->metroData($aData['f']['me']);
            $aData['metro_title'] = $aMetro['title'];
        }

        $nSpecID = $aData['spec_id'];
        if ( ! $nSpecID && ! $aData['cat_id']) {
            $nSpecID = static::SVC_STAIRS_USER;
        }
        $aData['stairs'] = $this->svc_stairs_block($nSpecID, $aData['cat_id']);

        return $this->viewPHP($aData, 'search.form');
    }

    /**
     * Кабинет: Общая Информация
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    public function my_common(array $userData = [])
    {
        $nUserID = $userData['id'];
        if( ! $nUserID) $this->errors->error404();

        $aData = $this->model->userData($nUserID, array('fav_cnt', 'resume_text', 'resume_file', 'status_text', 'schedule', 'specs', 'district_id'));
        $aData += $userData;

        $aFirstSpec = $aData['specs']= reset($aData['specs']);
        $aData['aPriceSett'] = Specializations::i()->aPriceSett($aFirstSpec['spec_id']);
        $aData['price_rate_text'] = unserialize($aFirstSpec['price_rate_text']);
        $aData['dynprops_simple'] = Specializations::i()->dpView($aFirstSpec['spec_id'], $aFirstSpec, 'd', 'view.dp.simple');
        $aData['dynprops_simple'] = json_decode($aData['dynprops_simple'], true);
        return $this->viewPHP($aData, 'owner.common');
    }

    /**
     * Профиль: Общая Информация
     * @param array $userData - данные о пользователе
     * @return string HTML
     */
    public function user_common(array $userData = array())
    {
        if (empty($userData['id'])) {
            $this->errors->error404();
        }

        return $this->my_common($userData);
    }

    /**
     * Кабинет: Информация
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    public function my_info(array $userData)
    {
        return $this->owner_info($userData['id'], $userData);
    }

    /**
     * Профиль: Информация
     * @param array $userData - данные о пользователе
     * @return string HTML
     */
    public function user_info(array $userData = array())
    {
        if (empty($userData['id'])) {
            $this->errors->error404();
        }

        return $this->owner_info($userData['id'], $userData);
    }

    /**
     * Информация о пользователе
     * @param integer $nUserID id пользователя
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    public function owner_info($nUserID, array $userData)
    {
        $sAct = $this->input->getpost('act', TYPE_STR);
        if (Request::isAJAX())
        {
            $aResponse = array();
            switch ($sAct)
            {
                case 'favs-all': # избранные: развернуть весь список
                {
                    $nUserID = $this->input->post('id', TYPE_UINT);
                    if ( ! $nUserID) {
                        $this->errors->impossible();
                    }

                    $aData = array();
                    $bMy = User::isCurrent($nUserID);
                    $aData['favs'] = $this->model->favList($nUserID, $bMy, 0);
                    if ($bMy) {
                        $aResponse['html'] = $this->viewPHP($aData, 'owner.info.fav.list.my');
                    } else {
                        $aResponse['html'] = $this->viewPHP($aData, 'owner.info.fav.list');
                    }
                } break;
                case 'favs-my-all': # избранные: развернуть весь список своих
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $aData = array();
                    $aData['favs_my'] = $this->model->favListMy(false, 0);
                    $aResponse['html'] = $this->viewPHP($aData, 'owner.info.fav.my.list');
                } break;
                case 'resume-data': # резюме: форма редактирования
                {
                    $nUserID = User::id();
                    if ( ! $nUserID) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $aUserData = $this->model->userData($nUserID, array('resume_text', 'resume_file'));
                    $aUserData['bMy'] = true;
                    $aUserData['resume_file_title'] = '';
                    if (!empty($aUserData['resume_file'])) {
                        list(,,$aUserData['resume_file_title']) = explode(';',$aUserData['resume_file']);
                    }

                    $aResponse['view'] = $this->viewPHP($aUserData, 'owner.info.resume');
                    $aResponse['form'] = $this->viewPHP($aUserData, 'owner.info.resume.form');
                    $aResponse['js'] = js::renderInline(js::POS_FOOT);
                } break;
            }
            $this->ajaxResponseForm($aResponse);
        }

        if ($sAct && Request::isPOST())
        {
            $aResponse = array();

            switch ($sAct) {
                case 'resume-save': # резюме: сохранение
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nUserID = User::id();
                    if ( ! $nUserID) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if (static::useClient()) {
                        if( ! User::isWorker()) {
                            $this->errors->impossible();
                            break;
                        }
                    }

                    $aData = $this->input->postm(array(
                        'resume_text' => TYPE_STR,
                    ));

                    $oAttach = $this->resumeAttach();
                    $oAttach->setAssignErrors(true);
                    $mAttach = $oAttach->uploadFILES('resume_file');

                    # антиспам фильтр
                    Site::i()->spamFilter(array(
                        array('text' => & $aData['resume_text'],   'error'=>_t('users', 'В указанном вами резюме присутствует запрещенное слово "[word]"')),
                    ));

                    if (!$this->errors->no()) {
                        break;
                    }

                    if (!empty($mAttach)) {
                        $oAttach->deleteOld($nUserID);
                        $aData['resume_file'] = $mAttach;
                    } else {
                        if ($this->input->post('resume_file_del', TYPE_UINT)) {
                            $oAttach->deleteOld($nUserID);
                            $aData['resume_file'] = '';
                        }
                    }

                    # Чистим текст
                    $parser = new \bff\utils\TextParser();
                    $aData['resume_text'] = $parser->parseWysiwygTextFrontend($aData['resume_text']);

                    $this->model->userSave($nUserID, $aData);
                } break;
            }
            $this->iframeResponseForm($aResponse);
        }

        if( ! $nUserID) $this->errors->error404();

        $aData = $this->model->userData($nUserID, array('fav_cnt', 'resume_text', 'resume_file', 'status_text', 'schedule'));
        $aData += $userData;

        if ( ! static::useClient() || $aData['type'] == static::TYPE_WORKER) {
            if (!empty($aData['resume_file'])) {
                list(,,$aData['resume_file_title']) = explode(';',$aData['resume_file']);
            } else {
                $aData['resume_file_title'] = '';
            }
        }

        $aData['favs'] = $this->model->favList($nUserID, User::isCurrent($nUserID), 12);
        $aData['favs_cnt'] = count($aData['favs']);

        if (User::isCurrent($nUserID)) {
            $aData['fav_my_cnt'] = $this->model->favListMy(true);
            $aData['favs_my'] = '';
            if ($aData['fav_my_cnt']) {
                $aData['favs_my'] = $this->model->favListMy(false, 12);
                $aData['favs_my_cnt'] = count($aData['favs_my']);
                $aData['favs_my'] = $this->viewPHP($aData, 'owner.info.fav.my.list');
            }
            $aData['favs'] = $this->viewPHP($aData, 'owner.info.fav.list.my');
        } else {
            $aData['favs'] = $this->viewPHP($aData, 'owner.info.fav.list');
        }

        # SEO: Профиль: Информация
        $seoURL = static::url('user.info', array('login'=>$userData['login']), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL);
        $this->seo()->setPageMeta($this->users(), 'profile', array(
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'region'  => $userData['region'],
            'country' => $userData['country'],
        ));

        return $this->viewPHP($aData, 'owner.info');
    }

    /**
     * Кабинет: Прайс
     * @param array $userData данные о пользователе
     * @return string
     */
    public function my_price(array $userData)
    {
        if ( ! Specializations::useServices(Specializations::SERVICES_PRICE_USERS)) {
            $this->errors->error404();
        }
        $nUserID = $userData['id'];
        $userData['specs'] = $this->model->userSpecs($nUserID, true);
        $userServices = $this->model->userSpecsServices($nUserID);
        $userData['services_cnt'] = count($userServices);
        foreach ($userServices as $v) {
            if (isset($userData['specs'][ $v['spec_id'] ])) {
                      $userData['specs'][ $v['spec_id'] ]['services'][] = $v;
            }
        }
        unset($userServices);

        foreach ($userData['specs'] as &$v) {
            if (empty($v['services'])) {
                $aServices = Specializations::model()->specializationDataServices($v['spec_id']);
                $v['services_cnt'] = count($aServices);
            }
        } unset($v);

        # SEO: Профиль: Прайс
        $seoURL = static::url('my.price', array(), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL);
        $this->seo()->setPageMeta($this, 'profile-price', array(
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'country' => $userData['country'],
        ), $userData);

        return $this->viewPHP($userData, 'my.price');
    }

    /**
     * Профиль: Прайс
     * @param array $userData данные о пользователе
     * @return string
     */
    public function user_price(array $userData)
    {
        if ( ! Specializations::useServices(Specializations::SERVICES_PRICE_USERS)) {
            $this->errors->error404();
        }
        $nUserID = $userData['id'];
        $userData['specs'] = $this->model->userSpecs($nUserID, true);
        $userServices = $this->model->userSpecsServices($nUserID);
        $userData['services_cnt'] = count($userServices);
        foreach ($userServices as $v) {
            if (isset($userData['specs'][ $v['spec_id'] ])) {
                      $userData['specs'][ $v['spec_id'] ]['services'][] = $v;
            }
        }
        unset($userServices);

        # Удалим специализации, в которых не указана цена и нет флага бесплатно
        foreach ($userData['specs'] as & $v) {
            if (empty($v['services'])) continue;
            foreach ($v['services'] as $kk => $vv) {
                if ($vv['price'] == 0 && !$vv['price_free']) {
                    unset($v['services'][$kk]);
                    continue;
                }
            }
        } unset($v);
        foreach ($userData['specs'] as $k => $v) {
            if (empty($v['services'])) {
                unset($userData['specs'][$k]);
                continue;
            }
        }

        # SEO: Профиль: Прайс
        $seoURL = static::url('user.price', array('login'=>$userData['login']), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL);
        $this->seo()->setPageMeta($this, 'profile-price', array(
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'region'  => $userData['region'],
            'country' => $userData['country'],
        ), $userData);

        return $this->viewPHP($userData, 'user.price');
    }

    /**
     * Поле ввода номера телефона
     * @param array $fieldAttr аттрибуты поля
     * @param array $options доп. параметры:
     *  'country' => ID страны по-умолчанию
     * @return string HTML
     */
    public function registerPhoneInput(array $fieldAttr = array(), array $options = array())
    {
        $fieldAttr = array_merge(array('name'=>'phone_number'), $fieldAttr);
        $countryList = Geo::countryList();
        $countrySelected = (!empty($options['country']) ? $options['country'] : Geo::i()->defaultCountry());
        if (!$countrySelected) {
            $filter = Geo::filter();
            if (!empty($filter['country'])) {
                $countrySelected = $filter['country'];
            }
        }
        if (!isset($countryList[$countrySelected])) {
            $countrySelected = key($countryList);
        }

        $aData = array(
            'attr' => &$fieldAttr,
            'options' => &$options,
            'countryList' => &$countryList,
            'countrySelected' => &$countryList[$countrySelected],
            'countrySelectedID' => $countrySelected,
        );
        return $this->viewPHP($aData, 'phone.input');
    }


    /**
     * Общий прайс услуг специализаций пользователей
     * @return string
     */
    public function price()
    {
        if ( ! Specializations::useServices()) {
            $this->errors->error404();
        }
        bff::setActiveMenu('//price');

        $nPageSize = config::sysAdmin('users.price.pagesize', 20, TYPE_UINT);
        $aData = array('pgn' => '', 'list' => '', 'cat_id' => 0, 'spec_id' => 0);
        $f = array(
            'spec' => $this->input->getpost('spec', TYPE_STR), # keyword специализации или категории/специализации
        );
        $this->input->postgetm(array(
            'page' => TYPE_UINT,  # № страницы
            'c'    => TYPE_UINT,  # регион: страна
            'r'    => TYPE_UINT,  # регион: область / регион
            'ct'   => TYPE_UINT,  # регион: город
        ), $f);
        $aData['f'] = &$f;

        $aFilter = array();
        $urlData = array('keyword'=>'', 'canonical'=>'');
        if ( ! empty($f['spec']))
        {
            # анализируем URL keyword
            $urlData = Specializations::i()->parseURLKeyword($f['spec'], 'price');
            if ($urlData === false) {
                if (Request::isAJAX()) {
                    $this->errors->reloadPage();
                    $this->ajaxResponseForm();
                }
                $this->errors->error404();
            }

            if ($urlData['spec']) { # специализация
                $aFilter['spec_id'] = $aData['spec_id'] = $urlData['spec']['id'];
                $aData['cat_id'] = ( $urlData['cat'] ? $urlData['cat']['id'] : $urlData['spec']['cat_id'] );
            } else if ($urlData['cat']) { # категория
                $aData['cat_id'] = $aFilter['cat_id'] = ( $urlData['cat'] ? $urlData['cat']['id'] : $urlData['spec']['cat_id'] );

            }
        }
        $url = static::url('price', array('keyword'=>$urlData['keyword']));

        # Регион
        if (Geo::filterEnabled() && Users::profileMap()) { # включен фильтр в шапке
            $geoFilter = Geo::filter('all');
            if (Geo::countrySelect() && ! empty($geoFilter['country'])) {
                $aFilter['reg1_country'] = $geoFilter['country'];
            }
            if ( ! empty($geoFilter['region'])) {
                $aFilter['reg2_region'] = $geoFilter['region'];
            }
            if ( ! empty($geoFilter['city'])) {
                $aFilter['reg3_city'] = $geoFilter['city'];
            }
        } else {
            if (Geo::countrySelect()) { # включен выбор страны
                if ($f['c']) {
                    $aFilter['reg1_country'] = $f['c'];
                }
                if ($f['ct']) { # указан город или регион
                    if ($f['c'] == $f['r']) { # pid совпадает со страной, значит регион
                        $aFilter['reg2_region'] = $f['ct'];
                    } else { # иначе город
                        $aFilter['reg3_city'] = $f['ct'];
                    }
                }
            } else {
                if ($f['ct']) { # указан город или регион
                    if (Geo::defaultCountry() == $f['r']) { # pid совпадает со страной по умолчанию, значит регион
                        $aFilter['reg2_region'] = $f['ct'];
                    } else { # иначе город
                        $aFilter['reg3_city'] = $f['ct'];
                    }
                }
            }
        }

        $aConfig = Specializations::servicesConfig();
        $aData['aConfig'] = $aConfig;
        $onlyAdmin = $aConfig['specs_services_price'] == Specializations::SERVICES_PRICE_ADMIN;
        $aData['bPriceAdmin'] = $onlyAdmin || $aConfig['specs_services_price_author'] == Specializations::SERVICES_PRICE_AUTHOR_ADMIN;
        $aFilter['bPriceAdmin'] = $aData['bPriceAdmin'];

        $aData['form'] = $this->servicesPriceForm($aData);

        $nCount = $this->model->priceList($aFilter, true);
        if ($nCount) {
            $pgnQuery = $f; unset($pgnQuery['spec']);
            $pgn = new Pagination($nCount, $nPageSize, array(
                'link'  => $url,
                'query' => $pgnQuery,
            ));
            $aTmp = $this->model->priceList($aFilter, false, $pgn->getLimitOffset());
            $aData['list'] = array();
            $link = array();
            if ($aData['cat_id']) {
                $link['cat'] = $aData['cat_id'];
            }
            if ($aData['spec_id']) {
                $link['spec'] = $aData['spec_id'];
            }
            foreach ($aTmp as $v) {
                if( ! $onlyAdmin) {
                    $link['keyword'] = $v['keyword'];
                    $v['link'] = static::url('price.users', $link);
                }
                $aData['list'][ $v['spec_id'] ][] = $v;
            }
            unset($aTmp);
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }
        $aData['list'] = $this->viewPHP($aData, 'price.list');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'   => $aData['pgn'],
                'list'  => $aData['list'],
            ));
        }

        # SEO:
        $seoRegion = Geo::regionData(
                ( ! empty($aFilter['reg3_city']) ? $aFilter['reg3_city'] :
                ( ! empty($aFilter['reg2_region']) ? $aFilter['reg2_region'] : 0 ) ) );
        $seoCountry = ! empty($seoRegion['country']) ? $seoRegion['country'] : ! empty($aFilter['reg1_country']) ? $aFilter['reg1_country'] : 0;

        $seoData = array(
            'page' => $f['page'],
            'region' => ( ! empty($seoRegion) ? $seoRegion : '' ),
            'country' => $seoCountry ? Geo::regionData($seoCountry) : '',
        );
        $canonical = array('page' => $f['page']);
        if ( ! Geo::filterEnabled()) {
            if($f['c'])  $canonical['c'] = $f['c'];
            if($f['ct']) $canonical['ct'] = $f['ct'];
            if($f['r'])  $canonical['r'] = $f['r'];
        }
        if ($aData['spec_id']) {
            # SEO: Прайс (специализация)
            $this->urlCorrection($url);
            $this->seo()->canonicalUrl(static::url('price', array('keyword'=>$urlData['canonical']), true), $canonical);
            $seoData['title'] = $urlData['spec']['title'];
            $this->setMeta('price-spec', $seoData, $urlData['spec']);
            $this->seo()->setText($urlData['spec'], $f['page']);
        } else if ($aData['cat_id']) {
            # SEO: Прайс (категория)
            $this->urlCorrection($url);
            $this->seo()->canonicalUrl(static::url('price', array('keyword'=>$urlData['canonical']), true), $canonical);
            $seoData['title'] = $urlData['cat']['title'];
            $this->setMeta('price-cat', $seoData, $urlData['cat']);
            $this->seo()->setText($urlData['cat'], $f['page']);
        } else {
            # SEO: Прайс (главная)
            $this->setMeta('price-index', $seoData, $seoData);
            $this->seo()->setText($seoData, $f['page']);
        }

        return $this->viewPHP($aData, 'price');
    }

    /**
     * Страница услуги. Список исполнителей, предоставляющих услугу.
     * @return string HTML
     */
    public function price_users()
    {
        if ( ! Specializations::useServices()) {
            $this->errors->error404();
        }
        bff::setActiveMenu('//price');

        $nPageSize = config::sysAdmin('users.prices.service.pagesize', 9, TYPE_UINT);
        $aData = array('pgn' => '', 'list' => '', 'cat_id' => 0, 'spec_id' => 0);
        $f = array(
            'service' => $this->input->getpost('service', TYPE_STR), # keyword специализации или категории/специализации
            'cat'     => $this->input->getpost('cat', TYPE_UINT),  # ID категории
            'spec'    => $this->input->getpost('spec', TYPE_UINT), # ID специализации
        );
        $this->input->postgetm(array(
            'page' => TYPE_UINT, # № страницы
            'c'    => TYPE_UINT, # регион: страна
            'r'    => TYPE_UINT, # регион: область / регион
            'ct'   => TYPE_UINT, # регион: город
        ), $f);

        $aData['f'] = &$f;
        $aData['cat_id'] = $f['cat'];
        $aData['spec_id'] = $f['spec'];
        $f['service'] = trim($f['service'], '/');
        if (empty($f['service'])) {
            $this->errors->error404();
        }
        $aData['service'] = Specializations::model()->specializationServiceDataByKeyword($f['service']);
        if (empty($aData['service'])) {
            $this->errors->error404();
        }
        $url = static::url('price.users', array('keyword'=>$aData['service']['keyword']));
        $filter = array('service_id' => $aData['service']['id']);

        # Регион
        if (Geo::filterEnabled() && Users::profileMap()) { # включен фильтр в шапке
            $geoFilter = Geo::filter('all');
            if (Geo::countrySelect() && ! empty($geoFilter['country'])) {
                $filter['reg1_country'] = $geoFilter['country'];
            }
            if ( ! empty($geoFilter['region'])) {
                $filter['reg2_region'] = $geoFilter['region'];
            }
            if ( ! empty($geoFilter['city'])) {
                $filter['reg3_city'] = $geoFilter['city'];
            }
        } else {
            if (Geo::countrySelect()) { # включен выбор страны
                if ($f['c']) {
                    $filter['reg1_country'] = $f['c'];
                }
                if ($f['ct']) { # указан город или регион
                    if ($f['c'] == $f['r']) { # pid совпадает со страной, значит регион
                        $filter['reg2_region'] = $f['ct'];
                    } else { # иначе город
                        $filter['reg3_city'] = $f['ct'];
                    }
                }
            } else {
                if ($f['ct']) { # указан город или регион
                    if (Geo::defaultCountry() == $f['r']) { # pid совпадает со страной по умолчанию, значит регион
                        $filter['reg2_region'] = $f['ct'];
                    } else { # иначе город
                        $filter['reg3_city'] = $f['ct'];
                    }
                }
            }
        }

        # исключаем исполнителей со стоимостью услуги указанной в 0, в случае если "бесплатно" недопустимо
        if ( ! $aData['service']['price_free']) {
            $filter['price'] = true;
        }

        $nCount = $this->model->priceServiceUsers($filter, true);
        if ($nCount) {
            $pgnQuery = $f; unset($pgnQuery['service']);
            $pgn = new Pagination($nCount, $nPageSize, array(
                'link'  => $url,
                'query' => $pgnQuery,
            ));
            $aData['list'] = $this->model->priceServiceUsers($filter, false, $pgn->getLimitOffset(), 'U.pro DESC, U.rating DESC');
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }
        $aData['list'] = $this->viewPHP($aData, 'price.users.list');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'   => $aData['pgn'],
                'list'  => $aData['list'],
            ));
        }

        $aData['bPriceAdmin'] = (Specializations::servicesConfig('specs_services_price_author') == Specializations::SERVICES_PRICE_AUTHOR_ADMIN);
        $aData['form'] = $this->servicesPriceForm($aData);
        $aData['spec'] = Specializations::model()->specializationData($aData['service']['spec_id']);
        $aData['spec']['link'] = static::url('price', array('keyword' => $aData['spec']['keyword']));
        $referer = Request::referer();
        if (mb_strpos($referer, static::url('price')) !== false) {
            $aData['spec']['link'] = $referer;
        }

        # SEO:
        $seoRegion = Geo::regionData(
            ( ! empty($filter['reg3_city']) ? $filter['reg3_city'] :
                ( ! empty($filter['reg2_region']) ? $filter['reg2_region'] : 0 ) ) );
        $seoCountry = ! empty($seoRegion['country']) ? $seoRegion['country'] : ! empty($filter['reg1_country']) ? $filter['reg1_country'] : 0;
        $canonical = array('page' => $f['page']);
        if ( ! Geo::filterEnabled()) {
            if($f['c'])  $canonical['c'] = $f['c'];
            if($f['ct']) $canonical['ct'] = $f['ct'];
            if($f['r'])  $canonical['r'] = $f['r'];
        }
        $seoData = array(
            'page' => $f['page'],
            'title' => $aData['service']['title'],
            'region' => ( ! empty($seoRegion) ? $seoRegion : '' ),
            'country' => $seoCountry ? Geo::regionData($seoCountry) : '',
        );
        $this->seo()->canonicalUrl(static::url('price.users', array('keyword' => $aData['service']['keyword']), true), $canonical);
        $this->setMeta('price-service', $seoData, $aData['service']);
        $this->seo()->setText($seoData, $f['page']);

        return $this->viewPHP($aData, 'price.users');
    }

    protected function servicesPriceForm(array &$aData)
    {
        $aData['specs'] = Specializations::model()->specializationsInAllCategories(
            array('id', 'keyword', 'title'), array('id', 'keyword', 'title'), true
        );
        if ($aData['specs']) {
            # формируем URL + помечаем активную категорию / специализацию
            $url = static::url('price');
            foreach ($aData['specs'] as &$v) {
                $v['url'] = $url.$v['keyword'].'/';
                if ( ! empty($v['specs'])) {
                    foreach ($v['specs'] as &$vv) {
                        $vv['url'] = $v['url'].$vv['keyword'].'/';
                        $vv['a'] = ( $aData['spec_id'] == $vv['id'] );
                    } unset($vv);
                    $v['a'] = ( $aData['cat_id'] == $v['id'] );
                } else {
                    $v['a'] = ( $aData['spec_id'] == $v['id'] );
                }
            } unset($v);
        }
        $aData['region_title'] = $aData['f']['ct'] ? Geo::i()->regionTitle($aData['f']['ct']) : '';

        return $this->viewPHP($aData, 'price.form');
    }


    /**
     * Данные для карты сайта
     * @return array
     */
    public function getSitemapData()
    {
        $aSpecs = Specializations::model()->specializationsInAllCategories(
            array('id','keyword','title'), array('id','keyword','title'), true
        );
        $url = static::url('list');
        if ($aSpecs) {
            # формируем URL + помечаем активную категорию / специализацию
            foreach ($aSpecs as &$v) {
                $v['url'] = $url.$v['keyword'].'/';
                if ( ! empty($v['specs'])) {
                    foreach ($v['specs'] as &$vv) {
                        $vv['url'] = $v['url'].$vv['keyword'].'/';
                    } unset($vv);
                }
            } unset($v);
        }

        $aSpecs = array_map(function($i) {
            if (isset($i['specs'])) {
                $i['sub'] = $i['specs'];
            }
            unset($i['specs']);
            return $i;
        }, $aSpecs);

        return array(
            'cats' => $aSpecs,
            'link' => $url,
        );
    }

    # ------------------------------------------------------------------------------------------------------------------------------
    # Услуги

    /**
     * Активация услуги "Аккаунт PRO"
     * @return string HTML
     */
    public function svc_pro()
    {
        $aSvc = Svc::model()->svcData(static::SVC_PRO);
        if (empty($aSvc)) $this->errors->error404();
        bff::setMeta(($svcTitle = $aSvc['title_view'][LNG]));

        $nUserID = User::id();
        if ( ! $nUserID || ($nUserID && static::useClient() && ! User::isWorker())) {
            return $this->showForbidden('', _t('svc', 'Услуга [title] доступна только исполнителям', array('title'=>'<strong>'.$svcTitle.'</strong>')));
        }
        if ( ! $aSvc['on']) {
            return $this->showForbidden('', _t('svc', 'Услуга [title] отключена', array('title'=>'<strong>'.$svcTitle.'</strong>')));
        }

        if (Request::isAJAX()) {
            do {
                $aResponse = array();

                if (!$this->security->validateToken()) {
                    $this->errors->reloadPage();
                    break;
                }

                $nMonth = $this->input->post('m', TYPE_UINT);

                if ( ! $nMonth) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! array_key_exists($nMonth, $aSvc['mass'])) {
                    $this->errors->reloadPage();
                    break;
                }
                $aSvcSettings = array(
                    'module' => $this->module_name,
                    'month'  => $nMonth,
                );

                $nPrice = $aSvc['mass'][$nMonth];
                $nBalance = User::data('balance');

                if ($nPrice <= $nBalance) {
                    Svc::i()->activate($this->module_name, static::SVC_PRO, array(), $nUserID, $nUserID, $nPrice, false, $aSvcSettings);
                } else {
                    $aResponse['redirect'] = Bills::url('my.pay', array('amount' => $nPrice - $nBalance));
                }

            } while (false);
            $this->ajaxResponseForm($aResponse);
        }

        $aData = array();

        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => _t('users', 'Платные сервисы'), 'link' => Svc::url('list')),
            array('title' => $svcTitle, 'active' => true),
        );

        $aData['svc'] = $aSvc;
        $aData['user'] = User::data(array('type','login','name','surname','pro','avatar','verified','sex','balance','pro_expire'), true);
        $aData['user']['id'] = $nUserID;
        return $this->viewPHP($aData, 'svc.pro');
    }

    /**
     * Активация услуги "Карусель"
     * @return string HTML
     */
    public function svc_carousel()
    {
        $aSvc = Svc::model()->svcData(static::SVC_CAROUSEL);
        if (empty($aSvc)) $this->errors->error404();
        bff::setMeta(($svcTitle = $aSvc['title_view'][LNG]));

        $nUserID = User::id();
        if ( ! $nUserID || ($nUserID && static::useClient() && ! User::isWorker())) {
            return $this->showForbidden('', _t('svc', 'Услуга [title] доступна только исполнителям', array('title'=>'<strong>'.$svcTitle.'</strong>')));
        }
        if ( ! $aSvc['on']) {
            return $this->showForbidden('', _t('svc', 'Услуга [title] отключена', array('title'=>'<strong>'.$svcTitle.'</strong>')));
        }

        if (Request::isAJAX()) {
            do {
                $aResponse = array();

                if (!$this->security->validateToken()) {
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->input->postm(array(
                    'title'   => array(TYPE_NOTAGS, 'len' => 50, 'len.sys' => 'users.carousel.title.limit'),
                    'message' => array(TYPE_TEXT, 'len' => 300, 'len.sys' => 'users.carousel.message.limit'),
                ));

                if (strlen($aData['message']) < 3) {
                    $this->errors->set(_t('svc', 'Текст слишком короткий'), 'message');
                }
                if ( ! $this->errors->no()) {
                    break;
                }

                $this->model->svcUserSettingsSave($nUserID, static::SVC_CAROUSEL, $aData);

                $nBalance = User::data('balance');
                if ($aSvc['price'] <= $nBalance) {
                    Svc::i()->activate($this->module_name, static::SVC_CAROUSEL, $aSvc, $nUserID, $nUserID);
                    $aResponse['redirect'] = bff::urlBase();
                } else {
                    $aResponse['redirect'] = Bills::url('my.pay', array('amount' => $aSvc['price'] - $nBalance));
                }

            } while (false);
            $this->ajaxResponseForm($aResponse);
        }

        # если пользователь уже заказывал услугу - подставим прошлый текст
        $aData = $this->model->svcUserSettings($nUserID, static::SVC_CAROUSEL);

        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => _t('users', 'Платные сервисы'), 'link' => Svc::url('list')),
            array('title' => $svcTitle, 'active' => true),
        );

        $aData['svc'] = $aSvc;
        $aData['user'] = User::data(array('type','login','name','surname','pro','avatar','verified','sex','balance'));
        $aData['user']['id'] = $nUserID;
        $aData['list'] = $this->model->svcUsersCarousel($aSvc['amount'] + 4);

        $this->prepareCarouselUsersList($aData['list']);

        return $this->viewPHP($aData, 'svc.carousel');
    }

    /**
     * Активация услуги "Лестница"
     * @return string HTML
     */
    public function svc_stairs()
    {
        $aSvc = Svc::model()->svcData(static::SVC_STAIRS);
        if (empty($aSvc)) $this->errors->error404();
        bff::setMeta(($svcTitle = $aSvc['title_view'][LNG]));

        $nUserID = User::id();
        if ( ! $nUserID || ($nUserID && static::useClient() && ! User::isWorker())) {
            return $this->showForbidden('', _t('svc', 'Услуга [title] доступна только исполнителям', array('title'=>'<strong>'.$svcTitle.'</strong>')));
        }
        if ( ! $aSvc['on']) {
            return $this->showForbidden('', _t('svc', 'Услуга [title] отключена', array('title'=>'<strong>'.$svcTitle.'</strong>')));
        }

        if ($this->input->get('up', TYPE_UINT)) {
            return $this->svc_stairs_up($nUserID, $aSvc);
        }

        if (Request::isAJAX()) {
            do {
                $aResponse = array();

                if (!$this->security->validateToken()) {
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->input->postm(array(
                    'weeks'       => TYPE_ARRAY_UINT,
                    'cat'         => TYPE_ARRAY_UINT,
                    'spec'        => TYPE_ARRAY_INT,
                    'title'       => TYPE_ARRAY_NOTAGS,
                    'message'     => TYPE_ARRAY_NOTAGS,
                    'description' => TYPE_ARRAY_NOTAGS,
                ));

                extract($aData, EXTR_REFS);

                $sum = 0;
                foreach ($weeks as $k => $v) {
                    if ( ! $v) continue;
                    if ( ! empty($title[$k]) && ! empty($message[$k])) {
                        $aSave = array(
                            'title'       => $title[$k],
                            'message'     => $message[$k],
                            'description' => $description[$k],
                        );
                        $this->model->svcUserSettingsSave($nUserID, static::SVC_STAIRS, $aSave);
                    }
                    $pr = $this->svcStairsPrice($spec[$k], $cat[$k], $aSvc);
                    if ( ! $pr) continue;
                    $sum += $v * $pr;
                }

                $nBalance = User::data('balance');
                if ($sum <= $nBalance) {
                    $notifySections = array();
                    foreach ($weeks as $k => $v) {
                        if ( ! $v) continue;
                        $pr = $this->svcStairsPrice($spec[$k], $cat[$k], $aSvc);
                        if ( ! $pr) continue;

                        $aSvcSettings = array(
                            'module'      => $this->module_name,
                            'weeks'       => $v,
                            'spec'        => $spec[$k],
                            'cat'         => $cat[$k],
                            'title'       => ! empty($title[$k]) ? $title[$k] : '',
                            'message'     => ! empty($message[$k]) ? $message[$k] : '',
                            'description' => ! empty($description[$k]) ? $description[$k] : '',
                        );
                        $nPrice = $pr * $v;
                        if (Svc::i()->activate($this->module_name, static::SVC_STAIRS, array(), $nUserID, $nUserID, $nPrice, false, $aSvcSettings)) {
                            $notifySections[] = $this->svcStairsTitle($spec[$k], $cat[$k]);
                        }
                    }
                    # Отправим уведомление пользователю
                    if ( ! empty($notifySections)) {
                        Users::sendMailTemplateToUser($nUserID, 'users_stairs', array(
                            'section' => join('; ', $notifySections)
                        ), Users::ENOTIFY_GENERAL);
                    }
                } else {
                    $aResponse['redirect'] = Bills::url('my.pay', array('amount' => $sum - $nBalance));
                }

            } while (false);
            $this->ajaxResponseForm($aResponse);
        }

        # если пользователь уже заказывал услугу - подставим прошлый текст
        $aData = $this->model->svcUserSettings($nUserID, static::SVC_STAIRS);

        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => _t('users', 'Платные сервисы'), 'link' => Svc::url('list')),
            array('title' => $svcTitle, 'active' => true),
        );

        $aData['svc'] = $aSvc;
        $aData['user'] = $this->model->userData($nUserID, [
            'type',
            'login',
            'name',
            'surname',
            'pro',
            'avatar',
            'verified',
            'sex',
            'balance',
            'spec_id',
            'reg1_country',
            'reg2_region',
            'reg3_city',
            'addr_addr',
            'district_id',
        ]);
        $aData['user']['id'] = $nUserID;
        $this->prepareStairsUserData($aData['user']);
        $aData['cats'] = Specializations::model()->specializationsInAllCategories(array('id','title'), array('id','title'));

        $aData['cats'] = static::aSvcStairs() + $aData['cats'];
        $aData['list'] = array('spec' => array(), 'cat' => array());
        $aList = $this->model->svcUsersStairs(array('user_id' => $nUserID));
        foreach ($aList as $v) {
            if ($v['spec_id'] != 0) {
                $aData['list']['spec'][ $v['spec_id'] ] = $v;
            } else {
                $aData['list']['cat'][ $v['cat_id'] ] = $v;
            }
        }

        $aData['spec_id'] = $this->input->get('spec_id', TYPE_INT);
        $aData['cat_id'] = $this->input->get('cat_id', TYPE_UINT);
        return $this->viewPHP($aData, 'svc.stairs');
    }

    /**
     * Активация поднятия для услуги "Лестница"
     * @return string HTML
     */
    public function svc_stairs_up($nUserID = 0, $aSvc = array())
    {
        if ( ! $nUserID) $nUserID = User::id();
        if (empty($aSvc)) {
            $aSvc = Svc::model()->svcData(static::SVC_STAIRS);
            if ( ! $aSvc['on']) {
                return '';
            }
        }

        $nSpecID = $this->input->postget('spec_id', TYPE_INT);
        $nCatID = $this->input->postget('cat_id', TYPE_UINT);

        if (Request::isAJAX()) {
            do {
                $aResponse = array();

                if (!$this->security->validateToken()) {
                    $this->errors->reloadPage();
                    break;
                }

                $sum = $this->input->post('sum', TYPE_NUM);
                if ($sum <= 0) {
                    $this->errors->reloadPage();
                    break;
                }

                $nBalance = User::data('balance');
                if ($sum <= $nBalance) {
                    $aData = $this->model->svcUsersStairs(array(
                        'user_id' => $nUserID,
                        'cat_id'  => $nCatID,
                        'spec_id' => $nSpecID,
                    ));
                    if (empty($aData)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $aSvcSettings = array(
                        'module'  => $this->module_name,
                        'sum'     => $sum,
                        'spec'    => $nSpecID,
                        'cat'     => $nCatID,
                    );
                    Svc::i()->activate($this->module_name, static::SVC_STAIRS, array(), $nUserID, $nUserID, $sum, false, $aSvcSettings);
                } else {
                    $aResponse['redirect'] = Bills::url('my.pay', array('amount' => $sum - $nBalance));
                }

            } while (false);
            $this->ajaxResponseForm($aResponse);
        }

        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => _t('users', 'Платные сервисы'), 'link' => Svc::url('list')),
            array('title' => _t('svc', 'Выбор позиции в лестнице'), 'active' => true),
        );

        $aData['svc'] = $aSvc;
        $aData['cats'] = Specializations::model()->specializationsInAllCategories(array('id','title'), array('id','title'));
        $aData['cats'] = static::aSvcStairs() + $aData['cats'];
        $aList = $this->model->svcUsersStairs(array('user_id' => $nUserID), false, '', 'spec_id, cat_id');

        $aListSort = array();
        foreach ($aList as $v) {
            if ($v['spec_id'] != 0) {
                $aListSort['spec'][ $v['spec_id'] ] = $v;
            } else {
                $aListSort['cat'][ $v['cat_id'] ] = $v;
            }
        }
        $aCurrent = array();
        if ($nSpecID != 0) {
            if (isset($aListSort['spec'][ $nSpecID ])) {
                $aCurrent = $aListSort['spec'][ $nSpecID ];
            }
        } else {
            if ($nCatID != 0) {
                if (isset($aListSort['cat'][$nCatID])) {
                    $aCurrent = $aListSort['cat'][$nCatID];
                }
            }
        }
        if (empty($aCurrent)) {
            $aCurrent = reset($aList);
        }

        foreach ($aData['cats'] as $k => $v) {
            if ($v['id'] < 0) {
                if (isset($aListSort['spec'][$v['id']])) {
                    $aData['cats'][$k]['expire'] = $aListSort['spec'][$v['id']]['expire'];
                } else {
                    unset($aData['cats'][$k]);
                }
            } else {
                if ( ! empty($v['specs'])) {
                    foreach ($v['specs'] as $kk => $vv) {
                        if (isset($aListSort['spec'][$vv['id']])) {
                            $aData['cats'][$k]['specs'][$kk]['expire'] = $aListSort['spec'][$vv['id']]['expire'];
                        } else {
                            unset($aData['cats'][$k]['specs'][$kk]);
                        }
                    }
                }
            }
        }
        foreach ($aData['cats'] as $k => $v) {
            if ($v['id'] > 0) {
                if (isset($aListSort['cat'][$v['id']])) {
                    $aData['cats'][$k]['expire'] = $aListSort['cat'][$v['id']]['expire'];
                } else {
                    if (empty($v['specs'])) {
                        unset($aData['cats'][$k]);
                    }
                }
            }
        }

        if ($aCurrent['spec_id'] != 0) {
            $aFilter = array('spec_id' => $aCurrent['spec_id']);
        } else {
            $aFilter = array('cat_id' => $aCurrent['cat_id']);
        }
        $aData['current'] = $aCurrent;
        $aData['balance'] = User::data('balance');

        $aData['list'] = $this->model->svcUsersStairs($aFilter);
        $aData['list'] = $this->viewPHP($aData, 'svc.stairs.up.list');

        return $this->viewPHP($aData, 'svc.stairs.up');
    }

    /**
     * Блок пользователей заказавших услугу "Карусель"
     * @return string HTML
     */
    public function svc_carousel_block()
    {
        if ( ! bff::servicesEnabled()) {
            return '';
        }

        $aSvc = Svc::model()->svcData(static::SVC_CAROUSEL);
        if ( ! $aSvc['on']) {
            return '';
        }

        $aData = array(
            'svc'  => $aSvc,
            'list' => $this->model->svcUsersCarousel($aSvc['amount']),
        );

        $this->prepareCarouselUsersList($aData['list']);
        return $this->viewPHP($aData, 'svc.carousel.block');
    }

    public function svc_carousel_block_orders()
    {
        if ( ! bff::servicesEnabled()) {
            return '';
        }

        $aSvc = Svc::model()->svcData(static::SVC_CAROUSEL);
        if ( ! $aSvc['on']) {
            return '';
        }

        $aData = array(
            'svc'  => $aSvc,
            'list' => $this->model->svcUsersCarousel($aSvc['amount']),
        );

        $this->prepareCarouselUsersList($aData['list']);
        return $this->viewPHP($aData, 'svc.carousel.block.orders');
    }

    /**
     * Блок пользователей заказавших услугу "Лестница"
     * @param int $nSpecID ID специализации
     * @param int $nCatID ID категории
     * @return string HTML
     */
    public function svc_stairs_block($nSpecID = 0, $nCatID = 0)
    {
        if ( ! bff::servicesEnabled()) {
            return '';
        }

        if ( ! $nSpecID && ! $nCatID) return '';
        $aSvc = Svc::model()->svcData(static::SVC_STAIRS);
        if ( ! $aSvc['on']) {
            return '';
        }
        if ($nSpecID) {
            $aFilter = array('spec_id' => $nSpecID);
        } else {
            $aFilter = array('cat_id' => $nCatID);
        }

        $aData = array(
            'cat_id'  => $nCatID,
            'spec_id' => $nSpecID,
            'svc'     => $aSvc,
            );
        $aData['list'] = $this->model->svcUsersStairs($aFilter);
        foreach ($aData['list'] as &$v){
            $v['spec_data'] = Specializations::model()->specializationData($v['spec_id']);
            $v['contacts_phones'] = $this->getUserContacts($v['user_id'], true);
        }unset($v);
        return $this->viewPHP($aData, 'svc.stairs.block');
    }

    /**
     * Добавим рейтинг за потраченные деньги на платные услуги
     * @param integer $nUserID ID пользователя
     * @param integer $nPrice потраченная сумма
     */
    public function svc_rating_money($nUserID, $nPrice)
    {
        if ($nUserID <=0 || $nPrice <= 0) return;

        $sett = strval(config::sys('users.rating.money', '0=0'));
        if (empty($sett)) return;
        $sett = explode('=', $sett);
        if (empty($sett[0]) || empty($sett[1])) return;

        /* Рейтинг за потраченные деньги на платные услуги, Деньги=Рейтинг (0=0 - не влияет на рейтинг)
             1=1   - 1 деньга   = +1 к рейтингу
             1=2   - 1 деньга   = +2 к рейтингу
             2=1   - 2 деньги   = +1 к рейтингу
           2.8=1.2 - 2.8 деньги = +1 к рейтингу
         */
        $fMultiplier = floatval($sett[1]) / floatval($sett[0]);
        $nRating = round($nPrice * $fMultiplier);
        if ($nRating > 0) {
            $this->model->ratingChange($nUserID, $nRating, 'users-money', array('money'=>$nPrice));
        }
    }

    public function prepareCarouselUsersList(&$aList)
    {
        foreach ($aList as &$user){
            $this->prepareStairsUserData($user);
        }unset($user);
    }

    public function prepareStairsUserData(&$data)
    {
        $data['spec_data'] = Specializations::model()->specializationData($data['spec_id']);
        $data['district_title'] = Geo::districtTitle($data['district_id']);

        if (!empty($data['reg3_city'])) {
            $data['region_data'] = Geo::regionData($data['reg3_city'], null);
            return;
        } elseif (!empty($data['reg2_region'])) {
            $data['region_data'] = Geo::regionData($data['reg2_region'], null);
            return;
        }
        if (!empty($data['reg1_country'])) {
            $data['region_data'] = Geo::regionData($data['reg1_country'], null);
            return;
        }
    }

    public function redirectToProfile()
    {
        $this->redirect(self::url('my.settings'));
    }

    /**
     * Перенаправление в кабинет пользователя
     */
    public function redirectToProfilePage()
    {
        $this->redirect(self::url('my.profile'));
    }

    public function cron()
    {
        if (!bff::cron()) {
            return;
        }

        # Удаляем неактивированные аккаунты пользователей (с просроченным периодом активации)
        $this->model->cronDeleteUnactivated();
        # Удаляем неактивированные заказы (с просроченным периодом активации)
        Orders::i()->ordersDeleteUnactivated();
        # Сбрасываем статистику ежедневных просмотров
        $this->model->cronResetViews();
        # Удаляем старые документы для проверки
        $this->verifiedImages()->deleteExpire();
        return;
    }

    /**
     * Пересчет позиций пользователей в специализациях
     * Рекомендуемый период: раз в час
     */
    public function cronSpecsPositions()
    {
        if (!bff::cron()) {
            return;
        }
        $this->model->cronCalcSpecsPositions();
        if (Specializations::useServices()) {
            $this->model->cronCalcPrice();
        }
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {

        return array(
            'cron' => array('period' => '10 0 * * *'),
            'cronSpecsPositions' => array('period' => '0 * * * *'),
        );
    }

}