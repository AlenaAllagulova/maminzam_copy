<?php

/**
 * Права доступа группы:
 *  - users: Пользователи
 *      - profile: Ваш профиль
 *      - members-listing: Список пользователей
 *      - admins-listing: Список администраторов
 *      - users-edit: Управление пользователями
 *      - ban: Блокировка пользователей
 *      - groups-listing: Список групп пользователей
 *      - groups-edit: Управление группами пользователей
 *      - tags: Управление навыками
 *      - svc: Управление услугами
 *      - settings: Настройки
 *      - seo: SEO
 */
class Users_ extends UsersBase
{
    public function main()
    {
        $this->login();
    }

    public function profile()
    {
        if (!$this->haveAccessTo('profile')) {
            return $this->showAccessDenied();
        }

        $bChangeLogin = false;

        $nUserID = User::id();
        if (!$nUserID) {
            $this->adminRedirect(Errors::IMPOSSIBLE, 'login');
        }

        if (Request::isPOST()) {

            $sLogin = $this->input->post('login', TYPE_STR);
            $bChangePassword = $this->input->post('changepass', TYPE_BOOL);
            $bChangeLogin = ($bChangeLogin && !empty($sLogin));

            $aData = $this->model->userData($nUserID, array('password', 'password_salt'));

            $sql = array();
            if ($this->errors->no() && $bChangePassword) {
                $this->input->postm(array(
                        'password0' => TYPE_NOTRIM, # текущий пароль
                        'password1' => TYPE_NOTRIM, # новый пароль
                        'password2' => TYPE_NOTRIM, # подтверждение нового пароля
                    ), $p
                );
                extract($p);

                do {
                    if (!$password0) {
                        $this->errors->set(_t('users', 'Укажите текущий пароль'), 'password0');
                        break;
                    }

                    if ($aData['password'] != $this->security->getUserPasswordMD5($password0, $aData['password_salt'])) {
                        $this->errors->set(_t('users', 'Текущий пароль указан некорректно'), 'password0');
                        break;
                    }

                    if (empty($password1) || strlen($password1) < $this->passwordMinLength) {
                        $this->errors->set(_t('users', 'Новый пароль слишком короткий'), 'password1');
                        break;
                    }
                    if ($password1 !== $password2) {
                        $this->errors->set(_t('users', 'Ошибка подтверждения пароля'), 'password2');
                        break;
                    }
                    if ($this->errors->no()) {
                        $sql['password'] = $this->security->getUserPasswordMD5($password1, $aData['password_salt']);
                    }
                } while (false);
            }

            if ($this->errors->no() && $bChangeLogin) {
                do {
                    if (empty($sLogin)) {
                        $this->errors->set(_t('users', 'Укажите логин'), 'login');
                        break;
                    }
                    if (!$this->isLoginCorrect($sLogin)) {
                        $this->errors->set(_t('users', 'Пожалуйста для поля <strong>логин</strong> используйте символы A-Z,a-z,0-9,_'), 'login');
                        break;
                    }

                    if ($this->model->userLoginExists($sLogin, $nUserID, array('list', 'view'))) {
                        $this->errors->set(_t('users', 'Указанный логин уже существует, укажите другой'), 'login');
                        break;
                    }

                    $sql['login'] = $sLogin;
                } while (false);
            }

            if ($this->errors->no('users.admin.profile.submit',array('id'=>$nUserID,'data'=>&$sql)) && !empty($sql)) {
                $this->model->userSave($nUserID, $sql);
                $this->security->expire();
                $this->adminRedirect(Errors::SUCCESS, 'profile');
            }
        }

        $aUserGroups = $this->model->userGroups($nUserID);
        if (empty($aUserGroups)) {
            $aUserGroups = array();
        }

        $aData = User::data(array('name', 'login', 'avatar', 'email'));
        $aData['user_id'] = $nUserID;
        $aData['tuid'] = $this->makeTUID($nUserID);
        $aData['groups'] = $aUserGroups;
        $aData['changelogin'] = $bChangeLogin;

        return $this->viewPHP($aData, 'admin.profile');
    }

    public function logout()
    {
        $this->security->sessionDestroy($this->adminLink('login'));
    }

    public function login()
    {
        if ($this->security->haveAccessToAdminPanel()) {
            $this->adminRedirect(null, 'profile');
        }

        $sLogin = '';
        if (Request::isPOST()) {
            $sLogin = $this->input->post('login', TYPE_STR);
            if (!$sLogin) {
                $this->errors->set('Укажите логин');
            }

            $sPassword = $this->input->post('password', TYPE_NOTRIM);
            if (!$sPassword) {
                $this->errors->set('Укажите пароль');
            }

            if ($this->errors->no('users.admin.login.step1')) {
                $this->isLoginOrEmail($sLogin, $isEmail);
                $aData = $this->model->userSessionData(array(($isEmail ? 'email' : 'login') => $sLogin));

                if (!empty($aData)) {
                    if ($aData['password'] != $this->security->getUserPasswordMD5($sPassword, $aData['password_salt'])) {
                        $aData = false;
                    }
                }

                if (!$aData) {
                    $this->errors->set('Логин или пароль были указаны некорректно');
                    $nUserID = 0;
                } else {
                    $nUserID = $aData['id'];

                    # аккаунт заблокирован
                    if ($aData['blocked'] == 1) {
                        $this->errors->set(sprintf('Акканут заблокирован: %s', $aData['blocked_reason']));
                    } # проверка IP в бан-листе
                    else {
                        if ($mBlocked = $this->checkBan(true)) {
                            $this->errors->set(_t('users', 'Доступ заблокирован по причине:<br />[reason]', array('reason' => $mBlocked)));
                        } # проверка доступа в админ-панель
                        else {
                            if (!$this->security->haveAccessToAdminPanel($nUserID)) {
                                $this->errors->accessDenied();
                            }
                        }
                    }
                }

                if ($this->errors->no('users.admin.login.step2', array('id'=>$nUserID,'data'=>&$aData))) {
                    $aUserGroups = $this->model->userGroups($nUserID, true);

                    # стартуем сессию администратора
                    $this->security->sessionStart();

                    # обновляем статистику
                    $this->model->userSave($nUserID, false, array(
                            'last_activity' => $this->db->now(),
                            'last_login2'   => $aData['last_login'],
                            'last_login'    => $this->db->now(),
                            'last_login_ip' => Request::remoteAddress(true),
                            'session_id'    => session_id(),
                        )
                    );

                    $this->security->setUserInfo($nUserID, $aUserGroups, $aData);

                    $referer = $this->input->post('ref', TYPE_NOTAGS);
                    if (!empty($referer) && $this->security->validateReferer($referer)) {
                        $this->redirect($referer);
                    }

                    $this->redirect($this->adminLink(null));
                }
            }
        }

        $aData = array('login' => $sLogin, 'errors' => $this->errors->get(true));
        echo $this->viewPHP($aData, 'login', TPL_PATH);
        exit;
    }

    //-------------------------------------------------------------------
    // users

    public function listing() # список пользователей
    {
        if (!$this->haveAccessTo('members-listing')) {
            return $this->showAccessDenied();
        }

        $f = $this->input->getm(array(
                'status' => TYPE_INT, # статус
                'q'      => TYPE_NOTAGS, #  поиск по ID/login/email
                'type'   => TYPE_UINT, # тип
                'country'=> TYPE_UINT, # страна
                'region' => TYPE_UINT, # город
                'r_from' => TYPE_NOTAGS, # регистрация: от
                'r_to'   => TYPE_NOTAGS, # регистрация: до
                'a_from' => TYPE_NOTAGS, # авторизация: от
                'a_to'   => TYPE_NOTAGS, # авторизация: до
                'page'   => TYPE_UINT, # страница
            )
        );

        $sqlFilter = array(':notSuperAdmin' => 'user_id != 1', 'admin' => 0);
        if ($f['status'] > 0) {
            switch ($f['status']) {
                case 1:
                    $sqlFilter['activated'] = 1;
                    break;
                case 2:
                    $sqlFilter['activated'] = 0;
                    break;
                case 3:
                    $sqlFilter['blocked'] = 1;
                    break;
                case 4:
                    $sqlFilter[':subscribed'] = 'enotify & ' . Users::ENOTIFY_NEWS;
                    $sqlFilter['blocked'] = 0;
                    break;
                case 5:
                    $sqlFilter['trusted'] = 1;
                    break;
                case 6:
                    $sqlFilter[':verified'] = 'verified > 0';
                    break;
            }
        }

        if ($f['q'] != '') {
            $sqlFilter[':q'] = array(
                '( user_id = :q_user_id OR login LIKE :q_login OR email LIKE :q_email OR phone_number LIKE :q_login )',
                ':q_user_id' => intval($f['q']),
                ':q_login'   => '%' . $f['q'] . '%',
                ':q_email'   => $f['q'] . '%',
            );
        }
        if ($f['country'] > 0) {
            $sqlFilter['reg1_country'] = $f['country'];
        }
        if ($f['region'] > 0) {
            $sqlFilter[':region'] = array('(reg2_region = :region OR reg3_city = :region)', ':region' => $f['region']);
        }
        if($f['type']){
            $sqlFilter['type'] = $f['type'];
        }

        # период регистрации
        if (!empty($f['r_from'])) {
            $r_from = strtotime($f['r_from']);
            if (!empty($r_from)) {
                $sqlFilter[':regFrom'] = array('created >= :regFrom', ':regFrom' => date('Y-m-d 00:00:00', $r_from));
            }
        }
        if (!empty($f['r_to'])) {
            $r_to = strtotime($f['r_to']);
            if (!empty($r_to)) {
                $sqlFilter[':regTo'] = array('created <= :regTo', ':regTo' => date('Y-m-d 23:59:59', $r_to));
            }
        }

        # период последней авторизации
        if (!empty($f['a_from'])) {
            $a_from = strtotime($f['a_from']);
            if (!empty($a_from)) {
                $sqlFilter[':authFrom'] = array('last_login >= :authFrom', ':authFrom' => date('Y-m-d 00:00:00', $a_from));
            }
        }
        if (!empty($f['a_to'])) {
            $a_to = strtotime($f['a_to']);
            if (!empty($a_to)) {
                $sqlFilter[':authTo'] = array('last_login <= :authTo', ':authTo' => date('Y-m-d 23:59:59', $a_to));
            }
        }

        $aOrdersAllowed = array('user_id' => 'asc', 'email' => 'asc', 'last_login' => 'desc', 'pro' => 'desc', 'verified' => 'desc');
        $defaultOrder = 'user_id' . tpl::ORDER_SEPARATOR . 'desc';
        if($f['status'] == 6) {
            $defaultOrder = 'verified' . tpl::ORDER_SEPARATOR . 'desc';
        }
        $aData = $this->prepareOrder($orderBy, $orderDirection, $defaultOrder, $aOrdersAllowed);

        if (!$f['page']) {
            $f['page'] = 1;
        }
        $aData['filter'] = '&' . http_build_query($f);

        $nTotal = $this->model->usersList($sqlFilter, array(), true);
        $oPgn = new Pagination($nTotal, 15, $this->adminLink("listing{$aData['filter']}&order=$orderBy-$orderDirection&page=" . Pagination::PAGE_ID));
        $aData['pgn'] = $oPgn->view();

        if ($orderBy == 'verified') {
            if ($orderDirection == 'desc') {
                $orderBy = 'FIELD(verified,'.static::VERIFIED_STATUS_WAIT.','.static::VERIFIED_STATUS_DECLINED.','.static::VERIFIED_STATUS_APPROVED.','.static::VERIFIED_STATUS_NONE.')';
                $orderDirection = '';
            } else {
                $orderBy = 'FIELD(verified,'.static::VERIFIED_STATUS_APPROVED.','.static::VERIFIED_STATUS_DECLINED.','.static::VERIFIED_STATUS_WAIT.','.static::VERIFIED_STATUS_NONE.')';
                $orderDirection = '';
            }
        }

        $aData['users'] = $this->model->usersList($sqlFilter, array(
                'user_id',
                'admin',
                'name',
                'login',
                'email',
                'last_login',
                'blocked',
                'activated',
                'pro',
                'verified',
            ), false, $oPgn->getLimitOffset(), "$orderBy $orderDirection"
        );

        foreach ($aData['users'] as &$v) {
            $v['tuid'] = $this->makeTUID($v['user_id']);
        }
        unset($v);

        $aData['f'] = $f;

        return $this->viewPHP($aData, 'admin.listing');
    }

    public function listing_moderators() # список модераторов
    {
        if (!$this->haveAccessTo('admins-listing')) {
            return $this->showAccessDenied();
        }

        $aOrdersAllowed = array('user_id' => 'desc', 'login' => 'asc', 'email' => 'asc');
        $aData = $this->prepareOrder($orderBy, $orderDirection, 'login' . tpl::ORDER_SEPARATOR . 'asc', $aOrdersAllowed);

        $nTotal = $this->model->usersList(array('admin' => 1), array(), true);
        $oPgn = new Pagination($nTotal, 20, $this->adminLink(__FUNCTION__ . "&order=$orderBy-$orderDirection&page=" . Pagination::PAGE_ID));
        $aData['pgn'] = $oPgn->view();

        $aData['users'] = $this->model->usersList(array('admin' => 1),
            array('user_id', 'login', 'email', 'password', 'blocked', 'deleted', 'activated'),
            false, $oPgn->getLimitOffset(), "$orderBy $orderDirection"
        );

        # получаем все группы с доступом в админ. панель
        $aUsersGroups = $this->model->usersGroups(true, 'user_id');

        foreach ($aData['users'] as $k => $v) {
            $aData['users'][$k]['tuid'] = $this->makeTUID($v['user_id']);
            $aData['users'][$k]['groups'] = array();
            if (isset($aUsersGroups[$v['user_id']])) {
                $aData['users'][$k]['groups'] = $aUsersGroups[$v['user_id']];
            }
        }

        $aData['page'] = $oPgn->getCurrentPage();

        return $this->viewPHP($aData, 'admin.listing.moderators');
    }

    public function user_add()
    {
        if (!$this->haveAccessTo('users-edit')) {
            return $this->showAccessDenied();
        }

        $this->validateUserData($aData, 0);

        if (Request::isPOST()) {
            $aResponse = array();
            $aGroupID = $aData['group_id'];
            do {

                if (!$this->errors->no()) {
                    break;
                }

                $aData['member'] = (in_array(self::GROUPID_MEMBER, $aGroupID) ? 1 : 0);
                $aData['password_salt'] = $this->security->generatePasswordSalt();
                $aData['password'] = $this->security->getUserPasswordMD5($aData['password'], $aData['password_salt']);
                $aData['activated'] = 1;

                unset($aData['group_id']);

                if (empty($aGroupID)) {
                    $aGroupID = array(self::GROUPID_MEMBER);
                }

                if (!$this->errors->no('users.admin.user.submit', array('id'=>0,'data'=>&$aData,'groups'=>&$aGroupID))) {
                    break;
                }

                $nUserID = $this->model->userCreate($aData, $aGroupID);
                if (!$nUserID) {
                    $this->errors->impossible();
                    break;
                }
                $aResponse['redirect'] = 1;

                # теги
                $this->userTags()->tagsSave($nUserID);

                $sqlUpdate = array();

                # загружаем аватар
                $aAvatar = $this->avatar($nUserID)->uploadFILES('avatar', false, false);
                if ($aAvatar !== false && !empty($aAvatar['filename'])) {
                    $sqlUpdate['avatar'] = $aAvatar['filename'];
                }

                # обновляем, является ли пользователь администратором
                $bIsAdmin = 0;
                if (count($aGroupID) == 1 && current($aGroupID) == self::GROUPID_MEMBER) {
                    $bIsAdmin = 0;
                } else {
                    if (in_array(self::GROUPID_SUPERADMIN, $aGroupID) ||
                        in_array(self::GROUPID_MODERATOR, $aGroupID)
                    ) {
                        $bIsAdmin = 1;
                    } else {
                        $aUserGroups = $this->model->groups();
                        foreach ($aUserGroups as $v) {
                            if ($v['adminpanel'] == 1) {
                                $bIsAdmin = 1;
                                break;
                            }
                        }
                    }
                }

                if ($bIsAdmin) {
                    $sqlUpdate['admin'] = $bIsAdmin;
                }

                if (!empty($sqlUpdate)) {
                    $this->model->userSave($nUserID, $sqlUpdate);
                }
            } while (false);

            $this->iframeResponseForm($aResponse);
        }

        $aData = array_merge($aData, array('password' => '', 'password2' => '', 'user_id' => '', 'avatar' => ''));
        $aActiveGroupsID = array(self::GROUPID_MEMBER);
        $this->prepareGroupsOptions($aData, array(USERS_GROUPS_SUPERADMIN), $aActiveGroupsID);

        $aData['phones'] = array(); # телефоны
        $aData['user_id'] = 0;
        $aData['rating'] = 0;
        $aData['city_data'] = array('title' => '');
        $aData['city_metro'] = Geo::cityMetro();
        if(Geo::defaultCity()){
            $aData['reg3_city'] = Geo::defaultCity();
            $aData['city_data'] = Geo::regionData(Geo::defaultCity());
            $aData['city_metro'] = Geo::cityMetro(Geo::defaultCity());
            $aData['reg1_country'] = $aData['city_data']['country']? $aData['city_data']['country']: Geo::defaultCountry();
        }else{
            $aData['reg1_country'] = Geo::defaultCountry()? Geo::defaultCountry(): 0;
        }
        $aData['dopSpecsOptions'] = Specializations::model()->specializationsOptions(0, 0, array(
                'empty'     => 'Выбрать',
                'emptyCats' => array('empty' => 'Выбрать'),
                'noAllSpec' => 1
            )
        );
        Geo::mapsAPI(true);
        if (Users::verifiedEnabled()) {
            $aData['verified_reason'] = array('id'=>0, 'cnt'=>0, 'message'=>'');
        }

        return $this->viewPHP($aData, 'admin.user.form');
    }

    public function user_edit()
    {
        if (!$this->haveAccessTo('users-edit')) {
            return $this->showAccessDenied();
        }

        if (!($nUserID = $this->input->get('rec', TYPE_UINT))) {
            $this->adminRedirect(Errors::IMPOSSIBLE, 'listing');
        }

        $sTUID = $this->input->get('tuid');
        if (!$this->checkTUID($sTUID, $nUserID)) {
            return $this->showAccessDenied();
        }

        $aData = array('admin' => 0);
        $verifiedEnabled = static::verifiedEnabled();

        # анализируем группы, в которые входит пользователь
        $bUserSuperadmin = 0;
        $aUserGroups = $this->model->userGroups($nUserID);
        foreach ($aUserGroups as $v) {
            if ($v['group_id'] == self::GROUPID_SUPERADMIN) {
                $bUserSuperadmin = 1;
            }
            if ($v['adminpanel'] == 1) {
                $aData['admin'] = 1;
            }
        }

        if (Request::isPOST()) {

            $aResponse = array('reload' => false);
            $this->validateUserData($aData, $nUserID);

            if (!$aData['admin']) { # удаляем настройки предназначенные для админов
                unset($aData['im_noreply']);
            }

            $aGroupID = $aData['group_id'];

            do {
                if (!$this->errors->no()) {
                    break;
                }

                # верификация:
                if ($verifiedEnabled) {
                    $oldData = $this->model->userData($nUserID, array('verified'));
                    if ($aData['verified'] == static::VERIFIED_STATUS_DECLINED) {
                        $verifiedReason = $this->input->post('verified_reason', TYPE_ARRAY);
                        $reason = $this->input->clean_array($verifiedReason, array(
                            'id'        => TYPE_INT,
                            'message'   => TYPE_STR,
                            'cnt'       => TYPE_UINT,
                        ));
                        $aData['verified_reason'] = serialize($reason);
                    } else {
                        $aData['verified_reason'] = '';
                    }

                    $deleted = $this->input->post('verified_deleted', TYPE_ARRAY_STR);
                    if ( ! empty($deleted)) {
                        $images = $this->verifiedImages($nUserID);
                        $images->deleteImages($deleted);
                    }
                }

                # теги
                $this->userTags()->tagsSave($nUserID);

                # основный данные
                unset($aData['group_id']);
                $aData['member'] = in_array(self::GROUPID_MEMBER, $aGroupID) ? 1 : 0;
                if (!$this->errors->no('users.admin.user.submit', array('id'=>$nUserID,'data'=>&$aData))) {
                    break;
                }
                $this->model->userSave($nUserID, $aData);

                # верификация:
                if ($verifiedEnabled)
                {
                    # обновим счетчик ожидающих проверки верификации
                    $this->verifiedCounterUpdate();

                    # отправим уведомление пользователю
                    if ($oldData['verified'] != $aData['verified']) {
                        if ($aData['verified'] == static::VERIFIED_STATUS_DECLINED) {
                            $reasonMessage = '';
                            if ($reason['id'] > 0) {
                                $verifiedReasons = static::verifiedReasons();
                                if (isset($verifiedReasons[ $reason['id'] ])) {
                                    $reasonMessage = $verifiedReasons[ $reason['id'] ]['t'];
                                }
                            } else {
                                if (isset($reason['message'])) {
                                    $reasonMessage = $reason['message'];
                                }
                            }
                            Users::sendMailTemplateToUser($nUserID, 'users_verified_declined', array(
                                'reason' => $reasonMessage,
                            ));
                        } else if($aData['verified'] == static::VERIFIED_STATUS_APPROVED) {
                            Users::sendMailTemplateToUser($nUserID, 'users_verified_approved', array());
                        }
                    }
                }

                $sqlUpdate = array();

                # аватар
                $aAvatar = $this->avatar($nUserID)->uploadFILES('avatar', true, false);
                if ($aAvatar !== false && !empty($aAvatar['filename'])) {
                    $sqlUpdate['avatar'] = $aAvatar['filename'];
                    $aResponse['reload'] = true;
                } else {
                    if ($this->input->postget('avatar_del', TYPE_BOOL)) {
                        if ($this->avatar($nUserID)->delete(false)) {
                            $sqlUpdate['avatar'] = '';
                            $aResponse['reload'] = true;
                        }
                    }
                }

                # связь с группами
                if ($bUserSuperadmin && !in_array(self::GROUPID_SUPERADMIN, $aGroupID)) {
                    $aGroupID = array_merge($aGroupID, array(self::GROUPID_SUPERADMIN));
                }
                $this->model->userToGroups($nUserID, $aGroupID);

                # обновляем, является ли пользователь администратором
                $bIsAdmin = 0;
                if ($this->errors->no()) {
                    if ($bUserSuperadmin || in_array(self::GROUPID_MODERATOR, $aGroupID)) {
                        $bIsAdmin = 1;
                    } elseif (count($aGroupID) == 1 && current($aGroupID) == self::GROUPID_MEMBER) {
                        $bIsAdmin = 0;
                    } else {
                        $aUserGroups = $this->model->userGroups($nUserID);
                        foreach ($aUserGroups as $v) {
                            if ($v['adminpanel'] == 1) {
                                $bIsAdmin = 1;
                                break;
                            }
                        }
                    }

                    if ($aData['admin'] != $bIsAdmin) {
                        $sqlUpdate['admin'] = $bIsAdmin;
                        if (!$bIsAdmin) {
                            $sqlUpdate['im_noreply'] = 0;
                        }
                    }
                }

                if (!empty($sqlUpdate)) {
                    $this->model->userSave($nUserID, $sqlUpdate);
                }

                # если пользователь редактирует собственные настройки
                if ($this->security->isCurrentUser($nUserID)) {
                    $this->security->expire();
                }

            } while (false);

            $this->iframeResponseForm($aResponse);
        }

        $aUserInfo = $this->model->userData($nUserID, '*');
        $aData = HTML::escape(array_merge($aUserInfo, $aData), 'html', array('name', 'surname'), true);

        $aActiveGroupsID = array();
        for ($j = 0; $j < count($aUserGroups); $j++) {
            $aActiveGroupsID[] = $aUserGroups[$j]['group_id'];
        }
        $this->prepareGroupsOptions($aData, ($bUserSuperadmin ? null : USERS_GROUPS_SUPERADMIN), $aActiveGroupsID);

        $aData['superadmin'] = $bUserSuperadmin;
        $aData['tuid'] = $sTUID;
        # страна, город, метро
        if(Geo::defaultCity() && empty($aData['reg3_city'])){
            $aData['reg3_city'] = Geo::defaultCity();
            $aData['city_data'] = Geo::regionData($aData['reg3_city']);
            $aData['city_metro'] = Geo::cityMetro($aData['reg3_city']);
            $aData['reg1_country'] = $aData['city_data']['country']? $aData['city_data']['country']: Geo::defaultCountry();
        }
        $aData['country_data'] = Geo::regionData($aData['reg1_country']);
        $aData['city_data'] = Geo::regionData($aData['reg3_city']);
        $aData['city_metro'] = Geo::cityMetro($aData['reg3_city'], $aData['metro_id'], true);

        $aData['dopSpecsOptions'] = Specializations::model()->specializationsOptions(0, 0, array(
                'empty'     => 'Выбрать',
                'emptyCats' => array('empty' => 'Выбрать'),
                'noAllSpec' => 1
            )
        );
        $oSpecs = Specializations::i();
        foreach ($aData['specs'] as &$v) {
            $v['dp'] = $oSpecs->dpForm($v['spec_id'], false, $v, 'd'.$v['spec_id']);
            $v['pr'] = $this->priceForm($v['spec_id'], $v);
            if (Specializations::useServices()) {
                $v['serv'] = $oSpecs->servicesForm($v['spec_id'], $aData['specsServices']);
            }
        } unset($v);

        # верификация:
        if ($verifiedEnabled) {
            $images = $this->verifiedImages($nUserID);
            $aData['verifiedImages'] = $images->getData();
            foreach ($aData['verifiedImages'] as &$v) {
                $v['link'] = $this->adminLink('ajax&act=verified-image-view&id='.$nUserID.'&image='.$v['id'].'&file='.$v['filename']);
            } unset($v);
            $aData['verified_reason'] = func::unserialize($aData['verified_reason'], array('id' => 0, 'message' => '', 'cnt' => 0));
        }


        $aData['frontendAuthUrl'] = static::urlBase().'/user/login_admin?user_id='.$aUserInfo['user_id'].'&hash='.$this->frontendAuthHash($aUserInfo['user_id'], $aUserInfo['email']);
        Geo::mapsAPI(true);
        return $this->viewPHP($aData, 'admin.user.form');
    }

    public function user_action()
    {
        if (!$this->haveAccessTo('users-edit')) {
            return $this->showAccessDenied();
        }

        if (!($nUserID = $this->input->getpost('rec', TYPE_UINT))) {
            $this->adminRedirect(Errors::IMPOSSIBLE);
        }

        $sTUID = $this->input->getpost('tuid');
        if (!$this->checkTUID($sTUID, $nUserID)) {
            return $this->showAccessDenied();
        }

        if ($this->model->userIsSuperAdmin($nUserID)) {
            $this->adminRedirect(Errors::ACCESSDENIED);
        }

        switch ($this->input->get('type')) {
            case 'delete': # удаление пользователя [нереализовано]
            {
                $this->adminRedirect(Errors::IMPOSSIBLE);

                # аватар
                $this->avatar($nUserID)->delete(false);
                # данные
                $this->model->deleteUser($nUserID);
            }
            break;
            case 'logout': # завершение сессии
            {

                $aData = $this->model->userData($nUserID, array('session_id'));
                $sUserSessionID = $aData['session_id'];
                if (!empty($sUserSessionID)) {
                    $this->security->impersonalizeSession($sUserSessionID, null, true, false);
                    $this->model->userSave($nUserID, false, array('session_id' => ''));

                    $this->adminRedirect(Errors::SUCCESS, "user_edit&rec=$nUserID&tuid=$sTUID");
                }

            }
            break;
        }

        $this->adminRedirect(Errors::SUCCESS);
    }

    public function ajax()
    {
        if (!$this->security->haveAccessToAdminPanel()) {
            return $this->showAccessDenied();
        }

        $nUserID = $this->input->getpost('id', TYPE_UINT);
        switch ($this->input->getpost('act', TYPE_STR)) {
            case 'user-info': # popup
            {
                $aData = $this->model->userData($nUserID, '*');
                $aData['region_title'] = Geo::regionTitle($aData['reg3_city']);
                $aData['tuid'] = $this->makeTUID($nUserID);
                $aData['sendmsg'] = ($this->security->isAdmin() || $aData['im_noreply'] == 0);
                echo $this->viewPHP($aData, 'admin.user.info');
                exit;
            }
            break;
            case 'user-block': # блокировка / разблокировка пользователя
            {
                if (!$this->haveAccessTo('users-edit')) {
                    $this->ajaxResponse(Errors::ACCESSDENIED);
                }
                if ($this->security->isCurrentUser($nUserID)) {
                    $this->ajaxResponse(Errors::IMPOSSIBLE);
                }

                $aData = $this->model->userData($nUserID, array('blocked', 'activated', 'email', 'name', 'session_id'));
                if (empty($aData)) {
                    $this->ajaxResponse(Errors::UNKNOWNRECORD);
                }

                $sBlockedReason = $this->input->postget('blocked_reason', TYPE_STR, array('len' => 1000, 'len.sys' => 'users.blocked_reason.limit'));
                $bBlocked = $this->input->postget('blocked', TYPE_BOOL);

                $aUpdate = array();
                if (!$bBlocked) {
                    $aUpdate['blocked'] = 0;
                } else {
                    $aUpdate['blocked'] = 1;
                    $aUpdate['blocked_reason'] = $sBlockedReason;
                }

                $bSaved = $this->model->userSave($nUserID, $aUpdate);
                if ($bSaved) {
                    if ($aUpdate['blocked'] != $aData['blocked']) {
                        # Триггер блокировки/разблокировки аккаунта
                        bff::i()->callModules('onUserBlocked', array($nUserID, $aUpdate['blocked']));

                        if ($aUpdate['blocked'] === 1) {
                            # аккаунт заблокирован
                            bff::sendMailTemplate(
                                array(
                                    'name'           => $aData['name'],
                                    'email'          => $aData['email'],
                                    'blocked_reason' => $sBlockedReason
                                ),
                                'users_blocked', $aData['email']
                            );

                            # убиваем сессию пользователя (если авторизован)
                            $this->security->impersonalizeSession($aData['session_id'], null, true, false);
                        } else {
                            # аккаунт разблокирован
                            bff::sendMailTemplate(
                                array(
                                    'name'  => $aData['name'],
                                    'email' => $aData['email']
                                ),
                                'users_unblocked', $aData['email']
                            );
                        }
                    }
                }

                $this->ajaxResponse(array('reason' => nl2br($sBlockedReason), 'blocked' => $aUpdate['blocked']));
            }
            break;
            case 'user-activate': # активация пользователя
            {
                do {
                    if (!$this->haveAccessTo('users-edit')) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $aData = $this->model->userData($nUserID, array('user_id', 'activated', 'blocked'));
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    } else {
                        if ($aData['activated'] == 1) {
                            $this->errors->set('Данный аккаунт уже активирован');
                            break;
                        } else {
                            if ($aData['blocked'] == 1) {
                                $this->errors->set('Невозможно активировать заблокированный аккаунт');
                                break;
                            }
                        }
                    }

                    $bSuccess = $this->model->userSave($nUserID, array(
                            'phone_number_verified' => 1,
                            'activate_key' => '',
                            'activated'    => 1
                        )
                    );

                    if (!empty($bSuccess)) {
                        # триггер активации аккаунта
                        bff::i()->callModules('onUserActivated', array($nUserID));
                        InternalMail::i()->sendRegInfo($nUserID);
                    }
                } while (false);

                $this->ajaxResponseForm();
            }
            break;
            case 'tags-suggest': # autocomplete.fb
            {
                $sQuery = $this->input->postget('tag', TYPE_NOTAGS);
                $this->userTags()->tagsAutocomplete($sQuery);
            }
            break;
            case 'tags-autocomplete': # autocomplete
            {
                $sQuery = $this->input->post('q', TYPE_NOTAGS);
                $this->userTags()->tagsAutocomplete($sQuery);
            }
            break;
            case 'spec-price': # настройки цены для специализации
            {
                $nSpecID = $this->input->post('spec_id', TYPE_UINT);
                $aData['pr'] = $this->priceForm($nSpecID);
                $this->ajaxResponseForm($aData);
            }
            break;
            case 'rating-change':
            {
                $aResp = array();
                do {
                    if( ! $nUserID){
                        $this->errors->set(Errors::IMPOSSIBLE);
                        break;
                    }

                    $nRatingCurrent = $this->input->post('rating_current', TYPE_INT);
                    $nRatingNew = $this->input->post('rating_new', TYPE_INT);

                    $aData = $this->model->userData($nUserID, array('rating'));
                    if ($nRatingCurrent != $aData['rating']) {
                        $this->errors->set(_t('users', 'Рейтинг изменился на [new]. Обновите страницу и повторите еще раз.', array('new'=>$aData['rating'])));
                        break;
                    }
                    if ($nRatingCurrent !== $nRatingNew) {
                        $res = $this->model->ratingChange($nUserID, ($nRatingNew-$nRatingCurrent), 'users-admin-change');
                        if (!$res) {
                            $this->errors->impossible();
                        }
                    }
                    $aResp['rating_new'] = $nRatingNew;
                    $aResp['rating_pro'] = static::rating($nRatingNew, true);

                } while(false);
                $this->ajaxResponseForm($aResp);
            }
            break;
            case 'verified-image-view':
            {
                if (!$this->haveAccessTo('users-edit')) {
                    $this->ajaxResponse(Errors::ACCESSDENIED);
                }
                $image = $this->input->get('image', TYPE_UINT);
                $images = $this->verifiedImages($nUserID);
                $data = $images->getImageData($image);
                if (empty($data)) {
                    break;
                }
                $images->show($data);
            }
            break;
        }

        $this->ajaxResponse(Errors::IMPOSSIBLE);
    }

    public function settings()
    {
        if (!$this->haveAccessTo('settings')) {
            return $this->showAccessDenied();
        }

        $aLang = array(
            'verified_requirement' => TYPE_STR,
        );

        if (Request::isAJAX()) {
            $this->input->postm(array(
                'specialization_limit'     => TYPE_UINT,
                'specialization_limit_pro' => TYPE_UINT,
                'contacts_worker_display'  => TYPE_UINT,
                'contacts_worker_view'     => TYPE_UINT,
                'contacts_client_display'  => TYPE_UINT,
                'contacts_client_view'     => TYPE_UINT,
            ), $aData);

            $aData['specialization_limit'];
            $aData['specialization_limit_pro'];
            if($aData['specialization_limit_pro'] < $aData['specialization_limit']){
                $this->errors->set(_t('users', 'Кол-во специализаций для PRO не может быть меньше чем у обычного аккаунта'));
                $this->ajaxResponseForm();
            }

            $aDisplay = array(static::CONTACTS_DISPLAY_ALL, static::CONTACTS_DISPLAY_PRO);
            if( ! in_array($aData['contacts_worker_display'], $aDisplay)){
                $aData['contacts_worker_display'] = reset($aDisplay);
            }
            if( ! in_array($aData['contacts_client_display'], $aDisplay)){
                $aData['contacts_client_display'] = reset($aDisplay);
            }

            $aView = array(static::CONTACTS_VIEW_ALL, static::CONTACTS_VIEW_AUTH, static::CONTACTS_VIEW_PRO);
            if( ! in_array($aData['contacts_worker_view'], $aView)){
                $aData['contacts_worker_view'] = reset($aView);
            }
            if( ! in_array($aData['contacts_client_view'], $aView)){
                $aData['contacts_client_view'] = reset($aView);
            }

            $this->input->postm_lang($aLang, $aData);
            $this->db->langFieldsModify($aData, $aLang, $aData);

            $this->configSave($aData);
            $this->ajaxResponseForm();
        }

        $aData = $this->configLoad(array(
            'specialization_limit_pro' => static::specializationsLimit(true),
            'specialization_limit'     => static::specializationsLimit(false),
            'contacts_worker_display'  => static::CONTACTS_DISPLAY_ALL,
            'contacts_worker_view'     => static::CONTACTS_VIEW_ALL,
            'contacts_client_display'  => static::CONTACTS_DISPLAY_ALL,
            'contacts_client_view'     => static::CONTACTS_VIEW_ALL,
        ));
        if( ! is_array($aData)) $aData = array();

        return $this->viewPHP($aData, 'admin.settings');
    }

    public function settingsSystem(array &$options = array())
    {
        $aData = array('options'=>&$options);
        return $this->viewPHP($aData, 'admin.settings.sys');
    }

    #---------------------------------------------------------------------------------------
    # теги

    public function tags()
    {
        if (!$this->haveAccessTo('tags')) {
            return $this->showAccessDenied();
        }

        return $this->userTags()->manage();
    }

    /**
     * Валидация пользователя
     * @param array @ref $aData данные
     * @param integer $nUserID ID пользователя
     */
    protected function validateUserData(&$aData, $nUserID = 0)
    {
        $aParams = array(
            'type'         => TYPE_UINT,
            'name'         => array(TYPE_NOTAGS, 'len' => 75, 'len.sys' => 'users.user.name.limit'),
            'surname'      => array(TYPE_NOTAGS, 'len' => 75, 'len.sys' => 'users.user.surname.limit'),
            'email'        => TYPE_NOTAGS,
            'login'        => TYPE_NOTAGS,
            'contacts'     => TYPE_ARRAY,
            'status'       => TYPE_UINT,
            'status_text'  => array(TYPE_NOTAGS, 'len' => 300, 'len.sys' => 'users.user.status.limit'),
            'resume_text'  => TYPE_STR,
            'about'        => TYPE_NOTAGS,
            'group_id'     => TYPE_ARRAY_UINT,
            'enotify'      => TYPE_ARRAY_UINT,
            'spec'         => TYPE_ARRAY, # специализации
            'trusted'      => TYPE_BOOL, # довереный пользователь
        );

        if ($nUserID) {
            $aParams['changepass'] = TYPE_BOOL;
            $aParams['password'] = array(TYPE_NOTRIM, 'len' => 100, 'len.sys' => 'users.user.password.limit');
            $aParams['im_noreply'] = TYPE_BOOL;
        } else {
            $aParams['password'] = array(TYPE_NOTRIM, 'len' => 100, 'len.sys' => 'users.user.password.limit');
            $aParams['password2'] = array(TYPE_NOTRIM, 'len' => 100, 'len.sys' => 'users.user.password.limit');
        }

        $usePhone = static::registerPhone();
        if ($usePhone) {
            $aParams['phone_number'] = TYPE_NOTAGS;
        }
        if (static::profileMap()) {
            $aParams['reg1_country'] = TYPE_UINT; # страна
            $aParams['reg2_region']  = TYPE_UINT; # область
            $aParams['reg3_city']    = TYPE_UINT; # город
            $aParams['district_id']  = TYPE_UINT; # район города
            $aParams['metro_id']     = TYPE_UINT; # станция метро
            $aParams['addr_addr']    = array(TYPE_NOTAGS, 'len' => 400, 'len.sys' => 'users.user.addr.limit');
            $aParams['addr_lat']     = TYPE_NUM; # адрес, координата LAT
            $aParams['addr_lng']     = TYPE_NUM; # адрес, координата LNG
        }
        if (static::profileBirthdate()) {
            $aParams['birthdate'] = TYPE_ARRAY_UINT; # дата рождения
        }
        if (static::profileSex()) {
            $aParams['sex'] = TYPE_UINT; # пол
        }
        if (Specializations::useServices(Specializations::SERVICES_PRICE_USERS)) {
            $aParams['specsServices'] = TYPE_ARRAY; # услуги специализаций
        }
        if (static::verifiedEnabled()) {
            $aParams['verified'] = TYPE_UINT;
        }
        if (static::rolesEnabled()) {
            $aParams['role_id'] = TYPE_UINT;
        }

        $this->input->postm($aParams, $aData);

        if (!$nUserID) {
            $aData['admin'] = 0;
        }

        if (Request::isPOST()) {
            # тип
            $aTypes = static::aTypes();
            if (!array_key_exists($aData['type'], $aTypes)) {
                $aData['type'] = key($aTypes);
            }

            # номер телефона
            if ($usePhone && (!empty($aData['phone_number']) || !$nUserID)) {
                if (!$this->input->isPhoneNumber($aData['phone_number'])) {
                    $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone_number');
                } elseif ($this->model->userPhoneExists($aData['phone_number'], $nUserID)) {
                    $this->errors->set(_t('users', 'Пользователь с таким номером телефона уже зарегистрирован'), 'phone_number');
                }
            }
            # email (для авторизации)
            if (!$this->input->isEmail($aData['email'])) {
                $this->errors->set('Email указан некорректно', 'email');
            } elseif ($this->model->userEmailExists($aData['email'], $nUserID)) {
                $this->errors->set('Указанный email уже существует', 'email');
            }

            # login
            if (!$this->isLoginCorrect($aData['login'])) {
                $this->errors->set('Укажите корректный логин', 'login');
            } elseif ($this->model->userLoginExists($aData['login'], $nUserID)) {
                $this->errors->set('Указанный логин уже существует', 'login');
            }

            if ($nUserID) {
                if ($aData['changepass']) {
                    if (strlen($aData['password']) < $this->passwordMinLength) {
                        $this->errors->set('Новый пароль слишком короткий', 'password');
                    } else {
                        $aDataCurrent = $this->model->userData($nUserID, array('password_salt'));
                        $aData['password'] = $this->security->getUserPasswordMD5($aData['password'], $aDataCurrent['password_salt']);
                    }
                } else {
                    unset($aData['password']);
                }
                unset($aData['changepass']);
            } else {
                if (strlen($aData['password']) < $this->passwordMinLength) {
                    $this->errors->set('Пароль слишком короткий', 'password');
                } elseif ($aData['password'] != $aData['password2']) {
                    $this->errors->set('Ошибка подтверждения пароля', 'password');
                }
                unset($aData['password2']);
                if (static::rolesEnabled()) {
                    $roles = static::roles();
                    if ( ! array_key_exists($aData['role_id'], $roles)) {
                        foreach ($roles as $v) {
                            if ($v['default']) { $aData['role_id'] = $v['id']; break; }
                        }
                    }
                }
            }

            # уведомления
            $aData['enotify'] = array_sum($aData['enotify']);

            # Чистим текст
            $parser = new \bff\utils\TextParser();
            $aData['resume_text'] = $parser->parseWysiwygTextFrontend($aData['resume_text']);

            $this->cleanUserData($aData);

            $bPro = false;
            if ($nUserID) {
                $aUserData = $this->model->userData($nUserID, array('pro'));
                $bPro = $aUserData['pro'];
            }
            $this->cleanSpecializations($aData, $aData['type'], $bPro);
            $this->cleanSpecializationsServices($aData);
        } else {
            $aData['pro'] = 0;
        }
    }

    protected function prepareGroupsOptions(&$aData, $mExceptGroupKeyword, $aActiveGroupsID = array())
    {
        $exists_options = '';
        $active_options = '';
        $aGroups = $this->model->groups($mExceptGroupKeyword, false);
        for ($i = 0; $i < count($aGroups); $i++) {
            if (in_array($aGroups[$i]['group_id'], $aActiveGroupsID)) {
                $active_options .= '<option value="' . $aGroups[$i]['group_id'] . '" style="color:' . $aGroups[$i]['color'] . ';">' . $aGroups[$i]['title'] . '</option>';
            } else {
                $exists_options .= '<option value="' . $aGroups[$i]['group_id'] . '" style="color:' . $aGroups[$i]['color'] . ';">' . $aGroups[$i]['title'] . '</option>';
            }
        }
        $aData['exists_options'] = $exists_options;
        $aData['active_options'] = $active_options;
    }

    # ------------------------------------------------------------------------------------------------------------------------------
    # Услуги

    public function svc_services()
    {
        if (!$this->haveAccessTo('svc')) {
            return $this->showAccessDenied();
        }

        $svc = Svc::model();

        if (Request::isPOST()) {
            $aResponse = array();

            switch ($this->input->getpost('act')) {
                case 'update':
                {

                    $nSvcID = $this->input->post('id', TYPE_UINT);
                    if (!$nSvcID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $aData = $svc->svcData($nSvcID, array('id', 'type', 'keyword'));
                    if (empty($aData) || $aData['type'] != Svc::TYPE_SERVICE) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $this->svcValidateData($aData['keyword'], Svc::TYPE_SERVICE, $aDataSave);

                    if ($this->errors->no()) {
                        # загружаем иконки
                        $oIcon = self::svcIcon($nSvcID);
                        $oIcon->setAssignErrors(false);
                        foreach ($oIcon->getVariants() as $iconField => $v) {
                            $oIcon->setVariant($iconField);
                            $aIconData = $oIcon->uploadFILES($iconField, true, false);
                            if (!empty($aIconData)) {
                                $aDataSave[$iconField] = $aIconData['filename'];
                            } else {
                                if ($this->input->post($iconField . '_del', TYPE_BOOL)) {
                                    if ($oIcon->delete(false)) {
                                        $aDataSave[$iconField] = '';
                                    }
                                }
                            }
                        }

                        # сохраняем
                        $svc->svcSave($nSvcID, $aDataSave);
                    }
                }
                break;
                case 'reorder': # сортировка услуг
                {

                    $aSvc = $this->input->post('svc', TYPE_ARRAY_UINT);
                    $svc->svcReorder($aSvc, Svc::TYPE_SERVICE);
                }
                break;
                default:
                {
                    $this->errors->impossible();
                }
                break;
            }

            $this->iframeResponseForm($aResponse);
        }

        $aData = array(
            'svc' => $svc->svcListing(Svc::TYPE_SERVICE, $this->module_name),
        );
        return $this->viewPHP($aData, 'admin.svc.services');
    }

    /**
     * Проверка данных услуги / пакета услуг
     * @param string $sSvcKeyword ID услуги / пакета услуг
     * @param integer $nType тип Svc::TYPE_
     * @param array $aData @ref проверенные данные
     */
    protected function svcValidateData($sSvcKeyword, $nType, &$aData)
    {
        $aParams = array(
            'price' => TYPE_PRICE,
        );

        if ($nType == Svc::TYPE_SERVICE) {
            $aSettings = array(
                'on'     => TYPE_BOOL, # включена
            );

            switch($sSvcKeyword){
                case 'pro':{
                    $aSettings['mass'] = TYPE_ARRAY_ARRAY;
                } break;
                case 'stairs':{
                    $aSettings += array(
                        'price_main'    => TYPE_UNUM,
                        'price_user'    => TYPE_UNUM,
                        'price_spec'    => TYPE_UNUM,
                        'price_up'      => TYPE_UNUM,
                    );
                    if(Specializations::catsOn()){
                        $aSettings['price_cat'] = TYPE_UNUM;
                    }
                } break;
                case 'carousel':{
                    $aSettings['amount'] = TYPE_UNUM;
                } break;

            }

            $aData = $this->input->postm($aParams);
            $aData['settings'] = $this->input->postm($aSettings);
            $this->input->postm_lang($this->model->langSvcServices, $aData['settings']);
            $aData['title'] = $aData['settings']['title_view'][LNG];

            switch ($sSvcKeyword) {
                case 'pro': {
                    $aMass = array();
                    foreach ($aData['settings']['mass'] as $v) {
                        $this->input->clean_array($v, array(
                            'm' => TYPE_UINT,
                            'p' => TYPE_PRICE,
                        ));
                        if($v['m']>0 && !isset($aMass[$v['m']])) {
                            $aMass[$v['m']] = $v['p'];
                        }
                    }

                    if (empty($aMass)) {
                        $this->errors->set( _t('svc', 'Укажите стоимость услуги') );
                        break;
                    }
                    $aData['settings']['mass'] = $aMass;
                } break;
                case 'carousel': {
                    if (empty($aData['settings']['amount'])) {
                        $aData['settings']['amount'] = 10;
                    }
                } break;

            }

        }
    }

    public function carousel()
    {
        if ( ! bff::servicesEnabled()) {
            return '';
        }

        $aSvc = Svc::model()->svcData(static::SVC_CAROUSEL);
        if ( ! $aSvc['on']) {
            return '';
        }

        if (!$this->haveAccessTo('carousel')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'delete':
                {

                    $nID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nID) {
                        $this->errors->impossible();
                        break;
                    }

                    $this->model->svcCarouselDelete($nID);
                } break;
                case 'form':
                {
                    $nID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aData = $this->model->svcCarouselData($nID);
                    $aResponse['html'] = $this->viewPHP($aData, 'admin.carousel.form');

                } break;
                case 'cancel':
                {
                    $nID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aData = array('v' => $this->model->svcCarouselData($nID));
                    $aResponse['html'] = $this->viewPHP($aData, 'admin.carousel.view');
                } break;
                case 'save':
                {

                    $nID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aData = $this->input->postm(array(
                        'title'   => array(TYPE_NOTAGS, 'len' => 50, 'len.sys' => 'users.carousel.title.limit'),
                        'message' => array(TYPE_NOTAGS, 'len' => 300, 'len.sys' => 'users.carousel.message.limit'),
                    ));

                    if (strlen($aData['message']) < config::sysAdmin('users.carousel.message.min', 3, TYPE_UINT)) {
                        $this->errors->set(_t('svc', 'Текст слишком короткий'), 'message');
                    }
                    if ( ! $this->errors->no()) {
                        break;
                    }

                    $this->model->svcCarouselSave($nID, $aData);
                    $aData = array('v' => $this->model->svcCarouselData($nID));
                    $aResponse['html'] = $this->viewPHP($aData, 'admin.carousel.view');
                } break;
               default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $aData = array();

        $aData['list'] = $this->model->svcUsersCarousel($aSvc['amount'] + 4);

        return $this->viewPHP($aData, 'admin.carousel');
    }

    public function stairs()
    {
        if ( ! bff::servicesEnabled()) {
            return '';
        }

        $aSvc = Svc::model()->svcData(static::SVC_STAIRS);
        if ( ! $aSvc['on']) {
            return '';
        }

        if (!$this->haveAccessTo('stairs')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'delete':
                {

                    $nID = $this->input->postget('id', TYPE_UINT);
                    if (!$nID) {
                        $this->errors->impossible();
                        break;
                    }

                    $this->model->svcStairsDelete($nID);
                }   break;
                case 'form':
                {
                    $nID = $this->input->postget('id', TYPE_UINT);
                    if (!$nID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aData = $this->model->svcStairsData($nID);
                    $aData['specs_select'] = $this->stairsSpecsSelect($aCats, $aData);
                    $aResponse['html'] = $this->viewPHP($aData, 'admin.stairs.form');

                } break;
                case 'cancel':
                {
                    $nID = $this->input->postget('id', TYPE_UINT);
                    if ( ! $nID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aData = array('v' => $this->model->svcStairsData($nID));
                    $aResponse['html'] = $this->viewPHP($aData, 'admin.stairs.view');
                } break;
                case 'save':
                {

                    $nID = $this->input->postget('id', TYPE_UINT);
                    if (!$nID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aOldData = $this->model->svcStairsData($nID);
                    if (empty($aOldData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->input->postm(array(
                        'title'   => array(TYPE_NOTAGS, 'len' => 50, 'len.sys' => 'users.stairs.title.limit'),
                        'message' => array(TYPE_NOTAGS, 'len' => 80, 'len.sys' => 'users.stairs.message.limit'),
                        'description' => array(TYPE_NOTAGS, 'len' => 400, 'len.sys' => 'users.stairs.description.limit'),
                        'cat_id'  => TYPE_INT,
                        'spec_id' => TYPE_INT,
                    ));

                    if (!$this->errors->no()) {
                        break;
                    }

                    if ($aData['cat_id'] < 0) {
                        $aData['spec_id'] = $aData['cat_id'];
                    }
                    if ($aData['spec_id']) {
                        $aData['cat_id'] = 0;
                    }

                    if ($aData['cat_id'] != $aOldData['cat_id'] || $aData['spec_id'] != $aOldData['spec_id']) {
                        $nCount = $this->model->svcUsersStairs(array(
                            'cat_id' => $aData['cat_id'],
                            'spec_id' => $aData['spec_id'],
                            'user_id' => $aOldData['user_id'],
                        ), true);
                        if ($nCount) {
                            $this->errors->set(_t('svc', 'У пользователя уже активирована услуга лестница для этого раздела'), 'message');
                            break;
                        }
                    }

                    $this->model->svcStairsSave($nID, $aData);
                    $aData = array('v' => $this->model->svcStairsData($nID));
                    $aResponse['html'] = $this->viewPHP($aData, 'admin.stairs.view');
                } break;
                case 'specs':
                {
                    $aData = $this->input->postm(array(
                        'cat_id'  => TYPE_INT,
                        'spec_id' => TYPE_INT,
                    ));
                    $aResponse['specs'] = $this->stairsSpecsSelect($aCats, $aData);
                } break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = $this->input->postgetm(array(
            'page'    => TYPE_UINT,
            'cat_id'  => TYPE_INT,
            'spec_id' => TYPE_INT,
        ));

        $aData = array('f' => $f);
        $aData['cats_specs_select'] = $this->stairsSpecsSelect($aData['cats'], $f, _t('', 'Все'));
        $aFilter = array();
        if($f['cat_id'] < 0){
            $f['spec_id'] = $f['cat_id'];
            $f['cat_id'] = 0;
        }
        if ($f['spec_id']) {
            $aFilter = array('spec_id' => $f['spec_id']);
        } else if($f['cat_id']) {
            $aFilter = array('cat_id' => $f['cat_id']);
        }

        $nCount = $this->model->svcUsersStairs($aFilter, true);
        $oPgn = new Pagination($nCount, 10, '#', 'jStairs.page('.Pagination::PAGE_ID.'); return false;');
        $oPgn->pageNeighbours = 6;
        $aData['pgn'] = $oPgn->view(array('arrows'=>false));
        $aData['list'] = $this->model->svcUsersStairs($aFilter, false, $oPgn->getLimitOffset());
        $aData['list'] = $this->viewPHP($aData, 'admin.stairs.list');
        if(Request::isAJAX()){
            $this->ajaxResponseForm(array(
                'list' => $aData['list'],
                'pgn'  => $aData['pgn'],
                'specs' => $aData['cats_specs_select'],
            ));
        }

        return $this->viewPHP($aData, 'admin.stairs');
    }

    public function stairsSpecsSelect(& $aCats = array(), $f = array(), $mEmpty = false)
    {
        if (empty($aCats)) {
            $aCats = static::aSvcStairs() + Specializations::model()->specializationsInAllCategories(array('id','title'), array('id','title'));
        }
        $bCatOn = Specializations::catsOn();
        if ($bCatOn) {
            if (isset($f['spec_id']) && $f['spec_id'] < 0) {
                $f['cat_id'] = $f['spec_id'];
                $f['spec_id'] = 0;
            }
            if ( ! isset($f['cat_id'])) {
                $f['cat_id'] = 0;
            }
            if ( ! isset($f['spec_id'])) {
                $f['spec_id'] = 0;
            }
            if ( ! $f['cat_id'] && $f['spec_id']) {
                foreach($aCats as $v){
                    if( ! empty($v['specs'])){
                        foreach($v['specs'] as $vv){
                            if($vv['id'] == $f['spec_id']){
                                $f['cat_id'] = $vv['cat_id'];
                                break 2;
                            }
                        }
                    }
                }
            }
            $sRet = '<select class="j-cat" name="cat_id">'.HTML::selectOptions($aCats, $f['cat_id'], $mEmpty, 'id', 'title').'</select>';
            if ($f['cat_id'] && ! empty($aCats[ $f['cat_id'] ]['specs'])) {
                $sRet .= '<select name="spec_id" style="margin-left: 6px;">'.HTML::selectOptions($aCats[ $f['cat_id'] ]['specs'], $f['spec_id'], _t('', 'Все'), 'id', 'title').'</select>';
            }
        } else {
            if ( ! isset($f['spec_id'])) {
                $f['spec_id'] = 0;
            }
            $sRet = '<select class="j-cat" name="spec_id">'.HTML::selectOptions($aCats, $f['spec_id'], $mEmpty, 'id', 'title').'</select>';
        }
        return $sRet;
    }
    
}