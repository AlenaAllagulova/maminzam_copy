<?php

class M_Users_
{
    public static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $menuTitle = 'Пользователи';
        $module = 'users';
        # Пользователи
        if ($security->haveAccessToModuleToMethod($module, 'members-listing')) {
            $verified = Users::verifiedEnabled();
            $menu->assign($menuTitle, 'Список пользователей', $module, 'listing', true, 1,
                array('rlink' => array('event' => 'user_add'), 'counter' => ($verified ? 'users_verified_waiting' : false))
            );
            $menu->assign($menuTitle, 'Настройки профиля', $module, 'profile', false, 10);
            if ($verified) {
                $menu->adminHeaderCounter($menuTitle, 'users_verified_waiting', $module, 'listing&status=6', 4, '', array('parent'=>'moderation'));
            }
        }
        # Модераторы
        if ($security->haveAccessToModuleToMethod($module, 'admins-listing')) {
            $menu->assign($menuTitle, 'Список модераторов', $module, 'listing_moderators', true, 2,
                array('rlink' => array('event' => 'user_add'))
            );
        }
        if ($security->haveAccessToModuleToMethod($module, 'users-edit')) {
            $menu->assign($menuTitle, 'Добавить пользователя', $module, 'user_add', false, 3);
            $menu->assign($menuTitle, 'Редактирование пользователя', $module, 'user_edit', false, 4);
            $menu->assign($menuTitle, 'Удаление пользователя', $module, 'user_delete', false, 5);
            $menu->assign($menuTitle, 'Блокировка пользователей', $module, 'ban', true, 6);
        }
        # Навыки
        if ($security->haveAccessToModuleToMethod($module, 'tags')) {
            $menu->assign($menuTitle, 'Навыки', $module, 'tags', true, 10);
        }

        # Услуги
        if ($security->haveAccessToModuleToMethod($module, 'svc')) {
            $menu->assign('Счет и услуги', 'Услуги: Исполнители', $module, 'svc_services', true, 30);
            $menu->assign($menuTitle, 'Карусель', $module, 'carousel', true, 70);
            $menu->assign($menuTitle, 'Лестница', $module, 'stairs', true, 80);
        }

        # Настройки
        if ($security->haveAccessToModuleToMethod($module, 'settings')) {
            $menu->assign($menuTitle, _t('','Settings'), $module, 'settings', true, 100);
        }

        # SEO
        if ($security->haveAccessToModuleToMethod($module, 'seo')) {
            $menu->assign('SEO', $menuTitle, $module, 'seo_templates_edit', true, 20);
        }

    }
}