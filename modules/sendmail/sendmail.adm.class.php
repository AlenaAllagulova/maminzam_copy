<?php

/**
 * Права доступа группы:
 *  - sendmail: Работа с почтой
 *      - massend: Массовая рассылка
 */
class Sendmail_ extends SendmailBase
{
    //---------------------------------------------------------------
    // рассылка писем

    public function massend_form()
    {
        if (!$this->haveAccessTo('massend')) {
            return $this->showAccessDenied();
        }

        $aData = array();
        $aData['noreply'] = config::sys('mail.noreply');
        $aData['fromname'] = config::sys('mail.fromname');
        $aData['wrappers'] = $this->model->wrappersOptions(0, '- Без шаблона -');
        return $this->viewPHP($aData, 'admin.massend');
    }

    public function massend_listing()
    {
        if (!$this->haveAccessTo('massend')) {
            return $this->showAccessDenied();
        }

        $aData['items'] = $this->db->select('SELECT * FROM ' . TABLE_MASSEND . ' WHERE type = '.static::TYPE_MANUAL.' ORDER BY id DESC');

        return $this->viewPHP($aData, 'admin.massend.listing');
    }

    public function ajax()
    {
        if (!$this->haveAccessTo('massend')) {
            return $this->showAccessDenied();
        }

        $aResponse = array();

        switch ($this->input->getpost('act', TYPE_STR)) {
            case 'massend-init': # инициализация рассылки
            {

                $this->input->postm(array(
                        'test'    => TYPE_BOOL,
                        'from'    => TYPE_STR,
                        'fromname'=> TYPE_STR,
                        'subject' => TYPE_STR,
                        'body'    => TYPE_STR,
                        'is_html' => TYPE_BOOL,
                        'wrapper_id' => TYPE_UINT,
                        'users_type' => TYPE_UINT,
                        'users_svc'  => TYPE_UINT,
                    ), $p
                );
                extract($p, EXTR_REFS);

                if (!$is_html) {
                    $body = nl2br($body);
                }
                $body = preg_replace("@<script[^>]*?>.*?</script>@si", '', $body);

                // set_time_limit(0);
                ignore_user_abort(true);

                if (!(!empty($from) && !empty($subject) && !empty($body))) {
                    $this->errors->impossible();
                    break;
                }

                if ($test) {
                    $aReceiversTest = $this->input->post('receivers_test', TYPE_STR);
                    $aReceiversTest = explode(',', $aReceiversTest);
                    if (!empty($aReceiversTest)) {
                        $aReceivers = array_map('trim', $aReceiversTest);
                    }

                    $nSendSuccess = 0;
                    $time_start = microtime(true);
                    $nReceiversTotal = sizeof($aReceivers);
                    $aReceiversSended = array();

                    # формируем текст письма на основе шаблона "massend"
                    $aTemplate = $this->getMailTemplate('sendmail_massend', array('msg' => $body), LNG, $is_html, $wrapper_id);
                    $body = $aTemplate['body'];

                    $mailer = new CMail();
                    $mailer->From = $from;
                    if ( ! empty($fromname)) {
                        $mailer->FromName = $fromname;
                    }
                    $mailer->Subject = $subject;
                    $mailer->MsgHTML($body);

                    for ($i = 0; $i < $nReceiversTotal; $i++) {
                        $mailer->AddAddress($aReceivers[$i]);
                        if ($mailer->Send()) {
                            $nSendSuccess++;
                        }

                        $mailer->ClearAddresses();
                        usleep(150000); // sleep for 0.15 second
                    }

                    $time_total = (microtime(true) - $time_start); //останавливаем секундомер

                    $this->ajaxResponse(array(
                            'total'      => $nReceiversTotal,
                            'success'    => $nSendSuccess,
                            'failed'     => ($nReceiversTotal - $nSendSuccess),
                            'sended'     => $aReceiversSended,
                            'time_total' => sprintf('%0.2f', $time_total),
                            'time_avg'   => sprintf('%0.2f', (!empty($nReceiversTotal) ? ($time_total / $nReceiversTotal) : 0)),
                            'res'        => $this->errors->no()
                        )
                    );
                } else {
                    # формируем список получателей, исключая заблокированных/неактивированных/не подписавшихся на рассылку
                    # учитывая настройки из админ панели
                    $filter = array('blocked' => 0, 'activated' => 1);
                    $filter[] = array('( enotify & :news ) != 0', ':news' => Users::ENOTIFY_NEWS);
                    if ($users_type && array_key_exists($users_type, Users::aTypes())) {
                        $filter['type'] = $users_type;
                    }
                    if ($users_type == Users::TYPE_WORKER) {
                        switch ($users_svc) {
                            case static::SVC_NOT_PRO:
                                $filter['pro'] = 0;
                                break;
                            case static::SVC_PRO:
                                $filter['pro'] = 1;
                                break;
                        }
                    }

                    $total = $this->model->initMassend($filter, true);
                    if (!$total) {
                        $this->errors->set('Получатели не найдены');
                        break;
                    }

                    $settings = array(
                        'from'       => $from,
                        'fromname'   => $fromname,
                        'subject'    => $subject,
                        'body'       => $body,
                        'is_html'    => $is_html,
                        'wrapper_id' => $wrapper_id,
                        'time_total' => 0,
                        'time_avg'   => 0,
                        'users_type' => $users_type,
                        'users_svc'  => $users_svc,
                    );
                    if( ! $this->model->initMassend($filter, false, $settings)){
                        $this->errors->set('Ошибка инициализации рассылки');
                    }
                }
            }
            break;
            case 'massend-delete': # удаление рассылки
            {

                $nMassendID = $this->input->post('rec', TYPE_UINT);
                if (!$nMassendID) {
                    break;
                }

                $this->db->delete(TABLE_MASSEND, $nMassendID);
                $this->db->delete(TABLE_MASSEND_RECEIVERS, array('massend_id' => $nMassendID));
            }
            break;
            case 'massend-info': # получение сведений о рассылке
            {
                $nMassendID = $this->input->get('id', TYPE_UINT);
                if (!$nMassendID) {
                    $this->errors->unknownRecord();
                    break;
                }

                $aData = $this->db->one_array('SELECT * FROM ' . TABLE_MASSEND . ' WHERE id = :id', array(':id' => $nMassendID));
                if (empty($aData)) {
                    $this->errors->unknownRecord();
                    break;
                }

                $aSettings = func::unserialize($aData['settings']);
                foreach (array('from','fromname','subject','body','time_total','time_avg','wrapper_id') as $k) {
                    if (isset($aSettings[$k])) $aData[$k] = $aSettings[$k];
                }
                $aData['users_type'] = ! empty($aSettings['users_type']) ? $aSettings['users_type'] : 0;
                $aData['users_svc'] = ! empty($aSettings['users_svc']) ? $aSettings['users_svc'] : 0;

                echo $this->viewPHP($aData, 'admin.massend.info');
                exit;
            }
            break;
            default:
                $this->errors->impossible();
        }

        $this->ajaxResponseForm($aResponse);
    }

}