<?php

class Sendmail_ extends SendmailBase
{
    /**
     * Cron: Массовая рассылка писем
     */
    public function cronMassend()
    {
        if (!bff::cron()) {
            return;
        }

        $this->db->begin(); # для LOCK записей таблицы получателей рассылки
        
        do {
            # получаем информацию об открытой рассылке
            $massend = $this->model->lastedMassend();
            if (empty($massend['id'])) {
                break;
            }
            $massendID = $massend['id'];

            $aSettings = func::unserialize($massend['settings']);
            if ($aSettings === false || empty($aSettings)) {
                $this->errors->set('corrupted massend-settings data (id=' . $massendID . ')');
                $this->model->closeMassend($massendID, true);
                break;
            }

            # формируем текст письма на основе шаблона "massend"
            $isHTML = (is_null($aSettings['is_html']) ? NULL : !empty($aSettings['is_html']));
            if (is_array($aSettings['body'])) {
                $msg = $aSettings['body'][LNG];
            } else {
                $msg = $aSettings['body'];
            }
            if($massend['type'] == static::TYPE_MANUAL) {
                $aTemplate = $this->getMailTemplate('sendmail_massend', array('msg' => $msg), LNG, $isHTML, ! empty($aSettings['wrapper_id']) ? $aSettings['wrapper_id'] : false);
                $aSettings['body'] = $aTemplate['body'];
            }else{
                $aSettings['body'] = $msg;
            }

            // SELECT ... FOR UPDATE (mysql)
            $receivers = $this->model->massendReceivers($massendID);
            if(empty($receivers)){
                $this->model->closeMassend($massendID, true);
                break;
            }

            $mailer = new CMail();

            if (!empty($aSettings['from'])) {
                $mailer->setFrom($aSettings['from'], isset($aSettings['fromname']) ? $aSettings['fromname'] : '');
            }

            if (BFF_DEBUG) {
                bff::log('massend started: ' . sizeof($receivers) . ' receivers', 'cron.log');
            }

            $result = array();
            $nSuccess = 0;
            foreach ($receivers as $v) {
                $mailer->AddAddress($v['email']);

                $mailer->AltBody = '';

                $replace = $receivers[$v['id']]['tpl_data'];
                # макросы получателя
                if (!isset($replace['{fio}'])) {
                    $fio = $v['name'] . ' ' . $v['surname'];
                    if (empty($v['name']) && empty($v['surname'])) {
                        $fio = $v['login'];
                    }
                    $replace['{fio}'] = $fio;
                }
                if (!isset($replace['{email}'])) {
                    $replace['{email}'] = $v['email'];
                }
                if (!isset($replace['{name}'])) {
                    $replace['{name}'] = !empty($v['name']) ? $v['name'] : $replace['{fio}'];
                }

                $mailer->Subject = strtr(is_array($aSettings['subject']) ? $aSettings['subject'][LNG] : $aSettings['subject'], $replace);
                $mailer->MsgHTML(strtr($aSettings['body'], $replace));

                $res = $mailer->Send() ? 1 : 0;
                if ($res) {
                    $nSuccess++;
                    $result[] = $v['id'];
                }

                $mailer->ClearAddresses();

                usleep(100000); // sleep for 0.1 second
            }

            if ( ! empty($result)) {
                $this->model->updateReceivers($massendID, $result);
            }

            $this->model->closeMassend($massendID);

            if (BFF_DEBUG) {
                bff::log('massend finish: ' . $nSuccess, 'cron.log');
            }

        }while(false);
        $this->db->commit();
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {

        return array(
            'cronMassend' => array('period' => '*/2 * * * *'),
        );
    }

}