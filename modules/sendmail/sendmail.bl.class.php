<?php

abstract class SendmailBase_ extends SendmailModule
{
    /** @var SendmailModel */
    public $model = null;

    # Тип рассылки:
    const TYPE_MANUAL = 1; # Инициированная администратором
    const TYPE_AUTO   = 2; # Автоматическая, инициированная по определенному событию

    # получатели
    const SVC_NOT_PRO = 1; # Все без услуги PRO
    const SVC_PRO     = 2; # Все с услугой PRO

    public function init()
    {
        parent::init();

        $this->aTemplates['sendmail_massend'] = array(
            'title'       => 'Массовая рассылка писем (общий шаблон)',
            'description' => 'Шаблон письма, используемого при массовой рассылке',
            'vars'        => array('{msg}' => 'Текст письма'),
            'impl'        => true,
            'priority'    => 1000,
            'enotify'     => -1,
        );

    }

}