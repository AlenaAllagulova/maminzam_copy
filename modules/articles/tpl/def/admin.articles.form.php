<?php
/**
 * @var $this Articles
 */
$aData = HTML::escape($aData, 'html', array('cat_id','title'));
$aData['edit'] = $edit = ! empty($id);
$aTabs = array(
    'info' => _t('articles','Основные'),
    'images' => _t('articles','Изображения'),
);
$edit = ! empty($id);
if ($edit) {
    $aTabs['comments'] = _t('articles','Комментарии');
}
$imagesLimit = Articles::imagesLimit();
if( ! $imagesLimit){
    unset($aTabs['images']);
}
$tab = $this->input->getpost('ftab', TYPE_NOTAGS);
if( ! isset($aTabs[$tab])) {
    $tab = 'info';
}

?>
<form name="ArticlesArticlesForm" id="ArticlesArticlesForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
    <input type="hidden" name="save" value="1" />
    <input type="hidden" name="id" value="<?= $id ?>" />
    <input type="hidden" name="cat_id" id="article-cat_id" value="<?= $cat_id ?>" />
    <div class="tabsBar" id="ArticlesFormTabs">
        <? foreach($aTabs as $k=>$v) { ?>
            <span class="tab<? if($k == $tab) { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
        <? } ?>
        <? if($edit): ?><div class="right"><a target="_blank" href="<?= Articles::url('view', array('id' => $id, 'keyword' => $keyword)) ?>">Просмотр &rarr;</a></div><? endif; ?>
    </div>
    <div class="j-tab <?= $tab != 'info' ? 'hidden' : '' ?>" id="j-tab-info">
    <table class="admtbl tbledit">
        <tr class="required check-select">
            <td class="row1 field-title" width="100">Категория<span class="required-mark">*</span>:</td>
            <td class="row2">
                <?
                foreach($cats as $lvl=>$v) {
                    ?><select class="cat-select" style="margin-right: 5px;<? if(empty($v['categories'])){ ?>display: none; <? } ?>" onchange="jArticlesArticlesForm.onCategory($(this))"><?= $v['categories'] ?></select><?
                }
                ?>
            </td>
        </tr>
        <tr class="required">
            <td class="row1 field-title"><?= _t('', 'Title') ?><span class="required-mark">*</span>:</td>
            <td class="row2">
                <input class="stretch" type="text" id="article-title" name="title" value="<?= $title ?>" />
            </td>
        </tr>
        <? if( ! empty($publicator)): ?>
            <tr>
                <td class="row1" colspan="2">
                    <?= $publicator->form($content, $id, 'content', 'jArticlesFormPublicator'); ?>
                </td>
            </tr>
        <? else: ?>
        <tr class="required">
            <td class="row1 field-title">Содержание<span class="required-mark">*</span>:</td>
            <td class="row2">
                <?= tpl::jwysiwyg($content, 'article-content,content', 0, 215); ?>
            </td>
        </tr>
        <? endif; ?>
        <tr>
            <td class="row1 field-title"></td>
            <td class="row2">
                <label class="checkbox">
                    <input name="comments_enabled" type="checkbox" <?= $comments_enabled ? ' checked="checked"' : '' ?> />
                    Комментарии разрешены
                </label>
            </td>
        </tr>
        <tr>
            <td class="row1 field-title"><?= _t('', 'Tags') ?>:</td>
            <td class="row2">
                <?= $this->articleTags()->tagsForm($id, $this->adminLink('listing&act=tags-suggest'), '700'); ?>
            </td>
        </tr>
        <?php if($edit): ?>
            <tr>
                <td class="row1 field-title"><?= _t('', 'User') ?></td>
                <td class="row2">
                    <a href="#" class="ajax" onclick="return bff.userinfo(<?= $user_id ?>);"><?= $email ?></a>
                </td>
            </tr>
            <tr>
                <td class="row1 field-title"></td>
                <td class="row2">
                    <label class="checkbox">
                        <input name="noindex" type="checkbox" <?= !empty($noindex) ? ' checked="checked"' : '' ?> />
                        Закрыть от индексации
                    </label>
                </td>
            </tr>
            <tr>
                <td class="row1" colspan="2">
                    <?= $this->viewPHP($aData, 'admin.articles.form.status'); ?>
                </td>
            </tr>
        <?php else: ?>
            <tr>
                <td class="row1 field-title"><?= _t('', 'User') ?><span class="required-mark">*</span></td>
                <td class="row2">
                    <input type="hidden" name="user_id" value="0" id="j-article-user-id" />
                    <input type="text" name="email" value="" id="j-article-user-email" class="autocomplete input-large" placeholder="<?= _t('', 'Enter user e-mail') ?>" />
                </td>
            </tr>
        <?php endif; ?>
        <tr class="footer">
            <td colspan="2">
                <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jArticlesArticlesForm.save(false);" />
                <? if ($edit) { ?><input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jArticlesArticlesForm.save(true);" /><? } ?>
                <? if ($edit) { ?><input type="button" onclick="jArticlesArticlesForm.del(); return false;" class="btn btn-danger button delete" value="<?= _t('', 'Delete') ?>" /><? } ?>
                <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jArticlesArticlesFormManager.action('cancel');" />
            </td>
        </tr>
    </table>
    </div>
</form>
<? if($imagesLimit): ?>
    <div class="j-tab <?= $tab != 'images' ? 'hidden' : '' ?>" id="j-tab-images"><?= $this->viewPHP($aData, 'admin.articles.form.images'); ?></div>
<? endif; ?>
<? if($edit): ?>
    <div class="j-tab <?= $tab != 'comments' ? 'hidden' : '' ?>" id="j-tab-comments"><?= $this->articleComments()->admListing($id) ?></div>
<? endif; ?>

<script type="text/javascript">
    var jArticlesArticlesForm =
        (function(){
            var $progress, $form, formChk, id = parseInt(<?= $id ?>);
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';
            var catCache = {};

            $(function(){
                $progress = $('#ArticlesArticlesFormProgress');
                $form = $('#ArticlesArticlesForm');

                // tabs
                var $tabs = $('.j-tab');
                $form.find('#ArticlesFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                    var key = $(this).data('key');
                    $tabs.addClass('hidden');
                    $('#j-tab-'+key).removeClass('hidden');
                    $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
                    if(bff.h) {
                        window.history.pushState({}, document.title, ajaxUrl + '&act=<?= $act ?>&id=<?= $id ?>&ftab=' + key);
                    }
                });

                <? if( ! $edit): ?>
                $form.find('#j-article-user-email').autocomplete(ajaxUrl+'&act=user',
                    {valueInput: $form.find('#j-article-user-id')});
                <? endif; ?>
            });

            function catView(data, $select)
            {
                if(data === 'empty') {
                    return;
                }

                if(data.subs>0) {
                    $select.after('<select class="cat-select" style="margin-right: 5px;" onchange="jArticlesArticlesForm.onCategory($(this))">'+data.cats+'</select>').show();
                    return;
                }
                formChk.check(false, true);
            }

            return {
                del: function()
                {
                    if( id > 0 ) {
                        bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                            false, {progress: $progress, repaint: false, onComplete:function(){
                                bff.success('Запись успешно удалена');
                                jArticlesArticlesFormManager.action('cancel');
                                jArticlesArticlesList.refresh();
                            }});
                    }
                },
                save: function(returnToList)
                {
                    if( ! formChk.check(true) ) return;
                    // check selected cats
                    var catsEmpty = $form.find('select.cat-select[value="0"]:visible');
                    if(catsEmpty.length > 0) {
                        bff.error('Выберите категорию');
                        return false;
                    }
                    var data = $form.serialize();
                    <? if( ! $edit): ?>
                    if(typeof(jArticleImages) == 'object') {
                        data = data + '&' + jArticleImages.serialize();
                    }
                    <? endif; ?>

                    bff.ajax(ajaxUrl, data, function(data){
                        if(data && data.success) {
                            bff.success('Данные успешно сохранены');
                            if(returnToList || ! id) {
                                jArticlesArticlesFormManager.action('cancel');
                                jArticlesArticlesList.refresh( ! id);
                            }
                        }
                    }, $progress);
                },
                onShow: function ()
                {
                    formChk = new bff.formChecker($form);
                },
                onCategory: function($select)
                {
                    catView('empty');

                    var catID = intval($select.val());
                    $form.find('#article-cat_id').val(catID);
                    $select.nextAll().remove();

                    if( ! catID) return;

                    if(catCache.hasOwnProperty(catID)) {
                        catView( catCache[catID], $select );
                    } else {
                        bff.ajax('<?= $this->adminLink('listing&act=category-data'); ?>', {'cat_id': catID}, function(data){
                            if(data && data.success) {
                                catView( (catCache[catID] = data), $select );
                            }
                        }, function(){
                            $progress.toggle();
                        });
                    }
                }

            };
        }());
</script>