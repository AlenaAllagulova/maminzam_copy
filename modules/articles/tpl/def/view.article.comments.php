<?php
    $nUserID = User::id();
    if(empty($comments_enabled)): ?>
        <div class="alert alert-info mrgt20">
            <?= _t('', 'Комментарии закрыты'); ?>
        </div>
    <? return; endif; ?>

    <? if($nUserID): ?>
    <div class="l-borderedBlock no-color j-comment">
        <header class="l-inside j-add-title">
            <a href="#j-add-comment" class="btn btn-link ajax-link o-btn-propose j-add-comment" data-toggle="collapse" data-parent="#accordion"><span><?= _t('', 'Опубликовать свой комментарий'); ?></span></a>
        </header>
        <div class="collapse" id="j-add-comment">
            <form class="form j-add-comment-form" role="form" method="post" action="">
                <input type="hidden" name="id" value="<?= $id ?>" />
                <div class="l-inside">
                      <div class="form-group j-required">
                        <label for="text" class="control-label"><?= _t('', 'Ваш комментарий'); ?>:</label>
                        <textarea name="message" rows="5" id="text" class="form-control"></textarea>
                      </div>
                      <button class="btn btn-primary mrgr10 j-submit"><i class="fa fa-check"></i> <?= _t('', 'Опубликовать'); ?></button>
                      <a href="#addcomment" class="ajax-link j-cancel" data-toggle="collapse" data-parent="#accordion"><span><?= _t('form', 'Отмена'); ?></span></a>
                </div>
            </form>
        </div>
    </div>
    <? endif; ?>

    <header class="title">
        <h4><i class="fa fa-comments-o"></i> <?= _t('', 'Комментарии'); ?> <span class="o-count-proposals"><?= $comments['total'] ?></span></h4>
    </header>

    <ul class="o-freelancersList l-comments-list media-list">
        <?= $comments_list ?>
    </ul>