<?php

abstract class ArticlesBase_ extends Module
{
    /** @var ArticlesModel */
    var $model = null;
    var $securityKey = '47a7bd4e3f14ee9b9f4efa8dffb93530';

    # Статус статьи
    const STATUS_ACTIVE  = 1; # активна
    const STATUS_HIDDEN  = 2; # скрыта
    const STATUS_DRAFT   = 3; # черновик
    const STATUS_BLOCKED = 5; # заблокирована

    public function init()
    {
        parent::init();

        $this->module_title = _t('articles','Статьи');

        bff::autoloadEx(array(
            'ArticlesImages'   => array('app', 'modules/articles/articles.images.php'),
            'ArticlesTags'     => array('app', 'modules/articles/articles.tags.php'),
            'ArticlesComments' => array('app', 'modules/articles/articles.comments.php'),
        ));
    }

    /**
     * Shortcut
     * @return Articles
     */
    public static function i()
    {
        return bff::module('Articles');
    }

    /**
     * Shortcut
     * @return ArticlesModel
     */
    public static function model()
    {
        return bff::model('Articles');
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic).'/articles/';
        switch ($key) {
            # Список
            case 'list':
                $url .= ( ! empty($opts['keyword']) ? $opts['keyword'].'/' : '' ) .
                     static::urlQuery($opts, array('keyword'));
                break;
            # Просмотр статьи
            case 'view':
                $url .= $opts['id'] . '-' . $opts['keyword'] . '.html'.
                    static::urlQuery($opts, array('id', 'keyword'));
                break;
            # Добавление статьи
            case 'add':
                $url .= 'add' . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Информация о статусе статьи
            case 'status':
                $url .= 'status' . static::urlQuery($opts);
                break;
            # Редактирование статьи
            case 'edit':
                $url .= 'edit' . static::urlQuery($opts);
                break;
            # Поиск статей по тегу
            case 'tag':
                $opts['tag'] = mb_strtolower($opts['tag']).'-'.$opts['id'];
                $url .= static::urlQuery($opts, array('id'));
                break;
            # Поиск статей по категории
            case 'search-cat':
                $url .= $opts['cat1_keyword'].'/'.( ! empty($opts['cat2_keyword']) ? $opts['cat2_keyword'].'/' : '');
                break;
        }
        
        return bff::filter('articles.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Ограничение кол-ва тегов на главной странице раздела в блоке слева.
     * Также в соответствующем блоке на странице "Карта сайта".
     * @return int
     */
    public static function tagsLimitIndex()
    {
        return config::sysAdmin('articles.tags.limit.index', 30, TYPE_UINT);
    }

    /**
     * Описание seo шаблонов страниц
     * @return array
     */
    public function seoTemplates()
    {
        return array(
            'pages'  => array(
                'index'  => array(
                    't'      => 'Список (главная)',
                    'list'   => true,
                    'macros' => array(),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'listing-category'  => array(
                    't'      => 'Список (категория)',
                    'list'   => true,
                    'inherit' => true,
                    'macros' => array(
                        'category' => array('t' => 'Название категории'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'view' => array(
                    't'      => 'Просмотр статьи',
                    'macros' => array(
                        'title'       => array('t' => 'Заголовок (до 50 символов)'),
                        'title.full'  => array('t' => 'Заголовок (полный)'),
                        'description' => array('t' => 'Описание (до 150 символов)'),
                        'tags'        => array('t' => 'Теги'),
                    ),
                    'fields' => array(
                        'share_title'       => array(
                            't'    => 'Заголовок (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                        'share_description' => array(
                            't'    => 'Описание (поделиться в соц. сетях)',
                            'type' => 'textarea',
                        ),
                        'share_sitename'    => array(
                            't'    => 'Название сайта (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                    ),
                ),
            ),
        );
    }

    /**
     * Включен ли раздел "Статьи"
     * @return bool true - включен
     */
    public static function enabled()
    {
        return config::sysAdmin('articles.enabled', true, TYPE_BOOL);
    }

    /**
     * Использовать премодерацию статей
     * @return bool
     */
    public static function premoderation()
    {
        return config::sysAdmin('articles.premoderation', true, TYPE_BOOL);
    }

    /**
     * Использовать публикатор для создания/редактирования статей
     */
    public static function publicatorEnabled()
    {
        return config::sysAdmin('articles.publicator', false, TYPE_BOOL);
    }

    /**
     * Максимально допустимое кол-во фотографий прикрепляемых к статье
     * 0 - возможность прикрепления фотографий выключена (недоступна)
     * @return integer
     */
    public static function imagesLimit()
    {
        return config::sysAdmin('articles.images.limit', 8, TYPE_UINT);
    }

    public function sendmailTemplates()
    {
        $aTemplates = array(
            'article_new' => array(
                'title'       => 'Статьи: Новая статья',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> после добавления статьи',
                'vars'        => array(
                    '{name}'      => 'Имя пользователя',
                    '{id}'        => 'ID статьи',
                    '{title}'     => 'Заголовок статьи',
                    '{view_link}' => 'Ссылка для просмотра статьи',
                ),
                'impl'        => true,
                'priority'    => 110,
                'enotify'     => Users::ENOTIFY_GENERAL,
            ),
            'article_block' => array(
                'title'       => 'Статьи: Статья заблокирована модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при блокировке статьи модератором',
                'vars'        => array(
                    '{name}'      => 'Имя пользователя',
                    '{id}'        => 'ID статьи',
                    '{title}'     => 'Заголовок статьи',
                    '{view_link}' => 'Ссылка для просмотра статьи',
                    '{reason}'    => 'Причина блокировки',
                ),
                'impl'        => true,
                'priority'    => 111,
                'enotify'     => Users::ENOTIFY_GENERAL,
            ),
            'article_approve' => array(
                'title'       => 'Статьи: Статья одобрена модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> после одобрения статьи модератором',
                'vars'        => array(
                    '{name}'      => 'Имя пользователя',
                    '{id}'        => 'ID статьи',
                    '{title}'     => 'Заголовок статьи',
                    '{view_link}' => 'Ссылка для просмотра статьи',
                ),
                'impl'        => true,
                'priority'    => 112,
                'enotify'     => Users::ENOTIFY_GENERAL,
            ),
        );

        return $aTemplates;
    }

    /**
     * Инициализация компонента работы с тегами
     * @return ArticlesTags
     */
    public function articleTags()
    {
        static $i;
        if (!isset($i)) {
            $i = new ArticlesTags();
            if (!bff::adminPanel()) {
                $i->module_dir_tpl = $this->module_dir_tpl;
            }
        }
        return $i;
    }

    /**
     * Инициализация компонента работы с комментариями к статьям
     * @return ArticlesComments component
     */
    public function articleComments()
    {
        static $i;
        if (!isset($i)) {
            $i = new ArticlesComments();
        }
        return $i;
    }

    /**
     * Инициализация компонента обработки фотографий постов ArticlesImages
     * @param mixed $nArticleID ID поста
     * @return ArticlesImages component
     */
    public static function articlesImages($nArticleID = false)
    {
        static $i;
        if( ! isset($i)) {
            $i = new ArticlesImages();
        }
        $i->setRecordID($nArticleID);
        return $i;
    }

    /**
     * Является ли текущий пользователь владельцем статьи
     * @param integer $nArticleID ID поста
     * @param integer|bool $nArticleUserID ID пользователя статьи или FALSE (получаем из БД)
     * @return boolean
     */
    public function isArticleOwner($nArticleID, $nArticleUserID = false)
    {
        $nUserID = User::id();
        if (!$nUserID) {
            return false;
        }

        if ($nArticleUserID === false) {
            $aData = $this->model->articleData($nArticleID, array('user_id'));
            if (empty($aData)) {
                return false;
            }

            $nArticleUserID = $aData['user_id'];
        }

        return ($nArticleUserID > 0 && $nUserID == $nArticleUserID);
    }

    /**
     * Удаление статьи
     * @param integer $nArticleID ID статьи
     * @return boolean
     */
    public function articleDelete($nArticleID)
    {
        if (empty($nArticleID)) return false;

        $publicator = false;
        if (static::publicatorEnabled()) {
            $publicator = $this->initPublicator();
            $data = $this->model->articleData($nArticleID, array('content'));
        }

        $res = $this->model->articleDelete($nArticleID);
        if (!empty($res)) {
            if ($publicator) {
                $publicator->dataDelete($data['content'], $nArticleID);
            }
            $this->articleTags()->onItemDelete($nArticleID);
            $this->articlesImages($nArticleID)->deleteAllImages(false);

            # обновляем счетчик статей "на модерации"
            $this->moderationCounterUpdate();

            return true;
        }

        return false;
    }

    /**
     * Актуализация счетчика статей ожидающих модерации
     * @param integer|null $increment
     */
    public function moderationCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->articlesModeratingCounter();
            config::save('articles_items_moderating', $count, true);
        } else {
            config::saveCount('articles_items_moderating', $increment, true);
        }
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nArticleID ID cтатьи или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @param bff\db\Publicator|boolean $publicator или FALSE
     * @return array параметры
     */
    protected function validateArticleData($nArticleID, $bSubmit, $publicator = false)
    {
        $aData = array();
        $aParam = array(
            'cat_id'  => TYPE_UINT,   # Категория
            'title'   => TYPE_NOTAGS, # Заголовок
            'content' => $publicator ? TYPE_ARRAY : TYPE_STR, # Содержание
            'comments_enabled' => TYPE_BOOL, # Комментарии включены
            'noindex' => TYPE_BOOL, # Закрыть от индексирования
        );
        if (!$nArticleID) {
            if (bff::adminPanel()) {
                $aParam['user_id'] = TYPE_UNUM;
            }
        }
        $this->input->postm($aParam, $aData);

        if ($bSubmit)
        {
            # Категория
            $nCategoryID = $aData['cat_id'];
            if ( ! $nCategoryID) {
                $this->errors->set(_t('articles','Выберите категорию'));
            } else {
                # проверяем наличие подкатегорий
                $nSubsCnt = $this->model->categorySubCount($nCategoryID);
                if ($nSubsCnt > 0) {
                    $this->errors->set(_t('articles','Выбранная категория не должна содержать подкатегории'));
                } else {
                    # сохраняем ID категорий(parent и текущей), для возможности дальнейшего поиска по ним
                    $nParentsID = $this->model->categoryParentsID($nCategoryID, true);
                    foreach ($nParentsID as $lvl=>$id) {
                        $aData['cat_id'.$lvl] = $id;
                    }
                }
            }

            if ( ! $nArticleID) {
                if ( ! bff::adminPanel()) {
                    $aData['user_id'] = User::id();
                }

                if ( ! $aData['user_id']) {
                    $this->errors->set(_t('articles','Укажите пользователя'));
                }
            }

            if ($publicator) {
                $data = $publicator->dataPrepare($aData['content'], $nArticleID);
                $aData['content'] = $data['content'];
                $aData['content_search'] = $data['content_search'][LNG];
                $aData['content_preview'] = nl2br(tpl::truncate($aData['content_search'], config::sysAdmin('articles.content_preview.truncate', 300, TYPE_UINT)));
                if (mb_strlen($aData['content_search']) < config::sysAdmin('articles.content.min', 10, TYPE_UINT)) {
                    $this->errors->set(_t('articles', 'Текст слишком короткий'));
                }
            } else {
                # Формируем краткий текст для списков
                $preview = strip_tags(preg_replace('/(\<br(\s*)?\/?\>|\<p\>|\<\/p\>)/mui', "\n", $aData['content']));
                $preview = trim(preg_replace("/\n+/u", "\n", $preview), "\n ");
                $aData['content_preview'] = nl2br(tpl::truncate($preview, config::sysAdmin('articles.content_preview.truncate', 300, TYPE_UINT)));
                if(mb_strlen($aData['content']) < config::sysAdmin('articles.content.min', 10, TYPE_UINT)){
                    $this->errors->set(_t('articles', 'Текст слишком короткий'));
                }
            }

            # Чистим текст
            if ( ! bff::adminPanel()) {

                # антиспам фильтр
                Site::i()->spamFilter(array(
                    array('text' => & $aData['title'],   'error'=>_t('', 'В указанном вами заголовке присутствует запрещенное слово "[word]"')),
                    array('text' => & $aData[($publicator ? 'content_search' : 'content')], 'error'=>_t('', 'В указанном вами тексте присутствует запрещенное слово "[word]"')),
                ));

                if ( ! $publicator) {
                    $parser = new \bff\utils\TextParser();
                    $aData['content'] = $parser->parseWysiwygTextFrontend($aData['content']);
                }
            }

            # URL-Keyword
            $aData['keyword'] = mb_strtolower(func::translit($aData['title']));
            $aData['keyword'] = preg_replace('/[^a-zA-Z0-9_\-]/', '', $aData['keyword']);

        } else {
            if (!$nArticleID) {
                $aData['comments_enabled'] = 1;
            }
        }
        return $aData;
    }

    /**
     * Инициализируем компонент Publicator
     * @return bff\db\Publicator
     */
    public function initPublicator()
    {
        static $i;
        if ( ! isset($i)) {
            $aSettings = array(
                'title' => false,
                'langs' => $this->locale->getLanguages(),
                'images_path'     => bff::path('articles', 'images'),
                'images_path_tmp' => bff::path('tmp', 'images'),
                'images_url'      => bff::url('articles', 'images'),
                'images_url_tmp'  => bff::url('tmp', 'images'),
                # photo
                'photo_sz_view' => array('width' => 800),
                # gallery
                'gallery_sz_view' => array(
                    'width'    => 800,
                    'height'   => false,
                    'vertical' => array('width' => false, 'height' => 400),
                    'quality'  => 95,
                    'sharp'    => array(), // no sharp
                ),
                'video_width'  => 560,
                'video_height' => 315,
                'use_wysiwyg'  => true,
                'controls' => array(
                    bff\db\Publicator::blockTypeText,
                    bff\db\Publicator::blockTypePhoto,
                    bff\db\Publicator::blockTypeGallery,
                    bff\db\Publicator::blockTypeVideo,
                ),
            );

            $aSettingsConfig = config::sys('articles.publicator.settings', array());
            if (!empty($aSettingsConfig) && is_array($aSettingsConfig)) {
                $aSettings = array_merge($aSettings, $aSettingsConfig);
            }

            # настройки водяного знака
            $watermark = Site::i()->watermarkSettings('articles');
            if (!empty($watermark)) {
                $aWatermark = array(
                    'watermark' => true,
                    'watermark_src' => $watermark['file']['path'],
                    'watermark_pos_x' => $watermark['pos_x'],
                    'watermark_pos_y' => $watermark['pos_y'],
                );
                $aSettings['gallery_sz_view'] += $aWatermark;
                $aSettings['photo_sz_view'] += $aWatermark;
            }

            $i = $this->attachComponent('publicator', new bff\db\Publicator($this->module_name, $aSettings));
        }
        return $i;
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('articles', 'images') => 'dir-split', # изображения
            bff::path('tmp', 'images')  => 'dir-only', # tmp
        ));
    }

}