<?php

class Articles_ extends ArticlesBase
{
    public function init()
    {
        parent::init();

        if (bff::$class == $this->module_name && Request::isGET()) {
            bff::setActiveMenu('//more/articles');
        }
    }

    /**
     * Поиск статей и просмотр общего списка
     */
    public function search()
    {
        $this->checkEnabled();

        $nPageSize = config::sysAdmin('articles.search.pagesize', 10, TYPE_UINT);
        $aData = array('list'=>array(), 'pgn'=>'', 'cat_id'=>0, 'cat_id1'=>0, 'cat_id2'=>0, 'tag_id'=>0);
        $aFilter = array();

        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT,   # № страницы
            'tag'  => TYPE_NOTAGS, # тег "keyword-id"
        ));
        $aData['f'] = &$f;

        # Фильтр по категории / подкатегории
        $f['cat'] = $this->input->getpost('cat', TYPE_NOTAGS); # keyword категории или категории/подкатегории
        $f['cat'] = trim($f['cat'], ' /');
        if ($f['cat']) {
            $sCat = ( ($pos = mb_strpos($f['cat'], '/')) ? mb_substr($f['cat'], $pos + 1) : $f['cat'] );
            $aCat = $aData['cat'] = $this->model->categoryMeta($sCat);
            if (empty($aCat) && ! Request::isAJAX()) {
                $this->errors->error404();
            }
            $aFilter['cat_id'.$aCat['numlevel']] = $aData['cat_id'] = $aData['cat_id'.$aCat['numlevel']] = $aCat['id'];
            if ($aCat['numlevel'] == 2) $aData['cat_id1'] = $aCat['pid'];
        }

        if ($f['tag']) {
            if (preg_match('/(.*)-(\d+)/', $f['tag'], $matches) && ! empty($matches[2])) {
                $aTagData = $this->articleTags()->tagData($matches[2]);
                if (empty($aTagData) || mb_strtolower($aTagData['tag']) != $matches[1]) $this->errors->error404();
                $aFilter['tag'] = $aData['tag_id'] = $aTagData['id'];
                $this->seo()->robotsIndex(false);
            } else {
                $this->errors->error404();
            }
        }

        # Категории
        $aData['cats'] = $this->model->categoriesListAll(array('id','pid','keyword','title'));
        foreach ($aData['cats'] as &$v) {
            $v['url'] = static::url('list', array('keyword'=>$v['keyword']));
            if ( ! empty($v['sub'])) {
                foreach ($v['sub'] as &$vv) {
                    $vv['keyword'] = $v['keyword'].'/'.$vv['keyword'];
                    $vv['url'] = static::url('list', array('keyword'=>$vv['keyword']));
                    if ($vv['id'] == $aData['cat_id']) $aData['cat']['keyword'] = $vv['keyword'];
                } unset($vv);
            }
        } unset($v);

        $nCount = $this->model->articlesList($aFilter, true);
        if ($nCount) {
            $pgnQuery = $f; unset($pgnQuery['cat']);
            $pgn = new Pagination($nCount, $nPageSize, array(
                'link'  => static::url('list', array('keyword'=>($aData['cat_id'] ? $aData['cat']['keyword'] : ''))),
                'query' => $pgnQuery,
            ));
            $aData['list'] = $this->model->articlesList($aFilter, false, $pgn->getLimitOffset(), 'A.created DESC, A.votes DESC');
            foreach ($aData['list'] as &$v) {
                $v['url_view'] = static::url('view', array('id'=>$v['id'], 'keyword'=>$v['keyword']));
            } unset($v);
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }

        $aData['list'] = $this->viewPHP($aData, 'search.list');
        $aData['cnt'] = $nCount;

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'  => $aData['pgn'],
                'list' => $aData['list'],
            ));
        }

        # SEO:
        $seoData = array('page' => $f['page']);
        if ($aData['cat_id']) {
            # Список (категория)
            $url = static::url('list', array('keyword'=>$aData['cat']['keyword']), true);
            $this->urlCorrection(static::urlDynamic($url));
            $this->seo()->canonicalUrl($url, $seoData);
            $this->setMeta('listing-category', array(
                'category' => $aData['cat']['title'],
                'page'     => $f['page'],
            ), $aData['cat']);
            $this->seo()->setText($aData['cat'], $f['page']);
        } else {
            # Список (главная)
            $this->urlCorrection(static::url('list'));
            $this->seo()->canonicalUrl(static::url('list', array(), true), $seoData);
            $this->setMeta('index', $seoData, $seoData);
            $this->seo()->setText($seoData, $f['page']);
        }

        # Теги
        $aData['tags'] = $this->articleTags()->tagsCloud(12, NULL, static::tagsLimitIndex());

        $aData['form'] = $this->viewPHP($aData, 'search.form');
        return $this->viewPHP($aData, 'search');
    }

    /**
     * Добавление статьи
     */
    public function add()
    {
        $this->checkEnabled();

        bff::setMeta(_t('articles', 'Добавление статьи'));
        $this->security->setTokenPrefix('articles-article-form');

        $publicator = false;
        if (static::publicatorEnabled()) {
            $publicator = $this->initPublicator();
        }

        $aData = $this->validateArticleData(0, Request::isPOST(), $publicator);
        $nUserID = User::id();

        if (Request::isPOST()) {
            do {
                $aResponse = array();

                if ( ! $this->errors->no()) {
                    break;
                }

                $func = $this->input->post('func', TYPE_STR);
                if ($func == 'preview' || $func == 'draft') {
                    $aData['status'] = static::STATUS_DRAFT;
                } else {
                    $aData['status'] = static::STATUS_ACTIVE;
                }

                # доверенный пользователь: без модерации
                if ($trusted = User::isTrusted($this->module_name)) {
                    $aData['moderated'] = 1;
                }

                # создаем статью
                $nArticleID = $this->model->articleSave(0, $aData);
                if ( ! $nArticleID) {
                    $this->errors->reloadPage();
                    break;
                }

                # отправим пользователю письмо
                Users::sendMailTemplateToUser(User::id(), 'article_new', array(
                    'id' => $nArticleID,
                    'title' => $aData['title'],
                    'view_link' => static::url('view', array('id' => $nArticleID, 'keyword' => $aData['keyword'])),
                ), Users::ENOTIFY_GENERAL);

                if ($publicator) {
                    # переносим фотографии в постоянную папку
                    $publicator->dataUpdate($aData['content'], $nArticleID);
                }

                # загружаем изображения
                if (static::imagesLimit()) {
                    # сохраняем / загружаем изображения
                    $oImages = $this->articlesImages($nArticleID);
                    $oImages->setAssignErrors(false);
                    if ($this->input->post('images_type', TYPE_STR) == 'simple') {
                        # загружаем
                        if (!empty($_FILES)) {
                            for ($i = 1; $i <= $oImages->getLimit(); $i++) {
                                $oImages->uploadFILES('images_simple_' . $i);
                            }
                            # удаляем загруженные через "удобный способ"
                            $aImages = $this->input->post('images', TYPE_ARRAY_STR);
                            $oImages->deleteImages($aImages);
                        }
                    } else {
                        # перемещаем из tmp-директории в постоянную
                        $oImages->saveTmp('images');
                    }
                }

                # теги
                $this->articleTags()->tagsSave($nArticleID);

                # обновляем счетчик статей "на модерации"
                if (!$trusted) {
                    $this->moderationCounterUpdate(1);
                }

                $aResponse['redirect'] = static::url('status', array('id' => $nArticleID));
                if ($func == 'preview') {
                    $aResponse['redirect'] = static::url('view', array('id' => $nArticleID, 'keyword' => $aData['keyword']));
                }

            } while (false);
            $this->iframeResponseForm($aResponse);
        }

        if( ! $nUserID) {
            return $this->showForbiddenGuests();
        }

        return $this->form(0, $aData);
    }

    /**
     * Редактирование статьи
     */
    public function edit()
    {
        $this->checkEnabled();

        $publicator = false;
        if (static::publicatorEnabled()) {
            $publicator = $this->initPublicator();
        }

        bff::setMeta(_t('articles', 'Редактирование статьи'));
        $this->security->setTokenPrefix('articles-article-form');

        if ( ! User::id()) {
            $this->redirect(Users::url('login'), false, true);
        }

        $nArticleID = $this->input->get('id', TYPE_UINT);
        if ( ! $nArticleID) {
            $this->errors->error404();
        }

        $aArticleData = $this->model->articleData($nArticleID, array(), true);
        if (empty($aArticleData)) {
            $this->errors->error404();
        }

        if ( ! $this->isArticleOwner($nArticleID, $aArticleData['user_id'])) {
            if (Request::isPOST()) {
                $this->errors->reloadPage();
                $this->ajaxResponseForm();
            }
            return $this->showForbidden('', _t('articles', 'Вы не являетесь автором данной статьи'));
        }

        if (Request::isPOST()) {
            $aData = $this->validateArticleData($nArticleID, true, $publicator);
            $aResponse = array();

            if ($this->errors->no()) {

                $func = $this->input->post('func', TYPE_STR);

                if ($aArticleData['status'] == static::STATUS_BLOCKED) {
                    # статья заблокирована, помечаем на проверку модератору
                    $aData['moderated'] = 0;
                }

                # доверенный пользователь: без модерации
                $trusted = User::isTrusted($this->module_name);
                # помечаем на модерацию при изменении: названия, описания
                if ($aArticleData['status'] == static::STATUS_ACTIVE) {
                    if ($aData['title'] != $aArticleData['title'] || $aData['content'] != $aArticleData['content']) {
                        if ($aArticleData['moderated'] != 0 && !$trusted) {
                            $aData['moderated'] = 2;
                        }
                    }
                }

                # проверим необходимость публикации чероновика
                if ($aArticleData['status'] == static::STATUS_DRAFT) {
                    if ($func == 'publicate') {
                        $aData['status'] = static::STATUS_ACTIVE;
                        if ($trusted) {
                            $aData['moderated'] = 1;
                        } else {
                            $aData['moderated'] = 0;
                        }
                    }
                }


                $this->model->articleSave($nArticleID, $aData);

                if (static::imagesLimit()) {
                    # сохраняем / загружаем изображения
                    $oImages = $this->articlesImages($nArticleID);
                    if ($this->input->post('images_type', TYPE_STR) == 'simple') {
                        # загружаем
                        if (!empty($_FILES) && $aArticleData['imgcnt'] < $oImages->getLimit()) {
                            for ($i = 1; $i <= $oImages->getLimit(); $i++) {
                                $oImages->uploadFILES('images_simple_' . $i);
                            }
                        }
                    } else {
                        # сохраняем порядок изображений
                        $aImages = $this->input->post('images', TYPE_ARRAY_STR);
                        $oImages->saveOrder($aImages, false);
                    }

                    # помечаем на модерацию при изменении изображений
                    if ($oImages->newImagesUploaded($this->input->post('images_hash', TYPE_STR))
                        && $aArticleData['moderated']!=0 && !$trusted) {
                        $this->model->articleSave($nArticleID, array('moderated' => 2));
                    }
                }

                # теги
                $this->articleTags()->tagsSave($nArticleID);

                # обновляем счетчик статей "на модерации"
                $this->moderationCounterUpdate();

                $aResponse['redirect'] = static::url('view', array('id' => $nArticleID, 'keyword' => $aData['keyword']));
                if ($func == 'preview') {
                    $aResponse['redirect'] = static::url('view', array('id' => $nArticleID, 'keyword' => $aData['keyword']));
                }

            }
            $this->iframeResponseForm($aResponse);
        }

        return $this->form($nArticleID, $aArticleData);
    }

    /**
     * Форма статьи
     * @param integer $nArticleID ID статьи
     * @param array $aData @ref
     * @return string HTML
     */
    protected function form($nArticleID, array &$aData)
    {
        $aData['id'] = $nArticleID;
        $aData['img'] = $this->articlesImages($nArticleID);

        if ($nArticleID) {
            if (static::imagesLimit()) {
                $aImages = $aData['img']->getData($aData['imgcnt']);
                $aData['images'] = array();
                foreach ($aImages as $v) {
                    $aData['images'][] = array(
                        'id'       => $v['id'],
                        'tmp'      => false,
                        'filename' => $v['filename'],
                        'i'        => $aData['img']->getURL($v, ArticlesImages::szForm, false)
                    );
                }
                $aData['imghash'] = $aData['img']->getLastUploaded();
            }
        } else {
            $aData['images'] = array();
            $aData['imgcnt'] = 0;
            $aData['imghash'] = '';
        }
        $aData['edit'] = ! empty($nArticleID);
        if (static::publicatorEnabled()) {
            $publicator = $this->initPublicator();
            if ($aData['edit'] && empty($aData['content_search'])) {
                $aData['content'] = $publicator->textConvert($aData['content']);
            }
            $aData['publicator'] = $publicator->formFrontend($aData['content'], $nArticleID, 'content', 'jArticleFormPublicator');
        }
        
        return $this->viewPHP($aData, 'form');
    }

    /**
     * Вывод сообщения о статусе статьи
     */
    public function status()
    {
        $this->checkEnabled();

        $nArticleID = $this->input->get('id', TYPE_UINT);
        if (!$nArticleID) {
            $this->errors->error404();
        }

        $nUserID = User::id();
        if( ! $nUserID){
            return $this->showForbiddenGuests();
        }

        $aData = $this->model->articleData($nArticleID, array('id', 'moderated', 'keyword', 'status'));
        if (empty($aData)) {
            $this->errors->error404();
        }

        if ($aData['moderated'] > 0) {
            $this->redirect(static::url('view', array('id'=>$aData['id'], 'keyword'=>$aData['keyword'])));
        }

        return $this->viewPHP($aData, 'status');
    }

    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR)) {
            case 'tags-autocomplete':
            {
                $sQuery = $this->input->post('q', TYPE_STR);
                $this->articleTags()->tagsAutocomplete($sQuery);
            } break;
            case 'vote': # голосование за статью
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nUserID = User::id();
                $p = $this->input->postm(array(
                    'id' => TYPE_UINT,  # ID статьи
                    't'  => TYPE_STR,   # Тип голоса
                ));
                switch ($p['t']) {
                    case 'good': $vote = 1; break;
                    case 'bad':  $vote = -1; break;
                    default:     $vote = 0; break;
                }
                if( ! $vote || ! $p['id']){
                    $this->errors->reloadPage();
                    break;
                }

                $nArticleID = $p['id'];
                $nArticleData = $this->model->articleData($nArticleID, array('user_id'));
                if ($nArticleData['user_id'] == $nUserID) {
                    $aResponse['disabled'] = 1;
                    break;
                }
                # проверка, не голосовал ли пользователь за данную статью
                $nSum = $this->model->votesSum(array('user_id'=>$nUserID, 'article_id'=>$nArticleID));
                if ($nSum) {
                    $aResponse['disabled'] = 1;
                    break;
                }

                $nVotesSum = $this->model->voteSave(array(
                    'article_id'=>$nArticleID,
                    'user_id' => $nUserID,
                    'vote' => $vote,
                ));
                if ($nVotesSum !== false) {
                    $aResponse['sum'] = $nVotesSum;
                    $aResponse['disabled'] = 1;
                }
            } break;
            case 'article-delete': # удаление статьи
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nArticleID = $this->input->post('id', TYPE_UINT);
                $aData = $this->model->articleData($nArticleID, array('user_id'));
                if (empty($aData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $this->isArticleOwner($nArticleID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $this->articleDelete($nArticleID)) {
                    $this->errors->reloadPage();
                    break;
                }

                $aResponse['redirect'] = static::url('list');

            } break;
            case 'article-hide': # скрыть статью
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nArticleID = $this->input->post('id', TYPE_UINT);
                $aData = $this->model->articleData($nArticleID, array('user_id', 'status'));
                if (empty($aData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $this->isArticleOwner($nArticleID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if($aData['status'] != static::STATUS_ACTIVE) {
                    $this->errors->reloadPage();
                    break;
                }

                $aUpdate = array();
                $aUpdate['status_prev'] = $aData['status'];
                $aUpdate['status'] = static::STATUS_HIDDEN;
                $aUpdate['status_changed'] = $this->db->now();

                $this->model->articleSave($nArticleID, $aUpdate);
            } break;
            case 'article-show': # отобразить статью
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nArticleID = $this->input->post('id', TYPE_UINT);
                $aData = $this->model->articleData($nArticleID, array('user_id', 'status'));
                if (empty($aData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $this->isArticleOwner($nArticleID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! in_array($aData['status'], array(static::STATUS_HIDDEN, static::STATUS_DRAFT))) {
                    $this->errors->reloadPage();
                    break;
                }

                $aUpdate = array();
                $aUpdate['status_prev'] = $aData['status'];
                $aUpdate['status'] = static::STATUS_ACTIVE;
                $aUpdate['status_changed'] = $this->db->now();
                if ($aData['status'] == static::STATUS_DRAFT) {
                    $aUpdate['moderated'] = User::isTrusted($this->module_name) ? 1 : 0;
                }

                $this->model->articleSave($nArticleID, $aUpdate);

                if ($aData['status'] == static::STATUS_DRAFT) {
                    $this->moderationCounterUpdate();
                    $aResponse['redirect'] = static::url('status', array('id' => $nArticleID));
                }

            } break;
            case 'comment-add': # комментарии: добавление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                $sMessage = $this->input->post('message', TYPE_TEXT, array('len' => 2500, 'len.sys' => 'articles.comment.message.limit'));
                $min = config::sys('articles.comment.message.min', 10, TYPE_UINT);
                if (mb_strlen($sMessage) < $min) {
                    $this->errors->set(_t('', 'Комментарий не может быть короче [min] символов', array('min' => $min)), 'message');
                    break;
                }

                # антиспам фильтр
                if(Site::i()->spamFilter(array(
                    array('text' => & $sMessage,   'error'=>_t('', 'В указанном вами комментарии присутствует запрещенное слово "[word]"')),
                ))) {
                    break;
                }

                $nArticleID = $this->input->post('id', TYPE_UINT);
                if ( ! $nArticleID) {
                    $this->errors->reloadPage();
                }

                $aArticleData = $this->model->articleData($nArticleID, array('status', 'comments_enabled', 'user_id'));
                if (empty($aArticleData)) {
                    $this->errors->reloadPage();
                }

                if ($aArticleData['status'] != static::STATUS_ACTIVE) {
                    $this->errors->impossible();
                }
                if ( ! $aArticleData['comments_enabled']) {
                    $this->errors->set(_t('articles', 'Добавление комментариев запрещено автором статьи'), 'message');
                    break;
                }

                # не чаще чем раз в {X} секунд с одного IP (для одного пользователя)
                if (Site::i()->preventSpam('articles-comment', config::sysAdmin('articles.prevent.spam', 10, TYPE_UINT))) {
                    break;
                }

                $nParent = $this->input->post('parent', TYPE_UINT);

                $aData = array(
                    'message' => $sMessage,
                    'name' => User::data('name'),
                );

                $nCommentID = $this->articleComments()->commentInsert($nArticleID, $aData, $nParent);
                if ($nCommentID) {
                    # оставили комментарий
                    $aCommentsData = array(
                        'aComments' => array(array(
                            'created'  => $this->db->now(),
                            'message'  => nl2br($aData['message']),
                            'name'     => User::data('name'),
                            'surname'  => User::data('surname'),
                            'pro'      => User::data('pro'),
                            'login'    => User::data('login'),
                            'sex'      => User::data('sex'),
                            'avatar'   => User::data('avatar'),
                            'last_activity' => User::data('last_activity'),
                            'deleted'  => 0,
                            'numlevel' => $nParent ? 2 : 1,
                            'user_id'  => User::id(),
                            'id'       => $nCommentID,
                        )),
                        'nArticleOwnerID' => $aArticleData['user_id'],
                        'nItemID'      => $nArticleID,
                    );
                    $aResponse['html'] = $this->commentsList($aCommentsData);
                }

            } break;
            case 'comment-delete': # комментарии: удаление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                $nCommentID = $this->input->post('id', TYPE_UINT);
                if ( ! $nCommentID) {
                    $this->errors->reloadPage();
                    break;
                }

                $nArticleID = $this->input->post('article_id', TYPE_UINT);
                if ( ! $nArticleID) {
                    $this->errors->reloadPage();
                    break;
                }

                $aArticleData = $this->model->articleData($nArticleID, array('status', 'comments_enabled', 'user_id'));
                if (empty($aArticleData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $aArticleData['comments_enabled']) {
                    $this->errors->reloadPage();
                    break;
                }

                $oComments = $this->articleComments();
                $aCommentData = $oComments->commentData($nArticleID, $nCommentID);
                if (empty($aCommentData)) {
                    $this->errors->reloadPage();
                    break;
                }
                $nUserID = User::id();
                if ($aCommentData['user_id'] == $nUserID) { # владелец комментария
                    $oComments->commentDelete($nArticleID, $nCommentID, ArticlesComments::commentDeletedByCommentOwner);
                } else {
                    if ($aArticleData['user_id'] == $nUserID) { # владелец статьи
                        $oComments->commentDelete($nArticleID, $nCommentID, ArticlesComments::commentDeletedByItemOwner);
                    } else {
                        $this->errors->reloadPage();
                        break;
                    }
                }

                $aCommentsData = $oComments->commentsData($nArticleID, 0, true, $nCommentID);
                if ( ! empty($aCommentsData)) {
                    $aCommentsData['nArticleOwnerID'] = $aArticleData['user_id'];
                    $aCommentsData['nItemID'] = $nArticleID;
                    $aResponse['html'] = $this->commentsList($aCommentsData);
                } else {
                    $aResponse['html'] = '';
                }
            } break;
            case 'comment-restore': # комментарии: восстановление
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                $nCommentID = $this->input->post('id', TYPE_UINT);
                if ( ! $nCommentID) {
                    $this->errors->reloadPage();
                    break;
                }

                $nArticleID = $this->input->post('article_id', TYPE_UINT);
                if ( ! $nArticleID) {
                    $this->errors->reloadPage();
                    break;
                }

                $aArticleData = $this->model->articleData($nArticleID, array('status', 'comments_enabled', 'user_id'));
                if (empty($aArticleData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $aArticleData['comments_enabled']) {
                    $this->errors->reloadPage();
                    break;
                }

                $oComments = $this->articleComments();
                $aCommentData = $oComments->commentData($nArticleID, $nCommentID);
                if (empty($aCommentData)) {
                    $this->errors->reloadPage();
                    break;
                }
                $nUserID = User::id();
                switch ($aCommentData['deleted']) {
                    case ArticlesComments::commentDeletedByItemOwner:
                        if ($aArticleData['user_id'] == $nUserID) {
                            $oComments->restore($nCommentID);
                        } else {
                            $this->errors->reloadPage();
                            break 2;
                        } break;
                    case ArticlesComments::commentDeletedByCommentOwner:
                        if ($aCommentData['user_id'] == $nUserID) {
                            $oComments->restore($nCommentID);
                        } else {
                            $this->errors->reloadPage();
                            break 2;
                        } break;
                    default:
                        $this->errors->reloadPage();
                        break 2;
                }

                $aCommentsData = $oComments->commentsData($nArticleID, 0, true, $nCommentID);
                if ( ! empty($aCommentsData)) {
                    $aCommentsData['nArticleOwnerID'] = $aArticleData['user_id'];
                    $aCommentsData['nItemID'] = $nArticleID;
                    $aResponse['html'] = $this->commentsList($aCommentsData);
                } else {
                    $aResponse['html'] = '';
                }

            } break;
            default:
                $this->errors->impossible();
        }
        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Управление изображениями статьи
     * @param getpost ::uint 'article_id' ID поста
     * @param getpost ::string 'act' действие
     */
    public function img()
    {
        $this->security->setTokenPrefix('articles-article-form');

        $nArticleID = $this->input->getpost('article_id', TYPE_UINT);
        $oImages = $this->articlesImages($nArticleID);
        $aResponse = array();

        switch ($this->input->getpost('act'))
        {
            case 'upload': # загрузка
            {
                $aResponse = array('success' => false);
                do {
                    if (!$this->security->validateToken(true, false)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if ($nArticleID) {
                        if (!$this->isArticleOwner($nArticleID)) {
                            $this->errors->set(_t('article', 'Вы не являетесь автором данной статьи'));
                            break;
                        }
                    }

                    $result = $oImages->uploadQQ();

                    $aResponse['success'] = ($result !== false && $this->errors->no());
                    if ($aResponse['success']) {
                        $aResponse = array_merge($aResponse, $result);
                        $aResponse['tmp'] = empty($nArticleID);
                        $aResponse['i'] = $oImages->getURL($result, ArticlesImages::szForm, $aResponse['tmp']);
                        unset($aResponse['dir'], $aResponse['srv']);
                    }
                } while (false);

                $aResponse['errors'] = $this->errors->get();
                $this->ajaxResponse($aResponse, true, false, true);
            }
                break;
            case 'delete': # удаление
            {
                $nImageID = $this->input->post('image_id', TYPE_UINT);
                $sFilename = $this->input->post('filename', TYPE_STR);

                # неуказан ID изображения ни filename временного
                if (!$nImageID && empty($sFilename)) {
                    $this->errors->reloadPage();
                    break;
                }

                if (!$this->security->validateToken(true, false)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($nArticleID) {
                    # проверяем доступ на редактирование
                    if (!$this->isArticleOwner($nArticleID)) {
                        $this->errors->set(_t('article', 'Вы не являетесь автором данной статьи'));
                        break;
                    }
                }

                if ($nImageID) {
                    # удаляем изображение по ID
                    $oImages->deleteImage($nImageID);
                } else {
                    # удаляем временное
                    $oImages->deleteTmpFile($sFilename);
                }
            }
                break;
            case 'delete-tmp': # удаление временного
            {
                $aFilenames = $this->input->post('filenames', TYPE_ARRAY_STR);
                $oImages->deleteTmpFile($aFilenames);
            }
                break;
            default:
                $this->errors->reloadPage();
        }

        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Просмотр статьи
     * @return string
     */
    public function view()
    {
        $this->checkEnabled();

        $userID = User::id();
        $nArticleID = $this->input->get('id', TYPE_UINT);
        if (!$nArticleID) {
            $this->errors->error404();
        }
        $aData = $this->model->articleData($nArticleID);
        if (empty($aData)) {
            $this->errors->error404();
        }

        $adminView = $this->input->get('adminView', TYPE_STR);
        if ($adminView != md5($aData['user_id'].$aData['id'].$aData['keyword'])) {
            $adminView = false;
        }

        $nUserID = User::id();
        if (!User::isCurrent($aData['user_id']) && !$adminView) {
            if ($aData['status'] != static::STATUS_ACTIVE) {
                $this->errors->error404();
            }
            if (static::premoderation() && ! $aData['moderated']) {
                $this->errors->error404();
            }
        }

        if (static::publicatorEnabled() && ! empty($aData['content_search'])) {
            # содержание
            $aData['content'] = $this->initPublicator()->view($aData['content'], $nArticleID, 'view.content', $this->module_dir_tpl);
        }

        $aData['cat'] = $this->model->categoriesListInArray(array($aData['cat_id1'], $aData['cat_id2']));

        # Хлебные крошки
        $crumbs = array(
            array('title' => _t('articles','Articles'), 'link' => static::url('list')),
        );
        $aLinkData = array();
        foreach ($aData['cat'] as $v) {
            $aLinkData['cat'.$v['numlevel'].'_keyword'] = $v['keyword'];
            $crumbs[] = array('title' => $v['title'], 'link' => static::url('search-cat', $aLinkData));
        }
        $crumbs[] = array('title' => $aData['title'], 'active' => true);
        $aData['breadcrumbs'] = &$crumbs;

        $aData['images'] = array();
        if (static::imagesLimit() && $aData['imgcnt']) {
            $oImages = $this->articlesImages($nArticleID);
            $aImages = $oImages->getData($aData['imgcnt']);
            foreach ($aImages as $v) {
                $aData['images'][] = array(
                    'id'       => $v['id'],
                    'filename' => $v['filename'],
                    'i'        => $oImages->getURL($v, array(
                        ArticlesImages::szView
                    ), false
                    )
                );
            }
        }

        # Теги
        $aData['tags'] = $this->articleTags()->tagsGet($nArticleID);

        # Счетчик просмотров
        if ($userID != $aData['user_id'] && ! $adminView && ! Request::isRefresh()) {
            $this->model->articleSave($nArticleID, array('views_total = views_total + 1'));
        }

        # SEO: Просмотр статьи
        $url = static::url('view', array('id' => $nArticleID, 'keyword' => $aData['keyword']), true);
        $this->urlCorrection(static::urlDynamic($url));
        $this->seo()->canonicalUrl($url);
        $seoTags = array(); foreach ($aData['tags'] as $v) $seoTags[] = $v['tag'];
        $this->setMeta('view', array(
            'title'       => tpl::truncate($aData['title'], config::sysAdmin('articles.view.meta.title.truncate', 50, TYPE_UINT), ''),
            'title.full'  => $aData['title'],
            'description' => tpl::truncate(strip_tags($aData['content_preview']), config::sysAdmin('articles.view.meta.description.truncate', 150, TYPE_UINT), ''),
            'tags'        => join(', ', $seoTags),
        ), $aData);
        # SEO: Open Graph
        $this->seo()->setSocialMetaOG($aData['share_title'], $aData['share_description'], array(), $url, $aData['share_sitename']);

        if ($aData['noindex']) {
            $this->seo()->robotsIndex(false);
        }

        # Автор статьи
        $aData['user'] = Users::model()->userData($aData['user_id'], array('user_id', 'login', 'name', 'surname', 'avatar', 'sex', 'pro', 'verified'));

        # Голос пользователя
        if ($nUserID) {
            $aData['vote'] = $this->model->votesSum(array('user_id' => $nUserID, 'article_id' => $nArticleID));
        } else {
            $aData['vote'] = 0;
        }

        # Комментарии
        $aData['comments_enabled'] = $aData['comments_enabled'] && $aData['status'] == static::STATUS_ACTIVE && $aData['moderated'] > 0;
        if ($aData['comments_enabled']) {
            $aData['comments'] = $this->articleComments()->commentsData($nArticleID, 0 ,true);
            $aData['comments']['nArticleOwnerID'] = $aData['user_id'];
            $aData['comments']['nItemID'] = $nArticleID;
            $aData['comments_list'] = $this->commentsList($aData['comments']);
        }

        return $this->viewPHP($aData, 'view');
    }

    /**
     * Вывод списка комментариев, рекурсивно
     * @param array $aData
     * @return string
     */
    public function commentsList($aData)
    {
        return $this->viewPHP($aData, 'view.article.comments.ajax');
    }

    public function articleStatusBlock(array &$aData)
    {
        if(empty($aData['user_id'])) return '';
        if($aData['user_id'] != User::id()) return '';
        if($aData['status'] == static::STATUS_DRAFT) return '';

        return $this->viewPHP($aData, 'status.block');
    }

    public function best()
    {
        if( ! static::enabled()) return '';
        $aData['list'] = $this->model->bestList();
        return $this->viewPHP($aData, 'best');
    }

    /**
     * Крон задача, для расчета популярных авторов статей
     * Рекомендуемый период: "раз в день"
     */
    public function cron()
    {
        if (!static::enabled()) {
            return;
        }
        if (!bff::cron()) {
            return;
        }
        $this->model->bestGenerate();
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {

        return array(
            'cron' => array('period' => '0 * * * *'),
        );
    }

    /**
     * Проверка, включен ли раздел "Статьи"
     */
    public function checkEnabled()
    {
        if (!static::enabled()) {
            if (Request::isPOST()) {
                $this->errors->reloadPage();
            } else {
                $this->errors->error404();
            }
        }
    }

    /**
     * Данные для карты сайта
     * @return array
     */
    public function getSitemapData()
    {
        $aCats = $this->model->categoriesListAll(array('id','pid','keyword','title'));
        foreach ($aCats as &$v) {
            $v['url'] = static::url('list', array('keyword'=>$v['keyword']));
            if ( ! empty($v['sub'])) {
                foreach ($v['sub'] as &$vv) {
                    $vv['keyword'] = $v['keyword'].'/'.$vv['keyword'];
                    $vv['url'] = static::url('list', array('keyword'=>$vv['keyword']));
                } unset($vv);
            }
        } unset($v);

        $aTags = $this->articleTags()->tagsCloud(12, NULL, static::tagsLimitIndex());
        foreach ($aTags as &$v) {
            $v['url'] = static::url('tag', array('tag' => $v['tag'], 'id' => $v['id']));
        } unset($v);

        return array(
            'categories' => array(
                'cats' => $aCats,
                'link' => static::url('list'),
            ),
            'themes' => array(
                'tags' => $aTags,
                'link' => static::url('list'),
            ),
            'link' => static::url('list'),
        );
    }

}