<?php
    /**
     * @var Specializations $this
     */
    $aData = HTML::escape($aData, 'html', array('keyword'));
    $edit = !empty($id);
    $aTabs = array(
        'info' => 'Основные',
        'seo-users' => 'SEO: Исполнители',
        'seo-qa' => 'SEO: Вопрос-Ответ',
    );
    if (!bff::moduleExists('qa')) {
        unset($aTabs['seo-qa']);
    }
    $aTabs['seo-price'] = 'SEO: Прайс';
?>
<form name="SpecializationsCategoriesForm" id="SpecializationsCategoriesForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
<input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
<input type="hidden" name="save" value="1" />
<input type="hidden" name="id" value="<?= $id ?>" />
<? if (sizeof($aTabs) > 1) { ?>
<div class="tabsBar" id="SpecializationsCategoriesFormTabs">
    <? foreach($aTabs as $k=>$v) { ?>
        <span class="tab<? if($k == 'info') { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
    <? } ?>
</div>
<? } ?>
<!-- таб: Основные -->
<div class="j-tab j-tab-info">
    <table class="admtbl tbledit">
        <tr class="required check-select displaynone">
            <td class="row1 field-title" width="100">Основной раздел<span class="required-mark">*</span>:</td>
            <td class="row2">
                <? if( $edit ) { ?>
                    <input type="hidden" name="pid" id="category-pid" value="<?= $pid ?>" />
                    <span class="bold"><?= $pid_path ?></span>
                <? } else { ?>
                    <select name="pid" id="category-pid"><?= $this->model->categoriesOptions($pid, false, 1) ?></select>
                <? } ?>
            </td>
        </tr>
        <?= $this->locale->buildForm($aData, 'categories-item','
        <tr class="required">
            <td class="row1 field-title">Название<span class="required-mark">*</span>:</td>
            <td class="row2">
                <input class="stretch <?= $key ?>" type="text" id="category-title-<?= $key ?>" name="title[<?= $key ?>]" value="<?= HTML::escape($aData[\'title\'][$key]); ?>" />
            </td>
        </tr>
        '); ?>
        <tr class="required">
            <td class="row1 field-title" width="100">URL-Keyword<span class="required-mark">*</span>:<br /><a href="#" onclick="return bff.generateKeyword('#category-title-<?= LNG ?>', '#category-keyword');" class="ajax desc small"><?= _t('', 'generate') ?></a></td>
            <td class="row2">
                <input class="stretch" type="text" id="category-keyword" name="keyword" value="<?= $keyword ?>" />
            </td>
        </tr>
        <tr>
            <td class="row1 field-title">Специализации:</td>
            <td class="row2">
                <?= $specs ?>
            </td>
        </tr>
        <tr>
            <td class="row1" colspan="2">
                <hr class="cut" />
            </td>
        </tr>
        <tr>
            <td class="row1 field-title"><?= _t('', 'Enabled') ?>:</td>
            <td class="row2">
                <input type="checkbox" id="category-enabled" name="enabled"<? if($enabled){ ?> checked="checked"<? } ?> />
            </td>
        </tr>
    </table>
</div>
<!-- таб: SEO: Исполнители -->
<div class="j-tab j-tab-seo-users hidden">
    <?= SEO::i()->form(Users::i(), $aData, 'search-cat', array('name_prefix'=>'users_')); ?>
</div>
<? if (bff::moduleExists('qa')) { ?>
<!-- таб: SEO: Вопрос-Ответ -->
<div class="j-tab j-tab-seo-qa hidden">
    <?= SEO::i()->form(Qa::i(), $aData, 'listing-cat', array('name_prefix'=>'qa_')); ?>
</div>
<? } ?>
<!-- таб: SEO: Прайс -->
<div class="j-tab j-tab-seo-price hidden">
    <?= SEO::i()->form(Users::i(), $aData, 'price-cat', array('name_prefix'=>'price_')); ?>
</div>
<div style="margin-top: 10px;">
    <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jSpecializationsCategoriesForm.save(false);" />
    <? if($edit) { ?><input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jSpecializationsCategoriesForm.save(true);" /><? } ?>
    <? if($edit) { ?><input type="button" onclick="jSpecializationsCategoriesForm.del(); return false;" class="btn btn-danger button delete" value="<?= _t('', 'Delete') ?>" /><? } ?>
    <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jSpecializationsCategoriesFormManager.action('cancel');" />
</div>
</form>

<script type="text/javascript">
var jSpecializationsCategoriesForm =
    (function(){
        var $progress, $form, formChk, id = <?= $id ?>;
        var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        $(function(){
            $progress = $('#SpecializationsCategoriesFormProgress');
            $form = $('#SpecializationsCategoriesForm');

            // tabs
            $form.find('#SpecializationsCategoriesFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                var key = $(this).data('key');
                $form.find('.j-tab').addClass('hidden');
                $form.find('.j-tab-'+key).removeClass('hidden');
                $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
            });
        });

        return {
            del: function()
            {
                if( id > 0 ) {
                    bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                        false, {progress: $progress, repaint: false, onComplete:function(){
                            bff.success('Запись успешно удалена');
                            jSpecializationsCategoriesFormManager.action('cancel');
                            jSpecializationsCategoriesList.refresh();
                        }});
                }
            },
            save: function(returnToList)
            {
                if( ! formChk.check(true) ) return;
                bff.ajax(ajaxUrl, $form.serialize(), function(data){
                    if(data && data.success) {
                        bff.success('Данные успешно сохранены');
                        if(returnToList || ! id) {
                            jSpecializationsCategoriesFormManager.action('cancel');
                            jSpecializationsCategoriesList.refresh( ! id);
                        }
                    }
                }, $progress);
            },
            onShow: function ()
            {
                formChk = new bff.formChecker($form);
            }
        };
    }());
</script>