<?php
if ( ! isset($spec_id)) $spec_id = 0;
if (Specializations::catsOn()): ?>
    <? if( ! empty($pid)): ?>
        <li class="p-categoriesList_active">
            <a href="#" class="small ajax-link j-back" data="{pid:<?= $pid; ?>, id:<?= $cat_id; ?>}"><span><?= _t('form', 'Вернуться назад'); ?></span></a>
            <h6><?= ! empty($cat_title) ? $cat_title : '' ?></h6>
        </li>
    <? else:
        $pid = Specializations::ROOT_SPEC;
    endif; ?>
<? endif; ?>
<? if( ! empty($aSpecs)):
        foreach($aSpecs as $k => $v): ?>
            <li<?= $v['id'] == $spec_id ? ' class="active"' : '' ?>><a href="#" class="j-spec" data="{spec:<?= $v['id']; ?>, cat:<?= $cat_id; ?>, cat_title:'<?= ! empty($cat_title) ? HTML::escape($cat_title, 'js') : '' ?>'}"><?= $v['title'] ?></a></li>
        <? endforeach;
   else:
       if( ! empty($aCats)):
            if( ! empty($numlevel)): ?>
                <li class="p-categoriesList_active">
                    <a href="#" class="small ajax-link j-back" data="{cat:<?= $cat_id; ?>}"><span><?= _t('form', 'Вернуться назад'); ?></span></a>
                    <h6><?= ! empty($title) ? $title : '' ?></h6>
                </li>
            <? else: if(config::sysAdmin('specializations.search', false, TYPE_BOOL)): ?>
                <li class="dropdown-menu-autocomplete j-spec-autocomplete" data="{pid:<?= $pid; ?>}">
                    <input type="text" class="form-control j-spec-autocomplete-text" placeholder="<?= _t('specs', 'Введите специализацию'); ?>" value="" autocomplete="off" />
                    <a href="#" class="dropdown-menu-autocomplete-clear hidden j-autocomplete-cancel"><i class="fa fa-times"></i></a>
                    <div class="dropdown-menu-autocomplete-progress hidden j-autocomplete-progress"><img alt="" src="<?= bff::url('/img/loader.gif') ?>"></div>
                </li>
            <? endif; endif;
            foreach($aCats as $k => $v): ?>
                <li<?= $cat_id == $v['id'] ? ' class="active"' : '' ?>><a href="#" class="j-cat" data="{id:<?= $v['id'] ?>}"><?= $v['title'] ?> &raquo;</a></li>
            <? endforeach;
       endif;
   endif;