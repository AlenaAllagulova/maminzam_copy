<?php

/**
 * Права доступа группы:
 *  - specializations: Специализации
 *      - specs: Управление специализациями
 *      - categories: Управление категориями
 *      - settings: Настройки
 */
class Specializations_ extends SpecializationsBase
{
    public function specializations_list()
    {
        if (!$this->haveAccessTo('specs')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'add':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateSpecializationData(0, $bSubmit);
                    if ($bSubmit) {

                        if ($this->errors->no()) {
                            $nSpecializationID = $this->model->specializationSave(0, $aData);
                            if ($nSpecializationID > 0) {
                            }
                            $this->aSpecsKeywords(true);
                        }
                    }
                    $aData['id'] = 0;
                    $aData['priceForm'] = $this->viewPHP($aData, 'admin.specializations.form.price');

                    $aResponse['form'] = $this->viewPHP($aData, 'admin.specializations.form');
                }
                break;
                case 'edit':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nSpecializationID = $this->input->postget('id', TYPE_UINT);
                    if (!$nSpecializationID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {

                        $aData = $this->validateSpecializationData($nSpecializationID, $bSubmit);
                        if ($this->errors->no()) {
                            $this->model->specializationSave($nSpecializationID, $aData);
                            $this->aSpecsKeywords(true);
                        }
                        $aData['id'] = $nSpecializationID;
                    } else {
                        $aData = $this->model->specializationData($nSpecializationID, true);
                        if (empty($aData)) {
                            $this->errors->unknownRecord();
                            break;
                        }
                    }
                    $aData['priceForm'] = $this->viewPHP($aData, 'admin.specializations.form.price');
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.specializations.form');
                }
                break;
                case 'expand':
                {
                    $nSpecializationID = $this->input->postget('id', TYPE_UINT);
                    if (!$nSpecializationID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $aData['list'] = $this->model->specializationsListing();
                    $aData['skip_norecords'] = false;
                    $aResponse['list'] = $this->viewPHP($aData, 'admin.specializations.listing.ajax');
                    $aResponse['cnt'] = sizeof($aData['list']);
                }
                break;
                case 'toggle':
                {
                    $nSpecializationID = $this->input->postget('id', TYPE_UINT);
                    if (!$nSpecializationID) {
                        $this->errors->unknownRecord();
                        break;
                    }
                    if ($nSpecializationID == static::ROOT_SPEC) {
                        $this->errors->impossible();
                        break;
                    }

                    $sToggleType = $this->input->get('type', TYPE_STR);
                    $this->model->specializationToggle($nSpecializationID, $sToggleType);
                }
                break;
                case 'rotate':
                {

                    $nCatID = $this->input->postget('cat', TYPE_UINT);
                    $this->model->specializationsRotate($nCatID);
                }
                break;
                case 'delete':
                {

                    $nSpecializationID = $this->input->postget('id', TYPE_UINT);
                    if (!$nSpecializationID) {
                        $this->errors->impossible();
                        break;
                    }
                    if ($nSpecializationID == static::ROOT_SPEC) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->specializationData($nSpecializationID, true);
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->model->specializationDelete($nSpecializationID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    }
                }
                break;
                case 'category-data':
                {
                    if (!static::catsOn()) {
                        $this->errors->unknownRecord();
                        break;
                    }
                    $nCategoryID = $aResponse['id'] = $this->input->post('cat_id', TYPE_UINT);
                    $aResponse['subs'] = 0;
                    $mLvl = $this->input->post('lvl', TYPE_STR);
                    $bNoAllSpec = $this->input->get('noAllSpec', TYPE_UINT);
                    if ($mLvl != 'spec') {
                        if ($nCategoryID) {
                            $aResponse['subs'] = $this->model->categorySubCount($nCategoryID);
                            if ($aResponse['subs'] > 0) {
                                $aResponse['cats'] = $this->model->categorySubOptions($nCategoryID, array(
                                        'sel'   => 0,
                                        'empty' => 'Выбрать'
                                    )
                                );
                                $aResponse['lvl'] = $mLvl + 1;
                            }
                        }
                        if (!$aResponse['subs']) {
                            $aSpecs = $this->model->specializationsOptions(0, $nCategoryID, array(
                                    'empty'     => 'Выбрать',
                                    'noAllSpec' => $bNoAllSpec
                                )
                            );
                            if (!empty($aSpecs['spec']['categories'])) {
                                $aResponse['cats'] = $aSpecs['spec']['categories'];
                                $aResponse['lvl'] = 'spec';
                                $aResponse['subs'] = $aSpecs['spec']['cnt'];
                            }
                        }
                    } else {
                        $aResponse['subs'] = 0;
                        $aResponse['lvl'] = 'spec';
                    }
                } break;
                case 'price-data':
                {
                    $nSpecID = $this->input->post('spec_id', TYPE_UINT);
                    $aResponse['price_sett'] = $this->aPriceSett($nSpecID);
                } break;
                case 'dp-data':
                {
                    $bSearch = $this->input->post('search', TYPE_BOOL);
                    $nSpecID = $this->input->post('spec_id', TYPE_UINT);
                    $sKey = $this->input->post('key', TYPE_STR);
                    $sModule = $this->input->get('module', TYPE_STR);
                    if ( ! $sKey) { $sKey = 'd'; }
                    if (Orders::dynpropsEnabled() && $sModule == 'orders') {
                        $aResponse['dp'] = $this->dpOrdersForm($nSpecID, $bSearch, false, $sKey);
                    } else {
                        $aResponse['dp'] = $this->dpForm($nSpecID, $bSearch, false, $sKey);
                    }
                } break;
                case 'services-data':
                {
                    $nSpecID = $this->input->post('spec_id', TYPE_UINT);
                    $nUserID = $this->input->post('user_id', TYPE_UINT);
                    $aUserServices = array();
                    if ($nUserID) {
                        $aUserServices = Users::model()->userSpecsServices($nUserID);
                    }
                    $aResponse['serv'] = $this->servicesForm($nSpecID, $aUserServices);
                } break;
                case 'dev-services-cache':
                {
                    if (!FORDEV) {
                        return $this->showAccessDenied();
                    }

                    set_time_limit(0);
                    ignore_user_abort(true);
                    $nCnt = Users::model()->generateUserSpecsServicesCache();
                    if ($this->errors->no()) {
                        return 'Обновлено ' . $nCnt . ' строк.';
                    } else {
                        return join('<br>', $this->errors->get());
                    }
                } break;
                default: {
                    $aResponse = false;
                } break;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = array();
        $this->input->postgetm(array(
                'cat' => TYPE_UINT,
            ), $f
        );

        # формируем фильтр списка специализаций
        $sql = array();
        $aData['pgn'] = '';
        $sOrder = 'S.num';
        if ($f['cat']) {
            $sql[':cat'] = array('C.cat_id = :catid', ':catid' => $f['cat']);
            $sOrder = 'C.num';
        }

        $aData['list'] = $this->model->specializationsListing($sql, false, '', $sOrder, array('allowRoot' => 1));

        $aData['noActions'] = $this->input->getpost('noActions', TYPE_BOOL);
        $aData['list'] = $this->viewPHP($aData, 'admin.specializations.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'list' => $aData['list'],
                    'pgn'  => $aData['pgn'],
                )
            );
        }

        $aData['f'] = $f;
        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        tpl::includeJS(array('tablednd','wysiwyg'), true);

        return $this->viewPHP($aData, 'admin.specializations.listing');
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nSpecializationID ID специализации или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateSpecializationData($nSpecializationID, $bSubmit)
    {
        $aData = array();
        $this->input->postm_lang($this->model->langSpecializations, $aData);
        $aParam = array(
            'keyword'    => TYPE_NOTAGS, # URL-Keyword
            'enabled'    => TYPE_BOOL,   # Включена
            'price_sett' => TYPE_ARRAY,  # Настройки цены
            # Использовать базовый шаблон SEO:
            'orders_mtemplate' => TYPE_BOOL, # Модуль: Orders
            'users_mtemplate'  => TYPE_BOOL, # Модуль: Users
            'qa_mtemplate'     => TYPE_BOOL, # Модуль: Qa
            'price_mtemplate'  => TYPE_BOOL, # Модуль: Users
        );
        $this->input->postm($aParam, $aData);

        $this->validateSpecializationPriceSettings($aData['price_sett'], $bSubmit);
        if ($bSubmit) {
            # URL-Keyword
            $aData['keyword'] = $this->db->getKeyword($aData['keyword'], $aData['title'][LNG], TABLE_SPECIALIZATIONS, $nSpecializationID, 'keyword', 'id');

            if ($nSpecializationID != Specializations::ROOT_SPEC) {
                $aData['pid'] = Specializations::ROOT_SPEC;
            } else {
                $aData['pid'] = 0;
            }
        } else {
            if ( ! $nSpecializationID) {
                $aData['orders_mtemplate'] = 1;
                $aData['users_mtemplate']  = 1;
                $aData['qa_mtemplate']     = 1;
                $aData['price_mtemplate']  = 1;
            }
        }

        return $aData;
    }

    /**
     * Обрабатываем параметры настроек цен специализации
     * @param array $aSettings @ref настройки
     * @param boolean $bSubmit выполняем сохранение/редактирование
     */
    protected function validateSpecializationPriceSettings(&$aSettings, $bSubmit)
    {
        $this->input->clean_array($aSettings, array(
                'curr'  => TYPE_UINT,
                'rates' => TYPE_ARRAY,
                'ex'    => ($bSubmit ? TYPE_ARRAY_UINT : TYPE_UINT),
            )
        );

        if ($bSubmit) {
            foreach ($aSettings['rates'] as $k => $v) {
                foreach ($this->locale->getLanguages(true) as $lng) {
                    $this->input->clean($v[$lng], TYPE_NOTAGS);
                }
                $aSettings['rates'][$k] = $v;
            }
            $aSettings['ex'] = array_sum($aSettings['ex']);
        }
    }

    public function specialization_services_list()
    {
        if (!$this->haveAccessTo('specs')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'add':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateSpecializationServiceData(0, $bSubmit);
                    if ($bSubmit) {

                        if ($this->errors->no()) {
                            $nServiceID = $this->model->specializationServiceSave(0, $aData);
                            if ($nServiceID > 0) {
                            }
                        }
                        break;
                    }
                    $aData['id'] = 0;
                    $aData['spec_id'] = $this->input->postget('spec', TYPE_UINT);
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.specialization.service.form');
                }
                break;
                case 'edit':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nServiceID = $this->input->postget('id', TYPE_UINT);
                    if (!$nServiceID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {

                        $aData = $this->validateSpecializationServiceData($nServiceID, $bSubmit);
                        if ($this->errors->no()) {
                            $this->model->specializationServiceSave($nServiceID, $aData);
                        }
                        break;
                    } else {
                        $aData = $this->model->specializationServiceData($nServiceID, true);
                        if (empty($aData)) {
                            $this->errors->unknownRecord();
                            break;
                        }
                    }
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.specialization.service.form');
                }
                    break;
                case 'rotate':
                {

                    $nSpecID = $this->input->postget('spec', TYPE_UINT);
                    $this->model->specializationServicesRotate($nSpecID);
                }
                    break;
                case 'delete':
                {

                    $nServiceID = $this->input->postget('id', TYPE_UINT);
                    if (!$nServiceID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->specializationServiceData($nServiceID, true);
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->model->specializationServiceDelete($nServiceID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    }
                }
                    break;
                default: {
                    $aResponse = false;
                } break;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = array();
        $this->input->postgetm(array(
            'spec' => TYPE_UINT,
        ), $f
        );
        $aData['list'] = $this->model->specializationDataServices($f['spec']);
        $aData['list'] = $this->viewPHP($aData, 'admin.specialization.services.list.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'list' => $aData['list'],
                )
            );
        }
        return '';
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $serviceID ID услуги или 0
     * @param boolean $submit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateSpecializationServiceData($serviceID, $submit)
    {
        $aData = array();
        $this->input->postm_lang($this->model->langServices, $aData);
        $priceUsers = Specializations::useServices(Specializations::SERVICES_PRICE_USERS);
        $aParam = array(
            'keyword'    => TYPE_NOTAGS, # URL-Keyword
            'price'      => TYPE_PRICE,
            'price_curr' => TYPE_UINT,
            'mtemplate'  => TYPE_BOOL, # Использовать базовый шаблон SEO
        );
        if ($priceUsers) {
            $aParam['price_free'] = TYPE_BOOL;
        }
        if ( ! $serviceID && $submit) {
            $aParam['spec_id'] = TYPE_UINT;
        }
        $this->input->postm($aParam, $aData);

        if ($submit) {
            # URL-Keyword
            $aData['keyword'] = $this->db->getKeyword($aData['keyword'], $aData['title'][LNG], TABLE_SPECIALIZATIONS_SERVICES, $serviceID, 'keyword', 'id');

        } else {
            if ( ! $serviceID && $priceUsers) {
                $aData['price_free'] = 1;
            }
        }

        return $aData;
    }

    public function categories_list()
    {
        if (!$this->haveAccessTo('categories')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'add':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateCategoryData(0, $bSubmit);
                    if ($bSubmit) {

                        if ($this->errors->no()) {
                            $nCategoryID = $this->model->categorySave(0, $aData);
                            if ($nCategoryID > 0) {
                                $this->aSpecsKeywords(true);
                            }
                        }
                    }

                    $aData['id'] = 0;
                    $aData['specs'] = $this->model->specializationsInCategory(false, array('S.id', 'SL.title'));
                    $sSpecsHTML = '';
                    $this->buildCols($sSpecsHTML, $aData['specs'], sizeof($aData['specs']), 0, create_function('$k,$v', '
                        return \'<div><label class="checkbox"><input type="checkbox" value="\'.$v[\'id\'].\'" name="specs[]" /> <span>\'.$v[\'title\'].\'</span></label></div>\';
                    '
                        )
                    );
                    $aData['specs'] = $sSpecsHTML;

                    $aResponse['form'] = $this->viewPHP($aData, 'admin.categories.form');
                }
                break;
                case 'edit':
                {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if (!$nCategoryID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {

                        $aData = $this->validateCategoryData($nCategoryID, $bSubmit);
                        if ($this->errors->no()) {
                            $this->model->categorySave($nCategoryID, $aData);
                            $this->aSpecsKeywords(true);
                        }
                        $aData['id'] = $nCategoryID;
                    } else {
                        $aData = $this->model->categoryData($nCategoryID, true);
                        if (empty($aData)) {
                            $this->errors->unknownRecord();
                            break;
                        }
                    }

                    $aData['pid_path'] = $this->model->categoryParentsTitle($nCategoryID);
                    $aData['specs'] = $this->model->specializationsInCategory($nCategoryID, array(
                            'S.id',
                            'SL.title'
                        ), array('all' => 1)
                    );
                    $sSpecsHTML = '';
                    $this->buildCols($sSpecsHTML, $aData['specs'], sizeof($aData['specs']), 0, create_function('$k,$i', '
                                return \'<div><label class="checkbox"><input type="checkbox" value="\'.$i[\'id\'].\'" \'.($i[\'a\']?\' checked="checked"\':\'\').\' name="specs[]" /> <span>\'.$i[\'title\'].\'</span></label></div>\';
                    '
                        )
                    );
                    $aData['specs'] = $sSpecsHTML;

                    $aResponse['form'] = $this->viewPHP($aData, 'admin.categories.form');
                }
                break;
                case 'expand':
                {
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if (!$nCategoryID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $aData['list'] = $this->model->categoriesListing(array('pid' => $nCategoryID));
                    $aData['skip_norecords'] = false;
                    $aResponse['list'] = $this->viewPHP($aData, 'admin.categories.listing.ajax');
                    $aResponse['cnt'] = sizeof($aData['list']);
                }
                break;
                case 'toggle':
                {
                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if (!$nCategoryID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $sToggleType = $this->input->get('type', TYPE_STR);
                    $this->model->categoryToggle($nCategoryID, $sToggleType);
                }
                break;
                case 'rotate':
                {

                    $this->model->categoriesRotate();
                }
                break;
                case 'delete':
                {

                    $nCategoryID = $this->input->postget('id', TYPE_UINT);
                    if (!$nCategoryID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->categoryData($nCategoryID, true);
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->model->categoryDelete($nCategoryID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    } else {
                    }
                }
                break;
                case 'specs':
                {
                    $aResponse['list'] = 'test';
                }
                break;
                case 'dev-treevalidate':
                {
                    if (!FORDEV || !BFF_DEBUG) {
                        return $this->showAccessDenied();
                    }

                    set_time_limit(0);
                    ignore_user_abort(true);
                    return $this->model->treeCategories->validate(true);
                }
                break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = array();
        $this->input->postgetm(array(
                'page' => TYPE_UINT,
            ), $f
        );

        # формируем фильтр списка категорий
        $sql = array();
        $sqlOrder = 'numleft';
        $aData['pgn'] = '';

        $sExpandState = $this->input->cookie(bff::cookiePrefix() . 'Categories_categories_expand', TYPE_STR);
        $aExpandID = (!empty($sExpandState) ? explode('.', $sExpandState) : array());
        $aExpandID = array_map('intval', $aExpandID);
        $aExpandID[] = 1;
        $sql[] = 'pid IN (' . join(',', $aExpandID) . ')';

        $aData['list'] = $this->model->categoriesListing($sql, false, '', $sqlOrder);

        $aData['list'] = $this->viewPHP($aData, 'admin.categories.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'list' => $aData['list'],
                    'pgn'  => $aData['pgn'],
                )
            );
        }

        $aData['f'] = $f;
        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        tpl::includeJS(array('tablednd','wysiwyg'), true);

        return $this->viewPHP($aData, 'admin.categories.listing');
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nCategoryID ID категории или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateCategoryData($nCategoryID, $bSubmit)
    {
        $aData = array();
        $this->input->postm_lang($this->model->langCategories, $aData);
        $this->input->postm(array(
            'pid'     => TYPE_UINT, # Основной раздел
            'keyword' => TYPE_NOTAGS, # URL-Keyword
            'enabled' => TYPE_BOOL, # Включена
            'specs'   => TYPE_ARRAY_UINT, # Специализации
            # Использовать базовый шаблон SEO:
            'orders_mtemplate' => TYPE_BOOL, # Модуль: Orders
            'users_mtemplate'  => TYPE_BOOL, # Модуль: Users
            'qa_mtemplate'     => TYPE_BOOL, # Модуль: Qa
            'price_mtemplate'  => TYPE_BOOL, # Модуль: Users
        ), $aData);

        if ($bSubmit) {
            # URL-Keyword
            $aData['keyword'] = $this->db->getKeyword($aData['keyword'], $aData['title'][LNG], TABLE_SPECIALIZATIONS_CATS, $nCategoryID, 'keyword', 'id');
        } else {
            if ( ! $nCategoryID) {
                $aData['orders_mtemplate'] = 1;
                $aData['users_mtemplate']  = 1;
                $aData['qa_mtemplate']     = 1;
                $aData['price_mtemplate']  = 1;
            }
        }

        return $aData;
    }

    public function settings()
    {
        if (!$this->haveAccessTo('settings')) {
            return $this->showAccessDenied();
        }

        $nSumAuthorRows = static::SERVICES_PRICE_AUTHOR_ROWS_MIN +
                          static::SERVICES_PRICE_AUTHOR_ROWS_AVG +
                          static::SERVICES_PRICE_AUTHOR_ROWS_MAX;

        if (Request::isAJAX()) {
            $this->input->postm(array(
                'specs_services_price'             => TYPE_UINT,
                'specs_services_price_author'      => TYPE_UINT,
                'specs_services_price_author_rows' => TYPE_ARRAY_UINT,
            ), $aData);


            if ( ! in_array($aData['specs_services_price'], array(static::SERVICES_PRICE_ADMIN, static::SERVICES_PRICE_USERS))) {
                $aData['specs_services_price'] = static::SERVICES_PRICE_USERS;
            }
            if ( ! in_array($aData['specs_services_price_author'], array(static::SERVICES_PRICE_AUTHOR_ADMIN, static::SERVICES_PRICE_AUTHOR_USERS))) {
                $aData['specs_services_price_author'] = static::SERVICES_PRICE_AUTHOR_USERS;
            }
            $aData['specs_services_price_author_rows'] = array_sum($aData['specs_services_price_author_rows']);
            if ($aData['specs_services_price_author_rows'] < 0 || $aData['specs_services_price_author_rows'] > $nSumAuthorRows) {
                $aData['specs_services_price_author_rows'] = $nSumAuthorRows;
            }

            $this->configSave($aData);
            $this->ajaxResponseForm();
        }

        $aData = static::servicesConfig();
        if ( ! is_array($aData)) $aData = array();

        return $this->viewPHP($aData, 'admin.settings');
    }

    public function settingsSystem(array &$options = array())
    {
        $aData = array('options'=>&$options);
        return $this->viewPHP($aData, 'admin.settings.sys');
    }
}