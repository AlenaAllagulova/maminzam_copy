<?php

use bff\db\Dynprops;

abstract class SpecializationsBase_ extends Module
{
    const ROOT_SPEC = 1; # ID Базовой специализации

    const PRICE_EX_PRICE = 0; # Базовая настройка цены
    const PRICE_EX_AGREE = 1; # По договоренности

    # Формирование цен на услуги
    const SERVICES_PRICE_ADMIN = 1; # Только администратором
    const SERVICES_PRICE_USERS = 2; # Пользователями и администратором

    # Общий прайс формируется
    const SERVICES_PRICE_AUTHOR_ADMIN = 1; # Администратором
    const SERVICES_PRICE_AUTHOR_USERS = 2; # Пользователями

    # Статистика цен
    const SERVICES_PRICE_AUTHOR_ROWS_MIN = 1;
    const SERVICES_PRICE_AUTHOR_ROWS_AVG = 2;
    const SERVICES_PRICE_AUTHOR_ROWS_MAX = 4;

    /** @var SpecializationsModel */
    var $model = null;
    var $securityKey = '5d94e48cfbc72dbb2876b7bb35a7c4b7';

    public function init()
    {
        parent::init();

        $this->module_title = _t('specs','Специализации');

        # инициализируем модуль дин. свойств
        if (strpos(bff::$event, 'dynprops') === 0) {
            $this->dp();
        }

        # инициализируем модуль дин. свойств
        if (strpos(bff::$event, 'dporders') === 0) {
            $this->dpOrders();
        }
    }

    /**
     * @return Specializations
     */
    public static function i()
    {
        return bff::module('Specializations');
    }

    /**
     * @return SpecializationsModel
     */
    public static function model()
    {
        return bff::model('Specializations');
    }

    /**
     * Использовать категории
     * @return bool
     */
    public static function catsOn()
    {
        return config::sysAdmin('specializations.cats.on', true, TYPE_BOOL);
    }

    /**
     * Использовать услуги для специализаций
     * @param bool|integer $servicesPriceConfig проверять доступно формирования цен:
     *  false - не проверять
     *  static::SERVICES_PRICE_USERS - доступно пользователям
     *  static::SERVICES_PRICE_ADMIN - доступно только администратору
     * @return bool
     */
    public static function useServices($servicesPriceConfig = false)
    {
        static $use;
        if (!isset($use)) {
            $use = config::sysAdmin('specializations.use.services', false, TYPE_BOOL);
        }
        if ($use && $servicesPriceConfig!==false) {
            return (static::servicesConfig('specs_services_price') == $servicesPriceConfig);
        }
        return $use;
    }

    /**
     * Настройки услуг специализаций
     * @param string|false $key ключ требуемой настройки или false (все настройки)
     * @return array|mixed
     */
    public static function servicesConfig($key = false)
    {
        $aData = static::i()->configLoad(array(
            'specs_services_price'             => static::SERVICES_PRICE_USERS,
            'specs_services_price_author'      => static::SERVICES_PRICE_AUTHOR_USERS,
            'specs_services_price_author_rows' => (static::SERVICES_PRICE_AUTHOR_ROWS_MIN +
                                                   static::SERVICES_PRICE_AUTHOR_ROWS_AVG +
                                                   static::SERVICES_PRICE_AUTHOR_ROWS_MAX),
        ));
        if (is_string($key) && isset($aData[$key])) {
            return $aData[$key];
        }
        return $aData;
    }

    /**
     * Формирование формы для услуг
     * @param integer $nSpecID ID специализации
     * @param array $aUserData данные о услугах пользователя
     * @return string HTML
     */
    public function servicesForm($nSpecID, $aUserData = array())
    {
        if (!$nSpecID || !static::useServices()) return '';

        $aData = static::servicesConfig();
        $aData['nSpecID'] = $nSpecID;
        $aData['services'] = $this->model->specializationDataServices($nSpecID);
        $aData['user'] = $aUserData;

        if (!bff::adminPanel()) {
            return $this->viewPHP($aData, 'spec.services.form');
        } else {
            return $this->viewPHP($aData, 'admin.spec.service.form');
        }
    }

    /**
     * Настройки цены для специализации
     * @param int $nSpecID ID специализации
     * @param boolean $bAllLang все языковые данные
     * @return array
     */
    public function aPriceSett($nSpecID = 0, $bAllLang = false)
    {
        if (!$nSpecID) {
            return array();
        }
        $aData = $this->model->specializationData($nSpecID, $bAllLang);
        $aPrice = array();

        if (!isset($aData['price_sett'])) {
            return $aPrice;
        }
        $aPrice = $aData['price_sett'];
        $aPrice['price_title'] = ! empty($aData['price_title']) ? $aData['price_title'] : _t('specializations', 'Цена');
        $aPrice['price_title_mod'] = ! empty($aData['price_title_mod']) ? $aData['price_title_mod'] : _t('specializations', 'По договоренности');

        return $aPrice;
    }

    /**
     * Типы сроков
     * @param int $selectedID ID текущего варианта
     * @param string $type тип возвращаемого значения
     * @return array|string
     */
    public static function aTerms($selectedID = 0, $type = 'opts')
    {
        $data = bff::filter('specializations.terms', array(
            1 => _t('specs', 'часа'),
            2 => _t('specs', 'дня'),
            3 => _t('specs', 'недель'),
            4 => _t('specs', 'месяцев'),
        ));
        switch ($type) {
            case 'opts':  return HTML::selectOptions($data, $selectedID); break;
            case 'title': return ( isset($data[$selectedID]) ? $data[$selectedID] : '' ); break;
            default:      return $data;
        }
    }

    /**
     * Инициализация компонента работы с дин. свойствами
     * @return \bff\db\Dynprops объект
     */
    public function dp()
    {
        static $oDp = null;
        if (isset($oDp)) {
            return $oDp;
        }

        # подключаем "Динамические свойства"
        $oDp = $this->attachComponent('dynprops',
            new Dynprops('owner_id', TABLE_SPECIALIZATIONS,
                TABLE_SPECIALIZATIONS_DP, TABLE_SPECIALIZATIONS_DPM,
                1 # полное наследование
            )
        );

        $oDp->setSettings(array(
                'module_name'  => $this->module_name,
                'typesAllowed' => array(
                    Dynprops::typeCheckboxGroup,
                    Dynprops::typeRadioGroup,
                    Dynprops::typeRadioYesNo,
                    Dynprops::typeCheckbox,
                    Dynprops::typeSelect,
                    Dynprops::typeInputText,
                    Dynprops::typeTextarea,
                    Dynprops::typeNumber,
                    Dynprops::typeRange,
                ),
                'langs'                => $this->locale->getLanguages(false),
                'langText'             => array(
                    'yes'    => _t('', 'Да'),
                    'no'     => _t('', 'Нет'),
                    'all'    => _t('', 'Все'),
                    'select' => _t('', 'Выбрать'),
                ),
                'ownerTableType'       => 1,
                'ownerTable_Title'     => 'title',
                'cache_method'         => 'Specializations_dpSettingsChanged',
                'typesAllowedParent'   => array(Dynprops::typeSelect),
                'datafield_int_last'   => 50,
                'datafield_text_first' => 51,
                'datafield_text_last'  => 55,
                'searchRanges'         => true,
                'cacheKey'             => true,
            )
        );

        $oDp->extraSettings( array(
            'group_id'   => array('title' => _t('', 'Группа'), 'input' => 'text'),
        ));

        return $oDp;
    }

    /**
     * Получаем дин. свойства специализации
     * @param integer $nSpecializationID ID специализации
     * @param boolean $bResetCache обнулить кеш
     * @return mixed
     */
    public function dpSettings($nSpecializationID, $bResetCache = false)
    {
        if ($nSpecializationID <= 0) {
            return array();
        }

        $cache = Cache::singleton($this->module_name, 'file');
        $cacheKey = 'specializations-dynprops-' . $nSpecializationID;
        if ($bResetCache) {
            # сбрасываем кеш настроек дин. свойств специализации
            return $cache->delete($cacheKey);
        } else {
            if (($aSettings = $cache->get($cacheKey)) === false) {
                $aSettings = $this->dp()->getByOwner($nSpecializationID, true, true, false);
                $cache->set($cacheKey, $aSettings); # кешируем
            }

            return $aSettings;
        }
    }

    /**
     * Метод вызываемый модулем \bff\db\Dynprops, в момент изменения настроек дин. свойств специализации
     * @param integer $nSpecializationID ID специализации
     * @param integer $nDynpropID ID дин.свойства
     * @param string $sEvent событие, генерирующее вызов метода
     * @return mixed
     */
    public function dpSettingsChanged($nSpecializationID, $nDynpropID, $sEvent)
    {
        if (empty($nSpecializationID)) {
            return false;
        }
        $this->dpSettings($nSpecializationID, true);
    }

    /**
     * Формирование SQL запроса для сохранения дин.свойств
     * @param integer $nSpecializationID ID специализации
     * @param string $sFieldname ключ в $_POST массиве
     * @return array
     */
    public function dpSave($nSpecializationID, $sFieldname = 'd')
    {
        $aData = $this->input->post($sFieldname, TYPE_ARRAY);

        $aDynpropsData = array();
        foreach ($aData as $props) {
            foreach ($props as $id => $v) {
                $aDynpropsData[$id] = $v;
            }
        }

        $aDynprops = $this->dp()->getByID(array_keys($aDynpropsData), true);

        return $this->dp()->prepareSaveDataByID($aDynpropsData, $aDynprops, 'update', true);
    }

    /**
     * Формирование формы редактирования / фильтра дин.свойств
     * @param integer $nSpecializationID ID специализации
     * @param boolean $bSearch формирование формы поиска
     * @param array|boolean $aData данные или FALSE
     * @param string $sKey ключ
     * @param string|boolean $sFormType тип формы: имя шаблона в модуле
     * @param string|boolean $sTemplateDir путь к шаблону (false - путь указанный компонентом)
     * @param array $aOnlyChild только указанные child свойства
     * @return string HTML template
     */
    public function dpForm($nSpecializationID, $bSearch = true, $aData = false, $sKey = 'd', $sFormType = false, $sTemplateDir = false, $aOnlyChild = array())
    {
        if (empty($nSpecializationID)) {
            return '';
        }

        if ($bSearch) {
            if (!bff::adminPanel()) {
                if (!$sFormType) {
                    $sFormType = 'dp.list';
                }
                if (!$sTemplateDir) {
                    $sTemplateDir = $this->module_dir_tpl;
                }
                $aForm = $this->dp()->form($nSpecializationID, $aData, true, true, $sKey, $sFormType, $sTemplateDir, array('filter'     => $aData,
                                                                                                                            'aOnlyChild' => $aOnlyChild
                    )
                );
            } else {
                $aForm = $this->dp()->form($nSpecializationID, $aData, true, true, $sKey, 'search.inline');
            }
        } else {
            if (!bff::adminPanel()) {
                if (!$sFormType) {
                    $sFormType = 'dp.form';
                }
                if (!$sTemplateDir) {
                    $sTemplateDir = $this->module_dir_tpl;
                }
                $aForm = $this->dp()->form($nSpecializationID, $aData, true, false, $sKey, $sFormType, $sTemplateDir);
            } else {
                $aForm = $this->dp()->form($nSpecializationID, $aData, true, false, $sKey, 'form.table');
            }
        }

        return (!empty($aForm['form']) ? $aForm['form'] : '');
    }

    /**
     * Отображение дин. свойств
     * @param integer $nSpecializationID ID специализации
     * @param array $aData данные
     * @param string $sKey ключ
     * @return string
     */
    public function dpView($nSpecializationID, $aData, $sKey = 'd', $sViewTpl = 'view.dp')
    {
        if (!bff::adminPanel()) {
            $aForm = $this->dp()->form($nSpecializationID, $aData, true, false, $sKey, $sViewTpl, $this->module_dir_tpl);
        } else {
            $aForm = $this->dp()->form($nSpecializationID, $aData, true, false, $sKey, 'view.table');
        }

        return (!empty($aForm['form']) ? $aForm['form'] : '');
    }

    /**
     * Список полных keywords (spec-keyword-cat-keyword) для специализаций
     * @param boolean $bResetCache обнулить кеш
     * @return array|mixed
     */
    public function aSpecsKeywords($bResetCache = false)
    {
        $cache = Cache::singleton($this->module_name, 'file');
        $cacheKey = 'specs-keywords'.(static::catsOn()?'-cats':'');
        if ($bResetCache) {
            # сбрасываем кеш
            return $cache->delete($cacheKey);
        } else {
            if (($aData = $cache->get($cacheKey)) === false) {
                $aData = $this->model->specsKeywords();
                $cache->set($cacheKey, $aData); # кешируем
            }
            return $aData;
        }
    }

    /**
     * Расшифровка в URL ключа поиска по категории специализации / специализации
     * @param string $key URL ключ
     * @param string $moduleName название модуля или ключ страницы
     * @return array|boolean
     */
    public function parseURLKeyword($key, $moduleName)
    {
        $res = array(
            'cat'  => false, # meta данные о категории или false
            'spec' => false, # meta данные о специализации или false
            'keyword' => '', # ключ для URL
            'canonical' => '', # ключ для канонического URL
        );

        $key = $res['keyword'] = $res['canonical'] = trim(strval($key), '/ ');
        if (empty($key)) {
            return false; # ключ пустой - 404
        }
        # 2 ключа:
        if (mb_strpos($key, '/')) {
            $key = explode('/', $key, 2);
            $res['cat'] = $this->model->categoryMeta($key[0], $moduleName);
            $res['spec'] = $this->model->specializationMeta($key[1], $moduleName);
            $res['canonical'] = $key[1];
        }
        # 1 ключ:
        else
        {
            if ( ! static::catsOn()) {
                # категории выключены - ключ специализации
                $res['spec'] = $this->model->specializationMeta($key, $moduleName);
                if (empty($res['spec'])) return false; # не нашли - 404
            } else {
                # 1. поиск по ключу - категории
                $res['cat'] = $this->model->categoryMeta($key, $moduleName);
                if (empty($res['cat'])) { # не нашли
                    # 2. поиск по ключу - специализации
                    $res['spec'] = $this->model->specializationMeta($key, $moduleName);
                    if (empty($res['spec'])) return false; # не нашли - 404
                }
            }
        }

        if ($res['cat'] && ! $res['spec']) {
            # список всех специализаций связанных с категорией
            $res['cat']['specs'] = $this->model->specializationsInCategory($res['cat']['id'], 'id');
        }
        if ( ! $res['cat'] && $res['spec']) {
            # верхняя категория в которой находится специализация
            $res['spec']['cat_id'] = ( static::catsOn() ? $this->model->specializationCategoryFirst($res['spec']['id']) : 0 );
        }

        return $res;
    }

    /**
     * Построение списков
     * @param $result
     * @param $data
     * @param $n
     * @param $values
     * @param $funcLI
     * @param int $cols
     */
    protected function buildCols(&$result, $data, $n, $values, $funcLI, $cols = 2)
    {
        $width = (100 / $cols) - 2;
        $inColumn = ceil($n / $cols);
        $specialColumns = round((($n / $cols) - intval($n / $cols)) * $cols);
        $c = 1.0;
        $added = 1;
        $tag = '<div class="left" style="margin-right:8px;width:' . $width . '%;">';
        $result .= $tag;
        foreach ($data as $k => $v) {
            $res = $funcLI($k, $v, $values);
            if (empty($res)) {
                continue;
            }
            $result .= $res;
            if ($added++ >= $inColumn && --$cols > 0) {
                $result .= '</div>' . $tag;
                $added = 1;
                if ($c++ == $specialColumns) {
                    --$inColumn;
                }
            }
        }
        $result .= '</div>';
    }

    /**
     * Инициализация компонента работы с дин. свойствами (для модуля заказов)
     * @return \bff\db\Dynprops объект
     */
    public function dpOrders()
    {
        static $oDp = null;
        if (isset($oDp)) {
            return $oDp;
        }

        # подключаем "Динамические свойства"
        $oDp = $this->attachComponent('dporders',
            new Dynprops('owner_id', TABLE_SPECIALIZATIONS,
                TABLE_SPECIALIZATIONS_DP_ORDERS, TABLE_SPECIALIZATIONS_DPM_ORDERS,
                1 # полное наследование
            )
        );

        $oDp->setSettings(array(
                'module_name'  => $this->module_name,
                'typesAllowed' => array(
                    Dynprops::typeCheckboxGroup,
                    Dynprops::typeRadioGroup,
                    Dynprops::typeRadioYesNo,
                    Dynprops::typeCheckbox,
                    Dynprops::typeSelect,
                    Dynprops::typeInputText,
                    Dynprops::typeTextarea,
                    Dynprops::typeNumber,
                    Dynprops::typeRange,
                ),
                'langs'                => $this->locale->getLanguages(false),
                'langText'             => array(
                    'yes'    => _t('', 'Да'),
                    'no'     => _t('', 'Нет'),
                    'all'    => _t('', 'Все'),
                    'select' => _t('', 'Выбрать'),
                ),
                'ownerTableType'       => 1,
                'ownerTable_Title'     => 'title',
                'cache_method'         => 'Specializations_dpOrdersSettingsChanged',
                'typesAllowedParent'   => array(Dynprops::typeSelect),
                'datafield_int_last'   => 15,
                'datafield_text_first' => 16,
                'datafield_text_last'  => 20,
                'searchRanges'         => true,
                'cacheKey'             => true,
                'act_listing'          => 'dporders_listing',
                'act_action'           => 'dporders_action',
            )
        );

        $oDp->extraSettings( array(
            'group_id'   => array('title' => _t('', 'Группа'), 'input' => 'text'),
        ));

        return $oDp;
    }

    /**
     * Получаем дин. свойства специализации (для модуля заказов)
     * @param integer $nSpecializationID ID специализации
     * @param boolean $bResetCache обнулить кеш
     * @return mixed
     */
    public function dpOrdersSettings($nSpecializationID, $bResetCache = false)
    {
        if ($nSpecializationID <= 0) {
            return array();
        }

        $cache = Cache::singleton($this->module_name, 'file');
        $cacheKey = 'specializations-dynprops-orders-' . $nSpecializationID;
        if ($bResetCache) {
            # сбрасываем кеш настроек дин. свойств специализации
            return $cache->delete($cacheKey);
        } else {
            if (($aSettings = $cache->get($cacheKey)) === false) {
                $aSettings = $this->dpOrders()->getByOwner($nSpecializationID, true, true, false);
                $cache->set($cacheKey, $aSettings); # кешируем
            }

            return $aSettings;
        }
    }

    /**
     * Метод вызываемый модулем \bff\db\Dynprops, в момент изменения настроек дин. свойств специализации (для модуля заказов)
     * @param integer $nSpecializationID ID специализации
     * @param integer $nDynpropID ID дин.свойства
     * @param string $sEvent событие, генерирующее вызов метода
     * @return mixed
     */
    public function dpOrdersSettingsChanged($nSpecializationID, $nDynpropID, $sEvent)
    {
        if (empty($nSpecializationID)) {
            return false;
        }
        $this->dpOrdersSettings($nSpecializationID, true);
    }

    /**
     * Формирование SQL запроса для сохранения дин.свойств (для модуля заказов)
     * @param integer $nSpecializationID ID специализации
     * @param string $sFieldname ключ в $_POST массиве
     * @return array
     */
    public function dpOrdersSave($nSpecializationID, $sFieldname = 'd')
    {
        $aData = $this->input->post($sFieldname, TYPE_ARRAY);

        $aDynpropsData = array();
        foreach ($aData as $props) {
            foreach ($props as $id => $v) {
                $aDynpropsData[$id] = $v;
            }
        }

        $aDynprops = $this->dpOrders()->getByID(array_keys($aDynpropsData), true);

        return $this->dpOrders()->prepareSaveDataByID($aDynpropsData, $aDynprops, 'update', true);
    }

    /**
     * Формирование формы редактирования / фильтра дин.свойств (для модуля заказов)
     * @param integer $nSpecializationID ID специализации
     * @param boolean $bSearch формирование формы поиска
     * @param array|boolean $aData данные или FALSE
     * @param string $sKey ключ
     * @param string|boolean $sFormType тип формы: имя шаблона в модуле
     * @param string|boolean $sTemplateDir путь к шаблону (false - путь указанный компонентом)
     * @param array $aOnlyChild только указанные child свойства
     * @return string HTML template
     */
    public function dpOrdersForm($nSpecializationID, $bSearch = true, $aData = false, $sKey = 'd', $sFormType = false, $sTemplateDir = false, $aOnlyChild = array())
    {
        if (empty($nSpecializationID)) {
            return '';
        }

        if ($bSearch) {
            if (!bff::adminPanel()) {
                if (!$sFormType) {
                    $sFormType = 'dp.list';
                }
                if (!$sTemplateDir) {
                    $sTemplateDir = $this->module_dir_tpl;
                }
                $aForm = $this->dpOrders()->form($nSpecializationID, $aData, true, true, $sKey, $sFormType, $sTemplateDir, array('filter'     => $aData,
                        'aOnlyChild' => $aOnlyChild
                    )
                );
            } else {
                $aForm = $this->dpOrders()->form($nSpecializationID, $aData, true, true, $sKey, 'search.inline');
            }
        } else {
            if (!bff::adminPanel()) {
                if (!$sFormType) {
                    $sFormType = 'dp.form';
                }
                if (!$sTemplateDir) {
                    $sTemplateDir = $this->module_dir_tpl;
                }
                $aForm = $this->dpOrders()->form($nSpecializationID, $aData, true, false, $sKey, $sFormType, $sTemplateDir);
            } else {
                $aForm = $this->dpOrders()->form($nSpecializationID, $aData, true, false, $sKey, 'form.table');
            }
        }

        return (!empty($aForm['form']) ? $aForm['form'] : '');
    }

    /**
     * Отображение дин. свойств (для модуля заказов)
     * @param integer $nSpecializationID ID специализации
     * @param array $aData данные
     * @param string $sKey ключ
     * @param string $sViewTpl название шаблона
     * @return string
     */
    public function dpOrdersView($nSpecializationID, $aData, $sKey = 'd', $sViewTpl = 'view.dp')
    {
        if (!bff::adminPanel()) {
            $aForm = $this->dpOrders()->form($nSpecializationID, $aData, true, false, $sKey, $sViewTpl, $this->module_dir_tpl);
        } else {
            $aForm = $this->dpOrders()->form($nSpecializationID, $aData, true, false, $sKey, 'view.table');
        }

        return (!empty($aForm['form']) ? $aForm['form'] : '');
    }

    public static function getSelectedSpec()
    {
        $aSelectedSpec = [
            'keyword' => '',
            'title'   => _t('', 'Выберите'),
            'id'      => '',
        ];

        $aAllSpecList = Specializations::model()->specializationsListing(['enabled' =>true]);

        foreach ($aAllSpecList as $spec){
            if(strstr(Request::uri(), $spec['keyword'])){
                $aSelectedSpec = $spec;
            }
        }
        if(empty($aSelectedSpec['keyword']) && is_array($aAllSpecList)){
            $aSelectedSpec = reset($aAllSpecList);
        }

        return $aSelectedSpec;
    }

}