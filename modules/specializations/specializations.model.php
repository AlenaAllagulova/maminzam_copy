<?php

class SpecializationsModel_ extends Model
{
    /** @var SpecializationsBase */
    protected $controller;

    public $langSpecializations = array(
        'title'              => TYPE_NOTAGS, # Название
        'title_index'        => TYPE_NOTAGS, # Название на главной
        # Orders
        'orders_mtitle'      => TYPE_NOTAGS, # Meta Title
        'orders_mkeywords'   => TYPE_NOTAGS, # Meta Keywords
        'orders_mdescription'=> TYPE_NOTAGS, # Meta Description
        'orders_seotext'     => TYPE_STR,    # SEO-Text
        # Users
        'users_mtitle'       => TYPE_NOTAGS, # Meta Title
        'users_mkeywords'    => TYPE_NOTAGS, # Meta Keywords
        'users_mdescription' => TYPE_NOTAGS, # Meta Description
        'users_seotext'      => TYPE_STR,    # SEO-Text
        # Qa
        'qa_mtitle'          => TYPE_NOTAGS, # Meta Title
        'qa_mkeywords'       => TYPE_NOTAGS, # Meta Keywords
        'qa_mdescription'    => TYPE_NOTAGS, # Meta Description
        'qa_seotext'         => TYPE_STR,    # SEO-Text
        # Прайс
        'price_mtitle'       => TYPE_NOTAGS, # Meta Title
        'price_mkeywords'    => TYPE_NOTAGS, # Meta Keywords
        'price_mdescription' => TYPE_NOTAGS, # Meta Description
        'price_seotext'      => TYPE_STR,    # SEO-Text
        # Цена
        'price_title'        => TYPE_NOTAGS, # Цена: название
        'price_title_mod'    => TYPE_NOTAGS, # Цена: название модификатора
    );

    /** @var bff\db\NestedSetsTree для категорий */
    public $treeCategories;
    public $langCategories = array(
        'title'               => TYPE_NOTAGS, # Название
        # Orders
        'orders_mtitle'       => TYPE_NOTAGS, # Meta Title
        'orders_mkeywords'    => TYPE_NOTAGS, # Meta Keywords
        'orders_mdescription' => TYPE_NOTAGS, # Meta Description
        'orders_seotext'      => TYPE_STR, # SEO-Text
        # Users
        'users_mtitle'        => TYPE_NOTAGS, # Meta Title
        'users_mkeywords'     => TYPE_NOTAGS, # Meta Keywords
        'users_mdescription'  => TYPE_NOTAGS, # Meta Description
        'users_seotext'       => TYPE_STR, # SEO-Text
        # Qa
        'qa_mtitle'           => TYPE_NOTAGS, # Meta Title
        'qa_mkeywords'        => TYPE_NOTAGS, # Meta Keywords
        'qa_mdescription'     => TYPE_NOTAGS, # Meta Description
        'qa_seotext'          => TYPE_STR, # SEO-Text
        # Прайс
        'price_mtitle'        => TYPE_NOTAGS, # Meta Title
        'price_mkeywords'     => TYPE_NOTAGS, # Meta Keywords
        'price_mdescription'  => TYPE_NOTAGS, # Meta Description
        'price_seotext'       => TYPE_STR, # SEO-Text
    );

    public $langServices = array(
        'title'               => TYPE_NOTAGS, # Название
        'measure'             => TYPE_NOTAGS, # Ед. измерения
        'description'         => TYPE_NOTAGS, # Описание
        'mtitle'              => TYPE_NOTAGS, # Meta Title
        'mkeywords'           => TYPE_NOTAGS, # Meta Keywords
        'mdescription'        => TYPE_NOTAGS, # Meta Description
        'seotext'             => TYPE_STR,    # SEO-Text
    );

    public function init()
    {
        parent::init();

        # подключаем nestedSets категории
        $this->treeCategories = new bff\db\NestedSetsTree(TABLE_SPECIALIZATIONS_CATS, 'id', 'pid');
        $this->treeCategories->init();
    }

    /**
     * Список специализаций
     * @param array $aFilter фильтр списка специализаций
     * @param bool $bCount только подсчет кол-ва специализаций
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @param array $aParam - параметры
     *              $aParam['allowRoot'] = 1 - включить безовую специализацию
     * @return mixed
     */
    public function specializationsListing($aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '', $aParam = array()) //admin
    {
        $aFilter[':lang'] = $this->db->langAnd(false, 'S', 'SL');
        if (empty($aParam['allowRoot'])) {
            $aFilter[] = array('S.pid = :root', ':root' => Specializations::ROOT_SPEC);
        }

        $sFrom = TABLE_SPECIALIZATIONS . ' S, ' . TABLE_SPECIALIZATIONS_LANG . ' SL ';
        if (array_key_exists(':cat', $aFilter)) {
            $sFrom .= ', ' . TABLE_SPECIALIZATIONS_IN_CATS . ' C ';
            $aFilter[':spec'] = ' S.id = C.spec_id ';
        }

        $aFilter = $this->prepareFilter($aFilter, 'S');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(S.id) FROM ' . $sFrom . $aFilter['where'], $aFilter['bind']);
        }
        return $this->db->select('SELECT S.*, SL.title_index
               FROM ' . $sFrom . '
               ' . $aFilter['where']
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']
        );
    }

    /**
     * Список специализаций
     * @param array $aField список полей
     * @param array $aFilter фильтр списка специализаций
     * @param bool $bCount только подсчет кол-ва специализаций
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function specializationsList($aField = array(), $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        if (!$bCount) {
            $aFilter[':lang'] = $this->db->langAnd(false, 'S', 'SL');
        }
        $aFilter = $this->prepareFilter($aFilter, 'S');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(S.id) FROM ' . TABLE_SPECIALIZATIONS . ' S ' . $aFilter['where'], $aFilter['bind']);
        }

        $aField = array('S.id') + $aField;
        $aData = $this->db->select('SELECT ' . join(',', $aField) . '
                                  FROM ' . TABLE_SPECIALIZATIONS . ' S, ' . TABLE_SPECIALIZATIONS_LANG . ' SL
                                  ' . $aFilter['where'] . '
                                  ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
                                  ' . $sqlLimit, $aFilter['bind']
        );

        if (!empty($aData)) {
            //
        }

        return $aData;
    }

    /**
     * Получение данных специализации
     * @param integer $nSpecializationID ID специализации
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function specializationData($nSpecializationID, $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT S.*
                    FROM ' . TABLE_SPECIALIZATIONS . ' S
                    WHERE S.id = :id',
                array(':id' => $nSpecializationID)
            );
            if (!empty($aData)) {
                $this->db->langSelect($nSpecializationID, $aData, $this->langSpecializations, TABLE_SPECIALIZATIONS_LANG);
            }
        } else {
            $aData = $this->db->one_array('SELECT S.*, SL.title as title, SL.title_index as title_index, SL.price_title, SL.price_title_mod
                    FROM ' . TABLE_SPECIALIZATIONS . ' S, ' . TABLE_SPECIALIZATIONS_LANG . ' SL
                    WHERE S.id = :id ' . $this->db->langAnd(true, 'S', 'SL'),
                array(':id' => $nSpecializationID)
            );
        }
        if (isset($aData['price_sett'])) {
            $priceSett =& $aData['price_sett'];
            $priceSett = (!empty($priceSett) ? func::unserialize($priceSett) : array());
            if ($priceSett === false) {
                $priceSett = array();
            }
            if (!isset($priceSett['rates'])) {
                $priceSett['rates'] = array();
            }
            if (!isset($priceSett['ex'])) {
                $priceSett['ex'] = Specializations::PRICE_EX_PRICE;
            }
            if (!$bEdit) {
                if (!empty($priceSett['rates'])) {
                    foreach ($priceSett['rates'] as $k => $v) {
                        if (!empty($v[LNG])) {
                            $priceSett['rates'][$k] = $v[LNG];
                        }
                    }
                }
            }
        }
        return $aData;
    }

    /**
     * Получение meta данных специализации
     * @param mixed $mSpecialization ID/Keyword специализации
     * @param string $sModuleName название модуля
     * @return array
     */
    public function specializationMeta($mSpecialization, $sModuleName)
    {
        $prefix = mb_strtolower($sModuleName).'_';
        $aData = $this->db->one_array('SELECT
                    S.id, S.keyword,
                    S.'.$prefix.'mtemplate as mtemplate,
                    SL.title,
                    SL.'.$prefix.'mtitle as mtitle,
                    SL.'.$prefix.'mkeywords as mkeywords,
                    SL.'.$prefix.'mdescription as mdescription,
                    SL.'.$prefix.'seotext as seotext
                FROM ' . TABLE_SPECIALIZATIONS . ' S, ' . TABLE_SPECIALIZATIONS_LANG . ' SL
                WHERE S.'.(is_int($mSpecialization) ? 'id' : 'keyword').' = :spec
                  AND S.enabled = 1 '
                    . $this->db->langAnd(true, 'S', 'SL'),
            array(':spec' => $mSpecialization)
        );
        if (empty($aData)) $aData = array();
        return $aData;
    }

    /**
     * Сохранение специализации
     * @param integer $nSpecializationID ID специализации
     * @param array $aData данные специализации
     * @return boolean|integer
     */
    public function specializationSave($nSpecializationID, array $aData)
    {
        if (empty($aData)) {
            return false;
        }

        if (isset($aData['price_sett'])) {
            $aData['price_sett'] = serialize($aData['price_sett']);
        }

        $aSave = array_diff_key($aData, $this->langSpecializations);
        if (isset($aData['title'])) {
            $aSave['title'] = $aData['title'][LNG];
        }

        if ($nSpecializationID > 0) {
            $aSave['modified'] = $this->db->now(); # Дата изменения

            $res = $this->db->update(TABLE_SPECIALIZATIONS, $aSave, array('id' => $nSpecializationID));

            $this->db->langUpdate($nSpecializationID, $aData, $this->langSpecializations, TABLE_SPECIALIZATIONS_LANG);
            return !empty($res);
        } else {
            $aSave['created'] = $this->db->now(); # Дата создания
            $aSave['modified'] = $this->db->now(); # Дата изменения
            $aSave['num'] = $this->db->one_data('SELECT MAX(NUM) FROM ' . TABLE_SPECIALIZATIONS) + 1;

            $nSpecializationID = $this->db->insert(TABLE_SPECIALIZATIONS, $aSave);
            if ($nSpecializationID > 0) {
                $this->db->langInsert($nSpecializationID, $aData, $this->langSpecializations, TABLE_SPECIALIZATIONS_LANG);
            }

            return $nSpecializationID;
        }
    }

    /**
     * Переключатели специализации
     * @param integer $nSpecializationID ID специализации
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function specializationToggle($nSpecializationID, $sField)
    {
        switch ($sField) {
            case 'enabled':
            { # Включен
                return $this->toggleInt(TABLE_SPECIALIZATIONS, $nSpecializationID, $sField, 'id');
            }
            break;
        }
    }

    /**
     * Перемещение специализации
     * @param integer $nCatID ID категории
     * @return mixed @see rotateTablednd
     */
    public function specializationsRotate($nCatID = 0)
    {
        if ($nCatID) {
            return $this->db->rotateTablednd(TABLE_SPECIALIZATIONS_IN_CATS, ' AND cat_id = ' . $nCatID, 'spec_id');
        } else {
            return $this->db->rotateTablednd(TABLE_SPECIALIZATIONS);
        }
    }

    /**
     * Удаление специализации
     * @param integer $nSpecializationID ID специализации
     * @return boolean
     */
    public function specializationDelete($nSpecializationID)
    {
        if (empty($nSpecializationID)) {
            return false;
        }
        $res = $this->db->delete(TABLE_SPECIALIZATIONS, array('id' => $nSpecializationID));
        if (!empty($res)) {
            $this->db->delete(TABLE_SPECIALIZATIONS_LANG, array('id' => $nSpecializationID));
            $this->db->delete(TABLE_SPECIALIZATIONS_IN_CATS, array('spec_id' => $nSpecializationID));
            $this->db->exec('
                DELETE SL, S FROM '.TABLE_SPECIALIZATIONS_SERVICES_LANG.' SL
                    INNER JOIN '.TABLE_SPECIALIZATIONS_SERVICES.' S ON SL.id = S.id
                WHERE S.spec_id = :id', array(':id' => $nSpecializationID));
            return true;
        }

        return false;
    }

    /**
     * Список специализации в категории
     * @param integer|boolean $nCategoryID ID категории, false - все категории
     * @param array $aFields - выбираемые поля
     * @param array $aParam - параметры
     *          $aParam['enabled'] - только влюченные специализации
     *          $aParam['all'] - все специализации с флагом 'a' для включенных в категории $nCategoryID
     * @return array
     */
    public function specializationsInCategory($nCategoryID = false, $aFields = array('S.*', 'SL.*'), $aParam = array())
    {
        $aWhere = array('S.pid = :root');
        if (!empty($aParam['enabled'])) {
            $aWhere[] = 'S.enabled = 1';
        }
        $bAll = !empty($aParam['all']);
        if ($aFields === 'id') {
            return $this->db->select_one_column('SELECT S.id
                    FROM ' . TABLE_SPECIALIZATIONS . ' S
                         ' . ($nCategoryID !== false ? ($bAll ? ' LEFT ' : ' INNER ') . ' JOIN ' . TABLE_SPECIALIZATIONS_IN_CATS . ' C ON C.cat_id = ' . $nCategoryID . ' AND C.spec_id = S.id ' : '') . '
                    WHERE ' . join(' AND ', $aWhere) . '
                    ORDER BY ' . (!$bAll && $nCategoryID !== false ? 'C.num' : 'S.num'), array('root' => Specializations::ROOT_SPEC)
            );
        } else {
            if ($nCategoryID) {
                $aFields[] = 'C.cat_id AS a';
            }
            return $this->db->select_key('SELECT ' . join(', ', $aFields) . '
                    FROM ' . TABLE_SPECIALIZATIONS . ' S
                        ' . ($nCategoryID !== false ? ($bAll ? ' LEFT ' : ' INNER ') . ' JOIN ' . TABLE_SPECIALIZATIONS_IN_CATS . ' C ON C.cat_id = ' . $nCategoryID . ' AND C.spec_id = S.id ' : '') . ',
                        ' . TABLE_SPECIALIZATIONS_LANG . ' SL
                    WHERE ' . join(' AND ', $aWhere) . $this->db->langAnd(true, 'S', 'SL') . '
                    ORDER BY ' . (!$bAll && $nCategoryID !== false ? 'C.num' : 'S.num'), 'id', array('root' => Specializations::ROOT_SPEC)
            );
        }
    }

    /**
     * Список специализаций в категориях
     * @param array $aCategories ID категорий
     * @param array $aFields - выбираемые поля
     * @return array [cat_id][spec_id] => data
     */
    public function specializationsInCategories($aCategories, $aFields = array())
    {
        $aWhere = array(
            $this->db->langAnd(false, 'S', 'SL'),
            'S.pid = :root',
        );
        $sFrom = '';
        if (Specializations::catsOn()) {
            if (empty($aCategories)) {
                return array();
            }
            $sFrom = TABLE_SPECIALIZATIONS_IN_CATS . ' C, ';
            $aWhere[] = $this->db->prepareIN('C.cat_id', $aCategories);
            $aFields[] = 'C.cat_id';
            $aFields[] = 'C.spec_id';
        } else {
            $aFields[] = 'S.id';
        }
        $aData = $this->db->select('SELECT ' . join(', ', $aFields) . '
                FROM ' . $sFrom . TABLE_SPECIALIZATIONS . ' S, ' . TABLE_SPECIALIZATIONS_LANG . ' SL
                WHERE ' . join(' AND ', $aWhere), array('root' => Specializations::ROOT_SPEC)
        );
        $aResult = array();

        if (Specializations::catsOn()) {
            foreach ($aData as $v) {
                $aResult[$v['cat_id']][$v['spec_id']] = $v;
            }
        } else {
            foreach ($aData as $v) {
                $aResult[0][$v['id']] = $v;
            }
        }

        return $aResult;
    }

    /**
     * Список всех специализаций по категориям
     * @param array $aFieldSpecs выбираемые поля для специализации
     * @param array $aFieldCats выбираемые поля для категории
     * @param bool $bCounts расчитать кол-во специализаций в категориях
     * @return mixed
     */
    public function specializationsInAllCategories(array $aFieldSpecs = array(), array $aFieldCats = array(), $bCounts = false)
    {
        if (!in_array('id', $aFieldSpecs)) {
            $aFieldSpecs[] = 'id';
        }
        foreach ($aFieldSpecs as &$v) {
            $v = (array_key_exists($v, $this->langSpecializations) ? 'SL' : 'S') . '.' . $v;
        } unset($v);

        if (Specializations::catsOn()) {
            if (!in_array('id', $aFieldCats)) {
                $aFieldCats[] = 'id';
            }
            if (!in_array('pid', $aFieldCats)) {
                $aFieldCats[] = 'id';
            }
            foreach ($aFieldCats as &$v) {
                $v = (array_key_exists($v, $this->langCategories) ? 'CL' : 'C') . '.' . $v;
            } unset($v);

            $aResult = $this->db->select_key('
                SELECT ' . join(',', $aFieldCats) . '
                FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C,
                     ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL
                WHERE ' . $this->db->langAnd(false, 'C', 'CL') . ' AND C.pid > 0 AND C.enabled = 1
                ORDER BY C.numleft', 'id'
            );

            $aData = $this->db->select('
                SELECT C.cat_id, ' . join(',', $aFieldSpecs) . '
                FROM ' . TABLE_SPECIALIZATIONS_IN_CATS . ' C,
                     ' . TABLE_SPECIALIZATIONS . ' S,
                     ' . TABLE_SPECIALIZATIONS_LANG . ' SL
                WHERE C.spec_id = S.id ' . $this->db->langAnd(true, 'S', 'SL') . ' AND S.pid > 0 AND S.enabled = 1
                ORDER BY C.num'
            );

            if ($bCounts) {
                $aCounts = $this->db->select_key('
                    SELECT id, COUNT(C.cat_id) AS cnt
                    FROM ' . TABLE_SPECIALIZATIONS . ' S,
                         ' . TABLE_SPECIALIZATIONS_IN_CATS . ' C
                    WHERE S.id = C.spec_id
                    GROUP BY S.id', 'id'
                );
            }

            foreach ($aData as $v) {
                if (!isset($aResult[$v['cat_id']])) {
                    continue;
                }
                if (!isset($aResult[$v['cat_id']]['specs'])) {
                    $aResult[$v['cat_id']]['specs'] = array();
                }
                if ($bCounts) {
                    $v['cnt'] = !empty($aCounts[$v['id']]['cnt']) ? $aCounts[$v['id']]['cnt'] : 0;
                }
                $aResult[$v['cat_id']]['specs'][$v['id']] = $v;
            }
            foreach($aResult as $k => $v){
                if(empty($v['specs'])){
                    unset($aResult[$k]);
                }
            }
            return $aResult;
        } else {
            return $this->db->select_key('
                SELECT ' . join(',', $aFieldSpecs) . '
                FROM ' . TABLE_SPECIALIZATIONS . ' S,
                     ' . TABLE_SPECIALIZATIONS_LANG . ' SL
                WHERE ' . $this->db->langAnd(false, 'S', 'SL') . ' AND S.pid > 0 AND S.enabled = 1
                ORDER BY S.num', 'id'
            );
        }
    }

    /**
     * Список полных keywords для специализаций
     * @return array|mixed
     */
    public function specsKeywords()
    {
        if (Specializations::catsOn()) {
            $aCounts = $this->db->select_key('
                SELECT id, COUNT(C.cat_id) AS cnt
                FROM ' . TABLE_SPECIALIZATIONS . ' S,
                     ' . TABLE_SPECIALIZATIONS_IN_CATS . ' C
                WHERE S.id = C.spec_id
                GROUP BY S.id', 'id'
            );

            $aData = $this->db->select('
                SELECT S.id AS spec_id, S.keyword AS spec_keyword, C.id AS cat_id, C.keyword AS cat_keyword
                FROM ' . TABLE_SPECIALIZATIONS . ' S,
                     ' . TABLE_SPECIALIZATIONS_IN_CATS . ' SC,
                     ' . TABLE_SPECIALIZATIONS_CATS . ' C
                WHERE S.id = SC.spec_id AND SC.cat_id = C.id
            ');
            $aResult = array();
            foreach ($aData as $v) {
                if (!empty($aCounts[$v['spec_id']]['cnt'])) {
                    $v['cnt'] = $aCounts[$v['spec_id']]['cnt'];
                } else {
                    $v['cnt'] = 0;
                }
                if ($v['cnt'] > 1) {
                    $aResult[$v['spec_keyword'] . '-' . $v['cat_keyword']] = $v;
                }
                $aResult[$v['spec_keyword']] = $v;
            }

            return $aResult;
        } else {
            return $this->db->select_key('
                SELECT S.id AS spec_id, S.keyword AS spec_keyword
                FROM ' . TABLE_SPECIALIZATIONS . ' S
            ', 'spec_keyword');
        }
    }

    /**
     * Формирование списков категорий / специализаций (при добавлении/редактировании записи)
     * @param integer $nSpecializationID ID специализации (selected)
     * @param integer $nCategoryID ID категории (0 - все)
     * @param mixed $mOptions select-options
     *              'empty' => 'Выбрать' - название не выбранного для специализаций
     *              'emptyCats' => array('empty'=>'Все') $mOptions для категорий
     *              'noAllSpec' => 1 - не выдавать специализации если не выбранна категория $nCategoryID
     * @return array [lvl=>[a=>selectedID, categories=>список категорий(массив или options)],...]
     */
    public function specializationsOptions($nSpecializationID = 0, $nCategoryID = 0, $mOptions = false)
    {
        $aSql = array();
        $sOrder = 'S.num';

        if (Specializations::catsOn()) {
            if (!$nCategoryID && $nSpecializationID) {
                $nCategoryID = $this->db->one_data('SELECT cat_id FROM ' . TABLE_SPECIALIZATIONS_IN_CATS . ' WHERE spec_id = :spec LIMIT 1 ', array(':spec' => $nSpecializationID));
            }
            if ($nCategoryID) {
                $aSql[':cat'] = array('C.cat_id = :catid', ':catid' => $nCategoryID);
                $sOrder = 'C.num';
            }
        } else {
            $nCategoryID = 0;
        }
        $aSpecs = $this->specializationsListing($aSql, false, '', $sOrder);
        $aCategoriesID = array();
        if (Specializations::catsOn()) {
            $aCategoriesID = $this->categoriesOptionsByLevel($this->categoryParentsID($nCategoryID), isset($mOptions['emptyCats']) ? $mOptions['emptyCats'] : array('empty' => 'Все категории'));
        }
        if (!Specializations::catsOn() || empty($mOptions['noAllSpec']) ||
            (!empty($mOptions['noAllSpec']) && $nCategoryID)
        ) {
            $aCategoriesID['spec'] = array(
                'a'          => $nSpecializationID,
                'categories' => HTML::selectOptions($aSpecs, $nSpecializationID, isset($mOptions['empty']) ? $mOptions['empty'] : false, 'id', 'title'),
                'cnt'        => count($aSpecs),
            );
        }

        return $aCategoriesID;
    }

    /**
     * Список специализаций из массива
     * @param array $aSpecs массив требуемых специализаций
     * @return array
     */
    public function specializationsListingInArray(array $aSpecs)
    {
        if (empty($aSpecs)) {
            return array();
        }
        $aData = $this->db->select_key('
            SELECT S.id, S.keyword, SL.title, SL.price_title
             FROM ' . TABLE_SPECIALIZATIONS . ' S,
                  ' . TABLE_SPECIALIZATIONS_LANG . ' SL
            WHERE ' . $this->db->langAnd(false, 'S', 'SL') . '
              AND ' . $this->db->prepareIN('S.id', $aSpecs) . '
            ORDER BY S.num
        ', 'id');
        if (empty($aData)) {
            $aData = array();
        }
        return $aData;
    }

    /**
     * Поиск по части названия специализации во всех категориях
     * @param string $str строка поиска
     * @return array|mixed
     */
    public function specializationInCatsSearch($str)
    {
        if( ! $str) return array();
        if( ! Specializations::catsOn()) return array();
        $limit = config::sysAdmin('specializations.search.limit', 10, TYPE_UINT);
        $data = $this->db->select('
            SELECT R.id AS spec_id, R.title AS spec_title, C.id AS cat_id, CL.title AS cat_title
            FROM (
                    SELECT S.id, SL.title
                    FROM '.TABLE_SPECIALIZATIONS.' S, '.TABLE_SPECIALIZATIONS_LANG.' SL
                    WHERE '.$this->db->langAnd(false, 'S', 'SL').' AND SL.title LIKE :str
                    LIMIT '.$limit.'
                ) R,
                ' . TABLE_SPECIALIZATIONS_IN_CATS . ' SC,
                ' . TABLE_SPECIALIZATIONS_CATS . ' C,
                ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL
            WHERE R.id = SC.spec_id AND SC.cat_id = C.id AND '.$this->db->langAnd(false, 'C', 'CL').'
            ORDER BY C.numleft, SC.num 
        ', array(':str' => '%'.$str.'%'));
        $specs = array();
        foreach($data as $k => $v){
            if(in_array($v['spec_id'], $specs)){
                unset($data[$k]);
                continue;
            }
            $specs[] = $v['spec_id'];
        }
        return $data;
    }

    /**
     * Получение настроек услуг специализации
     * @param integer $nSpecializationID ID специализации
     * @param bool $bEdit
     * @return mixed
     */
    public function specializationDataServices($nSpecializationID, $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->select_key('SELECT S.*
                    FROM ' . TABLE_SPECIALIZATIONS_SERVICES . ' S
                    WHERE S.spec_id = :id
                    ORDER BY S.num',
                'id', array(':id' => $nSpecializationID)
            );
            if (!empty($aData)) {
                $aLang = $this->db->select('SELECT SL.*
                    FROM ' . TABLE_SPECIALIZATIONS_SERVICES_LANG . ' SL, '.TABLE_SPECIALIZATIONS_SERVICES.' S
                    WHERE SL.id = S.id AND S.spec_id = :id',
                    array(':id' => $nSpecializationID)
                );
                foreach ($aLang as $v) {
                    if (isset($aData[ $v['id'] ])) {
                        foreach ($this->langServices as $field => $vv) {
                            $aData[ $v['id'] ][ $field ][ $v['lang'] ] = $v[ $field ];
                        }
                    }
                }
                unset($aLang);
            }
        } else {
            $aData = $this->db->select_key('SELECT S.*, SL.title, SL.measure
                    FROM ' . TABLE_SPECIALIZATIONS_SERVICES . ' S, ' . TABLE_SPECIALIZATIONS_SERVICES_LANG . ' SL
                    WHERE S.spec_id = :id ' . $this->db->langAnd(true, 'S', 'SL').'
                    ORDER BY S.num',
                'id', array(':id' => $nSpecializationID)
            );
        }
        return $aData;
    }

    /**
     * Получение данных о услуге специализации
     * @param integer $serviceID ID услуги
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function specializationServiceData($serviceID, $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT S.*
                    FROM ' . TABLE_SPECIALIZATIONS_SERVICES . ' S
                    WHERE S.id = :id',
                array(':id' => $serviceID)
            );
            if (!empty($aData)) {
                $this->db->langSelect($serviceID, $aData, $this->langServices, TABLE_SPECIALIZATIONS_SERVICES_LANG);
            }
        } else {
            $aData = $this->db->one_array('SELECT S.*, SL.title as title
                    FROM ' . TABLE_SPECIALIZATIONS_SERVICES . ' S, ' . TABLE_SPECIALIZATIONS_SERVICES_LANG . ' SL
                    WHERE S.id = :id ' . $this->db->langAnd(true, 'S', 'SL'),
                array(':id' => $serviceID)
            );
        }
        return $aData;
    }

    /**
     * Получение данных об услуге специализации по ключу
     * @param string $keyword ключ
     * @return array
     */
    public function specializationServiceDataByKeyword($keyword)
    {
        if (empty($keyword)) return array();
        return $this->db->one_array('
            SELECT S.*, SL.*
            FROM '.TABLE_SPECIALIZATIONS_SERVICES.' S,
                 '.TABLE_SPECIALIZATIONS_SERVICES_LANG.' SL
            WHERE '.$this->db->langAnd(false, 'S', 'SL').' AND S.keyword = :keyword
        ', array(':keyword' => $keyword));
    }

    /**
     * Сохранение услуги специализации
     * @param integer $serviceID ID услуги
     * @param array $aData данные услуги
     * @return boolean|integer
     */
    public function specializationServiceSave($serviceID, array $aData)
    {
        if (empty($aData)) {
            return false;
        }

        $aSave = array_diff_key($aData, $this->langServices);

        if ($serviceID > 0) {
            $aSave['modified'] = $this->db->now(); # Дата изменения

            $res = $this->db->update(TABLE_SPECIALIZATIONS_SERVICES, $aSave, array('id' => $serviceID));

            $this->db->langUpdate($serviceID, $aData, $this->langServices, TABLE_SPECIALIZATIONS_SERVICES_LANG);
            return !empty($res);
        } else {
            if (empty($aSave['spec_id'])) {
                return false;
            }
            $aSave['created'] = $this->db->now(); # Дата создания
            $aSave['modified'] = $this->db->now(); # Дата изменения
            $aSave['num'] = $this->db->one_data('SELECT MAX(num) FROM ' . TABLE_SPECIALIZATIONS_SERVICES .' WHERE spec_id = :spec', array(':spec' => $aSave['spec_id'])) + 1;

            $serviceID = $this->db->insert(TABLE_SPECIALIZATIONS_SERVICES, $aSave);
            if ($serviceID > 0) {
                $this->db->langInsert($serviceID, $aData, $this->langServices, TABLE_SPECIALIZATIONS_SERVICES_LANG);
            }
            return $serviceID;
        }
    }

    /**
     * Перемещение услуг в специализации
     * @param integer $specID ID специализации
     * @return mixed @see rotateTablednd
     */
    public function specializationServicesRotate($specID)
    {
        if ( ! $specID) return false;
        return $this->db->rotateTablednd(TABLE_SPECIALIZATIONS_SERVICES, ' AND spec_id = ' . $specID, 'id', 'num', false, '', 'dnds-');
    }

    /**
     * Удаление услуги специализации
     * @param integer $specID ID специализации
     * @return boolean
     */
    public function specializationServiceDelete($specID)
    {
        if (empty($specID)) {
            return false;
        }
        $res = $this->db->delete(TABLE_SPECIALIZATIONS_SERVICES, array('id' => $specID));
        if (!empty($res)) {
            $this->db->delete(TABLE_SPECIALIZATIONS_SERVICES_LANG, array('id' => $specID));
            return true;
        }
        return false;
    }

    /**
     * Список услуг специализаций из массива
     * @param array $aSpecs массив требуемых специализаций
     * @return array
     */
    public function specializationsServicesListingInArray(array $aSpecs)
    {
        if (empty($aSpecs)) {
            return array();
        }
        $aSpecs = array_unique($aSpecs);
        return $this->db->select_key('
            SELECT S.*, SL.title, SL.measure
            FROM '.TABLE_SPECIALIZATIONS_SERVICES . ' S,
                 '.TABLE_SPECIALIZATIONS_SERVICES_LANG . ' SL
            WHERE '.$this->db->langAnd(false, 'S', 'SL').'
              AND '.$this->db->prepareIN('S.spec_id', $aSpecs) . '
            ORDER BY S.num
        ', 'id');

    }

    /**
     * Получаем ID верхней категории в которой находится специализация
     * @param integer $nSpecializationID ID специализации
     * @return array
     */
    public function specializationCategoryFirst($nSpecializationID)
    {
        return (int)$this->db->one_data('SELECT C.id
                FROM ' . TABLE_SPECIALIZATIONS_IN_CATS . ' SC,
                     ' . TABLE_SPECIALIZATIONS_CATS . ' C
                WHERE SC.spec_id = :spec AND SC.cat_id = C.id
                ORDER BY C.numleft ASC
                LIMIT 1',
            array(':spec' => $nSpecializationID)
        );
    }

    /**
     * Список категорий
     * @param array $aFilter фильтр списка категорий
     * @param bool $bCount только подсчет кол-ва категорий
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function categoriesListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '') //admin
    {
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter[] = 'pid != 0';
        $aFilter = $this->prepareFilter($aFilter, 'C');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(C.id) FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C, ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL ' . $aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('SELECT C.id, C.created, CL.title, C.enabled, C.pid, C.numlevel, ((C.numright-C.numleft)-1) as node
               FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C, ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL
               ' . $aFilter['where']
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']
        );
    }

    /**
     * Список категорий
     * @param array $aFilter фильтр списка категорий
     * @param bool $bCount только подсчет кол-ва категорий
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function categoriesList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        $aFilter[] = 'pid != 0';
        if (!$bCount) {
            $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        }
        $aFilter = $this->prepareFilter($aFilter, 'C');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(C.id) FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C ' . $aFilter['where'], $aFilter['bind']);
        }

        $aData = $this->db->select('SELECT C.id
                                  FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C, ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL
                                  ' . $aFilter['where'] . '
                                  ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
                                  ' . $sqlLimit, $aFilter['bind']
        );

        if (!empty($aData)) {
            //
        }

        return $aData;
    }

    /**
     * Получение данных категории
     * @param integer $nCategoryID ID категории
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function categoryData($nCategoryID, $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT C.*, ((C.numright-C.numleft)-1) as node
                    FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C
                    WHERE C.id = :id',
                array(':id' => $nCategoryID)
            );
            if (!empty($aData)) {
                $this->db->langSelect($nCategoryID, $aData, $this->langCategories, TABLE_SPECIALIZATIONS_CATS_LANG);
            }
        } else {
            $aData = $this->db->one_array('SELECT C.*, ((C.numright-C.numleft)-1) as node, CL.title
                    FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C, ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL
                    WHERE C.id = :id ' . $this->db->langAnd(true, 'C', 'CL'),
                array(':id' => $nCategoryID)
            );
        }

        return $aData;
    }

    /**
     * Сохранение категории
     * @param integer $nCategoryID ID категории
     * @param array $aData данные категории
     * @return boolean|integer
     */
    public function categorySave($nCategoryID, array $aData)
    {
        if (empty($aData)) {
            return false;
        }
        $aSpecs = $aData['specs'];
        unset($aData['specs']);

        if ($nCategoryID > 0) {
            $aData['modified'] = $this->db->now(); # Дата изменения

            if (isset($aData['pid'])) { # запрет изменения pid
                unset($aData['pid']);
            }
            $res = $this->db->update(TABLE_SPECIALIZATIONS_CATS, array_diff_key($aData, $this->langCategories), array('id' => $nCategoryID));

            $this->db->langUpdate($nCategoryID, $aData, $this->langCategories, TABLE_SPECIALIZATIONS_CATS_LANG);
            $this->categorySaveSpecs($nCategoryID, $aSpecs);

            return !empty($res);
        } else {

            $aData['created'] = $this->db->now(); # Дата создания
            $aData['modified'] = $this->db->now(); # Дата изменения

            $nCategoryID = $this->treeCategories->insertNode($aData['pid']);
            if ($nCategoryID > 0) {
                unset($aData['pid']);
                $this->db->update(TABLE_SPECIALIZATIONS_CATS, array_diff_key($aData, $this->langCategories), 'id = :id', array(':id' => $nCategoryID));
                $this->db->langInsert($nCategoryID, $aData, $this->langCategories, TABLE_SPECIALIZATIONS_CATS_LANG);
                $this->categorySaveSpecs($nCategoryID, $aSpecs);
            }

            return $nCategoryID;
        }
    }

    /**
     * Сохранение специализаций в категории
     * @param integer $nCategoryID ID категории
     * @param array $aSpecs массив специализаций
     * @return boolean
     */
    public function categorySaveSpecs($nCategoryID, $aSpecs)
    {
        if (!$nCategoryID) {
            return false;
        }
        $aData = $this->db->select_key('
            SELECT cat_id, spec_id, num
            FROM ' . TABLE_SPECIALIZATIONS_IN_CATS . '
            WHERE cat_id = :cat', 'spec_id', array(':cat' => $nCategoryID)
        );
        $aInsert = array();
        $nCnt = 1;
        if (!empty($aData)) {
            foreach ($aData as $v) {
                if ($nCnt <= $v['num']) {
                    $nCnt = $v['num'] + 1;
                }
            }
        }
        foreach ($aSpecs as $spec_id) {
            if (!isset($aData[$spec_id])) {
                $aInsert[] = array(
                    'cat_id'  => $nCategoryID,
                    'spec_id' => $spec_id,
                    'num'     => $nCnt++,
                );
            } else {
                unset($aData[$spec_id]);
            }
        }
        if (!empty($aInsert)) {
            $this->db->multiInsert(TABLE_SPECIALIZATIONS_IN_CATS, $aInsert);
        }
        if (!empty($aData)) {
            foreach ($aData as $v) {
                $this->db->delete(TABLE_SPECIALIZATIONS_IN_CATS, array(
                        'cat_id'  => $nCategoryID,
                        'spec_id' => $v['spec_id'],
                    )
                );
            }
        }

        return true;
    }

    /**
     * Переключатели категории
     * @param integer $nCategoryID ID категории
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function categoryToggle($nCategoryID, $sField)
    {
        switch ($sField) {
            case 'enabled': # Включена
            {
                return $this->toggleInt(TABLE_SPECIALIZATIONS_CATS, $nCategoryID, $sField, 'id');
            }
            break;
        }
    }

    /**
     * Перемещение категории
     * @return mixed @see rotateTablednd
     */
    public function categoriesRotate()
    {
        return $this->treeCategories->rotateTablednd();
    }

    /**
     * Удаление категории
     * @param integer $nCategoryID ID категории
     * @return boolean
     */
    public function categoryDelete($nCategoryID)
    {
        if (empty($nCategoryID)) {
            return false;
        }
        $nSubCnt = $this->categorySubCount($nCategoryID);
        if (!empty($nSubCnt)) {
            $this->errors->set('Невозможно выполнить удаление категории при наличии подкатегорий');

            return false;
        }

        $nItems = $this->db->one_data('SELECT COUNT(I.spec_id) FROM ' . TABLE_SPECIALIZATIONS_IN_CATS . ' I WHERE I.cat_id = :id', array(':id' => $nCategoryID));
        if (!empty($nItems)) {
            $this->errors->set('Невозможно выполнить удаление категории при наличии вложенных элементов');

            return false;
        }

        $aDeletedID = $this->treeCategories->deleteNode($nCategoryID);
        $res = !empty($aDeletedID);
        if (!empty($res)) {
            $this->db->delete(TABLE_SPECIALIZATIONS_CATS_LANG, array('id' => $nCategoryID));

            return true;
        }

        return false;
    }

    /**
     * Получаем кол-во вложенных категорий
     */
    public function categorySubCount($nCategoryID)
    {
        return $this->treeCategories->getChildrenCount($nCategoryID);
    }

    /**
     * Формирование списка подкатегорий
     * @param integer $nCategoryID ID категории
     * @param mixed $mOptions формировать select-options или FALSE
     * @param array $aFilter фильтр категорий
     * @return array|string
     */
    public function categorySubOptions($nCategoryID, $mOptions = false, $aFilter = array())
    {
        $aFilter['pid'] = $nCategoryID;
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter = $this->prepareFilter($aFilter, 'C');
        $aData = $this->db->select('SELECT C.id, CL.title
                    FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C, ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL
                    ' . $aFilter['where'] . '
                    ORDER BY C.numleft', $aFilter['bind']
        );

        if (empty($mOptions)) {
            return $aData;
        }

        return HTML::selectOptions($aData, $mOptions['sel'], $mOptions['empty'], 'id', 'title');
    }

    /**
     * Формирование списка основных категорий
     * @param integer $nSelectedID ID выбранной категории
     * @param mixed $mEmptyOpt невыбранное значение
     * @param integer $nType тип списка: 0 - все(кроме корневого), 1 - список при добавлении категории, 2 - список при добавлении записи
     * @param array $aOnlyID только список определенных категорий
     * @return string <option></option>...
     */
    public function categoriesOptions($nSelectedID = 0, $mEmptyOpt = false, $nType = 0, $aOnlyID = array())
    {
        $aFilter = array();
        if ($nType == 1) {
            $aFilter[] = 'numlevel < 1';
        } else {
            $aFilter[] = 'numlevel > 0';
        }
        if (!empty($aOnlyID)) {
            $aFilter[':only'] = '(C.id IN (' . join(',', $aOnlyID) . ') OR C.pid IN(' . join(',', $aOnlyID) . '))';
        }

        # Chrome не понимает style="padding" в option
        $bUsePadding = (mb_stripos(Request::userAgent(), 'chrome') === false);
        $bJoinItems = ($nType > 0);
        $aFilter[':lang'] = $this->db->langAnd(false, 'C', 'CL');
        $aFilter = $this->prepareFilter($aFilter, 'C');

        $aCategories = $this->db->select('SELECT C.id, CL.title, C.numlevel, ((C.numright-C.numleft)-1) as node
                    ' . ($bJoinItems ? ', COUNT(I.spec_id) as items ' : '') . '
               FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C
                    ' . ($bJoinItems ? ' LEFT JOIN ' . TABLE_SPECIALIZATIONS_IN_CATS . ' I ON C.id = I.cat_id ' : '') . '
                    , ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL
               ' . $aFilter['where'] . '
               GROUP BY C.id
               ORDER BY C.numleft', $aFilter['bind']
        );

        $sOptions = '';
        foreach ($aCategories as $v) {
            $nNumlevel = & $v['numlevel'];
            $bDisable = ($nType > 0 && ($nType == 2 ? $v['node'] > 0 : ($nNumlevel > 0 && $v['items'] > 0)));
            $sOptions .= '<option value="' . $v['id'] . '" ' .
                ($bUsePadding && $nNumlevel > 1 ? 'style="padding-left:' . ($nNumlevel * 10) . 'px;" ' : '') .
                ($v['id'] == $nSelectedID ? ' selected' : '') .
                ($bDisable ? ' disabled' : '') .
                '>' . (!$bUsePadding && $nNumlevel > 1 ? str_repeat('  ', $nNumlevel) : '') . $v['title'] . '</option>';
        }

        if ($mEmptyOpt !== false) {
            $nValue = 0;
            if (is_array($mEmptyOpt)) {
                $nValue = key($mEmptyOpt);
                $mEmptyOpt = current($mEmptyOpt);
            }
            $sOptions = '<option value="' . $nValue . '" class="bold">' . $mEmptyOpt . '</option>' . $sOptions;
        }

        return $sOptions;
    }

    /**
     * Формирование списков категорий (при добавлении/редактировании записи)
     * @param array $aCategoriesID ID категорий [lvl=>selectedID, ...]
     * @param mixed $mOptions формировать select-options или нет (false)
     * @return array [lvl=>[a=>selectedID, categories=>список категорий(массив или options)],...]
     */
    public function categoriesOptionsByLevel($aCategoriesID, $mOptions = false)
    {
        if (empty($aCategoriesID)) {
            return array();
        }

        // формируем список требуемых уровней категорий
        $aLevels = array();
        $bFill = true;
        $parentID = 1;
        foreach ($aCategoriesID as $lvl => $nCategoryID) {
            if ($nCategoryID || $bFill) {
                $aLevels[$lvl] = $parentID;
                if (!$nCategoryID) {
                    break;
                }
                $parentID = $nCategoryID;
            } else {
                break;
            }
        }

        if (empty($aLevels)) {
            return array();
        }

        $aData = $this->db->select('SELECT C.id, CL.title, C.numlevel as lvl
                    FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C, ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL
                    WHERE C.numlevel IN (' . join(',', array_keys($aLevels)) . ')
                      AND C.pid IN(' . join(',', $aLevels) . ')
                      ' . $this->db->langAnd(true, 'C', 'CL') . '
                    ORDER BY C.numleft'
        );
        if (empty($aData)) {
            return array();
        }

        $aLevels = array();
        foreach ($aData as $v) {
            $aLevels[$v['lvl']][$v['id']] = $v;
        }
        unset($aData);

        foreach ($aCategoriesID as $lvl => $nSelectedID) {
            if (isset($aLevels[$lvl])) {
                $aCategoriesID[$lvl] = array(
                    'a'          => $nSelectedID,
                    'categories' => (!empty($mOptions) ?
                            HTML::selectOptions($aLevels[$lvl], $nSelectedID, $mOptions['empty'], 'id', 'title') :
                            $aLevels[$lvl]),
                );
            } else {
                $aCategoriesID[$lvl] = array(
                    'a'          => $nSelectedID,
                    'categories' => false,
                );
            }
        }

        return $aCategoriesID;
    }

    /**
     * Получаем данные parent-категорий
     * @param integer $nCategoryID ID категории
     * @param bool $bIncludingSelf включать текущую в итоговых список
     * @param bool $bExludeRoot исключить корневой раздел
     * @return array array(lvl=>id, ...)
     */
    public function categoryParentsID($nCategoryID, $bIncludingSelf = true, $bExludeRoot = true)
    {
        if (empty($nCategoryID)) {
            return array(1 => 0);
        }
        $aData = $this->treeCategories->getNodeParentsID($nCategoryID, ($bExludeRoot ? ' AND numlevel > 0' : ''), $bIncludingSelf, array(
                'id',
                'numlevel'
            )
        );
        $aParentsID = array();
        if (!empty($aData)) {
            foreach ($aData as $v) {
                $aParentsID[$v['numlevel']] = $v['id'];
            }
        }

        return $aParentsID;
    }

    /**
     * Получаем названия parent-категорий
     * @param integer $nCategoryID ID категории
     * @param boolean $bIncludingSelf включать текущую в итоговых список
     * @param boolean $bExludeRoot исключить корневой раздел
     * @param mixed $mSeparator объединить в одну строку или FALSE
     * @return array array(lvl=>id, ...)
     */
    public function categoryParentsTitle($nCategoryID, $bIncludingSelf = false, $bExludeRoot = false, $mSeparator = true)
    {
        $aParentsID = $this->treeCategories->getNodeParentsID($nCategoryID, ($bExludeRoot ? ' AND numlevel > 0' : ''), $bIncludingSelf);
        if (empty($aParentsID)) {
            return ($mSeparator !== false ? '' : array());
        }

        $aData = $this->db->select_one_column('SELECT CL.title
                   FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C
                      , ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL
                   WHERE ' . $this->db->prepareIN('C.id', $aParentsID) . '' . $this->db->langAnd(true, 'C', 'CL') . '
                   ORDER BY C.numleft'
        );

        $aData = (!empty($aData) ? $aData : array());

        if ($mSeparator !== false) {
            return join('  ' . ($mSeparator === true ? '>' : $mSeparator) . '  ', $aData);
        } else {
            return $aData;
        }
    }

    /**
     * Получим ID корневой категории
     * @return int
     */
    public function categoryRootNodeID()
    {
        return $this->treeCategories->getRootNodeID(0, $bCreated);
    }

    /**
     * Получение meta данных категории специализации
     * @param mixed $mCategory ID/Keyword категории специализации
     * @param string $sModuleName название модуля
     * @return array
     */
    public function categoryMeta($mCategory, $sModuleName)
    {
        $prefix = mb_strtolower($sModuleName).'_';
        $aData = $this->db->one_array('SELECT
                    C.id, C.keyword,
                    C.'.$prefix.'mtemplate as mtemplate,
                    CL.title,
                    CL.'.$prefix.'mtitle as mtitle,
                    CL.'.$prefix.'mkeywords as mkeywords,
                    CL.'.$prefix.'mdescription as mdescription,
                    CL.'.$prefix.'seotext as seotext
                FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C, ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL
                WHERE C.'.(is_int($mCategory) ? 'id' : 'keyword').' = :cat '
                    . $this->db->langAnd(true, 'C', 'CL'),
            array(':cat' => $mCategory)
        );
        if (empty($aData)) $aData = array();
        return $aData;
    }

    /**
     * Список категорий из массива
     * @param array $aCats массив требуемых категорий
     * @return array
     */
    public function categoriesListingInArray(array $aCats)
    {
        if (empty($aCats)) {
            return array();
        }
        $aData = $this->db->select_key('
            SELECT C.id, C.keyword, CL.title
             FROM ' . TABLE_SPECIALIZATIONS_CATS . ' C,
                  ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL
            WHERE ' . $this->db->langAnd(false, 'C', 'CL') . '
              AND ' . $this->db->prepareIN('C.id', $aCats) . '
            ORDER BY C.numleft
        ', 'id');
        if (empty($aData)) {
            $aData = array();
        }
        return $aData;
    }

    public function getLocaleTables()
    {
        return array(
            TABLE_SPECIALIZATIONS          => array('type' => 'table', 'fields' => $this->langSpecializations, 'title' => _t('specs', 'Специализации')),
            TABLE_SPECIALIZATIONS_CATS     => array('type' => 'table', 'fields' => $this->langCategories, 'title' => _t('specs', 'Категории')),
            TABLE_SPECIALIZATIONS_DP       => array(
                'type'   => 'fields',
                'fields' => array(
                    'title'       => TYPE_NOTAGS,
                    'description' => TYPE_NOTAGS,
                    'group_id'    => TYPE_STR,
                ),
                'title' => _t('specs', 'Дин. свойства'),
            ),
            TABLE_SPECIALIZATIONS_DPM => array(
                'type' => 'fields',
                'fields' => array('name' => TYPE_NOTAGS),
                'title' => _t('specs', 'Дин. свойства (значения)'),
                'id' => array('dynprop_id', 'value'),
            ),
            TABLE_SPECIALIZATIONS_DP_ORDERS => array(
                'type'   => 'fields',
                'fields' => array(
                    'title'       => TYPE_NOTAGS,
                    'description' => TYPE_NOTAGS,
                    'group_id'    => TYPE_STR,
                ),
                'title' => _t('specs', 'Дин. свойства заказов'),
            ),
            TABLE_SPECIALIZATIONS_DPM_ORDERS => array(
                'type' => 'fields',
                'fields' => array('name' => TYPE_NOTAGS),
                'title' => _t('specs', 'Дин. свойства заказов (значения)'),
                'id' => array('dynprop_id', 'value'),
            ),
        );
    }
}