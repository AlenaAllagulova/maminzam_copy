<?php

class Qa_ extends QaBase
{
    public function init()
    {
        parent::init();

        if (bff::$class == $this->module_name && Request::isGET()) {
            bff::setActiveMenu('//qa');
        }
    }

    /**
     * Список, поиск вопросов
     */
    public function search()
    {
        $this->checkEnabled();

        $nPageSize = config::sysAdmin('qa.search.pagesize', 10, TYPE_UINT);

        $aFilter = array();
        if (static::premoderation()){
            $aFilter[] = 'moderated > 0';
        }
        $aData = array('list'=>array(), 'pgn'=>'', 'cat_id'=>0, 'spec_id'=>0);

        $f = array(
            'spec' => $this->input->getpost('spec', TYPE_NOTAGS), # keyword специализации или категории/специализации
        );
        $this->input->postgetm(array(
            'page' => TYPE_UINT,   # № страницы
            'q'    => TYPE_NOTAGS, # строка поиска
            'sl'   => TYPE_UINT,   # 0 - все / 1 - решенные / 2 - нерешенные
        ), $f);
        $aData['f'] = &$f;

        $seoFilter = 0;

        $urlData = array('keyword'=>'', 'canonical'=>'');
        if ( ! empty($f['spec']))
        {
            # анализируем URL keyword
            $urlData = Specializations::i()->parseURLKeyword($f['spec'], $this->module_name);
            if ($urlData === false) {
                if (Request::isAJAX()) {
                    $this->errors->reloadPage();
                    $this->ajaxResponseForm();
                }
                $this->errors->error404();
            }
            if ($urlData['spec']) { # специализация
                $aFilter['spec_id'] = $aData['spec_id'] = $urlData['spec']['id'];
                $aData['cat_id'] = ( $urlData['cat'] ? $urlData['cat']['id'] : $urlData['spec']['cat_id'] );
            } else if ($urlData['cat']) { # категория
                $aFilter['spec_id'] = $urlData['cat']['specs'];
                $aData['cat_id'] = $urlData['cat']['id'];
            }
        }
        $url = static::url('list', array('keyword'=>$urlData['keyword']));

        if ($f['q']) {
            $aFilter['text'] = $f['q'];
        }

        if ($f['sl']) {
            switch ($f['sl']) {
                case 1:
                    $aFilter['solved'] = 1; $seoFilter++;
                    break;
                case 2:
                    $aFilter['solved'] = 0; $seoFilter++;
                    break;
            }
        }

        $aData['cnt'] = $this->model->questionsList($aFilter, true);
        if ($aData['cnt']) {
            $pgnQuery = $f; unset($pgnQuery['spec']);
            $pgn = new Pagination($aData['cnt'], $nPageSize, array(
                'link'  => $url,
                'query' => $pgnQuery,
            ));
            $aData['list'] = $this->model->questionsList($aFilter, false, $pgn->getLimitOffset(), 'Q.created DESC');
            foreach ($aData['list'] as $k => &$v) {
                if ($f['q']) {
                    $v['n'] = ($pgn->getCurrentPage() - 1) * $nPageSize + $k + 1;
                    foreach (explode(' ', $f['q']) as $vv) {
                        $v['title'] = str_replace($vv, '<em>' . $vv . '</em>', $v['title']);
                    }
                }
            } unset($v);
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }

        if ($f['q']) {
            $aData['list'] = $this->viewPHP($aData, 'search.keywords.list');
        } else {
            $aData['list'] = $this->viewPHP($aData, 'search.list');
        }
        $aData['count'] = tpl::declension($aData['cnt'], _t('qa', 'вопрос;вопроса;вопросов'));
        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'   => $aData['pgn'],
                'list'  => $aData['list'],
                'count' => $aData['count'],
            ));
        }

        if ($f['q']) {
            # SEO: Поиск по ключевому слову
            $this->seo()->robotsIndex(false);
            $this->setMeta('search-keyword', array(
                'query' => tpl::truncate($f['q'], config::sysAdmin('qa.search.meta.query.truncate', 50, TYPE_UINT), ''),
                'page'  => $f['page'],
            ));
            return $this->viewPHP($aData, 'search.keywords');
        }

        # блок "Лидеры раздела"
        $aData['best'] = '';
        if (static::bestEnabled()) {
            $filter = array();
            if (isset($aFilter['spec_id'])) {
                $filter['spec_id'] = $aFilter['spec_id'];
            }
            $aData['best'] = $this->model->bestList($filter, static::bestLimit());
            $aData['best'] = $this->viewPHP($aData, 'best');
        }

        $aData['solved'] = array(
            0 => array('id' => 0, 't' => _t('qa', 'Все')),
            1 => array('id' => 1, 't' => _t('qa', 'Решенные')),
            2 => array('id' => 2, 't' => _t('qa', 'Нерешенные')),
        );

        # SEO:
        $seoData = array(
            'page' => $f['page'],
        );
        $this->seo()->robotsIndex(!$seoFilter);
        if ($aData['spec_id']) {
            # SEO: Список (специализация)
            $this->urlCorrection($url);
            $this->seo()->canonicalUrl(static::url('list', array('keyword'=>$urlData['canonical']), true), array('page'=>$f['page']));
            $seoData['title'] = $urlData['spec']['title'];
            $this->setMeta('listing-spec', $seoData, $urlData['spec']);
            $this->seo()->setText($urlData['spec'], $f['page']);
        } else if ($aData['cat_id']) {
            # SEO: Список (категория)
            $this->urlCorrection($url);
            $this->seo()->canonicalUrl(static::url('list', array('keyword'=>$urlData['canonical']), true), array('page'=>$f['page']));
            $seoData['title'] = $urlData['cat']['title'];
            $this->setMeta('listing-cat', $seoData, $urlData['cat']);
            $this->seo()->setText($urlData['cat'], $f['page']);
        } else {
            # SEO: Список
            $this->urlCorrection(static::url('list'));
            $this->seo()->canonicalUrl(static::url('list', array(), true), array('page'=>$f['page']));
            $this->setMeta('listing', $seoData, $seoData);
            $this->seo()->setText($seoData, $f['page']);
        }

        $aData['form'] = $this->searchForm($aData);
        return $this->viewPHP($aData, 'search');
    }

    protected function searchForm(array &$aData)
    {
        # Фильтр поиска
        $aData['specs'] = Specializations::model()->specializationsInAllCategories(
            array('id', 'keyword', 'title'), array('id', 'keyword', 'title'), true
        );
        if ($aData['specs']) {
            # формируем URL + помечаем активную категорию / специализацию
            $url = static::url('list');
            foreach ($aData['specs'] as &$v) {
                $v['url'] = $url.$v['keyword'].'/';
                if ( ! empty($v['specs'])) {
                    foreach ($v['specs'] as &$vv) {
                        $vv['url'] = $v['url'].$vv['keyword'].'/';
                        $vv['a'] = ( $aData['spec_id'] == $vv['id'] );
                    } unset($vv);
                    $v['a'] = ( $aData['cat_id'] == $v['id'] );
                } else {
                    $v['a'] = ( $aData['spec_id'] == $v['id'] );
                }
            } unset($v);
        }

        return $this->viewPHP($aData, 'search.form');
    }

    /**
     * Форма добавления вопроса
     */
    public function add()
    {
        bff::setMeta(_t('qa', 'Добавление вопроса'));
        $this->security->setTokenPrefix('qa-question-form');

        $aData = $this->validateQuestionData(0, Request::isPOST());
        $nUserID = User::id();

        $this->checkEnabled();

        if (Request::isPOST()) {
            do {
                $aResponse = array();

                if ( ! $nUserID) {
                    $userData = Users::i()->register($this->userRegisterEmbedded);
                    if (empty($userData['user_id']) && $this->errors->no()) {
                        $this->errors->set(_t('', 'Только для зарегистрированных пользователей'));
                        break;
                    }
                    $nUserID = $userData['user_id'];
                    if (empty($aData['user_id'])) {
                        $aData['user_id'] = $nUserID;
                    }
                    $aData['status'] = static::STATUS_NOTACTIVATED;
                } else {
                    if ( ! $this->security->validateToken()) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $aData['status'] = static::STATUS_OPENED;
                }
                if ( ! $this->errors->no()) {
                    break;
                }

                # доверенный пользователь: без модерации
                if ($trusted = User::isTrusted($this->module_name)) {
                    $aData['moderated'] = 1;
                }

                # создаем вопрос
                $nQuestionID = $this->model->questionSave(0, $aData);
                if ( ! $nQuestionID) {
                    $this->errors->reloadPage();
                    break;
                }

                # обновляем счетчик вопросов "на модерации"
                if (!$trusted) {
                    $this->moderationCounterUpdate(1);
                }

                $aResponse['redirect'] = static::url('status', array('id' => $nQuestionID));

            } while (false);
            $this->ajaxResponseForm($aResponse);
        }

        return $this->form(0, $aData);
    }

    /**
     * Форма редактирования вопроса
     */
    public function edit()
    {
        bff::setMeta(_t('qa', 'Редактирование вопроса'));
        $this->security->setTokenPrefix('qa-question-form');

        $nQuestionID = $this->input->get('id', TYPE_UINT);
        if ( ! $nQuestionID) {
            $this->errors->error404();
        }
        $aQuestionData = $this->model->questionData($nQuestionID, array(), true);
        if (empty($aQuestionData)) {
            $this->errors->error404();
        }

        if ( ! User::id()) {
            $this->redirect(Users::url('login'), false, true);
        }

        $this->checkEnabled();

        if ( ! $this->isQuestionOwner($nQuestionID, $aQuestionData['user_id'])) {
            if (Request::isPOST()) {
                $this->errors->reloadPage();
                $this->ajaxResponseForm();
            }
            return $this->showForbidden('', _t('qa', 'Вы не являетесь владельцем данного вопроса'));
        }

        if (Request::isPOST()) {
            $aData = $this->validateQuestionData($nQuestionID, true);
            $aResponse = array();

            do {
                if (!$this->errors->no()) break;
                if (!$this->security->validateToken()) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($aQuestionData['status'] == self::STATUS_BLOCKED) {
                    # вопрос заблокирован, помечаем на проверку модератору
                    $aData['moderated'] = 0;
                }
                # помечаем на модерацию при изменении: заголовка, описания
                if ($aData['title'] != $aQuestionData['title'] || $aData['description'] != $aQuestionData['description']) {
                    if ($aQuestionData['moderated']!=0 && ! User::isTrusted($this->module_name)) {
                        $aData['moderated'] = 2;
                    }
                }

                $this->model->questionSave($nQuestionID, $aData);

                # обновляем счетчик вопросов "на модерации"
                $this->moderationCounterUpdate();

                $aResponse['redirect'] = static::url('view', array('id' => $nQuestionID, 'keyword' => $aData['keyword']));
            } while(false);
            $this->ajaxResponseForm($aResponse);
        }

        return $this->form($nQuestionID, $aQuestionData);
    }

    protected function form($nQuestionID, array &$aData)
    {
        $aData['id'] = $nQuestionID;

        if ($nQuestionID) {
            $aSpec = Specializations::model()->specializationData($aData['spec_id']);
            $aData['spec'] = array('cat_id' => $aData['cat_id'], 'spec_id' => $aData['spec_id'], 'spec_title' => $aSpec['title']);
            if (Specializations::catsOn()) {
                $aCat = Specializations::model()->categoryData($aData['cat_id']);
                $aData['spec']['cat_title'] = $aCat['title'];
            }
        } else {
            $aData['spec'] = array();
        }

        $aData['edit'] = ! empty($nQuestionID);
        return $this->viewPHP($aData, 'form');
    }

    /**
     * Вывод сообщения о статусе вопроса
     */
    public function status()
    {
        $this->checkEnabled();
        if (Request::isPOST()) {
            Users::i()->register($this->userRegisterEmbedded);
            exit;
        }

        bff::setMeta(_t('qa', 'Публикация вопроса'));
        $nQuestionID = $this->input->get('id', TYPE_UINT);
        if ( ! $nQuestionID) {
            $this->errors->error404();
        }
        $aData = $this->model->questionData($nQuestionID, array('id','keyword','moderated','status'));
        if (empty($aData)) {
            $this->errors->error404();
        }
        if ($aData['status'] == static::STATUS_NOTACTIVATED) {
            if ( ! User::id()) {
                $regData = $this->security->getSESSION('users-register-data');
                if ( ! empty($regData)) {
                    $aData['confirm'] = Users::i()->register($this->userRegisterEmbedded + array('step' => 'confirm'));
                }
            }
        }
        if ( ! User::id() && empty($aData['confirm'])) {
            return $this->showForbiddenGuests();
        }

        if ($aData['moderated']) {
            $this->redirect(static::url('view', array('id' => $nQuestionID, 'keyword' => $aData['keyword'])));
        }

        return $this->viewPHP($aData, 'status');
    }

    /**
     * Просмотр вопроса
     */
    public function view()
    {
        $this->checkEnabled();

        $userID = User::id();
        $nQuestionID = $this->input->get('id', TYPE_UINT);
        if ( ! $nQuestionID) {
            $this->errors->error404();
        }
        $aData = $this->model->questionDataView($nQuestionID);
        if (empty($aData)) {
            $this->errors->error404();
        }

        if ( ! User::isCurrent($aData['user_id'])) {
            if ( ! in_array($aData['status'], array(static::STATUS_OPENED, static::STATUS_CLOSED))){
                $this->errors->error404();
            }
            if (static::premoderation() && ! $aData['moderated']){
                $this->errors->error404();
            }
        }

        # Хлебные крошки
        $urlList = static::url('list');
        $crumbs = array(
            array('title' => _t('qa', 'Ответы'), 'link' => $urlList),
        );
        if (Specializations::catsOn()) {
            $urlList .= $aData['cat_keyword'].'/';
            $crumbs[] = array('title' => $aData['cat_title'], 'link' => $urlList);
        }
        $urlList .= $aData['spec_keyword'].'/';
        $crumbs[] = array('title' => $aData['spec_title'], 'link' => $urlList);
        $crumbs[] = array('title' => $aData['title'], 'active' => true);
        $aData['breadcrumbs'] = &$crumbs;

        # Ответы
        $aData['answers'] = $this->view_answers($nQuestionID, User::isCurrent($aData['user_id']), $aData['status']);

        # Счетчик просмотров
        if ($userID != $aData['user_id'] && ! Request::isRefresh()) {
            $this->model->questionSave($nQuestionID, array('views_total = views_total + 1'));
        }

        # SEO: Просмотр вопроса
        $seoURL = static::url('view', array('id'=>$nQuestionID, 'keyword'=>$aData['keyword']), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL);
        $this->setMeta('view', array(
            'title'      => tpl::truncate($aData['title'], config::sysAdmin('qa.view.meta.title.truncate', 50, TYPE_UINT), ''),
            'title.full' => $aData['title'],
            'description'=> tpl::truncate(trim(strip_tags($aData['description']), '.,'), config::sysAdmin('qa.view.meta.description.truncate', 150, TYPE_UINT)),
        ), $aData);
        # SEO: Open Graph
        $this->seo()->setSocialMetaOG($aData['share_title'], $aData['share_description'], array(), $seoURL, $aData['share_sitename']);

        return $this->viewPHP($aData, 'view');
    }

    protected function view_answers($nQuestionID, $bQuestionOwner = false, $nQuestionStatus = 0)
    {
        $aData = array('id' => $nQuestionID);
        $aData['bQuestionOwner'] = $bQuestionOwner;
        $aData['nQuestionStatus'] = $nQuestionStatus;
        $aData['answers'] = $this->model->answersList($aData['id']);
        $aData['comments'] = $this->answerComments()->commentsGroup($nQuestionID);

        return $this->viewPHP($aData, 'view.answers');
    }

    /**
     * Кабинет: Вопросы
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    public function my_qa(array $userData)
    {
        return $this->owner_listing($userData['id'], $userData);
    }

    /**
     * Профиль: Вопросы
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    public function user_qa(array $userData = array())
    {
        if (empty($userData['id'])) {
            $this->errors->error404();
        }

        return $this->owner_listing($userData['id'], $userData);
    }

    /**
     * Список вопросов пользователя
     * @param integer $nUserID ID пользователя
     * @param array $aData данные о пользователе
     * @return string HTML
     */
    protected function owner_listing($nUserID = 0, array $aData)
    {
        if( ! $nUserID) $this->errors->error404();

        $this->checkEnabled();

        $nPageSize = config::sysAdmin('qa.profile.pagesize', 10, TYPE_UINT);

        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT, # № страницы
            'qa'   => TYPE_UINT, # 0 - все, 1 - вопросы, 2 - ответы
            'st'   => TYPE_UINT, # 0 - все, 1 - решенные, 2 - не решенные
        ));
        $aData['f'] = &$f;

        $seoFilter = 0;
        $aFilter = array();

        switch ($f['qa']) {
            case 1: $aFilter['user_id'] = $nUserID; break; # вопросы
            case 2: $aFilter['answer']  = $nUserID; break; # ответы
        }
        if ($f['qa']) $seoFilter++;

        switch ($f['st']) {
            case 1: $aFilter['solved'] = 1; break; # решенные
            case 2: $aFilter['solved'] = 0; break; # не решенные
        }
        if ($f['st']) $seoFilter++;

        $aData['pgn'] = '';
        $aData['count'] = $this->model->questionOwnerList($nUserID, $aFilter, true);
        if ($aData['count']) {
            $pgn = new Pagination($aData['count'], $nPageSize, array(
                'link'  => Qa::url('user.list', array('login' => $aData['login'])),
                'query' => $f,
            ));
            $aData['list'] = $this->model->questionOwnerList($nUserID, $aFilter, false, $pgn->getLimitOffset());
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }

        $aData['list'] = $this->viewPHP($aData, 'owner.list.ajax');
        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'    => $aData['pgn'],
                'list'   => $aData['list'],
            ));
        }

        $aData['qa'] = array(
            0 => array( 'id' => 0, 't' => _t('qa', 'Все')),
            1 => array( 'id' => 1, 't' => $aData['my'] ? _t('qa', 'Мои вопросы') : _t('qa', 'Вопросы')),
            2 => array( 'id' => 2, 't' => $aData['my'] ? _t('qa', 'Мои ответы') : _t('qa', 'Ответы')),
        );

        $aData['st'] = array(
            0 => array( 'id' => 0, 't' => _t('qa', 'Любой статус')),
            1 => array( 'id' => 1, 't' => _t('qa', 'Решенные')),
            2 => array( 'id' => 2, 't' => _t('qa', 'Не решенные')),
        );

        # SEO: Кабинет: Вопросы
        $this->seo()->robotsIndex( ! $seoFilter);
        $seoURL = static::url('user.list', array('login'=>$aData['login']), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL, array('page'=>$f['page']));
        $this->seo()->setPageMeta($this->users(), 'profile-qa', array(
            'name'    => $aData['name'],
            'surname' => $aData['surname'],
            'login'   => $aData['login'],
            'city'    => $aData['city'],
            'region'  => $aData['region'],
            'country' => $aData['country'],
            'page'    => $f['page'],
        ), $aData);

        return $this->viewPHP($aData, 'owner.list');
    }

    /**
     * Блок статуса вопроса
     * @param array $aData @ref данные о вопросе
     * @return string HTML
     */
    public function questionStatusBlock(array &$aData)
    {
        if (empty($aData['user_id']) ||
            ! User::isCurrent($aData['user_id'])) {
            return '';
        }

        return $this->viewPHP($aData, 'status.block');
    }

    /**
     * Дополнительные ajax запросы
     */
    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR))
        {
            case 'question-close': # закрыть вопрос
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nQuestionID = $this->input->post('id', TYPE_UINT);
                if ( ! $nQuestionID){
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->model->questionData($nQuestionID, array('user_id', 'status'));
                if (empty($aData)){
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $this->isQuestionOwner($nQuestionID, $aData['user_id'])){
                    $this->errors->impossible();
                    break;
                }

                if ($aData['status'] != static::STATUS_OPENED) {
                    $this->errors->reloadPage();
                    break;
                }
                $aUpdate = array(
                    'status'         => static::STATUS_CLOSED,
                    'status_prev'    => $aData['status'],
                    'status_changed' => $this->db->now(),
                    'expire'         => $this->db->now(),
                );

                $this->model->questionSave($nQuestionID, $aUpdate);

            } break;
            case 'question-open': # открыть вопрос
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nQuestionID = $this->input->post('id', TYPE_UINT);
                if( ! $nQuestionID){
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->model->questionData($nQuestionID, array('user_id', 'status', 'term'));
                if(empty($aData)){
                    $this->errors->reloadPage();
                    break;
                }

                if( ! $this->isQuestionOwner($nQuestionID, $aData['user_id'])){
                    $this->errors->impossible();
                    break;
                }

                if($aData['status'] != static::STATUS_CLOSED){
                    $this->errors->reloadPage();
                    break;
                }
                $aUpdate = array(
                    'status'         => static::STATUS_OPENED,
                    'status_prev'    => $aData['status'],
                    'status_changed' => $this->db->now(),
                    'solved'         => 0,
                );
                # время приема ответов
                $aTerms = static::aTerms($nDefTerm);
                if( ! array_key_exists($aData['term'], $aTerms)){
                    $aData['term'] = $nDefTerm;
                }
                $aUpdate['expire'] = date('Y-m-d H:i:s', time() + $aTerms[ $aData['term'] ]['days'] * 24*60*60);

                $this->model->questionSave($nQuestionID, $aUpdate);

            } break;
            case 'question-solved': # вопрос решен
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nQuestionID = $this->input->post('id', TYPE_UINT);
                if( ! $nQuestionID){
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->model->questionData($nQuestionID, array('user_id', 'status', 'term'));
                if(empty($aData)){
                    $this->errors->reloadPage();
                    break;
                }

                if( ! $this->isQuestionOwner($nQuestionID, $aData['user_id'])){
                    $this->errors->reloadPage();
                    break;
                }

                if( ! in_array($aData['status'], array(static::STATUS_OPENED, static::STATUS_CLOSED))){
                    $this->errors->reloadPage();
                    break;
                }
                $aUpdate = array(
                    'status'         => static::STATUS_CLOSED,
                    'status_prev'    => $aData['status'],
                    'status_changed' => $this->db->now(),
                    'solved'         => 1,
                    'expire'         => $this->db->now(),
                );

                $this->model->questionSave($nQuestionID, $aUpdate);

            } break;
            case 'question-delete': # удалить вопрос
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nQuestionID = $this->input->post('id', TYPE_UINT);
                if( ! $nQuestionID){
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->model->questionData($nQuestionID, array('user_id', 'status'));
                if(empty($aData)){
                    $this->errors->reloadPage();
                    break;
                }

                if( ! $this->isQuestionOwner($nQuestionID, $aData['user_id'])){
                    $this->errors->reloadPage();
                    break;
                }

                if( ! in_array($aData['status'], array(static::STATUS_OPENED, static::STATUS_CLOSED, static::STATUS_BLOCKED))){
                    $this->errors->reloadPage();
                    break;
                }
                $aUpdate = array(
                    'status'         => static::STATUS_DELETED,
                    'status_prev'    => $aData['status'],
                    'status_changed' => $this->db->now(),
                );

                $aResponse['redirect'] = static::url('my.list');

                $this->model->ratingUpdate('dec', $nQuestionID); # обновим рейтинг

                $this->model->questionSave($nQuestionID, $aUpdate);

                # обновляем счетчик вопросов "на модерации"
                $this->moderationCounterUpdate();

            } break;
            case 'answer-add': # добавление ответа
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                $aData = $this->validateAnswerData(0, true);
                $aData['user_id'] = User::id();
                if ($this->errors->no())
                {
                    if ( ! $this->isAllowAddAnswer($aData['question_id'], $aData['user_id'])) {
                        $this->errors->impossible();
                        break;
                    }

                    $nAnswerID = $this->model->answerSave(0, $aData);
                    if ($nAnswerID) {
                        $aQuestionData = $this->model->questionData($aData['question_id'], array('id','user_id','status', 'keyword','title'));
                        $aResponse['answers'] = $this->view_answers($aData['question_id'], User::isCurrent($aQuestionData['user_id']), $aQuestionData['status']);
                        # обновляем счетчик ответов "на модерации"
                        $this->moderationAnswersCounterUpdate(1);

                        # Отправим письмо пользователю автору вопроса
                        Users::sendMailTemplateToUser($aQuestionData['user_id'], 'qa_answer_new', array(
                            'question_id'    => $aQuestionData['id'],
                            'question_title' => $aQuestionData['title'],
                            'question_url'   => static::url('view', array('id'=>$aQuestionData['id'], 'keyword'=>$aQuestionData['keyword'])),
                        ), Users::ENOTIFY_GENERAL);
                    }
                }
            } break;
            case 'answer-vote': # голосование за ответ
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nUserID = User::id();
                $p = $this->input->postm(array(
                    'id' => TYPE_UINT,  # ID ответа
                    't'  => TYPE_STR,   # Тип голоса
                ));
                switch ($p['t']) {
                    case 'good': $vote = 1; break;
                    case 'bad':  $vote = -1; break;
                    default:     $vote = 0; break;
                }
                if( ! $vote || ! $p['id']){
                    $this->errors->reloadPage();
                    break;
                }

                $nAnswerID = $p['id'];
                $nAnswerData = $this->model->answerData($nAnswerID, array('user_id'));
                if($nAnswerData['user_id'] == $nUserID){
                    $aResponse['disabled'] = 1;
                    break;
                }
                # проверка, не голосовал ли пользователь за данный ответ
                $nSum = $this->model->votesSum(array('user_id'=>$nUserID, 'answer_id'=>$nAnswerID));
                if ($nSum) {
                    $aResponse['disabled'] = 1;
                    break;
                }

                $nVotesSum = $this->model->voteSave(array(
                    'answer_id' => $nAnswerID,
                    'user_id' => $nUserID,
                    'vote' => $vote,
                ));
                if($nVotesSum !== false){
                    $aResponse['sum'] = $nVotesSum;
                    $aResponse['disabled'] = 1;
                }
            } break;
            case 'answer-best': # лучший ответ
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nQuestionID = $this->input->post('question_id', TYPE_UINT);
                if( ! $nQuestionID){
                    $this->errors->reloadPage();
                    break;
                }

                $nAnswerID = $this->input->post('answer_id', TYPE_UINT);
                if( ! $nAnswerID){
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->model->questionData($nQuestionID, array('user_id', 'status', 'term'));
                if(empty($aData)){
                    $this->errors->reloadPage();
                    break;
                }

                if( ! $this->isQuestionOwner($nQuestionID, $aData['user_id'])){
                    $this->errors->impossible();
                    break;
                }

                if( ! in_array($aData['status'], array(static::STATUS_OPENED, static::STATUS_CLOSED))){
                    $this->errors->reloadPage();
                    break;
                }

                $aAnswerData = $this->model->answerData($nAnswerID, array('question_id'));
                if(empty($aAnswerData)){
                    $this->errors->reloadPage();
                    break;
                }

                if($aAnswerData['question_id'] != $nQuestionID){
                    $this->errors->reloadPage();
                    break;
                }

                $this->model->ratingUpdate('dec', $nQuestionID); # обновим рейтинг

                $this->model->answerBest($nQuestionID, $nAnswerID);

                $this->model->ratingUpdate('inc', $nQuestionID); # обновим рейтинг

            } break;
            case 'answer-delete': # удаление ответа владельцем вопроса
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nQuestionID = $this->input->post('question_id', TYPE_UINT);
                if( ! $nQuestionID){
                    $this->errors->reloadPage();
                    break;
                }

                $nAnswerID = $this->input->post('answer_id', TYPE_UINT);
                if ( ! $nAnswerID) {
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->model->questionData($nQuestionID, array('user_id', 'status', 'term'));
                if (empty($aData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! $this->isQuestionOwner($nQuestionID, $aData['user_id'])) {
                    $this->errors->impossible();
                    break;
                }

                if ( ! in_array($aData['status'], array(static::STATUS_OPENED, static::STATUS_CLOSED))){
                    $this->errors->reloadPage();
                    break;
                }

                $aAnswerData = $this->model->answerData($nAnswerID, array('question_id', 'best'));
                if (empty($aAnswerData)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($aAnswerData['question_id'] != $nQuestionID) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($aAnswerData['best']) {
                    $this->errors->set(_t('qa', 'Невозможно удалить ответ, отмеченный как лучший'));
                    break;
                }

                $this->model->answerSave($nAnswerID, array('status' => static::STATUS_ANS_DEL_OWNER_QUEST));
                # обновляем счетчик ответов "на модерации"
                $this->moderationAnswersCounterUpdate();
            } break;
            case 'answer-delete-owner': # удаление ответа владельцем ответа
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nAnswerID = $this->input->post('id', TYPE_UINT);
                if (!$nAnswerID) {
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->model->answerData($nAnswerID, array('user_id','best'));
                if (empty($aData) || ! User::isCurrent($aData['user_id'])) {
                    $this->errors->impossible();
                    break;
                }
                if ($aData['best']) {
                    $this->errors->set(_t('qa', 'Невозможно удалить ответ, отмеченный как лучший'));
                    break;
                }

                $this->model->answerSave($nAnswerID, array('status' => static::STATUS_ANS_DEL_OWNER_ANS));
                # обновляем счетчик ответов "на модерации"
                $this->moderationAnswersCounterUpdate();

            } break;
            case 'comment-add': # добавление комментария
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                $sMessage = $this->input->post('message', TYPE_TEXT, array('len' => 2500, 'len.sys' => 'qa.comment.message.limit'));
                $min = config::sys('blog.comment.message.min', 10, TYPE_UINT);
                if (mb_strlen($sMessage) < $min) {
                    $this->errors->set(_t('', 'Комментарий не может быть короче [min] символов', array('min' => $min)), 'message');
                    break;
                }

                # антиспам фильтр
                if(Site::i()->spamFilter(array(
                    array('text' => & $sMessage,   'error'=>_t('', 'В указанном вами комментарии присутствует запрещенное слово "[word]"')),
                ))){
                    break;
                }

                $nAnswerID = $this->input->post('id', TYPE_UINT);
                if ( ! $nAnswerID) {
                    $this->errors->reloadPage(); break;
                }

                $nAnswerData = $this->model->answerData($nAnswerID, array('question_id', 'status'));
                if (empty($nAnswerData)){
                    $this->errors->reloadPage(); break;
                }

                if ($nAnswerData['status'] != static::STATUS_ANS_DEFAULT){
                    $this->errors->impossible(); break;
                }

                # не чаще чем раз в {X} секунд с одного IP (для одного пользователя)
                if (Site::i()->preventSpam('qa-comment', config::sysAdmin('qa.prevent.spam', 20, TYPE_UINT))) {
                    break;
                }

                $aData = array(
                    'message' => $sMessage,
                    'name' => User::data('name'),
                );

                $oComm = $this->answerComments();
                $oComm->setGroupID($nAnswerData['question_id']);
                $bSuccess = $oComm->commentInsert($nAnswerID, $aData);
                if ($bSuccess !== false) {
                    # оставили комментарий
                    $aCommentsData = array(
                        'created' => $this->db->now(),
                        'message' => $aData['message'],
                        'name'    => User::data('name'),
                        'surname' => User::data('surname'),
                        'pro'     => User::data('pro'),
                        'login'   => User::data('login'),
                        'last_activity' => User::data('last_activity'),
                    );
                    $aResponse['html'] = $this->viewPHP($aCommentsData, 'view.answers.comment.ajax');
                }

            } break;

            default:
                $this->errors->impossible();
        }
        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Проверка, включен ли раздел "Ответы"
     */
    public function checkEnabled()
    {
        if (!static::enabled()) {
            if (Request::isPOST()) {
                $this->errors->reloadPage();
            } else {
                $this->errors->error404();
            }
        }
    }

    /**
     * Крон задача, для закрытия вопросов
     * Рекомендуемый период: "раз в час"
     */
    public function cron()
    {
        if (!bff::cron()) {
            return;
        }

        $this->model->questionsCronStatus();
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {

        return array(
            'cron' => array('period' => '0 * * * *'),
        );
    }

    /**
     * Данные для карты сайта
     * @return array
     */
    public function getSitemapData()
    {
        $aSpecs = Specializations::model()->specializationsInAllCategories(
            array('id', 'keyword', 'title'), array('id', 'keyword', 'title'), true
        );
        $url = static::url('list');
        if ($aSpecs) {
            # формируем URL + помечаем активную категорию / специализацию
            foreach ($aSpecs as &$v) {
                $v['url'] = $url.$v['keyword'].'/';
                if ( ! empty($v['specs'])) {
                    foreach ($v['specs'] as &$vv) {
                        $vv['url'] = $v['url'].$vv['keyword'].'/';
                    } unset($vv);
                }
            } unset($v);
        }

        $aSpecs = array_map(function($i) {
            if(isset($i['specs'])) {
                $i['sub'] = $i['specs'];
            }
            unset($i['specs']);
            return $i;
        }, $aSpecs);

        return array(
            'cats' => $aSpecs,
            'link' => $url,
        );
    }

}