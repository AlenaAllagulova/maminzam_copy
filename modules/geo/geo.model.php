<?php

class GeoModel_ extends GeoModelBase
{
    /**
     * Формируем список станций метро для автокомплитера
     * @param array $aFilter фильтр
     * @param int $nLimit
     * @param string $sOrderBy
     * @return mixed
     */
    public function metroAcList(array $aFilter, $nLimit = 0, $sOrderBy = '')
    {
        $aFilter = $this->prepareFilter($aFilter, 'M');

        return $this->db->select_key('SELECT M.*, M.title_' . LNG . ' as title
              FROM ' . TABLE_REGIONS_METRO . ' M
              ' . $aFilter['where'] . '
              ORDER BY ' . (!empty($sOrderBy) ? $sOrderBy : 'R.num') . '
              ' . (!empty($nLimit) ? $this->db->prepareLimit(0, $nLimit) : ''), 'id', $aFilter['bind']
        );
    }

    # ---------------------------------------------------------------------------------
    # Районы

    /**
     * Формируем список районов города - frontend
     * @param int $nCityID ID города
     * @param array $aFilter фильтр районов
     * @param array $aBind
     * @param string $sLang ключ локализации
     * @return mixed
     */
    public function districtsList($nCityID, array $aFilter = array(), array $aBind = array(), $sLang = LNG)
    {
        if ($nCityID && ! isset($aFilter['city_id'])) {
            $aFilter['city_id'] = $nCityID;
        }
        $aFilter = $this->prepareFilter($aFilter, false, $aBind);

        return $this->db->select_key('SELECT id,
                                          title_' . $sLang . ' as t,
                                          title_' . $sLang . ' as title,
                                          ybounds,
                                          ypoly,
                                          city_id
                                      FROM ' . TABLE_REGIONS_DISTRICTS . '
                                      ' . $aFilter['where'] . '
                                      ORDER BY 2', 'id', $aFilter['bind']
        );
    }

}