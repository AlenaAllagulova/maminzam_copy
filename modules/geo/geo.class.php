<?php

class Geo_ extends GeoBase
{

    /**
     * Инициализируем фильтр по региону, общий в шапке
     * @param mixed $key ключ требуемых данных о текущих настройках фильтра по региону или FALSE (все данные)
     * @return mixed
     */
    public static function filter($key = false)
    {
        static $inited = false, $current = array(), $country = array(), $region = array(), $city = array();

        if (!$inited)
        {
            $inited = true;
            $user = static::filterUser();
            if ($user) {
                $user = static::regionData($user);
                switch ($user['numlevel']) {
                    case static::lvlCountry:
                        $country = $user;
                        break;
                    case static::lvlRegion:
                        $region = $user;
                        $country = static::regionData($region['country']);
                        break;
                    case static::lvlCity:
                        $city = $user;
                        $region = static::regionData($city['pid']);
                        $country = static::regionData($city['country']);
                        break;
                }
            }

            $current = ($city ? $city : ($region ? $region : ($country ? $country : array('id' => 0))));
        }

        switch ($key)
        {
            case 'id': # id текущего: города | региона | страны | 0
            {
                return (!empty($current['id']) ? $current['id'] : 0);
            } break;
            case 'id-city': # id текущего: города | 0
            {
                return (!empty($city['id']) ? $city['id'] : 0);
            } break;
            case 'id-region': # id текущего: региона | 0
            {
                return (!empty($region['id']) ? $region['id'] : 0);
            } break;
            case 'id-country': # id текущей: страны | 0
            {
                return (!empty($country['id']) ? $country['id'] : 0);
            } break;
            case 'all':
            {
                return array(
                    'country' => (!empty($country['id']) ? $country['id'] : 0),
                    'region' => (!empty($region['id']) ? $region['id'] : 0),
                    'city' => (!empty($city['id']) ? $city['id'] : 0),
                    );
            } break;
            case 'url': # данные о городе и регионе
            {
                return array(
                    'city'   => ($city ? $city['keyword'] : ''),
                    'region' => ($region ? $region['keyword'] : ''),
                    'country' => ($country ? $country['keyword'] : ''),
                );
            } break;
            default:
                return $current;
        }
    }

    /**
     * Получаем / устанавливаем ID страны/города/региона пользователя
     * @param integer|boolean $nRegionID устанавливаем ID города/региона или FALSE (получаем текущий)
     * @return integer
     */
    public static function filterUser($nRegionID = false)
    {
        if (bff::$isBot) {
            if ($nRegionID !== false) {
                return $nRegionID;
            } else {
                return 0;
            }
        }

        $cookieKey = bff::cookiePrefix() . 'geo';
        $cookieExpire = 100; # кол-во дней

        if ($nRegionID !== false) {
            # обновляем куки
            if (!isset($_COOKIE[$cookieKey]) || $_COOKIE[$cookieKey] != $nRegionID) {
                Request::setCOOKIE($cookieKey, $nRegionID, $cookieExpire);
                $_COOKIE[$cookieKey] = $nRegionID;
            }
        } else {
            # получим регион из url
            $regFromUrl = Geo::filterUrl('id');
            if ($regFromUrl) {
                $nRegionID = $regFromUrl;
                $_COOKIE[$cookieKey] = $nRegionID;
                Request::setCOOKIE($cookieKey, $nRegionID, $cookieExpire);
            } else {
                if (!isset($_COOKIE[$cookieKey])) {
                    # определяем ID город по IP адресу пользователя
                    if(Geo::defaultCity()){
                        $nRegionID = Geo::defaultCity();
                    }else{
                        if (config::sys('geo.ip.location', false, TYPE_BOOL)) {
                            $aData = static::model()->regionDataByIp();
                            if (empty($aData)) {
                                $aData = array('id' => 0);
                            } else {
                                if (!static::regionCorrect($aData)) {
                                    $aData['id'] = 0;
                                }
                            }

                            $nRegionID = $aData['id'];
                            if ($nRegionID > 0) {
                                # кешируем
                                static::regionData($aData['id'], $aData);
                                static::regionDataByKeyword($aData['keyword'], $aData);
                            }
                        } else {
                            $nRegionID = 0;
                        }
                        if ($nRegionID) {
                            config::set('geo.ip.location.detect', $nRegionID);
                        }
                        $nRegionID = 0;
                    }

                    $_COOKIE[$cookieKey] = $nRegionID;
                    Request::setCOOKIE($cookieKey, $nRegionID, $cookieExpire);
                } else {
                    # получаем из куков
                    $nRegionID = bff::input()->cookie($cookieKey, TYPE_UINT);
                }
            }
        }

        return $nRegionID;
    }

    /**
     * Проверяем корректность региона
     * @param $data array данные региона
     * @return bool
     */
    public static function regionCorrect($data)
    {
        if (empty($data)) return false;
        if (static::countrySelect()) {
            $countries = static::countryList();
            if ($data['numlevel'] == static::lvlCountry) {
                if (array_key_exists($data['id'], $countries))
                    return true;
            }
            if (array_key_exists($data['country'], $countries))
                return true;
        } else {
            if ($data['numlevel'] == static::lvlCountry)
                return false;
            if ($data['country'] == static::defaultCountry())
                return true;
        }
        return false;
    }

    /**
     * Форма для выбора региона в шапке
     * @return string HTML
     */
    public function filterForm()
    {
        $countrySelect = Geo::countrySelect();
        $data = Geo::filter();
        $data['countrySelect'] = $countrySelect;
        $data['regionBlock'] = '';
        $data['cityBlock'] = '';
        $data['countryID'] = 0;
        $data['regionID'] = 0;
        $data['cityID'] = 0;
        $data['cityTitle'] = '';
        $data['cityNoRegion'] = 0;
        if ($data['id']) {
            switch ($data['numlevel'])
            {
                case static::lvlCountry:
                {
                    $data['countryID'] = $data['id'];
                    if ( ! empty($data['filter_noregions'])) {
                        $data['cityNoRegion'] = 1;
                        $data['regionBlock'] = $this->filterData('city-noregions', $data['country']);
                    } else {
                        $data['regionBlock'] = $this->filterData('region', $data['id']);
                    }
                } break;
                case static::lvlRegion:
                {
                    $data['regionBlock'] = $this->filterData('region', $data['country']);
                    $data['cityBlock'] = $this->filterData('city', $data['id']);
                    $data['regionTitle'] = $data['title'];
                    $data['regionID'] = $data['id'];
                    $data['countryID'] = $countrySelect ? $data['country'] : 0;
                } break;
                case static::lvlCity:
                {
                    $data['countryID'] = $data['country'];
                    $country = static::regionData($data['country']);
                    if ( ! empty($country['filter_noregions'])) {
                        $data['cityNoRegion'] = 1;
                        $data['regionBlock'] = $this->filterData('city-noregions', $data['country']);
                        $data['cityID'] = $data['id'];
                        $data['cityTitle'] = $data['title'];
                    } else {
                        if(Geo::defaultCity()){
                            $data['districtsBlock'] = $this->filterData('districts', $data['id']);
                        }else{
                            $data['regionBlock'] = $this->filterData('region', $data['country']);
                            $data['cityBlock'] = $this->filterData('city', $data['pid']);
                            $parent = static::regionData($data['pid']);
                            $data['regionTitle'] = $parent['title'];
                            $data['regionID'] = $parent['id'];
                        }
                    }
                } break;
            }
        } else {
            $data['numlevel'] = 0;
            if (!$countrySelect) {
                $data['numlevel'] = static::lvlCountry;
                $data['countryID'] = 0;
                $data['regionBlock'] = $this->filterData('region', Geo::defaultCountry());
            }
        }

        return $this->viewPHP($data, 'filter.form');
    }

    /**
     * Данные для формы выбора региона в шапке
     * @param $type string тип
     * @param $id integer ID региона
     * @return string HTML
     */
    protected function filterData($type, $id)
    {
        switch ($type)
        {
            case 'region':
            {
                $data = static::regionList($id ? $id : Geo::defaultCountry());
                $result = array();
                foreach ($data as $v) {
                    $letter = mb_substr($v['title'], 0, 1);
                    $result[$letter][] = $v;
                }
                $cols = 3;
                $inCol = ceil(count($data) / $cols);
                $data = array('regions' => $result, 'cols' => $cols, 'inCol' => $inCol, 'id' => $id);
                return $this->viewPHP($data, 'filter.form.regions');
            } break;
            case 'city':
            {
                $data = self::cityList($id ? $id : Geo::defaultCountry());
                $result = array();
                foreach ($data as $v) {
                    $letter = mb_substr($v['title'], 0, 1);
                    $result[$letter][] = $v;
                }
                $cols = 4;
                if (count($data) <= 20 && count($data) > 8) $cols = 3;
                $inCol = ceil(count($data) / $cols);
                $data = array('regions' => $result, 'cols' => $cols, 'inCol' => $inCol, 'id' => $id);
                return $this->viewPHP($data, 'filter.form.regions');
            } break;
            case 'city-noregions':
            {
                $data = static::regionPreSuggest($id ? $id : Geo::defaultCountry(), true, 50);
                $result = array();
                foreach ($data as $v) {
                    $v['main'] = 0;
                    $result[' '][] = $v;
                }
                $cols = 4;
                if (count($data) <= 20 && count($data) > 8) $cols = 3;
                $inCol = ceil(count($data) / $cols);
                $data = array('regions' => $result, 'cols' => $cols, 'inCol' => $inCol, 'id' => $id);
                return $this->viewPHP($data, 'filter.form.regions');
            } break;
            case 'districts':
            {
                $data = static::districtList($id ? $id : Geo::defaultCity());
                $result = array();
                foreach ($data as $v) {
                    $letter = mb_substr($v['title'], 0, 1);
                    $result[$letter][] = $v;
                }
                $cols = 3;
                $inCol = ceil(count($data) / $cols);
                $data = array('regions' => $result, 'cols' => $cols, 'inCol' => $inCol, 'id' => $id);
                return $this->viewPHP($data, 'filter.form.districts');
            } break;
        }
        return '';
    }

    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR)) {
            /**
             * Autocomplete для городов/областей
             */
            case 'region-suggest':
            {
                $this->regionSuggest(false);
            }
            break;
            case 'country-presuggest':
            {
                $nCountryID = $this->input->postget('country', TYPE_UINT);
                $mResult = false;
                if ($nCountryID) {
                    $aData = static::regionPreSuggest($nCountryID, true);
                    $mResult = array();
                    foreach ($aData as $v) {
                        $mResult[] = array($v['id'], $v['title'], $v['metro'], $v['pid']);
                    }
                }
                $this->ajaxResponse($mResult);
            }
            break;
            /**
             * Список районов города
             * @param int $nCityID ID города
             * @param bool $bOptions true - в формате select::options, false - array
             */
            case 'districts-list':
            {
                $nCityID = $this->input->postget('city', TYPE_UINT);
                $bOptions = $this->input->postget('opts', TYPE_BOOL);
                if (!$nCityID) {
                    $this->errors->impossible();
                    break;
                }
                if ($bOptions) {
                    $aResponse['districts'] = static::districtOptions($nCityID, 0, _t('geo', 'Все районы'));
                } else {
                    $aResponse['districts'] = static::districtList($nCityID);
                }
            }
            break;
            /**
             * Список станций метро города для формы ОБ
             * @param int $nCityID ID города
             */
            case 'form-metro':
            {
                $nCityID = $this->input->postget('city', TYPE_UINT);
                $aData = static::cityMetro($nCityID, 0, false);
                $aData['none'] = $this->input->postget('none', TYPE_BOOL);
                $aResponse['data'] = $aData['data'];
                $aResponse['branches'] = $this->viewPHP($aData, 'form.metro.step1');
                $aResponse['stations'] = array();
                foreach ($aData['data'] as $k => $v) {
                    $v['city_id'] = $nCityID;
                    $aResponse['stations'][$k] = $this->viewPHP($v, 'form.metro.step2');
                }
            }
            break;
            case 'has-metro':
            {
                $nCityID = $this->input->postget('city', TYPE_UINT);
                $aResponse['hasMetro'] = $this->hasMetro($nCityID);

            } break;
            case 'metro-suggest':
            {
                $this->metroSuggest(false);
            } break;
            case 'filter-region':
            {
                $id = $this->input->post('id', TYPE_UINT);
                $aResponse['html'] = $this->filterData('region', $id);
            } break;
            case 'filter-city':
            {
                $id = $this->input->post('id', TYPE_UINT);
                $aResponse['html'] = $this->filterData('city', $id);
            } break;
            case 'filter-city-noregions':
            {
                $id = $this->input->post('id', TYPE_UINT);
                $aResponse['html'] = $this->filterData('city-noregions', $id);
            } break;
            case 'filter-location':
            {
                $loc = $this->input->post('loc', TYPE_STR);
                $type = static::urlType();
                $pos = strpos($loc, SITEHOST);
                if ( ! $pos) break;
                $loc = substr($loc, $pos + strlen(SITEHOST));
                if ($type == self::URL_SUBDIR) {
                    Geo::filterUrlExtract($loc, $keyword);
                    if ($keyword) {
                        $keyword .= '/';
                        $loc = str_replace($keyword, '', $loc);
                    }

                }
                $lng = bff::locale()->getLanguageUrlPrefix();
                if ($lng) {
                    $loc = str_replace($lng, '', $loc);
                }

                $id = $this->input->post('id', TYPE_UINT);
                $region = false;
                if ($id) {
                    $data = static::regionData($id);
                    if (empty($data)) {
                        break;
                    }
                    $region = array();
                    switch ($data['numlevel']) {
                        case static::lvlCity:    $region['city'] = $data['keyword'];  break;
                        case static::lvlRegion:  $region['region'] = $data['keyword'];  break;
                        case static::lvlCountry: $region['country'] = $data['keyword'];  break;
                    }
                }
                $aResponse['location'] = Geo::url($region, false, false).$loc;
            } break;
            default:
                $this->errors->impossible();
        }
        $this->ajaxResponseForm($aResponse);
    }

}