<?php
    $cnt = 0;
    $class = 'col-sm-'.($cols == 4 ? '3' : '4');
?>
<div class="row j-regions-block" data-id="<?= $id ?>">
    <div class="<?= $class ?>">
        <ul class="navbar-header-dropdown-countries">
        <? foreach($regions as $k => $v): ?>
            <? if($cnt >= $inCol): $cnt = 0; ?>
                    </ul>
                </div>
                <div class="<?= $class ?>">
                    <ul class="navbar-header-dropdown-countries">
            <? endif; ?>
            <li><span><?= $k ?></span>
                <ul>
                    <? foreach($v as $vv): ?>
                        <? if($cnt >= $inCol): $cnt = 0; ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="<?= $class ?>">
                            <ul class="navbar-header-dropdown-countries">
                                <li>
                                    <ul>
                        <? endif; ?>
                        <? $mail = ! empty($vv['main']); ?>
                        <li>
                            <?= $mail ? '<strong>' : '' ?>
                            <a href="<?=  User::isWorker() ?
                                          Orders::url('list', ['dt'=> $vv['id']]) :
                                          ( (!User::id() || User::isClient()) ? Users::url('list', ['dt'=> $vv['id']]):'javascript:void(0);');?>"
                               class=""
                               data-id="<?= $vv['id'] ?>">
                                <?= $vv['title'] ?>
                            </a>
                            <?= $mail ? '</strong>' : '' ?>
                        </li>
                    <? $cnt++; endforeach; ?>
                </ul>
            </li>
        <? endforeach; ?>
        </ul>
    </div>
</div>