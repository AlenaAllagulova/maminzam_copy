<?php
    $cnt = 0;
    $class = 'col-sm-'.($cols == 4 ? '3' : '4');
?>
<div class="row j-regions-block" data-id="<?= $id ?>">
    <div class="<?= $class ?>">
        <ul class="navbar-header-dropdown-countries">
        <? foreach($regions as $k => $v): ?>
            <? if($cnt >= $inCol): $cnt = 0; ?>
                    </ul>
                </div>
                <div class="<?= $class ?>">
                    <ul class="navbar-header-dropdown-countries">
            <? endif; ?>
            <li><span><?= $k ?></span>
                <ul>
                    <? foreach($v as $vv): ?>
                        <? if($cnt >= $inCol): $cnt = 0; ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="<?= $class ?>">
                            <ul class="navbar-header-dropdown-countries">
                                <li>
                                    <ul>
                        <? endif; ?>
                        <? $mail = ! empty($vv['main']); ?>
                        <li>
                            <?= $mail ? '<strong>' : '' ?>
                            <a href="#" class="j-select" data-id="<?= $vv['id'] ?>">
                                <?= $vv['title'] ?>
                            </a>
                            <?= $mail ? '</strong>' : '' ?>
                        </li>
                    <? $cnt++; endforeach; ?>
                </ul>
            </li>
        <? endforeach; ?>
        </ul>
    </div>
</div>