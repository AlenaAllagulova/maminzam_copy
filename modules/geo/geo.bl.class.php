<?php

abstract class GeoBase_ extends GeoModule
{
    /** @var GeoModel */
    public $model = null;
    /** @var array доступные для редактирования (задействованные) geo-сущности */
    public static $useRegions = array(
        self::lvlCountry,
        self::lvlRegion,
        self::lvlCity,
        self::lvlDistrict,
        self::lvlMetro
    );

    /**
     * Тип формирования URL с учетом региона / города
     */
    const URL_NONE      = 0; # регион и город не фигурируют в URL - example.com
    const URL_SUBDIR    = 1; # поддиректории для областей(регионов) или городов - example.com/city/
    const URL_SUBDOMAIN = 2; # поддомены для областей(регионов), поддомены для городов - city.region.example.com/

    public function init()
    {
        parent::init();

        $this->regionsFormExtraFields(self::lvlCountry, array(
            array('field'=>'filter_noregions', 'type'=>'checkbox', 'title'=>'Фильтр региона: пропускать шаг выбора области'),
        ));
    }

    /**
     * ID города по-умолчанию
     * @return mixed
     */
    public static function defaultCity()
    {
        return config::sys('geo.default.city', static::$defaultCity, TYPE_UINT);
    }

    /**
     * Формирование URL, с учетом фильтра по "региону"
     * @param array $opts параметры:
     *  > region - keyword/array данные региона(области)
     *  > city - keyword/array данные города
     * @param boolean $dynamic динамическая ссылка
     * @param boolean $trailingSlash завершающий слеш
     * @return string
     */
    public static function url(& $opts = array(), $dynamic = false, $trailingSlash = true)
    {
        static $urlType;
        if (!isset($urlType)) {
            $urlType = static::urlType();
        }
        $protocol = ($dynamic ? '//' : Request::scheme() . '://');
        $sitehost = ($dynamic ? '{sitehost}' : SITEHOST . bff::locale()->getLanguageUrlPrefix(LNG));
        $trailingSlash = ($trailingSlash ? '/' : '');
        $geo = false;
        # формируем URL с учетом указанного города (city), области (region)
        if (isset($opts['region']) || isset($opts['city']) || isset($opts['country'])) {
            $geo = array('city' => false, 'region' => false, 'country' => false);

            # город
            if (isset($opts['city'])) {
                $geo['city'] = (is_string($opts['city']) ? $opts['city'] : (isset($opts['city']['keyword']) ? $opts['city']['keyword'] : ''));
            }
            # область
            if (isset($opts['region'])) {
                $geo['region'] = (is_string($opts['region']) ? $opts['region'] : (isset($opts['region']['keyword']) ? $opts['region']['keyword'] : ''));
            }
            # страна
            if (isset($opts['country'])) {
                $geo['country'] = (is_string($opts['country']) ? $opts['country'] : (isset($opts['country']['keyword']) ? $opts['country']['keyword'] : ''));
            }
        }
        # формируем URL с учетом текущего фильтра по "региону"
        else
        {
            if (static::filterEnabled() && $opts !== false) {
                $geo = static::filter('url');
            }
        }
        unset($opts['country'], $opts['region'], $opts['city']);

        $url = $base = $protocol . $sitehost . $trailingSlash;
        if ( ! empty($geo)) {
            switch ($urlType) {
                case self::URL_SUBDOMAIN: # поддомены
                    $subDomain = ($geo['city'] ? $geo['city'].'.' :
                        ($geo['region'] ? $geo['region'].'.' :
                            (static::countrySelect() ? ($geo['country'] ? $geo['country'].'.' : '') : '')));
                    $url = $protocol.$subDomain.$sitehost.$trailingSlash;
                    break;
                case self::URL_SUBDIR: # поддиректории
                    $subDir = ($geo['city'] ? '/'.$geo['city'] :
                        ($geo['region'] ? '/'.$geo['region'] :
                            (static::countrySelect() ? ($geo['country'] ? '/'.$geo['country'] : '') : '')));
                    $url = $protocol.$sitehost.$subDir.$trailingSlash;
                    break;
            }
        }
        
        return bff::filter('geo.url', $url, array('opts'=>$opts, 'dynamic'=>$dynamic, 'trailingSlash'=>$trailingSlash, 'geo'=>$geo, 'base'=>$base));
    }

    /**
     * Получаем ID города/региона на основе текущего URL
     * @param mixed $key ключ требуемых данных
     * @return integer|array
     */
    public static function filterUrl($key = false)
    {
        static $inited = false, $country = 0, $region = 0, $city = 0, $id = 0, $keyword = '';
        if ( ! $inited) {
            $inited = true;
            switch (static::urlType()) {
                case self::URL_SUBDOMAIN: # поддомены
                    $host = Request::host(SITEHOST);
                    $data = static::filterUrlExtract($host, $keyword, $id, $country, $region, $city);
                    break;
                case self::URL_SUBDIR: # поддиректории
                    $uri = Request::uri();
                    $data = static::filterUrlExtract($uri, $keyword, $id, $country, $region, $city);
                    break;
                case self::URL_NONE: # регион не фигурирует в URL
                    break;
            }
            if (isset($data['enabled']) && !$data['enabled'] && Request::isGET()) {
                bff::errors()->error404();
            }
        }

        if ($key == 'id') {
            return $id;
        } else {
            if ($key == 'keyword') {
                return $keyword;
            }
        }

        return array('city' => $city, 'region' => $region, 'country' => $country, 'id' => $id);
    }

    public static function filterUrlExtract($url, & $keyword = '', & $id = 0, & $country = 0, & $region = 0, & $city = 0)
    {
        $data = array();
        switch (static::urlType()) {
            case self::URL_SUBDOMAIN: # поддомены
            {
                if (preg_match('/(.*)\.' . preg_quote(SITEHOST) . '/', $url, $matches) > 0 && !empty($matches[1])) {
                    # страна / область / город
                    $data = static::regionDataByKeyword($matches[1]);
                    if ($data) {
                        if ($data['numlevel'] == self::lvlCity) {
                            $city = $data;
                        } else {
                            if ($data['numlevel'] == self::lvlRegion) {
                                $region = $data;
                            } else {
                                if ($data['numlevel'] == self::lvlCountry) {
                                    $country = $data;
                                } else {
                                    if (Request::isGET()) {
                                        bff::errors()->error404();
                                    }
                                }
                            }
                        }
                    }
                }
            } break;
            case self::URL_SUBDIR: # поддиректории
            {
                $lng = bff::locale()->getLanguageUrlPrefix();
                if (preg_match('/^\/' . ($lng ? preg_quote(trim($lng, '/')) . '\/' : '') . '([^\/]+)\/(.*)/', $url, $matches) > 0 && !empty($matches[1])) {
                    $data = static::regionDataByKeyword($matches[1]);
                    if ($data) {
                        if ($data['numlevel'] == self::lvlCity) {
                            $city = $data;
                        } else {
                            if ($data['numlevel'] == self::lvlRegion) {
                                $region = $data;
                            } else {
                                if ($data['numlevel'] == self::lvlCountry) {
                                    $country = $data;
                                } else {
                                    if (Request::isGET()) {
                                        bff::errors()->error404();
                                    }
                                }
                            }
                        }
                    }
                }
            }  break;
            case self::URL_NONE: # регион не фигурирует в URL
            {
                # не анализируем URL
            } break;
        }

        list($id, $keyword) = ($city ? array($city['id'], $city['keyword']) :
            ($region ? array($region['id'], $region['keyword']) :
                ($country ? array($country['id'], $country['keyword']) :
                    array(0, ''))));

        return $data;
    }

    /**
     * Текущий тип формирования URL с учетом региона и города
     * @return mixed
     */
    public static function urlType()
    {
        return config::sysAdmin('geo.filter.url', self::URL_NONE, TYPE_UINT);
    }

    /**
     * Включен ли выбор региона в шапке
     * @return bool true - включен
     */
    public static function filterEnabled()
    {
        return config::sysAdmin('geo.filter', false, TYPE_BOOL);
    }

    /**
     * Autocomplete для городов/областей
     * @param bool $bReturnArray вернуть результат в виде массива
     * @return array|void
     */
    public function regionSuggest($bReturnArray = false)
    {
        $aData = $this->input->postm(array(
            # часть названия города/области (вводимая пользователем)
            'q'       => TYPE_NOTAGS,
            # выполнять поиск среди городов и областей(регионов), false - только среди городов
            'reg'     => TYPE_BOOL,
            # выполнять поиск среди стран, городов и областей(регионов), false - зависит от reg
            'all'     => TYPE_BOOL,
            # выполнять поиск среди городов в которых есть метро
            'metro'   => TYPE_BOOL,
            # ID области(региона) или 0 (во всех областях)
            'region'  => TYPE_UINT,
            # ID страны или 0 (Geo::defaultCountry())
            'country' => TYPE_UINT,
        ));
        extract($aData);

        $sQuery = $this->input->cleanSearchString($q, 50);
        $aLevels = array(self::lvlCity);
        if ($reg) {
            $aLevels[] = self::lvlRegion; # города и области
        }

        # получаем список подходящих по названию городов
        $sqlTitle = 'R.title_' . LNG;
        $aFilter = array(
            'country' => (!empty($country) ? $country : static::defaultCountry()),
            'enabled' => 1,
            ':title'  => array($sqlTitle . ' LIKE (:q)', ':q' => $sQuery . '%')
        );
        if ($metro) {
            $aFilter['metro'] = 1;
        }
        if ($region) {
            $aFilter['pid'] = $region;
        }
        if ($all) {
            $aLevels = array(self::lvlCity, self::lvlRegion, self::lvlCountry);
            unset($aFilter['country']);
        }

        $aResult = static::model()->regionsList($aLevels, $aFilter, 0, 10, 'R.numlevel DESC, R.main DESC, ' . $sqlTitle . ' ASC');
        if ($bReturnArray) {
            return $aResult;
        }

        $aCity = array();
        foreach ($aResult as $v) {
            $aCity[] = array(
                $v['id'],
                $v['title'] . (!bff::adminPanel() ? '<br /><small class="grey">' . $v['ptitle'] . '</small>' : ''),
                $v['metro'],
                $v['pid'],
                $v['country']
            );
        }

        $this->ajaxResponse($aCity);
    }

    /**
     * Autocomplete для станций метро
     * @param bool $bReturnArray вернуть результат в виде массива
     * @return array|void
     */
    public function metroSuggest($bReturnArray = false)
    {
        $aData = $this->input->postm(array(
                'q'       => TYPE_NOTAGS,       # часть названия станции метро (вводимая пользователем)
                'city'  => TYPE_UINT,           # ID города
            )
        );
        extract($aData);

        $sQuery = $this->input->cleanSearchString($q, 50);

        # получаем список подходящих по названию городов
        $sqlTitle = 'M.title_' . LNG;
        $aFilter = array(
            'city_id'    => $city,
            'branch'     => 0,
            ':title'  => array($sqlTitle . ' LIKE (:q)', ':q' => $sQuery . '%')
        );

        $aResult =$this->model->metroAcList($aFilter, 10, 'M.num ASC, ' . $sqlTitle . ' ASC');
        if ($bReturnArray) {
            return $aResult;
        }

        $aData = array();
        foreach ($aResult as $v) {
            $aData[] = array(
                $v['id'],
                $v['title'],
            );
        }

        $this->ajaxResponse($aData);
    }

    /**
     * Формируем подсказку(presuggest) состоящую из основных городов, в формате JSON
     * @param int $nCountryID ID страны или 0 (Geo::defaultCountry())
     * @param bool $bReturnArray вернуть результат в виде массива
     * @param int $nLimit лимит
     */
    public static function regionPreSuggest($nCountryID = 0, $bReturnArray = false, $nLimit = 15)
    {
        if (empty($nCountryID)) {
            $nCountryID = static::defaultCountry();
        }

        $cache = Cache::singleton('geo');
        $cacheKey = 'city-presuggest-' . $nCountryID . '-' . LNG . '-' . $nLimit;
        if (($aData = $cache->get($cacheKey)) === false) {
            # получаем список предвыбранных основных городов страны
            $aData = static::model()->regionsList(self::lvlCity, array(
                    'main>0',
                    'enabled' => 1,
                    'country' => $nCountryID
                ), 0, $nLimit, 'main'
            );
            $cache->set($cacheKey, $aData);
        }
        if ($bReturnArray === true) {
            return $aData;
        }

        $aResult = array();
        foreach ($aData as $v) {
            $aResult[] = array($v['id'], $v['title'], $v['metro'], $v['pid']);
        }

        if ($bReturnArray === 2) {
            return $aResult;
        }

        return func::php2js($aResult); # возвращаем в JSON-формате для autocomplete.js
    }

    /**
     * Формируем список областей(регионов)
     * @param int $nCountryID ID страны или 0 (в стране static::defaultCountry(), если используется)
     * @param bool $bResetCache сбросить кеш
     * @return array
     */
    public static function regionList($nCountryID = 0, $bResetCache = false)
    {
        if (empty($nCountryID)) {
            $nCountryID = static::defaultCountry();
        }

        $cache = Cache::singleton('geo');
        $cacheKey = 'region-list-' . $nCountryID . '-' . LNG;
        if ($bResetCache === true || ($aData = $cache->get($cacheKey)) === false) {
            $aFilter = array('enabled' => 1);
            $aFilter['country'] = $nCountryID;
            $aData = static::model()->regionsList(self::lvlRegion, $aFilter);
            $cache->set($cacheKey, $aData);
        }

        return $aData;
    }

    /**
     * Формируем список основных городов
     * @param int $nRegionID ID области(региона) или 0 (список основных городов страны Geo::defaultCountry())
     * @param bool $bResetCache сбросить кеш
     * @return array
     */
    public static function cityList($nRegionID, $bResetCache = false)
    {
        $cache = Cache::singleton('geo');
        $cacheKey = 'city-list-' . $nRegionID . '-' . LNG;
        if ($bResetCache === true || ($aData = $cache->get($cacheKey)) === false) {
            $aFilter = array('enabled' => 1);
            if ($nRegionID) {
                $aFilter['pid'] = $nRegionID;
            } else {
                $aFilter['country'] = static::defaultCountry();
            }
            $aData = static::model()->regionsList(self::lvlCity, $aFilter, 0, 0, 'R.num');
            $cache->set($cacheKey, $aData);
        }

        return $aData;
    }

    /**
     * Формируем список районов города
     * @param int $nCityID ID города
     * @param bool $bResetCache сбросить кеш
     * @return array
     */
    public static function districtList($nCityID = 0, $bResetCache = false)
    {
        $cache = Cache::singleton('geo');
        $cacheKey = 'district-list-' . $nCityID . '-' . LNG;
        if ($bResetCache === true || ($aData = $cache->get($cacheKey)) === false) {
            $aData = static::model()->districtsList($nCityID);
            $cache->set($cacheKey, $aData);
        }

        return $aData;
    }

    /**
     * Формируем список районов города в формате select::options
     * @param int $nCityID ID города
     * @param int|bool $mSelectedID
     * @param mixed $mEmptyOption название option-пункта в случае если район не указан
     * @return string
     */
    public static function districtOptions($nCityID = 0, $mSelectedID = false, $mEmptyOption = 'Выбрать')
    {
        return HTML::selectOptions(static::districtList($nCityID), $mSelectedID, $mEmptyOption, 'id', 't');
    }

    /**
     * Сбрасываем кеш
     * @param mixed $mLevel тип региона Geo::lvl...
     * @param string $mExtra
     */
    public function resetCache($mLevel = false, $mExtra = '')
    {
        Cache::singleton('geo')->flush('geo');
    }

    /**
     * Задействовать страну при выборе региона
     * @return boolean
     */
    public static function countrySelect()
    {
        return config::sysAdmin('geo.country.select', true);
    }

    /**
     * Список стран
     * @return mixed
     */
    public static function countryList()
    {
        return static::model()->regionsList(static::lvlCountry, array('pid'=>0, 'enabled'=>1));
    }

    /**
     * Тип карт
     * @param boolean $includeEditor подключать редактор карты
     * @return mixed
     */
    public static function mapsAPI($includeEditor = false)
    {
        switch (static::mapsType())
        {
            case self::MAPS_TYPE_YANDEX:
                tpl::includeJS(static::$ymapsJS, false);
                break;
            case self::MAPS_TYPE_GOOGLE:
                tpl::includeJS(Request::scheme().'://maps.googleapis.com/maps/api/js?key='.config::sysAdmin('geo.maps.googleKey','').'&v=3&sensor=false&language='.LNG, false);
                break;
        }
        if ($includeEditor) {
            tpl::includeJS('maps.editor', true);
        }
    }

    public static function isDistrictBelongToCity($district_id, $city_id)
    {
        if (!$district_id || !$city_id) {
            return false;
        }

        if (key_exists($district_id, Geo::districtList($city_id))) {
            return true;
        }

        return false;
    }

    public static function districtTitle($district_id)
    {
        if (!$district_id) {
            return false;
        }

        $data = self::model()->districtData($district_id);
        if (empty($data)) {
            return false;
        }
        $title = $data['title_' . LNG];

        return $title;
    }

    /**
     * Проверка района города на наличие и принадлежность выбранному городу
     * @param $district_id - id района города
     * @param $city_id - id города
     */
    public function validateDistrictID($district_id, $city_id)
    {
        if (empty($district_id)) {
            $this->errors->set(_t('district', 'Выберите район города'));
        } elseif (!Geo::isDistrictBelongToCity((int)$district_id, $city_id)) {
            $this->errors->set(_t('district', 'Район города указан некорректно, не принадлежит городу'));
        }
    }

}