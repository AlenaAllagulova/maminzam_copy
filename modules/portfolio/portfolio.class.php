<?php

class Portfolio_ extends PortfolioBase
{
    /**
     * Добавление работы
     */
    public function add()
    {
        $nUserID = User::id();
        $aData = $this->validateItemData(0, Request::isPOST(), $nUserID);

        if (Request::isPOST()) {
            do {
                $aResponse = array();

                if (!User::isWorker() && Users::useClient()) {
                    $this->errors->set(_t('portfolio', 'Добавлять работы могут только исполнители'));
                    break;
                }

                if (!$this->errors->no()) {
                    break;
                }

                # создаем работу
                $aData['user_id'] = $nUserID;

                # доверенный пользователь: без модерации
                if ($trusted = User::isTrusted($this->module_name)) {
                    $aData['moderated'] = 1;
                }

                $nItemID = $this->model->itemSave(0, $aData, $nUserID);
                if (!$nItemID) {
                    $this->errors->reloadPage();
                    break;
                }

                # накручиваем счетчик кол-ва работ пользователя
                $this->security->userCounter('portfolio', 1, $nUserID);

                # сохраним превью
                $aUpdate = array();
                $oPreview = static::itemPreview($nItemID);
                foreach ($oPreview->getVariants() as $iconField => $v) {
                    $oPreview->setVariant($iconField);
                    $aIconData = $oPreview->uploadFILES($iconField, true, false);
                    if (!empty($aIconData)) {
                        $aUpdate[$iconField] = $aIconData['filename'];
                    } else {
                        if ($this->input->post($iconField . '_del', TYPE_BOOL)) {
                            if ($oPreview->delete(false)) {
                                $aUpdate[$iconField] = '';
                            }
                        }
                    }
                }
                if (!empty($aUpdate)) {
                    $this->model->itemSave($nItemID, $aUpdate);
                }

                # загружаем изображения
                if (static::imagesLimit()) {
                    # сохраняем / загружаем изображения
                    $oImages = $this->itemImages($nItemID);
                    $oImages->setAssignErrors(false);
                    if ($this->input->post('images_type', TYPE_STR) == 'simple') {
                        # загружаем
                        if (!empty($_FILES)) {
                            for ($i = 1; $i <= $oImages->getLimit(); $i++) {
                                $oImages->uploadFILES('images_simple_' . $i);
                            }
                            # удаляем загруженные через "удобный способ"
                            $aImages = $this->input->post('images', TYPE_ARRAY_STR);
                            $oImages->deleteImages($aImages);
                        }
                    } else {
                        # перемещаем из tmp-директории в постоянную
                        $oImages->saveTmp('images');
                    }
                }
                $this->model->makeUserWorksCache($nUserID);  # обновим кеш работ

                # обновляем счетчик работ "на модерации"
                if (!$trusted) {
                    $this->moderationCounterUpdate(1);
                }

                $aResponse['redirect'] = static::url('status', array('id' => $nItemID));

            } while (false);
            $this->iframeResponseForm($aResponse);
        }

        if ($nUserID && Users::useClient() && !User::isWorker()) {
            return $this->showForbidden('', _t('portfolio', 'Добавлять работы могут только исполнители'));
        }
        if (!$nUserID) {
            return $this->showForbiddenGuests();
        }

        bff::setMeta(_t('portfolio', 'Добавление работы в портфолио'));
        return $this->form(0, $aData);
    }

    /**
     * Редактирование работы
     */
    public function edit()
    {
        $nUserID = User::id();
        if (!$nUserID) {
            $this->redirect(Users::url('login'), false, true);
        }

        $nItemID = $this->input->get('id', TYPE_UINT);
        if (!$nItemID) {
            $this->errors->error404();
        }
        $aItemData = $this->model->itemData($nItemID, array(), true);
        if (empty($aItemData)) {
            $this->errors->error404();
        }

        if (!$this->isItemOwner($nItemID, $aItemData['user_id'])) {
            if (Request::isPOST()) {
                $this->errors->reloadPage();
                $this->iframeResponseForm();
            }
            return $this->showForbidden('', _t('portfolio', 'Вы не является владельцем данной работы'));
        }

        if (Request::isPOST()) {
            $aData = $this->validateItemData($nItemID, true, $nUserID);
            $aResponse = array();

            if ($this->errors->no()) {

                if ($aItemData['status'] == self::STATUS_BLOCKED) {
                    # заказ заблокирован, помечаем на проверку модератору
                    $aData['moderated'] = 0;
                }

                # доверенный пользователь: без модерации
                $trusted = User::isTrusted($this->module_name);
                # помечаем на модерацию при изменении: названия, описания
                if ($aData['title'] != $aItemData['title'] || $aData['descr'] != $aItemData['descr']) {
                    if ($aItemData['moderated'] != 0 && !$trusted) {
                        $aData['moderated'] = 2;
                    }
                }

                $this->model->itemSave($nItemID, $aData, $nUserID);

                # сохраним превью
                $aUpdate = array();
                $oPreview = static::itemPreview($nItemID);
                foreach ($oPreview->getVariants() as $iconField => $v) {
                    $oPreview->setVariant($iconField);
                    $aIconData = $oPreview->uploadFILES($iconField, true, false);
                    if (!empty($aIconData)) {
                        $aUpdate[$iconField] = $aIconData['filename'];
                    } else {
                        if ($this->input->post($iconField . '_del', TYPE_BOOL)) {
                            if ($oPreview->delete(false)) {
                                $aUpdate[$iconField] = '';
                            }
                        }
                    }
                }
                if (!empty($aUpdate)) {
                    $this->model->itemSave($nItemID, $aUpdate);
                }

                if (static::imagesLimit()) {
                    # сохраняем / загружаем изображения
                    $oImages = $this->itemImages($nItemID);
                    if ($this->input->post('images_type', TYPE_STR) == 'simple') {
                        # загружаем
                        if (!empty($_FILES) && $aItemData['imgcnt'] < $oImages->getLimit()) {
                            for ($i = 1; $i <= $oImages->getLimit(); $i++) {
                                $oImages->uploadFILES('images_simple_' . $i);
                            }
                        }
                    } else {
                        # сохраняем порядок изображений
                        $aImages = $this->input->post('images', TYPE_ARRAY_STR);
                        $oImages->saveOrder($aImages, false);
                    }

                    # помечаем на модерацию при изменении: фотографий
                    if ($oImages->newImagesUploaded($this->input->post('images_hash', TYPE_STR))
                        && $aItemData['moderated'] != 0  && ! $trusted
                    ) {
                        $this->model->itemSave($nItemID, array('moderated' => 2));
                    }
                }

                $this->model->makeUserWorksCache($nUserID);  # обновим кеш работ

                # обновляем счетчик работ "на модерации"
                $this->moderationCounterUpdate();

                $aResponse['redirect'] = static::url('user.view',
                    array('id' => $nItemID, 'keyword' => $aData['keyword'], 'login' => User::data('login'))
                );
            }
            $this->iframeResponseForm($aResponse);
        }

        bff::setMeta(_t('portfolio', 'Редактирование работы в портфолио'));
        return $this->form($nItemID, $aItemData);
    }

    /**
     * Форма работы
     * @param integer $nItemID ID работы
     * @param array $aData @ref
     * @return string HTML
     */
    protected function form($nItemID, array &$aData)
    {
        $nUserID = User::id();
        $aData['id'] = $nItemID;
        $aData['img'] = $this->itemImages($nItemID);

        if ($nItemID) {
            if (static::imagesLimit()) {
                $aImages = $aData['img']->getData($aData['imgcnt']);
                $aData['images'] = array();
                foreach ($aImages as $v) {
                    $aData['images'][] = array(
                        'id'       => $v['id'],
                        'tmp'      => false,
                        'filename' => $v['filename'],
                        'i'        => $aData['img']->getURL($v, PortfolioItemImages::szForm, false)
                    );
                }
                $aData['imghash'] = $aData['img']->getLastUploaded();
            }
            $aData['priceSett'] = Specializations::i()->aPriceSett($aData['spec_id']);
            $aData['priceForm'] = $this->viewPHP($aData, 'form.price');
        } else {
            $aData['images'] = array();
            $aData['imgcnt'] = 0;
            $aData['imghash'] = '';
            $aData['priceForm'] = '';
        }
        $aData['specs'] = Users::model()->userSpecs($nUserID, false);
        foreach ($aData['specs'] as $k => $v) {
            $aData['specs'][$k]['t'] = (!empty($v['cat_title']) ? $v['cat_title'] . ' / ' : '') . $v['spec_title'];
            if ($aData['spec_id'] == $v['spec_id']) {
                $aData['specs'][$k]['a'] = 1;
            } else {
                $aData['specs'][$k]['a'] = 0;
            }
        }

        # получим размер preview
        $oPreview = static::itemPreview($nItemID);
        $aSizes = $oPreview->getVariants();
        $aSizes = reset($aSizes);
        $aSizes = $aSizes['sizes'];
        $aSizes = reset($aSizes);
        $aData['preview_sizes'] = $aSizes['width'] . 'x' . $aSizes['height'];

        $aData['edit'] = !empty($nItemID);

        return $this->viewPHP($aData, 'form');
    }

    /**
     * Вывод сообщения о статусе работы
     */
    public function status()
    {
        $nItemID = $this->input->get('id', TYPE_UINT);
        if (!$nItemID) {
            $this->errors->error404();
        }
        if (!User::id()) {
            return $this->showForbiddenGuests();
        }

        $aData = $this->model->itemData($nItemID, array('moderated', 'spec_id', 'keyword', 'user_id'));
        if (empty($aData) || ! User::isCurrent($aData['user_id'])) {
            $this->errors->error404();
        }

        if ($aData['moderated'] > 0) {
            $this->redirect(static::url('user.view', array('id'=>$nItemID, 'keyword'=>$aData['keyword'], 'login' => User::data('login'))));
        }

        return $this->viewPHP($aData, 'status');
    }

    /**
     * Кабинет: Портфолио
     * @param array $userData данные о пользователе
     */
    public function my_portfolio(array $userData)
    {
        return $this->user_portfolio($userData);
    }

    /**
     * Профиль: Портфолио
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    public function user_portfolio(array $userData)
    {
        if (empty($userData['id'])) {
            $this->errors->error404();
        }

        # просмотр работы
        $itemID = $this->input->get('id', TYPE_UINT);
        if ($itemID) {
            return $this->owner_view($itemID, $userData);
        }

        # список работ
        return $this->owner_listing($userData['id'], $userData);
    }

    /**
     * Кабинет: Настройка портфолио
     * @return string HTML
     */
    public function my_settings()
    {
        $this->security->setTokenPrefix('portfolio-my-settings');
        $nUserID = User::id();

        if (Request::isPOST()) {

            $aData = $this->input->postm(array(
                'portfolio_intro' => array(TYPE_NOTAGS, 'len'=>3000, 'len.sys' => 'portfolio.intro.limit'), # Описание
            ));

            # антиспам фильтр
            Site::i()->spamFilter(array(
                array('text' => & $aData['portfolio_intro'], 'error'=>_t('', 'В указанном вами заголовке присутствует запрещенное слово "[word]"')),
            ));

            if($this->errors->no()) {
                Users::model()->userSave($nUserID, $aData);
            }
            $this->ajaxResponseForm();
        } else {
            $aData = Users::model()->userData($nUserID, array('portfolio_intro'));
        }

        if ($nUserID && Users::useClient() && !User::isWorker()) {
            return $this->showForbidden('', _t('portfolio', 'Портфолио доступно только исполнителям'));
        }
        if (!$nUserID) {
            return $this->showForbiddenGuests();
        }

        bff::setMeta(_t('', 'Settings'));
        return $this->viewPHP($aData, 'settings');
    }

    /**
     * Список работ портфолио пользователя
     * @param integer $userID ID пользователя
     * @param array $userData данные пользователя
     * @return string HTML
     */
    protected function owner_listing($userID, array $userData)
    {
        $pagination_on = false; # Использовать постраничную навигацию
        $pageSize = 10;

        $aData = Users::model()->userData($userID, array('portfolio_intro'));
        if (empty($aData)) {
            $this->errors->error404();
        }

        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT, # № страницы
            'spec' => TYPE_UINT, # ID специализации
        ));
        $aData['f'] = &$f;
        $aData['user'] = &$userData;

        $seoFilter = 0;
        $aFilter = array();

        if ($f['spec']) {
            $aFilter['spec_id'] = $f['spec'];
            $seoFilter++;
        }

        $aData['pgn'] = '';
        $aData['count'] = $this->model->itemsList($userID, $aFilter, true);
        if ($aData['count']) {
            if($pagination_on) {
                $pgn = new Pagination($aData['count'], $pageSize, array(
                    'link' => static::url('user.listing', array('login' => $userData['login'])),
                    'query' => $f,
                ));
                $aData['list'] = $this->model->itemsList($userID, $aFilter, false, $pgn->getLimitOffset());
                $aData['pgn'] = $pgn->view();
                $f['page'] = $pgn->getCurrentPage();
            } else {
                $aData['list'] = $this->model->itemsList($userID, $aFilter);
            }
        }
        $aData['specs'] = array(
            0 => array(
                'id'    => 0,
                'title' => _t('portfolio', 'Все разделы')
            )
        );
        $aData['specs'] += $this->model->specsOwnerList($userID);

        $aData['list'] = $this->viewPHP($aData, 'owner.list.ajax');
        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'  => $aData['pgn'],
                'list' => $aData['list'],
            ));
        }

        # SEO: Портфолио: список работ
        $this->seo()->robotsIndex(!$seoFilter);
        $seoURL = static::url('user.listing', array('login'=>$userData['login']), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL);
        $this->seo()->setPageMeta($this->users(), 'profile-portfolio', array(
            'description' => tpl::truncate(strip_tags($aData['portfolio_intro']), config::sysAdmin('portfolio.listing.meta.description.truncate', 150, TYPE_UINT)),
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'region'  => $userData['region'],
            'country' => $userData['country'],
            'page'    => $f['page'],
        ), $aData);

        return $this->viewPHP($aData, 'owner.list');
    }

    /**
     * Просмотр работы пользователя
     * @param integer $nItemID ID работы
     * @param array $userData данные пользователя
     * @return string
     */
    protected function owner_view($nItemID, array $userData)
    {
        if (!$nItemID) $this->errors->error404();
        $aData = $this->model->itemData($nItemID);
        if (empty($aData)) $this->errors->error404();

        # Статус
        if ( ! $userData['my']) {
            # Неактивная
            if ($aData['status'] != static::STATUS_ACTIVE) {
                $this->errors->error404();
            }
            # Ожидает модерации
            if (static::premoderation() && ! $aData['moderated']) {
                $this->errors->error404();
            }
        }
        $aData['user'] = &$userData;

        # Изображения
        $aData['images'] = array();
        if (static::imagesLimit() && $aData['imgcnt']) {
            $oImages = $this->itemImages($nItemID);
            $aImages = $oImages->getData($aData['imgcnt']);
            foreach ($aImages as $v) {
                $aData['images'][] = array(
                    'id'       => $v['id'],
                    'filename' => $v['filename'],
                    'i'        => $oImages->getURL($v, array(PortfolioItemImages::szView), false)
                );
            }
        }

        # Следующая работа
        $aData['next'] = $this->model->itemNext($nItemID, $userData['id'], $aData['created']);
        if (!empty($aData['next'])) {
            $aData['next']['link'] = static::url('user.view', array('id'      => $aData['next']['id'],
                                                                    'keyword' => $aData['next']['keyword'],
                                                                    'login'   => $userData['login']
                )
            );
        }
        # Предыдущая работа
        $aData['prev'] = $this->model->itemPrev($nItemID, $userData['id'], $aData['created']);
        if (!empty($aData['prev'])) {
            $aData['prev']['link'] = static::url('user.view', array('id'      => $aData['prev']['id'],
                                                                    'keyword' => $aData['prev']['keyword'],
                                                                    'login'   => $userData['login']
                )
            );
        }

        # SEO: Портфолио: просмотр работы
        $seoURL = static::url('user.view', array('id'=>$nItemID, 'keyword'=>$aData['keyword'], 'login'=>$userData['login']), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL);
        $this->seo()->setPageMeta($this->users(), 'profile-portfolio-view', array(
            'title' => tpl::truncate($aData['title'], 50, ''),
            'title.full' => $aData['title'],
            'description' => tpl::truncate(strip_tags($aData['descr']), config::sysAdmin('portfolio.view.meta.description.truncate', 150, TYPE_UINT)),
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'country' => $userData['country'],
        ), $aData);
        # SEO: Open Graph
        $seoSocialImages = array();
        foreach ($aData['images'] as $v) $seoSocialImages[] = $v['i'][PortfolioItemImages::szView];
        $this->seo()->setSocialMetaOG($aData['share_title'], $aData['share_description'], $seoSocialImages, $seoURL, $aData['share_sitename']);

        return $this->viewPHP($aData, 'owner.view');
    }

    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR)) {
            # форма редактирования раздела пользователя
            case 'spec-edit-form':
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nSpecID = $this->input->post('id', TYPE_UINT);
                $nUserID = User::id();
                $aData = $this->model->specUserData($nUserID, $nSpecID);
                $aData['specs'] = $this->model->specsOwnerList($nUserID);
                $aData['pro'] = User::isPro();
                unset($aData['specs'][$nSpecID]);

                $aResponse['form'] = $this->viewPHP($aData, 'owner.form.spec');
            }
            break;
            # сохранение раздела пользователя
            case 'spec-edit':
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nSpecID = $this->input->post('id', TYPE_UINT);
                $nUserID = User::id();

                $aParams = array(
                    'descr' => TYPE_NOTAGS,
                );
                if ( ! bff::servicesEnabled() || User::isPro() || ! static::previewOnlyPro()) {
                    $aParams['preview'] = TYPE_BOOL;
                }

                $aData = $this->input->postm($aParams);
                $nOrder = $this->input->post('order', TYPE_UINT);
                if ($nOrder == 1) { # Первый по счету;
                    $this->model->specsUserCheckNum($nUserID);
                    $aData['num'] = 1;
                    $this->model->specsUserNumInc($nUserID);
                }
                if ($nOrder == 2) { # После другого раздела;
                    $this->model->specsUserCheckNum($nUserID);
                    $nSpecAfterID = $this->input->post('cat_after', TYPE_UINT);
                    $aSpecData = $this->model->specUserData($nUserID, $nSpecAfterID);
                    $aData['num'] = $aSpecData['num'] + 1;
                    $this->model->specsUserNumInc($nUserID, $aSpecData['num']);
                }
                $this->model->specUserSave($nUserID, $nSpecID, $aData);
                $aResponse['descr'] = nl2br($aData['descr']);
            }
            break;
            case 'form-price':  # форма стоимости и сроков
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nSpecID = $this->input->post('id', TYPE_UINT);
                if( ! $nSpecID){
                    $aResponse['html'] = '';
                    break;
                }

                $aData = array(
                    'price'      => 0,
                    'price_curr' => 0,
                    'price_rate' => 0,
                );
                $aData['priceSett'] = Specializations::i()->aPriceSett($nSpecID);
                $aResponse['html'] = $this->viewPHP($aData, 'form.price');
            }
            break;
            case 'item-delete': # удаление работы
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nItemID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->itemData($nItemID);

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if (!$this->isItemOwner($nItemID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if (!$this->itemDelete($nItemID)) {
                    $this->errors->reloadPage();
                    break;
                }
            }
            break;

            default:
                $this->errors->impossible();
        }
        $this->ajaxResponseForm($aResponse);
    }

    public function itemStatusBlock(array &$aData)
    {
        if (empty($aData['user_id'])) {
            return '';
        }
        if (!User::isCurrent($aData['user_id'])) {
            return '';
        }

        return $this->viewPHP($aData, 'status.block');
    }

    /**
     * Управление изображениями работы
     * @param getpost ::uint 'item_id' ID работы
     * @param getpost ::string 'act' действие
     */
    public function img()
    {
        $nItemID = $this->input->getpost('item_id', TYPE_UINT);
        $oImages = $this->itemImages($nItemID);
        $aResponse = array();

        switch ($this->input->getpost('act'))
        {
            case 'upload':
            {
                $aResponse = array('success' => false);
                do {
                    if (!$this->security->validateToken(true, false)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if ($nItemID) {
                        if (!$this->isItemOwner($nItemID)) {
                            $this->errors->set(_t('portfolio', 'Вы не является владельцем данной работы'));
                            break;
                        }
                    }

                    $result = $oImages->uploadQQ();

                    $aResponse['success'] = ($result !== false && $this->errors->no());
                    if ($aResponse['success']) {
                        $aResponse = array_merge($aResponse, $result);
                        $aResponse['tmp'] = empty($nItemID);
                        $aResponse['i'] = $oImages->getURL($result, PortfolioItemImages::szForm, $aResponse['tmp']);
                        unset($aResponse['dir'], $aResponse['srv']);
                    }
                } while (false);

                $aResponse['errors'] = $this->errors->get();
                $this->ajaxResponse($aResponse, true, false, true);
            }
            break;
            case 'delete':
            {
                $nImageID = $this->input->post('image_id', TYPE_UINT);
                $sFilename = $this->input->post('filename', TYPE_NOTAGS);

                # неуказан ID изображения ни filename временного
                if (!$nImageID && empty($sFilename)) {
                    $this->errors->reloadPage();
                    break;
                }

                if (!$this->security->validateToken(true, false)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($nItemID) {
                    # проверяем доступ на редактирование
                    if (!$this->isItemOwner($nItemID)) {
                        $this->errors->set(_t('portfolio', 'Вы не является владельцем данной работы'));
                        break;
                    }
                }

                if ($nImageID) {
                    # удаляем изображение по ID
                    $oImages->deleteImage($nImageID);
                } else {
                    # удаляем временное
                    $oImages->deleteTmpFile($sFilename);
                }
            }
            break;
            case 'delete-tmp':
            {
                $aFilenames = $this->input->post('filenames', TYPE_ARRAY_STR);
                $oImages->deleteTmpFile($aFilenames);
            }
            break;
            default:
                $this->errors->reloadPage();
        }

        $this->ajaxResponseForm($aResponse);
    }

}