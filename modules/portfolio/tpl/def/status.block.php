<?php
/**
 * @var $this Portfolio
 */
?>
<? if(!$moderated && Portfolio::premoderation()): ?><div class="alert alert-warning" role="alert"><?= _t('portfolio', 'Ожидает проверки менеджера') ?></div><? endif; ?>
<? if($status == Portfolio::STATUS_BLOCKED): ?>
    <div class="alert alert-danger" role="alert"><b><?= _t('portfolio', 'Заблокирована менеджером.') ?></b>
        <? if( ! empty($blocked_reason)): ?><br /><?= _t('', 'Причина блокировки:'); ?> <?= tpl::blockedReason($blocked_reason) ?><? endif; ?>
    </div>
<? endif; ?>