<?php

abstract class ContactsBase_ extends Module
{
    /** @var ContactsModel */
    var $model = null;

    const TYPE_SITE_ERROR   = 1;
    const TYPE_TECH_SUPPORT = 2;

    public function init()
    {
        parent::init();

        $this->module_title = _t('contacts','Контакты');

        bff::autoloadEx(array(
            'ContactsAttachment' => array('app', 'modules/contacts/contacts.attach.php'),
        ));
    }

    public function sendmailTemplates()
    {
        return array(
            'contacts_admin' => array(
                'title'       => 'Форма контактов: уведомление о новом сообщении',
                'description' => 'Уведомление, отправляемое <u>администратору</u> (' . config::sys('mail.admin') . ') после отправки сообщения через форму контактов',
                'vars'        => array('{name}' => 'Имя', '{email}' => 'Email', '{message}' => 'Сообщение'),
                'impl'        => true,
                'priority'    => 90,
                'enotify'     => -1,
            ),
        );
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts доп. параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # главная
            case 'write':
                $url .= '/help/write' . static::urlQuery($opts);
                break;
        }
        return bff::filter('contacts.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Использовать прикрепления
     * @return bool
     */
    public static function attachmentsEnabled()
    {
        return config::sysAdmin('contacts.attachments.enabled', true, TYPE_BOOL);
    }

    public function getContactTypes($options = false)
    {
        $types = bff::filter('contacts.types.list', array(
            self::TYPE_SITE_ERROR   => array('id'    => self::TYPE_SITE_ERROR,
                                             'title' => _t('contacts', 'Ошибка на сайте')
            ),
            self::TYPE_TECH_SUPPORT => array('id'    => self::TYPE_TECH_SUPPORT,
                                             'title' => _t('contacts', 'Технический вопрос')
            ),
        ));

        if ($options) {
            return HTML::selectOptions($types, 0, false, 'id', 'title');
        }

        return $types;
    }

    public function updateCounter($nTypeID, $nIncrement)
    {
        config::saveCount('contacts_new', $nIncrement, true);
        config::saveCount('contacts_new_' . $nTypeID, $nIncrement, true);
    }

    /**
     * Инициализация компонента работы с вложениями
     * @return ContactsAttachment
     */
    public function attach()
    {
        static $i;
        if (!isset($i)) {
            # до 5 мегабайт
            $i = new ContactsAttachment(bff::path('contacts'), 5242880);
        }

        return $i;
    }

    /**
     * Загрузка приложения к сообщению
     * @param string $inputName имя input-file поля
     * @return string имя загруженного файла
     */
    public function attachUpload($inputName = 'attach')
    {
        if ( ! static::attachmentsEnabled()) {
            return '';
        }

        return $this->attach()->uploadFILES($inputName);
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('contacts') => 'dir', # вложения
        ));
    }
}