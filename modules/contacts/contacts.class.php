<?php

class Contacts_ extends ContactsBase
{
    public function write()
    {
        if (Request::isPOST())
        {
            do {
                $nUserID = User::id();

                $p = $this->input->postm(array(
                    'name'    => array(TYPE_NOTAGS, 'len' => 70, 'len.sys' => 'contacts.name.limit'), # имя
                    'email'   => array(TYPE_NOTAGS, 'len' => 70, 'len.sys' => 'contacts.email.limit'), # e-mail
                    'ctype'   => TYPE_UINT,   # тип контакта
                    'message' => array(TYPE_TEXT, 'len'=>3000, 'len.sys' => 'contacts.message.limit'), # сообщение
                    'captcha' => TYPE_NOTAGS, # капча
                ));

                if (!$this->input->isEmail($p['email'], false)) {
                    $this->errors->set(_t('contacts', 'E-mail адрес указан некорректно'), 'email');
                }

                if (empty($p['message'])) {
                    $this->errors->set(_t('contacts', 'Укажите текст сообщения'), 'message');
                }

                if (!$nUserID) {
                    if (!CCaptchaProtection::isCorrect($p['captcha'])) {
                        $this->errors->set(_t('contacts', 'Результат с картинки указан некорректно'), 'captcha');
                    }
                }

                if ( ! $this->errors->no()) {
                    break;
                }
                CCaptchaProtection::reset();
                unset($p['captcha']);

                # не чаще чем раз в {X} секунд с одного IP (для одного пользователя)
                if (Site::i()->preventSpam('contacts-write', config::sysAdmin('contacts.prevent.spam', 20, TYPE_UINT))) {
                    break;
                }

                # загружаем вложения
                if (static::attachmentsEnabled()) {
                    $this->attach()->setAssignErrors(true);
                    $p['attach'] = $this->attachUpload();
                    if ( ! $this->errors->no()) {
                        break;
                    }
                }

                # проверяем на корректность "тип контакта", если некорректный берем первый из доступных
                $aContactTypes = $this->getContactTypes(false);
                if (!isset($aContactTypes[$p['ctype']])) {
                    $p['ctype'] = key($aContactTypes);
                }

                $nContactID = $this->model->contactSave(0, $p);

                if ($nContactID) {
                    $this->updateCounter($p['ctype'], 1);

                    bff::sendMailTemplate(array(
                        'name'    => $p['name'],
                        'email'   => (!$nUserID ? $p['email'] : User::data('email')),
                        'message' => nl2br($p['message']),
                    ), 'contacts_admin', config::sys('mail.admin'));
                }

            } while(false);

            if (Request::isAJAX()) {
                $this->ajaxResponseForm();
            }
            $this->iframeResponseForm();
        }

        if (User::id()) {
            $aData['user'] = User::data(array('name','email'));
            $aData['user'] = HTML::escape($aData['user']);
        } else {
            $aData['user'] = array('name'=>'', 'email'=>'');
        }

        $aData['ctype_options'] = $this->getContactTypes(true);

        $aData['breadCrumbs'] = array(
            array('title' => _t('help', 'Help'), 'link' => Help::url('index'), 'active' => false),
            array('title' => _t('help', 'Задать вопрос'), 'active' => true),
        );

        # SEO: Связь с администрацией
        $this->urlCorrection(static::url('write'));
        $this->seo()->canonicalUrl(static::url('write', array(), true));
        $this->seo()->setPageMeta('site', 'contact-form', array(), $aData);

        bff::setActiveMenu('//more/help');
        return $this->viewPHP($aData, 'write');
    }

}