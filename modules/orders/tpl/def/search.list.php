<?php
$bPromote = ! empty($bPromote);
if( ! empty($list)):
    $lng_pro_only = _t('orders', 'Только для [pro]', array('pro'=>'<span class="pro">pro</span>'));
    $lng_offers = _t('orders', 'предложение;предложения;предложений');
    $aServiceTypes = Orders::aServiceTypes();
    $tagsLimit = Orders::searchTagsLimit();
    $fairplayEnabled = bff::fairplayEnabled();
    ?>
    <? if($bPromote): ?><div class="l-projectList mrgb30"><? else: ?><ul class="l-projectList"><? endif; ?>
    <? foreach($list as $v): ?>
        <<?= $bPromote ? 'div' : 'li' ?> class="j-order <?= $v['svc_marked'] ? ' highlited' : '' ?>">
            <header class="l-project-title">
                <? if($v['status'] == Orders::STATUS_CLOSED): ?><i class="fa fa-lock show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Закрытый заказ'); ?>"></i><? endif; ?>
                <? if($v['svc_fixed'] || $bPromote): ?><i class="fa fa-thumb-tack c-icon-fixed<?= ! $v['svc_fixed'] ? ' j-fixed hidden' : '' ?>"></i><? endif; ?>
                <? if($fairplayEnabled && $v['fairplay']): ?><i class="fa fa-shield c-safe-color show-tooltip" data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip"></i><? endif; ?>
                <a href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a>
            </header>

            <div class="l-project-head">
                <? if($v['type'] == Orders::TYPE_SERVICE): ?>
                    <? if($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                        <span class="l-price_na"><?= ! empty($v['price_rate_text'][LNG]) ? $v['price_rate_text'][LNG] : _t('orders', 'По договоренности'); ?></span>
                    <? else: ?>
                        <span class="l-price"><?= tpl::formatPrice($v['price']) ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?> <? if( ! empty($v['price_rate_text'][LNG])): ?><small><?= $v['price_rate_text'][LNG] ?></small><? endif; ?></span>
                    <? endif; ?>
                <? endif; ?>
                <? if( ! empty($v['aTags'])): $n = 0; foreach($v['aTags'] as $vv): ?>
                    <? if($tagsLimit && $n == $tagsLimit): ?><a class="l-tag l-tag-more j-tag-more" href="#"><?= _t('', 'еще ...'); ?></a><? endif; $n++; ?>
                    <a href="<?= Orders::url('search-tag', $vv) ?>" class="l-tag<?= $tagsLimit && $n > $tagsLimit ? ' hidden' : '' ?>"><?= $vv['tag'] ?></a>
                <? endforeach; endif; ?>
            </div>

            <article>
                <p><?= tpl::truncate($v['descr'], config::sysAdmin('orders.search.list.descr.truncate', 250, TYPE_UINT)); ?></p>
            </article>

            <ul class="l-item-features">
                <? if($v['pro']): ?><li><span class="small"><?= $lng_pro_only; ?></span></li><? endif; ?>
                <? if( ! empty($v['city_data']['title'])): ?>
                    <li>
                        <i class="fa fa-map-marker"></i>
                        <?= $v['city_data']['title']?>
                    </li>
                    <? if(!empty($v['district_id'])):?>
                        <li>
                            <?= _t('', 'р-н ');?>
                            <?= Geo::districtTitle($v['district_id'])?>
                        </li>
                    <?endif;?>
                <? endif; ?>
                <? if($v['type'] == Orders::TYPE_SERVICE):?><li><i class="fa fa-<?= $aServiceTypes[ $v['service_type'] ]['c'] ?>"></i> <?= $aServiceTypes[ $v['service_type'] ]['t'] ?></li><? endif; ?>
                <li><i class="fa fa-clock-o"></i> <?= tpl::date_format_spent($v['created'], false, true) ?></li>
                <? if($v['expire'] != '0000-00-00 00:00:00'): ?><li><i class="fa fa-calendar"></i> <?= _t('orders', 'до [date]', array('date' => tpl::date_format3($v['expire'], 'd.m.Y'))); ?></li><? endif; ?>
                <li><a href="<?= $v['url_view'] ?>#offers"><i class="fa fa-comments-o c-link-icon"></i><span><?= tpl::declension($v['offers_cnt'], $lng_offers)?></span></a></li>
            </ul>

        </<?= $bPromote ? 'div' : 'li' ?>>
    <? endforeach; ?>
    </<?= $bPromote ? 'div' : 'ul' ?>>
<? else: ?>
    <div class="alert alert-info"><?= _t('orders', 'Заказы не найдены'); ?></div>
<? endif;