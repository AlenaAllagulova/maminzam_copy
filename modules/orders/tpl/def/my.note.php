<?php
    /**
     * Форма управления заметкой о заказе
     * @var $this Orders
     */
    tpl::includeJS('orders.note', false, 2);

    if( ! isset($id)) { $aData['id'] = 0; }
    if( ! isset($note)) { $aData['note'] = ''; }
?>
<?= $this->viewPHP($aData, 'my.note.block'); ?>
<script type="text/javascript">
<? js::start() ?>
    jOrdersNote.init(<?= func::php2js(array(
        'lang' => array(
    ),
)) ?>);
<? js::stop() ?>
</script>
