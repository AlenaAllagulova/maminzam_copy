<?php
/**
 * @var $this Orders
 * @var $types array типы заказа
 */
$aData = HTML::escape($aData, 'html', array('title','descr','addr_addr'));
$aData['edit'] = $edit = ! empty($id);
if( ! array_key_exists($type, $types) ){
    $type = key($types);
}
$typeClass = function($typeNeed) use ($type) {
    return 'j-type j-type-'.$typeNeed.($type != $typeNeed ? ' hidden' : '');
};
$tab = $this->input->getpost('ftab', TYPE_NOTAGS);
$aTabs = array(
    'info'        => _t('orders','Описание'),
    'images'      => _t('orders','Фото'),
    'attachments' => _t('orders','Файлы'),
    'offers'      => _t('orders','Заявки'),
);
if( ! Orders::imagesLimit()) unset($aTabs['images']);
if( ! Orders::attachmentsLimit()) unset($aTabs['attachments']);
if($edit){
    if(bff::servicesEnabled()){
        $aTabs['svc'] = _t('orders','Услуги');
    }
}else{
    unset($aTabs['offers']);
}
$bMapEnabled = Orders::mapEnabled();
$fairplayEnabled = bff::fairplayEnabled();

$aTabs = bff::filter('orders.admin.order.form.tabs', $aTabs, array('edit'=>$edit,'data'=>&$aData,'tab'=>&$tab));
if( ! isset($aTabs[$tab])) {
    $tab = 'info';
}
if(count($aTabs) > 1): ?>
<div class="tabsBar">
    <? foreach($aTabs as $k=>$v): ?>
        <span class="tab<?= $k==$tab ? ' tab-active' : '' ?>"><a href="#" onclick="jOrdersOrdersForm.onTab('<?= $k ?>', this); return false;"><?= $v ?></a></span>
    <? endforeach; ?>
    <div class="progress" style="margin-left: 5px; display: none;" id="form-progress"></div>
</div>
<? endif; ?>
<div id="order-form-block-info" class="hidden">
<form name="OrdersOrdersForm" id="OrdersOrdersForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
    <input type="hidden" name="save" value="1" />
    <input type="hidden" name="id" value="<?= $id ?>" />
    <input type="hidden" name="addr_lat" id="order-addr_lat" value="<?= $addr_lat ?>" />
    <input type="hidden" name="addr_lng" id="order-addr_lng" value="<?= $addr_lng ?>" />
    <table class="admtbl tbledit">
        <tr class="<?= ( ! Orders::useProducts() ? 'hidden' : 'required' ) ?>">
            <td class="row1 field-title"><?= _t('orders', 'Тип') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <? if($edit): ?>
                    <input type="hidden" name="type" value="<?= $type ?>" />
                    <strong><?= $types[$type]['t'] ?></strong>
                    <? if($visibility == Orders::VISIBILITY_PRIVATE): ?>
                        <div class="right desc" style="margin-right: 10px;">приватный заказ <i class="icon icon-eye-close disabled"></i></div>
                    <? endif; ?>
                <? else: foreach($types as $v): ?>
                    <label class="inline radio"><input type="radio" name="type" value="<?= $v['id'] ?>" <?= ($type == $v['id'] ? 'checked="checked"' : '') ?> onchange="jOrdersOrdersForm.onType($(this))"><?= $v['t'] ?></label>
                <? endforeach; endif; ?>
            </td>
        </tr>
        <tr class="required">
            <td class="row1 field-title" width="100"><?= _t('orders', 'Заголовок') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <input class="stretch" type="text" id="order-title" name="title" value="<?= $title ?>" maxlength="<?= Orders::titleLimit() ?>" />
            </td>
        </tr>
        <tr class="required">
            <td class="row1 field-title"><?= _t('orders', 'Описание') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <textarea class="stretch" rows="5" name="descr"><?= $descr ?></textarea>
            </td>
        </tr>
        <? if($fairplayEnabled): ?>
            <tr>
                <td class="row1 field-title"><?= _t('orders', 'Способ оплаты') ?></td>
                <td class="row2">
                    <? if($edit): ?>
                        <input type="hidden" name="fairplay" value="<?= $fairplay ?>" />
                        <strong><?= $fairplay == Orders::FAIRPLAY_USE ? _t('orders','Безопасная сделка') : _t('orders','Прямая оплата') ?></strong>
                    <? else: ?>
                        <label class="inline radio"><input type="radio" name="fairplay" value="<?= Orders::FAIRPLAY_USE ?>"><?= _t('orders','Безопасная сделка') ?></label>
                        <label class="inline radio"><input type="radio" name="fairplay" value="<?= Orders::FAIRPLAY_NONE ?>"><?= _t('orders','Прямая оплата') ?></label>
                    <? endif; ?>
                </td>
            </tr>
        <? endif; ?>
        <? if(Orders::termsEnabled()): ?>
        <tr>
            <td class="row1 field-title"><?= _t('orders', 'Прием заявок') ?></td>
            <td class="row2">
                <select name="term" style="width: 100px;<?= $edit ? 'display:none;' : '' ?>" onchange="jOrdersOrdersForm.onTerm($(this))"><?= Orders::aTermsOptions( $edit ? $term : false, $days) ?></select>
                <span style="margin-left: 20px; <?= $edit ? 'display:none;' : '' ?>">до <span class="j-term-title"><?= tpl::date_format2(time() + $days * 24*60*60, false, true) ?></span></span>
                <? if($edit): ?>
                    <? if($expire == '0000-00-00 00:00:00'): ?>
                        <span>Бессрочно</span>
                    <? else: ?>
                        <span>до <?= tpl::date_format2($expire, false, true) ?></span>
                    <? endif; ?>
                    <a href="#" class="ajax desc j-term-change" onclick="jOrdersOrdersForm.onTermChange($(this)); return false;">изменить</a>
                <? endif; ?>
            </td>
        </tr>
        <? endif; ?>

        <tr class="check-select <?= $typeClass(Orders::TYPE_SERVICE) ?>">
            <td class="row1 field-title"><?= _t('orders', 'Специализации') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <? $bAddSpecs = ! empty($specs) && count($specs) < Orders::specsLimit(); ?>
                <div id="j-specs-select-block" <? if( $bAddSpecs ) { ?> style="display: none;"<? } ?>><?
                foreach($specsOptions as $lvl => $v) {
                    ?><select class="spec-select" data-lvl="<?= $lvl ?>" style="margin-right: 5px;<? if(empty($v['categories'])){ ?>display: none; <? } ?>" onchange="jOrdersOrdersForm.onSpecialization($(this))"><?= $v['categories'] ?></select><?
                }
                ?></div><div id="j-specs-selected" class="hide left"></div>
                <? if( $bAddSpecs ) { ?><a href="#" onclick="$('#j-specs-select-block').show(); $(this).hide(); return false;" class="left btn btn-mini" style="margin-left: 3px;">+ добавить</a><? } ?>
                <div class="clearfix"></div>
            </td>
        </tr>
        <tr class="check-select <?= $typeClass(Orders::TYPE_PRODUCT) ?>">
            <td class="row1 field-title"><?= _t('orders', 'Категории') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <? $bAddCats = ! empty($cats) && count($cats) < Orders::catsLimit(); ?>
                <div id="j-cats-select-block" <? if( $bAddCats ) { ?> style="display: none;"<? } ?>><?
                foreach($catsOptions as $lvl => $v) {
                    ?><select class="cat-select" style="margin-right: 5px;<? if(empty($v['categories'])){ ?>display: none; <? } ?>" onchange="jOrdersOrdersForm.onCategory($(this))"><?= $v['categories'] ?></select><?
                }
                ?></div><div id="j-cats-selected" class="hide left"></div>
                <? if( $bAddCats ) { ?><a href="#" onclick="$('#j-cats-select-block').show(); $(this).hide(); return false;" class="left btn btn-mini" style="margin-left: 3px;"><?= _t('', '+ add') ?></a><? } ?>
                <div class="clearfix"></div>
            </td>
        </tr>
        <tr class="check-select <?= $typeClass(Orders::TYPE_SERVICE) ?>">
            <td class="row1 field-title"><?= _t('orders', 'Тип услуги') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <?  if( ! array_key_exists($service_type, $service_types)){
                        $service_type = reset($service_types);
                        $service_type = $service_type['id'];
                    }
                    foreach($service_types as $v): ?>
                <label class="inline radio"><input type="radio" name="service_type" value="<?= $v['id'] ?>" <?= ($service_type == $v['id'] ? 'checked="checked"' : '') ?>><?= $v['t'] ?></label>
                <? endforeach; ?>
            </td>
        </tr>
        <tbody id="orders-dynprops"><?= (!empty($dp) ? $dp : '') ?></tbody>
        <tr class="check-select <?= $typeClass(Orders::TYPE_SERVICE) ?>">
            <td class="row1 field-title"><?= _t('orders', 'Навыки') ?></td>
            <td class="row2">
                <?= $this->orderTags()->tagsForm($id, $this->adminLink('ajax&act=tags-suggest', bff::$class), 715) ?>
            </td>
        </tr>
        <tr id="order-city-block">
            <td class="row1 field-title"><?= _t('orders', 'Регион') ?></td>
            <td class="row2">
                <? $bAddRegions = ( ! empty($regions) && count($regions) < Orders::regionsLimit() ); ?>
                <div id="j-region-select-block" <? if( $bAddRegions ) { ?> style="display: none;"<? } ?>>
                <? if(Geo::countrySelect()): ?>
                    <div id="j-country-input-block" class="left" style="margin-right:5px;">
                        <select id="j-order-country"
                                disabled="disabled"
                                onchange="jOrdersOrdersForm.onCountry($(this));">
                            <?= HTML::selectOptions(Geo::countryList(), Geo::defaultCountry(), _t('', 'Select'), 'id', 'title') ?></select>
                    </div>
                <? endif; ?>
                    <input type="hidden" id="order-city-id" />
                    <div class="relative left"
                         id="j-region-input-block"
                         style="margin-bottom: 3px;">
                        <input type="text"
                               id="order-city-ac"
                               class="autocomplete"
                               <?= (Geo::defaultCity() ?
                                   'value="' . Geo::regionTitle(Geo::defaultCity()) .
                                   '" disabled="disabled"' :
                                   ''); ?>
                               placeholder="<?= _t('orders', 'Введите название региона') ?>"
                               style="width: 220px;" />
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="j-regions-selected" class="hide left"></div>
                <? if( $bAddRegions ) : ?>
                    <a href="#"
                       onclick="$('#j-region-select-block').show(); $(this).hide(); return false;"
                       class="left btn btn-mini"
                       style="margin-left: 3px;">
                    <?= _t('', '+ add') ?>
                    </a>
                <? endif; ?>
                <div class="clearfix"></div>
            </td>
        </tr>
        <tr id="order-district-block">
            <td class="row1 field-title"><?= _t('district', 'Район города') ?></td>
            <td class="row2">
                <select class="form-control"
                        id="j-order-distrit"
                        name="district_id">
                        <?= HTML::selectOptions(Geo::districtList(Geo::defaultCity()),
                                                (!empty($district_id) ? $district_id : 0),
                                                _t('', 'Выбрать'),
                                                'id',
                                                'title') ?>
                </select>
                <div class="clearfix"></div>
            </td>
        </tr>
        <? if($bMapEnabled): ?>
        <tr id="j-addr-block" <?= empty($regions) ? 'class="hidden"' : '' ?>>
            <td class="row1 field-title"><?= _t('orders', 'Адрес') ?></td>
            <td class="row2">
                <?  $bHideAddr = ($edit && ! empty($addr_addr));
                    if( $bHideAddr ): ?>
                    <div class="j-hide-addr">
                        <?= $addr_addr ?><span class="desc"> - </span><a href="#" class="desc ajax" onclick="return jOrdersOrdersForm.onShowAddr();"><?= _t('', 'change') ?></a>
                    </div>
                <? endif; ?>
                <div id="order-addr" class="<?= $bHideAddr ? 'hidden' : '' ?> j-addr">
                    <input type="text" name="addr_addr" id="order-addr_addr" value="<?= $addr_addr ?>" style="width: 300px;" maxlength="400" />
                    <a href="#" class="ajax" onclick="jOrdersOrdersForm.onMapSearch(); return false;"><?= _t('orders', 'найти адрес') ?></a>
                    <div id="order-addr-map" class="map-google" style="width: 620px; height: 260px; margin-top: 5px;"></div>
                </div>
            </td>
        </tr>
        <? endif; ?>
        <tbody class="<?= $typeClass(Orders::TYPE_SERVICE) ?>" id="j-order-price"><?= (!empty($priceForm) ? $priceForm : '') ?></tbody>
        <tr>
            <td class="row1 field-title"><?= _t('orders', 'Только для PRO') ?></td>
            <td class="row2">
                <input type="checkbox" name="pro" value="1" <?= ($pro ? ' checked="checked"' : '') ?> />
            </td>
        </tr>
        <tr>
            <td class="row1 field-title"><?= _t('orders', 'Это срочное объявление') ?></td>
            <td class="row2">
                <input type="checkbox" name="is_immediate" value="1" <?= ($is_immediate ? ' checked="checked"' : '') ?> />
            </td>
        </tr>
        <?php bff::hook('orders.admin.order.form', array('edit'=>$edit,'data'=>&$aData)); ?>
    <? if($edit): ?>
        <tr>
            <td class="row1 field-title"><?= _t('', 'User') ?></td>
            <td class="row2">
                <a href="#" class="ajax" onclick="return bff.userinfo(<?= $user_id ?>);"><?= $email ?></a>
            </td>
        </tr>
        <tr>
            <td class="row1 field-title"><?= _t('orders', 'Исполнитель') ?></td>
            <td class="row2">
                <? if( ! empty($performer_id)): ?>
                    <a href="#" class="ajax" onclick="return bff.userinfo(<?= $performer_id ?>);"><?= $performer_email ?></a> <?= _t('orders', 'назначен [date]', array('date'=>tpl::date_format3($performer_created))) ?>
                <? else: ?>
                    <?= _t('orders', 'Не назначен') ?>
                <? endif; ?>
            </td>
        </tr>
        <tr>
            <td class="row1" colspan="2">
                <? $aData['user'] = array('blocked'=>$user_blocked); ?>
                <?= $this->viewPHP($aData, 'admin.orders.form.status'); ?>
            </td>
        </tr>
    <? else: ?>
        <tr>
            <td class="row1 field-title"><?= _t('', 'User') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <input type="hidden" name="user_id" value="0" id="j-order-user-id" />
                <input type="text" name="email" value="" id="j-order-user-email" class="autocomplete" placeholder="<?= _t('', 'Enter user e-mail') ?>" style="width: 220px;" />
            </td>
        </tr>
    <? endif; ?>
        <tr class="footer">
            <td colspan="2">
                <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jOrdersOrdersForm.save(false);" />
                <? if($edit) { ?><input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jOrdersOrdersForm.save(true);" /><? } ?>
                <? if($edit) { ?><input type="button" onclick="jOrdersOrdersForm.del(); return false;" class="btn btn-danger button delete" value="<?= _t('', 'Delete') ?>" /><? } ?>
                <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jOrdersOrdersFormManager.action('cancel');" />
            </td>
        </tr>
    </table>
</form>
</div>

<? if(Orders::imagesLimit()): ?>
<div id="order-form-block-images" class="hidden"><?= $this->viewPHP($aData, 'admin.orders.form.images'); ?></div>
<? endif; ?>

<? if(Orders::attachmentsLimit()): ?>
    <div id="order-form-block-attachments" class="hidden"><?= $this->viewPHP($aData, 'admin.orders.form.attachments'); ?></div>
<? endif; ?>

<div id="order-form-block-offers" class="hidden"><?= $this->viewPHP($aData, 'admin.offers.listing.list'); ?></div>

<? if ($edit && bff::servicesEnabled()):?>
<div id="order-form-block-svc" class="hidden"><?= $this->viewPHP($aData, 'admin.orders.form.svc'); ?></div>
<? endif; ?>

<?php bff::hook('orders.admin.order.form.tabs.content', array('edit'=>$edit,'data'=>&$aData,'tabs'=>$aTabs)); ?>

<script type="text/javascript">
var jOrdersOrdersForm =
(function(){
    var $progress, $form, $dp, formChk, id = <?= $id ?>, blocksPrefix = 'order-form-block-';
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';
    var catCache = {}, specCache = {};
    var $types, $specsSelect, $specsSelected, $catsSelect, $catsSelected, $regionsSelect, $regionsSelected, $regionBlock, $addrBlock, $country = false;
    var city = {$block:0,$ac:0, acApi:0, data:{}};
    var addr = {$block:0, i:false,addr:0,lat:0,lng:0, mapEditor:0};
    var specPriceCache = {}, specDpCache = {}, specCnt = 1;
    var $specPriceBlock, nSpecID = <?= ! empty($spec['spec_id']) ? $spec['spec_id'] : 0 ?>;
<?
    $aTermsText = array();
    foreach (Orders::aTerms() as $v) {
        $aTermsText[ $v['id'] ] = $v['date'];
    }
?>
    var terms = <?= func::php2js($aTermsText) ?>;

    $(function(){
        $progress = $('#OrdersOrdersFormProgress');
        $form = $('#OrdersOrdersForm');
        $dp = $form.find('#orders-dynprops');

        $types = $form.find('.j-type');
        $specsSelected = $('#j-specs-selected');
        $catsSelected = $('#j-cats-selected');
        $regionsSelected = $('#j-regions-selected');
        $regionBlock = $('#j-region-input-block');
        $country = $('#j-order-country');
        $specsSelect = $('#j-specs-select-block');
        $catsSelect = $('#j-cats-select-block');
        $regionsSelect = $('#j-region-select-block');
        $addrBlock = $('#j-addr-block');
        $specPriceBlock = $('#j-order-price');

        $catsSelected.on('click', '.j-delete', function(){
            $(this).closest('.j-selected').remove();
            if($catsSelected.find('.j-cat-id').length < <?= Orders::catsLimit() ?>){
                $catsSelect.show();
            }
            setDp(<?= Orders::TYPE_PRODUCT ?>);
            return false;
        });

        $specsSelected.on('click', '.j-delete', function(){
            $(this).closest('.j-selected').remove();
            if($specsSelected.find('.j-spec-id').length < <?= Orders::specsLimit() ?>){
                $specsSelect.show();
            }
            setPrice();
            setDp(<?= Orders::TYPE_SERVICE ?>);
            return false;
        });

        $regionsSelected.on('click', '.j-delete', function(){
            $(this).closest('.j-selected').remove();
            var cnt = $regionsSelected.find('.j-region-id').length;
            if(cnt < <?= Orders::regionsLimit() ?>){
                $regionsSelect.show();
            }
            if( ! cnt){
                $addrBlock.hide();
            }
            return false;
        });

        city.$block = $form.find('#order-city-block');
        city.$ac = $form.find('#order-city-ac').autocomplete('<?= $this->adminLink('regionSuggest', 'geo') ?>',
            {valueInput: $form.find('#order-city-id'), params:{country:<?= Geo::defaultCountry() ?>}, suggest: <?= Geo::regionPreSuggest(Geo::defaultCountry()) ?>,
                onSelect: function(cityID, cityTitle, ex){
                    if( ! ex.changed ) return;
                    regionSelected(cityID, cityTitle, $country ? $country.find(':selected').text() : false);
                    city.acApi.reset();
                    <? if($bMapEnabled): ?>
                    // map
                    addr.$block.show();
                    if(ex.title.length > 0) {
                        jOrdersOrdersForm.onMapSearch();
                    }
                    if(cityID){
                        initAddr();
                    }
                    <? endif; ?>
                }}, function(){ city.acApi = this; });

        addr.$block = $form.find('#order-addr');
        initBlock('<?= $tab ?>', false);

        <? if( ! $edit): ?>
        $form.find('#j-order-user-email').autocomplete(ajaxUrl+'&act=user',
            {valueInput: $form.find('#j-order-user-id')});
        setPrice();
        <? endif; ?>

        <? if($edit || ! empty($regions)):
            if( ! empty($cats)): foreach($cats as $v):?>catSelected(<?= $v['cat_id']?>, '<?= HTML::escape($v['title'], 'js') ?>');<? endforeach; endif;
            if( ! empty($specs)): foreach($specs as $v): ?>specSelected(false, <?= $v['spec_id']?>, <?= func::php2js($v)?>);<? endforeach; endif;
            if( ! empty($regions)):
                foreach($regions as $v):?>
                regionSelected(<?= $v['reg3_city']?>,
                               '<?= HTML::escape($v['title'], 'js') ?>',
                                <?= Geo::countrySelect() ? '\''.HTML::escape($v['country_title'], 'js').'\'': 'false' ?>);
                <? endforeach;
             endif;
        endif; ?>
    });

    function catView(data, $select)
    {
        if(data === 'empty') {
            setDp(<?= Orders::TYPE_PRODUCT ?>);
            return;
        }

        if(data.subs>0) {
            $select.after('<select class="cat-select" style="margin-right: 5px;" onchange="jOrdersOrdersForm.onCategory($(this))">'+data.cats+'</select>').show();
            return;
        } else {
            if(data.id){
                var title = '';
                $select.siblings().each(function(){
                    if(title.length) title = title + ' / ';
                    title = title + $(this).find(':selected').text();
                });
                if(title.length) title = title + ' / ';
                title = title + $select.find(':selected').text();
                catSelected(data.id, title);
            }
        }
        setDp(<?= Orders::TYPE_PRODUCT ?>);
        formChk.check(false, true);
    }

    function catSelected(id, title)
    {
        if( id > 0 && ! $catsSelected.find('.j-cat-id[value="'+id+'"]').length ){
            $catsSelected.append(
                '<span class="label j-selected" style="margin:0 2px 2px 2px;">'+title+'<a href="#" class="j-delete" style="margin-left: 3px;"><i class="icon-remove icon-white" style="margin-top: 0px;"></i></a><input type="hidden" name="cats[]" class="j-cat-id" value="'+id+'" /></span>'
            ).removeClass('hide');
            if($catsSelected.find('.j-cat-id').length >= <?= Orders::catsLimit() ?>){
                $catsSelect.hide();
            }
        }
    }

    function specSelected($select, specID, oSpec)
    {
        if(specID <= 0){
            setDp(<?= Orders::TYPE_SERVICE ?>);
            return;
        }
        var $exist = $specsSelected.find('.j-spec-spec-id[value="'+specID+'"]');
        if($exist.length){
            $exist.closest('.j-selected').remove();
        }

        var title = '';
        var inputs = '';
        if($select && $select.length){
            var $s = $select.parent().find('select');
            $s.each(function(){
                var $el = $(this);
                var id = intval($el.val());
                inputs += '<input type="hidden" name="specs['+specCnt+']['+$el.data('lvl')+']" class="j-spec-'+$el.data('lvl')+'-id" value="'+id+'" />';

                if(id){
                    if(title.length) title += ' / ';
                    title += $el.find(':selected').text();
                }
            });
        } else {
            if(oSpec.hasOwnProperty('cat_title')){
                title = oSpec.cat_title + ' / ';
            }
            title += oSpec.spec_title;
            if(oSpec.hasOwnProperty('lvl')){
                inputs = '<input type="hidden" name="specs['+specCnt+']['+oSpec.lvl+']" class="j-spec-'+oSpec.lvl+'-id" value="'+oSpec.cat_id+'" />';
            }
            inputs += '<input type="hidden" name="specs['+specCnt+'][spec]" class="j-spec-spec-id" value="'+oSpec.spec_id+'" />';
        }

        $specsSelected.append(
            '<span class="label j-selected" style="margin:0 2px 2px 2px;">'+title+'<a href="#" class="j-delete" style="margin-left: 3px;"><i class="icon-remove icon-white" style="margin-top: 0px;"></i></a>'+inputs+'</span>'
        ).removeClass('hide');
        if($specsSelected.find('.j-spec-spec-id').length >= <?= Orders::specsLimit() ?>){
            $specsSelect.hide();
        }
        if($select && $select.length){
            setPrice();
            setDp(<?= Orders::TYPE_SERVICE ?>);
        }
        specCnt++;
    }

    function setPrice()
    {
        <? if( ! $edit): ?>
        var t = intval($form.find('[name="type"]:checked').val());
        if( t != <?= Orders::TYPE_SERVICE ?>){
            return;
        }
        <? endif; ?>

        var $specFirst = $specsSelected.find('.j-selected:first');
        var spec_id = 0;
        if($specFirst.length){
            spec_id = intval($specFirst.find('.j-spec-spec-id').val());
        }
        if(specPriceCache.hasOwnProperty(spec_id)){
            $specPriceBlock.html(specPriceCache[spec_id]);
            nSpecID = spec_id;
        } else {
            bff.ajax('<?= $this->adminLink('orders_list&act=price-block'); ?>', {spec_id: spec_id}, function(data){
                if(data && data.success) {
                    $specPriceBlock.html(specPriceCache[spec_id] = data.html);
                    nSpecID = spec_id;
                }
            });
        }
    }

    function setDp(type)
    {
        var $bEmpty = true;
        if(type == <?= Orders::TYPE_SERVICE ?> ){
            var $specFirst = $specsSelected.find('.j-selected:first');
            if($specFirst.length){
                var spec_id = intval($specFirst.find('.j-spec-spec-id').val());
                if(spec_id){
                    $bEmpty = false;
                    if(specDpCache.hasOwnProperty(spec_id)){
                        $dp.html(specDpCache[spec_id]);
                    } else {
                        bff.ajax('<?= $this->adminLink('specializations_list&act=dp-data&module=orders', 'specializations'); ?>', {spec_id: spec_id}, function(data){
                            if(data && data.dp) {
                                $dp.html(specDpCache[spec_id] = data.dp);
                            }
                        });
                    }
                }
            }
        }
        if(type == <?= Orders::TYPE_PRODUCT ?> ){
            var $catFirst = $catsSelected.find('.j-selected');
            if($catFirst.length){
                var cat_id = intval($catFirst.find('.j-cat-id').val());
                if(cat_id){
                    if(catCache.hasOwnProperty(cat_id)) {
                        if(catCache[cat_id].hasOwnProperty('dp')){
                            $bEmpty = false;
                            $dp.html(catCache[cat_id].dp);
                        }
                    }
                }
            }
        }
        if($bEmpty){
            $dp.html('');
        }
    }

    function regionSelected(region_id, title, country)
    {
        if(region_id > 0 && ! $regionsSelected.find('.j-region-id[value="'+region_id+'"]').length ){
            $regionsSelected.append(
                '<span class="label j-selected" style="margin:0 2px 2px 2px;" data-country="'+country+'" data-city="'+title+'">'+(country ? country + ' / ' : '') + title +'<a href="#" class="j-delete" style="margin-left: 3px;"><i class="icon-remove icon-white" style="margin-top: 0px;"></i></a><input type="hidden" name="regions[]" class="j-region-id" value="'+region_id+'" /></span>'
            ).removeClass('hide');
            if($regionsSelected.find('.j-region-id').length >= <?= Orders::regionsLimit() ?>){
                $regionsSelect.hide();
            }
        }
    }

    function initAddr()
    {
        $addrBlock.show();
        $form.find('.j-hide-addr').remove();

        if( addr.i === true ) {
            addr.map.refresh();
            return;
        }

        addr.addr = $form.find('#order-addr_addr');
        addr.lat  = $form.find('#order-addr_lat');
        addr.lng  = $form.find('#order-addr_lng');
        addr.i    = true;

        addr.map = bff.map.init('order-addr-map', [addr.lat.val(), addr.lng.val()], function(map){
            addr.mapEditor = bff.map.editor();
            addr.mapEditor.init({
                map: map, version: '2.1',
                coords: [addr.lat, addr.lng],
                address: addr.addr,
                addressKind: 'house',
                updateAddressIgnoreClass: 'typed'
            });

            addr.addr.bind('change keyup input', $.debounce(function(){
                if( ! $.trim(addr.addr.val()).length ) {
                    addr.addr.removeClass('typed');
                } else {
                    addr.addr.addClass('typed');
                    jOrdersOrdersForm.onMapSearch();
                }
            }, 700));
            jOrdersOrdersForm.onMapSearch();
        }, {zoom:12});

    }

    function initBlock(key, $block)
    {
        if($block === false) {
            $block = $('#'+blocksPrefix+key);
        }
        $block.addClass('inited').removeClass('hidden');
    }

    return {
        del: function()
        {
            if( id > 0 ) {
                bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                    false, {progress: $progress, repaint: false, onComplete:function(){
                        bff.success('Запись успешно удалена');
                        jOrdersOrdersFormManager.action('cancel');
                        jOrdersOrdersList.refresh();
                    }});
            }
        },
        save: function(returnToList)
        {
            if( ! formChk.check(true) ) return;
            var $bl = $types.filter(':visible');
            if($bl.find('.j-selected').length){
                $bl.removeClass('clr-error');
            } else {
                $bl.addClass('clr-error');
                return;
            }
            var data = $form.serialize();
            <? if( ! $edit): ?>
            if(typeof(jOrdersOrderImages) == 'object'){
                data = data + '&' + jOrdersOrderImages.serialize();
            }
            if(typeof(jOrdersOrderAttachments) == 'object'){
                data = data + '&' + jOrdersOrderAttachments.serialize();
            }
            <? endif; ?>

            bff.ajax(ajaxUrl, data, function(data){
                if(data && data.success) {
                    bff.success('Данные успешно сохранены');
                    if(returnToList || ! id) {
                        jOrdersOrdersFormManager.action('cancel');
                        jOrdersOrdersList.refresh( ! id);
                    }
                }
            }, $progress);
        },
        onShow: function()
        {
            formChk = new bff.formChecker($form);
            <? if($bMapEnabled && ! empty($regions)): ?>
            jOrdersOrdersForm.onShowAddr();
            <? endif; ?>
        },
        onCategory: function($select)
        {
            catView('empty');

            var catID = intval($select.val());
            $select.nextAll().remove();

            if( ! catID) return;

            if(catCache.hasOwnProperty(catID)) {
                catView( catCache[catID], $select );
            } else {
                bff.ajax('<?= $this->adminLink('products&act=category-data', 'shop'); ?>', {cat_id: catID}, function(data){
                    if(data && data.success) {
                        catView( (catCache[catID] = data), $select );
                    }
                }, function(){
                    $progress.toggle();
                });
            }
        },
        onSpecialization: function($select)
        {
            function specView(data, $select)
            {
                if(data.subs>0) {
                    $select.after('<select class="spec-select" data-lvl="'+data.lvl+'" style="margin-right: 5px;" onchange="jOrdersOrdersForm.onSpecialization($(this))">'+data.cats+'</select>').show();
                }
            }
            var catID = intval($select.val());
            var lvl = $select.data('lvl');
            if(lvl == 'spec'){
                specSelected($select, catID);
            } else {
                $select.nextAll().remove();

                if(specCache.hasOwnProperty(catID)) {
                    specView( specCache[catID], $select );
                } else {
                    bff.ajax('<?= $this->adminLink('specializations_list&act=category-data&noAllSpec=1', 'specializations'); ?>', {cat_id: catID, lvl:$select.data('lvl')}, function(data){
                        if(data && data.success) {
                            specView( (specCache[catID] = data), $select );
                        }
                    });
                }
                setDp(<?= Orders::TYPE_SERVICE ?>);
            }
        },
        onType:function($type)
        {
            var v = intval($type.val());
            $types.addClass('hidden');
            $types.filter('.j-type-' + v).removeClass('hidden');
            setPrice();
            setDp(v);
            formChk.check(false, true);
        },
        onCountry:function($el)
        {
            var country = intval($el.val());
            if(country){
                $regionBlock.show();
                city.acApi.setParam('country', country);
                if( city.data.hasOwnProperty(country) ) {
                    city.acApi.setSuggest(city.data[country], true);
                } else {
                    bff.ajax('<?= $this->adminLink('ajax&act=country-presuggest', 'geo') ?>', {country:country}, function(data){
                        city.data[country] = data;
                        city.acApi.setSuggest(data, true);
                    });
                }
            } else {
                $regionBlock.hide();

            }
        },
        <? if($bMapEnabled): ?>
        onMapSearch: function()
        {
            if( ! addr.mapEditor) return false;
            var $fst = $regionsSelected.find('.j-selected:first');
            var q = [];
            <? if( ! Geo::countrySelect()){ ?>q.push( '<?= HTML::escape(Geo::regionTitle(Geo::defaultCountry()), 'js') ?>' );<? }else{
            ?>q.push( $.trim( $fst.data('country')) );<? } ?>
            var q_city = $.trim( $fst.data('city') ); if( q_city.length ) q.push( q_city );
            var q_addr = $.trim( addr.addr.val() ); if( q_addr.length ) q.push( q_addr );
            q = q.join(', '); if( addr.lastQuery == q ) return false;
            addr.mapEditor.search( addr.lastQuery = q, false, function(){
                addr.mapEditor.centerByMarker();
            } );

            return false;
        },
        onShowAddr: function()
        {
            $form.find('.j-addr').removeClass('hidden');
            initAddr();
            return false;
        },
        <? endif; ?>
        onTab: function(key, tabLink)
        {
            $('[id^="'+blocksPrefix+'"]').addClass('hidden');
            var $block = $('#'+blocksPrefix+key).removeClass('hidden');
            if( ! $block.hasClass('inited')) {
                initBlock(key, $block);
            }
            $(tabLink).parent().addClass('tab-active').siblings().removeClass('tab-active');
            if(bff.h) {
                window.history.pushState({}, document.title, ajaxUrl + '&act=<?= $act ?>&id=<?= $id ?>&ftab=' + key);
            }
        },
        onTerm: function($select)
        {
            id = $select.val();
            var $t = $form.find('.j-term-title');
            if(terms.hasOwnProperty(id)){
                if(terms[id]){
                    $t.html(terms[id]);
                    $t.parent().show();
                }else{
                    $t.parent().hide();
                }
            }else{
                $t.parent().hide();
            }
        },
        onTermChange: function($el)
        {
            $el.prev().remove();
            $el.prev().show().prev().show();
            $el.remove();
            $form.append('<input type="hidden" name="set_term" value="1" />');
            $form.find('[name="term"]').trigger('change');
        }
    };
}());

</script>