<?php
$aTabs = bff::filter('orders.admin.settings.tabs', array(
    'general'  => 'Общие',
    'social'  => 'Поделиться',
    'images'  => 'Изображения',
));

$aUsersTypes = Users::aTypes();
?>
<?= tplAdmin::blockStart(_t('orders','Orders').' / '._t('','Settings'), false, array('id'=>'OrdersSettingsFormBlock')); ?>

<form name="OrdersSettingsForm" id="OrdersSettingsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="edit" />
    <input type="hidden" name="save" value="1" />
    <div class="tabsBar" id="OrdersSettingsFormTabs">
        <? foreach($aTabs as $k=>$v) { ?>
            <span class="tab<? if($k == 'general') { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
        <? } ?>
        <div id="OrdersSettingsFormProgress" class="progress" style="display: none;"></div>
        <div class="clear"></div>
    </div>
    <div class="j-tab j-tab-general">
        <table class="admtbl tbledit">
            <tr>
                <td class="row1" width="300"><span class="field-title">Отображать ссылку на профиль заказчика на странице заказа:</span></td>
                <td class="row2">
                    <label class="radio"><input type="radio" name="contacts_view" value="<?= Orders::ORDERS_CONTACTS_VIEW_ALL ?>"  <?= $contacts_view == Orders::ORDERS_CONTACTS_VIEW_ALL ?  'checked="checked"' : '' ?>>всем</label>
                    <label class="radio"><input type="radio" name="contacts_view" value="<?= Orders::ORDERS_CONTACTS_VIEW_AUTH ?>" <?= $contacts_view == Orders::ORDERS_CONTACTS_VIEW_AUTH ? 'checked="checked"' : '' ?>>авторизованным</label>
                    <label class="radio"><input type="radio" name="contacts_view" value="<?= Orders::ORDERS_CONTACTS_VIEW_PRO ?>"  <?= $contacts_view == Orders::ORDERS_CONTACTS_VIEW_PRO ?  'checked="checked"' : '' ?>>авторизованным с платным аккаунтом</label>
                </td>
            </tr>
            <tr>
                <td class="row1"><span class="field-title">Количество заявок на заказы (в день) для <b>PRO</b>:</span></td>
                <td class="row2">
                    <input type="number" min="1" step="1" class="short" name="offers_limit_pro" value="<?= $offers_limit_pro ?>">
                </td>
            </tr>
            <tr>
                <td class="row1"><span class="field-title">Количество заявок на заказы (в день) для <b>не PRO</b>:</span></td>
                <td class="row2">
                    <input  type="number" min="1" step="1" class="short" name="offers_limit" value="<?= $offers_limit ?>">
                </td>
            </tr>
            <tr>
                <td colspan="2"><hr /></td>
            </tr>
            <tr>
                <td class="row1"><span class="field-title">Рассылка уведомлений о новых заказах<? if(sizeof($aUsersTypes) > 1){ ?> (исполнителям)<? } ?>:</span></td>
                <td class="row2">
                    <div>
                        <select name="enotify_neworders_type" style="width: 150px;" class="left">
                            <?= HTML::selectOptions(array(
                                Orders::ENOTIFY_NEWORDERS_NEVER => 'отключить',
                                Orders::ENOTIFY_NEWORDERS_NOW   => 'мгновенная',
                                Orders::ENOTIFY_NEWORDERS_HOURS => 'каждые X часов',
                                Orders::ENOTIFY_NEWORDERS_DAY   => 'раз в сутки',
                            ), $enotify_neworders_type); ?>
                        </select>
                        <div class="left" style="margin-left: 10px;">
                            <div class="j-enotify-neworders-type j-enotify-neworders-type-<?= Orders::ENOTIFY_NEWORDERS_DAY ?>" <?= $enotify_neworders_type != Orders::ENOTIFY_NEWORDERS_DAY ? 'style="display:none;"' : '' ?>>
                                начиная с <input type="time" name="enotify_neworders_begin" class="short" value="<?= $enotify_neworders_begin ?>" /> часов
                            </div>
                            <div class="j-enotify-neworders-type j-enotify-neworders-type-<?= Orders::ENOTIFY_NEWORDERS_HOURS ?>" <?= $enotify_neworders_type != Orders::ENOTIFY_NEWORDERS_HOURS ? 'style="display:none;"' : '' ?>>
                                каждые <input type="number" min="1" max="12" maxlength="2" name="enotify_neworders_interval" value="<?= $enotify_neworders_interval ?>" style="width: 30px;" /> часа(-ов)
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="j-enotify-neworders-type-receivers"<? if($enotify_neworders_type == Orders::ENOTIFY_NEWORDERS_NEVER) { ?> style="display: none;" <? } ?>>
                        <label class="radio"><input type="radio" name="enotify_neworders_user" value="<?= Orders::ENOTIFY_RECEIVERS_ALL ?>" <?= ($enotify_neworders_user == Orders::ENOTIFY_RECEIVERS_ALL ? 'checked="checked"' : '') ?>>всем</label>
                        <label class="radio"><input type="radio" name="enotify_neworders_user" value="<?= Orders::ENOTIFY_RECEIVERS_PRO ?>" <?= ($enotify_neworders_user == Orders::ENOTIFY_RECEIVERS_PRO ? 'checked="checked"' : '') ?>>только платным аккаунтам</label>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div class="j-tab j-tab-social hidden">
        <table class="admtbl tbledit">
            <tr>
                <td class="row1"><span class="field-title">Блок поделиться:</span></td>
            </tr>
            <tr>
                <td class="row2">
                    <textarea name="share_code" rows="10"><?= ! empty($aData['share_code']) ? HTML::escape($aData['share_code']) : '' ?></textarea>
                </td>
            </tr>
        </table>
    </div>

    <div class="j-tab j-tab-images hidden">
        <table class="admtbl tbledit">
            <tr>
                <td class="row1" width="350"><span class="field-title"><b><?= _t('orders','Orders') ?></b></span></td>
                <td class="row2">
                </td>
            </tr>
            <tr>
                <td class="row1"><span class="field-title">Минимально допустимый размер изображения по ширине:</span></td>
                <td class="row2">
                    <input type="text" class="short" name="orders_images_width_min" value="<?= $orders_images_width_min ?>"> px
                </td>
            </tr>
            <tr>
                <td class="row1"><span class="field-title">Максимально допустимый размер изображения по ширине:</span></td>
                <td class="row2">
                    <input type="text" class="short" name="orders_images_width_max" value="<?= $orders_images_width_max ?>"> px
                </td>
            </tr>
            <tr>
                <td class="row1"><span class="field-title">Минимально допустимый размер изображения по высоте:</span></td>
                <td class="row2">
                    <input type="text" class="short" name="orders_images_height_min" value="<?= $orders_images_height_min ?>"> px
                </td>
            </tr>
            <tr>
                <td class="row1"><span class="field-title">Максимально допустимый размер изображения по высоте:</span></td>
                <td class="row2">
                    <input type="text" class="short" name="orders_images_height_max" value="<?= $orders_images_height_max ?>"> px
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr class="cut">
                </td>
            </tr>
            <tr>
                <td class="row1" width="350"><span class="field-title"><b>Заявки</b></span></td>
                <td class="row2">
                </td>
            </tr>
            <tr>
                <td class="row1"><span class="field-title">Минимально допустимый размер изображения по ширине:</span></td>
                <td class="row2">
                    <input type="text" class="short" name="offers_images_width_min" value="<?= $offers_images_width_min ?>"> px
                </td>
            </tr>
            <tr>
                <td class="row1"><span class="field-title">Максимально допустимый размер изображения по ширине:</span></td>
                <td class="row2">
                    <input type="text" class="short" name="offers_images_width_max" value="<?= $offers_images_width_max ?>"> px
                </td>
            </tr>
            <tr>
                <td class="row1"><span class="field-title">Минимально допустимый размер изображения по высоте:</span></td>
                <td class="row2">
                    <input type="text" class="short" name="offers_images_height_min" value="<?= $offers_images_height_min ?>"> px
                </td>
            </tr>
            <tr>
                <td class="row1"><span class="field-title">Максимально допустимый размер изображения по высоте:</span></td>
                <td class="row2">
                    <input type="text" class="short" name="offers_images_height_max" value="<?= $offers_images_height_max ?>"> px
                </td>
            </tr>

        </table>
    </div>

    <? bff::hook('orders.admin.settings.tabs.content', array('data'=>&$aData)); ?>

    <div style="margin-top: 10px;">
        <input type="submit" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jOrdersSettingsForm.save(false);" />
    </div>

</form>
<?= tplAdmin::blockStop(); ?>

<script type="text/javascript">
    var jOrdersSettingsForm =
        (function(){
            var $progress, $form, f;
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $progress = $('#OrdersSettingsFormProgress');
                $form = $('#OrdersSettingsForm');
                f = $form.get(0);

                // tabs
                $form.find('#OrdersSettingsFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
                    var key = $(this).data('key');
                    $form.find('.j-tab').addClass('hidden');
                    $form.find('.j-tab-'+key).removeClass('hidden');
                    $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
                });

                $form.on('change', '[name="enotify_neworders_type"]', function(){
                    var v = intval($(this).val());
                    $form.find('.j-enotify-neworders-type').hide();
                    $form.find('.j-enotify-neworders-type-'+v).show();
                    var $receivers = $form.find('.j-enotify-neworders-type-receivers');
                    if (v == intval(<?= Orders::ENOTIFY_NEWORDERS_NEVER ?>)) {
                        $receivers.hide();
                    } else {
                        $receivers.show();
                    }
                });

                if (bff.bootstrapJS()) {
                    $form.find('.j-contacts-view-tooltip').tooltip();
                }

            });

            return {
                save: function()
                {
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('Данные успешно сохранены');
                        }
                    }, $progress);
                }
            };
        }());
</script>