<?php
    /** @var $attach OrdersOrderAttachments */
?>
<form method="post" action="" id="order-form-attachments">
    <div class="relative">
        <div class="left">
            <div class="desc">
                <?= _t('orders', 'Выберите файлы на Вашем компьютере.<br />Размер одного файла не должен превышать [maxsize].', array(
                    'maxsize' => $attach->getMaxSize(true)
                )) ?>
                <div id="order-attach-upload" style="height: 20px; width: 160px"><a href="javascript:void(0);" class="ajax"><?= _t('orders', 'Загрузить файлы') ?></a></div>
                <div id="order-attach-uploader" style="margin-top: 10px;"></div>
            </div>
        </div>
        <div class="clear"></div>
        <span id="progress-attachments" class="progress" style="display:none; position: absolute; right:0; top:5px;"></span>
    </div>

    <ul id="order-attachments" style="display: block; margin: 0;"></ul>
    <div class="clearfix"></div>

    <div class="noattach-hide" style="<? if($attachcnt<1){ ?>display:none;<? } ?>">
        <p class="desc" style="margin: 5px 0;"><?= _t('orders', 'перетяните файлы для изменения порядка') ?><strong>&nbsp;&nbsp;&harr;</strong></p>
        <div>
            <? if($edit) { ?>
                <input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save order') ?>" onclick="jOrdersOrderAttachments.save();" />
                <input type="button" class="btn btn-danger delete button submit" style="display: none;" onclick="if(confirm('<?= _t('orders', 'Удалить все файлы?') ?>')) jOrdersOrderAttachments.delAll(true, []);" value="<?= _t('orders', 'Удалить все файлы') ?>" />
            <? } ?>
        </div>
    </div>

</form>

<script type="text/javascript">
var jOrdersOrderAttachments = (function(){
    var url = '<?= $this->adminLink(bff::$event); ?>&id=<?= $id ?>&act=';
    var $form, $progress, uploader, $attach;

    $(function(){
        $form = $('#order-form-attachments');
        $progress = $('#progress-attachments');
        $attach = $('#order-attachments', $form);

        // init uploader
        uploader = new qq.FileUploaderBasic({
            button: $('#order-attach-upload', $form).get(0),
            action: url+'attach-upload',
            limit: <?= $attach->getLimit(); ?>,
            uploaded: <?= $attachcnt; ?>,
            multiple: true,
            onSubmit: function(id, fileName) {
                $progress.show();
            },
            onComplete: function(id, fileName, data) {
                if(data && data.success) {
                    onAttachUpload(data);
                } else {
                    if(data.errors) {
                        bff.error( data.errors );
                    }
                }
                if( ! uploader.getInProgress()) {
                    $progress.hide();
                }
                return true;
            }
        });

        initRotate(false);
        <? if( ! $edit) { ?>
        $(window).bind('beforeunload', function(){
            var $fn = $attach.find('input.attachfn');
            if($fn.length > 0) {
                var fn = [];
                $fn.each(function(){
                    fn.push($(this).val());
                });
                jOrdersOrderAttachments.delAll(false, fn);
            }
        });
        <? } ?>

        <? if( ! empty($attachments)): foreach($attachments as $v): $v['url'] = $attach->getURL($v, false); $v['size'] = tpl::filesize($v['size']); ?>
            onAttachUpload(<?= func::php2js($v); ?>);
        <? endforeach; endif; ?>

    });

    function initRotate(update)
    {
        if(update === true) {
            $attach.sortable('refresh');
            onCountChanged();
        } else {
            $attach.sortable();
        }
    }

    function onAttachUpload(data)
    {
        var attachID = data.id;

        if(uploader.getUploaded() == 0) $attach.find('li').remove();

        $attach.append('<li class="j-attach j-attach'+attachID+'">\
            <input type="hidden" class="attachfn" name="attach['+attachID+']" rel="'+attachID+'" value="'+(data.filename)+'" />\
            <a class="but cross" href="#" onclick="if(confirm(\'<?= _t('orders', 'Удалить файл?') ?>\')) return jOrdersOrderAttachments.del('+attachID+', \''+(data.filename)+'\'); return false;"></a>\
            <a href="'+data.url+'" target="_blank">'+data.origin+'</a> ('+data.size+')\
        </li>');
        initRotate(true);
    }

    function onCountChanged()
    {
        var cnt = $attach.find('li').length;
        if(cnt > 0) {
            $form.find('.noattach-hide').show();
        } else {
            $form.find('.noattach-hide').hide();
        }
    }

    return {
        del: function(attachID, imageFilename)
        {
            bff.ajax(url+'attach-delete',{attach_id: <? if($edit){ ?>attachID<? } else { ?>0<? } ?>, filename: imageFilename}, function(data){
                if(data && data.success) {
                    $form.find('li.j-attach'+attachID).remove();
                    uploader.decrementUploaded();
                    initRotate(true);
                }
            }, $progress);
            return false;
        },
        delAll: function(async, filenames)
        {
            bff.ajax(url+'attach-delete-all', {filenames: filenames}, function(data){
                if(data && data.success) {
                    $attach.empty();
                    uploader.resetUploaded();
                    initRotate(true);
                }
            }, $progress, {async: async});
            return false;
        },
        save: function()
        {
            bff.ajax(url+'attach-saveorder', $form.serialize(), function(data){
                if(data.success) {
                    bff.success('<?= _t('orders', 'Порядок успешно сохранен') ?>');
                }
            }, $progress);
        },
        serialize: function()
        {
            return $form.serialize();
        }
    }
}());
</script>