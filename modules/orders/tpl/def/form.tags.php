<?php
/**
 * Ввод навыков
 * @var $this UsersTags
 */

tpl::includeJS(array('autocomplete'), true);
tpl::includeJS('tags', false);
?>
<div class="form-group <?= ! empty($params['class']) ? $params['class'] : '' ?>" id="j-tags">
    <label for="j-tag-select" class="col-sm-3 control-label o-control-label"><?= _t('', 'Навыки'); ?></label>
    <div class="<?= ! empty($params['wightClass']) ? $params['wightClass'] : 'col-sm-9' ?>">
        <input type="hidden" id="j-tag-value" />
        <input type="hidden" name="tags_current" value="<? $tags_ksort = $tags; ksort($tags_ksort); echo HTML::escape(join(',',array_keys($tags_ksort))); ?>" />
        <input type="text" class="form-control" id="j-tag-select" placeholder="<?= _t('users', 'Начните вводить название навыка'); ?>" />
        <div class="o-tagSkills j-selected-tags"></div>
    </div>
</div>
<script type="text/javascript">
    <? js::start() ?>
    jTags.init(<?= func::php2js(array(
        'module' => bff::$class,
        'tags'   => ! empty($tags) ? $tags : array(),
    )) ?>);
    <? js::stop() ?>
</script>