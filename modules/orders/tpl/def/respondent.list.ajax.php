<?php

/**
 * Список заказов пользователя (только список)
 * @var $this Orders
 * @var $user array
 */

$aServiceTypes = Orders::aServiceTypes();
$aOfferStatuses = array(
    Orders::OFFER_STATUS_CANDIDATE => array(
        'id' => Orders::OFFER_STATUS_CANDIDATE,
        't'  => _t('orders', 'Вас выбрали кандидатом.'),
        'c'  => 'alert-candidate',
        'fa' => 'fa-check'
    ),
    Orders::OFFER_STATUS_PERFORMER => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER,
        't'  => _t('orders', 'Вас выбрали исполнителем.'),
        'c'  => 'alert-chosen',
        'fa' => 'fa-trophy text-orange'
    ),
    Orders::OFFER_STATUS_CANCELED  => array(
        'id'  => Orders::OFFER_STATUS_CANCELED,
        't'  => _t('orders', 'Вы получили отказ.'),
        'c'  => 'alert-decline',
        'fa' => 'fa-times'
    ),
    Orders::OFFER_STATUS_PERFORMER_START => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER_START,
        't'  => _t('orders', 'Вам предложили стать исполнителем.'),
        'c'  => 'alert-chosen',
        'fa' => 'fa-trophy text-orange'
    ),
    Orders::OFFER_STATUS_PERFORMER_DECLINE => array(
        'id'  => Orders::OFFER_STATUS_PERFORMER_DECLINE,
        't'  => _t('orders', 'Вы отказались от заказа.'),
        'c'  => 'alert-decline',
        'fa' => 'fa-times'
    ),
);
$userID = user::id();
$tagsLimit = Orders::searchTagsLimit();
$ordersOpinions = Orders::ordersOpinions();
$fairplayEnabled = bff::fairplayEnabled();
if( ! empty($list)): ?>
    <ul class="l-projectList">
        <? foreach($list as $v): ?>
        <li class="j-order" data-id="<?= $v['id'] ?>">
            <header class="l-project-title">
                <? if($v['my']): ?>
                    <a href="#" class="l-project-title-delete link-red ajax-link j-trash" data-id="<?= $v['offer_id'] ?>">
                        <i class="fa fa-trash-o c-link-icon"></i>
                        <span class="hidden-xs">
                            <?= $v['user_removed'] ? _t('orders', 'Из корзины') : _t('orders', 'В корзину') ?>
                        </span>
                    </a>
                <? endif; ?>
                <? if($v['visibility'] == Orders::VISIBILITY_PRIVATE): ?><i class="fa fa-eye-slash show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Приватный заказ'); ?>"></i><? endif; ?>
                <? if($v['status'] == Orders::STATUS_CLOSED): ?><i class="fa fa-lock show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('orders', 'Закрытый заказ'); ?>"></i><? endif; ?>
                <? if($fairplayEnabled && $v['fairplay']): ?><i class="fa fa-shield c-safe-color show-tooltip" data-original-title="<?= _t('fp', 'Безопасная сделка'); ?>" title="" data-placement="top" data-toggle="tooltip"></i><? endif; ?>
                <? if($v['pro']): ?><span class="pro show-tooltip" data-toggle="tooltip" data-placement="top" title="<?= _t('', 'Только для PRO'); ?>" data-original-title="<?= _t('', 'Только для PRO'); ?>">pro</span><? endif; ?>
                <a href="<?= $v['url_view'] ?>"><?= $v['title'] ?></a>
            </header>

            <div class="l-project-head">
                <? if($v['type'] == Orders::TYPE_SERVICE): ?>
                    <? if($v['price_ex'] == Specializations::PRICE_EX_AGREE): ?>
                        <span class="l-price_na"><?= _t('orders', 'По договоренности'); ?></span>
                    <? else: ?>
                        <span class="l-price"><?= tpl::formatPrice($v['price']); ?> <?= Site::currencyData($v['price_curr'], 'title_short'); ?> <? if( ! empty($v['price_rate_text'][LNG])): ?><small><?= $v['price_rate_text'][LNG] ?></small><? endif; ?></span>
                    <? endif; ?>
                <? endif; ?>
                <? if( ! empty($v['aTags'])): $n = 0; foreach($v['aTags'] as $vv):?>
                    <? if($tagsLimit && $n == $tagsLimit): ?><a class="l-tag l-tag-more j-tag-more" href="#"><?= _t('', 'еще ...'); ?></a><? endif; $n++; ?>
                    <a href="<?= Orders::url('search-tag', $vv) ?>" class="l-tag<?= $tagsLimit && $n > $tagsLimit ? ' hidden' : '' ?>"><?= $vv['tag'] ?></a>
                <? endforeach; endif; ?>
            </div>

            <? if( ! empty($aOfferStatuses[ $v['offer_status'] ])): $aStatus = $aOfferStatuses[ $v['offer_status'] ]; ?>
            <div class="alert <?= $aStatus['c'] ?> p-profile-alert">
                <? if( ! empty($aStatus['fa'])): ?><i class="fa <?= $aStatus['fa'] ?> pull-right"></i><? endif; ?>
                <?= $aStatus['t'] ?>
                <? if($ordersOpinions && $v['offer_status'] == Orders::OFFER_STATUS_PERFORMER_START): ?>
                    <div class="p-profile-alert-controls">
                        <a href="#" class="btn btn-success btn-sm j-performer-start-agree" data-id="<?= $v['offer_id'] ?>"><?= _t('orders', 'Подтвердить заказ'); ?></a>
                    </div>
                <? endif; ?>
            </div>
            <? endif; ?>
            <? if($ordersOpinions && $v['my'] && $v['offer_status'] == Orders::OFFER_STATUS_PERFORMER_AGREE):
                if($fairplayEnabled): ?>
                    <div class="alert alert-chosen o-freelancer-defined p-profile-alert">
                        <span><?= _t('orders', 'Вас выбрали исполнителем'); ?></span>
                        <? if(isset($v['workflow']['url'])): ?>
                            <div class="p-profile-alert-controls">
                                <a href="<?= $v['workflow']['url'] ?>" class="ajax-link"><i class="fa fa-rocket c-link-icon"></i><span><?= _t('fp', 'Ход работ'); ?></span></a>
                            </div>
                        <? endif; ?>
                    </div>
                <? else: ?>
                    <div class="alert alert-chosen o-freelancer-defined p-profile-alert">
                        <?  $meEnd = isset($v['opinions'][ $userID ]); if($meEnd) { $myOpinion = $v['opinions'][ $userID ]; }
                            $clientEnd = isset($v['opinions'][ $v['order_user_id'] ]); if($clientEnd) { $clientOpinion = $v['opinions'][ $v['order_user_id'] ]; }
                        ?>
                        <span><?= $meEnd ? _t('orders', 'Вы завершили заказ.')
                                         : ( $clientEnd ? _t('orders', 'Заказчик [link] завершил(а) заказ.', array('link' => tpl::userLink($clientOpinion, 'no-login no-verified no-pro')))
                                                        : _t('orders', 'Вас выбрали исполнителем')) ?></span>
                        <div class="p-profile-alert-controls">
                            <? if($clientEnd): ?><a href="#" class="ajax-link j-opinion-show" data-id="<?= $clientOpinion['id'] ?>"><span><?= _t('orders', 'Отзыв заказчика'); ?></span></a><? endif; ?>
                            <? if($meEnd): ?>
                                <a href="#" class="ajax-link j-opinion-show" data-id="<?= $myOpinion['id'] ?>"><span><?= _t('orders', 'Ваш отзыв'); ?></span></a>
                            <? else: ?>
                                <button type="button" class="btn btn-success btn-sm j-opinion-add" data-id="<?= $v['id'] ?>"><?= $clientEnd ? _t('orders', 'Оставить отзыв') : _t('orders', 'Завершить заказ'); ?></button>
                            <? endif; ?>
                        </div>
                    </div>
                <? endif;
            endif; ?>

            <article>
                <p><?= nl2br($v['descr']); ?></p>
            </article>

            <ul class="l-item-features">
                <? if( ! empty($v['city_data']['title'])): ?><li><i class="fa fa-map-marker"></i> <?= $v['city_data']['title']?></li><? endif; ?>
                <? if($v['type'] == Orders::TYPE_SERVICE):?><li><i class="fa fa-<?= $aServiceTypes[ $v['service_type'] ]['c'] ?>"></i> <?= $aServiceTypes[ $v['service_type'] ]['t'] ?></li><? endif; ?>
                <li><i class="fa fa-clock-o"></i> <?= tpl::date_format_spent($v['created'], false, true) ?></li>
                <li><a href="<?= $v['url_view'] ?>#offers"><i class="fa fa-comments-o c-link-icon"></i><span><?= tpl::declension($v['offers_cnt'], _t('orders', 'предложение;предложения;предложений'))?></span></a></li>
                <? if($v['my']): ?>
                    <li><a href="<?= $v['url_view_chat']?>"><i class="fa fa-comment c-link-icon"></i><span><?= tpl::declension($v['chatcnt'], _t('orders', 'сообщение;сообщения;сообщений'))?></span></a><?= $v['chat_new_worker'] ? ' <span class="label label-new">+'.$v['chat_new_worker'].'</span>' : '' ?></li>
                <? endif; ?>
                <li><?=_t('', 'Просмотров [cnt]', ['cnt' => $v['views_total']])?></li>
            </ul>

            <?/* todo-clazion: заметка по заказу пользователя */?>
            <?= $this->getOrderNote($v['id']);?>
        </li>
        <? endforeach; ?>
    </ul>
<? else: ?>
<div class="alert alert-info"><?= _t('orders', 'Заказы не найдены'); ?></div>
<? endif; ?>