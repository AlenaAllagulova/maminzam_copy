<?php
/**
 * Список заказов пользователя.
 * Пользователь = владелец заказов
 * @var $this Users
 */
    tpl::includeJS('orders.owner.list', false, 5);
    tpl::includeJS('orders.opinions', false, 2);
?>
    <div class="p-profileContent" id="j-orders-owner-list">

        <form method="get" action="" id="j-orders-owner-list-form">
            <input type="hidden" name="t" value="<?= $f['t'] ?>" />
            <input type="hidden" name="st" value="<?= $f['st'] ?>" />
            <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <div class="p-profile-title p-proflie-client-title">
            <div class="p-profile-title-columns">
                <div class="p-profile-title-columns-left">
                    <? if(Orders::useProducts()):
                        $aTypes = array('0' => array('id' => 0, 't' => _t('orders', 'Все заказы'))) + Orders::aTypes(); ?>
                    <div class="dropdown p-profileOrder-swicher">
                        <a href="#" class="ajax-link" data-toggle="dropdown"><span class="j-f-type-title"><?= $aTypes[ $f['t'] ]['t'] ?></span><b class="caret"></b></a>
                        <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                            <? foreach($aTypes as $v): ?>
                            <li class="<?= $f['t'] == $v['id'] ? 'active' : '' ?><?= empty($counts['type'][ $v['id'] ]) ? ' hidden' : '' ?>"><a href="#" class="j-f-type" data-id="<?= $v['id'] ?>"><?= $v['t'] ?> <small class="j-cnt-type-<?= $v['id'] ?>">(<?= $counts['type'][ $v['id'] ] ?>)</small></a></li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                    <? endif; ?>

                    <div class="dropdown visible-xs visible-sm">
                        <a href="#" class="ajax-link" data-toggle="dropdown"><span class="j-f-status-title"><?= $aStatus[ $f['st'] ]['t'] ?></span><b class="caret"></b></a>
                        <ul class="dropdown-menu c-dropdown-caret_left" role="menu">
                            <? foreach($aStatus as $v): ?>
                                <li class="<?= $f['st'] == $v['id'] ? 'active' : '' ?><?= empty($counts['status'][ $v['id'] ]) ? ' hidden' : '' ?>"><a href="#" class="j-f-status" data-id="<?= $v['id']?>"><?= $v['t'] ?> <small class="j-cnt-status-<?= $v['id'] ?>">(<?= $counts['status'][ $v['id'] ] ?>)</small></a></li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                    <div class="clearfix"></div>

                    <ul class="p-profile-submenu hidden-xs hidden-sm">
                        <? foreach($aStatus as $v): ?>
                            <li class="<?= $f['st'] == $v['id'] ? 'active' : '' ?><?= empty($counts['status'][ $v['id'] ]) ? ' hidden' : '' ?>"><a href="#" class="j-f-status" data-id="<?= $v['id']?>"><?= $v['t'] ?> <small class="j-cnt-status-<?= $v['id'] ?>">(<?= $counts['status'][ $v['id'] ] ?>)</small></a></li>
                        <? endforeach; ?>
                    </ul>
                </div>
                <? if( ! Users::useClient() || (Users::useClient() && User::isClient())): ?>
                    <div class="p-addOrder p-profile-title-columns-right">
                        <a href="<?= Orders::url('add')?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?= _t('orders', 'Добавить заказ'); ?></a>
                    </div>
                <? endif; ?>
            </div>
        </div>
        </form>

        <div class="j-list"><?= $list ?></div>

        <div class="j-pagination"><?= $pgn ?></div>

    </div>
<script type="text/javascript">
<? js::start() ?>
jOrdersOwnerList.init(<?= func::php2js(array(
    'lang'   => array(
        'order_delete' => _t('orders', 'Удалить заказ?'),
    ),
    'types'  => isset($aTypes) ? $aTypes : array(),
    'status' => $aStatus,
    'ordersOpinions' => Orders::ordersOpinions(),
    'ajax' => true,
)) ?>);
jOrdersOpinions.init(<?= func::php2js(array(
    'lang'   => array(
        'left'        => _t('users','[symbols] осталось'),
        'symbols'     => explode(';', _t('users', 'знак;знака;знаков')),
        'you_opinion' => _t('orders', 'Ваш отзыв'),
        'check_type'  => _t('opinions', 'Укажите тип отзыва'),
    ),
    'block' => 'j-orders-owner-list',
)) ?>);
<? js::stop() ?>
</script>