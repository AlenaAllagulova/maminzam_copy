<?php
/**
 * Форма предложения заказа исполнителю (краткая)
 * @var $this Orders
 */
$hideSpec = ! empty($spec);
$fairplayEnabled = bff::fairplayEnabled();
?>
<form class="form-horizontal" role="form" action="<?= Orders::url('add') ?>" method="post" id="j-order-form">
    <input type="hidden" name="visibility" value="<?= Orders::VISIBILITY_PRIVATE ?>" />
    <!-- Title -->
    <div class="form-group j-required">
        <label for="title" class="col-sm-3 control-label o-control-label"><?= _t('orders', 'Заголовок'); ?> <i class="text-danger">*</i></label>
        <div class="col-sm-9">
            <input type="text" name="title" value="" class="form-control" placeholder="<?= _t('orders', 'Что требуется сделать'); ?>" autocomplete="off" maxlength="<?= Orders::titleLimit() ?>" />
        </div>
    </div>

    <!-- Description -->
    <div class="form-group j-required">
        <label for="description" class="col-sm-3 control-label o-control-label"><?= _t('orders', 'Описание'); ?> <i class="text-danger">*</i></label>
        <div class="col-sm-9">
            <textarea rows="5" name="descr" class="form-control" placeholder="<?= _t('orders', 'Подробно опишите задачу, сроки выполнения, другие условия работы'); ?>"></textarea>
        </div>
    </div>

    <!-- Specialization -->
    <div class="form-group j-specs <?= $hideSpec ? 'hidden' : '' ?>">
        <label for="inputSpec" class="col-sm-3 control-label o-control-label"><?= _t('', 'Специализация'); ?></label>
        <div class="col-sm-9">
            <?= Specializations::i()->specSelect(1, (!empty($spec) ? $spec : array()), array('inputName' => 'specs')); ?>
        </div>
    </div>

    <!-- Period -->
    <div class="form-group hidden">
        <label class="col-sm-3 control-label o-control-label"><?= _t('orders', 'Прием заявок'); ?></label>
        <div class="col-sm-3">
            <select class="form-control" name="term"><?= Orders::aTermsOptions(false, $days, true) ?></select>
        </div>
        <div class="col-sm-6 p-profile-period">
            <?= _t('orders', 'до [date]', array('date'=>'<strong id="j-term-title">'.tpl::date_format2(time() + $days * 24*60*60, false, true).'</strong>')); ?>
        </div>
    </div>

    <!-- Budget -->
    <div class="<?= ! $hideSpec ? 'hidden' : '' ?>" id="j-order-price-block"><?= $this->viewPHP($aData, 'form.price'); ?></div>

    <? if($fairplayEnabled): ?>
        <div class="form-group">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="fairplay" value="<?= Orders::FAIRPLAY_USE ?>"/> <?= _t('fp', 'Предлагаю оплату через <a [link]>Безопасную Сделку</a>', array('link' => 'href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-shield c-link-icon c-safe-color"></i')); ?>
                    </label>
                </div>
            </div>
        </div>
    <? endif; ?>

    <!-- Submit -->
    <div class="form-group">
        <div class="col-sm-3 o-control-label"></div>
        <div class="col-sm-9 c-formSubmit">
            <button class="btn btn-primary c-formSuccess j-submit"><i class="fa fa-rocket"></i> <?= _t('orders', 'Предложить'); ?></button>
            <a class="btn btn-default c-formSuccess j-full-add" href="#"><?= _t('orders', 'Уточнить детали'); ?> <i class="fa fa-angle-right"></i></a>
            <a class="c-formCancel ajax-link" href="#" data-dismiss="modal"><?= _t('form', 'Отмена'); ?></a>
        </div>
    </div>

</form>