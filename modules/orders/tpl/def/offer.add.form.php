<?php
    tpl::includeJS(array('qquploader', 'ui.sortable'), true);
    tpl::includeJS('orders.offer.add', false, 3);
    $nDefCurr = config::sys('currency.default');
    $nSpecID = 0;
    $type_service = ($type == Orders::TYPE_SERVICE);
    if ($type_service) {
        $spec = array();
        if( ! empty($specs)){ $spec = reset($specs); }
        if( ! empty($spec['spec_id'])){  $nSpecID = $spec['spec_id']; }
        $aPriceSett = Specializations::i()->aPriceSett($nSpecID);
    }
    $fairplayEnabled = bff::fairplayEnabled();
?>
<div class="l-borderedBlock no-color">
    <header class="l-inside">
        <a href="#offer-add" class="btn btn-link ajax-link o-btn-propose" id="j-collapse-add" data-toggle="collapse" data-parent="#accordion"><span><?= _t('orders-offers', 'Опубликовать своё предложение'); ?></span></a>
    </header>

    <div class="collapse" id="offer-add">

    <form action="" method="post" id="j-offer-add-form" class="form-horizontal" role="form">
    <input type="hidden" name="order_id" value="<?= $id ?>" />

    <div class="l-inside">

        <div class="form-group o-propose-inputs o-price-inputs">
            <label for="j-price-from" class="col-sm-2 control-label"><?= $type_service && ! empty($aPriceSett['price_title']) ? $aPriceSett['price_title'] : _t('', 'Стоимость:'); ?></label>
            <div class="col-sm-10">
                <div class="input-group">
                    <input name="price_from" type="text" id="j-price-from" class="form-control input-sm" placeholder="<?= _t('', 'от'); ?>"/>
                    <input name="price_to" type="text" class="form-control input-sm" placeholder="<?= _t('', 'до'); ?>"/>
                    <select name="price_curr" class="form-control input-sm"><?= Site::currencyOptions($nDefCurr) ?></select>
                    <? if ($type_service && ! empty($aPriceSett['rates'])): ?>
                        <select name="price_rate" class="form-control input-sm"><?= HTML::selectOptions($aPriceSett['rates']) ?></select>
                    <? endif; ?>
                </div>
            </div>
        </div>

        <? if($type_service): ?>
        <div class="form-group o-propose-inputs o-price-inputs j-terms">
            <label for="terms" class="col-sm-2 control-label"><?= _t('', 'Сроки'); ?></label>
            <div class="col-sm-10">
                <div class="input-group">
                    <input name="terms_from" type="text" class="form-control input-sm" placeholder="<?= _t('', 'от'); ?>"/>
                    <input name="terms_to" type="text" class="form-control input-sm" placeholder="<?= _t('', 'до'); ?>"/>
                    <select name="terms_type" class="form-control input-sm"><?= Specializations::aTerms() ?></select>
                </div>
            </div>
        </div>
        <? endif; ?>

        <div class="row j-required">
            <label for="text" class="col-sm-2 control-label"><?= _t('orders-offers', 'Описание'); ?></label>
            <div class="col-sm-10">
                <textarea name="descr" rows="5" id="text" class="form-control"></textarea>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <? if( ! bff::servicesEnabled() || User::isPro()): ?>
                <div class="checkbox">
                    <label>
                        <input name="client_only" type="checkbox" /> <?= _t('orders-offers', 'Сделать предложение видимым только для заказчика'); ?>
                    </label>
                </div>
                <? endif; ?>
                <? if($fairplayEnabled && ! $fairplay): ?>
                <div class="checkbox">
                    <label>
                        <input name="fairplay" type="checkbox" /> <?= _t('fp', 'Предпочитаю оплату работы через <a [link]>Безопасную Сделку</a>', array('link' => 'href="'.Fairplay::url('info.orders.add').'" target="_blank"><i class="fa fa-shield c-link-icon c-safe-color"></i')); ?>
                    </label>
                </div>
                <? endif; ?>
            </div>
        </div>

        <? if(Orders::offerExamplesLimit()): ?>
        <div class="row">
            <label class="col-sm-2 control-label">
                <?= _t('orders-offers', 'Примеры работ'); ?>
            </label>
            <div class="col-sm-10">
                <?= _t('orders-offers', 'Выберите работы, которые Вы хотите показать заказчику. Максимум: [limit]', array('limit' => Orders::offerExamplesLimit())); ?>
                <div class="o-propose-works">

                    <? if( ! empty($availableExamples)): ?>
                    <div class="o-project-thumb">
                        <div class="o-inner">
                            <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#j-examples-popup"><?= _t('', 'Добавить'); ?></a>
                        </div>
                    </div>
                    <? endif; ?>

                    <div id="j-offer-examples"></div>

                    <div class="clearfix"></div>

                    <p>
                        <a class="btn btn-default btn-sm mrgt20" id="j-upload-img"><i class="fa fa-upload"></i> <?= _t('orders-offers', 'Загрузить файл с компьютера'); ?></a>
                        <img alt="" src="<?= bff::url('/img/loader.gif') ?>" class="j-progress mrgt20 hidden" />
                    </p>

                </div>

            </div>
        </div>
        <? endif; ?>

    </div>

    <?= $availableExamples ?>

    <div class="l-inside">
        <div class="row">
            <div class="col-md-10 col-md-offset-2 o-publicate-button">
                <a class="btn btn-success mrgr10 j-submit"><i class="fa fa-check"></i> <?= _t('orders-offers', 'Добавить предложение'); ?></a>
                <a href="#propose" class="ajax-link j-cancel" data-toggle="collapse" data-parent="#accordion"><span><?= _t('form', 'Отмена'); ?></span></a>
            </div>
        </div>
    </div><!-- /.l-inside -->

    </form>

    </div><!-- /.collapse -->

</div><!-- /.bordered-block no-color -->
<script type="text/javascript">
<? js::start() ?>
jOrdersOfferAddForm.init(<?= func::php2js(array(
    'lang' => array(
        'upload_typeError'    => _t('form', 'Допустимы только следующие типы файлов: {extensions}'),
        'upload_sizeError'    => _t('form', '"Файл {file} слишком большой, максимально допустимый размер {sizeLimit}'),
        'upload_minSizeError' => _t('form', 'Файл {file} имеет некорректный размер'),
        'upload_emptyError'   => _t('form', 'Файл {file} имеет некорректный размер'),
        'upload_limitError'   => _t('form', 'Вы можете загрузить не более {limit} файлов'),
        'upload_onLeave'      => _t('form', 'Происходит загрузка изображения, если вы покинете эту страницу, загрузка будет прекращена'),
    ),
    'examplesLimit' => Orders::offerExamplesLimit(),
    'imgMaxSize'    => $img->getMaxSize(),
    'imgUploaded'   => 0,
    'imgData'       => array(),
)) ?>);
<? js::stop() ?>
</script>