<?php
/**
 * Форма заказа (добавление / редактирование)
 * @var $this Orders
 */
$bMapEnabled = Orders::mapEnabled();
if($bMapEnabled){
    Geo::mapsAPI(true);
}
tpl::includeJS(array('autocomplete', 'qquploader', 'ui.sortable'), true);
tpl::includeJS('orders.order.form', false, 4);
$aData = HTML::escape($aData, 'html', array('title','descr','addr_addr'));

$edit = ! empty($id);
if($edit){
    $formTitle = _t('orders', 'Редактирование заказа');
    $url = Orders::url('edit', array('id'=>$id));
} else {
    $formTitle = _t('orders', 'Добавить заказ');
    $id = 0;
    $url = Orders::url('add');
}
$bWorkerInvite =  Orders::invitesEnabled() && ! empty($workerInvite);
if($bWorkerInvite){
    $formTitleWorker = _t('orders', 'Добавить заказ для [worker]', array('worker' => tpl::userLink($workerInvite, 'no-login')));
}
$aTypes = Orders::aTypes();
if( ! Orders::useProducts()){
    $type = Orders::TYPE_SERVICE;
}
$sClassService = 'j-type j-type-'.Orders::TYPE_SERVICE.( $type && $type != Orders::TYPE_SERVICE ? ' hidden' : '');
$sClassProduct = 'j-type j-type-'.Orders::TYPE_PRODUCT.( $type != Orders::TYPE_PRODUCT ? ' hidden' : '');
foreach($aSvc as $k => $v){
    switch($v['id']){
        case Orders::SVC_FIX:    $aSvc[$k]['fa'] = 'fa-thumb-tack'; break;
        case Orders::SVC_UP:     $aSvc[$k]['fa'] = 'fa-arrow-up';   break;
        case Orders::SVC_MARK:   $aSvc[$k]['fa'] = 'fa-bolt';       break;
        case Orders::SVC_HIDDEN: $aSvc[$k]['fa'] = 'fa-eye-slash';  break;
    }
}
$termsEnabled = Orders::termsEnabled();
$fairplayEnabled = bff::fairplayEnabled();
?>
<div class="container">

    <section class="l-mainContent">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <?= tpl::getBreadcrumbs(array(
                    array('title' => _t('orders','Orders'),'link'=>Orders::url('list')),
                    array('title' => $formTitle,'active'=>true),
                )); ?>

                <div class="p-profileContent">

                    <div class="p-profile-title">
                        <h4><?= $bWorkerInvite ? $formTitleWorker : $formTitle ?></h4>
                    </div><!-- /.p-profile-title -->

                    <form class="form-horizontal" role="form" action="" method="POST" enctype="multipart/form-data" id="j-order-form">

                        <!-- Type -->
                        <? if(Orders::useProducts()): ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label o-control-label"><?= _t('orders', 'Тип заказа'); ?></label>
                            <div class="col-sm-9">
                            <?  if($edit): ?>
                                    <div class="p-form-noinput"><?= $aTypes[$type]['t'] ?></div>
                            <?  else:
                                    foreach($aTypes as $k => $v){ $aTypes[$k]['a'] = 0; }
                                    $aTypes[Orders::TYPE_SERVICE]['a'] = 1;
                                ?>
                                <div class="btn-group" data-toggle="buttons">
                                    <? foreach($aTypes as $v): ?>
                                    <label class="btn btn-default<?= $v['a'] ? ' active' : '' ?>">
                                        <input type="radio" name="type" value="<?= $v['id'] ?>"<?= $v['a'] ? ' checked="checked"': '' ?> /> <?= $v['t'] ?>
                                    </label>
                                    <? endforeach; ?>
                                </div>
                            <? endif; ?>
                            </div>
                        </div>
                        <? else: ?>
                        <input type="radio" name="type" value="<?= Orders::TYPE_SERVICE ?>" checked="checked" style="display: none;" />
                        <? endif; ?>

                        <? if($bWorkerInvite): ?>
                            <input type="hidden" name="visibility" value="<?= Orders::VISIBILITY_PRIVATE ?>" />
                            <input type="hidden" name="create_invite" value="<?= $workerInvite['user_id'] ?>" />
                        <? endif; ?>

                        <!-- Title -->
                        <div class="form-group j-required">
                            <label for="title" class="col-sm-3 control-label o-control-label"><?= _t('', 'Заголовок'); ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-8">
                                <input type="text" name="title" value="<?= $title ?>" class="form-control" id="title" placeholder="<?= _t('orders', 'Что требуется сделать'); ?>" autocomplete="off" maxlength="<?= Orders::titleLimit() ?>"/>
                            </div>
                        </div>

                        <!-- Description -->
                        <div class="form-group j-required">
                            <label for="description" class="col-sm-3 control-label o-control-label"><?= _t('orders', 'Описание'); ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-8">
                                <textarea rows="5" name="descr" class="form-control" id="description" placeholder="<?= _t('orders', 'Подробно опишите задачу, сроки выполнения, другие условия работы'); ?>"><?= $descr ?></textarea>
                            </div>
                        </div>

                        <? if($termsEnabled): ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label o-control-label"><?= _t('orders', 'Прием заявок'); ?></label>
                            <div class="col-sm-3 <?= $edit ? 'hidden' : '' ?>">
                                <select class="form-control" name="term"><?= Orders::aTermsOptions( $edit ? $term : isset($term) ? $term : false, $days) ?></select>
                            </div>
                            <div class="col-sm-6 p-profile-period<?= $edit || ! $days ? ' hidden' : '' ?>">
                                <?= _t('orders', 'до [date]', array('date'=>'<strong id="j-term-title">'.tpl::date_format2(time() + $days * 86400, false, true).'</strong>')); ?>
                            </div>
                            <? if($edit): ?>
                            <div class="col-sm-6 p-profile-period">
                                <? if($expire == '0000-00-00 00:00:00'): ?>
                                    <strong><?= _t('orders','Бессрочно') ?></strong>
                                <? else: ?>
                                    <?= _t('orders', 'до [date]', array('date'=>'<strong>'.tpl::date_format2($expire, false, true).'</strong>')); ?>
                                <? endif; ?>
                                <a href="#" class="col-sm-offset-1 ajax-link j-term-change"><span><?= _t('', 'Изменить'); ?></span></a>
                            </div>
                            <? endif; ?>
                        </div>
                        <? endif; ?>

                        <div class="form-group <?= $sClassService ?> j-specs <?= $bWorkerInvite && !empty($specs) ? 'hidden' : '' ?>">
                            <label for="inputSpec" class="col-sm-3 control-label o-control-label"><?= _t('', 'Специализация'); ?></label>
                            <div class="col-sm-9">
                                <?= Specializations::i()->specSelect(1, (!empty($specs) ? reset($specs) : array()), array('inputName' => 'specs')); ?>
                            </div>
                        </div>

                        <? if(Orders::useProducts()):?>
                        <div class="form-group <?= $sClassProduct ?> j-cats">
                            <label for="inputSpec" class="col-sm-3 control-label o-control-label"><?= _t('', 'Категория'); ?></label>
                            <div class="col-sm-9">
                                <? $cat = array();
                                if( ! empty($cats)){ $cat = reset($cats); } ?>
                                <?= Shop::i()->catSelect(1, $cat); ?>
                            </div>
                        </div>
                        <? endif; ?>

                        <div class="form-group <?= $sClassService ?>">
                            <label for="inputSpec" class="col-sm-3 control-label o-control-label"><?= _t('orders', 'Тип услуги'); ?></label>
                            <div class="col-sm-9">
                                <?  $aServiceTypes = Orders::aServiceTypes();
                                    foreach($aServiceTypes as $k => $v){ $aServiceTypes[$k]['a'] = 0; }
                                    if( ! array_key_exists($service_type, $aServiceTypes)){
                                        $service_type = reset($aServiceTypes);
                                        $service_type = $service_type['id'];
                                    }
                                    $aServiceTypes[ $service_type ]['a'] = 1;
                                ?>
                                <div class="btn-group" data-toggle="buttons">
                                    <? foreach($aServiceTypes as $v): ?>
                                        <label class="btn btn-default<?= $v['a'] ? ' active' : '' ?>">
                                            <i class="fa fa-<?= $v['c'] ?>"></i>
                                            <input type="radio" name="service_type" value="<?= $v['id'] ?>"<?= $v['a'] ? ' checked="checked"': '' ?> /> <?= $v['t'] ?>
                                        </label>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>

                        <div id="j-order-form-dp"><?= ! empty($dp) ? $dp : '' ?></div>

                        <div class="<?= $sClassService ?>">
                            <?= $this->orderTags()->formFront($id, array('wightClass' => 'col-sm-8')); ?>
                        </div>

                        <!-- Budget -->
                        <div class="<?= $sClassService ?>" id="j-order-price-block"><?
                            if(empty($spec) && ! empty($specs)){ $spec = reset($specs); $aData['spec'] = $spec; }
                            ?><?= $this->viewPHP($aData, 'form.price'); ?></div>

                        <? $region = array('reg1_country' => 0, 'reg3_city' => 0, 'title' => '');
                        if (empty($regions)) {
                            if (Geo::filterEnabled()) {
                                $filter = Geo::filter();
                                if (!empty($filter['id'])) {
                                    if ($filter['numlevel'] == Geo::lvlCity) {
                                        $region = array('reg1_country' => $filter['country'], 'reg3_city' => $filter['id'], 'title' => $filter['title']);
                                    } else {
                                        $region = array('reg1_country' => $filter['country'] ? $filter['country'] : $filter['id'], 'reg3_city' => 0, 'title' => '');
                                    }
                                }
                            }
                        } else {
                            $region = reset($regions);
                        }
                        ?>
                        <div class="form-group">
                            <label for="country" class="col-sm-3 control-label o-control-label"><?= _t('', 'Регион'); ?></label>
                            <? if(Geo::countrySelect()): ?>
                            <div class="col-sm-4 mrgb5">
                                <select class="form-control" id="j-order-country"><?= HTML::selectOptions(Geo::countryList(), $region['reg1_country'], _t('', 'Выбрать'), 'id', 'title') ?></select>
                            </div>
                            <? endif; ?>
                            <div class="col-sm-4 j-region">
                                <input type="hidden" name="regions[]" value="<?= $region['reg3_city'] ?>" id="j-order-city-value" />
                                <input type="text" class="form-control" id="j-order-city-select" value="<?= $region['title'] ?>" placeholder="<?= _t('', 'Город'); ?>" />
                            </div>
                        </div>

                        <? if($bMapEnabled): ?>
                        <!-- Address -->
                        <div class="form-group j-region j-city">
                            <label for="address" class="col-sm-3 control-label o-control-label"><?= _t('', 'Адрес объекта'); ?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="addr_addr" id="j-order-addr-addr" value="<?= $addr_addr ?>" placeholder="<?= _t('', 'Укажите улицу, район, номер дома и т.п.'); ?>" />
                            </div>
                        </div>

                        <!-- Map -->
                        <div class="form-group j-region j-city">
                            <input type="hidden" name="addr_lat" id="j-order-addr-lat" value="<?= $addr_lat ?>" />
                            <input type="hidden" name="addr_lng" id="j-order-addr-lng" value="<?= $addr_lng ?>" />

                            <div class="col-sm-3 o-control-label"></div>
                            <div class="col-sm-8">
                                <div id="j-order-addr-map" style="width:100%; height:300px; display: none;" class="p-addProject_map map-google"></div>
                            </div>
                        </div>
                        <? endif; ?>

                        <!-- Gallery -->
                        <? if(Orders::imagesLimit()): ?>
                        <div class="form-group j-images">
                            <input type="hidden" name="images_type" value="ajax" class="j-images-type-value" />
                            <input type="hidden" name="images_hash" value="<?= $imghash ?>" />
                            <label class="col-sm-3 control-label o-control-label"><?= _t('', 'Изображения'); ?></label>

                            <div class="col-sm-9">

                                <div id="p-gallery-show" class="j-images-type j-images-type-ajax">

                                    <ul class="p-addWork-gallery j-img-slots">

                                        <? for($i = 1; $i <= Orders::imagesLimit(); $i++): ?>
                                        <li class="j-img-slot">
                                            <a class="p-hasimg hidden j-img-preview">
                                                <div class="p-galleryImg-container">
                                                    <img class="j-img-img" src="" alt="" />
                                                </div>
                                            </a>
                                            <a href="#" class="p-galleryImg-remove link-delete hidden j-img-delete j-img-preview"><i class="fa fa-times-circle-o"></i></a>
                                            <a class="j-img-upload">
                                                <div class="p-galleryImg-container">
                                                    <i class="fa fa-plus-circle j-img-link"></i>
                                                </div>
                                            </a>
                                            <input type="hidden" name="" value="" class="j-img-fn" />
                                        </li>
                                        <? endfor; ?>

                                        <div class="clearfix"></div>
                                    </ul>

                                    <div class="help-block">
                                        <?= _t('', 'Вы можете сортировать изображения просто перетаскивая их'); ?>
                                    </div>

                                    <?= _t('', 'Если у вас возникли проблемы воспользуйтесь <a [a_simple]><span>альтернативной формой</span></a>', array('a_simple' => 'href="#" class="ajax-link j-images-toggler" data-type="simple"')); ?>

                                </div>

                                <div id="p-gallery" class="p-gallery-alternate hide j-images-type j-images-type-simple">
                                    <? for($i = 1; $i <= Orders::imagesLimit() - $imgcnt; $i++): ?>
                                    <div><input name="images_simple_<?= $i ?>" type="file" /></div>
                                    <? endfor; ?>

                                    <a href="#" class="ajax-link c-formCancel j-images-toggler" data-type="ajax"><span><?= _t('', 'Удобная форма загрузки изображений'); ?></span></a>

                                </div>

                            </div>

                        </div>
                        <? endif; ?>

                        <!-- Attachments -->
                        <? if(Orders::attachmentsLimit()): ?>
                        <div class="form-group j-attachments">
                            <input type="hidden" name="attachments_hash" value="<?= $attachhash ?>" />

                            <label class="col-sm-3 control-label o-control-label"><?= _t('', 'Файлы'); ?></label>
                            <div class="col-sm-7 p-form-noinput">
                                <a class="btn btn-sm btn-default j-upload"><?= _t('', 'Выбрать файл'); ?></a>
                                <img alt="" src="<?= bff::url('/img/loader.gif') ?>" class="j-progress hidden">
                                <ul class="p-addProject_files j-list">
                                </ul>
                            </div>
                        </div>
                        <? endif; ?>

                        <!-- Filters -->
                        <? if(bff::servicesEnabled() && ! $bWorkerInvite): ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label o-control-label">&nbsp;</label>
                            <div class="col-sm-7">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="pro" <?= $pro ? ' checked="checked"' : '' ?>>
                                        <?= _t('orders', 'Только для [pro]', array('pro'=>'<span class="pro">pro</span>')); ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <? endif; ?>

                        <? if($fairplayEnabled): ?>
                            <div class="form-group mrgt15">
                                <label class="col-sm-3 control-label o-control-label"><?= _t('fp', 'Способ оплаты'); ?></label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="fairplay" value="<?= Orders::FAIRPLAY_USE ?>" <?= ($edit || $bWorkerInvite) && $fairplay == Orders::FAIRPLAY_USE ? 'checked="checked"' : '' ?>>
                                            <?= _t('fp', 'Безопасная сделка (с резервированием бюджета)'); ?> <a href="<?= Fairplay::url('info.orders.add') ?>" target="_blank"><i class="fa fa-question-circle"></i> </a>
                                        </label>
                                        <div class="help-block mrgt0 mrgb0"><?= _t('fp', 'Безопасное сотрудничество с гарантией возврата денег. Вы резервируете бюджет заказа, а мы гарантируем вам возврат суммы, если работа выполнена некачественно или не в срок.'); ?></div>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="fairplay" value="<?= Orders::FAIRPLAY_NONE ?>" <?= ($edit || $bWorkerInvite) && $fairplay == Orders::FAIRPLAY_NONE ? 'checked="checked"' : '' ?>>
                                            <?= _t('fp', 'Прямая оплата исполнителю на его кошелек/счет'); ?>
                                        </label>
                                        <div class="help-block mrgt0 mrgb0"><?= _t('fp', 'Сотрудничество без участия сайта в процессе оплаты. Вы сами договариваетесь с исполнителем о способе и порядке оплаты. И самостоятельно регулируете все претензии, связанные с качеством и сроками выполнения работы.'); ?></div>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>

                        <? if( ! $edit && ! User::id()): ?>
                            <? if(Users::rolesEnabled()): ?>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= _t('users', 'Тип аккаунта'); ?></label>
                                    <div class="col-sm-9">
                                        <div class="btn-group" data-toggle="buttons">
                                            <? foreach(Users::roles() as $v): ?>
                                                <label class="btn btn-default <?= $v['default'] ? 'active' : '' ?>">
                                                    <input type="radio" name="role_id" value="<?= $v['id'] ?>" <?= $v['default'] ? 'checked="checked"' : '' ?>> <?= $v['t'] ?>
                                                </label>
                                            <? endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            <? endif; ?>

                        <div class="form-group">
                            <label for="j-email" class="col-sm-3 control-label o-control-label"><?= _t('', 'Email'); ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-4">
                                <input type="email" name="email" class="form-control j-required" id="j-email" placeholder="<?= _t('', 'Электронная почта'); ?>" autocomplete="off" />
                            </div>
                        </div><!-- /.form-group-->

                        <? if(Users::registerPhone()): ?>
                            <div class="form-group">
                                <label for="inputPhone" class="col-sm-3 control-label"><?= _t('users', 'Телефон') ?>  <i class="text-danger">*</i></label>
                                <div class="col-sm-4">
                                    <?= Users::i()->registerPhoneInput(array('id'=>'j-u-register-phone','name'=>'phone')) ?>
                                </div>
                            </div>
                        <? endif; ?>

                        <div class="form-group">
                            <label for="j-name" class="col-sm-3 control-label o-control-label"><span class="j-name"><?= _t('', 'Имя'); ?></span> <i class="text-danger">*</i></label>
                            <div class="col-sm-4">
                                <input type="text" name="name" class="form-control j-required" id="j-name" placeholder="<?= _t('', 'Ваше имя'); ?>" autocomplete="off" />
                            </div>
                        </div><!-- /.form-group-->

                        <? if(config::sysAdmin('users.register.agreement', true, TYPE_BOOL)) { ?>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="agreement" class="j-agreement" autocomplete="off" /><?= _t('', 'Я соглашаюсь с <a href="[link_agreement]" target="_blank">правилами использования сервиса</a>, а также с передачей и обработкой моих данных.', array('link_agreement'=>Users::url('agreement'))) ?> <i class="text-danger">*</i>
                                    </label>
                                </div>
                            </div><!-- /.col-sm-8 -->
                        </div><!-- /.form-group-->
                        <? } ?>
                        <? endif; ?>

                        <? if( ! empty($phone_on)): ?>
                            <div class="form-group">
                                <label for="inputPhone" class="col-sm-3 control-label"><?= _t('users', 'Телефон') ?>  <i class="text-danger">*</i></label>
                                <div class="col-sm-4">
                                    <?= Users::i()->registerPhoneInput(array('id'=>'j-u-register-phone','name'=>'phone')) ?>
                                </div>
                            </div>
                        <? endif; ?>


                        <? if(bff::servicesEnabled() && User::id() && ! $bWorkerInvite && ( $edit && $visibility == Orders::VISIBILITY_ALL && $status == Orders::STATUS_OPENED || ! $edit)): ?>
                            <div class="row">
                                <label class="col-sm-3 control-label o-control-label"><?= _t('', 'Платные услуги'); ?></label>
                                <div class="col-sm-9">
                                    <div class="panel-group o-advertise-accordion">
                                    <?  $cur = Site::currencyDefault(); $svc = (int)$svc;
                                        foreach($aSvc as $v):
                                            if( ! $v['on']) continue;
                                        ?>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <a class="j-accordion">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="svc[]" value="<?= $v['id'] ?>" <?= $svc & $v['id'] ? ' disabled="disabled" checked="checked" ': '' ?> />
                                                                <i class="fa <?= $v['fa'] ?>"></i><span><?= $v['title_view'][LNG] ?></span>
                                                                <div class="o-advertise-price"><span class="j-svc-price-<?= $v['id'] ?>"><?= round($v['price']) ?></span> <?= $cur; ?></div>
                                                            </label>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div id="j-svc-<?= $v['id'] ?>" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <? if($v['id'] == Orders::SVC_FIX && ! empty($v['per_day'])): ?>
                                                        <div class="o-advertise-accordion-count form-inline">
                                                            <div class="form-group">
                                                                <label for="j-fixed-count"><?= _t('', 'Закрепить на'); ?></label>
                                                                <input type="text" name="svc_fixed_days" maxlength="3" class="form-control input-sm" id="j-fixed-count" value="<?= $v['period'] ?>">
                                                                <?= _t('', 'дней'); ?>
                                                            </div>
                                                        </div>
                                                        <? endif; ?>
                                                        <?= $v['description_full'][LNG] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>

                        <div class="form-group">
                            <div class="col-sm-3 o-control-label"></div>
                            <div class="col-sm-9 c-formSubmit">
                                <a class="btn btn-primary c-formSuccess  j-submit" data-loading-text="<?= _t('form', 'Подождите...') ?>"><i class="fa fa-rocket"></i> <?= $edit ? _t('form', 'Сохранить') : ( $bWorkerInvite ? _t('orders', 'Предложить') : _t('orders', 'Опубликовать заказ')) ?> <span></span></a>
                                <a class="c-formCancel j-cancel" href="#"><?= _t('form', 'Отмена'); ?></a>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </section>

</div>
<script type="text/javascript">
<? js::start();
    $aTermsText = array();
    if ($termsEnabled) {
        foreach (Orders::aTerms() as $v) {
            $aTermsText[ $v['id'] ] = $v['date'];
        }
    }
?>
jOrdersOrderForm.init(<?= func::php2js(array(
    'lang' => array(
        'saved_success' => _t('orders', 'Заказ успешно сохранен'),
        'spec_wrong'    => _t('orders', 'Укажите специализацию'),
        'cat_wrong'    => _t('orders', 'Укажите категорию'),
        'upload_typeError' => _t('form', 'Допустимы только следующие типы файлов: {extensions}'),
        'upload_sizeError' => _t('form', 'Файл {file} слишком большой, максимально допустимый размер {sizeLimit}'),
        'upload_minSizeError' => _t('form', 'Файл {file} имеет некорректный размер'),
        'upload_emptyError' => _t('form', 'Файл {file} имеет некорректный размер'),
        'upload_limitError' => _t('form', 'Вы можете загрузить не более {limit} файлов'),
        'upload_onLeave' => _t('form', 'Происходит загрузка изображения, если вы покинете эту страницу, загрузка будет прекращена'),
        'agreement' => _t('users', 'Пожалуйста подтвердите, что Вы согласны с пользовательским соглашением'),
        'fairplay'  => _t('fp', 'Укажите способ оплаты'),
        'name'  => array(
            'r'.Users::ROLE_PRIVATE => array('n' => _t('users', 'Имя'),      'p' => _t('', 'Ваше имя')),
            'r'.Users::ROLE_COMPANY => array('n' => _t('users', 'Компания'), 'p' => _t('', 'Название вашей компании')),
        ),
    ),
    'url'           => $url,
    'defCountry'    => Geo::defaultCountry(),
    'geoCountry'    => Geo::regionTitle( ! empty($reg1_country) ? $reg1_country : Geo::defaultCountry()),
    'regionPreSuggest' => Geo::regionPreSuggest( ! empty($reg1_country) ? $reg1_country : ( ! empty($region['reg1_country']) ? $region['reg1_country'] : 0), 2),
    'geoCity'       => $region['reg3_city'],
    'mapEnabled'    => $bMapEnabled,
    'itemID'        => $id,
    'imgLimit'      => Orders::imagesLimit(),
    'imgMaxSize'    => $img->getMaxSize(),
    'imgUploaded'   => $imgcnt,
    'imgData'       => $images,
    'attachLimit'   => Orders::attachmentsLimit(),
    'attachMaxSize' => $attach->getMaxSize(),
    'attachUploaded'=> $attachcnt,
    'attachData'    => $attachments,
    'typeService'   => Orders::TYPE_SERVICE,
    'typeProduct'   => (Orders::useProducts() ? Orders::TYPE_PRODUCT : 0),
    'typeID'        => $edit ? $type : 0,
    'terms'         => $aTermsText,
    'invite'        => $bWorkerInvite ? 1 : 0,
    'fairplay'      => $fairplayEnabled ? 1 : 0,
)) ?>);
<? js::stop() ?>
</script>