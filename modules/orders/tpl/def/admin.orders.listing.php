<?php
    /**
     * @var $this Orders
     */
if(Orders::mapEnabled()) {
    Geo::mapsAPI(true);
}
tpl::includeJS(array('ui.sortable', 'qquploader', 'autocomplete', 'autocomplete.fb', 'comments'), true);  # адрес, форма, изображения, заявки
?>
<?= tplAdmin::blockStart(_t('orders','Orders').' / '._t('orders','Добавление заказы'), false, array('id'=>'OrdersOrdersFormBlock','style'=>'display:none;')); ?>
<div id="OrdersOrdersFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart(_t('orders','Orders').' / '._t('orders','Список заказов'), true, array('id'=>'OrdersOrdersListBlock','class'=>(!empty($act) ? 'hidden' : '')),
    array('title'=>_t('orders','+ добавить заказ'),'class'=>'ajax','onclick'=>'return jOrdersOrdersFormManager.action(\'add\',0);')); ?>
<?
$aTabs = array(
    0 => array('t'=>_t('orders','Открытые')),
    1 => array('t'=>_t('orders','Закрытые')),
    2 => array('t'=>_t('orders','На модерации'), 'counter'=>config::get('orders_orders_moderating', 0)),
    3 => array('t'=>_t('orders','Заблокированные')),
    4 => array('t'=>_t('orders','Неактивированные'),'c'=>' class="disabled" '),
    5 => array('t'=>_t('orders','Все')),
);
?>
<div class="tabsBar" id="OrdersOrdersListTabs">
    <? foreach($aTabs as $k=>$v) { ?>
        <span class="tab <? if($f['tab']==$k) { ?>tab-active<? } ?>"><a href="#" onclick="return jOrdersOrdersList.onTab(<?= $k ?>,this);" <?= (!empty($v['c']) ? $v['c'] : '') ?>><?= $v['t'] ?><? if(! empty($v['counter'])){ ?> (<?= $v['counter'] ?>)<? } ?></a></span>
    <? } ?>
    <div id="OrdersOrdersProgress" class="progress" style="display: none;"></div>
</div>
<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="OrdersOrdersListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <input type="hidden" name="tab" value="<?= $f['tab'] ?>" />

        <div class="left" style="<? if( ! Orders::useProducts()) { ?>display: none; <? } ?>">
            <select name="type" style="width: 100px;" onchange="jOrdersOrdersList.onType();"><?= HTML::selectOptions(Orders::aTypes(), $f['type'], _t('orders', '- Тип -'), 'id', 't') ?></select>
        </div>
        <? if(Geo::countrySelect()): ?>
        <div class="left" style="margin-left:4px;">
            <select name="country" class="j-change" style="width: 100px;" onchange="jOrdersOrdersList.onCountry();"> <?= HTML::selectOptions(Geo::countryList(), $f['country'], 'Страна', 'id', 'title') ?></select>
        </div>
        <? endif; ?>
        <div class="left" style="margin-left:4px;">
            <input type="hidden" name="region" value="<?= $f['region'] ?>" />
            <div class="relative"><input type="text" style="width:120px;" id="j-f-region" class="autocomplete" placeholder="<?= _t('orders', 'Регион') ?>" value="<?= $f['region'] ? Geo::regionTitle($f['region']) : '' ?>" /></div>
        </div>
        <div class="left" style="margin-left:4px;">
            <input type="text" style="width:130px;" maxlength="150" name="title" placeholder="<?= _t('orders', 'ID / Ключевое слово') ?>" value="<?= HTML::escape($f['title']) ?>" />
        </div>
        <div class="left" style="margin-left:4px;">
            <input type="text" style="width:155px;" maxlength="150" name="user" placeholder="<?= _t('', 'ID / логин / E-mail пользователя') ?>" value="<?= HTML::escape($f['user']) ?>" />
        </div>

        <input type="button" class="left btn btn-small button cancel" style="margin-left: 4px;" onclick="jOrdersOrdersList.submit(false);" value="<?= _t('', 'search') ?>" />
        <div class="left" style="margin-left: 8px;"><a class="ajax cancel" onclick="jOrdersOrdersList.submit(true); return false;"><?= _t('', 'reset') ?></a></div>
        <div class="clearfix"></div>
        <div class="j-type j-type-<?= Orders::TYPE_SERVICE ?> <?= $f['type'] != Orders::TYPE_SERVICE ? 'hidden' : '' ?>">
            <hr />
            <div><?
                foreach($specsOptions as $lvl => $v):
                    ?><select class="spec-select" data-lvl="<?= $lvl ?>" style="margin-right: 5px;<? if(empty($v['categories'])){ ?>display: none; <? } ?>" onchange="jOrdersOrdersList.onSpecialization($(this))"><?= $v['categories'] ?></select><?
                endforeach;
                ?></div><div id="j-f-specs-selected" class="hide"></div>
        </div>
        <div class="j-type j-type-<?= Orders::TYPE_PRODUCT ?> <?= $f['type'] != Orders::TYPE_PRODUCT ? 'hidden' : '' ?>">
            <hr />
            <div><?
                foreach($catsOptions as $lvl => $v) {
                    ?><select class="cat-select" style="margin-right: 5px;<? if(empty($v['categories'])){ ?>display: none; <? } ?>" onchange="jOrdersOrdersList.onCategory($(this))"><?= $v['categories'] ?></select><?
                }
                ?></div><div id="j-f-cats-selected" class="hide"></div>
        </div>
        <div class="clear"></div>
    </form>
    <div id="j-massModerateInfo" class="well well-small hide">
        <span class="j-info"></span>&nbsp;
        <input type="submit" class="btn btn-mini btn-success success button" onclick="jOrdersOrdersList.massModerate();" value="<?= _t('orders', 'одобрить выбранные'); ?>" />
    </div>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="OrdersOrdersListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="20"><label class="checkbox inline"><input type="checkbox" id="j-check-all" onclick="jOrdersOrdersList.massModerate('check-all',this);" /></label></th>
        <th width="70">ID</th>
        <th class="left"><?= _t('orders', 'Заказ') ?></th>
        <th></th>
        <th width="135"><?= _t('', 'Created') ?></th>
        <th width="125"><?= _t('', 'Action') ?></th>
    </tr>
    </thead>
    <tbody id="OrdersOrdersList">
    <?= $list ?>
    </tbody>
</table>
<div id="OrdersOrdersListPgn"><?= $pgn ?></div>

<?= tplAdmin::blockStop(); ?>
<div>
    <div class="left">

    </div>
    <div class="right desc" style="width:60px; text-align:right;">

    </div>
</div>

<script type="text/javascript">
var jOrdersOrdersFormManager = (function(){
    var $progress, $block, $blockCaption, $formContainer, process = false;
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

    $(function(){
        $formContainer = $('#OrdersOrdersFormContainer');
        $progress = $('#OrdersOrdersProgress');
        $block = $('#OrdersOrdersFormBlock');
        $blockCaption = $block.find('span.caption');

        <? if(!empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
    });

    function onFormToggle(visible)
    {
        if(visible) {
            jOrdersOrdersList.toggle(false);
            if(jOrdersOrdersForm) jOrdersOrdersForm.onShow();
        } else {
            jOrdersOrdersList.toggle(true);
        }
    }

    function initForm(type, id, params)
    {
        if( process ) return;
        bff.ajax(ajaxUrl,params,function(data){
            if(data && (data.success || intval(params.save)===1)) {
                $blockCaption.html((type == 'add' ? '<?= _t('orders','Orders') ?> / <?= _t('orders', 'Добавление заказа') ?>' : '<?= _t('orders', 'Редактирование заказа') ?>'));
                $formContainer.html(data.form);
                $block.show();
                $.scrollTo( $blockCaption, {duration:500, offset:-300});
                onFormToggle(true);
                if(bff.h) {
                    window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                }
            } else {
                jOrdersOrdersList.toggle(true);
            }
        }, function(p){ process = p; $progress.toggle(); });
    }

    function action(type, id, params)
    {
        <? $tab = $this->input->get('ftab', TYPE_NOTAGS); ?>
        params = $.extend(params || {}, {act:type<?= ! empty($tab) ? ',ftab:\''.$tab.'\'' : '' ?>});
        switch(type) {
            case 'add':
            {
                if( id > 0 ) return action('edit', id, params);
                if($block.is(':hidden')) {
                    initForm(type, id, params);
                } else {
                    action('cancel');
                }
            } break;
            case 'cancel':
            {
                $block.hide();
                onFormToggle(false);
            } break;
            case 'edit':
            {
                if( ! (id || 0) ) return action('add', 0, params);
                params.id = id;
                initForm(type, id, params);
            } break;
        }
        return false;
    }

    return {
        action: action
    };
}());

var jOrdersOrdersList = (function(){
    var $progress, $block, $list, $listTable, $listPgn, filters, processing = false, tab = <?= $f['tab'] ?>, reg = {$ac:0,api:0, data:{}}, tabMod=2;
    var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';
    var catCache = {}, $catsSelected;
    var specCache = {}, $specsSelected;
    var $checkAll, $checkAllTh, $moderate, $moderateInfo;

    $(function(){
        $progress  = $('#OrdersOrdersProgress');
        $block     = $('#OrdersOrdersListBlock');
        $list      = $block.find('#OrdersOrdersList');
        $listTable = $block.find('#OrdersOrdersListTable');
        $listPgn   = $block.find('#OrdersOrdersListPgn');
        filters    = $block.find('#OrdersOrdersListFilters').get(0);

        $list.delegate('a.order-edit', 'click', function(){
            var id = intval($(this).attr('rel'));
            if(id>0) jOrdersOrdersFormManager.action('edit',id);
            return false;
        });

        $list.delegate('a.order-toggle', 'click', function(){
            var id = intval($(this).attr('rel'));
            var type = $(this).data('type');
            if(id>0) {
                var params = {progress: $progress, link: this};
                bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, params);
            }
            return false;
        });

        $list.delegate('a.order-del', 'click', function(){
            var id = intval($(this).attr('rel'));
            if(id>0) del(id, this);
            return false;
        });

        reg.$ac = $block.find('#j-f-region').autocomplete('<?= $this->adminLink('regionSuggest', 'geo') ?>',
            {valueInput: $block.find('[name="region"]'), suggest: <?= Geo::regionPreSuggest($f['country']) ?>, params:{reg:1, country:<?= $f['country'] ? $f['country'] : Geo::defaultCountry() ?>},
                onSelect: function(regionID, regionTitle, ex){
                    jOrdersOrdersList.submit();
                }}, function(){ reg.api = this; });

        $specsSelected = $('#j-f-specs-selected');
        $catsSelected = $('#j-f-cats-selected');

        $catsSelected.on('click', '.j-delete', function(){
            $(this).closest('.j-selected').remove();
            return false;
        });

        $specsSelected.on('click', '.j-delete', function(){
            $(this).closest('.j-selected').remove();
            return false;
        });

        $(window).bind('popstate',function(){
            if('state' in window.history && window.history.state === null) return;
            var loc = document.location;
            var actForm = /act=(add|edit)/.exec( loc.search.toString() );
            if( actForm!=null ) {
                var actId = /id=([\d]+)/.exec(loc.search.toString());
                jOrdersOrdersFormManager.action(actForm[1], actId && actId[1]);
            } else {
                jOrdersOrdersFormManager.action('cancel');
                updateList(false);
            }
        });

        <? if( ! empty($f['cats'])): foreach($f['cats'] as $v): ?>
            catSelected(<?= $v['id'] ?>, '<?= HTML::escape($v['title'], 'js') ?>');
        <? endforeach; endif; ?>
        <? if( ! empty($f['specs'])): foreach($f['specs'] as $v): ?>
            specSelected(<?= $v['id'] ?>, '<?= HTML::escape($v['title'], 'js') ?>');
        <? endforeach; endif; ?>

        $checkAll = $('#j-check-all');
        $checkAllTh = $checkAll.parents('th:eq(0)');
        $moderate = $('#j-massModerateInfo');
        $moderateInfo = $moderate.find('.j-info');

        massModerateActions();
    });

    function isProcessing()
    {
        return processing;
    }

    function del(id, link)
    {
        bff.ajaxDelete('Удалить?', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
        return false;
    }

    function updateList(updateUrl)
    {
        if(isProcessing()) return;
        var f = $(filters).serialize();
        bff.ajax(ajaxUrl, f, function(data){
            if(data) {
                $list.html( data.list );
                $listPgn.html( data.pgn );
                massModerateActions();
                if(updateUrl !== false && bff.h) {
                    window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                }
            }
        }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
    }

    function setPage(id)
    {
        filters.page.value = intval(id);
    }

    function catView(data, $select)
    {
        if(data === 'empty') {
            return;
        }

        if(data.subs>0) {
            $select.after('<select class="cat-select" style="margin-right: 5px;" onchange="jOrdersOrdersList.onCategory($(this))">'+data.cats+'</select>').show();
            return;
        } else {
            if(data.id){
                var title = '';
                $select.siblings().each(function(){
                    if(title.length) title = title + ' / ';
                    title = title + $(this).find(':selected').text();
                });
                if(title.length) title = title + ' / ';
                title = title + $select.find(':selected').text();
                catSelected(data.id, title);
            }
        }
    }

    function catSelected(id, title)
    {
        if( id > 0 && ! $catsSelected.find('.j-cat-id[value="'+id+'"]').length ){
            $catsSelected.append(
                '<span class="label j-selected" style="margin:0 2px 2px 2px;">'+title+'<a href="#" class="j-delete" style="margin-left: 3px;"><i class="icon-remove icon-white" style="margin-top: 0px;"></i></a><input type="hidden" name="cats[]" class="j-cat-id" value="'+id+'" /></span>'
            ).removeClass('hide');
        }
    }

    function specView(data, $select)
    {
        if(data === 'empty') {
            return;
        }

        if(data.subs>0) {
            $select.after('<select class="spec-select" data-lvl="'+data.lvl+'" style="margin-right: 5px;" onchange="jOrdersOrdersList.onSpecialization($(this))">'+data.cats+'</select>').show();
            return;
        }
    }

    function specSelected(id, title)
    {
        if( id > 0 && ! $specsSelected.find('.j-spec-id[value="'+id+'"]').length ){
            $specsSelected.append(
                '<span class="label j-selected" style="margin:0 2px 2px 2px;">'+title+'<a href="#" class="j-delete" style="margin-left: 3px;"><i class="icon-remove icon-white" style="margin-top: 0px;"></i></a><input type="hidden" name="specs[]" class="j-spec-id" value="'+id+'" /></span>'
            ).removeClass('hide');
        }
    }

    function massModerateActions()
    {
        $checkAllTh.toggle(tab === tabMod);
    }


    return {
        submit: function(resetForm)
        {
            if(isProcessing()) return false;
            setPage(1);
            if(resetForm) {
                filters['title'].value = '';
                filters['region'].value = '';
                <? if(Geo::countrySelect()): ?>
                filters['country'].value = 0;
                <? endif; ?>
                filters['user'].value = '';
                reg.$ac.val('');

                jOrdersOrdersList.onType();
            }
            updateList();
        },
        page: function (id)
        {
            if(isProcessing()) return false;
            setPage(id);
            updateList();
        },
        onTab: function(tabNew, link)
        {
            if(isProcessing() || tabNew == tab) return false;
            setPage(1);
            tab = filters.tab.value = tabNew;
            updateList();
            $(link).parent().addClass('tab-active').siblings().removeClass('tab-active');
            $moderate.hide();
            return false;
        },
        refresh: function(resetPage,updateUrl)
        {
            if(resetPage) setPage(0);
            updateList(updateUrl);
        },
        toggle: function(show)
        {
            if(show === true) {
                $block.show();
                if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
            }
            else $block.hide();
        },
        onType: function()
        {
            $block.find('.j-type').addClass('hidden');
            $catsSelected.empty();
            $specsSelected.empty();
            $block.find('.j-type-' + $(filters['type']).val()).removeClass('hidden');
            jOrdersOrdersList.submit();
        },
        onCountry: function()
        {
            var country = intval(filters['country'].value);
            if(country){
                reg.api.setParam('country', country);

                if( reg.data.hasOwnProperty(country) ) {
                    reg.api.setSuggest(reg.data[country], true);
                } else {
                    bff.ajax('<?= $this->adminLink('ajax&act=country-presuggest', 'geo') ?>', {country:country}, function(data){
                        reg.data[country] = data;
                        reg.api.setSuggest(data, true);
                    });
                }
            }
            jOrdersOrdersList.submit();
        },
        onCategory: function($select)
        {
            catView('empty');

            var catID = intval($select.val());
            $select.nextAll().remove();

            if( ! catID) return;

            if(catCache.hasOwnProperty(catID)) {
                catView( catCache[catID], $select );
            } else {
                bff.ajax('<?= $this->adminLink('products&act=category-data', 'shop'); ?>', {'cat_id': catID}, function(data){
                    if(data && data.success) {
                        catView( (catCache[catID] = data), $select );
                    }
                }, function(){
                    $progress.toggle();
                });
            }
        },
        onSpecialization: function($select)
        {
            specView('empty');

            var catID = intval($select.val());
            var lvl = $select.data('lvl');
            if(lvl == 'spec'){
                specSelected(catID, $select.find(':selected').text());
            } else {
                $select.nextAll().remove();

                if(specCache.hasOwnProperty(catID)) {
                    specView( specCache[catID], $select );
                } else {
                    bff.ajax('<?= $this->adminLink('specializations_list&act=category-data', 'specializations'); ?>', {'cat_id': catID, 'lvl':$select.data('lvl')}, function(data){
                        if(data && data.success) {
                            specView( (specCache[catID] = data), $select );
                        }
                    }, function(){
                        $progress.toggle();
                    });
                }
            }

        },
        massModerate: function(act, extra)
        {
            var $c = $('input.j-item-check:visible:checked:not(:disabled)');
            var $all = $('input.j-item-check:visible:not(:disabled)');

            switch(act) {
                case 'check-all': {
                    if(!$all.length) return false;
                    if(!$c.length || $c.length < $all.length) {
                        $all.prop('checked', true); // доотмечаем неотмеченные
                        $(extra).prop('checked', true);
                        $moderateInfo.html('<?= _t('orders', 'Выбрано'); ?> <strong>' + $all.length + '</strong> ' + bff.declension($all.length,['<?= _t('orders', 'заказ'); ?>','<?= _t('orders', 'заказа'); ?>','<?= _t('orders', 'заказов'); ?>'], false));
                        $moderate.show();
                    } else {
                        $all.prop('checked',false); // снимаем все
                        $(extra).prop('checked',false);
                        $moderate.hide();
                    }
                    return false;
                } break;
                case 'check': {
                    if(!$c.length || $c.length <= 0) {
                        $moderate.hide();
                    } else {
                        $moderateInfo.html('<?= _t('orders', 'Выбрано'); ?> <strong>' + $c.length + '</strong> ' + bff.declension($c.length,['<?= _t('orders', 'заказ'); ?>','<?= _t('orders', 'заказа'); ?>','<?= _t('orders', 'заказов'); ?>'], false));
                        $moderate.show();
                    }

                    if(!$c.length || $c.length < $all.length) $checkAll.prop('checked',false);
                    else $checkAll.prop('checked',true);

                    return false;
                } break;
            }

            if(intval($c.length)<=0){ bff.error('<?= _t('orders', 'Нет отмеченных объявлений'); ?>'); return; }
            if(!bff.confirm('sure')) return;

            if(processing) return false;
            processing = true;
            bff.ajax(ajaxUrl+'mass-approve', $c.serialize(), function(resp){
                processing = false;
                $moderate.hide();
                if(resp && resp.success) {
                    bff.success('<?= _t('orders', 'Успешно одобрено:'); ?> '+resp.updated);
                    setTimeout(function(){ updateList(); }, 1000);
                }
            }, $progress);
        }


    };
}());
</script>