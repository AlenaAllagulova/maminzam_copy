<?php
tpl::includeJS('svc.orders', false, 2);
$cur = Site::currencyDefault();
$sSum = 0;
foreach($svc as $k => $v){
    switch($v['id']){
        case Orders::SVC_FIX:    $svc[$k]['fa'] = 'fa-thumb-tack'; break;
        case Orders::SVC_UP:     $svc[$k]['fa'] = 'fa-arrow-up'; break;
        case Orders::SVC_MARK:   $svc[$k]['fa'] = 'fa-bolt'; break;
        case Orders::SVC_HIDDEN: $svc[$k]['fa'] = 'fa-eye-slash'; break;
    }
}
?>
    <div class="container">

        <section class="l-mainContent">

            <div class="row">

                <div class="col-md-8 col-md-offset-2">

                    <?= tpl::getBreadcrumbs($breadcrumbs); ?>

                    <div class="p-profileContent" id="j-order-block">
                        <?= $list ?>
                    </div>

                        <form method="post" action="" id="j-svc-orders">
                            <? if($bStatus): ?><input type="hidden" name="status" value="1" /><? endif; ?>

                            <h4><?= _t('svc', '1. Выберите услугу'); ?></h4>

                            <div class="panel-group o-advertise-accordion">

                                <? foreach($svc as $v):
                                if( ! $v['on']) continue;
                                $bPreset = $preset & $v['id'];
                                if($bPreset){
                                    $sum = $v['price'];
                                    if($v['id'] == Orders::SVC_FIX && ! empty($v['per_day'])){
                                        $days = ! empty($svc_fixed_days) ? $svc_fixed_days : $v['period'];
                                        $sum = round($v['price'] * $days, 2);
                                    }
                                    $sSum += $sum;
                                }
                                ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a class="j-accordion">
                                            <div class="checkbox">
                                              <label>
                                                  <input type="checkbox" name="svc[]" value="<?= $v['id'] ?>" data-price="<?= $v['price'] ?>" <?= $bPreset ? 'checked="checked"' : '' ?> />
                                                  <i class="fa <?= $v['fa'] ?>"></i><span><?= $v['title_view'][LNG] ?></span>
                                                  <div class="o-advertise-price"><?= round($v['price']) ?> <?= $cur; ?></div>
                                              </label>
                                            </div>
                                        </a>
                                    </div>
                                    <div id="j-svc-<?= $v['id'] ?>" class="panel-collapse collapse <?= $bPreset ? 'in' : '' ?>">
                                        <div class="panel-body">
                                            <? if($v['id'] == Orders::SVC_FIX && ! empty($v['per_day'])): ?>
                                                <div class="o-advertise-accordion-count form-inline">
                                                    <div class="form-group">
                                                        <label for="j-fixed-count"><?= _t('', 'Закрепить на'); ?></label>
                                                        <input type="text" name="fixed_days" maxlength="3" class="form-control input-sm" id="j-fixed-count" value="<?= ! empty($svc_fixed_days) ? $svc_fixed_days : $v['period'] ?>">
                                                        <?= _t('', 'дней'); ?>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                            <?= $v['description_full'][LNG] ?>
                                        </div>
                                    </div>
                                </div>
                                <? endforeach; ?>
                            </div>

                            <h4><?= _t('svc', '2. Выберите способ оплаты'); ?></h4>

                            <?= Bills::i()->payMethodsList(); ?>

                            <p>
                                <h5 class="mrgt0 text-center-mobile"><?= _t('svc', 'Всего к оплате:'); ?> <span class="j-sum"><?= round($sSum) ?></span> <?= $cur ?></h5>
                            </p>

                            <div class="c-formSubmit">
                                <button class="btn btn-primary c-formSuccess j-submit <?= $sSum <=0 ? 'hidden' : '' ?>"><?= _t('svc', 'Оплатить'); ?></button>
                                <a class="c-formCancel j-cancel" href="#"><?= _t('form', 'Отмена'); ?></a>
                            </div>

                        </form>

                </div>

            </div>

        </section>

    </div>
<script type="text/javascript">
    <? js::start(); ?>
    $(function(){
        jSvcOrders.init(<?= func::php2js(array(
            'lang' => array(
                'success' => _t('svc', 'Услуга успешно активирована'),
                'select_pay' => _t('svc', 'Выберите способ оплаты'),
            ),
            'balance' => User::balance(),
            'fix'  => Orders::SVC_FIX,
            'mark' => Orders::SVC_MARK,
            'preset' => $preset,
            'svc_marked' => $svc_marked,
            'svc_fixed'  => $svc_fixed,
        )) ?>);
    });
    <? js::stop(); ?>
</script>