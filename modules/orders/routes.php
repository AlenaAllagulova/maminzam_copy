<?php

return [
    # просмотр заказа
    'orders-view' => [
        'pattern'  => 'orders/([\d]+)\-(.*)\.html',
        'callback' => 'orders/view/id=$1',
        'priority' => 30,
    ],
    # действия с заказом
    'orders-action' => [
        'pattern'  => 'orders/(add|edit|promote|activate|status|rss)',
        'callback' => 'orders/$1/',
        'priority' => 40,
    ],
    # поиск по ключевому слову
    'orders-search-key' => [
        'pattern'  => 'orders/(.+)/',
        'callback' => 'orders/search/orders=$1',
        'priority' => 48,
    ],

    # поиск с заказом
    'orders-search' => [
        'pattern'  => 'orders/(.+)',
        'callback' => 'orders/search/orders=$1',
        'priority' => 50,
    ],

    # заказы  главная
    'orders-main' => [
        'pattern'  => 'orders(/|)',
        'callback' => 'orders/search',
        'priority' => 52,
    ],
];