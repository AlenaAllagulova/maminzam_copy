<?php

class Orders_ extends OrdersBase
{
    public function init()
    {
        parent::init();

        if (bff::$class == $this->module_name) {
            bff::setActiveMenu('//orders');
        }
    }

    /**
     * Добавление заказа
     */
    public function add()
    {
        $this->security->setTokenPrefix('order-item-form');

        $aData = $this->validateOrderData(0, Request::isPOST());
        $nUserID = User::id();

        $invitesEnabled = static::invitesEnabled();
        $registerPhone = Users::registerPhone(); # задействовать: номер телефона

        $workerInvite = 0;
        if($invitesEnabled){
            $workerInvite = $this->input->postget('worker_invite', TYPE_UINT);
        }

        if (Request::isPOST() && ! $workerInvite) {
            do {
                $aResponse = array();
                $bNeedActivation = false;

                if (!$nUserID) {
                    # проверяем IP для неавторизованных
                    $mBanned = Users::i()->checkBan(true);
                    if ($mBanned) {
                        $this->errors->set(_t('orders', 'В доступе отказано по причине: [reason]', array('reason' => $mBanned)));
                        break;
                    }

                    if ($registerPhone) {
                        $phone = $this->input->post('phone', TYPE_NOTAGS, array('len' => 30, 'len.sys' => 'orders.phone.limit'));
                        if ( ! $this->input->isPhoneNumber($phone)) {
                            $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                            break;
                        }
                    }

                    # регистрируем нового или задействуем существующего пользователя по E-mail или Телефону
                    $sEmail = $this->input->post('email', TYPE_NOTAGS, array('len' => 100, 'len.sys' => 'orders.email.limit')); # E-mail
                    $sName = $this->input->post('name', TYPE_NOTAGS, array('len' => 75, 'len.sys' => 'orders.name.limit'));
                    if (!$this->input->isEmail($sEmail)) {
                        $this->errors->set(_t('orders', 'E-mail адрес указан некорректно'));
                        break;
                    }
                    # антиспам фильтр: временные ящики
                    if(Site::i()->spamEmailTemporary($sEmail)){
                        $this->errors->set(_t('', 'Указанный вами email адрес находится в списке запрещенных, используйте например @gmail.com'));
                        break;
                    }

                    $aUserData = Users::model()->userDataByFilter(($registerPhone ? array('phone_number'=>$phone) :  array('email' => $sEmail)),
                        array('user_id', 'activated', 'blocked', 'blocked_reason', 'type', 'activate_key', 'email')
                    );
                    if (empty($aUserData)) {
                        # проверяем уникальность email адреса
                        if ($registerPhone && Users::model()->userEmailExists($sEmail, $aUserData['user_id'])) {
                            $this->errors->set(_t('users', 'Пользователь с таким e-mail адресом уже зарегистрирован. <a [link_forgot]>Забыли пароль?</a>',
                                array('link_forgot' => 'href="' . Users::url('forgot') . '"')
                            ), 'email');
                            break;
                        }

                        if (!$this->errors->no('orders.order.add.step1.guest',array('id'=>0,'data'=>&$aData,'email'=>&$sEmail,'name'=>&$sName))) {
                            break;
                        }

                        # Создаем аккаунт пользователя
                        $aRegisterData = array(
                            'email' => $sEmail,
                            'name'  => $sName,
                        );
                        if ($registerPhone) $aRegisterData['phone_number'] = $phone;
                        if (Users::rolesEnabled()) {
                            $roleID = $this->input->post('role_id', TYPE_UINT);
                            if (!array_key_exists($roleID, Users::roles())) {
                                $roleID = Users::ROLE_PRIVATE;
                            }
                            $aRegisterData['role_id'] = $roleID;
                        }
                        if (Users::useClient()) {
                            $aRegisterData['type'] = Users::TYPE_CLIENT;
                        }
                        $aUserData = Users::i()->userRegister($aRegisterData);

                        if (empty($aUserData['user_id'])) {
                            $this->errors->set(_t('orders', 'При публикации заказа возникла ошибка, обратитесь к администратору'));
                            break;
                        }
                    } else {
                        # пользователь существует
                        # его аккаунт заблокирован
                        if ($aUserData['blocked']) {
                            $this->errors->set(_t('orders', 'В доступе отказано по причине: [reason]', array('reason' => $aUserData['blocked_reason'])));
                            break;
                        }
                        # исполнитель
                        if (Users::useClient() && $aUserData['type'] == Users::TYPE_WORKER) {
                            $this->errors->set(_t('orders', 'Добавлять заказы могут только заказчики'));
                            break;
                        }
                        $sEmail = $aUserData['email'];
                    }
                    $nUserID = $aUserData['user_id'];
                    $aData['user_id'] = $nUserID;
                    $bNeedActivation = true;
                } else {
                    if (!User::isClient() && Users::useClient()) {
                        $this->errors->set(_t('orders', 'Добавлять заказы могут только заказчики'));
                        break;
                    }
                    # если пользователь авторизован и при этом не вводил номер телефона ранее
                    if ($registerPhone){
                        $aUserData = User::data(array('user_id', 'email', 'name', 'phone_number', 'phone_number_verified'), true);
                        if (empty($aUserData['phone_number']) || !$aUserData['phone_number_verified']) {
                            $phone = $this->input->post('phone', TYPE_NOTAGS, array('len'=>30, 'len.sys' => 'orders.phone.limit'));
                            if (!$this->input->isPhoneNumber($phone)) {
                                $this->errors->set(_t('users', 'Номер телефона указан некорректно'), 'phone');
                                break;
                            }
                            if (Users::model()->userPhoneExists($phone, $nUserID)) {
                                $this->errors->set(_t('users', 'Пользователь с таким номером телефона уже зарегистрирован.'), 'phone');
                                break;
                            }
                            $aActivation = Users::i()->getActivationInfo();
                            Users::model()->userSave($nUserID, array(
                                'activate_key'    => $aActivation['key'],
                                'activate_expire' => $aActivation['expire'],
                                'phone_number'    => $phone,
                            ));
                            $aUserData['activate_key'] = $aActivation['key'];
                            $sName = $aUserData['name'];
                            $sEmail = $aUserData['email'];
                            $bNeedActivation = true;
                        }
                    }
                }

                if (!$this->errors->no('orders.order.add.step2',array('id'=>0,'data'=>&$aData,'activation'=>$bNeedActivation))) {
                    break;
                }

                $trusted = User::isTrusted($this->module_name);
                if ($bNeedActivation) {
                    $aData['status'] = self::STATUS_NOTACTIVATED;
                    $aActivation = $this->getActivationInfo();
                    $aData['activate_key'] = $aActivation['key'];
                    $aData['activate_expire'] = $aActivation['expire'];
                } else {
                    $aData['status'] = self::STATUS_OPENED;
                    $aData['moderated'] = $trusted ? 1 : 0; # помечаем на модерацию
                    if($invitesEnabled && ! $aData['moderated'] && $aData['visibility'] == static::VISIBILITY_PRIVATE){
                        $aData['moderated'] = 2;    # для приватных заказов - постмодерация
                    }
                }

                if (!$this->errors->no('orders.order.add.step3',array('id'=>0,'data'=>&$aData,'activation'=>$bNeedActivation))) {
                    break;
                }

                # создаем заказ
                $nOrderID = $this->model->orderSave(0, $aData, 'd');
                if (!$nOrderID) {
                    $this->errors->reloadPage();
                    break;
                } else {
                    if (!$bNeedActivation) {
                        # накручиваем счетчик кол-ва заказов пользователя
                        $this->security->userCounter('orders', 1, $nUserID);
                        if ($trusted || !static::premoderation()) {
                            # уведомление исполнителей о новом заказе
                            $this->enotifyNewOrder($nOrderID);
                        }
                        $aResponse['id'] = $nOrderID;
                    }
                }

                # загружаем изображения
                if (static::imagesLimit()) {
                    # сохраняем / загружаем изображения
                    $oImages = $this->orderImages($nOrderID);
                    $oImages->setAssignErrors(false);
                    if ($this->input->post('images_type', TYPE_STR) == 'simple') {
                        # загружаем
                        if (!empty($_FILES)) {
                            for ($i = 1; $i <= $oImages->getLimit(); $i++) {
                                $oImages->uploadFILES('images_simple_' . $i);
                            }
                            # удаляем загруженные через "удобный способ"
                            $aImages = $this->input->post('images', TYPE_ARRAY_STR);
                            $oImages->deleteImages($aImages);
                        }
                    } else {
                        # перемещаем из tmp-директории в постоянную
                        $oImages->saveTmp('images');
                    }
                }

                # загружаем файлы
                if (static::attachmentsLimit()) {
                    $this->orderAttachments($nOrderID)->saveTmp('attach');
                }

                # теги
                $this->orderTags()->tagsSave($nOrderID);

                # обновляем счетчик заказов "на модерации"
                if (!$trusted) {
                    $this->moderationCounterUpdate(1);
                }

                # отправляем письмо cо ссылкой на активацию заказа
                if ($bNeedActivation) {
                    if ($registerPhone) {
                        # отправляем sms c кодом активации
                        Users::i()->sms(false)->sendActivationCode($phone, $aUserData['activate_key']);
                        $aMailData = array(
                            'id'            => $aUserData['user_id'],
                            'name'          => $sName,
                            'password'      => isset($aUserData['password']) ? $aUserData['password'] : '',
                            'phone'         => $phone,
                            'email'         => $sEmail,
                            'activate_link' => isset($aUserData['activate_link']) ? $aUserData['activate_link'] : ''
                        );
                        # Сохраняем данные для повторной отправки кода
                        $this->security->sessionStart();
                        $this->security->setSESSION('users-register-data', $aMailData);
                    } else {
                        $aMailData = array(
                            'name'          => $sName,
                            'email'         => $sEmail,
                            'activate_link' => $aActivation['link'] . '_' . $nOrderID
                        );
                        bff::sendMailTemplate($aMailData, 'orders_order_activate', $sEmail);
                    }
                }

                $svc = $this->input->post('svc', TYPE_ARRAY_UINT);
                $svc = array_sum($svc);
                if ($svc) {
                    $params = array('id' => $nOrderID, 'preset' => $svc, 'status' => 1);
                    $fixedDays = $this->input->post('svc_fixed_days', TYPE_UINT);
                    if ($fixedDays) {
                        $params['svc_fixed_days'] = $fixedDays;
                    }
                    $aResponse['redirect'] = static::url('promote', $params);
                } else {
                    $aResponse['redirect'] = static::url('status', array('id' => $nOrderID));
                }

                # создадим приглашение для исполнителя, если необходимо
                $inviteUserID = $this->input->getpost('create_invite', TYPE_UINT);
                if ($inviteUserID) {
                    $this->createInvite($nOrderID, $inviteUserID);
                }

            } while (false);
            $this->iframeResponseForm($aResponse);
        }

        if ($nUserID && Users::useClient() && !User::isClient()) {
            return $this->viewPHP($aData, 'add.forbidden');
        }

        if($workerInvite){
            $aData['workerInvite'] = Users::model()->userData($workerInvite, array('user_id', 'type', 'login', 'name', 'surname', 'avatar', 'sex', 'pro', 'verified'));
            if(empty($aData['workerInvite']) || $aData['workerInvite']['type'] != Users::TYPE_WORKER){
                unset($aData['workerInvite']);
            }else {
                $aData['type'] = static::TYPE_SERVICE;
            }
        }

        if ($registerPhone && $nUserID) {
            $aUserData = User::data(array('phone_number', 'phone_number_verified'), true);
            if (empty($aUserData['phone_number']) || !$aUserData['phone_number_verified']) {
                $aData['phone_on'] = true;
            }
        }


        # SEO: Добавление заказа
        $url = static::url('add', array(), true);
        $this->urlCorrection(static::urlDynamic($url));
        $this->seo()->canonicalUrl($url);
        $this->setMeta('add', array(), $aData);

        return $this->form(0, $aData);
    }

    /**
     * Редактирование заказа
     */
    public function edit()
    {
        $this->security->setTokenPrefix('order-item-form');

        if (!User::id()) {
            $this->redirect(Users::url('login'), false, true);
        }

        $nOrderID = $this->input->get('id', TYPE_UINT);
        if (!$nOrderID) {
            $this->errors->error404();
        }
        $aOrderData = $this->model->orderData($nOrderID, array(), true);
        if (empty($aOrderData)) {
            $this->errors->error404();
        }

        if (!$this->isOrderOwner($nOrderID, $aOrderData['user_id'])) {
            if (Request::isPOST()) {
                $this->errors->reloadPage();
                $this->iframeResponseForm();
            }
            return $this->showForbidden('', _t('orders', 'Вы не является владельцем данного заказа'));
        }

        if (Request::isPOST()) {
            $nTypeID = $aOrderData['type']; // При редактировании изменить тип нельзя
            $aData = $this->validateOrderData($nOrderID, true, $nTypeID);
            $aResponse = array();

            if ($this->errors->no('orders.order.edit.step1',array('id'=>$nOrderID,'data'=>&$aData,'before'=>$aOrderData))) {

                if ($aOrderData['status'] == self::STATUS_BLOCKED) {
                    # заказ заблокирован, помечаем на проверку модератору
                    $aData['moderated'] = 0;
                }

                # доверенный пользователь: без модерации
                $trusted = User::isTrusted($this->module_name);
                # помечаем на модерацию при изменении: названия, описания
                if ($aData['title'] != $aOrderData['title'] || $aData['descr'] != $aOrderData['descr']) {
                    if ($aOrderData['moderated']!=0 && !$trusted) {
                        $aData['moderated'] = 2;
                    }
                }

                $this->model->orderSave($nOrderID, $aData, 'd');

                if (static::imagesLimit()) {
                    # сохраняем / загружаем изображения
                    $oImages = $this->orderImages($nOrderID);
                    if ($this->input->post('images_type', TYPE_STR) == 'simple') {
                        # загружаем
                        if (!empty($_FILES) && $aOrderData['imgcnt'] < $oImages->getLimit()) {
                            for ($i = 1; $i <= $oImages->getLimit(); $i++) {
                                $oImages->uploadFILES('images_simple_' . $i);
                            }
                            $aResponse['redirect'] = static::url('edit', array('id' => $nOrderID));
                        }
                    } else {
                        # сохраняем порядок изображений
                        $aImages = $this->input->post('images', TYPE_ARRAY_STR);
                        $oImages->saveOrder($aImages, false);
                    }

                    # помечаем на модерацию при изменении: фотографий
                    if ($oImages->newImagesUploaded($this->input->post('images_hash', TYPE_STR))
                        && $aOrderData['moderated']!=0 && !$trusted) {
                        $this->model->orderSave($nOrderID, array('moderated' => 2));
                    }
                }
                if (static::attachmentsLimit()) {
                    $oAttachments = $this->orderAttachments($nOrderID);
                    # помечаем на модерацию при изменении: прикреплений
                    if ($oAttachments->newAttachmentsUploaded($this->input->post('attachments_hash', TYPE_STR))
                        && $aOrderData['moderated']!=0 && !$trusted) {
                        $this->model->orderSave($nOrderID, array('moderated' => 2));
                    }
                }

                # обновляем счетчик заказов "на модерации"
                $this->moderationCounterUpdate();

                # теги
                $this->orderTags()->tagsSave($nOrderID);

                $svc = $this->input->post('svc', TYPE_ARRAY_UINT);
                $svc = array_sum($svc);
                if($svc){
                    $params = array('id' => $nOrderID, 'preset' => $svc);
                    $fixedDays = $this->input->post('svc_fixed_days', TYPE_UINT);
                    if ($fixedDays) {
                        $params['svc_fixed_days'] = $fixedDays;
                    }
                    $aResponse['redirect'] = static::url('promote', $params);
                } else {
                    $aResponse['redirect'] = static::url('view', array('id' => $nOrderID, 'keyword' => $aData['keyword']));
                }

            }
            $this->iframeResponseForm($aResponse);
        }

        # SEO:
        bff::setMeta(_t('orders', 'Редактирование заказа'));
        $this->seo()->robotsIndex(false);
        return $this->form($nOrderID, $aOrderData);
    }

    /**
     * Форма заказа
     * @param integer $nOrderID ID заказа
     * @param array $aData @ref
     * @return string HTML
     */
    protected function form($nOrderID, array &$aData)
    {
        $aData['id'] = $nOrderID;
        $aData['img'] = $this->orderImages($nOrderID);
        $aData['attach'] = $this->orderAttachments($nOrderID);
        $aData['aSvc'] =  Svc::model()->svcListing(Svc::TYPE_SERVICE, $this->module_name);

        if( ! empty($aData['type'])) {
            if ( ! empty($aData['specs'])) {
                if ($aData['type'] == static::TYPE_SERVICE) {
                    $aFirstSpec = reset($aData['specs']);
                    if (static::dynpropsEnabled()) {
                        $aData['dp'] = Specializations::i()->dpOrdersForm($aFirstSpec['spec_id'], false, $aData, 'd', 'form.dp', $this->module_dir_tpl);
                    } else {
                        $aData['dp'] = Specializations::i()->dpForm($aFirstSpec['spec_id'], false, $aData, 'd', 'form.dp', $this->module_dir_tpl);
                    }
                    if(empty($aFirstSpec['keyword'])){
                        $key = array_keys($aData['specs']);
                        $key = reset($key);
                        $spec = Specializations::model()->specializationData($aFirstSpec['spec_id']);
                        if( ! empty($spec)) {
                            $aData['specs'][$key]['keyword'] = $spec['keyword'];
                            $aData['specs'][$key]['spec_title'] = $spec['title'];
                            if(Specializations::catsOn()){
                                $cat = Specializations::model()->categoryData($aFirstSpec['cat_id']);
                                if( ! empty($cat)) {
                                    $aData['specs'][$key]['lvl'] = $cat['numlevel'];
                                    $aData['specs'][$key]['pid'] = $cat['pid'];
                                    $aData['specs'][$key]['cat_title'] = $cat['title'];
                                }
                            }
                        }
                    }
                }
            }
            if( ! empty($aData['cats'])) {
                if ($aData['type'] == static::TYPE_PRODUCT && static::useProducts()) {
                    $aFirstCat = reset($aData['cats']);
                    $aData['dp'] = Shop::i()->dpForm($aFirstCat['cat_id'], false, $aData, 'd', 'form.dp', $this->module_dir_tpl);
                }
            }
        }

        if ($nOrderID) {
            $aData['attachments'] = array();
            if (static::imagesLimit()) {
                $aImages = $aData['img']->getData($aData['imgcnt']);
                $aData['images'] = array();
                foreach ($aImages as $v) {
                    $aData['images'][] = array(
                        'id'       => $v['id'],
                        'tmp'      => false,
                        'filename' => $v['filename'],
                        'i'        => $aData['img']->getURL($v, OrdersOrderImages::szSmall, false)
                    );
                }
                $aData['imghash'] = $aData['img']->getLastUploaded();
            }
            if (static::attachmentsLimit()) {
                $aAttachments = $aData['attach']->getData($aData['attachcnt']);
                foreach ($aAttachments as $v) {
                    $aData['attachments'][] = array(
                        'id'       => $v['id'],
                        'tmp'      => false,
                        'filename' => $v['filename'],
                        'i'        => $aData['attach']->getURL($v, false),
                        'size'     => tpl::filesize($v['size']),
                        'origin'   => $v['origin'],
                    );
                }
                $aData['attachhash'] = $aData['attach']->getLastUploaded();
            }

            # региональная стоимость услуг
            if (!empty($aData['regions'])) {
                $cityID = reset($aData['regions']);
                $cityID = $cityID['reg3_city'];
                $ids = array();
                foreach ($aData['aSvc'] as $v) $ids[] = $v['id'];
                $aSvcPrices = $this->model->svcPricesEx($ids, $cityID);
                foreach ($aData['aSvc'] as & $v) {
                    if (!empty($aSvcPrices[ $v['id'] ]) && $aSvcPrices[ $v['id'] ] > 0) {
                        $v['price'] = $aSvcPrices[ $v['id'] ];
                    }
                } unset($v);
            }
        } else {
            $aData['images'] = array();
            $aData['imgcnt'] = 0;
            $aData['imghash'] = '';

            $aData['attachments'] = array();
            $aData['attachcnt'] = 0;
            $aData['attachhash'] = '';
            $aData['svc'] = 0;

            # в поднятии нет необходимости
            unset($aData['aSvc']['orders_up']);
        }
        return $this->viewPHP($aData, 'form');
    }

    /**
     * Вывод сообщения о текущем статусе заказа
     */
    public function status()
    {
        bff::setMeta(_t('orders', 'Информация о заказе'));

        $nOrderID = $this->input->get('id', TYPE_UINT);
        if (!$nOrderID) {
            $this->errors->error404();
        }

        $aData = $this->model->orderData($nOrderID, array('moderated', 'keyword', 'status', 'user_id'));
        if (empty($aData)) {
            $this->errors->error404();
        }

        if( ! User::id()){
            if ($aData['status'] != static::STATUS_NOTACTIVATED) {
                return $this->showForbiddenGuests();
            }
        } else {
            if ($aData['moderated'] > 0) {
                $this->redirect(static::url('view', array('id' => $nOrderID, 'keyword' => $aData['keyword'])));
            }
        }
        if ($aData['status'] == static::STATUS_NOTACTIVATED && Users::registerPhone()) {
            if (Request::isPOST()) {
                Users::i()->register_phone($aData['user_id'], static::url('status', array('id' => $nOrderID)));
            }
            $user = Users::model()->userData($aData['user_id'], array('phone_number', 'phone_number_verified'));
            if ( ! empty($user['phone_number']) && ! empty($user['phone_number_verified'])) {
                $this->model->orderSave($nOrderID, array(
                        'activate_key' => '', # чистим ключ активации
                        'status_prev'  => self::STATUS_NOTACTIVATED,
                        'status'       => self::STATUS_OPENED,
                        'moderated'    => 0, # помечаем на модерацию
                    )
                );
                # обновляем счетчик заказов "на модерации"
                $this->moderationCounterUpdate();
                $this->redirect(static::url('status', array('id' => $nOrderID)));
            } else {
                $user['phone'] = $user['phone_number'];
                $user['url'] = static::url('status', array('id' => $nOrderID));
                $aData['phone_form'] = Users::i()->viewPHP($user, 'auth.register.phone');
            }
        }
        return $this->viewPHP($aData, 'status');
    }

    /**
     * Просмотр заказа
     */
    public function view()
    {
        if(!User::id()){
            $this->errors->error404();
        }
        $nOrderID = $this->input->get('id', TYPE_UINT);
        if (!$nOrderID) {
            $this->errors->error404();
        }
        $aData = $this->model->orderDataView($nOrderID);
        if (empty($aData)) {
            $this->errors->error404();
        }
        if ($aData['svc'] & static::SVC_HIDDEN) {
            $this->seo()->robotsIndex(false);
            if( ! User::id()){
                $this->errors->error404();
            }
        }
        $userID = User::id();
        $owner = User::isCurrent($aData['user_id']);

        $offerAddEnable = true;
        if (static::invitesEnabled() && $aData['visibility'] == static::VISIBILITY_PRIVATE) {
            do {
                if ($owner) break;

                if ($userID) {
                    $invites = $this->model->invitesDataByFilter(array(
                        'user_id'  => $userID,
                        'order_id' => $nOrderID,
                    ));
                    if ($invites) {
                        $offerAddEnable = false;
                        break;
                    }

                    $offers = $this->model->offersDataByFilter(array('order_id' => $nOrderID, 'user_id' => $userID));
                    if ($offers) {
                        break;
                    }
                }
                $this->errors->error404();
            } while(false);
        }

        if ($aData['moderated'] == 0 && static::premoderation() && ! $owner) {
            return $this->showForbidden(_t('orders', 'Заказ №[id]', array('id'=>$nOrderID)), _t('orders', 'Заказ ожидает проверки модератора'));
        }

        # общие данные с ходом работ
        $this->viewDataPrepare($aData);

        $aData['offers_list'] = $this->offers_list($nOrderID, $aData, $bMyPresent, false);

        $this->getPerformerStateBlock($aData, $nOrderID);

        # Форма добавления заявки
        $aData['offerAddForm'] = '';
        if ( ! $bMyPresent && $offerAddEnable) {
            if ($this->isOfferAddAllow($aData, $nAddProhibited)) {
                $aData['offerAddForm'] = $this->offer_add($aData);
            } else {
                $aProhibited = array(
                    'nProhibited' => $nAddProhibited,
                );
                $aData['offerAddForm'] = $this->viewPHP($aProhibited, 'offer.add.prohibited');
            }
        }
        # Данные о выбранном исполнителе
        if ($aData['performer_id']) {
            $aData['performer'] = Users::model()->userData($aData['performer_id'], [
                'login',
                'name',
                'surname',
                'pro',
                'sex',
                'verified',
                'avatar',
                'specs',
            ]);
            if ($owner && static::ordersOpinions()) {
                $aData['performer_offer'] = $this->model->orderOffers($nOrderID, array('user_id' => $aData['performer_id']));
                $aData['performer_offer'] = reset($aData['performer_offer']);
            }
            $aData['performer']['contacts_phones'] = Users::i()->getUserContacts($aData['performer_id'], true);
        }

        # Данные о ходе работ по договору
        if (bff::fairplayEnabled() && ( $owner || User::isCurrent($aData['performer_id']))) {
            $workfow = Fairplay::model()->workflowData(array('order_id' => $nOrderID), array('id'));
            if ( ! empty($workfow['id'])) {
                $workfow['url'] = Fairplay::url('view', array('id' => $workfow['id'], 'keyword' => $aData['keyword']));
                $aData['workfow'] = $workfow;
            }
        }

        if (static::invitesEnabled() && $owner) {
            $invites = func::unserialize($aData['invited_users']);
            if ( ! empty($invites)) {
                $aData['invites'] = $this->model->orderInvitesList(array_keys($invites));
                foreach ($invites as $v) {
                    if ( ! isset($aData['invites'][ $v['user_id'] ])) continue;
                    $aData['invites'][ $v['user_id'] ]['created'] = $v['created'];
                }
                uasort($aData['invites'], function($a, $b){
                    return $a['created'] < $b['created'];
                });
            }
        }

        # Счетчик просмотров
        if ($userID != $aData['user_id'] && ! Request::isRefresh()) {
            $this->model->orderSave($nOrderID, array('views_total = views_total + 1'));
        }

        # SEO: Просмотр заказа
        $seoTags = array(); foreach ($aData['tags'] as $v) $seoTags[] = $v['tag'];
        $seoURL = static::url('view', array('id' => $nOrderID, 'keyword' => $aData['keyword']), true);
        $seoData = array(
            'title'       => tpl::truncate($aData['title'], config::sysAdmin('orders.view.meta.title.truncate', 50, TYPE_UINT), ''),
            'title.full'  => $aData['title'],
            'description' => tpl::truncate(strip_tags($aData['descr']), config::sysAdmin('orders.view.meta.description.truncate', 150, TYPE_UINT)),
            'price'       => ( $aData['price_ex'] == Specializations::PRICE_EX_AGREE ? _t('orders', 'по договоренности') :
                ( ! empty($aData['price']) ? $aData['price'].' '.Site::currencyData($aData['price_curr'], 'title_short').
                    ( ! empty($aData['price_rate_text'][LNG]) ? ' / '. $aData['price_rate_text'][LNG] : '' ) : '' )
            ),
            'tags'    => join(', ', $seoTags),
        );
        $seoData['region']  = array();
        $seoData['country']  = array();
        if ( ! empty($aData['regions'][0])) {
            $seoData['region']  = Geo::regionData($aData['regions'][0]['reg3_city']);
            $seoData['country'] = Geo::regionData($aData['regions'][0]['reg1_country']);
        }
        if ( ! empty($aData['specs'][0])) {
            $seoData['spec'] = $aData['specs'][0]['spec_title'];
            if (Specializations::catsOn() && ! empty($aData['specs'][0]['cat_title'])) {
                $seoData['category'] = $aData['specs'][0]['cat_title'];
            }
        }
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL);
        $this->setMeta('view', $seoData, $aData);
        # SEO: Open Graph
        $seoSocialImages = array();
        foreach ($aData['images'] as $v) {
            $seoSocialImages[] = $v['i'][OrdersOrderImages::szView];
        }
        $this->seo()->setSocialMetaOG($aData['share_title'], $aData['share_description'], $seoSocialImages, $seoURL, $aData['share_sitename']);
        return $this->viewPHP($aData, 'view');
    }

    public function getPerformerStateBlock(&$aData, $nOrderID){
        $ordersOpinions = static::ordersOpinions();
        if (!User::isWorker() || !$ordersOpinions) {
            return;
        }

        $data['statusList'] = array(
            Orders::OFFER_STATUS_ADD => array(
                'id'   => Orders::OFFER_STATUS_ADD,
                'd'    => _t('orders-offers', ''),
                't'    => _t('orders-offers', 'Вы отправили отклик. Теперь родитель рассмотрит Вашу кандидатуру.'),
                'c'    => 'alert-candidate',
                'img'  => '/img/waving-hand.svg',
                'fa'   => '/img/waving-hand.svg'
            ),
            Orders::OFFER_STATUS_CANDIDATE => array(
                'id'   => Orders::OFFER_STATUS_CANDIDATE,
                'd'    => _t('orders-offers', 'Вы кандидат'),
                't'    => _t('orders-offers', 'Вас выбрали кандидатом. У Вас больше шансов стать няней в этой семье.'),
                'c'    => 'alert-candidate',
                'img'  => '/img/stars.svg',
                'fa'   => '/img/stars.svg'
            ),
            Orders::OFFER_STATUS_PERFORMER => array(
                'id'  => Orders::OFFER_STATUS_PERFORMER,
                'd'   => _t('orders-offers', 'Вы исполнитель'),
                't'   => _t('orders-offers', 'Вас выбрали исполнителем.'),
                'c'   => 'alert-chosen',
                'img' => '',
                'fa'  => 'fa-trophy text-orange',
            ),
            Orders::OFFER_STATUS_CANCELED => array(
                'id'  => Orders::OFFER_STATUS_CANCELED,
                'd'   => _t('orders-offers', 'Вам отказанно'),
                't'   => _t('orders-offers', 'Вам отказали по этому объявлению. Пора искать новые предложения.'),
                'c'   => 'alert-decline',
                'img' => '/img/sad.svg',
                'fa'  => '/img/sad.svg',
            ),
            Orders::OFFER_STATUS_PERFORMER_DECLINE => array(
                'id' => Orders::OFFER_STATUS_PERFORMER_DECLINE,
                't' => _t('orders-offers', 'Вы отказались от заказа. Найдите новые объявления'),
                'd' => _t('orders-offers', 'Вы отказались от заказа. Найдите новые объявления'),
                'c' => 'alert-decline',
                'img' => '/img/forbidden.svg',
                'fa' => 'fa-times'
            ),
            Orders::OFFER_STATUS_PERFORMER_AGREE => array(
                'id'  => Orders::OFFER_STATUS_PERFORMER_AGREE,
                't'   => _t('orders-offers', 'Вы приняты на работу. Поздравляем!'),
                'd'   => _t('orders-offers', 'Вы исполнитель'),
                'c'   => 'alert-candidate',
                'img' => '/img/confetti.svg',
            ),
        );

        $data['performerState'] = $this->model->ordersListPerformerState(['order_id' => $nOrderID]);

        if(is_array($data['performerState']) && !empty($data['performerState'])){
            $data['performerState'] = reset($data['performerState']);
            if($data['performerState']['status'] == Orders::OFFER_STATUS_PERFORMER_START){
                $data = array_merge($data,[
                    'offer_id' =>  $data['performerState']['id'],
                    'login' =>  $data['performerState']['login'],
                    'user_id' =>  $data['performerState']['user_id'],
                ]);
                $data['performerStartBlock'] = $this->viewPHP($data, 'performer.start.block');
            }

        }

        $aData['currentUserPerformerState'] = $this->viewPHP($data, 'performer.state');

    }

    /**
     * Подроговка данных для просмотра заказа
     * @param array $aData ref
     */
    public function viewDataPrepare( & $aData)
    {
        $nOrderID = $aData['id'];
        $aData['avatar_small'] = UsersAvatar::url($aData['user_id'], $aData['avatar'], UsersAvatar::szSmall, $aData['sex']);

        # Фотографии
        $aData['images'] = array();
        if (static::imagesLimit() && $aData['imgcnt']) {
            $oImages = $this->orderImages($nOrderID);
            $aImages = $oImages->getData($aData['imgcnt']);
            foreach ($aImages as $v) {
                $aData['images'][] = array(
                    'id'       => $v['id'],
                    'filename' => $v['filename'],
                    'i'        => $oImages->getURL($v, array(
                        OrdersOrderImages::szSmall,
                        OrdersOrderImages::szView
                    ), false
                    )
                );
            }
        }

        # Файлы
        if (static::attachmentsLimit() && $aData['attachcnt']) {
            $oAttach = $this->orderAttachments($nOrderID);
            $aAttachments = $oAttach->getData($aData['attachcnt']);
            $aData['attachments'] = array();
            foreach ($aAttachments as $v) {
                $aData['attachments'][] = array(
                    'id'       => $v['id'],
                    'filename' => $v['filename'],
                    'i'        => $oAttach->getURL($v, false),
                    'size'     => tpl::filesize($v['size']),
                    'origin'   => $v['origin'],
                );
            }
        }

        # Теги
        $aData['tags'] = $this->orderTags()->tagsGet($nOrderID);

        # Дин. свойства
        if ($aData['type'] == static::TYPE_SERVICE) {
            $aFirstSpec = reset($aData['specs']);
            if (static::dynpropsEnabled()) {
                $aData['dynprops'] = Specializations::i()->dpOrdersView($aFirstSpec['spec_id'], $aData);
                $aData['dynprops_simple'] = Specializations::i()->dpOrdersView($aFirstSpec['spec_id'], $aData, 'd', 'view.dp.simple');
                $aData['dynprops_simple'] = json_decode($aData['dynprops_simple'], true);
            } else {
                $aData['dynprops'] = Specializations::i()->dpView($aFirstSpec['spec_id'], $aData);
            }
        }
        if ($aData['type'] == static::TYPE_PRODUCT && static::useProducts()) {
            $aFirstCat = reset($aData['cats']);
            $aData['dynprops'] = Shop::i()->dpView($aFirstCat['cat_id'], $aData);
        }

        # Хлебные крошки
        $crumbs = array(
            array('title' => _t('orders','Orders'), 'link' => static::url('list')),
        );
        switch ($aData['type']) {
            case static::TYPE_SERVICE: {
                $specFirst = reset($aData['specs']);
                $crumbs[] = array('title' => $specFirst['spec_title'], 'link' => static::url('list.single', array('keyword'=>$specFirst['keyword'])));
                $aData['serviceTypes'] = self::aServiceTypes();
            } break;
            case static::TYPE_PRODUCT: {
                $catFirst = reset($aData['cats']);
                if ($catFirst['cat_id2']) {
                    $crumbs[] = array('title' => $catFirst['t2'], 'link' => static::url('list.single', array('keyword'=>$catFirst['k2'], 't'=>static::TYPE_PRODUCT)));
                }
                $crumbs[] = array('title' => $catFirst['t1'], 'link' => static::url('list.single', array('keyword'=>$catFirst['k1'], 't'=>static::TYPE_PRODUCT)));
            } break;
        }
        $crumbs[] = array('title' => $aData['title'], 'active' => true);
        $aData['breadcrumbs'] = &$crumbs;
    }

    /**
     * Поиск заказов
     */
    public function search()
    {
        if(!User::id() || (User::id() && User::isClient())){
            $this->redirect(Users::url('list'));
        }
        $nPageSize = config::sysAdmin('orders.search.pagesize', 10, TYPE_UINT);
        $aData = array('list'=>array(), 'pgn'=>'');
        $aFilter = array(
            'status' => array(static::STATUS_OPENED),
        );
        if (static::searchClosed()) {
            $aFilter['status'][] = static::STATUS_CLOSED;
        }

        $f = array(
            'orders' => $this->input->getpost('orders', TYPE_NOTAGS), # keyword категорий или специализаций, разделенных через запятую
        );
        $aParams = array(
            'page' => TYPE_UINT,  # № страницы
            't'    => TYPE_UINT,  # тип заказа: услуга, товар
            'st'   => TYPE_UINT,  # тип услуги: разовый заказ, конкурс ...
            'c'    => TYPE_UINT,  # регион: страна
            'r'    => TYPE_UINT,  # регион: область / регион
            'ct'   => TYPE_UINT,  # регион: город
            'dt'   => TYPE_UINT,  # регион-город: район горда
            'pf'   => TYPE_PRICE, # бюджет: от
            'pt'   => TYPE_PRICE, # бюджет: до
            'pc'   => TYPE_UINT,  # бюджет: валюта
            'pex'  => TYPE_UINT,  # бюджет: экстра
            'pro'  => TYPE_UINT,  # только для PRO
            'm'    => TYPE_UINT,  # вид списка: карта, кратко, списком
            'q'    => TYPE_NOTAGS,# строка поиска
            'tag'  => TYPE_NOTAGS,# тег: "keyword-id"
            'd'    => TYPE_ARRAY, # дин. свойства
            'dc'   => TYPE_ARRAY, # дин. свойства child
            '4me'  => TYPE_UINT,  # только для пользователя (если авторизован и исполнитель - фильтр по заявленным специализациям исполнителя)
        );
        $this->input->postgetm($aParams, $f);

        $aData['f'] = &$f;
        $seoFilter = 0;
        # "память" у фильтра (сохранение/загрузка фильтра в/из БД)
        $bFilterLoad = false;
        $bFilterMemory = config::sys('orders.filter.memory', false, TYPE_BOOL);
        if ($nUserID = User::id() && $bFilterMemory) {
            $aIgnore = array('tag', 'q', '4me');
            $bEmpty = true;
            $bIgnore = false;
            foreach ($f as $k => $v) {
                if ($k == 'page') continue;
                if (in_array($k, $aIgnore)) {
                    if (!empty($v)) {
                        $bIgnore = true;
                        break;
                    }
                }
                if (!empty($v)) {
                    $bEmpty = false;
                }
            }
            if ($this->input->postget('clear', TYPE_UINT)) { # нажали очистить фильтр => очистим в бд
                $bEmpty = false;
            }
            if (!$bIgnore) {
                if ($bEmpty) {
                    $aMem = Users::model()->userData($nUserID, array('orders_search_filter'));
                    if (!empty($aMem['orders_search_filter'])) {
                        $f = func::unserialize($aMem['orders_search_filter']);
                        $bFilterLoad = true;
                    }
                    foreach ($aParams as $k => $v) {
                        if (!isset($f[$k])) {
                            $f[$k] = 0;
                        }
                    }
                } else {
                    $aSave = $f;
                    foreach ($aIgnore as $v) {
                        unset($aSave[$v]);
                    }
                    Users::model()->userSave($nUserID, array(), array('orders_search_filter' => serialize($aSave)));
                }
            }
        }

        $types = static::aTypes();
        if (!array_key_exists($f['t'], $types)) {
            $f['t'] = 0;
            if (!$f['q'] && !$f['tag']) {
                $f['t'] = static::TYPE_SERVICE;
            }
        }
        if ($f['t']) {
            $aFilter['type'] = $f['t'];
        }

        if ($f['4me']) {
            if ( ! User::id()) {
                $this->redirect(Users::url('login'), false, true);
            }
            if (Users::useClient() && ! User::isWorker()) {
                $f['4me'] = 0;
            }
            if ($f['t'] != static::TYPE_SERVICE) {
                $f['4me'] = 0;
            }
            if ($f['4me']) {
                $aFilter['userSpecs'] = User::id();
            }
        }

        # Фильтр по специализации (услуги) / категории (товары)
        $aData['fspecs'] = array();
        $aData['fspecs_keyword'] = '';
        $aData['fcats'] = array();
        if ( ! empty($f['orders']))
        {
            $aKeywords = explode(',', $f['orders']);
            foreach ($aKeywords as &$v) { $v = trim($v); } unset($v);
            if ($f['t'] == static::TYPE_SERVICE && ! empty($aKeywords)) {
                $aSpecsKeywords = Specializations::i()->aSpecsKeywords();
                foreach ($aKeywords as $v) {
                    if (!empty($aSpecsKeywords[$v]['spec_id'])) {
                        $aFilter['spec_id'][] = $aSpecsKeywords[$v]['spec_id'];
                        $aData['fspecs_keyword'] = $v;
                        if (Specializations::catsOn()) {
                            $aData['fspecs'][$aSpecsKeywords[$v]['cat_id']][$aSpecsKeywords[$v]['spec_id']] = 1;
                        } else {
                            $aData['fspecs'][$aSpecsKeywords[$v]['spec_id']] = 1;
                        }
                    }
                }
                if (empty($aFilter['spec_id']) && ! Request::isAJAX()) {
                    if ($bFilterLoad) {
                        $f['orders'] = '';
                        unset($aFilter['cat_id'], $aFilter['spec_id']);
                    } else {
                        $this->errors->error404();
                    }
                }
            }
            if ($f['t'] == static::TYPE_PRODUCT && ! empty($aKeywords) && static::useProducts()) {
                $aKeywords = Shop::model()->categoriesDataByKeywords($aKeywords);
                foreach ($aKeywords as $v) {
                    $aFilter['cat_id'][] = $v['id'];
                    if ($v['numlevel'] == 1) {
                        $aData['fcats'][$v['id']] = 1;
                    } else {
                        $aData['fcats'][$v['pid']][$v['id']] = 1;
                    }
                }
                if (empty($aFilter['cat_id']) && ! Request::isAJAX()) {
                    if ($bFilterLoad) {
                        $f['orders'] = '';
                        unset($aFilter['cat_id'], $aFilter['spec_id']);
                    } else {
                        $this->errors->error404();
                    }
                }
            }
        }

        if (!empty($aFilter['spec_id'])) {
            $nSpecID = (count($aFilter['spec_id']) == 1 ? reset($aFilter['spec_id']) : SpecializationsBase::ROOT_SPEC);
            if (static::dynpropsEnabled()) {
                $dp = Specializations::i()->dpOrders()->prepareSearchQuery($f['d'], $f['dc'], Specializations::i()->dpOrdersSettings($nSpecID), 'O.');
            } else {
                $dp = Specializations::i()->dp()->prepareSearchQuery($f['d'], $f['dc'], Specializations::i()->dpSettings($nSpecID), 'O.');
            }
            if (!empty($dp)) {
                $aFilter[':dp'] = $dp;
            }
            if (sizeof($aFilter['spec_id']) > 1 || ! empty($aFilter[':dp'])) {
                $seoFilter++;
            }
        }
        if (!empty($aFilter['cat_id'])) {
            if (count($aFilter['cat_id']) == 1) {
                $nCatID = $aData['nCatID'] = reset($aFilter['cat_id']);
                $dp = Shop::i()->dp()->prepareSearchQuery($f['d'], $f['dc'], Shop::i()->dpSettings($nCatID), 'O.');
                if (!empty($dp)) {
                    $aFilter[':dp'] = $dp;
                }
            }
            if (sizeof($aFilter['cat_id']) > 1 || ! empty($aFilter[':dp'])) {
                $seoFilter++;
            }
        }

        if ($f['st']) {
            $aFilter['service_type'] = $f['st'];
            $seoFilter++;
        }

        if (Geo::filterEnabled()) { # включен фильтр в шапке
            $geoFilter = Geo::filter('all');
            if (Geo::countrySelect() && ! empty($geoFilter['country'])) {
                $aFilter['reg1_country'] = $geoFilter['country'];
            }
            if ( ! empty($geoFilter['region'])) {
                $aFilter['reg2_region'] = $geoFilter['region'];
            }
            if ( ! empty($geoFilter['city'])) {
                $aFilter['reg3_city'] = $geoFilter['city'];
            }
        } else {
            if (Geo::countrySelect()) { # включен выбор страны
                if ($f['c']) {
                    $aFilter['reg1_country'] = $f['c'];
                } else {
                    $f['ct'] = 0;
                    $f['r'] = 0;
                }
                if ($f['ct']) { # указан город или регион
                    if ($f['c'] == $f['r']) { # pid совпадает со страной, значит регион
                        $aFilter['reg2_region'] = $f['ct'];
                    } else { # иначе город
                        $aFilter['reg3_city'] = $f['ct'];
                    }
                }
            } else {
                if ($f['ct']) { # указан город или регион
                    if (Geo::defaultCountry() == $f['r']) { # pid совпадает со страной по умолчанию, значит регион
                        $aFilter['reg2_region'] = $f['ct'];
                    } else { # иначе город
                        $aFilter['reg3_city'] = $f['ct'];
                    }
                }
            }
        }


        if($f['dt']){
            $aFilter['district_id'] = $f['dt'];
        }

        # Цена
        if ($f['pex']) { # флаги (по договору)
            $aFilter[':price_ex'] = array('price_ex & :ex != 0', 'ex' => $f['pex']);
            $seoFilter++;
        } else {
            if ($f['pf']) { # от
                $aFilter[':price_from'] = array(
                    'price_search >= :pf',
                    'pf' => Site::currencyPriceConvertToDefault($f['pf'], $f['pc'])
                );
                $seoFilter++;
            }
            if ($f['pt']) { # до
                $aFilter[':price_to'] = array(
                    'price_search <= :pt',
                    'pt' => Site::currencyPriceConvertToDefault($f['pt'], $f['pc'])
                );
                $seoFilter++;
            }
        }

        if ($f['pro']) {
            $aFilter['pro'] = 1;
            $seoFilter++;
        }

        if ($f['m'] == 1) {
            $aFilter[':addr_lat'] = 'addr_lat != 0';
            $aFilter[':addr_lng'] = 'addr_lng != 0';
        }

        # Фильтр по тегу
        if ($f['tag']) {
            if (preg_match('/(.*)-(\d+)/', $f['tag'], $matches) && ! empty($matches[2])) {
                $aTagData = $this->orderTags()->tagData($matches[2]);
                if (empty($aTagData) || mb_strtolower($aTagData['tag']) != $matches[1]) $this->errors->error404();
                $aFilter['tag_id'] = $aData['nTagID'] = $aTagData['id'];
                $aData['sTagTitle'] = $aTagData['tag'];
            } else {
                $this->errors->error404();
            }
        }

        if ($f['q']) {
            $aFilter['text'] = $f['q'];
        }

        if ($f['m'] == 2) { // кратко
            $nPageSize *= 3;
        }

        # URL
        $url = static::url('list');
        $urlQuery = $f;
        if (!empty($nSpecID)) {
            $aSpecData = Specializations::model()->specializationMeta(intval($nSpecID), $this->module_name);
            if (static::searchOneSpec() && ! empty($aSpecData)) {
                unset($urlQuery['orders']);
                $url = static::url('list.single', array('keyword'=>$aSpecData['keyword']));
            }
        } else if (!empty($nCatID)) {
            $aCategoryData = Shop::model()->categoryMeta(intval($nCatID), $this->module_name);
            if ( ! empty($aCategoryData)) {
                unset($urlQuery['orders']);
                $url = static::url('list.single', array('keyword'=>$aCategoryData['keyword']));
            }
        }

        # Формирование списка заказов
        $nCount = $this->model->ordersList($aFilter, true);
        if ($nCount) {
            $pgn = new Pagination($nCount, $nPageSize, array(
                'link'  => $url,
                'query' => $urlQuery,
            ));
            $aData['list'] = $this->model->ordersList($aFilter, false, $pgn->getLimitOffset(), 'svc_fixed DESC, O.svc_fixed_order DESC, O.svc_order DESC');
            $bAddNumRow = ($f['m'] == 1 || $f['q'] || $f['tag']);
            foreach ($aData['list'] as $k => &$v) {
                $v['url_view'] = static::url('view', array('id'=>$v['id'], 'keyword'=>$v['keyword']));
                if ($bAddNumRow) {
                    $v['n'] = ($pgn->getCurrentPage() - 1) * $nPageSize + $k + 1;
                }
                if ($f['q']) {
                    foreach (explode(' ', $f['q']) as $vv) {
                        $v['title'] = str_replace($vv, '<em>' . $vv . '</em>', $v['title']);
                        $v['descr'] = str_replace($vv, '<em>' . $vv . '</em>', $v['descr']);
                    }
                }
                $v['offer_status'] = $this->model->offersDataByFilter(array('order_id' => $v['id'], 'user_id' => User::id()), ['status']);
                $v['specs'] = $this->model->orderSpecs($v['id']);
                $v['order_data'] = $this->model->orderData($v['id']);
                # Дин. свойства array
                if ($v['order_data']['type'] == static::TYPE_SERVICE) {
                    $aFirstSpec = reset($v['specs']);
                    if (static::dynpropsEnabled()) {
                        $v['dynprops_simple'] = Specializations::i()->dpOrdersView($aFirstSpec['spec_id'], $v['order_data'], 'd', 'view.dp.simple');
                        $v['dynprops_simple'] = json_decode($v['dynprops_simple'], true);
                    }
                }
            } unset($v);
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }

        # Карта
        if ($f['m'] == 1) {
            if ($nCount) {
                $aData['points'] = array();
                foreach ($aData['list'] as &$v) {
                    $aParam = array('v' => &$v);
                    $aData['points'][] = array(
                        'lat' => $v['addr_lat'],
                        'lng' => $v['addr_lng'],
                        'n'   => $v['n'],
                        'b'   => $this->viewPHP($aParam, 'search.map.balloon'),
                        'id'  => $v['id'],
                    );
                } unset($v);
            }
            $seoFilter++;
            $aData['list'] = $this->viewPHP($aData, 'search.map.list');
        } else {
            if ($f['q'] || $f['tag']) {
                $aData['list'] = $this->viewPHP($aData, 'search.keywords.list');
            } else {
                if ($f['m'] == 2) {
                    $aData['list'] = $this->viewPHP($aData, 'search.list.short');
                } else {
                    $aData['list'] = $this->viewPHP($aData, 'search.list');
                }
                if (Request::isAJAX()) {
                    $this->searchRssData($aData);
                }
            }
        }

        $aData['count'] = tpl::declension($nCount, _t('orders', 'заказ;заказа;заказов'));
        if (Request::isAJAX()) {
            $aResponse = array(
                'pgn'    => $aData['pgn'],
                'list'   => $aData['list'],
                'count'  => $aData['count'],
                'points' => (isset($aData['points']) ? $aData['points'] : NULL),
            );
            if (!empty($aData['rss'])) {
                $aResponse['rss'] = $aData['rss'];
            }
            $this->ajaxResponseForm($aResponse);
        }

        if ($f['q'] || $f['tag']) {
            # SEO: Поиск заказов по ключевому слову
            $this->seo()->robotsIndex(false);
            $this->setMeta('search-keyword', array(
                'query' => ( $f['tag'] ? ( ! empty($aData['sTagTitle']) ? $aData['sTagTitle'] : '') : tpl::truncate($f['q'], config::sysAdmin('orders.search.meta.query.truncate', 50, TYPE_UINT), '') ),
                'page'  => $f['page'],
            ));
            return $this->viewPHP($aData, 'search.keywords');
        }

        # SEO:
        $seoRegion = Geo::regionData(
                ( ! empty($aFilter['reg3_city']) ? $aFilter['reg3_city'] :
                ( ! empty($aFilter['reg2_region']) ? $aFilter['reg2_region'] : 0 ) ) );
        $seoCountry = ( ! empty($seoRegion['country']) ? $seoRegion['country'] : ( ! empty($aFilter['reg1_country']) ? $aFilter['reg1_country'] : 0 ));
        $seoData = array(
            'page' => $f['page'],
            'region' => ( ! empty($seoRegion) ? $seoRegion : '' ),
            'country' => $seoCountry ? Geo::regionData($seoCountry) : '',
        );
        $canonical = array('page' => $f['page']);
        if( ! Geo::filterEnabled()){
            if($f['c'])  $canonical['c'] = $f['c'];
            if($f['ct']) $canonical['ct'] = $f['ct'];
            if($f['r'])  $canonical['r'] = $f['r'];
        }
        $this->seo()->robotsIndex(!$seoFilter);
        if (!empty($nSpecID)) {
            if (static::dynpropsEnabled()) {
                $aData['dp'] = Specializations::i()->dpOrdersForm($nSpecID, true, $f, 'd', 'search.dp', $this->module_dir_tpl);
            } else {
                $aData['dp'] = Specializations::i()->dpForm($nSpecID, true, $f, 'd', 'search.dp', $this->module_dir_tpl);
            }
            # SEO: Список заказов - услуги (специализация)
            if (empty($aSpecData)) $this->errors->error404();
            $this->seo()->canonicalUrl(static::url('list.single', array('keyword'=>$aSpecData['keyword']), true), $canonical);
            $seoData['spec'] = $aSpecData['title'];
            $this->setMeta('search-service', $seoData, $aSpecData);
            $this->seo()->setText($aSpecData, $f['page']);
        } else if (!empty($nCatID)) {
            $aData['dp'] = Shop::i()->dpForm($nCatID, true, $f, 'd', 'search.dp', $this->module_dir_tpl);
            # SEO: Список заказов - товары (категория)
            if (empty($aCategoryData)) $this->errors->error404();
            $this->seo()->canonicalUrl(static::url('list.single', array('keyword'=>$aCategoryData['keyword'], 't'=>static::TYPE_PRODUCT), true), $canonical);
            $seoData['category'] = $aCategoryData['title'];
            $this->setMeta('search-product', $seoData, $aCategoryData);
            $this->seo()->setText($aCategoryData, $f['page']);
        } else {
            # SEO: Список заказов (главная)
            $this->seo()->canonicalUrl(static::url('list', array(), true), $canonical);
            $seoData['type'] = ( isset($types[$f['t']]) ? $types[$f['t']]['plural'] : $types[static::TYPE_SERVICE]['plural'] );
            $this->setMeta('search-index', $seoData, $seoData);
            $this->seo()->setText($seoData, $f['page']);
        }

        $aData['form'] = $this->searchForm($aData);
        return $this->viewPHP($aData, 'search');
    }

    protected function searchForm(array &$aData)
    {
        if ($aData['f']['t'] == static::TYPE_SERVICE) {
            # список категорий специализаций + специализаций
            $aData['specs'] = Specializations::model()->specializationsInAllCategories(
                array('id', 'keyword', 'title'), array('id', 'keyword', 'title'), true
            );
            if ($aData['specs']) {
                $catsOn = Specializations::catsOn();
                foreach ($aData['specs'] as $k=>&$v) {
                    # категория без специализаций
                    if ($catsOn) {
                        if (empty($v['specs'])) {
                            unset($aData['specs'][$k]); continue;
                        }
                        foreach ($v['specs'] as &$vv) {
                            if ($vv['cnt'] > 1) {
                                $vv['keyword'] = $vv['keyword'].'-'.$v['keyword'];
                            }
                            $vv['url'] = static::url('list.single', array('keyword'=>$vv['keyword']));
                        } unset($vv);
                    }
                    $v['url'] = static::url('list.single', array('keyword'=>$v['keyword']));
                } unset($v);
            }
        }
        if ($aData['f']['t'] == static::TYPE_PRODUCT && static::useProducts()) {
            # список категорий товаров
            $aData['cats'] = Shop::model()->categoriesListAll(array('id', 'keyword', 'title'));
            if ($aData['cats'] && static::searchOneSpec()) {
                foreach ($aData['cats'] as &$v) {
                    $v['url'] = static::url('list.single', array('keyword'=>$v['keyword'], 't'=>static::TYPE_PRODUCT));
                    foreach ($v['sub'] as &$vv) {
                        $vv['url'] = static::url('list.single', array('keyword'=>$vv['keyword'], 't'=>static::TYPE_PRODUCT));
                    } unset($vv);
                } unset($v);
            }
        }
        $aData['region_title'] = $aData['f']['ct'] ? Geo::i()->regionTitle($aData['f']['ct']) : '';
        $this->searchRssData($aData);
        return $this->viewPHP($aData, 'search.form');
    }

    protected function searchRssData(array &$aData)
    {
        if (!config::sysAdmin('orders.rss.enabled', false, TYPE_BOOL)) {
            return;
        }
        $aData['rss'] = array('t' => $aData['f']['t'], 'cat' => array(), 'cat_id' => 0, 'sub' => array(), 'sub_id' => 0);
        if ($aData['f']['t'] == static::TYPE_SERVICE) {
            if ( ! isset($aData['specs'])) {
                $aData['specs'] = Specializations::model()->specializationsInAllCategories(
                    array('id', 'keyword', 'title'), array('id', 'keyword', 'title'), true
                );
            }
            if ($aData['specs']) {
                $catsOn = Specializations::catsOn();
                if ($catsOn) {
                    $aData['rss']['cat'] = array(0 => _t('orders', 'Все разделы'));
                    $aData['rss']['sub'] = array(0 => _t('orders', 'Все специализации'));
                    if ( ! empty($aData['fspecs']) && count($aData['fspecs']) == 1) {
                        $t = array_keys($aData['fspecs']);
                        $aData['rss']['cat_id'] = reset($t);
                        $t = reset($aData['fspecs']);
                        if (is_array($t) && count($t) == 1) {
                            $t = array_keys($t);
                            $aData['rss']['sub_id'] = reset($t);
                        }
                    }
                } else {
                    $aData['rss']['cat'] = array(0 => _t('orders', 'Все специализации'));
                    if (!empty($aData['fspecs']) && count($aData['fspecs']) == 1) {
                        $t = array_keys($aData['fspecs']);
                        $aData['rss']['cat_id'] = reset($t);
                    }
                }
                foreach ($aData['specs'] as $k => $v) {
                    if ($catsOn) {
                        foreach ($v['specs'] as $vv) {
                            if ($aData['rss']['cat_id'] == $v['id']) {
                                $aData['rss']['sub'][ $vv['id'] ] = $vv['title'];
                            }
                        }
                    }
                    $aData['rss']['cat'][ $v['id'] ] = $v['title'];
                }
            }
        } else if ($aData['f']['t'] == static::TYPE_PRODUCT && static::useProducts()) {
            if ( ! isset($aData['cats'])) {
                $aData['cats'] = Shop::model()->categoriesListAll(array('id', 'keyword', 'title'));
            }
            if ($aData['cats']) {
                $aData['rss']['cat'] = array(0 => _t('orders', 'Все категории'));
                if ( ! empty($aData['fcats']) && count($aData['fcats']) == 1) {
                    $t = array_keys($aData['fcats']);
                    $aData['rss']['cat_id'] = reset($t);
                    $t = reset($aData['fcats']);
                    if(is_array($t) && count($t) == 1){
                        $t = array_keys($t);
                        $aData['rss']['sub_id'] = reset($t);
                    }
                }
                foreach ($aData['cats'] as $v) {
                    $aData['rss']['cat'][ $v['id'] ] = $v['title'];
                }
                if ( ! empty($aData['cats'][ $aData['rss']['cat_id'] ]['sub'])) {
                    $aData['rss']['sub'] = array(0 => _t('orders', 'Все категории'));
                    foreach ($aData['cats'][ $aData['rss']['cat_id'] ]['sub'] as $v) {
                        $aData['rss']['sub'][ $v['id'] ] = $v['title'];
                    }
                }
            }
        }
        if (Request::isAJAX()) {
            if ( ! empty($aData['rss']['cat'])) {
                $aData['rss']['cat'] = HTML::selectOptions($aData['rss']['cat'], $aData['rss']['cat_id']);
            } else {
                $aData['rss']['cat'] = '';
            }
            if ( ! empty($aData['rss']['sub'])) {
                $aData['rss']['sub'] = HTML::selectOptions($aData['rss']['sub'], $aData['rss']['sub_id']);
            } else {
                $aData['rss']['sub'] = '';
            }
        }
    }

    /**
     * Дополнительные ajax запросы
     */
    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR)) {
            case 'tags-autocomplete':
            {
                $sQuery = $this->input->post('q', TYPE_NOTAGS);
                $this->orderTags()->tagsAutocomplete($sQuery);
            }
            break;
            case 'order-delete': # удаление заказа
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }


                if( ! static::deleteAllow()){
                    $this->errors->impossible();
                    break;
                }

                $nOrderID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->orderData($nOrderID, array('user_id', 'status', 'performer_id'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if (!$this->isOrderOwner($nOrderID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if (static::ordersOpinions()) {
                    if ($aData['performer_id']) {
                        $offer = $this->model->offersDataByFilter(array('order_id' => $nOrderID, 'user_id' => $aData['performer_id']), array('status'));
                        if ($offer['status'] == static::OFFER_STATUS_PERFORMER_AGREE) {
                            $this->errors->set(_t('orders', 'Невозможно удалить завершенный заказ'));
                            break;
                        }
                    }
                }

                if (!$this->orderDelete($nOrderID)) {
                    $this->errors->reloadPage();
                    break;
                }
            }
            break;
            case 'order-hide': # закрыть заказ
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nOrderID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->orderData($nOrderID, array('user_id', 'status'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if ( ! $this->isOrderOwner($nOrderID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if($aData['status'] != static::STATUS_OPENED) {
                    $this->errors->reloadPage();
                    break;
                }

                $aUpdate = array();
                $aUpdate['status_prev'] = $aData['status'];
                $aUpdate['status'] = static::STATUS_CLOSED;
                $aUpdate['status_changed'] = $this->db->now();

                $this->model->orderSave($nOrderID, $aUpdate);
            } break;
            case 'order-show': # опубликовать заказ
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nOrderID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->orderData($nOrderID, array('user_id', 'status', 'term', 'performer_id'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if ( ! $this->isOrderOwner($nOrderID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                if($aData['status'] != static::STATUS_CLOSED) {
                    $this->errors->reloadPage();
                    break;
                }

                if(static::ordersOpinions()){
                    if($aData['performer_id']){
                        $offer = $this->model->offersDataByFilter(array('order_id' => $nOrderID, 'user_id' => $aData['performer_id']), array('status'));
                        if($offer['status'] == static::OFFER_STATUS_PERFORMER_AGREE){
                            $this->errors->set(_t('orders', 'Невозможно опубликовать завершенный заказ'));
                            break;
                        }
                    }
                }

                # Срок приема заявок
                if(static::termsEnabled()){
                    $aData['expire'] = $this->termExpire($aData['term']);
                }

                $aUpdate = array();
                $aUpdate['status_prev'] = $aData['status'];
                $aUpdate['status'] = static::STATUS_OPENED;
                $aUpdate['status_changed'] = $this->db->now();

                $this->model->orderSave($nOrderID, $aUpdate);
            } break;
            case 'order-trash': # перемещение заказа в корзину
            {
                if ( ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nOrderID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->orderData($nOrderID, array('user_id'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if ( ! $this->isOrderOwner($nOrderID, $aData['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }

                $this->model->orderSave($nOrderID, array('removed = 1 - removed'));
            } break;
            case 'offer-trash': # удаление заявки
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nOfferID = $this->input->post('id', TYPE_UINT);

                $aData = $this->model->offerData($nOfferID, array('user_id'));

                if (empty($aData)) {
                    $this->errors->impossible();
                    break;
                }

                if ($aData['user_id'] != User::id()) {
                    $this->errors->impossible();
                    break;
                }

                $this->model->offerSave($nOfferID, array('user_removed = ! user_removed'), array('id' => $nOfferID));
            }
            break;
            case 'search-price-ex':
            {
                $nSpecID = $this->input->post('id', TYPE_UINT);
                if (!$nSpecID) {
                    break;
                }
                $aData = array(
                    'nSpecID' => $nSpecID,
                    'f' => array(
                        'pf'  => 0,
                        'pt'  => 0,
                        'pc'  => 0,
                        'pex' => 0,
                        't' => static::TYPE_SERVICE
                    ));
                $aResponse['html'] = $this->viewPHP($aData, 'search.form.price.ex');
            }
            break;
            case 'price-form-sett':
            {
                $nSpecID = $this->input->post('spec_id', TYPE_UINT);
                if (!$nSpecID) {
                    break;
                }
                $aData = array(
                    'spec'       => array('spec_id' => $nSpecID),
                    'price'      => 0,
                    'price_ex'   => 0,
                    'price_rate' => 0,
                    'price_curr' => 0,
                );
                $aResponse['html'] = $this->viewPHP($aData, 'form.price');
            }
            break;
            case 'dp-form':
            {
                $nType = $this->input->post('type', TYPE_UINT);
                $nID = $this->input->post('id', TYPE_UINT);
                $bSearch = $this->input->post('search', TYPE_BOOL);
                $sFormName = $bSearch ? 'search.dp' : 'form.dp';
                if ($nType == static::TYPE_SERVICE) {
                    if (static::dynpropsEnabled()) {
                        $aResponse['dp'] = Specializations::i()->dpOrdersForm($nID, $bSearch, false, 'd', $sFormName, $this->module_dir_tpl);
                    } else {
                        $aResponse['dp'] = Specializations::i()->dpForm($nID, $bSearch, false, 'd', $sFormName, $this->module_dir_tpl);
                    }
                }
                if ($nType == static::TYPE_PRODUCT && static::useProducts()) {
                    $aResponse['dp'] = Shop::i()->dpForm($nID, $bSearch, false, 'd', $sFormName, $this->module_dir_tpl);
                }

            }
            break;
            # дин. свойства: child-свойства
            case 'dp-child':
            {
                $p = $this->input->postm(array(
                        'dp_id'       => TYPE_UINT, # ID parent-дин.свойства
                        'dp_value'    => TYPE_UINT, # ID выбранного значения parent-дин.свойства
                        'name_prefix' => TYPE_NOTAGS, # Префикс для name
                        'type'        => TYPE_UINT, # тип заказа
                    )
                );

                if (empty($p['dp_id']) && empty($p['dp_value'])) {
                    $this->errors->impossible();
                } else {
                    if ($p['type'] == static::TYPE_SERVICE) {
                        if (static::dynpropsEnabled()) {
                            $aData = Specializations::i()->dpOrders()->formChildByParentIDValue($p['dp_id'], $p['dp_value'], array(
                                    'name' => $p['name_prefix'],
                                    'class' => 'form-control'
                                ), false
                            );
                        } else {
                            $aData = Specializations::i()->dp()->formChildByParentIDValue($p['dp_id'], $p['dp_value'], array(
                                    'name' => $p['name_prefix'],
                                    'class' => 'form-control'
                                ), false
                            );
                        }
                    }
                    if ($p['type'] == static::TYPE_PRODUCT && static::useProducts()) {
                        $aData = Shop::i()->dp()->formChildByParentIDValue($p['dp_id'], $p['dp_value'], array(
                                'name'  => $p['name_prefix'],
                                'class' => 'form-control'
                            ), false
                        );
                    }
                    $aResponse['form'] = $aData;
                }
            }
            break;
            case 'dp-child-search':
            {
                $p = $this->input->postm(array(
                        'dp_id'    => TYPE_UINT, # ID parent-дин.свойства
                        'dp_value' => TYPE_UINT, # ID выбранного значения parent-дин.свойства
                        'type'     => TYPE_UINT, # тип заказа
                        'id'       => TYPE_UINT, # ID специализации или категории
                    )
                );

                if (empty($p['dp_id']) && empty($p['dp_value'])) {
                    $this->errors->impossible();
                } else {
                    if ($p['type'] == static::TYPE_SERVICE) {
                        if (static::dynpropsEnabled()) {
                            $aResponse['form'] = Specializations::i()->dpOrdersForm($p['id'], true, false, 'd', 'search.dp', $this->module_dir_tpl, $p);
                        } else {
                            $aResponse['form'] = Specializations::i()->dpForm($p['id'], true, false, 'd', 'search.dp', $this->module_dir_tpl, $p);
                        }
                    }
                    if ($p['type'] == static::TYPE_PRODUCT && static::useProducts()) {
                        $aResponse['form'] = Shop::i()->dpForm($p['id'], true, false, 'd', 'search.dp', $this->module_dir_tpl, $p);
                    }
                }
            }
            break;
            case 'rss-sub':
            {
                $p = $this->input->postm(array(
                    'cat_id' => TYPE_UINT, # id категории
                    't'      => TYPE_UINT # id типа заказов
                ));
                $aData = array('f' => array('t' => $p['t']));
                if ($p['t'] == static::TYPE_SERVICE) {
                    $aData['fspecs'] = array($p['cat_id'] => 1);
                } else if ($p['t'] == static::TYPE_PRODUCT) {
                    $aData['fcats'] = array($p['cat_id'] => 1);
                }
                $this->searchRssData($aData);
                $aResponse['sub'] = $aData['rss']['sub'];
            }
            break;
            # стоимость услуг для формы добавления/редактирования
            case 'order-form-svc-prices':
            {
                if (!bff::servicesEnabled()) {
                    $aResponse['prices'] = array();
                    break;
                }
                $nCityID = $this->input->post('city', TYPE_UINT);

                $aSvcData = Svc::model()->svcListing(Svc::TYPE_SERVICE, $this->module_name);
                $ids = array();
                foreach ($aSvcData as $v) $ids[] = $v['id'];
                $aSvcPrices = $this->model->svcPricesEx($ids, $nCityID);
                foreach ($aSvcData as $v) {
                    if (empty($aSvcPrices[ $v['id'] ]) || $aSvcPrices[ $v['id'] ] <= 0) {
                        $aSvcPrices[ $v['id'] ] = $v['price'];
                    }
                }
                $aResponse['prices'] = $aSvcPrices;
            }
            break;
            case 'invite-modal': # модальное окно предложения заказа исполнителю
            {
                if ( ! static::invitesEnabled() ||
                     ! $this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->validateOrderData(0, false, static::TYPE_SERVICE);
                $this->security->setTokenPrefix('order-item-form');
                $workerID = $this->input->post('id', TYPE_UINT);
                if ( ! $workerID) {
                    $this->errors->reloadPage();
                    break;
                }

                $filter = array(
                    'status'         => static::STATUS_OPENED,
                    'user_id'        => User::id(),
                    'performer_id'   => 0,
                    'visibility'     => static::VISIBILITY_PRIVATE,
                    'exclude_invite' => $workerID,
                    'exclude_offer'  => $workerID,
                );
                # список Приватных заказов
                $data['list'] = $this->model->ordersListInvite($filter);
                if ( ! empty($data['list'])) {
                    $data['private'] = $this->viewPHP($data, 'invite.modal.list');
                }
                # список Публичных заказов
                $filter['visibility'] = static::VISIBILITY_ALL;
                if (static::premoderation()) {
                    $filter[':moderated'] = 'O.moderated > 0';
                }
                $data['list'] = $this->model->ordersListInvite($filter);
                if ( ! empty($data['list'])) {
                    $data['public'] = $this->viewPHP($data, 'invite.modal.list');
                }
                # форма предложения нового заказа
                $workerData = Users::model()->userData($workerID, array('specs'));
                if( ! empty($workerData['specs'])){
                    foreach($workerData['specs'] as $v){
                        if($v['main']){
                            $data['spec'] = $v;
                            break;
                        }
                    }
                    if(empty($data['spec'])){
                        $data['spec'] = reset($workerData['specs']);
                    }
                }
                $data['form'] = $this->viewPHP($data, 'invite.modal.form');
                $aResponse['html'] = $this->viewPHP($data, 'invite.modal');
            }
            break;
            case 'invite': # предложение заказа исполнителю
            {
                if ( ! static::invitesEnabled()) {
                    break;
                }

                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                $orderID = $this->input->post('order', TYPE_UINT);
                if ( ! $orderID) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->orderData($orderID, array('user_id', 'status', 'performer_id'));
                if (empty($data['user_id']) || ! User::isCurrent($data['user_id'])) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($data['status'] != static::STATUS_OPENED || $data['performer_id']) {
                    $this->errors->reloadPage();
                    break;
                }
                $userID = $this->input->post('user', TYPE_UINT);
                if ( ! $userID) {
                    $this->errors->reloadPage();
                    break;
                }
                $user = Users::model()->userData($userID, array('type', 'blocked', 'deleted', 'activated'));
                if (empty($user['type']) || $user['type'] != Users::TYPE_WORKER) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($user['activated'] == 0 || $user['blocked'] || $user['deleted']) {
                    $this->errors->reloadPage();
                    break;
                }
                $cnt = $this->model->invitesDataByFilter(array(
                    'order_id' => $orderID,
                    'user_id' => $userID,
                ));
                if ($cnt) {
                    $this->errors->set(_t('orders', 'Вы уже предлагали исполнителю данный заказ'));
                    break;
                }
                $offers = $this->model->offersDataByFilter(array('order_id' => $orderID, 'user_id' => $userID));
                if ($offers) {
                    $this->errors->set(_t('orders', 'Вы уже предлагали исполнителю данный заказ'));
                    break;
                }

                # не чаще чем раз в {X} секунд с одного IP (для одного пользователя)
                if (Site::i()->preventSpam('order-invite', config::sysAdmin('orders.invite.prevent.spam', 10, TYPE_UINT))) {
                    $this->errors->reloadPage();
                    break;
                }

                $message = $this->input->post('message', TYPE_NOTAGS);
                $this->createInvite($orderID, $userID, $message);
            }
            break;
            case 'invite-decline': # отклонение предложения исполнителем
            {
                if( ! static::invitesEnabled()){
                    break;
                }

                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $message = $this->input->post('message', TYPE_TEXT, array('len' => 3000, 'len.sys' => 'orders.invite.message.limit'));
                if (strlen($message) < config::sys('orders.invite.message.min', 5, TYPE_UINT)) {
                    $this->errors->set(_t('orders', 'Укажите причину отказа'));
                    break;
                }

                $orderID = $this->input->post('id', TYPE_UINT);
                if ( ! $orderID) {
                    $this->errors->reloadPage();
                    break;
                }
                $order = $this->model->orderData($orderID, array('user_id', 'title', 'keyword'));
                if (empty($order)) {
                    $this->errors->reloadPage();
                    break;
                }
                $userID = User::id();
                $cnt = $this->model->invitesDataByFilter(array(
                    'user_id'  => $userID,
                    'order_id' => $orderID,
                ));

                if ( ! $cnt) {
                    $this->errors->reloadPage();
                    break;
                }

                $offerID = $this->model->offerSave(0, array(
                    'order_id'       => $orderID,
                    'user_id'        => $userID,
                    'status'         => static::OFFER_STATUS_INVITE_DECLINE,
                    'status_created' => $this->db->now(),
                    'user_ip'        => Request::remoteAddress(),
                    'client_only'    => true,
                    'descr'          => $message,
                    'from_invite'    => 1,
                ));
                if ($offerID) {
                    $this->model->orderSave($orderID, array('offers_cnt = offers_cnt + 1'));

                    $this->model->inviteDelete(array(
                        'user_id'  => $userID,
                        'order_id' => $orderID,
                    ));
                    $this->model->calcUserCounters($userID);

                    # Отправим уведомление заказчику
                    $user = User::data(array('email','name','surname','login'));
                    $client = Users::model()->userDataEnotify($order['user_id']);
                    $fio = array();
                    if (!empty($user['name'])) $fio[] = $user['name'];
                    if (!empty($user['surname'])) $fio[] = $user['surname'];
                    $user['fio'] = ( ! empty($fio) ? join(' ', $fio) : $user['login']);
                    Users::sendMailTemplateToUser($client, 'order_invite_decline', array(
                        'client_url'   => Users::url('profile', array('login' => $client['login'])),
                        'worker_fio'   => $user['fio'],
                        'worker_name'  => $user['name'],
                        'worker_login' => $user['login'],
                        'worker_url'   => Users::url('profile', array('login' => $user['login'])),
                        'order_title'  => $order['title'],
                        'order_url'    => static::url('view', array('id' => $orderID, 'keyword' => $order['keyword'])),
                        'message'      => tpl::truncate($message, config::sysAdmin('orders.email.invite.message.truncate', 150, TYPE_UINT)),
                    ));
                }
            }
            break;
            case 'offer-modal': # модальное окно для принятие приглашения, заявка в упрощеной форме
            {
                if ( ! static::invitesEnabled()) {
                    break;
                }

                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $orderID = $this->input->post('id', TYPE_UINT);
                if ( ! $orderID) {
                    $this->errors->reloadPage();
                    break;
                }
                $order = $this->model->orderData($orderID, array(), true);
                if (empty($order)) {
                    $this->errors->reloadPage();
                    break;
                }

                $userID = User::id();
                $cnt = $this->model->invitesDataByFilter(array(
                    'user_id'  => $userID,
                    'order_id' => $orderID,
                ));
                if ( ! $cnt) {
                    $this->errors->reloadPage();
                    break;
                }

                $offers = $this->model->offersDataByFilter(array(
                    'user_id'  => $userID,
                    'order_id' => $orderID,
                ));
                if ($offers) {
                    $this->errors->reloadPage();
                    break;
                }
                $aResponse['html'] = $this->viewPHP($order, 'offer.modal');
            }
            break;
            default:
                $this->errors->impossible();
        }
        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Создадим предложение заказа для исполнителя
     * @param integer $orderID ID заказа
     * @param integer $workerID ID исполнителя
     * @param string $message текст сообщения
     * @return integer|bool
     */
    protected function createInvite($orderID, $workerID, $message = '')
    {
        if( ! $orderID) return false;
        if( ! $workerID) return false;
        $data = array(
            'order_id' => $orderID,
            'user_id'  => $workerID,
            'message'  => $message,
        );
        $this->model->inviteSave($data);

        # запомним список приглашенных пользователей для просмотра заваза
        $order = $this->model->orderData($orderID, array('title', 'keyword', 'invited_users'));
        $invited = func::unserialize($order['invited_users']);
        $invited[$workerID] = array('user_id' => $workerID, 'created' => time());
        $this->model->orderSave($orderID, array('invited_users' => serialize($invited)));

        # пересчитаем счетчик новых заказов пользователя
        $this->model->calcUserCounters($workerID);

        # Отправим уведомление исполнителю
        $user = User::data(array('email','name','surname','login'));
        $worker = Users::model()->userDataEnotify($workerID);
        $fio = array();
        if (!empty($user['name'])) $fio[] = $user['name'];
        if (!empty($user['surname'])) $fio[] = $user['surname'];
        $user['fio'] = ( ! empty($fio) ? join(' ', $fio) : $user['login']);

        Users::sendMailTemplateToUser($worker, 'order_invite_start', array(
            'client_name'  => $user['name'],
            'client_fio'   => $user['fio'],
            'client_login' => $user['login'],
            'client_url'   => Users::url('profile', array('login' => $user['login'])),
            'worker_url'   => Users::url('profile', array('login' => $worker['login'])),
            'order_title'  => $order['title'],
            'order_url'    => static::url('view', array('id' => $orderID, 'keyword' => $order['keyword'])),
          //'message'      => tpl::truncate($message, 150),
        ));
        return true;
    }

    /**
     * Количество предложений заказов у пользователя
     * @return integer
     */
    public function invitesCount()
    {
        if (!static::invitesEnabled() && !static::ordersOpinions()) return 0;

        $userID = User::id();
        if (!$userID) return 0;

        $invites = $this->model->invitesDataByFilter(array(
            'user_id' => $userID,
        ));
        $offers = $this->model->offersDataByFilter(array(
            'user_id' => $userID,
            'status'  => static::OFFER_STATUS_PERFORMER_START,
        ));
        return $invites + $offers;
    }

    /**
     * Управление изображениями заказа
     * @param getpost ::uint 'item_id' ID заказа
     * @param getpost ::string 'act' действие
     */
    public function img()
    {
        $this->security->setTokenPrefix('order-item-form');

        $nOrderID = $this->input->getpost('item_id', TYPE_UINT);
        $oImages = $this->orderImages($nOrderID);
        $aResponse = array();

        switch ($this->input->getpost('act')) {
            case 'upload':
            {
                $aResponse = array('success' => false);
                do {
                    if (!$this->security->validateToken(true, false)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if ($nOrderID) {
                        if (!$this->isOrderOwner($nOrderID)) {
                            $this->errors->set(_t('orders', 'Вы не является владельцем данного заказа'));
                            break;
                        }
                    }

                    $result = $oImages->uploadQQ();

                    $aResponse['success'] = ($result !== false && $this->errors->no());
                    if ($aResponse['success']) {
                        $aResponse = array_merge($aResponse, $result);
                        $aResponse['tmp'] = empty($nOrderID);
                        $aResponse['i'] = $oImages->getURL($result, OrdersOrderImages::szSmall, $aResponse['tmp']);
                        unset($aResponse['dir'], $aResponse['srv']);
                    }
                } while (false);

                $aResponse['errors'] = $this->errors->get();
                $this->ajaxResponse($aResponse, true, false, true);
            }
            break;
            case 'delete':
            {
                $nImageID = $this->input->post('image_id', TYPE_UINT);
                $sFilename = $this->input->post('filename', TYPE_NOTAGS);

                # неуказан ID изображения ни filename временного
                if (!$nImageID && empty($sFilename)) {
                    $this->errors->reloadPage();
                    break;
                }

                if (!$this->security->validateToken(true, false)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($nOrderID) {
                    # проверяем доступ на редактирование
                    if (!$this->isOrderOwner($nOrderID)) {
                        $this->errors->set(_t('orders', 'Вы не является владельцем данного заказа'));
                        break;
                    }
                }

                if ($nImageID) {
                    # удаляем изображение по ID
                    $oImages->deleteImage($nImageID);
                } else {
                    # удаляем временное
                    $oImages->deleteTmpFile($sFilename);
                }
            }
            break;
            case 'delete-tmp':
            {
                $aFilenames = $this->input->post('filenames', TYPE_ARRAY_STR);
                $oImages->deleteTmpFile($aFilenames);
            }
            break;
            default:
                $this->errors->set(_t('', 'Неудалось выполнить данное действие'));
        }

        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Управление изображениями заявки
     * @param getpost ::uint 'item_id' - ID заявки
     * @param getpost ::string 'act' - action
     */
    public function offer_img()
    {
        $nOfferID = $this->input->getpost('item_id', TYPE_UINT);
        $oImages = $this->offerImages($nOfferID);
        $aResponse = array();

        switch ($this->input->getpost('act')) {
            case 'upload':
            {
                $aResponse = array('success' => false);
                do {
                    if (!$this->security->validateToken(true, false)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if ($nOfferID) {
                        if (!$this->isOfferOwner($nOfferID)) {
                            $this->errors->set(_t('orders', 'Вы не является владельцем данного предложения'));
                            break;
                        }
                    }

                    $result = $oImages->uploadQQ();

                    $aResponse['success'] = ($result !== false && $this->errors->no());
                    if ($aResponse['success']) {
                        $aResponse = array_merge($aResponse, $result);
                        $aResponse['tmp'] = empty($nOrderID);
                        $aResponse['i'] = $oImages->getURL($result, OrdersOfferImages::szSmall, $aResponse['tmp']);
                        unset($aResponse['dir'], $aResponse['srv']);
                    }
                } while (false);

                $aResponse['errors'] = $this->errors->get();
                $this->ajaxResponse($aResponse, true, false, true);
            }
            break;
            case 'delete':
            {
                $nImageID = $this->input->post('image_id', TYPE_UINT);
                $sFilename = $this->input->post('filename', TYPE_NOTAGS);

                # неуказан ID изображения ни filename временного
                if (!$nImageID && empty($sFilename)) {
                    $this->errors->reloadPage();
                    break;
                }

                if (!$this->security->validateToken(true, false)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($nOfferID) {
                    # проверяем доступ на редактирование
                    if (!$this->isOfferOwner($nOfferID)) {
                        $this->errors->set(_t('orders', 'Вы не является владельцем данного предложения'));
                        break;
                    }
                }

                if ($nImageID) {
                    # удаляем изображение по ID
                    $oImages->deleteImage($nImageID);
                } else {
                    # удаляем временное
                    $oImages->deleteTmpFile($sFilename);
                }
            }
            break;
            case 'delete-tmp':
            {
                $aFilenames = $this->input->post('filenames', TYPE_ARRAY_STR);
                $oImages->deleteTmpFile($aFilenames);
            }
            break;
            default:
                $this->errors->reloadPage();
        }

        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Управление прикреплениями к заказам
     * @param getpost ::uint 'item_id' ID заказа
     * @param getpost ::string 'act' действие
     */
    public function attach()
    {
        $this->security->setTokenPrefix('order-item-form');

        $nOrderID = $this->input->getpost('item_id', TYPE_UINT);
        $oAttachments = $this->orderAttachments($nOrderID);
        $aResponse = array();

        switch ($this->input->getpost('act')) {
            case 'upload':
            {
                $aResponse = array('success' => false);
                do {
                    if (!$this->security->validateToken(true, false)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if ($nOrderID) {
                        if (!$this->isOrderOwner($nOrderID)) {
                            $this->errors->set(_t('orders', 'Вы не является владельцем данного заказа'));
                            break;
                        }
                    }

                    $result = $oAttachments->uploadQQ();

                    $aResponse['success'] = ($result !== false && $this->errors->no());
                    if ($aResponse['success']) {
                        $aResponse = array_merge($aResponse, $result);
                        $aResponse['tmp'] = empty($nOrderID);
                        $aResponse['i'] = $oAttachments->getURL($result, $aResponse['tmp']);
                        $aResponse['size'] = tpl::filesize($aResponse['size']);
                        unset($aResponse['dir'], $aResponse['srv']);
                    }
                } while (false);

                $aResponse['errors'] = $this->errors->get();
                $this->ajaxResponse($aResponse, true, false, true);
            }
            break;
            case 'delete':
            {
                $nAttachID = $this->input->post('file_id', TYPE_UINT);
                $sFilename = $this->input->post('filename', TYPE_NOTAGS);

                # неуказан ID изображения ни filename временного
                if (!$nAttachID && empty($sFilename)) {
                    $this->errors->reloadPage();
                    break;
                }

                if (!$this->security->validateToken(true, false)) {
                    $this->errors->reloadPage();
                    break;
                }

                if ($nOrderID) {
                    # проверяем доступ на редактирование
                    if (!$this->isOrderOwner($nOrderID)) {
                        $this->errors->set(_t('orders', 'Вы не является владельцем данного заказа'));
                        break;
                    }
                }

                if ($nAttachID) {
                    # удаляем файл по ID
                    $oAttachments->deleteAttach($nAttachID);
                } else {
                    # удаляем временное
                    $oAttachments->deleteTmpFile($sFilename);
                }
            }
            break;
            case 'delete-tmp':
            {
                $aFilenames = $this->input->post('filenames', TYPE_ARRAY_STR);
                $oAttachments->deleteTmpFile($aFilenames);
            }
            break;
            default:
                $this->errors->reloadPage();
        }

        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Активация заказа
     * @param get ::string 'c' - ключ активации
     * @return string HTML
     */
    public function activate()
    {
        $langActivateTitle = _t('orders', 'Активация заказа');

        $sCode = $this->input->get('c', TYPE_STR); # ключ активации + ID заказа
        list($sCode, $nItemID) = explode('_', (!empty($sCode) && (strpos($sCode, '_') !== false) ? $sCode : '_'), 2);
        $nItemID = $this->input->clean($nItemID, TYPE_UINT);

        # 1. Получаем данные о заказе:
        $aData = $this->model->orderData($nItemID, array(
                'user_id',
                'status',
                'activate_key',
                'activate_expire',
                'blocked_reason'
            )
        );
        if (empty($aData)) {
            # не нашли такого заказа
            return $this->showForbidden($langActivateTitle,
                _t('orders', 'Заказ не найден. Возможно период действия ссылки активации вашего заказа истек.')
            );
        }
        if ($aData['activate_key'] != $sCode || strtotime($aData['activate_expire']) < BFF_NOW) {
            # код неверный
            #  или
            # срок действия кода активации устек
            return $this->showForbidden($langActivateTitle,
                _t('orders', 'Срок действия ссылки активации истек либо она некорректна. Пожалуйста, <a [link_add]>добавьте новый заказ</a>.', array('link_add' => 'href="' . self::url('add') . '"'))
            );
        }
        if ($aData['status'] == self::STATUS_BLOCKED) {
            # заказ был заблокирован (модератором?!)
            return $this->showForbidden($langActivateTitle,
                _t('orders', 'Заказ был заблокирован модератором, причина: [reason]', array(
                        'reason' => $aData['blocked_reason']
                    )
                )
            );
        }

        # 2. Получаем данные о пользователе:
        $aUserData = Users::model()->userData($aData['user_id'], array(
            'user_id', 'email', 'name', 'password', 'password_salt',
            'activated', 'blocked', 'blocked_reason'
        ));
        if (empty($aUserData)) {
            return $this->showForbidden($langActivateTitle, _t('orders', 'Ошибка активации, обратитесь в службу поддержки.'));
        } else {
            $nUserID = $aUserData['user_id'];
            # аккаунт заблокирован
            if ($aUserData['blocked']) {
                return $this->showForbidden($langActivateTitle,
                    _t('orders', 'Ваш аккаунт заблокирован. За детальной информацией обращайтесь в службу поддержки.')
                );
            }
            # активируем аккаунт
            if (!$aUserData['activated']) {
                $sPassword = func::generator(12); # генерируем новый пароль
                $aUserData['password'] = $this->security->getUserPasswordMD5($sPassword, $aUserData['password_salt']);
                $bSuccess = Users::model()->userSave($nUserID, array(
                        'activated'    => 1,
                        'activate_key' => '',
                        'password'     => $aUserData['password'],
                    )
                );
                if ($bSuccess) {
                    # триггер активации аккаунта
                    bff::i()->callModules('onUserActivated', array($nUserID));
                    # отправляем письмо об успешной автоматической регистрации
                    bff::sendMailTemplate(array(
                            'name'     => $aUserData['name'],
                            'email'    => $aUserData['email'],
                            'password' => $sPassword
                        ),
                        'users_register_auto', $aUserData['email']
                    );
                }
            }
            # авторизуем, если текущий пользователь неавторизован
            if (!User::id()) {
                Users::i()->userAuth($nUserID, 'user_id', $aUserData['password'], true);
            }
        }

        # 3. Публикуем заказ:
        $bSuccess = $this->model->orderSave($nItemID, array(
                'activate_key' => '', # чистим ключ активации
                'status_prev'  => self::STATUS_NOTACTIVATED,
                'status'       => self::STATUS_OPENED,
                'moderated'    => 0, # помечаем на модерацию
            )
        );

        # обновляем счетчик заказов "на модерации"
        $this->moderationCounterUpdate();

        if (!$bSuccess) {
            return $this->showForbidden($langActivateTitle,
                _t('orders', 'Ошибка активации, обратитесь в службу поддержки.')
            );
        }

        if (!static::premoderation()) {
            # уведомление исполнителей о новом заказе
            $this->enotifyNewOrder($nItemID);
        }

        # накручиваем счетчик кол-ва заказов авторизованного пользователя
        $this->security->userCounter('orders', 1, $nUserID); # +1

        $this->redirect(static::url('status', array('id' => $nItemID)));
        return '';
    }

    /**
     * Кабинет: Заказы
     * @param array $userData данные о пользователе
     */
    public function my_orders(array $userData)
    {
        if (Users::useClient() && User::isWorker()) {
            return $this->orders_respondent_listing($userData['id'], $userData);
        }

        return $this->user_orders($userData);
    }

    /**
     * Кабинет: Заявки
     * @param array $userData данные о пользователе
     */
    public function my_offers(array $userData)
    {
        if (User::isClient() && Users::useClient()) {
            return $this->orders_owner_listing($userData['id'], $userData);
        }

        return $this->orders_respondent_listing($userData['id'], $userData);
    }

    /**
     * Профиль: Заказы
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    public function user_orders(array $userData)
    {
        if (empty($userData['id'])) {
            $this->errors->error404();
        }

        return $this->orders_owner_listing($userData['id'], $userData);
    }

    /**
     * Список заказов пользователя (владельца заказов).
     * @param integer $nUserID ID пользователя
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    protected function orders_owner_listing($nUserID, array $userData)
    {
        $aFilter = array(
            'user_id' => $nUserID,
            'removed' => 0,
        );

        $seoFilter = 0;
        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT, # № страницы
            't'    => TYPE_UINT, # Тип
            'st'   => TYPE_UINT, # Статус
        ));

        if ($f['t']) {
            $aFilter['type'] = $f['t'];
            $seoFilter++;
        }

        if ($f['st']) {
            switch ($f['st']) {
                case static::STATUS_OPENED:
                case static::STATUS_CLOSED:
                case static::STATUS_BLOCKED:
                    $aFilter['status'] = $f['st'];
                    break;
                case static::STATUS_NOTMODERATED:
                    $aFilter['moderated'] = 0;
                    break;
                case static::STATUS_IN_WORK:
                    $aFilter['in_work'] = 1;
                    break;
                case static::STATUS_REMOVED:
                    $aFilter['removed'] = 1;
                    break;
            }
            $seoFilter++;
        }

        $aData['aStatus'] = array(
            0                           => array( 'id' => 0,                            't' => _t('orders', 'Любой статус')),
            static::STATUS_IN_WORK      => array( 'id' => static::STATUS_IN_WORK,       't' => _t('orders', 'В работе') ),
            static::STATUS_OPENED       => array( 'id' => static::STATUS_OPENED,        't' => _t('orders', 'Открытые') ),
            static::STATUS_CLOSED       => array( 'id' => static::STATUS_CLOSED,        't' => _t('orders', 'Закрытые') ),
            static::STATUS_BLOCKED      => array( 'id' => static::STATUS_BLOCKED,       't' => _t('orders', 'Заблокированные') ),
            static::STATUS_NOTMODERATED => array( 'id' => static::STATUS_NOTMODERATED,  't' => _t('orders', 'На модерации') ),
            static::STATUS_REMOVED      => array( 'id' => static::STATUS_REMOVED,       't' => _t('orders', 'Корзина') ),
        );
        if ( ! $userData['my']) {
            unset(  $aData['aStatus'][static::STATUS_IN_WORK],
                    $aData['aStatus'][static::STATUS_BLOCKED],
                    $aData['aStatus'][static::STATUS_NOTMODERATED],
                    $aData['aStatus'][static::STATUS_REMOVED]
            );
        }
        $aData['pgn'] = '';
        $aData['counts'] = $this->model->ordersListOwnerCounts($aFilter);
        $nCount = $this->model->ordersListOwner($aFilter, true);
        if ($nCount) {
            $pgn = new Pagination($nCount, config::sysAdmin('orders.profile.pagesize', 10, TYPE_UINT), array(
                'link'  => static::url('user.listing', array('login'=>$userData['login'])),
                'query' => $f,
            ));
            $aData['list'] = $this->model->ordersListOwner($aFilter, false, $pgn->getLimitOffset(), 'O.created DESC');
            foreach ($aData['list'] as &$v) {

            } unset($v);
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }
        $aData['list'] = $this->viewPHP($aData, 'owner.list.ajax');
        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'    => $aData['pgn'],
                'list'   => $aData['list'],
                'counts' => $aData['counts'],
            ));
        }

        # SEO:
        $this->seo()->robotsIndex(!$seoFilter);
        $seoURL = static::url('user.listing', array('login'=>$userData['login']), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL, array('page'=>$f['page']));
        $this->seo()->setPageMeta($this->users(), 'profile-orders', array(
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'region'  => $userData['region'],
            'country' => $userData['country'],
            'page'    => $f['page'],
        ), $aData);

        $aData['f'] = &$f;
        $aData['user'] = &$userData;
        return $this->viewPHP($aData, 'owner.listing');
    }

    /**
     * Список заказов пользователя (владельца одной из заявок к заказам).
     * @param integer $nUserID ID пользователя
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    protected function orders_respondent_listing($nUserID, array $userData)
    {
        $ordersOpinions = static::ordersOpinions();
        $invitesEnabled = static::invitesEnabled();
        $aFilter = array(
            'user_id' => $nUserID,
        );

        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT, # № страницы
            'st'   => TYPE_UINT, # Статус
        ));

        $aFilterTypes = array(
            0 => array('id' => 0, 't' => _t('orders', 'Все заказы')),
            1 => array('id' => 1, 't' => _t('orders', 'Не определен')),
            2 => array('id' => 2, 't' => _t('orders', 'Кандидат')),
            3 => array('id' => 3, 't' => _t('orders', 'Исполнитель')),
            4 => array('id' => 4, 't' => _t('orders', 'Отказы')),
            5 => array('id' => 5, 't' => _t('orders', 'Вы отказались')),
            6 => array('id' => 6, 't' => _t('orders', 'Корзина')),
        );
        if( ! $ordersOpinions){
            unset($aFilterTypes[5]);
        }

        switch ($f['st']) {
            case 0: # все, кроме отправленных в корзину
                $aFilter['user_removed'] = 0;
                break;
            case 1:
                $aFilter['status'] = 0;
                $aFilter['user_removed'] = 0;
                break;
            case 2:
                $aFilter['status'] = static::OFFER_STATUS_CANDIDATE;
                $aFilter['user_removed'] = 0;
                break;
            case 3:
                $aFilter['status'] = array(
                    static::OFFER_STATUS_PERFORMER,
                    static::OFFER_STATUS_PERFORMER_START,
                    static::OFFER_STATUS_PERFORMER_AGREE,
                );
                $aFilter['user_removed'] = 0;
                break;
            case 4:
                $aFilter['status'] = static::OFFER_STATUS_CANCELED;
                $aFilter['user_removed'] = 0;
                break;
            case 5:
                $aFilter['status'] = array(static::OFFER_STATUS_PERFORMER_DECLINE, static::OFFER_STATUS_INVITE_DECLINE);
                $aFilter['user_removed'] = 0;
                break;
            case 6: # отправленные в корзину
                $aFilter['user_removed'] = 1;
                break;
        }

        $aCounts = $this->model->ordersListRespondentCounts($aFilter, false);
        if( ! $ordersOpinions){
            unset($aCounts[5]);
        }
        foreach ($aCounts as $k => $v) {
            $aFilterTypes[$k]['c'] = $v;
        }

        $aData = array('types' => $aFilterTypes, 'f'=>&$f, 'user'=>&$userData, 'pgn'=>'');
        $aData['aOrderMainStatuses'] = static::getOrderMainStatusesContent();
        $nCount = $this->model->ordersListRespondent($aFilter, true);
        if ($nCount) {
            $pgn = new Pagination($nCount, config::sysAdmin('orders.profile.pagesize', 10, TYPE_UINT), array(
                'link'  => static::url('user.listing', array('login'=>$userData['login'])),
                'query' => $f,
            ));
            $aData['list'] = $this->model->ordersListRespondent($aFilter, false, $pgn->getLimitOffset(), 'O.created DESC');
            foreach ($aData['list'] as &$v) {
                $v['url_view'] = static::url('view', array('id'=>$v['id'], 'keyword'=>$v['keyword']));
                $v['url_view_chat'] = static::url('view', array('id'=>$v['id'], 'keyword'=>$v['keyword'], 'chat'=>$v['offer_id']));
                $v['my'] = User::isCurrent($v['user_id']);
                $v['specs'] = $this->model->orderSpecs($v['id']);
                # Дин. свойства
                if ($v['type'] == static::TYPE_SERVICE) {
                    $aFirstSpec = reset($v['specs']);
                    if (static::dynpropsEnabled()) {
                        $v['dynprops_simple'] = Specializations::i()->dpOrdersView($aFirstSpec['spec_id'], $v, 'd', 'view.dp.simple');
                        $v['dynprops_simple'] = json_decode($v['dynprops_simple'], true);
                    }
                }
            } unset($v);
            $aData['pgn'] = $pgn->view();
            $f['page'] = $pgn->getCurrentPage();
        }
        $aData['list'] = $this->viewPHP($aData, 'respondent.list.ajax');
        if (Request::isAJAX()) {
            $aCounts = array();
            foreach ($aFilterTypes as $k => $v) {
                $aCounts[$k] = $v['c'];
            }
            $this->ajaxResponseForm(array(
                'pgn'    => $aData['pgn'],
                'list'   => $aData['list'],
                'counts' => $aCounts,
            ));
        }
        $count = 0;
        if ($ordersOpinions && User::isCurrent($nUserID)) {
            $aData['performerStart'] = $this->model->ordersListPerformerStart();
            $count += sizeof($aData['performerStart']);
            foreach ($aData['performerStart'] as &$v) {
                $v['link'] = static::url('view', array('id'=>$v['order_id'], 'keyword'=>$v['keyword']));
                $v['link_chat'] = static::url('view', array('id'=>$v['order_id'], 'keyword'=>$v['keyword'], 'chat'=>$v['id']));
                $v['reg3_city'] = $this->model->getOrderCity($v['order_id']);
                if ($v['reg3_city']) {
                    $v['city_data'] = Geo::regionData($v['reg3_city']);
                }
                $v['specs'] = $this->model->orderSpecs($v['order_id']);
                $v['order_data'] = $this->model->orderData($v['order_id']);
                # Дин. свойства
                if ($v['order_data']['type'] == static::TYPE_SERVICE) {
                    $aFirstSpec = reset($v['specs']);
                    if (static::dynpropsEnabled()) {
                        $v['dynprops_simple'] = Specializations::i()->dpOrdersView($aFirstSpec['spec_id'], $v['order_data'], 'd', 'view.dp.simple');
                        $v['dynprops_simple'] = json_decode($v['dynprops_simple'], true);
                    }
                }
            } unset($v);
            $aData['performerStart'] = $this->viewPHP($aData, 'respondent.list.performer.start');
        }
        if ($invitesEnabled && User::isCurrent($nUserID)){
            $aData['ordersInvites'] = $this->model->ordersListInvite(array(
                'worker_id' => User::id(),
            ));
            $count += sizeof($aData['ordersInvites']);
            $delete = array();
            foreach ($aData['ordersInvites'] as $k => &$v) {
                if($v['order_status'] != static::STATUS_OPENED){
                    $delete[] = $v['id'];
                    unset($aData['ordersInvites'][$k]);
                    continue;
                }
                $v['link'] = static::url('view', array('id'=>$v['id'], 'keyword'=>$v['keyword']));
                $v['specs'] = $this->model->orderSpecs($v['id']);
                # Дин. свойства
                if ($v['type'] == static::TYPE_SERVICE) {
                    $aFirstSpec = reset($v['specs']);
                    if (static::dynpropsEnabled()) {
                        $v['dynprops_simple'] = Specializations::i()->dpOrdersView($aFirstSpec['spec_id'], $v, 'd', 'view.dp.simple');
                        $v['dynprops_simple'] = json_decode($v['dynprops_simple'], true);
                    }
                }
            } unset($v);
            $aData['ordersInvites'] = $this->viewPHP($aData, 'respondent.list.invites');
            if( ! empty($delete)){
                # удалим зависшие заявки - заказы уже закрылись
                $this->model->inviteDelete(array(
                    'user_id'  => $nUserID,
                    'order_id' => $delete,
                ));
                $count = -1;
            }
        }

        if(User::isCurrent($nUserID)){
            # проверка необходимости пересчета счетчика в шапке
            $counts = bff::security()->userCounter(array());
            if( ! isset($counts['cnt_orders_offers_worker'])){
                $counts['cnt_orders_offers_worker'] = 0;
            }
            if($count != $counts['cnt_orders_offers_worker']){
                $this->model->calcUserCounters($nUserID);
            }
        }

        # SEO:
        $this->seo()->robotsIndex(false);
        if (Users::useClient()) {
            $this->urlCorrection(static::url('user.listing', array('login'=>$userData['login'])));
            $this->seo()->setPageMeta($this->users(), 'profile-orders', array(
                'name'    => $userData['name'],
                'surname' => $userData['surname'],
                'login'   => $userData['login'],
                'city'    => $userData['city'],
                'country' => $userData['country'],
                'page'    => $f['page'],
            ), $aData);
        } else {
            $this->urlCorrection(static::url('my.offers'));
            $this->seo()->setPageMeta($this->users(), 'profile-offers', array(
                'name'    => $userData['name'],
                'surname' => $userData['surname'],
                'login'   => $userData['login'],
                'city'    => $userData['city'],
                'country' => $userData['country'],
                'page'    => $f['page'],
            ), $aData);
        }
        return $this->viewPHP($aData, 'respondent.listing');
    }

    /**
     * Проверка возможности добавления заявки к заказу
     * @param array $aOrderData @ref данные о заказе: id, type, status, order_pro
     * @param integer $nDenyReasons @ref битовая маска причин отказа
     *  1 - LimitExceed: превышение лимита заявок в день
     *  2 - ProOnly: только для PRO
     *  4 - specCheckFalse: исполнитель не соответствует специализации заказа
     * @return bool
     */
    protected function isOfferAddAllow(array &$aOrderData, &$nDenyReasons = 0)
    {
        $nDenyReasons = 0;
        do {
            if (empty($aOrderData) || $aOrderData['status'] != static::STATUS_OPENED) break;

            $nUserID =  User::id();
            if ( ! $nUserID || ! User::isWorker()) break;

            # Невозможно добавить заявку к своему заказу
            if($aOrderData['user_id'] == $nUserID){
                break;
            }

            # Кол-во оставленных заявок за последние сутки
            $offersToday = $this->model->offersDataByFilter(array(
                'user_id' => $nUserID,
                ':created' => array(
                    'created >= :fr AND created <= :to',
                    ':fr' => date('Y-m-d H:i:s', strtotime('-1day')),
                    ':to' => date('Y-m-d H:i:s')),
                'from_invite' => 0,
            ));
            if (User::isPro()) {
                if ($offersToday >= static::offersLimit(true)) {
                    $nDenyReasons |= 1; # LimitExceed
                    break;
                }
            } else {
                if (!empty($aOrderData['order_pro'])) {
                    $nDenyReasons |= 2; # ProOnly
                    break;
                }
                if ($offersToday >= static::offersLimit()) {
                    $nDenyReasons |= 1; # LimitExceed
                    break;
                }
            }

            # Проверка на соответствие специализации исполнителя специализации заказа
            if (static::offerSpecCheck() && $aOrderData['type'] == static::TYPE_SERVICE) {
                $aOrderSpecs = ( ! empty($aOrderData['specs']) ? $aOrderData['specs'] : $this->model->orderSpecs($aOrderData['id']) );
                if ( ! empty($aOrderSpecs)) {
                    $aUsersSpecs = Users::model()->userSpecs($nUserID, true);
                    if (empty($aUsersSpecs)) {
                        $nDenyReasons |= 4; # specCheckFalse
                        break;
                    }
                    $aUsersSpecs = array_keys($aUsersSpecs);
                    $bExist = false;
                    foreach ($aOrderSpecs as $v) {
                        if (in_array($v['spec_id'], $aUsersSpecs)) {
                            $bExist = true;
                            break;
                        }
                    }
                    if ( ! $bExist) {
                        $nDenyReasons |= 4; # specCheckFalse
                        break;
                    }
                }
            }

            return true;
        } while(false);
        return false;
    }

    /**
     * Блок данных о статусе заказа
     * @param array $aData @ref данные о заказе
     * @return string
     */
    public function orderStatusBlock(array &$aData)
    {
        if (empty($aData['user_id'])) return '';
        if ($aData['user_id'] != User::id()) return '';

        return $this->viewPHP($aData, 'status.block');
    }

    /**
     * Форма добавления заявки
     * @param array $aData данные заявки
     * @return string HTML
     */
    public function offer_add(array $aData = array())
    {
        if (Request::isAJAX()) {
            $nUserID = User::id();
            $aResponse = array();
            switch ($this->input->getpost('act', TYPE_STR)) {
                case 'add':
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }


                    $aData = $this->validateOfferData(0, Request::isPOST());
                    if ($this->errors->no('orders.offer.add.step1',array('id'=>0,'data'=>&$aData))) {
                        $nOrderID = $aData['order_id'];

                        $aOrderData = $this->model->orderData($nOrderID, array('id', 'type', 'status', 'pro', 'keyword', 'title', 'user_id', 'visibility', 'fairplay'));
                        if (empty($aOrderData)) {
                            $this->errors->reloadPage();
                            break;
                        }
                        $invite = false;
                        if (static::invitesEnabled()) {
                            $invite = $this->model->invitesDataByFilter(array(
                                'user_id'  => $nUserID,
                                'order_id' => $nOrderID,
                            ));
                            if ($aOrderData['visibility'] == static::VISIBILITY_PRIVATE) {
                                if ( ! $invite) {
                                    $this->errors->reloadPage();
                                }
                            }
                        }
                        if(bff::fairplayEnabled()){
                            if($aOrderData['fairplay'] == static::FAIRPLAY_USE){
                                $aData['fairplay'] = static::FAIRPLAY_USE;
                            }
                        }

                        if ( ! $invite) {
                            $aOrderData['order_pro'] = $aOrderData['pro'];
                            if (!$this->isOfferAddAllow($aOrderData)) {
                                $this->errors->set(_t('orders', 'Вы не можете добавлять предложения.'));
                                break;
                            }
                        }

                        $offers = $this->model->offersDataByFilter(array(
                            'user_id'  => $nUserID,
                            'order_id' => $nOrderID,
                        ));
                        if ($offers) {
                            $this->errors->set(_t('orders', 'Вы не можете добавить больше одного предложения.'));
                            break;
                        }

                        $aData['user_id'] = $nUserID;
                        $aData['user_ip'] = Request::remoteAddress();
                        $aData['is_new'] = 1;

                        # доверенный пользователь: без модерации
                        if ($trusted = User::isTrusted($this->module_name)) {
                            $aData['moderated'] = 1;
                        }
                        if ($invite) {
                            $aData['client_only'] = 1;
                            $aData['from_invite'] = 1;
                        }

                        if (!$this->errors->no('orders.offer.add.step2',array('id'=>0,'data'=>&$aData))) {
                            break;
                        }

                        $nOfferID = $this->model->offerSave(0, $aData);
                        if ($nOfferID) {
                            if (static::offerExamplesLimit()) {
                                $oImages = $this->offerImages($nOfferID);
                                $oImages->saveTmp('images');
                                $oExamples = $this->offerExamples($nOfferID);
                                $oExamples->save('sort');
                            }
                            $this->model->orderSave($nOrderID, array('offers_cnt = offers_cnt + 1'));
                            $aResponse['reload'] = 1;

                            # Обновляем счетчик заявок "на модерации"
                            if (!$trusted) {
                                $this->moderationOffersCounterUpdate(1);
                            }

                            # пересчитаем счетчик новых заказов пользователя
                            $this->model->calcUserCounters($aOrderData['user_id']);

                            if ($invite) {
                                $this->model->inviteDelete(array(
                                    'user_id'  => $nUserID,
                                    'order_id' => $nOrderID,
                                ));
                                $this->model->calcUserCounters($nUserID);

                                # Отправим уведомление заказчику
                                $aUserData = User::data(array('email','name','surname','login'));
                                $client = Users::model()->userDataEnotify($aOrderData['user_id']);
                                $fio = array();
                                if (!empty($aUserData['name'])) $fio[] = $aUserData['name'];
                                if (!empty($aUserData['surname'])) $fio[] = $aUserData['surname'];
                                $aUserData['fio'] = ( ! empty($fio) ? join(' ', $fio) : $aUserData['login']);

                                Users::sendMailTemplateToUser($client, 'order_invite_agree', array(
                                    'client_url'   => Users::url('profile', array('login' => $client['login'])),
                                    'worker_name'  => $aUserData['name'],
                                    'worker_fio'   => $aUserData['fio'],
                                    'worker_login' => $aUserData['login'],
                                    'worker_url'   => Users::url('profile', array('login' => $aUserData['login'])),
                                    'order_title'  => $aOrderData['title'],
                                    'order_url'    => static::url('view', array('id' => $nOrderID, 'keyword' => $aOrderData['keyword'])),
                                    'message'      => tpl::truncate($aData['descr'], config::sysAdmin('orders.email.invite.message.truncate', 150, TYPE_UINT)),
                                ));
                            } else {
                                # Отправим уведомление о новой заявке заказчику
                                $aUserData = User::data(array('email','name','surname','login'));
                                $fio = array();
                                if (!empty($aUserData['name'])) $fio[] = $aUserData['name'];
                                if (!empty($aUserData['surname'])) $fio[] = $aUserData['surname'];
                                $aUserData['fio'] = ( ! empty($fio) ? join(' ', $fio) : $aUserData['login']);
                                Users::sendMailTemplateToUser($aOrderData['user_id'], 'order_offer_add', array(
                                    'worker_name' => $aUserData['name'],
                                    'worker_fio'   => $aUserData['fio'],
                                    'worker_login' => $aUserData['login'],
                                    'worker_url' => Users::url('profile', array('login' => $aUserData['login'])),
                                    'order_title' => $aOrderData['title'],
                                    'order_url' => static::url('view', array('id' => $nOrderID, 'keyword' => $aOrderData['keyword'])),
                                    'offer_descr' => tpl::truncate($aData['descr'], config::sysAdmin('orders.email.offer.descr.truncate', 150, TYPE_UINT)),
                                ));
                            }
                        } else {
                            $this->errors->reloadPage();
                            break;
                        }
                    }
                }
                break;
                default:
                    $this->errors->reloadPage();
            }
            $this->ajaxResponseForm($aResponse);
        }

        $aData['img'] = $this->offerImages();
        $aData['availableExamples'] = '';
        $aExamples = $this->offerExamples()->availableExamples();
        if( ! empty($aExamples)) {
            if(isset($aData['specs'])){
                $aExamples['specs'] = $aData['specs'];
            }
            $aData['availableExamples'] = $this->viewPHP($aExamples, 'offer.add.form.examples');
        }

        return $this->viewPHP($aData, 'offer.add.form');
    }

    /**
     * Валидация данных для фомы добавления заявки
     * @param integer $nOfferID ID заявки
     * @param bool $bSubmit флаг сабмита формы
     * @return array
     */
    protected function validateOfferData($nOfferID, $bSubmit)
    {
        $aData = array();
        $aParam = array(
            'order_id'        => TYPE_UINT, # id заказа
            'price_from'      => TYPE_PRICE, # стоимость от
            'price_to'        => TYPE_PRICE, # стоимость до
            'price_curr'      => TYPE_UINT, # валюта бюджета
            'price_rate'      => TYPE_UINT, # срок ед измерения
            'terms_from'      => TYPE_UINT, # сроки: от
            'terms_to'        => TYPE_UINT, # сроки: до
            'terms_type'      => TYPE_UINT, # вариант сроков
            'descr'           => array(TYPE_TEXT, 'len' => 3000, 'len.sys' => 'orders.offer.descr.limit'), # описание
            'client_only'     => TYPE_BOOL, # подтип для услуги
        );
        if(bff::fairplayEnabled()){
            $aParam['fairplay'] = TYPE_BOOL; # Флаг безопасной сделки
        }

        $this->input->postm($aParam, $aData);
        if ($bSubmit) {
            if (!$aData['descr']) {
                $this->errors->set(_t('orders', 'Не указано описание'));
            }

            # антиспам фильтр
            if (Site::i()->spamFilter(array(
                array('text' => & $aData['descr'], 'error'=>_t('', 'В указанном вами описании присутствует запрещенное слово "[word]"')),
            ))) {
                return false;
            }

            if (!$aData['order_id']) {
                $this->errors->set(_t('orders', 'Не указан заказ'));

                return false;
            }

            $aOrderData = $this->model->orderData($aData['order_id'], array('type', 'status'));
            if (empty($aOrderData)) {
                $this->errors->set(_t('orders', 'Неверный заказ'));

                return false;
            }
            if ($aOrderData['status'] != static::STATUS_OPENED) {
                $this->errors->set(_t('orders', 'К этому заказу нельзя добавлять предложения'));

                return false;
            }

            if ($aOrderData['type'] == static::TYPE_SERVICE)
            {
                $aSpec = $this->model->orderSpecs($aData['order_id']);
                $aSpec = reset($aSpec);
                $nFirstSpecID = !empty($aSpec['spec_id']) ? $aSpec['spec_id'] : 0;
                if ($nFirstSpecID) {
                    $aPriceSett = Specializations::i()->aPriceSett($nFirstSpecID, true);
                    if (!empty($aPriceSett['rates'][$aData['price_rate']])) {
                        $aRates = $aPriceSett['rates'][$aData['price_rate']];
                        foreach ($aRates as $k => $v) {
                            $aRates[$k] = mb_strtolower($v);
                        }
                        $aData['price_rate_text'] = serialize($aRates);
                    }
                }
            } else if ($aOrderData['type'] == static::TYPE_PRODUCT) {
                $aData['terms_from'] = 0;
                $aData['terms_to']   = 0;
                $aData['terms_type'] = 0;
            }
            if (bff::servicesEnabled() && ! User::isPro()){
                unset($aData['client_only']);
            }

            bff::hook('orders.offer.validate', array('id'=>$nOfferID,'data'=>&$aData,'order'=>$aOrderData));
        }

        return $aData;
    }

    /**
     * Список заявок к заказу
     * @param integer $nOrderID ID заказа
     * @param array $aOrderData данные заказа
     * @param bool $bMyPresent @ref есть заявка текущего пользователя
     * @return string HTML
     */
    public function offers_list($nOrderID = 0, array $aOrderData = array(), &$bMyPresent = false, $bShowPerformerStart = true)
    {
        $nUserID = User::id();
        $ordersOpinions = static::ordersOpinions();
        $fairplayEnabled = bff::fairplayEnabled();

        if (Request::isAJAX()) {
            $aResponse = array();
            switch ($this->input->getpost('act', TYPE_STR)) {
                case 'chat': # переписка: обновление блока
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nOfferID = $this->input->post('id', TYPE_UINT);
                    if (!$nOfferID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aOfferData = $this->model->offerData($nOfferID, array('user_id', 'order_id', 'chat_new_client', 'chat_new_worker'));
                    if (empty($aOfferData)) {
                        $this->errors->impossible();
                        break;
                    }

                    # чат видят только владелец заявки и владелец заказа
                    if ($aOfferData['user_id'] != $nUserID) {
                        $aOrderData = $this->model->orderData($aOfferData['order_id'], array('user_id'));
                        if (empty($aOrderData)) {
                            $this->errors->accessDenied();
                            break;
                        }
                        if ($aOrderData['user_id'] != $nUserID) {
                            $this->errors->accessDenied();
                            break;
                        }
                    }

                    if ($aOfferData['chat_new_client'] || $aOfferData['chat_new_worker']) {
                        if ($aOfferData['user_id'] == $nUserID) { # чат открыл исполнитель
                            $this->model->offerSave($nOfferID, array('chat_new_worker' => 0));
                        } else { # чат открыл заказчик
                            $this->model->offerSave($nOfferID, array('chat_new_client' => 0));
                        }
                        # пересчитаем счетчик новых заказов пользователя
                        $this->model->calcUserCounters($nUserID);
                    }

                    $aData = array();
                    $aData['chat'] = $this->model->chatData($nOfferID);
                    foreach ($aData['chat'] as $v) {
                        if ($v['is_new'] && $v['author_id'] != $nUserID) {
                            $this->model->chatSave($v['id'], array('is_new' => 0));
                        }
                    }
                    $aResponse['html'] = $this->viewPHP($aData, 'offer.chat.ajax');
                }
                break;
                case 'chat-add': # переписка: добавление
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nOfferID = $this->input->post('id', TYPE_UINT);
                    if (!$nOfferID) {
                        $this->errors->impossible();
                        break;
                    }

                    $sMessage = $this->input->post('message', TYPE_TEXT, array('len' => 3000, 'len.sys' => 'orders.chat.message.limit'));
                    if (!strlen($sMessage)) {
                        $this->errors->set(_t('orders', 'Не указан текст'));
                        break;
                    }
                    $nChatID = $this->offerChatAdd($nOfferID, $sMessage);
                    if($nChatID) {
                        $aData = array();
                        $aData['chat'] = $this->model->chatData($nOfferID, array('id' => $nChatID));
                        $aResponse['html'] = $this->viewPHP($aData, 'offer.chat.ajax');
                    }
                }
                break;
                case 'status': # изменение статуса заявки
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nOfferID = $this->input->post('id', TYPE_UINT);
                    if (!$nOfferID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $nStatus = $this->input->post('status', TYPE_UINT);
                    if (!in_array($nStatus, array(static::OFFER_STATUS_CANDIDATE, static::OFFER_STATUS_PERFORMER, static::OFFER_STATUS_CANCELED))) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($ordersOpinions && $nStatus == static::OFFER_STATUS_PERFORMER){
                        $this->errors->reloadPage();
                        break;
                    }

                    $aOfferData = $this->model->offerData($nOfferID, array('order_id', 'user_id', 'status'));
                    if (empty($aOfferData)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($ordersOpinions && $aOfferData == static::OFFER_STATUS_PERFORMER_START) {
                        $this->errors->set(_t('order', 'Невозможно изменить статус'));
                        break;
                    }

                    $nOrderID = $aOfferData['order_id'];
                    $aOrderData = $this->model->orderData($nOrderID, array('user_id', 'status', 'performer_id', 'offers_cnt'));
                    if (empty($aOrderData)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if (!$this->isOrderOwner($nOrderID, $aOrderData['user_id'])) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($aOrderData['status'] != static::STATUS_OPENED) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if ($nStatus == static::OFFER_STATUS_PERFORMER) {
                        if ($aOrderData['performer_id']) {
                            if ($aOrderData['performer_id'] != $aOfferData['user_id']) {
                                $this->errors->reloadPage();
                            }
                            break;
                        } else {
                            $this->model->orderSave($nOrderID, array(
                                    'performer_id'      => $aOfferData['user_id'],
                                    'performer_created' => $this->db->now(),
                                )
                            );
                            $nAddRating = static::ratingPerformer();
                            if ($nAddRating > 0) {
                                # добавим рейтинг - назначили исполнителем
                                Users::model()->ratingChange($aOfferData['user_id'], $nAddRating, 'orders-offer-performer-add',
                                    array('order_id'=>$nOrderID, 'offer_id'=>$nOfferID));
                            }
                        }
                    } else {
                        if ($aOrderData['performer_id'] && ! $ordersOpinions) {
                            $this->model->orderSave($nOrderID, array(
                                    'performer_id'      => 0,
                                    'performer_created' => 0,
                                )
                            );
                            $nAddRating = static::ratingPerformer();
                            if ($nAddRating > 0) {
                                # уменьшим рейтинг - убрали исполнителя
                                Users::model()->ratingChange($aOrderData['performer_id'], -$nAddRating, 'orders-offer-performer-del',
                                    array('order_id'=>$nOrderID, 'offer_id'=>$nOfferID));
                            }
                        }
                    }

                    $this->model->offerSave($nOfferID, array(
                            'status'         => $nStatus,
                            'status_created' => $this->db->now(),
                            'enotify_send'   => 0,
                        )
                    );
                }
                break;
                case 'performer-start': # Предложение стать исполнителем (заказчиком)
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nOfferID = $this->input->post('id', TYPE_UINT);
                    if (!$nOfferID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if( ! $ordersOpinions){
                        $this->errors->reloadPage();
                        break;
                    }
                    $aOfferData = $this->model->offerData($nOfferID, array('order_id', 'user_id', 'fairplay'));
                    if (empty($aOfferData)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $nOrderID = $aOfferData['order_id'];

                    $aOrderData = $this->model->orderData($nOrderID, array('user_id', 'status', 'performer_id', 'fairplay'));
                    if (empty($aOrderData)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if (!$this->isOrderOwner($nOrderID, $aOrderData['user_id'])) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($aOrderData['status'] != static::STATUS_OPENED) {
                        $this->errors->set(_t('order', 'Заказ снят с публикации.'));
                        break;
                    }
                    if ($aOrderData['performer_id']) {
                        $this->errors->set(_t('order', 'Исполнитель уже назначен.'));
                        break;
                    }
                    $offers = $this->model->orderOffers($nOrderID, array('status' =>
                        array(static::OFFER_STATUS_PERFORMER_START, static::OFFER_STATUS_PERFORMER_AGREE))
                    );
                    if( ! empty($offers)){
                        $this->errors->set(_t('order', 'Исполнитель уже выбран.'));
                        break;
                    }

                    $offerSave = array(
                        'status'         => static::OFFER_STATUS_PERFORMER_START,
                        'status_created' => $this->db->now(),
                        'enotify_send'   => 0,
                    );
                    $orderSave = array(
                        'performer_id'      => $aOfferData['user_id'],
                        'performer_created' => $this->db->now(),
                    );

                    $workflow = false;
                    if ($fairplayEnabled) {
                        $workflow = Fairplay::i()->validateOfferWorkflowData();
                        if ( ! $this->errors->no()) {
                            break;
                        }
                        # переведем заказ в с БС, если необходимо
                        if ($aOrderData['fairplay'] != $workflow['fairplay']) {
                            $orderSave['fairplay'] = $workflow['fairplay'];
                        }
                        $offerSave['workflow'] = serialize($workflow);
                    }

                    $message = $this->input->post('message', TYPE_TEXT, array('len' => 3000, 'len.sys' => 'orders.performer.message.limit'));
                    if ($message) {
                        $chatID = $this->model->chatSave(0, array(
                                'offer_id'  => $nOfferID,
                                'author_id' => $nUserID,
                                'message'   => $message,
                            )
                        );
                        $offerSave['chat_id'] = $chatID;
                    }

                    $this->model->offerSave($nOfferID, $offerSave);

                    $this->model->orderSave($nOrderID, $orderSave);

                    # пересчитаем счетчик новых заказов пользователя
                    $this->model->calcUserCounters($aOfferData['user_id']);
                    $data = array('workflow' => $workflow, 'offerID' => $nOfferID);
                    $aResponse['html'] = $this->viewPHP($data, 'offers.list.alert.performer.start');
                }
                break;
                case 'performer-cancel': # Отмена предложения стать исполнителем (заказчиком)
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nOfferID = $this->input->post('id', TYPE_UINT);
                    if (!$nOfferID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if( ! $ordersOpinions){
                        $this->errors->reloadPage();
                        break;
                    }
                    $aOfferData = $this->model->offerData($nOfferID, array('order_id', 'user_id', 'status', 'enotify_send'));
                    if (empty($aOfferData)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $nOrderID = $aOfferData['order_id'];

                    $aOrderData = $this->model->orderData($nOrderID, array('id', 'user_id', 'status', 'performer_id', 'title', 'keyword'));
                    if (empty($aOrderData)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if (!$this->isOrderOwner($nOrderID, $aOrderData['user_id'])) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($aOfferData['status'] != static::OFFER_STATUS_PERFORMER_START) {
                        $this->errors->set(_t('orders', 'Невозможно отменить исполнителя'));
                        break;
                    }
                    $this->model->offerSave($nOfferID, array(
                            'status'         => 0,
                            'status_created' => $this->db->now(),
                            'enotify_send'   => 0,
                        )
                    );
                    $this->model->orderSave($nOrderID, array(
                            'performer_id'      => 0,
                            'performer_created' => $this->db->now(),
                        )
                    );
                    # пересчитаем счетчик новых заказов пользователя
                    $this->model->calcUserCounters($aOfferData['user_id']);

                    if ($aOfferData['enotify_send'] != '0000-00-00 00:00:00') {
                        $this->orderOfferMailSendNow('order_offer_performer_stop', $aOrderData['performer_id'], $aOrderData);
                    }
                }
                break;
                case 'modal-performer-decline': # окно для ввода причины отказа
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $nOfferID = $this->input->post('id', TYPE_UINT);
                    if (!$nOfferID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if( ! $ordersOpinions){
                        $this->errors->reloadPage();
                        break;
                    }
                    $aOfferData = $this->model->offerData($nOfferID, array('order_id', 'user_id', 'status'));
                    if (empty($aOfferData)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($aOfferData['user_id'] != $nUserID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($aOfferData['status'] != static::OFFER_STATUS_PERFORMER_START) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nOrderID = $aOfferData['order_id'];

                    $data = $this->model->orderData($nOrderID, array('title', 'keyword'));
                    if (empty($data)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $data['link'] = static::url('view', array('id' => $nOrderID, 'keyword' => $data['keyword']));
                    $data['id'] = $nOfferID;
                    $aResponse['html'] = $this->viewPHP($data, 'performer.decline.modal');
                }
                break;
                case 'performer-decline': # Отказ от предложения стать исполнителем (исполнителем)
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nOfferID = $this->input->post('id', TYPE_UINT);
                    if (!$nOfferID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if( ! $ordersOpinions){
                        $this->errors->reloadPage();
                        break;
                    }

                    $message = $this->input->post('message', TYPE_TEXT, array('len' => 3000, 'len.sys' => 'orders.performer.message.limit'));
                    if (strlen($message) < config::sys('orders.performer.message.min', 5, TYPE_UINT)) {
                        $this->errors->set(_t('orders', 'Укажите причину отказа'));
                        break;
                    }

                    $aOfferData = $this->model->offerData($nOfferID, array('order_id', 'user_id', 'status', 'enotify_send'));
                    if (empty($aOfferData)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($aOfferData['user_id'] != $nUserID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($aOfferData['status'] != static::OFFER_STATUS_PERFORMER_START) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nOrderID = $aOfferData['order_id'];

                    $aOrderData = $this->model->orderData($nOrderID, array('id', 'user_id', 'status', 'performer_id', 'title', 'keyword'));
                    if (empty($aOrderData)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $chatID = $this->offerChatAdd($nOfferID, $message, false);
                    $this->model->offerSave($nOfferID, array(
                        'status'         => static::OFFER_STATUS_PERFORMER_DECLINE,
                        'status_created' => $this->db->now(),
                        'chat_id'        => $chatID,
                    ));
                    $this->model->orderSave($nOrderID, array(
                        'performer_id'      => 0,
                        'performer_created' => $this->db->now(),
                    ));
                    # пересчитаем счетчик новых заказов пользователя
                    $this->model->calcUserCounters($nUserID);
                    $aOrderData['{message}']  = $message;
                    $this->orderOfferMailSendNow('order_offer_performer_decline', $aOrderData['user_id'], $aOrderData);
                }
                break;
                case 'performer-agree': # Согласие на предложение стать исполнителем (исполнителем)
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nOfferID = $this->input->post('id', TYPE_UINT);
                    if (!$nOfferID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ( ! $ordersOpinions) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $aOfferData = $this->model->offerData($nOfferID, array('order_id', 'user_id', 'status', 'enotify_send'));
                    if (empty($aOfferData)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($aOfferData['user_id'] != $nUserID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if ($aOfferData['status'] != static::OFFER_STATUS_PERFORMER_START) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nOrderID = $aOfferData['order_id'];

                    $aOrderData = $this->model->orderData($nOrderID, array('id', 'user_id', 'status', 'performer_id', 'title', 'keyword'));
                    if (empty($aOrderData)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if ($aOrderData['performer_id'] != $nUserID || $aOrderData['status'] != static::STATUS_OPENED) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $this->model->offerSave($nOfferID, array(
                        'status'         => static::OFFER_STATUS_PERFORMER_AGREE,
                        'status_created' => $this->db->now(),
                    ));
                    $this->model->orderSave($nOrderID, array(
                        'status_prev'    => $aOrderData['status'],
                        'status'         => static::STATUS_CLOSED,
                        'status_changed' => $this->db->now(),
                    ));
                    if($fairplayEnabled){
                        Fairplay::i()->workflowCreate($nOrderID);
                    }

                    # пересчитаем счетчик новых заказов пользователя
                    $this->model->calcUserCounters($nUserID);
                    $this->orderOfferMailSendNow('order_offer_performer_agree', $aOrderData['user_id'], $aOrderData);
                }
                break;
                case 'modal-workflow-change':
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if( ! $fairplayEnabled){
                        $this->errors->reloadPage();
                        break;
                    }
                    $offerID = $this->input->post('id', TYPE_UINT);
                    if ( ! $offerID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $offer = $this->model->offerData($offerID, array('id', 'order_id', 'status', 'workflow'));
                    if (empty($offer) || $offer['status'] != Orders::OFFER_STATUS_PERFORMER_START) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $offer['workflow'] = func::unserialize($offer['workflow']);
                    $order = $this->model->orderData($offer['order_id'], array('id', 'user_id'));
                    if (empty($order) || $order['user_id'] != $nUserID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $aResponse['html'] = $this->viewPHP($offer, 'offers.list.workflow.change.modal');
                }
                break;
                case 'workflow-change':
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if( ! $fairplayEnabled){
                        $this->errors->reloadPage();
                        break;
                    }
                    $offerID = $this->input->post('id', TYPE_UINT);
                    if ( ! $offerID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $offer = $this->model->offerData($offerID, array('id', 'order_id', 'status'));
                    if (empty($offer) || $offer['status'] != Orders::OFFER_STATUS_PERFORMER_START) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $orderID = $offer['order_id'];
                    $order = $this->model->orderData($orderID, array('id', 'user_id', 'fairplay', 'performer_id'));
                    if (empty($order) || $order['user_id'] != $nUserID) {
                        $this->errors->reloadPage();
                        break;
                    }
                    $workflow = Fairplay::i()->validateOfferWorkflowData();
                    if( ! $this->errors->no()){
                        break;
                    }
                    # переведем заказ в с БС, если необходимо
                    if($order['fairplay'] != $workflow['fairplay']) {
                        $this->model->orderSave($orderID, array(
                            'fairplay' => $workflow['fairplay'],
                        ));
                    }
                    $this->model->offerSave($offerID, array(
                        'workflow' => serialize($workflow),
                    ));
                    $data = array('workflow' => $workflow, 'offerID' => $offerID);
                    $aResponse['html'] = $this->viewPHP($data, 'offers.list.alert.performer.start');
                    $data['performer'] = Users::model()->userData($order['performer_id'], array('login', 'name', 'surname', 'pro', 'sex', 'verified'));
                    $aResponse['general'] = $this->viewPHP($data, 'offers.list.alert.performer.start');
                }
                break;
                default:
                    $this->errors->impossible();
            }
            $this->ajaxResponseForm($aResponse);
        }

        $aData = array('myOffer' => '', 'offers_cnt' => $aOrderData['offers_cnt'], 'fairplay' => $aOrderData['fairplay'],
                       'price' => $aOrderData['price_search'], 'order_id' => $nOrderID, 'keyword' => $aOrderData['keyword']);

        # скрываем список заявок для неавторизованного пользователя
        if (!$nUserID) {
            return $this->viewPHP($aData, 'offers.list.guest');
        }

        $isOrderOwner = $this->isOrderOwner($nOrderID, $aOrderData['user_id']);
        $aFilter = array();
        if ( ! $isOrderOwner) {
            # скрываем заявки помеченные как "видимые только для заказчика"
            if(User::isWorker()){
                $aFilter[':client_only'] = array(' (O.client_only = 0 OR O.user_id = :me) ', ':me' => $nUserID);
            } else {
                $aFilter['client_only'] = 0;
            }
        }
        $aOffers = $this->model->orderOffers($nOrderID, $aFilter);
        # заказ просматривает: владелец заказа
        if ($isOrderOwner)
        {
            # переместим исполнителя вверх, если назначен
            if ($aOrderData['performer_id']) {
                foreach ($aOffers as $k => $v) {
                    if ($v['user_id'] == $aOrderData['performer_id']) {
                        $aData['performer_status'] = $v['status'];
                        unset($aOffers[$k]);
                        array_unshift($aOffers, $v);
                        break;
                    }
                }
            }
            # показываем примеры работ
            $isNew = false;
            $aData['statuses'] = array();
            foreach ($aOffers as &$v) {
                $v['examples'] = $this->offerExamples($v['id'])->getData($v['examplescnt'], 'id', $v['login']);
                $v['bAllWork'] = 1;
                $v['examples'] = $this->viewPHP($v, 'offers.list.examples');
                if($v['is_new']){
                    $isNew = true;
                }
                if( ! isset($aData['statuses'][ $v['status'] ])){
                    $aData['statuses'][ $v['status'] ] = 0;
                }
                $aData['statuses'][ $v['status'] ]++;
            }
            unset($v);

            $aData['offers'] = $aOffers;
            $aData['bChoice'] = ($aOrderData['status'] == static::STATUS_OPENED);
            $aData['performer_id'] = $aOrderData['performer_id'];

            if($isNew){
                # удалим признак новых заявок
                $this->model->orderOffersClearNew($nOrderID);
                # пересчитаем счетчик новых заказов пользователя
                $this->model->calcUserCounters(User::id());
            }

            $aData['order_title'] = $aOrderData['title'];
            return $this->viewPHP($aData, 'offers.list.owner');
        }
        $nMy = false;
        if ($nUserID) {
            foreach ($aOffers as $k => &$v) {
                $v['examples'] = $this->offerExamples($v['id'])->getData($v['examplescnt'], 'id', $v['login']);
                $v['examples'] = $this->viewPHP($v, 'offers.list.examples');
                if ($v['user_id'] == $nUserID) {
                    $nMy = $k;
                    $v['bMy'] = true;
                    $v['descr'] = $this->offer_my_descr($v);
                    $bMyPresent = true;
                } else {
                    $v['descr'] = nl2br($v['descr']);
                }
            }
            unset($v);
        }

        if ($nMy !== false && $ordersOpinions) {
            $performer = $this->model->ordersListPerformerStart(array('order_id' => $nOrderID));
            if( ! empty($performer)) {
                $performer = reset($performer);
                $aOffers[$nMy]['performer_start'] = $this->viewPHP($performer, 'offer.my.performer.start');
                $aOffers[$nMy]['show_erformer_start'] = $bShowPerformerStart;
            }
        }

        # в приватном заказе видим только свою заявку
        if (static::invitesEnabled() && $aOrderData['visibility'] == static::VISIBILITY_PRIVATE) {
            if ($nMy !== false) {
                $aMy = $aOffers[$nMy];
                $aOffers = array($aMy);
                $nMy = 0;
            } else {
                $aOffers = array();
            }
        }

        # переместим свою заявку выше остальных
        if ($nMy) {
            $aMy = $aOffers[$nMy];
            unset($aOffers[$nMy]);
            array_unshift($aOffers, $aMy);
        }

        $aData['offers'] = &$aOffers;

        return $this->viewPHP($aData, 'offers.list');
    }

    /**
     * Переписка между заказчиком и исполнителем, добавление сообщения
     * @param integer $nOfferID ID заявки
     * @param string $sMessage сообщение
     * @param bool $sendEmail отправить почтовое уведомление
     * @return bool|integer ID сообщения
     */
    protected function offerChatAdd($nOfferID, $sMessage, $sendEmail = true)
    {
        $nUserID = User::id();
        $aOfferData = $this->model->offerData($nOfferID, array('user_id', 'order_id', 'chat_new_client', 'chat_new_worker'));
        if (empty($aOfferData)) {
            $this->errors->impossible();
            return false;
        }

        $aOrderData = $this->model->orderData($aOfferData['order_id'], array('user_id', 'title', 'keyword'));
        $nAddrUserID = $aOrderData['user_id'];
        # чат добавляют только владелец заявки и владелец заказа
        if ($aOfferData['user_id'] != $nUserID) {
            $nAddrUserID = $aOfferData['user_id'];
            if (empty($aOrderData)) {
                $this->errors->accessDenied();
                return false;
            }
            if ($aOrderData['user_id'] != $nUserID) {
                $this->errors->accessDenied();
                return false;
            }
        }

        $nChatID = $this->model->chatSave(0, array(
                'offer_id'  => $nOfferID,
                'author_id' => $nUserID,
                'message'   => $sMessage,
                'is_new'    => 1,
            )
        );

        if ($nChatID) {
            if($nUserID == $aOfferData['user_id']){
                # владалец заявки
                $this->model->offerSave($nOfferID, array('chat_new_client' => $aOfferData['chat_new_client'] + 1));

                # пересчитаем счетчик новых заказов пользователя
                $this->model->calcUserCounters($aOrderData['user_id']);
            } else {
                # владелец заказа
                $this->model->offerSave($nOfferID, array('chat_new_worker' => $aOfferData['chat_new_worker'] + 1));

                # пересчитаем счетчик новых заказов пользователя
                $this->model->calcUserCounters($aOfferData['user_id']);
            }

            if ($sendEmail) {
                # Отправляем уведомление
                Users::sendMailTemplateToUser($nAddrUserID, 'orders_offer_chat', array(
                    'author_name' => User::data('name'),
                    'order_id' => $aOfferData['order_id'],
                    'order_url' => static::url('view', array('id' => $aOfferData['order_id'], 'keyword' => $aOrderData['keyword'])),
                    'order_title' => $aOrderData['title'],
                    'message' => nl2br(tpl::truncate($sMessage, config::sysAdmin('orders.offer.chat.message.truncate', 150, TYPE_UINT))),
                ), Users::ENOTIFY_GENERAL);
            }
        }
        return $nChatID;
    }

    /**
     * Форма просмотра заявки пользователем (собственной заявки)
     * @param array $aData @ref данные о заявке
     * @return string HTML
     */
    public function offer_my_descr(array &$aData = array())
    {
        # Интервал времени, в течении которого доступно редактирование описания заявки
        $nSecsEditAfterAdd = 3600;

        if (Request::isAJAX()) {
            $nUserID = User::id();
            $aResponse = array();
            switch ($this->input->getpost('act', TYPE_STR)) {
                case 'descr': # добавление / редактирование текста заявки
                {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if (!User::isWorker()) {
                        $this->errors->set(_t('orders', 'Вы не можете добавлять предложения'));
                        break;
                    }

                    $sDescr = $this->input->post('descr', TYPE_TEXT, array('len' => 3000, 'len.sys' => 'orders.offer.descr.limit'));
                    if (!strlen($sDescr)) {
                        $this->errors->set(_t('orders', 'Не указано описание'));
                        break;
                    }

                    $nOfferID = $this->input->post('id', TYPE_UINT);
                    if (!$nOfferID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aOfferData = $this->model->offerData($nOfferID, array('user_id', 'created'));
                    if (empty($aOfferData)) {
                        $this->errors->impossible();
                        break;
                    }

                    if ((BFF_NOW - strtotime($aOfferData['created'])) > $nSecsEditAfterAdd) {
                        $this->errors->set(_t('orders', 'Изменить описание можно только в течении часа'));
                        break;
                    }

                    if ($aOfferData['user_id'] != $nUserID) {
                        $this->errors->impossible();
                        break;
                    }

                    if ($this->errors->no()) {
                        $this->model->offerSave($nOfferID, array('descr' => $sDescr));
                        $aResponse['descr'] = nl2br($sDescr);
                    }
                }
                break;
                default:
                    $this->errors->impossible();
            }
            $this->ajaxResponseForm($aResponse);
        }

        $aData['allow_edit'] = (!$aData['chatcnt'] && (BFF_NOW - strtotime($aData['created'])) < $nSecsEditAfterAdd);

        return $this->viewPHP($aData, 'offer.my.descr');
    }

    /**
     * Cron: Отправляем email-уведомления об изменении статуса заявки к заказу
     * Рекомендуемый интервал: раз в 15 мин
     */
    public function cronOfferStatus()
    {
        if (!bff::cron()) {
            return;
        }

        # интервал задержки отправки уведомления после изменения статуса заявки
        $nSecDelay = (60 * 15); # 15 минут

        $aOffers = $this->model->offerCronStatusData($nSecDelay);
        if (empty($aOffers)) return;

        foreach ($aOffers as $v) {
            # независимо от успешности отправки письма - помечаем дату отправления
            $this->model->offerSave($v['id'], array('enotify_send' => $this->db->now()));
            $template = 'order_offer_change_status';
            if ($v['{status}'] == static::OFFER_STATUS_PERFORMER_START) {
                $template = 'order_offer_performer_start';
                $v['{fio}'] = $v['{name}'].' '.$v['{surname}'];
                if (empty($v['{name}']) && empty($v['{surname}'])) {
                    $v['{fio}'] = $v['{worker_login}'];
                }
                if (empty($v['{name}'])) {
                    $v['{name}'] = $v['{fio}'];
                }
                $v['{client_fio}'] = $v['{client_name}'].' '.$v['{client_surname}'];
                if (empty($v['{client_name}']) && empty($v['{client_surname}'])) {
                    $v['{client_fio}'] = $v['{client_login}'];
                }
            }
            $this->orderOfferMailSend($template, $v);
        }
    }

    /**
     * Отправка почтового уведомления пользователю (при работе с заявками)
     * @param string $template шаблон
     * @param integer $data данные для шаблона
     */
    protected function orderOfferMailSend($template, $data)
    {
        static $mail;
        if( ! $mail){
            $mail = new CMail();
        }

        static $mailTemplates;
        if ( ! isset($mailTemplates[$template])) {
            $mailTemplates[$template] = Sendmail::i()->getMailTemplate($template, array());
        }
        $mailTemplate = $mailTemplates[$template];

        $aStatus = array(
            static::OFFER_STATUS_CANDIDATE => _t('orders-offers', 'Вас выбрали кандидатом.'),
            static::OFFER_STATUS_PERFORMER => _t('orders-offers', 'Вас выбрали исполнителем.'),
            static::OFFER_STATUS_CANCELED  => _t('orders-offers', 'Вы получили отказ.'),
            static::OFFER_STATUS_PERFORMER_START => '', # используется отдельный шаблон 'order_offer_performer_start', в нем нет статуса
        );

        if (isset($aStatus[$data['{status}']])) {
            $data['{status}'] = $aStatus[$data['{status}']];
        }else{
            return;
        }
        $data['{order_url}'] = static::url('view', array('id' => $data['order_id'], 'keyword' => $data['keyword']));
        if(isset($data['{client_login}'])){
            $data['{client_url}'] = Users::url('profile', array('login' => $data['{client_login}']));
        }
        if(isset($data['{worker_login}'])) {
            $data['{worker_url}'] = Users::url('profile', array('login' => $data['{worker_login}']));
        }
        try {
            unset($data['id'], $data['order_id'], $data['keyword']);
            $mail->AltBody = '';
            $mail->Subject = strtr($mailTemplate['subject'], $data);
            $mail->MsgHTML(strtr($mailTemplate['body'], $data));
            $mail->AddAddress($data['{email}']);
            $mail->Send();
            $mail->ClearAddresses();
        } catch (\Exception $e) {
            $this->errors->set('enotify error: ' . $e->getMessage(), true);
            $mail->SmtpClose();
        }
    }

    /**
     * Немедленная отправка почтового уведомления пользователю, подготовка общих данных (при работе с заявками)
     * @param string $template шаблон
     * @param integer $userID id пользователя
     * @param integer $data данные для шаблона
     */
    protected function orderOfferMailSendNow($template, $userID, $data)
    {
        $data['{status}'] = 0;
        $data['{order_title}'] = $data['title'];
        $data['order_id'] = $data['id'];
        $client = Users::model()->userData($data['user_id'], array('name', 'surname', 'login', 'email'));
        foreach($client as $k => $v){
            $data['{client_'.$k.'}'] = $v;
        }
        $worker = Users::model()->userData($data['performer_id'], array('name', 'surname', 'login', 'email'));
        foreach($worker as $k => $v){
            $data['{worker_'.$k.'}'] = $v;
        }
        switch($template){
            case 'order_offer_performer_stop':
                $data['{fio}'] = $worker['name'].' '.$worker['surname'];
                if (empty($worker['name']) && empty($worker['surname'])) {
                    $data['{fio}'] = $worker['login'];
                }
                $data['{name}'] = ! empty($worker['name']) ? $worker['name'] : $data['{fio}'];
                $data['{client_fio}'] = $client['name'].' '.$client['surname'];
                if (empty($client['name']) && empty($client['surname'])) {
                    $data['{client_fio}'] = $client['login'];
                }
                break;
            case 'order_offer_performer_agree':
            case 'order_offer_performer_decline':
                $data['{fio}'] = $client['name'].' '.$client['surname'];
                if (empty($client['name']) && empty($client['surname'])) {
                    $data['{fio}'] = $client['login'];
                }
                $data['{name}'] = ! empty($client['name']) ? $client['name'] : $data['{fio}'];
                $data['{worker_fio}'] = $worker['name'].' '.$worker['surname'];
                if (empty($worker['name']) && empty($worker['surname'])) {
                    $data['{worker_fio}'] = $worker['login'];
                }
                break;
        }

        if ($userID == $data['user_id']) {
            $data['{email}'] = $data['{client_email}'];
        } else {
            $data['{email}'] = $data['{worker_email}'];
        }
        $this->orderOfferMailSend($template, $data);
    }

    /**
     * Форма добавления отклика
     */
    public function simple_offer_add()
    {
        if (Request::isAJAX()) {
            $nUserID = User::id();
            $aResponse = array();
            switch ($this->input->getpost('act', TYPE_STR)) {
                case 'add': {
                    if (!$this->security->validateToken(false, true)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $aData = array();
                    $aParam = array(
                        'order_id' => TYPE_UINT, # id заказа
                    );

                    $this->input->postm($aParam, $aData);

                    if (!$aData['order_id']) {
                        $this->errors->set(_t('orders', 'Не указан заказ'));
                        return false;
                    }

                    $aOrderData = $this->model->orderData($aData['order_id'], [
                                                                                'id',
                                                                                'type',
                                                                                'status',
                                                                                'pro',
                                                                                'keyword',
                                                                                'title',
                                                                                'user_id',
                                                                                'visibility',
                                                                                'fairplay'
                                                                            ]);
                    if (empty($aOrderData)) {
                        $this->errors->set(_t('orders', 'Неверный заказ'));

                        return false;
                    }
                    if ($aOrderData['status'] != static::STATUS_OPENED) {
                        $this->errors->set(_t('orders', 'К этому заказу нельзя добавлять предложения'));
                        return false;
                    }

                    if ($this->errors->no('orders.offer.add.step1', array('id' => 0, 'data' => &$aData))){
                        $nOrderID = $aData['order_id'];

                    }

                    $aOrderData = $this->model->orderData($nOrderID, array());

                    $invite = false;
                    if (static::invitesEnabled()) {
                        $invite = $this->model->invitesDataByFilter(array(
                            'user_id' => $nUserID,
                            'order_id' => $nOrderID,
                        ));
                        if ($aOrderData['visibility'] == static::VISIBILITY_PRIVATE) {
                            if (!$invite) {
                                $this->errors->reloadPage();
                            }
                        }
                    }
                    if (bff::fairplayEnabled()) {
                        if ($aOrderData['fairplay'] == static::FAIRPLAY_USE) {
                            $aData['fairplay'] = static::FAIRPLAY_USE;
                        }
                    }

                    if (!$invite) {
                        $aOrderData['order_pro'] = $aOrderData['pro'];
                        if (!$this->isOfferAddAllow($aOrderData)) {
                            $this->errors->set(_t('orders', 'Вы не можете добавлять предложения.'));
                            break;
                        }
                    }

                    $offers = $this->model->offersDataByFilter(array(
                        'user_id' => $nUserID,
                        'order_id' => $nOrderID,
                    ));
                    if ($offers) {
                        $this->errors->set(_t('orders', 'Вы не можете добавить больше одного предложения.'));
                        break;
                    }

                    $aData['user_id'] = $nUserID;
                    $aData['user_ip'] = Request::remoteAddress();
                    $aData['is_new'] = 1;

                    # доверенный пользователь: без модерации
                    if ($trusted = User::isTrusted($this->module_name)) {
                        $aData['moderated'] = 1;
                    }
                    if ($invite) {
                        $aData['client_only'] = 1;
                        $aData['from_invite'] = 1;
                    }

                    if (!$this->errors->no('orders.offer.add.step2', array('id' => 0, 'data' => &$aData))) {
                        break;
                    }

                    $nOfferID = $this->model->offerSave(0, $aData);
                    if ($nOfferID) {
                        $this->model->orderSave($nOrderID, array('offers_cnt = offers_cnt + 1'));

                        $aResponse['reload'] = 1;

                        # Обновляем счетчик заявок "на модерации"
                        if (!$trusted) {
                            $this->moderationOffersCounterUpdate(1);
                        }

                        # пересчитаем счетчик новых заказов пользователя
                        $this->model->calcUserCounters($aOrderData['user_id']);

                        $aData['descr'] = _t('offer', Orders::OFFER_TEMPLATE);

                        if ($invite) {
                            $this->model->inviteDelete(array(
                                'user_id' => $nUserID,
                                'order_id' => $nOrderID,
                            ));
                            $this->model->calcUserCounters($nUserID);

                            # Отправим уведомление заказчику
                            $aUserData = User::data(array('email', 'name', 'surname', 'login'));
                            $client = Users::model()->userDataEnotify($aOrderData['user_id']);
                            $fio = array();
                            if (!empty($aUserData['name'])) $fio[] = $aUserData['name'];
                            if (!empty($aUserData['surname'])) $fio[] = $aUserData['surname'];
                            $aUserData['fio'] = (!empty($fio) ? join(' ', $fio) : $aUserData['login']);

                            Users::sendMailTemplateToUser($client, 'order_invite_agree', array(
                                'client_url' => Users::url('profile', array('login' => $client['login'])),
                                'worker_name' => $aUserData['name'],
                                'worker_fio' => $aUserData['fio'],
                                'worker_login' => $aUserData['login'],
                                'worker_url' => Users::url('profile', array('login' => $aUserData['login'])),
                                'order_title' => $aOrderData['title'],
                                'order_url' => static::url('view', array('id' => $nOrderID, 'keyword' => $aOrderData['keyword'])),
                                'message' => $aData['descr'],
                            ));
                        } else {

                            # Отправим уведомление о новой заявке заказчику
                            $aUserData = User::data(array('email', 'name', 'surname', 'login'));
                            $fio = array();
                            if (!empty($aUserData['name'])) $fio[] = $aUserData['name'];
                            if (!empty($aUserData['surname'])) $fio[] = $aUserData['surname'];
                            $aUserData['fio'] = (!empty($fio) ? join(' ', $fio) : $aUserData['login']);
                            Users::sendMailTemplateToUser($aOrderData['user_id'], 'order_offer_add', array(
                                'worker_name' => $aUserData['name'],
                                'worker_fio' => $aUserData['fio'],
                                'worker_login' => $aUserData['login'],
                                'worker_url' => Users::url('profile', array('login' => $aUserData['login'])),
                                'order_title' => $aOrderData['title'],
                                'order_url' => static::url('view', array('id' => $nOrderID, 'keyword' => $aOrderData['keyword'])),
                                'offer_descr' => $aData['descr'],
                            ));
                        }
                    } else {
                        $this->errors->reloadPage();
                        break;
                    }
                    $aResponse['success'] = $this->errors->no();
                }
                break;
                default:
                    $this->errors->reloadPage();
            }
            $this->ajaxResponseForm($aResponse);
        }
    }

    /**
     * Cron: Закрываем заказы с назначенным исполнителем через X дней
     * Рекомендуемый интервал: раз в сутки в 00:00:00
     */
    public function cronOrdersClose()
    {
        if (!bff::cron()) {
            return;
        }

        $this->model->ordersCronClose(3);
    }

    /**
     * Cron: Рассылка email-уведомлений исполнителям о новых заказах (раз в сутки или каждые [N] часов)
     * Рекомендуемый интервал: раз в 5 минут
     */
    public function cronNewOrders()
    {
        if (!bff::cron()) {
            return;
        }

        # ID пользователя на котором остановились в прошлой итерации. 0 - рассылка только начинается
        $nUserIDLast = (int)config::get('orders.enotify.neworders.last-user-id', 0);
        switch (config::get('orders_enotify_neworders_type'))
        {
            case static::ENOTIFY_NEWORDERS_DAY: # ежедневная
            {
                $tEnded = (int)config::get('orders.enotify.neworders.ended', 0);
                $bComplete = false;
                if ($nUserIDLast) { # продолжение рассылки
                    $bComplete = $this->model->processEnotifyNewOrderGroup($nCount);
                    if (BFF_DEBUG) {
                        bff::log('Enotify new orders (day) continue: ' . $nCount, 'cron.log');
                    }
                } else {
                    if ($tEnded) {
                        if (date('Ymd') == date('Ymd', $tEnded)) { # сегодня уже рассылали
                            return;
                        }
                    }

                    $begin = strval(config::get('orders_enotify_neworders_begin', '06:00'));
                    if (!empty($begin)) {
                        $begin = strtotime($begin);
                        if ($begin !== false) {
                            if (time() > $begin) {
                                # начало новой рассылки
                                $bComplete = $this->model->processEnotifyNewOrderGroup($nCount);
                                if (BFF_DEBUG) {
                                    bff::log('Enotify new orders (day) start: ' . $nCount, 'cron.log');
                                }
                                config::save('orders.enotify.neworders.begin', time(), true);
                            }
                        }
                    }
                }
                if ($bComplete) {
                    config::save('orders.enotify.neworders.ended', time(), true);
                    if (BFF_DEBUG) {
                        bff::log('Enotify new orders (day) stop', 'cron.log');
                    }
                }
            } break;
            case static::ENOTIFY_NEWORDERS_HOURS: # каждые [N] часов
            {
                $bComplete = false;
                if ($nUserIDLast) { # продолжение рассылки
                    $bComplete = $this->model->processEnotifyNewOrderGroup($nCount);
                    if (BFF_DEBUG) {
                        bff::log('Enotify new orders (hours) continue: ' . $nCount, 'cron.log');
                    }
                } else {
                    $interval = intval(config::get('orders_enotify_neworders_interval', 3)) * 3600; # интервал между рассылками в секундах
                    $tPrev = intval(config::get('orders.enotify.neworders.begin', 0)); # время начала предыдущей рассылки
                    $tNext = false;
                    if ($tPrev) {
                        if (date('Ymd') == date('Ymd', $tPrev)) {
                            # сегодня рассылки были
                            $tNext = $tPrev + $interval; # время начала следующей рассылки
                            if (date('Ymd') != date('Ymd', $tNext)) { # сегодня больше рассылки не делаем
                                return;
                            }
                        }
                    }
                    if ( ! $tNext) {
                        $tNext = strtotime(date('Ymd')) + $interval;
                    }

                    if (time() > $tNext) {
                        # старт новой рассылки
                        $bComplete = $this->model->processEnotifyNewOrderGroup($nCount);
                        if (BFF_DEBUG) {
                            bff::log('Enotify new orders (hours) start: ' . $nCount, 'cron.log');
                        }
                        config::save('orders.enotify.neworders.begin', time(), true);
                    }
                }
                if ($bComplete) {
                    config::save('orders.enotify.neworders.ended', time(), true);
                    if (BFF_DEBUG) {
                        bff::log('Enotify new orders (hours) stop', 'cron.log');
                    }
                }
            } break;
        }
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {

        return array(
            'cronOfferStatus' => array('period' => '*/15 * * * *'),
            'cronOrdersClose' => array('period' => '0 0 * * *'),
            'cronNewOrders' => array('period' => '*/5 * * * *'),
        );
    }

    /**
     * Данные для страницы "Карта сайта"
     * @return array
     */
    public function getSitemapData()
    {
        $aData = array(
            'services' => array(),
        );
        # Список категорий специализаций + специализаций
        $aSpecs = Specializations::model()->specializationsInAllCategories(
            array('id', 'keyword', 'title'), array('id', 'keyword', 'title'), true
        );
        $urlKey = static::searchOneSpec() ? 'list.single' : 'list.multi';
        if ($aSpecs) {
            $catsOn = Specializations::catsOn();
            foreach ($aSpecs as $k=>&$v) {
                if ($catsOn) {
                    if (empty($v['specs'])) {
                        unset($aSpecs[$k]); continue;
                    }
                    foreach ($v['specs'] as &$vv) {
                        if ($vv['cnt'] > 1) {
                            $vv['keyword'] = $vv['keyword'].'-'.$v['keyword'];
                        }
                        $vv['url'] = static::url($urlKey, array('keyword'=>$vv['keyword']));
                    } unset($vv);
                } else {
                    # категория без специализаций
                    $v['url'] = static::url($urlKey, array('keyword' => $v['keyword']));
                }
            } unset($v);
            $aSpecs = array_map(function($i) {
                if (isset($i['specs'])) {
                    $i['sub'] = $i['specs'];
                }
                unset($i['specs']);
                return $i;
            }, $aSpecs);
            $aData['services']['cats'] = $aSpecs;
            $aData['services']['link'] = static::url('list');
        }

        if (static::useProducts()) {
            # Список категорий товаров
            $aCats = Shop::model()->categoriesListAll(array('id', 'keyword', 'title'));
            if ($aCats) {
                foreach ($aCats as &$v) {
                    if( ! empty($v['sub'])){
                        foreach ($v['sub'] as &$vv) {
                            $vv['url'] = static::url($urlKey, array('keyword'=>$vv['keyword'], 't'=>static::TYPE_PRODUCT));
                        } unset($vv);
                    }else {
                        $v['url'] = static::url($urlKey, array('keyword'=>$v['keyword'], 't'=>static::TYPE_PRODUCT));
                    }
                } unset($v);
            }
            $aData['products'] = array(
                'cats' => $aCats,
                'link' => static::url('list', array('t'=>static::TYPE_PRODUCT)),
            );
        }

        return $aData;
    }

    /**
     * RSS Лента
     */
    function rss()
    {
        # Параметры ленты:
        $g = $this->input->getm(array(
            't'      => TYPE_UINT, # ID тип заказов (static::TYPE_)
            'cat_id' => TYPE_UINT, # ID Категории
            'sub_id' => TYPE_UINT, # ID Подкатегории / специализации
        )); extract($g);
        if ( ! in_array($t, array(static::TYPE_SERVICE, static::TYPE_PRODUCT))) {
            $t = static::TYPE_SERVICE;
        }

        # Формируем + кешируем
        $cache = Cache::singleton($this->module_name, 'file', array('lifeTime'=>(15*60))); # кешируем на Х минут
        $cacheKey = 'rss-'.$t.'-'.$cat_id.'-'.$sub_id.'-'.LNG;
        if (($data = $cache->get($cacheKey)) === false) {
            $data = $this->generateRss($t, $cat_id, $sub_id);
            $cache->set($cacheKey, $data);
        }

        header('Content-Type: application/rss+xml; charset=UTF-8');
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

        echo $data;
        bff::shutdown();
    }

    /**
     * Формируем RSS ленту
     * @param integer $typeID тип заказов (static::TYPE_)
     * @param integer $categoryID ID Категории
     * @param integer $subID ID Подкатегории / специализации
     * @param string $lng Ключ языка
     * @return string
     */
    protected function generateRss($typeID, $categoryID, $subID)
    {
        $aFilter = array('status' => array(static::STATUS_OPENED), 'exclude-svc-hidden'=>1);
        if (static::searchClosed()) {
            $aFilter['status'][] = static::STATUS_CLOSED;
        }

        # Получаем данные о категориях / специализациях
        if ($typeID == static::TYPE_PRODUCT && static::useProducts()) {
            $aCats = Shop::model()->categoriesListAll(array('id', 'keyword', 'title'));
            $aFilter['SelectCat'] = 1;
            if ($subID) {
                $aFilter['cat_id'] = array($subID);
            } else {
                if ($categoryID) {
                    if ( ! empty($aCats[$categoryID]['sub'])) {
                        $aFilter['cat_id'] = array();
                        foreach ($aCats[$categoryID]['sub'] as $v) {
                            $aFilter['cat_id'][] = $v['id'];
                        }
                    } else {
                        $aFilter['cat_id'] = array($categoryID);
                    }
                }
            }
        } else {
            $aSpecs = Specializations::model()->specializationsInAllCategories(
                array('id', 'keyword', 'title'), array('id', 'keyword', 'title'), true
            );
            $aFilter['SelectSpec'] = 1;
            $bCatON = Specializations::catsOn();
            if ($bCatON) {
                if ($subID) {
                    $aFilter['spec_id'] = array($subID);
                } else if ($categoryID) {
                    if ( ! empty($aSpecs[ $categoryID ])) {
                        $aFilter['spec_id'] = array();
                        foreach ($aSpecs[ $categoryID ]['specs'] as $v) {
                            $aFilter['spec_id'][] = $v['id'];
                        }
                    }
                }
            } else {
                if ($categoryID) {
                    $aFilter['spec_id'] = array($categoryID);
                }
            }
        }

        $aItems = array();
        $aData = $this->model->ordersList($aFilter, false, ' LIMIT 30 ', 'svc_fixed DESC, O.svc_fixed_order DESC, O.svc_order DESC');
        if ( ! empty($aData)) {
            foreach ($aData as $v) {
                $sCatTitle = '';
                if ($typeID == static::TYPE_PRODUCT) {
                    $v['cats1'] = (int)$v['cats1'];
                    $v['cats2'] = (int)$v['cats2'];
                    if ( ! empty($aCats[ $v['cats1'] ]['title'])){
                        $sCatTitle = $aCats[ $v['cats1'] ]['title'];
                    }
                    if ( ! empty($aCats[ $v['cats1'] ]['sub'][ $v['cats2'] ]['title'])) {
                        $sCatTitle .= ' / '.$aCats[ $v['cats1'] ]['sub'][ $v['cats2'] ]['title'];
                    }
                } else {
                    $v['specs'] = (int)$v['specs'];
                    if ($bCatON) {
                        $v['cats'] = (int)$v['cats'];
                        if ( ! empty($aSpecs[ $v['cats'] ]['specs'][$v['specs'] ])) {
                            $sCatTitle = $aSpecs[ $v['cats'] ]['title'] .' / '.$aSpecs[ $v['cats'] ]['specs'][$v['specs'] ]['title'];
                        }
                    } else {
                        if ( ! empty($aSpecs[ $v['specs'] ])) {
                            $sCatTitle = $aSpecs[ $v['specs'] ]['title'];
                        }
                    }

                }
                $url = static::url('view', array('id' => $v['id'], 'keyword' => $v['keyword']));
                $aItems[] = '<item>
                <title>'.strip_tags($v['title']).'</title>
                <link>'.$url.'</link>
                <description>'.htmlspecialchars($v['descr']).'</description>
                <guid>'.$url.'</guid>
                <category>'.strip_tags($sCatTitle).'</category>
                <pubDate>'.date('D, j M Y G:i:s O', strtotime($v['created'])).'</pubDate>
            </item>';
            }
        }

        $sTitle = _t('', 'Заказы на [site]', array('site' => Site::title('orders.rss')));
        $sCatTitle = '';
        if ($typeID == static::TYPE_PRODUCT) {
            if ( ! empty($aCats[ $categoryID ]['title'])) {
                $sCatTitle = $aCats[ $categoryID ]['title'];
            }
            if ( ! empty($aCats[ $categoryID ]['sub'][ $subID ]['title'])) {
                $sCatTitle .= ' / '.$aCats[ $categoryID ]['sub'][ $subID ]['title'];
            }
        } else {
            if ($bCatON) {
                if ( ! empty($aSpecs[ $categoryID ])) {
                    $sCatTitle = $aSpecs[ $categoryID ]['title'];
                }
                if ( ! empty($aSpecs[ $categoryID ]['specs'][$subID])) {
                    $sCatTitle .= ' / '.$aSpecs[ $categoryID ]['specs'][$subID]['title'];
                }
            } else {
                if ( ! empty($aSpecs[ $categoryID ])) {
                    $sCatTitle = $aSpecs[ $categoryID ]['title'];
                }
            }
        }
        if ($sCatTitle) {
            $sCatTitle = ' ('.$sCatTitle.')';
        }
        $sTitle .= $sCatTitle;

        $lang = $this->locale->getLanguageSettings(LNG, 'locale');
        $lang = mb_strtolower($lang);
        $lang = str_replace('_', '-', $lang);

        $sDescription = '<title>'.$sTitle.'</title>
                       <link>'.SITEURL.'</link>
                       <description>'.$sTitle.'</description>
                       <language>'.$lang.'</language>
                       <pubDate>'.date('D, j M Y G:i:s O').'</pubDate>
                       <lastBuildDate>'.date('D, j M Y G:i:s O').'</lastBuildDate>
                       <image>
                        <url>'.Site::logoURL('orders.rss').'</url>
                        <title>'.$sTitle.'</title>
                        <link>'.SITEURL.'</link>
                       </image>';

        return strtr('<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"><channel>{description}{items}</channel></rss>',
            array(
                '{description}'=>$sDescription,
                '{items}'=>join('', $aItems)
            )
        );
    }

    # ------------------------------------------------------------------------------------------------------------------------------
    # Услуги

    /**
     * Продвижение заказа
     */
    public function promote()
    {
        if( ! bff::servicesEnabled()){
            $this->errors->error404();
        }

        $this->security->setTokenPrefix('orders-order-promote');

        $nOrderID = $this->input->get('id', TYPE_UINT);
        if ( ! $nOrderID) {
            $this->errors->error404();
        }
        if (Request::isAJAX()) {
            do {
                $aResponse = array();

                if (!$this->security->validateToken()) {
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->input->postm(array(
                    'svc'           => TYPE_ARRAY_UINT,
                    'ps'            => TYPE_NOTAGS,
                    'fixed_days'    => TYPE_UINT,
                ));
                extract($aData);

                $aOrderData = $this->model->orderData($nOrderID, array('status', 'svc'));
                $cityID = 0;
                $aOrderRegions = $this->model->orderRegions($nOrderID);
                if (!empty($aOrderRegions)) {
                    $aOrderRegions = reset($aOrderRegions);
                    $cityID = $aOrderRegions['reg3_city'];
                }
                $nUserID = User::id();
                if( ! $nUserID){
                    break;
                }
                if (empty($aOrderData)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ($aOrderData['status'] != static::STATUS_OPENED) {
                    $this->errors->reloadPage();
                    break;
                }

                $aSvcSettings = array(
                    'module' => $this->module_name,
                    'id'     => $nOrderID,
                    'svc'    => array(),
                );
                if ( ! empty($fixed_days)) {
                    $aSvcSettings['fixed_days'] = $fixed_days;
                }
                $nSvcKeys = 0;
                $nSum = 0;
                foreach($svc as $v){
                    $aSvc = Svc::model()->svcData($v);
                    if(empty($aSvc)) continue;
                    if( ! $aSvc['on']) continue;
                    if($aSvc['module'] != $this->module_name) continue;
                    if($cityID){
                        $price = $this->model->svcPricesEx(array($v), $cityID);
                        if ( ! empty($price[ $v ]) && $price[ $v ] > 0) {
                            $aSvc['price'] = $price[ $v ];
                        }
                    }
                    $sum = $aSvc['price'];
                    if ($v == static::SVC_FIX && ! empty($aSvc['per_day'])) {
                        if (empty($fixed_days)) {
                            $fixed_days = 1;
                        }
                        $sum = round($sum * $fixed_days, 2);
                    }
                    $nSum += $sum;
                    $nSvcKeys += $v;
                    $aSvcSettings['svc'][] = $v;
                }
                if (!$nSum) {
                    break;
                }
                $aSvcSettings['price'] = $nSum;
                if ($ps == 'balance') {
                    $nBalance = User::balance();
                    if ($nBalance >= $nSum) {
                        Svc::i()->activate($this->module_name, $nSvcKeys, array(), $nOrderID, $nUserID, $nSum, false, $aSvcSettings);
                    } else {
                        $this->errors->set(_t('svc', 'Недостаточно средств. Пополните счет или выберите другой способ оплаты.'));
                        break;
                    }
                } else {
                    $aResponse['form'] = Bills::i()->createBill($ps, $nSum, $aSvcSettings, $nSvcKeys, $nOrderID);
                }
                if ($this->input->post('status', TYPE_UINT)) {
                    $aResponse['redirect'] = static::url('status', array('id'=>$nOrderID));
                }

            } while (false);
            $this->ajaxResponseForm($aResponse);
        }

        $aData = $this->model->ordersList(array('id'=>$nOrderID, 'status'=>static::STATUS_OPENED, ':moderated' => 'O.moderated < 3'));
        if (empty($aData)) {
            $this->redirect(static::url('status', array('id' => $nOrderID)));
        }
        $aData = reset($aData);
        $aData['url_view'] = static::url('view', array('id'=>$nOrderID, 'keyword'=>$aData['keyword']));

        $aData['bPromote'] = 1;
        $aData['list'] = array($aData);
        $aData['list'] = $this->viewPHP($aData, 'search.list');

        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => $aData['title'], 'link' => static::url('view', array('id' => $aData['id'], 'keyword' => $aData['keyword']))),
            array('title' => _t('orders', 'Продвижение заказа'), 'active' => true),
        );

        $aData['svc'] =  Svc::model()->svcListing(Svc::TYPE_SERVICE, $this->module_name);

        # определяем необходимость поднятия для заказа
        if ($aData['svc_fixed']) {
            $sOrder = 'O.svc_fixed_order DESC';
        } else {
            $sOrder = 'O.svc_order DESC';
        }
        $aDataOrder = $this->model->ordersList(array('status'=>static::STATUS_OPENED), false, $this->db->prepareLimit(0, 1), $sOrder);
        $aDataOrder = reset($aDataOrder);
        if ($aDataOrder['id'] == $aData['id']) {
            # в поднятии нет необходимости
            unset($aData['svc']['orders_up']);
        }

        if (!empty($aData['reg3_city'])) {
            $ids = array();
            foreach($aData['svc'] as $v) $ids[] = $v['id'];
            $aSvcPrices = $this->model->svcPricesEx($ids, $aData['reg3_city']);
            foreach ($aData['svc'] as & $v) {
                if (!empty($aSvcPrices[ $v['id'] ]) && $aSvcPrices[ $v['id'] ] > 0) {
                    $v['price'] = $aSvcPrices[ $v['id'] ];
                }
            } unset($v);
        }

        $aData['preset'] = $this->input->get('preset', TYPE_UINT);
        $aData['svc_fixed_days'] = $this->input->get('svc_fixed_days', TYPE_UINT);
        $aData['bStatus'] = $this->input->get('status', TYPE_UINT);

        bff::setMeta(_t('orders', 'Продвижение заказа'));
        return $this->viewPHP($aData, 'promote');
    }

    /**
     * Форма управления заметкой о заказе (просмотр / добавление / редактирование)
     * @param integer $nOrderID ID заказа, о котором размещается заметка
     * @param array $aParams параметры
     * @return string
     */
    public function getOrderNote($nOrderID, $aParams = array())
    {
        $nAuthorID = User::id();
        if (!$nAuthorID || !$nOrderID) {
            return '';
        }

        $aData = $this->model->noteData($nAuthorID, $nOrderID);
        if (empty($aData['order_id'])) {
            $aData['order_id'] = $nOrderID;
        }
        $aData += $aParams;

        return $this->viewPHP($aData, 'my.note');
    }

    public function noteBlock($aData)
    {
        return $this->viewPHP($aData, 'my.note.block');
    }

}