<?php

defined('TABLE_ORDERS') or define('TABLE_ORDERS', DB_PREFIX.'orders'); # заказы
define('TABLE_ORDERS_SPECS',           DB_PREFIX.'orders_specs'); # заказы + специализации
define('TABLE_ORDERS_CATS',            DB_PREFIX.'orders_cats'); # заказы + категории
define('TABLE_ORDERS_REGIONS',         DB_PREFIX.'orders_regions'); # заказы + регионы
define('TABLE_ORDERS_IMAGES',          DB_PREFIX.'orders_images'); # изображения заказов
define('TABLE_ORDERS_IN_TAGS',         DB_PREFIX.'orders_in_tags'); # заказы + навыки
define('TABLE_ORDERS_ATTACHMENTS',     DB_PREFIX.'orders_attachments'); # файлы (вложения) заказов
define('TABLE_ORDERS_SVC_PRICE',       DB_PREFIX.'orders_svc_price'); # региональная стоимость платных услуг заказов
define('TABLE_ORDERS_INVITES',         DB_PREFIX.'orders_invites'); # предложения заказов исполнителям

define('TABLE_ORDERS_OFFERS',          DB_PREFIX.'orders_offers'); # заявки
define('TABLE_ORDERS_OFFERS_EXAMPLES', DB_PREFIX.'orders_offers_examples'); # примеры заявок
define('TABLE_ORDERS_OFFERS_IMAGES',   DB_PREFIX.'orders_offers_images'); # изображения заявок
define('TABLE_ORDERS_OFFERS_CHAT',     DB_PREFIX.'orders_offers_chat'); # общение в заявке

class OrdersModel_ extends Model
{
    /** @var OrdersBase */
    protected $controller;

    public $langSvcServices = array(
        'title_view'       => TYPE_NOTAGS, # название
        'description'      => TYPE_STR,    # описание (краткое)
        'description_full' => TYPE_STR,    # описание (подробное)
    );

    public $langSvcPacks = array(
        'title_view'       => TYPE_NOTAGS, # название
        'description'      => TYPE_STR,    # описание (краткое)
        'description_full' => TYPE_STR,    # описание (подробное)
    );

    public function init()
    {
        parent::init();
    }

    /**
     * Список заказов
     * @param array $aFilter фильтр списка заказов
     * @param bool $bCount только подсчет кол-ва заказов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function ordersListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '') //admin
    {
        $sFrom = TABLE_ORDERS . ' O ';
        $aGroupBy = array();

        if (array_key_exists(':region', $aFilter) || array_key_exists(':country', $aFilter)) {
            $sFrom .= ', ' . TABLE_ORDERS_REGIONS . ' R ';
            $aFilter[':joinregion'] = ' O.id = R.order_id ';
            $aGroupBy[] = 'O.id';
        }

        if (array_key_exists(':cats', $aFilter)) {
            $sFrom .= ', ' . TABLE_ORDERS_CATS . ' C ';
            $aFilter[':joincats'] = ' O.id = C.order_id ';
            $aGroupBy[] = 'O.id';
        }

        if (array_key_exists(':specs', $aFilter)) {
            $sFrom .= ', ' . TABLE_ORDERS_SPECS . ' S ';
            $aFilter[':joinspecs'] = ' O.id = S.order_id ';
            $aGroupBy[] = 'O.id';
        }

        if (array_key_exists(':user', $aFilter)) {
            $sFrom .= ', ' . TABLE_USERS . ' U ';
            $aFilter[':ujoin'] = ' O.user_id = U.user_id ';
        }

        $aFilter = $this->prepareFilter($aFilter, 'O');
        $aGroupBy = (!empty($aGroupBy) ? join(',', array_unique($aGroupBy)) : '');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(O.id) FROM ' . $sFrom . $aFilter['where'] . (!empty($aGroupBy) ? ' GROUP BY ' . $aGroupBy : ''), $aFilter['bind']);
        }

        return $this->db->select('SELECT O.id, O.keyword, O.created, O.title, O.enabled, O.imgcnt, O.status, O.moderated, O.visibility, O.fairplay
               FROM ' . $sFrom . $aFilter['where']
            . (!empty($aGroupBy) ? ' GROUP BY ' . $aGroupBy : '')
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']
        );
    }

    /**
     * Список заявок
     * @param array $aFilter фильтр списка заказов
     * @param bool $bCount только подсчет кол-ва заказов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function offersListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '') //admin
    {
        $sFrom = TABLE_ORDERS_OFFERS . ' O, ' . TABLE_USERS . ' U, '.TABLE_ORDERS.' R ';
        $aFilter[':ujoin'] = ' O.user_id = U.user_id AND O.order_id = R.id';

        $aFilter = $this->prepareFilter($aFilter, 'O');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(O.id) FROM ' . $sFrom . $aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('
            SELECT O.id, O.created, O.descr, O.moderated, O.order_id, O.user_id, O.status, R.keyword, U.name, U.email, U.blocked AS ublocked, U.login
               FROM ' . $sFrom . $aFilter['where']
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']
        );
    }

    /**
     * Список заказов. Поиск на главной
     * @param array $aFilter фильтр списка заказов
     * @param bool $bCount только подсчет кол-ва заказов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function ordersList(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        $sFrom = '';
        $bSelectSpec = false;
        if (array_key_exists('spec_id', $aFilter)) {
            $sFrom .= ', ' . TABLE_ORDERS_SPECS . ' S ';
            $aFilter[':jspec'] = 'O.id = S.order_id ';
            $aFilter[':spec_id'] = $this->db->prepareIN('S.spec_id', $aFilter['spec_id']);
            unset($aFilter['spec_id']);
        }
        if( ! empty($aFilter['SelectSpec'])){
            $bSelectSpec = true;
            if( ! isset($aFilter[':jspec'])){
                $sFrom .= ', ' . TABLE_ORDERS_SPECS . ' S ';
                $aFilter[':jspec'] = 'O.id = S.order_id ';
            }
        }

        $bSelectCat = false;
        if (array_key_exists('cat_id', $aFilter)) {
            $sFrom .= ', ' . TABLE_ORDERS_CATS . ' C ';
            $aFilter[':jcat'] = 'O.id = C.order_id ';
            $aFilter[':cat_id'] = $this->db->prepareIN('C.cat_id', $aFilter['cat_id']);
            unset($aFilter['cat_id']);
        }
        if( ! empty($aFilter['SelectCat'])){
            $bSelectCat = true;
            if( ! isset($aFilter[':jcat'])){
                $sFrom .= ', ' . TABLE_ORDERS_CATS . ' C ';
                $aFilter[':jcat'] = 'O.id = C.order_id ';
            }
        }

        if (array_key_exists('userSpecs', $aFilter)) {
            $sFrom .= ', ' . TABLE_USERS_SPECIALIZATIONS . ' US ';
            $aFilter[':jus'] = array('US.spec_id = S.spec_id AND US.user_id = :ususer', ':ususer' => $aFilter['userSpecs']);
            if( ! isset($aFilter[':jspec'])){
                $sFrom .= ', ' . TABLE_ORDERS_SPECS . ' S ';
                $aFilter[':jspec'] = 'O.id = S.order_id ';
            }
            unset($aFilter['userSpecs']);
        }

        unset($aFilter['SelectSpec'], $aFilter['SelectCat']);

        # Фильтр по специализациям пользователя
        if (array_key_exists('userSpecs', $aFilter)) {
            $sFrom .= ', ' . TABLE_USERS_SPECIALIZATIONS . ' US ';
            $aFilter[':jus'] = array('US.spec_id = S.spec_id AND US.user_id = :ususer', ':ususer' => $aFilter['userSpecs']);
            if ( ! isset($aFilter[':jspec'])) {
                $sFrom .= ', ' . TABLE_ORDERS_SPECS . ' S ';
                $aFilter[':jspec'] = 'O.id = S.order_id ';
            }
            unset($aFilter['userSpecs']);
        }

        # фильтр по региону (если есть)
        $bLjRegion = true;
        foreach (array('reg1_country', 'reg2_region', 'reg3_city') as $v) {
            if (!isset($aFilter[$v])) {
                continue;
            }
            if (!isset($aFilter[':jregions'])) {
                $sFrom .= ', ' . TABLE_ORDERS_REGIONS . ' R ';
                $aFilter[':jregions'] = 'O.id = R.order_id ';
                $bLjRegion = false;
            }
            $aFilter[':' . $v] = array('R.' . $v . ' = :v' . $v, ':v' . $v => $aFilter[$v]);
            unset($aFilter[$v]);
        }

        $bTagTT = false;
        # поиск:
        if (isset($aFilter['text'])) {
            $sText = trim($aFilter['text']);
            if ( ! empty($sText)) {
                # поиск по заголовку, описанию
                $aFilter[':text'] = '( '.$this->db->prepareFulltextQuery($sText, 'O.title, O.descr');
                # поиск по тегу (при совпадении с названием тега)
                $aTagIDs = $this->db->select_one_column('SELECT id FROM '.TABLE_TAGS.' WHERE tag LIKE :tag', array(':tag' => $sText));
                if( ! empty($aTagIDs)){
                    $aFilter[':text'] .= ' OR '.$this->db->prepareIN('TT.tag_id', $aTagIDs);
                    $bTagTT = true;
                }
                # поиск по специализации (при совпадении с названием специализации)
                $nSpecID = $this->db->one_data('SELECT id FROM '.TABLE_SPECIALIZATIONS_LANG.' WHERE title = :title', array(':title' => $sText));
                if($nSpecID){
                    $sFrom = ' LEFT JOIN ' . TABLE_ORDERS_SPECS . ' LjS ON LjS.order_id = O.id AND LjS.spec_id = '.$nSpecID.' '.$sFrom;
                    $aFilter[':text'] .=  ' OR LjS.order_id IS NOT NULL ';
                }
                $aFilter[':text'] .= ' )';
            }
            unset($aFilter['text']);
        }

        # фильтр по тегу
        if (array_key_exists('tag_id', $aFilter)) {
            $aFilter[':tag_id'] = array('TT.tag_id = :tag', ':tag' => $aFilter['tag_id']);
            $bTagTT = true;
            unset($aFilter['tag_id']);
        }
        if ($bTagTT) {
            $sFrom .= ', ' . TABLE_ORDERS_IN_TAGS . ' TT ';
            $aFilter[':jtags'] = 'O.id = TT.order_id';
        }

        # скрываем заказы с активированной услугой "Скрытый заказ"
        if ( ! User::id() || isset($aFilter['exclude-svc-hidden'])) {
            $aFilter[':hidden'] = '(O.svc & '.Orders::SVC_HIDDEN.') = 0';
            if (isset($aFilter['exclude-svc-hidden'])) {
                unset($aFilter['exclude-svc-hidden']);
            }
        }

        if( ! isset($aFilter[':moderated'])) {
            if (Orders::premoderation()) {
                $aFilter[':moderated'] = 'O.moderated > 0';
            }
        }
        $aFilter['visibility'] = Orders::VISIBILITY_ALL;

        $aFilter = $this->prepareFilter($aFilter, 'O');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(id) FROM
                ( SELECT O.id FROM ' . TABLE_ORDERS . ' O ' . $sFrom . $aFilter['where'] . ' GROUP BY O.id ) sl', $aFilter['bind']);
        }

        $aData = $this->db->select('
            SELECT O.id, O.user_id, O.type, O.service_type, O.status, O.title, O.descr, O.keyword, O.pro, O.fairplay, O.created, O.offers_cnt,
                   O.price, O.price_curr, O.price_ex, O.price_rate_text, O.addr_lat, O.addr_lng, O.views_total, O.expire,
                   ((O.svc & ' . Orders::SVC_MARK . ') > 0) as svc_marked,
                   ((O.svc & ' . Orders::SVC_FIX . ') > 0) as svc_fixed,
                   GROUP_CONCAT(T.tag_id) AS tags, 
                   O.district_id, O.start_date,
                   MAX(R.reg3_city) AS reg3_city
                   '.($bSelectSpec ? ', GROUP_CONCAT(S.spec_id) AS specs, GROUP_CONCAT(S.cat_id) AS cats' : '').'
                   '.($bSelectCat ? ', GROUP_CONCAT(C.cat_id1) AS cats1, GROUP_CONCAT(C.cat_id2) AS cats2 ' : '').'
            FROM ' . TABLE_ORDERS . ' O
                LEFT JOIN ' . TABLE_ORDERS_IN_TAGS . ' T ON O.id = T.order_id ' .
            ($bLjRegion ? ' LEFT JOIN ' . TABLE_ORDERS_REGIONS . ' R ON O.id = R.order_id ' : '') .
            $sFrom . $aFilter['where'] . '
            GROUP BY O.id
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, $aFilter['bind']
        );

        if ( ! empty($aData)) {
            $this->ordersListPrepare($aData);
        } else {
            $aData = array();
        }

        return $aData;
    }

    /**
     * Список заказов пользователя. Пользователь - владелец заказов
     * @param array $aFilter фильтр списка заказов
     * @param bool $bCount только подсчет кол-ва заказов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function ordersListOwner(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        if ( ! User::id()) {
            $aFilter[':hidden'] = ' (O.svc & '.Orders::SVC_HIDDEN.') = 0 ';
        }
        $myList = User::id() == $aFilter['user_id'];
        if ( ! $myList) {
            $aFilter['visibility'] = Orders::VISIBILITY_ALL;
        }

        if (empty($aFilter['user_id']) || !$myList) {
            $aFilter[':status'] = $this->db->prepareIN('O.status', array(Orders::STATUS_OPENED, Orders::STATUS_CLOSED));
            if (Orders::premoderation()) {
                $aFilter[':moderated'] = 'O.moderated > 0';
            }
        }

        # В работе
        $join = '';
        $statusSelect = 'O.status';
        $ordersOpinions = Orders::ordersOpinions();
    
        if ($ordersOpinions){
            $join = ' LEFT JOIN bff_opinions OP ON  (O.id = OP.order_id AND O.user_id = OP.author_id) ';
            $statusSelect = '(CASE WHEN (O.status = '.Orders::STATUS_CLOSED.' AND OP.author_id IS NULL) THEN '.Orders::STATUS_IN_WORK.'
                                   ELSE O.status 
                              END) as status';
        }
        if ( ! empty($aFilter['in_work'])) {
            if ($ordersOpinions) {
                $join = ' JOIN bff_orders_offers F ON (O.id = F.order_id AND O.performer_id = F.user_id)
                          LEFT JOIN '. TABLE_OPINIONS .' OP ON  (O.id = OP.order_id AND O.user_id = OP.author_id) ';
                
                $aFilter[':agree'] = array('F.status = :agree', ':agree' => Orders::OFFER_STATUS_PERFORMER_AGREE);
                $aFilter[':author_id'] = 'OP.author_id IS NULL';
            } else {
                $aFilter[':performer'] = 'O.performer_id > 0';
            }
            unset($aFilter['in_work']);
        }

        $aFilter = $this->prepareFilter($aFilter, 'O');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(O.id) FROM ' . TABLE_ORDERS . ' O ' .$join. $aFilter['where'], $aFilter['bind']);
        }

        $aData = $this->db->select('
            SELECT 
                O.*, 
                O.id, 
                O.user_id, 
                O.type, 
                O.service_type, 
                '.$statusSelect.', 
                O.title, O.descr, O.keyword, O.pro, O.fairplay, O.created, O.offers_cnt,
                   O.price, O.price_curr, O.price_ex, O.price_rate_text, O.moderated, O.blocked_reason, O.visibility, O.removed,
                   ((O.svc & ' . Orders::SVC_MARK . ') > 0) as svc_marked,
                   GROUP_CONCAT(T.tag_id) AS tags, MAX(R.reg3_city) AS reg3_city,
                   O.performer_id, O.views_total, O.district_id,
                   U.name AS performer_name, U.surname AS performer_surname, U.login AS performer_login,
                   U.pro AS performer_pro, U.verified AS performer_verified, U.avatar AS performer_avatar,
                   U.spec_id AS performer_spec_id, U.sex AS performer_sex 
            FROM ' . TABLE_ORDERS . ' O
                LEFT JOIN ' . TABLE_ORDERS_IN_TAGS . ' T ON O.id = T.order_id
                LEFT JOIN ' . TABLE_ORDERS_REGIONS . ' R ON O.id = R.order_id
                LEFT JOIN ' . TABLE_USERS . ' U ON O.performer_id = U.user_id
                '.$join.'
            ' . $aFilter['where'] . '
            GROUP BY O.id
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, $aFilter['bind']
        );

        if ( ! empty($aData)) {
            if ($myList) { # просматривает владелец заказов. Кабинет клиента
                $orders = array(); # список id выбранных для отображения заказов
                $performers = array(); # id заказов с выбраным исполнителем
                foreach($aData as $v){
                    $orders[] = $v['id'];
                    if ($v['performer_id']) {
                        $performers[] = $v['id'];
                    }
                }
                # счетчики новых заявок и сообщений для выбранных заказов
                $offers = $this->db->select_key('
                    SELECT F.order_id, SUM(F.is_new) AS offers_new, SUM(F.chatcnt) AS chat_cnt, SUM(F.chat_new_client) AS chat_new
                    FROM '.TABLE_ORDERS_OFFERS.' F
                    WHERE '.$this->db->prepareIN('order_id', $orders).'
                    GROUP BY F.order_id
                ', 'order_id');
                foreach ($aData as & $v) {
                    if (isset($offers[ $v['id'] ])) {
                        unset($offers[ $v['id'] ]['order_id']);
                        $v += $offers[ $v['id'] ];
                    } else {
                        $v += array('offers_new' => 0, 'chat_cnt' => 0, 'chat_new' => 0);
                    }
                } unset($v);
            }
            $this->ordersListPrepare($aData);
            if (Orders::ordersOpinions() && $myList) {
                if( ! empty($performers)) { # если есть заказы с исполнителями, узнаем статус заказов
                    $performers = $this->db->select_key('
                    SELECT order_id, id AS performer_offer_id, status AS offer_status
                    FROM ' . TABLE_ORDERS_OFFERS . '
                    WHERE ' . $this->db->prepareIN('order_id', $performers) . '
                      AND ' . $this->db->prepareIN('status', array(Orders::OFFER_STATUS_PERFORMER_START, Orders::OFFER_STATUS_PERFORMER_AGREE))
                        , 'order_id');
                }

                # для отклоненных предложений, выводим причину
                $declined = array();
                $tmp = $this->db->select('
                    SELECT F.id, F.order_id, F.descr,
                           U.login, U.name, U.surname, U.pro, U.avatar, U.sex, U.verified,
                           C.id AS chat_id, C.message, C.is_new
                    FROM ' . TABLE_ORDERS_OFFERS . ' F
                        LEFT JOIN '.TABLE_ORDERS_OFFERS_CHAT.' C ON F.chat_id = C.id
                        ,' . TABLE_USERS . ' U
                    WHERE ' . $this->db->prepareIN('F.order_id', $orders) . '
                      AND F.status = '.Orders::OFFER_STATUS_PERFORMER_DECLINE.'
                      AND F.user_id = U.user_id
                    ORDER BY F.status_created DESC
                    ');
                if( ! empty($tmp)){
                    foreach ($tmp as $v) {
                        if ( ! isset($declined[ $v['order_id'] ])) {
                            $declined[ $v['order_id'] ] = $v;
                        }
                    }
                }
                $updateCounters = false;
                $agree = array(); # исполнитель согласился с предложением, возможно есть отзывы, которые необходимо показать в кабинете
                foreach ($aData as & $v) {
                    if (isset($performers[ $v['id'] ])) {
                        if($performers[ $v['id'] ]['offer_status'] == Orders::OFFER_STATUS_PERFORMER_AGREE){
                            $agree[] = $v['id'];
                        }
                        unset($performers[ $v['id'] ]['order_id']);
                        $v += $performers[ $v['id'] ];
                    }
                    if (isset($declined[ $v['id'] ])) { # сбросим флаг нового сообщения и пересчитаем счетчик, т.к. исполнитель его уже увидел
                        $v['declined'] = $declined[ $v['id'] ];
                        if($declined[ $v['id'] ]['is_new']){
                            $this->chatSave($declined[ $v['id'] ]['chat_id'], array('is_new' => 0));
                            $this->db->exec('
                                UPDATE '.TABLE_ORDERS_OFFERS.'
                                SET chat_new_client = (
                                    SELECT COUNT(id)
                                    FROM '.TABLE_ORDERS_OFFERS_CHAT.'
                                    WHERE offer_id = :offer AND is_new = 1 )
                                WHERE id = :offer
                                    ', array(
                                ':offer' => $declined[ $v['id'] ]['id']
                            ));
                            $updateCounters = true;
                        }
                    }
                } unset($v);
                if ($updateCounters) { # пересчитаем итоговые счетчики
                    $this->calcUserCounters(User::id());
                }
                $fairplayEnabled = bff::fairplayEnabled();
                if (!empty($agree) && ! $fairplayEnabled) {
                    # список отзывов для отображения в кабинете
                    $tmp = Opinions::model()->opinionsList(array(
                        'order_id'  => $agree,
                    ));
                    if (!empty($tmp)) {
                        $opinions = array();
                        foreach ($tmp as $v) {
                            $opinions[ $v['order_id'] ][ $v['author_id'] ] = $v;
                        }
                        foreach ($aData as & $v) {
                            if (isset($opinions[ $v['id'] ])) {
                                $v['opinions'] = $opinions[ $v['id'] ];
                            }
                        } unset($v);
                    }
                }
                if ($fairplayEnabled) {
                    # список договоров с ходом работ
                    $workflows = Fairplay::model()->workflowsDataByFilter(array('order_id' => $orders), array('id', 'order_id'), false);
                    $workflows = func::array_transparent($workflows, 'order_id', true);
                    foreach ($aData as & $v) {
                        if (isset($workflows[ $v['id'] ])) {
                            $w = $workflows[ $v['id'] ];
                            $v['workflow']['url'] = Fairplay::url('view', array('id' => $w['id'], 'keyword' => $v['keyword']));
                        }
                    } unset($v);
                }
            }
        } else {
            $aData = array();
        }
        return $aData;
    }

    /**
     * Счетчики для списка заказов пользователя. Пользователь - владелец заказов
     * @param array $aOrigFilter
     * @return array
     */
    public function ordersListOwnerCounts(array $aOrigFilter = array())
    {
        $aData = array('status' => array());
        $bMyList = User::id() == $aOrigFilter['user_id'];
        if( ! $bMyList){
            $aOrigFilter['visibility'] = Orders::VISIBILITY_ALL;
        }

        $ordersOpinions = Orders::ordersOpinions();
        $aFilter = $aOrigFilter;
        unset($aFilter['status'], $aFilter['moderated'], $aFilter['in_work']);
        $aFilter['removed'] = 0;
        $aFilter = $this->prepareFilter($aFilter, 'O');
        $aStatus = $this->db->select('
            SELECT O.status,'.($bMyList ? 'O.moderated, ' : '').' COUNT(O.id) AS cnt
            FROM ' . TABLE_ORDERS . ' O
            ' . $aFilter['where'] . '
            GROUP BY O.status'.($bMyList ? ', O.moderated' : ''), $aFilter['bind']
        );
        if($bMyList){
            $aData['status'][Orders::STATUS_NOTMODERATED] = 0;
        }

        foreach ($aStatus as $v) {
            if($bMyList){
                if($v['moderated'] == 0){
                    $aData['status'][Orders::STATUS_NOTMODERATED] += $v['cnt'];
                }
                if( ! isset($aData['status'][$v['status']])){
                    $aData['status'][$v['status']] = $v['cnt'];
                } else {
                    $aData['status'][$v['status']] += $v['cnt'];
                }
            }else {
                if($v['status'] != Orders::STATUS_BLOCKED) {
                    $aData['status'][$v['status']] = $v['cnt'];
                }
            }
        }
        $aData['status'][0] = array_sum($aData['status']) -
            (isset($aData['status'][Orders::STATUS_NOTMODERATED])
                ? $aData['status'][Orders::STATUS_NOTMODERATED]
                : 0
            ) ;
        foreach (array(Orders::STATUS_OPENED, Orders::STATUS_CLOSED, Orders::STATUS_BLOCKED, Orders::STATUS_NOTMODERATED) as $v) {
            if (!isset($aData['status'][$v])) {
                $aData['status'][$v] = 0;
            }
        }
        
        # В работе
        $aFilter = $aOrigFilter;
        unset($aFilter['status'], $aFilter['moderated'], $aFilter['in_work']);
        $aFilter['removed'] = 0;

        if ($ordersOpinions) {
            $aFilter[':agree'] = array('F.status = :agree', ':agree' => Orders::OFFER_STATUS_PERFORMER_AGREE);
            $aFilter[':author_id'] = 'OP.author_id IS NULL';
        } else {
            $aFilter[':performer'] = 'O.performer_id > 0';
        }
        
        $aFilter = $this->prepareFilter($aFilter, 'O');

        $ordersOpinionsJoin = $ordersOpinions 
                            ? ' JOIN bff_orders_offers F ON (O.id = F.order_id AND O.performer_id = F.user_id)
                                LEFT JOIN '. TABLE_OPINIONS .' OP ON  (O.id = OP.order_id AND O.user_id = OP.author_id) '
                            : '';

        $aData['status'][Orders::STATUS_IN_WORK] = $this->db->one_data('
            SELECT COUNT(DISTINCT O.id)
            FROM 
                ' . TABLE_ORDERS . ' O
                ' . $ordersOpinionsJoin . '
            ' . $aFilter['where'] . ' 
        ', $aFilter['bind']);

        # Корзина
        $aFilter = $aOrigFilter;
        unset($aFilter['status'], $aFilter['moderated'], $aFilter['in_work']);
        $aFilter['removed'] = 1;
        $aFilter = $this->prepareFilter($aFilter, 'O');
        $aData['status'][Orders::STATUS_REMOVED] = $this->db->one_data('
            SELECT COUNT(*) FROM ' . TABLE_ORDERS . ' O ' . $aFilter['where'] . '
        ', $aFilter['bind']);

        if (Orders::useProducts()) {
            $aData['type'] = array();
            $aFilter = $aOrigFilter;
            unset($aFilter['type']);

            # В работе
            $join = '';
            if ( ! empty($aFilter['in_work'])) {
                if ($ordersOpinions) {
                    $join = ', '.TABLE_ORDERS_OFFERS.' F ';
                    $aFilter[':offers'] = 'O.id = F.order_id';
                    $aFilter[':performer'] = 'O.performer_id = F.user_id';
                    $aFilter[':agree'] = array('F.status = :agree', ':agree' => Orders::OFFER_STATUS_PERFORMER_AGREE);
                } else {
                    $aFilter[':performer'] = 'O.performer_id > 0';
                }
                unset($aFilter['in_work']);
            }

            $aFilter = $this->prepareFilter($aFilter, 'O');
            $aTypes = $this->db->select('
                SELECT O.type, COUNT(O.id) AS cnt
                FROM ' . TABLE_ORDERS . ' O '.$join.'
                ' . $aFilter['where'] . '
                GROUP BY O.type', $aFilter['bind']
            );
            foreach ($aTypes as $v) {
                $aData['type'][$v['type']] = $v['cnt'];
            }
            $aData['type'][0] = array_sum($aData['type']);

            foreach (Orders::aTypes() as $k => $v) {
                if (!isset($aData['type'][$k])) {
                    $aData['type'][$k] = 0;
                }
            }
        }

        return $aData;
    }

    /**
     * Список заказов пользователя. Пользователь - владелец одной из заявок к заказам
     * @param array $aFilter фильтр списка заказов
     * @param bool $bCount только подсчет кол-ва заказов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function ordersListRespondent(array $aFilter = array(), $bCount = false, $sqlLimit = '', $sqlOrder = '') // frontend
    {
        $userID = User::id();
        $myList = ! empty($aFilter['user_id']) && $userID == $aFilter['user_id'];
        $aFilter = $this->prepareFilter($aFilter, 'F');

        if ($bCount) {
            return $this->db->one_data('SELECT 
                                            COUNT(F.id) 
                                            FROM ' . TABLE_ORDERS_OFFERS . ' F '
                                            . $aFilter['where'].' AND F.status != '.Orders::OFFER_STATUS_PERFORMER_START,
                $aFilter['bind']);
        }
    
        $join = '';
        $statusSelect = 'O.status';
        $ordersOpinions = Orders::ordersOpinions();
    
        if ($ordersOpinions){
            $join = ' LEFT JOIN '. TABLE_OPINIONS .' OP ON  (O.id = OP.order_id AND O.user_id = OP.author_id) ';
            $statusSelect = '(CASE WHEN (O.status = '.Orders::STATUS_CLOSED.' AND OP.author_id IS NULL) THEN '.Orders::STATUS_IN_WORK.'
                                   ELSE O.status 
                              END) as status';
        }
        
        $aData = $this->db->select('
            SELECT 
                O.*, 
                O.id, 
                O.type, 
                O.service_type, 
                ' . $statusSelect . ', 
                O.title, O.descr, O.keyword, O.pro, O.fairplay, O.created, O.offers_cnt,
                   O.price, O.price_curr, O.price_ex, O.price_rate_text, O.user_id AS order_user_id, O.visibility,
                   O.views_total, O.district_id, 
                   GROUP_CONCAT(T.tag_id) AS tags, MAX(R.reg3_city) AS reg3_city,
                   F.id as offer_id, F.status AS offer_status, F.user_id, F.chatcnt, F.user_removed, F.chat_new_worker
            FROM ' . TABLE_ORDERS_OFFERS . ' F,
                 ' . TABLE_ORDERS . ' O
                    LEFT JOIN ' . TABLE_ORDERS_IN_TAGS . ' T ON O.id = T.order_id
                    LEFT JOIN ' . TABLE_ORDERS_REGIONS . ' R ON O.id = R.order_id
                    ' . $join . '
            ' . $aFilter['where'] . ' AND  F.order_id = O.id AND F.status != '.Orders::OFFER_STATUS_PERFORMER_START.' 
            GROUP BY O.id ' .
            (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, $aFilter['bind']
        );
        if ( ! empty($aData)) {
            $this->ordersListPrepare($aData);
        } else {
            $aData = array();
        }
        if (Orders::ordersOpinions() && $myList) { # просматривает владелец одной из заявок. Кабинет исполнителя
            $performer = array(); # исполнитель согласился с предложением, возможно есть отзывы, которые необходимо показать в кабинете
            foreach ($aData as $v) {
                if ($v['offer_status'] == Orders::OFFER_STATUS_PERFORMER_AGREE) {
                    $performer[] = $v['id'];
                }
            }
            if (bff::fairplayEnabled()) {
                # список договоров с ходом работ
                $workflows = Fairplay::model()->workflowsDataByFilter(array('order_id' => $performer), array('id', 'order_id'), false);
                $workflows = func::array_transparent($workflows, 'order_id', true);
                foreach ($aData as & $v) {
                    if (isset($workflows[ $v['id'] ])) {
                        $w = $workflows[ $v['id'] ];
                        $v['workflow']['url'] = Fairplay::url('view', array('id' => $w['id'], 'keyword' => $v['keyword']));
                    }
                } unset($v);
            } else {
                if( ! empty($performer)) {
                    # список отзывов для отображения в кабинете
                    $tmp = Opinions::model()->opinionsList(array(
                        'order_id' => $performer,
                    ));
                    if( ! empty($tmp)) {
                        $opinions = array();
                        foreach($tmp as $v) {
                            $opinions[$v['order_id']][$v['author_id']] = $v;
                        }
                        foreach($aData as &$v) {
                            if(isset($opinions[$v['id']])) {
                                $v['opinions'] = $opinions[$v['id']];
                            }
                        }
                        unset($v);
                    }
                }
            }
        }

        return $aData;
    }

    /**
     * Список заказов для исполнителя с предложением стать исполнителем по заказу(performer)
     * @param array $filter фильтр списка
     * @return mixed
     */
    public function ordersListPerformerStart($filter = array())
    {
        $filter += array(
            'user_id' => User::id(),
            'status' => Orders::OFFER_STATUS_PERFORMER_START,
        );
        $filter = $this->prepareFilter($filter, 'F');

        return $this->db->select('
            SELECT F.id, F.chat_id, F.price_from, F.price_to, F.price_curr, F.price_rate, F.price_rate_text,
                   F.terms_from, F.terms_to, F.terms_type, F.workflow,
                   O.id AS order_id, O.title, O.keyword, O.fairplay, O.start_date,
                   U.login, U.name, U.surname, U.pro, U.avatar, U.sex, U.verified, U.user_id,
                   C.message
            FROM ' . TABLE_ORDERS_OFFERS . ' F
                    LEFT JOIN ' . TABLE_ORDERS_OFFERS_CHAT . ' C ON F.chat_id = C.id
                 , ' . TABLE_ORDERS . ' O, ' . TABLE_USERS . ' U
            ' . $filter['where'] . ' AND F.order_id = O.id AND O.user_id = U.user_id
            ORDER BY O.created DESC
        ', $filter['bind']);
    }

    public function getOrderCity($nOrderId)
    {
        if(!$nOrderId){
            return false;
        }

        $sql = 'SELECT 
                  MAX(reg3_city) AS  reg3_city 
                  FROM ' . TABLE_ORDERS_REGIONS . ' 
                  WHERE :order_id';

        return $this->db->one_data($sql, [':order_id'=> $nOrderId]);
    }

    /**
     * Список заказов для исполнителя со статусом предложения стать исполнителем по заказу(performer)
     * @param array $filter фильтр списка
     * @return mixed
     */
    public function ordersListPerformerState($filter = array())
    {
        $filter += array(
            'user_id' => User::id(),
        );
        $filter = $this->prepareFilter($filter, 'F');

        return $this->db->select('
            SELECT F.id, F.chat_id, F.price_from, F.price_to, F.price_curr, F.price_rate, F.price_rate_text,
                   F.terms_from, F.terms_to, F.terms_type, F.workflow,
                   F.status,
                   O.id AS order_id, O.title, O.keyword, O.fairplay,
                   U.login, U.name, U.surname, U.pro, U.avatar, U.sex, U.verified, U.user_id,
                   C.message
            FROM ' . TABLE_ORDERS_OFFERS . ' F
                    LEFT JOIN ' . TABLE_ORDERS_OFFERS_CHAT . ' C ON F.chat_id = C.id
                 , ' . TABLE_ORDERS . ' O, ' . TABLE_USERS . ' U
            ' . $filter['where'] . ' AND F.order_id = O.id AND O.user_id = U.user_id
            ORDER BY O.created DESC
        ', $filter['bind']);
    }

    /**
     * Счетчики для списка заказов пользователя. Пользователь - владелец одной из заявок к заказам
     * @param array $aOrigFilter
     * @param bool $bNeedPerformerStartStatus необходимость учитывать статус OFFER_STATUS_PERFORMER_START - Предложение стать исполнителем
     * @return array
     */
    public function ordersListRespondentCounts(array $aOrigFilter = array(), $bNeedPerformerStartStatus = true)
    {
        unset($aOrigFilter['status']);

        $aFilter = $aOrigFilter;
        $aFilter['user_removed'] = 0;
        $aFilter = $this->prepareFilter($aFilter, 'O');
        $aStatus = $this->db->select_key('
            SELECT O.status, COUNT(O.id) AS cnt
            FROM ' . TABLE_ORDERS_OFFERS . ' O
            ' . $aFilter['where'] . '
            GROUP BY O.status', 'status', $aFilter['bind']
        );

        $aOrigFilter['user_removed'] = 1;
        $aData = array(
            1 => !empty($aStatus[0]['cnt']) ? $aStatus[0]['cnt'] : 0,
            2 => !empty($aStatus[Orders::OFFER_STATUS_CANDIDATE]['cnt']) ? $aStatus[Orders::OFFER_STATUS_CANDIDATE]['cnt'] : 0,
            3 => !empty($aStatus[Orders::OFFER_STATUS_PERFORMER]['cnt']) ? $aStatus[Orders::OFFER_STATUS_PERFORMER]['cnt'] : 0,
            4 => !empty($aStatus[Orders::OFFER_STATUS_CANCELED]['cnt']) ? $aStatus[Orders::OFFER_STATUS_CANCELED]['cnt'] : 0,
            5 => !empty($aStatus[Orders::OFFER_STATUS_PERFORMER_DECLINE]['cnt']) ? $aStatus[Orders::OFFER_STATUS_PERFORMER_DECLINE]['cnt'] : 0,
            6 => $this->ordersListRespondent($aOrigFilter, true),
        );
        if( $bNeedPerformerStartStatus && ! empty($aStatus[Orders::OFFER_STATUS_PERFORMER_START]['cnt'])){
            $aData[3] += $aStatus[Orders::OFFER_STATUS_PERFORMER_START]['cnt'];
        }
        if( ! empty($aStatus[Orders::OFFER_STATUS_PERFORMER_AGREE]['cnt'])){
            $aData[3] += $aStatus[Orders::OFFER_STATUS_PERFORMER_AGREE]['cnt'];
        }
        if( ! empty($aStatus[Orders::OFFER_STATUS_INVITE_DECLINE]['cnt'])){
            $aData[5] += $aStatus[Orders::OFFER_STATUS_INVITE_DECLINE]['cnt'];
        }

        $aSum = $aData; unset($aSum[6]);
        $aData[0] = array_sum($aSum);

        return $aData;
    }

    /**
     * Подготовка данных списка заказов
     * @param array $aData @ref данные о заказах
     */
    protected function ordersListPrepare(array &$aData = array())
    {
        if (empty($aData)) return;

        $aTags = array();
        foreach ($aData as &$v) {
            if ($v['tags']) {
                $tags = array_unique(explode(',', $v['tags']));
                if (!empty($tags)) {
                    $v['aTags'] = $tags;
                    foreach ($tags as $t) {
                        if (!in_array($t, $aTags)) {
                            $aTags[] = $t;
                        }
                    }
                }
            }
            if ($v['price_rate_text']) {
                $v['price_rate_text'] = func::unserialize($v['price_rate_text']);
            }
            if ($v['reg3_city']) {
                $v['city_data'] = Geo::regionData($v['reg3_city']);
            }
        } unset($v);

        if (!empty($aTags)) {
            $aTags = $this->db->select_key('SELECT id, tag FROM ' . TABLE_TAGS . ' WHERE ' . $this->db->prepareIN('id', $aTags), 'id');
            foreach ($aData as &$v) {
                if (!empty($v['aTags'])) {
                    foreach ($v['aTags'] as $i => $tag_id) {
                        if (!empty($aTags[$tag_id])) {
                            $v['aTags'][$i] = $aTags[$tag_id];
                        }
                    }
                }
            } unset($v);
        }
    }

    /**
     * Список заказов заказчика для предложения исполнителю
     * @param array $filter фильтр списка заказов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function ordersListInvite(array $filter = array(), $sqlLimit = '', $sqlOrder = 'O.created DESC') // frontend
    {
        $from = 'FROM '.TABLE_ORDERS.' O ';
        $bind = array();
        $fields = array();

        if(isset($filter['exclude_invite'])){ # исключим заказы, которые уже предлагали исполнителю с id = $filter['exclude_invite']
            $from .= ' LEFT JOIN '.TABLE_ORDERS_INVITES.' I ON O.id = I.order_id AND I.user_id = :exclude';
            $bind[':exclude'] = $filter['exclude_invite'];
            $filter[':invite'] = 'I.user_id IS NULL';
            unset($filter['exclude_invite']);
        }
        if(isset($filter['exclude_offer'])){ # исключим заказы, по которым исполнитель с id = $filter['exclude_offer'] уже оставил заявку
            $from .= ' LEFT JOIN '.TABLE_ORDERS_OFFERS.' F ON O.id = F.order_id AND F.user_id = :exclude_o';
            $bind[':exclude_o'] = $filter['exclude_offer'];
            $filter[':offer'] = 'F.user_id IS NULL';
            unset($filter['exclude_offer']);
        }

        if(isset($filter['worker_id'])){ # только заказы предложенные исполнителю с id = $filter['worker_id']
            $from .= ' INNER JOIN '.TABLE_ORDERS_INVITES.' I ON O.id = I.order_id 
                       INNER JOIN '.TABLE_USERS.' U ON O.user_id = U.user_id ';
            $filter[':worker'] = array(' I.user_id = :worker', ':worker' => $filter['worker_id']);
            $fields = array('U.login', 'U.name', 'U.surname', 'U.pro', 'U.avatar', 'U.sex', 'U.verified', 'I.message');
            unset($filter['worker_id']);
        }

        $from .= ' LEFT JOIN ' . TABLE_ORDERS_REGIONS . ' R ON O.id = R.order_id ';
        $filter = $this->prepareFilter($filter, 'O', $bind);

        $data = $this->db->select('
        SELECT O.*, O.id, O.type, O.title, O.descr, O.status, O.views_total, O.created, O.keyword, O.offers_cnt,
               O.price, O.price_curr, O.price_ex, O.price_rate_text, O.status AS order_status, O.district_id, MAX(R.reg3_city) AS reg3_city '.
            ( ! empty($fields) ? ','.join(',', $fields) : '').'
            ' . $from . '
            ' . $filter['where'] . '
            ' . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '') . '
            ' . $sqlLimit, $filter['bind']
        );
        foreach($data as $k=>&$v){
            if(empty($data[$k]['id'])){
                unset($data[$k]);
                continue;
            }
            $v['price_rate_text'] = func::unserialize($v['price_rate_text']);
            if ($v['reg3_city']) {
                $v['city_data'] = Geo::regionData($v['reg3_city']);
            }
        } unset($v);
        return $data;
    }

    /**
     * Получение данных заказа
     * @param integer $nOrderID ID заказа
     * @param array $aFields
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function orderData($nOrderID, array $aFields = array(), $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT O.*, U.email, U.blocked AS user_blocked
                    FROM ' . TABLE_ORDERS . ' O, ' . TABLE_USERS . ' U
                    WHERE O.id = :id AND O.user_id = U.user_id',
                array(':id' => $nOrderID)
            );
            if (empty($aData)) {
                return array();
            }

            switch ($aData['type']) {
                case Orders::TYPE_SERVICE:
                {
                    $aData['specs'] = $this->orderSpecs($nOrderID);
                }
                break;
                case Orders::TYPE_PRODUCT:
                {
                    $aData['cats'] = $this->orderCats($nOrderID);
                }
                break;
            }
            $aData['regions'] = $this->orderRegions($nOrderID);
            $aData['schedule'] = $this->orderShedule($nOrderID);
        } else {
            if (empty($aFields)) {
                $aFields = array('O.*');
            }
            $aData = $this->db->one_array('SELECT ' . join(',', $aFields) . '
                    FROM ' . TABLE_ORDERS . ' O
                    WHERE O.id = :id',
                array(':id' => $nOrderID)
            );
        }
        if (!empty($aData['price_rate_text'])) {
            $aData['price_rate_text'] = func::unserialize($aData['price_rate_text']);
        }

        return $aData;
    }

    public function ordersUnactivated()
    {
        return $this->db->select_one_column('
            SELECT O.id
            FROM '.TABLE_ORDERS.' O LEFT JOIN '.TABLE_USERS.' U ON O.user_id = U.user_id
            WHERE U.user_id IS NULL
        ');
    }

    /**
     * Получение данных заказа для просмотра
     * @param integer $nOrderID ID заказа
     * @return array
     */
    public function orderDataView($nOrderID)
    {
        $join = '';
        $statusSelect = 'O.status';
        $ordersOpinions = Orders::ordersOpinions();
    
        if ($ordersOpinions){
            $join = ' LEFT JOIN bff_opinions OP ON  (O.id = OP.order_id AND O.user_id = OP.author_id) ';
            $statusSelect = '(CASE WHEN (O.status = '.Orders::STATUS_CLOSED.' AND OP.author_id IS NULL) THEN '.Orders::STATUS_IN_WORK.'
                                   ELSE O.status 
                              END) as status';
        }
        
        $aData = $this->db->one_array('
            SELECT 
                O.*, 
                ' . $statusSelect . ',
                O.pro AS order_pro, 
                U.type AS user_type, 
                U.created AS user_created, U.opinions_cache, U.contacts,
                U.name, U.surname, U.login, U.avatar, U.pro, U.avatar, U.verified, U.sex, US.last_activity
            FROM 
                ' . TABLE_ORDERS . ' O 
                JOIN ' . TABLE_USERS . ' U ON O.user_id = U.user_id
                JOIN '.TABLE_USERS_STAT.' US ON O.user_id = US.user_id
                '.$join.'
            WHERE O.id = :id',
            array(':id' => $nOrderID)
        );

        if (empty($aData)) {
            return array();
        }

        switch ($aData['type']) {
            case Orders::TYPE_SERVICE:
            {
                $aData['specs'] = $this->orderSpecs($nOrderID);
            }
            break;
            case Orders::TYPE_PRODUCT:
            {
                $aData['cats'] = $this->orderCats($nOrderID);
            }
            break;
        }
        $aData['regions'] = $this->orderRegions($nOrderID);

        if (!empty($aData['price_rate_text'])) {
            $aData['price_rate_text'] = func::unserialize($aData['price_rate_text']);
        }

        $aData['schedule'] = $this->orderShedule($nOrderID);
        return $aData;
    }

    /**
     * Получение данных о специализациях для заказа
     * @param integer $nOrderID ID заказа
     * @return mixed
     */
    public function orderSpecs($nOrderID)
    {
        $bCatON = Specializations::catsOn();

        return $this->db->select('
                        SELECT O.*, SL.title AS spec_title, S.keyword ' . ($bCatON ? ', CL.title AS cat_title, C.numlevel AS lvl, C.pid ' : '') . '
                        FROM ' . TABLE_ORDERS_SPECS . ' O,
                             ' . TABLE_SPECIALIZATIONS . ' S, ' . TABLE_SPECIALIZATIONS_LANG . ' SL
                             ' . ($bCatON ? ', ' . TABLE_SPECIALIZATIONS_CATS . ' C, ' . TABLE_SPECIALIZATIONS_CATS_LANG . ' CL ' : '') . '
                        WHERE ' . $this->db->langAnd(false, 'S', 'SL') . ' AND O.spec_id = S.id AND O.order_id = :order_id' .
            ($bCatON ? $this->db->langAnd(true, 'C', 'CL') . ' AND O.cat_id = C.id ' : '') . '
                        ORDER BY O.num ',
            array(':order_id' => $nOrderID)
        );
    }

    /**
     * Получение данных о категориях для заказа
     * @param integer $nOrderID ID заказа
     * @return mixed
     */
    public function orderCats($nOrderID)
    {
        $aData = $this->db->select('
            SELECT O.*, CL.title AS t1, C.keyword as k1, CL1.title AS t2, C1.keyword as k2
            FROM ' . TABLE_ORDERS_CATS . ' O
                 LEFT JOIN ' . TABLE_SHOP_CATS . ' C1 ON O.cat_id1 = C1.id
                 LEFT JOIN ' . TABLE_SHOP_CATS_LANG . ' CL1 ON ' . $this->db->langAnd(false, 'C1', 'CL1') . '
                 ,
                 ' . TABLE_SHOP_CATS . ' C,
                 ' . TABLE_SHOP_CATS_LANG . ' CL
            WHERE ' . $this->db->langAnd(false, 'C', 'CL') . ' AND O.cat_id = C.id AND O.order_id = :order_id',
            array(':order_id' => $nOrderID)
        );

        foreach ($aData as $k => $v) {
            $aData[$k]['title'] = ($v['t2'] != $v['t1'] ? $v['t2'] . ' / ' : '') . $v['t1'];
        }

        return $aData;
    }

    /**
     * Получение данных о регионых для заказа
     * @param integer $nOrderID ID заказа
     * @return mixed
     */
    public function orderRegions($nOrderID)
    {
        return $this->db->select('
            SELECT O.*, R.title_' . LNG . ' as title, C.title_' . LNG . ' as country_title
            FROM ' . TABLE_ORDERS_REGIONS . ' O,
                 ' . TABLE_REGIONS . ' R,
                 ' . TABLE_REGIONS . ' C
            WHERE O.reg3_city = R.id AND O.order_id = :order_id AND O.reg1_country = C.id',
            array(':order_id' => $nOrderID)
        );
    }

    /**
     * Получение данных о графике работ для заказа
     * @param integer $nOrderID ID заказа
     * @return mixed
     */
    public function orderShedule($nOrderID)
    {
        $aRes = $this->db->select('
            SELECT *
            FROM ' . TABLE_ORDERS_SCHEDULE . ' OS
            WHERE OS.order_id = :order_id ',
            array(':order_id' => $nOrderID)
        );

        if(empty($aRes)){
            return $aRes;
        }

        foreach ($aRes as $item){
            $aSchedule[$item['day_id']][] = (int)$item['period_id'];
        }
        return $aSchedule;
    }

    /**
     * Данные о нескольких заказах
     * @param array $aFilter фильтр
     * @param array $aFields список полей требуемых данных
     * @param string $sqlOrder
     * @param bool $nLimit
     * @return mixed
     */
    public function ordersDataByFilter(array $aFilter, array $aFields = array(), $sqlOrder = '', $nLimit = false)
    {
        $aFilter = $this->prepareFilter($aFilter);

        $aParams = array();
        if (empty($aFields)) {
            $aFields = array('*');
        }
        if (!is_array($aFields)) {
            $aFields = array($aFields);
        }
        foreach ($aFields as $v) {
            $aParams[] = $v;
        }

        return $this->db->select_key('SELECT ' . join(',', $aParams) . '
                                  FROM ' . TABLE_ORDERS . '
                                  ' . $aFilter['where']
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . (!empty($nLimit) ? $this->db->prepareLimit(0, $nLimit) : ''),
            'id',
            $aFilter['bind']
        );
    }

    /**
     * Обновляем данные о заказах
     * @param array $aOrdersID ID заказов
     * @param array $aData данные
     * @return integer кол-во обновленных заказов
     */
    public function ordersSave(array $aOrdersID, array $aData)
    {
        if (empty($aOrdersID) || empty($aData)) {
            return 0;
        }

        return $this->db->update(TABLE_ORDERS, $aData, array('id' => $aOrdersID));
    }

    public function ordersCnt($nUserID = 0)
    {
        return $this->db->one_data('SELECT COUNT(*) FROM '.TABLE_ORDERS.' WHERE user_id = :user', array(':user' => $nUserID));
    }

    /**
     * Получение данных о заявкак к заказу
     * @param integer $nOrderID ID заказа
     * @param array $aFilter фильтр
     * @return mixed
     */
    public function orderOffers($nOrderID, array $aFilter = array())
    {
        $aFilter['order_id'] = $nOrderID;
        $aFilter = $this->prepareFilter($aFilter, 'O');
        $catOn = Specializations::catsOn();
        $aData = $this->db->select('
            SELECT O.id, O.status, O.price_from, O.price_to, O.price_curr, O.price_rate, O.price_rate_text, O.terms_from, O.terms_to, O.terms_type,
                   O.descr, O.fairplay, O.client_only, O.user_id, O.created, O.chatcnt, O.examplescnt, O.is_new, O.chat_new_client, O.chat_new_worker, O.workflow,
                   
                   UN.id as note_id,
                   UN.note,
                   
                   U.name, U.surname, U.avatar, U.sex, U.login, U.verified, U.pro, U.type AS user_type, U.opinions_cache, U.reg3_city, U.reg1_country, U.contacts, U.spec_id, US.last_activity,
                   U.blocked AS user_blocked, SL.title AS spec_title, S.keyword AS main_spec_keyword'.($catOn ? ', C.keyword AS main_cat_keyword ' : '').'
            FROM 
                    ' . TABLE_ORDERS_OFFERS . ' O
                    JOIN ' . TABLE_USERS . ' U ON (
                        O.user_id = U.user_id AND 
                        U.deleted = 0)
                    JOIN ' . TABLE_USERS_STAT . ' US ON U.user_id = US.user_id
                    JOIN ' . TABLE_ORDERS . ' ORD ON O.order_id = ORD.id
                    LEFT JOIN ' . TABLE_USERS_NOTES . ' UN ON (
                        O.user_id = UN.user_id AND
                        ORD.user_id = UN.author_id)
                    LEFT JOIN ' . TABLE_SPECIALIZATIONS . ' S ON U.spec_id = S.id
                    LEFT JOIN ' . TABLE_SPECIALIZATIONS_LANG . ' SL ON ' . $this->db->langAnd(false, 'S', 'SL') . '
                    '.($catOn ? ' LEFT JOIN ' . TABLE_SPECIALIZATIONS_CATS . ' C ON U.cat_id = C.id ' : '').'
            ' . $aFilter['where'] . '
            ORDER BY O.created', $aFilter['bind']
        );

        return $aData;
    }

    /**
     * Сохранение заказа
     * @param integer $nOrderID ID заказа
     * @param array $aSave данные заказа
     * @param mixed $sDynpropsDataKey ключ данных дин. свойств, например 'd' или FALSE
     * @return boolean|integer
     */
    public function orderSave($nOrderID, array $aSave, $sDynpropsDataKey = false)
    {
        if (empty($aSave)) {
            return false;
        }
        if (!empty($sDynpropsDataKey)) {
            $this->prepareDP($nOrderID, $aSave, $sDynpropsDataKey);
        }
        $aData = $aSave;
        unset($aData['cats'], $aData['specs'], $aData['regions'], $aData['schedule']);

        if (isset($aData['status']) || isset($aData['status_prev']) || ! $nOrderID) {
            $aData['status_changed'] = $this->db->now();
        }

        if ($nOrderID > 0) {
            $aData['modified'] = $this->db->now(); # Дата изменения

            $res = $this->db->update(TABLE_ORDERS, $aData, array('id' => $nOrderID));

            $mRet = !empty($res);
        } else {
            $aData['created'] = $this->db->now(); # Дата создания
            $aData['modified'] = $this->db->now(); # Дата изменения
            if(empty($aData['svc_order'])){
                $aData['svc_order'] = $aData['created'];
            }

            $nOrderID = $this->db->insert(TABLE_ORDERS, $aData);
            if ($nOrderID > 0) {
                #
            }
            $mRet = $nOrderID;
        }

        if ($nOrderID) {
            $this->orderSaveCats($nOrderID, $aSave);
            $this->orderSaveSpec($nOrderID, $aSave);
            $this->orderSaveRegions($nOrderID, $aSave);
            $this->orderSaveSchedule($nOrderID, $aSave);
        }

        return $mRet;
    }

    /**
     * Сохранение данных графика работ заказа
     * @param $nOrderID ID заказа
     * @param array $aData ['schedule'] данные графика работ
     */
    public function orderSaveSchedule($nOrderID, array $aData)
    {
        if (!empty($aData['schedule'])){
            $this->db->delete(TABLE_ORDERS_SCHEDULE, array('order_id' => $nOrderID));
            $aInsert = array();
            foreach ($aData['schedule'] as $day_id => $periods) {
                # разворачиваем данные графика
                foreach ($periods as $period_id){
                    $aInsert[] = [
                        'order_id'  => $nOrderID,
                        'day_id'    => $day_id,
                        'period_id' =>$period_id
                    ];
                }
            }
            $this->db->multiInsert(TABLE_ORDERS_SCHEDULE, $aInsert);
        }
    }

    /**
     * Получаем общее кол-во заказов, ожидающих модерации
     * @return integer
     */
    public function ordersModeratingCounter()
    {
        return (int)$this->db->one_data('SELECT COUNT(O.id)
                FROM ' . TABLE_ORDERS . ' O
                WHERE O.moderated != 1'
        );
    }

    /**
     * Подготовка к сохранению данных дин. свойств
     * @param integer $nOrderID ID заказа
     * @param array $aData @ref
     * @param string $sDynpropsDataKey
     */
    public function prepareDP($nOrderID, array & $aData, $sDynpropsDataKey)
    {
        if (!$nOrderID) {
            $nTypeID = $aData['type'];
        } else {
            $nTypeID = $this->db->one_data('SELECT type FROM ' . TABLE_ORDERS . ' WHERE id = :id', array('id' => $nOrderID));
        }
        if ($nTypeID == Orders::TYPE_SERVICE) {
            if (!empty($aData['specs'])) {
                $aFirstSpec = reset($aData['specs']);
                if (!empty($aFirstSpec['spec_id'])) {
                    if (Orders::dynpropsEnabled()) {
                        $aDataDP = Specializations::i()->dpOrdersSave($aFirstSpec['spec_id'], $sDynpropsDataKey);
                        $aData = array_merge($aData, $aDataDP);
                    } else {
                        $aDataDP = Specializations::i()->dpSave($aFirstSpec['spec_id'], $sDynpropsDataKey);
                        $aData = array_merge($aData, $aDataDP);
                    }
                }
            }
        }
        if ($nTypeID == Orders::TYPE_PRODUCT && Orders::useProducts()) {
            if (!empty($aData['cats'])) {
                $nFirstCatID = reset($aData['cats']);
                if (!empty($nFirstCatID)) {
                    $aDataDP = Shop::i()->dpSave($nFirstCatID, $sDynpropsDataKey);
                    $aData = array_merge($aData, $aDataDP);
                }
            }
        }

    }

    /**
     * Сохранение специализаций заказа
     * @param integer $nOrderID ID заказа
     * @param array $aData ['specs'] массив специализаций
     */
    public function orderSaveSpec($nOrderID, array $aData)
    {
        if (!empty($aData['specs'])) {
            $aSpecs = $aData['specs'];
            $this->db->delete(TABLE_ORDERS_SPECS, array('order_id' => $nOrderID));
            foreach ($aSpecs as $k => $v) {
                $aSpecs[$k]['order_id'] = $nOrderID;
            }
            $this->db->multiInsert(TABLE_ORDERS_SPECS, $aSpecs);
        }
    }

    /**
     * Сохранение категорий заказа
     * @param integer $nOrderID ID заказа
     * @param array $aData ['cats'] массив категорий
     */
    public function orderSaveCats($nOrderID, array $aData)
    {
        if (!empty($aData['cats'])) {
            $this->db->delete(TABLE_ORDERS_CATS, array('order_id' => $nOrderID));
            $aInsert = array();
            foreach ($aData['cats'] as $v) {
                if (!$v) {
                    continue;
                }
                $aCats = Shop::model()->categoryParentsID($v);
                if (count($aInsert) < Orders::catsLimit()) {
                    $aInsert[] = array(
                        'order_id' => $nOrderID,
                        'cat_id'   => $v,
                        'cat_id1'  => isset($aCats[1]) ? $aCats[1] : 0,
                        'cat_id2'  => isset($aCats[2]) ? $aCats[2] : 0,
                    );
                }
            }
            $this->db->multiInsert(TABLE_ORDERS_CATS, $aInsert);
        }
    }

    /**
     * Сохранение регионов заказа
     * @param integer $nOrderID ID заказа
     * @param array $aData ['regions'] массив регионов
     */
    public function orderSaveRegions($nOrderID, array $aData)
    {
        if (!empty($aData['regions'])) {
            $this->db->delete(TABLE_ORDERS_REGIONS, array('order_id' => $nOrderID));
            $aInsert = array();
            foreach ($aData['regions'] as $v) {
                if (count($aInsert) < Orders::regionsLimit()) {
                    # разворачиваем данные о регионе: city_id => reg1_country, reg2_region, reg3_city
                    $aRegions = Geo::model()->regionParents($v);
                    $aInsert[] = array_merge(array('order_id' => $nOrderID), $aRegions['db']);
                }
            }
            $this->db->multiInsert(TABLE_ORDERS_REGIONS, $aInsert);
        }
    }

    /**
     * Переключатели заказа
     * @param integer $nOrderID ID заказа
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function orderToggle($nOrderID, $sField)
    {
        switch ($sField) {
            case 'enabled': # Включен
            {
                return $this->toggleInt(TABLE_ORDERS, $nOrderID, $sField, 'id');
            }
            break;
        }
    }

    /**
     * Удаление заказа
     * @param integer $nOrderID ID заказа
     * @return boolean
     */
    public function orderDelete($nOrderID)
    {
        if (empty($nOrderID)) {
            return false;
        }
        $res = $this->db->delete(TABLE_ORDERS, array('id' => $nOrderID));
        if (!empty($res)) {
            $this->db->delete(TABLE_ORDERS_CATS, array('order_id' => $nOrderID));
            $this->db->delete(TABLE_ORDERS_SPECS, array('order_id' => $nOrderID));
            $this->db->delete(TABLE_ORDERS_REGIONS, array('order_id' => $nOrderID));
            $this->db->delete(TABLE_ORDERS_IN_TAGS, array('order_id' => $nOrderID));
            $this->db->delete(TABLE_ORDERS_INVITES, array('order_id' => $nOrderID));
            $aOffers = $this->db->select_one_column('SELECT id FROM ' . TABLE_ORDERS_OFFERS . ' WHERE order_id = :order', array(':order' => $nOrderID));
            $this->controller->offerDelete($aOffers, false);

            return true;
        }

        return false;
    }

    /**
     * Получение данных заявки
     * @param integer $nOfferID ID заявки
     * @param array $aFields
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function offerData($nOfferID, array $aFields = array(), $bEdit = false)
    {
        if ($bEdit) {
            if (empty($aFields)) {
                $aFields = array('O.*');
            }
            $aData = $this->db->one_array('SELECT ' . join(',', $aFields) . '
                    FROM ' . TABLE_ORDERS_OFFERS . ' O
                    WHERE O.id = :id',
                array(':id' => $nOfferID)
            );
        } else {
            if (empty($aFields)) {
                $aFields = array('O.*');
            }
            $aData = $this->db->one_array('SELECT ' . join(',', $aFields) . '
                    FROM ' . TABLE_ORDERS_OFFERS . ' O
                    WHERE O.id = :id',
                array(':id' => $nOfferID)
            );
        }

        return $aData;
    }

    /**
     * Сохранение заявки
     * @param integer $nOfferID ID заявки
     * @param array $aData данные заявки
     * @return boolean|integer
     */
    public function offerSave($nOfferID, array $aData)
    {
        if (empty($aData)) {
            return false;
        }

        if ($nOfferID > 0) {
            $aData['modified'] = $this->db->now(); # Дата изменения

            $res = $this->db->update(TABLE_ORDERS_OFFERS, $aData, array('id' => $nOfferID));

            $mRet = !empty($res);
        } else {
            $aData['created'] = $this->db->now(); # Дата создания
            $aData['modified'] = $this->db->now(); # Дата изменения

            $nOfferID = $this->db->insert(TABLE_ORDERS_OFFERS, $aData);
            if ($nOfferID > 0) {
                #
            }
            $mRet = $nOfferID;
        }

        return $mRet;
    }

    /**
     * Данные о заявке по фильтру
     * @param array $filter
     * @param bool|array $fields false - только подсчитать количество
     * @param bool $oneArray
     * @return mixed
     */
    public function offersDataByFilter(array $filter, $fields = false,  $oneArray = true)
    {
        $filter = $this->prepareFilter($filter, 'O');
        if (empty($fields)) {
            return (int)$this->db->one_data('SELECT COUNT(*) FROM ' . TABLE_ORDERS_OFFERS . ' O ' . $filter['where'], $filter['bind']);
        } else {
            if ($oneArray) {
                return $this->db->one_array('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_ORDERS_OFFERS . ' O ' . $filter['where'].' LIMIT 1', $filter['bind']);
            } else {
                return $this->db->select('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_ORDERS_OFFERS . ' O ' . $filter['where'], $filter['bind']);
            }
        }
    }

    /**
     * Данные о переписке в заявке
     * @param integer $nOfferID ID заявки
     * @param array $aFilter
     * @return mixed
     */
    public function chatData($nOfferID, array $aFilter = array())
    {
        $aFilter[':joinUser'] = 'C.author_id = U.user_id';
        $aFilter['offer_id'] = $nOfferID;
        $aFilter = $this->prepareFilter($aFilter, 'C');

        return $this->db->select('
            SELECT C.*, U.name, U.surname, U.login, U.verified, U.pro
            FROM ' . TABLE_ORDERS_OFFERS_CHAT . ' C,
                 ' . TABLE_USERS . ' U ' .
            $aFilter['where'], $aFilter['bind']
        );
    }

    /**
     * Сохранение сообщения переписки в заявке
     * @param integer $nChatID ID переписки
     * @param array $aData
     * @return bool|int
     */
    public function chatSave($nChatID, $aData)
    {
        if (empty($aData)) {
            return false;
        }

        if ($nChatID > 0) {
            $res = $this->db->update(TABLE_ORDERS_OFFERS_CHAT, $aData, array('id' => $nChatID));

            $mRet = !empty($res);
        } else {
            $aData['created'] = $this->db->now(); # Дата создания

            $nChatID = $this->db->insert(TABLE_ORDERS_OFFERS_CHAT, $aData);
            if ($nChatID > 0) {
                $this->db->update(TABLE_ORDERS_OFFERS, array('chatcnt = chatcnt + 1'), array('id' => $aData['offer_id']));
            }
            $mRet = $nChatID;
        }

        return $mRet;
    }

    /**
     * Удаление нескольких заявок
     * @param array $mOffersID ID заявок
     * @param bool $bUpdateCounters выполнять пересчет счетчиков
     * @return bool
     */
    public function offersDelete(array $mOffersID, $bUpdateCounters = true)
    {
        if ($bUpdateCounters) {
            /* MySQL only */
            $aOrders = $this->db->select_one_column('SELECT DISTINCTROW order_id FROM ' . TABLE_ORDERS_OFFERS . ' WHERE ' . $this->db->prepareIN('id', $mOffersID));
        }
        $this->db->delete(TABLE_ORDERS_OFFERS, array('id' => $mOffersID));
        $this->db->delete(TABLE_ORDERS_OFFERS_CHAT, array('offer_id' => $mOffersID));
        if ($bUpdateCounters) {
            if (!empty($aOrders)) {
                $this->db->update(TABLE_ORDERS, array('offers_cnt' => 0), array('id' => $aOrders));
                $aCounts = $this->db->select('
                    SELECT order_id, COUNT(id) AS cnt
                    FROM ' . TABLE_ORDERS_OFFERS . '
                    WHERE ' . $this->db->prepareIN('order_id', $aOrders) . '
                    GROUP BY order_id
                '
                );
                if (!empty($aCounts)) {
                    $sSql = 'UPDATE ' . TABLE_ORDERS . ' SET offers_cnt = CASE ';
                    foreach ($aCounts as $v) {
                        $sSql .= ' WHEN id = ' . $v['order_id'] . ' THEN ' . $v['cnt'] . ' ';
                    }
                    $sSql .= ' ELSE offers_cnt END ';
                    $this->db->exec($sSql);
                }
                $this->db->exec('UPDATE ' . TABLE_ORDERS . '
                    SET performer_id = 0, performer_created = 0
                    WHERE id IN (
                        SELECT tmp.id FROM (
                            SELECT R.id, F.id AS offer_id
                            FROM ' . TABLE_ORDERS . ' R
                                LEFT JOIN ' . TABLE_ORDERS_OFFERS . ' F ON R.id = F.order_id AND F.status = ' . Orders::OFFER_STATUS_PERFORMER . '
                        ) AS tmp
                        WHERE tmp.offer_id IS NULL
                    )'
                );
            }
        }

        return true;
    }

    /**
     * Удаление признака новых заявок у заказа
     * @param $orderID integer ID заказа
     */
    public function orderOffersClearNew($orderID)
    {
        if ( ! $orderID) return;

        $this->db->update(TABLE_ORDERS_OFFERS, array('is_new' => 0), array('order_id' => $orderID));
    }

    /**
     * Пересчет счетчиков количества заявок к заказам
     * @return array
     */
    public function reCalcOffersCnt()
    {
        return $this->db->exec('
            UPDATE '.TABLE_ORDERS.' oU, (
                SELECT O.id, COUNT(F.id) AS cnt
                FROM '.TABLE_ORDERS.' O
                    LEFT JOIN '.TABLE_ORDERS_OFFERS.' F ON O.id = F.order_id
                GROUP BY O.id
            ) CNT
            SET oU.offers_cnt = CNT.cnt
            WHERE oU.id = CNT.id
        ');
    }

    /**
     * Обновление статуса заявок по крону
     * @param integer $nSecDelay таймаут в секундах
     * @return mixed
     */
    public function offerCronStatusData($nSecDelay)
    {
        if (Orders::ordersOpinions()) {
            return $this->db->select('
                SELECT O.id, O.status AS `{status}`, O.order_id, R.title AS `{title}`, R.keyword, U.name AS `{name}`, U.email AS `{email}`,
                       R.title AS `{order_title}`, U.name AS `{worker_name}`, U.login AS `{worker_login}`, U.surname AS `{surname}`,
                       cU.name AS `{client_name}`, cU.surname AS `{client_surname}`, cU.login AS `{client_login}`, C.message AS `{message}`
                FROM ' . TABLE_ORDERS_OFFERS . ' O
                        LEFT JOIN '.TABLE_ORDERS_OFFERS_CHAT.' C ON O.chat_id = C.id
                     ,' . TABLE_ORDERS . ' R
                     ,' . TABLE_USERS . ' U
                     ,' . TABLE_USERS . ' cU
                WHERE O.order_id = R.id AND R.status != ' . Orders::STATUS_BLOCKED . '
                  AND O.user_id = U.user_id AND U.blocked = 0
                  AND R.user_id = cU.user_id AND cU.blocked = 0
                  AND O.status != 0
                  AND O.enotify_send = 0
                  AND O.status_created < :datetime
            ', array(':datetime' => date('Y-m-d H:i:s', time() - $nSecDelay)));
        } else {
            return $this->db->select('
                SELECT O.id, O.status AS `{status}`, O.order_id, R.title AS `{title}`, R.keyword, U.name AS `{name}`, U.email AS `{email}`
                FROM ' . TABLE_ORDERS_OFFERS . ' O,
                     ' . TABLE_ORDERS . ' R,
                     ' . TABLE_USERS . ' U
                WHERE O.order_id = R.id AND R.status != ' . Orders::STATUS_BLOCKED . '
                  AND O.user_id = U.user_id AND U.blocked = 0
                  AND O.status != 0
                  AND O.enotify_send = 0
                  AND O.status_created < :datetime
            ', array(':datetime' => date('Y-m-d H:i:s', time() - $nSecDelay)));
        }
    }

    /**
     * Закрытие заказов по крону
     * @param integer $nDaysAfterSet кол-во дней прошедших после выбора исполнителя
     */
    public function ordersCronClose($nDaysAfterSet)
    {
        if( ! Orders::ordersOpinions()) {
            $this->db->exec('
                UPDATE ' . TABLE_ORDERS . ' SET status = :status_closed
                WHERE status = :status_opened AND performer_id > 0 AND performer_created < :datetime
            ', array(
                        ':datetime' => date('Y-m-d', strtotime('-' . $nDaysAfterSet . ' days')),
                        ':status_opened' => Orders::STATUS_OPENED,
                        ':status_closed' => Orders::STATUS_CLOSED,
                    )
                );
        }

        # Закроем заказы, у которых закончился срок публикации
        if(Orders::termsEnabled()) {
            $this->db->exec('
            UPDATE ' . TABLE_ORDERS . '
                SET status = :closed, status_prev = :opened
            WHERE status = :opened AND expire < :now AND expire != 0 '.
                (Orders::ordersOpinions() ? ' AND performer_id = 0 ' : '' ),
                array(
                    ':closed' => Orders::STATUS_CLOSED,
                    ':opened' => Orders::STATUS_OPENED,
                    ':now' => $this->db->now(),
                ));
        }
    }

    /**
     * Получаем общее кол-во заявок, ожидающих модерации
     * @return integer
     */
    public function offersModeratingCounter()
    {
        return (int)$this->db->one_data('SELECT COUNT(O.id)
                FROM ' . TABLE_ORDERS_OFFERS . ' O
                WHERE O.moderated != 1
                '
        );
    }

    /**
     * Получение заказов для формирования файла Sitemap.xml (cron)
     * @param array $aFilter фильтр
     * @param string $sPriority приоритетность url
     * @return callable callback-генератор строк вида array [['l'=>'url страницы','m'=>'дата последних изменений'],...]
     */
    public function ordersSitemapXmlData(array $aFilter = array(), $sPriority = '')
    {
        $aFilter['status'] = Orders::STATUS_OPENED;
        $aFilter['visibility'] = Orders::VISIBILITY_ALL;
        if (Orders::premoderation()) {
            $aFilter[] = 'moderated > 0';
        }

        return function($count = false, callable $callback = null) use ($aFilter, $sPriority) {
            if ($count) {
                $aFilter = $this->prepareFilter($aFilter);
                return $this->db->one_data('SELECT COUNT(*) FROM '.TABLE_ORDERS.' '.$aFilter['where'], $aFilter['bind']);
            } else {
                $aFilter = $this->prepareFilter($aFilter, '', array(
                    ':format' => '%Y-%m-%d',
                ));
                $this->db->select_iterator('
                    SELECT id, keyword, DATE_FORMAT(modified, :format) as m
                    FROM ' . TABLE_ORDERS . '
                    '. $aFilter['where'] .'
                    ORDER BY modified DESC',
                    $aFilter['bind'],
                    function(&$item) use (&$callback, $sPriority){
                        $item['l'] = Orders::url('view', array('id' => $item['id'], 'keyword' => $item['keyword']));
                        if ( ! empty($sPriority)) {
                            $item['p'] = $sPriority;
                        }
                        $callback($item);
                    });
            }
            return false;
        };
    }

    /**
     * Событие изменение курса валюты
     * @param integer $nCurrencyID ID валюты
     */
    public function onCurrencyRateChange($nCurrencyID)
    {
        if ($nCurrencyID <=0 || $nCurrencyID == Site::currencyDefault('id')) return;

        $this->db->exec('UPDATE '.TABLE_ORDERS.'
            SET price_search = ROUND(price * :rate)
            WHERE price_curr = :currency
              AND price > 0', array(
                ':currency' => $nCurrencyID,
                ':rate' => floatval(Site::currencyData($nCurrencyID, 'rate')),
              ));
    }

    /**
     * Формирование рассылки для уведомлений исполнителей о новом заказе (мгновенная отправка)
     * @param integer $nOrderID ID заказа
     * @param integer $nReceiversTotal кол-во получателей, если 0 только подсчет кол-ва
     * @param array $aTplData данные для отправки
     * @return mixed
     */
    public function makeEnotifyNewOrderNow($nOrderID, $nReceiversTotal = 0, $aTplData = array())
    {
        $bOnlyPro = (config::get('orders_enotify_neworders_user') == Orders::ENOTIFY_RECEIVERS_PRO);
        $regions = $this->db->select('SELECT reg1_country, reg3_city FROM '.TABLE_ORDERS_REGIONS.' WHERE order_id = :id', array(':id' => $nOrderID));
        $reg = array();
        $bind = array(':order' => $nOrderID, ':enotify' => Users::ENOTIFY_ORDERS_NEW);
        if (!empty($regions)) {
            foreach ($regions as $k => $v) {
                $reg[] = ' ( U.reg3_city > 0 AND U.reg3_city = :city'.$k.' ) OR ( U.reg3_city = 0 AND U.reg1_country = :country'.$k.' ) ';
                $bind[':city'.$k] = $v['reg3_city'];
                $bind[':country'.$k] = $v['reg1_country'];
            }
        }

        if ($nReceiversTotal == 0) {
            return $this->db->one_data('
                SELECT COUNT(*) FROM (
                    SELECT U.user_id
                    FROM '.TABLE_ORDERS_SPECS.' OS,
                         '.TABLE_USERS_SPECIALIZATIONS.' US,
                         '.TABLE_USERS.' U
                    WHERE OS.order_id = :order AND OS.spec_id = US.spec_id AND US.user_id = U.user_id AND US.disabled = 0
                      AND (U.enotify & :enotify != 0)
                      '.($bOnlyPro ? ' AND U.pro = 1 ' : '').
                        ( ! empty($reg) ? ' AND ( '.join(' OR ', $reg).' ) ' : '').'
                    GROUP BY U.user_id
                ) sl
            ', $bind);
        }

        # Формируем рассылку
        $nMassendID = $this->db->insert(TABLE_MASSEND, array(
                'type'     => Sendmail::TYPE_AUTO,
                'total'    => $nReceiversTotal,
                'started'  => $this->db->now(),
                'settings' => serialize(array(
                        'subject'    => $aTplData['subject'],
                        'body'       => $aTplData['body'],
                        'is_html'    => $aTplData['is_html'],
                        'time_total' => 0,
                        'time_avg'   => 0
                    )
                ),
            )
        );
        if (empty($nMassendID)) {
            bff::log('Ошибка инициализации рассылки');
            return false;
        }


        # Формируем список получателей рассылки
        $bind[':massend'] = $nMassendID;
        $this->db->exec('
            INSERT INTO '.TABLE_MASSEND_RECEIVERS.' (massend_id, user_id)
                SELECT :massend AS massend_id, U.user_id
                FROM '.TABLE_ORDERS_SPECS.' OS,
                     '.TABLE_USERS_SPECIALIZATIONS.' US,
                     '.TABLE_USERS.' U
                WHERE OS.order_id = :order AND OS.spec_id = US.spec_id AND US.user_id = U.user_id AND US.disabled = 0
                  AND (U.enotify & :enotify != 0)
                  '.($bOnlyPro ? ' AND U.pro = 1 ' : '').
                    ( ! empty($reg) ? ' AND ( '.join(' OR ', $reg).' ) ' : '').'
                GROUP BY U.user_id
            ', $bind);

        return true;
    }

    /**
     * Рассылка уведомлений исполнителям о новых заказах
     * @param integer $nCount @ref количество отправленных писем
     * @return bool была ли завершена рассылка
     */
    public function processEnotifyNewOrderGroup(& $nCount = 0)
    {
        $nCount = 0;
        $nUserIDLast  = (int)config::get('orders.enotify.neworders.last-user-id', 0);
        $bOnlyPro     = config::get('orders_enotify_neworders_user', Orders::ENOTIFY_RECEIVERS_ALL) == Orders::ENOTIFY_RECEIVERS_PRO;
        $nLimit       = config::sys('orders.enotify.neworders.iteration', 100, TYPE_UINT);
        $nOrdersLimit = config::sys('orders.enotify.neworders.bulk.limit', 25, TYPE_UINT);

        $aUsers = $this->db->select('
            SELECT U.user_id, U.name, U.surname, U.email, U.login, U.reg1_country, U.reg3_city, S.enotify_neworders_last AS last
            FROM '.TABLE_USERS.' U, '.TABLE_USERS_STAT.' S
            WHERE U.user_id > :last
              AND U.activated = 1 AND U.blocked = 0
              AND (U.enotify & :enotify != 0)
            '.(Users::useClient() ? ' AND U.type = ' . Users::TYPE_WORKER : '').
              ($bOnlyPro ? ' AND U.pro = 1 ' : '') .'
              AND U.user_id = S.user_id
            ORDER BY U.user_id
            LIMIT '.($nLimit + 1).'
        ', array(':last' => $nUserIDLast, ':enotify' => Users::ENOTIFY_ORDERS_NEW));
        if (empty($aUsers)) $aUsers = array();

        $bContinue = count($aUsers) > $nLimit;
        if ($bContinue) {
            array_pop($aUsers);
        }
        foreach ($aUsers as $v)
        {
            $nUserIDLast = $v['user_id'];

            if ($v['last'] == '0000-00-00 00:00:00') {
                $v['last'] = date('Y-m-d H:i:s', strtotime('-1 day'));
            }

            $bRegion = $v['reg3_city'] || $v['reg1_country'];

            $bind = array(':user_id' => $v['user_id'], ':last_approved' => $v['last']);
            if($bRegion){
                if($v['reg3_city']) {
                    $bind[':city'] = $v['reg3_city'];
                }else{
                    $bind[':country'] = $v['reg1_country'];
                }
            }

            $aOrders = $this->db->select('
                SELECT O.id, O.title, O.keyword
                FROM '.TABLE_ORDERS.' O
                    '.( $bRegion ? ' LEFT JOIN '.TABLE_ORDERS_REGIONS.' R ON O.id = R.order_id ' : '' ).'
                    , '.TABLE_ORDERS_SPECS.' S, '.TABLE_USERS_SPECIALIZATIONS.' US
                WHERE O.id = S.order_id AND S.spec_id = US.spec_id
                  AND US.user_id = :user_id AND US.disabled = 0 AND O.approved >= :last_approved
                  AND O.type = '.Orders::TYPE_SERVICE.' AND O.status = '.Orders::STATUS_OPENED.'
                  '.( $bRegion ? ' AND ( R.reg3_city IS NULL OR '.( $v['reg3_city'] ? ' R.reg3_city = :city ' : ' R.reg1_country = :country ' ).' )' : '' ).'
                  AND O.visibility = '.Orders::VISIBILITY_ALL.'
                GROUP BY O.id
                ORDER BY O.created DESC
                LIMIT '.($nOrdersLimit + 1).'
            ', $bind);

            if (empty($aOrders)) continue;

            $bMore = count($aOrders) > $nOrdersLimit;
            if ($bMore) {
                array_pop($aOrders);
            }
            $sBlock = '';
            foreach ($aOrders as $vv) {
                $sBlock .= '<a href="'.Orders::url('view', array('id' => $vv['id'], 'keyword' => $vv['keyword'])).'">'.$vv['title'].'</a><br />'."\n";
            }
            if ($bMore) {
                $sBlock .= '<a href="'.Orders::url('list', array('4me' => 1)).'">'._t('orders', 'Еще...').'</a><br />'."\n";
            }
            $fio = $v['name'].' '.$v['surname'];
            if (empty($v['name']) && empty($v['surname'])) {
                $fio = $v['login'];
            }
            $name = ! empty($v['name']) ? $v['name'] : $fio;

            if (bff::sendMailTemplate(array(
                    'fio'          => $fio,
                    'name'         => $name,
                    'orders_block' => $sBlock,
                ),
                'orders_order_new_group', $v['email']
            )) {
                $nCount++;
            }

            Users::model()->userSave($v['user_id'], array(), array('enotify_neworders_last' => $this->db->now()));
        }
        if ($bContinue) {
            # продолжим рассылку с этого пользователя на следующей итерации
            config::save('orders.enotify.neworders.last-user-id', $nUserIDLast, true);
        } else {
            # последняя итерация
            config::save('orders.enotify.neworders.last-user-id', 0, true);
        }
        return ! $bContinue;
    }

    /**
     * Подсчет счетчика новых событий для пользователя
     * @param integer $userID ID пользователя
     */
    public function calcUserCounters($userID)
    {
        if ( ! $userID) return;

        $calcWorker = true;
        $calcClient = true;
        if (Users::useClient()) {
            $user = Users::model()->userData($userID, array('type'));
            if (empty($user['type'])) return;
            if ($user['type'] == Users::TYPE_WORKER) {
                $calcClient = false;
            } else if ($user['type'] == Users::TYPE_CLIENT) {
                $calcWorker = false;
            }
        }

        $update = array();
        if ($calcClient) {
            # новые сообщения в заявках (в опубликованных заказах)
            $data = $this->db->one_array('
                SELECT SUM(is_new) AS offers, SUM(chat_new_client) AS chat
                FROM '.TABLE_ORDERS_OFFERS.' F,
                     '.TABLE_ORDERS.' O
                WHERE F.order_id = O.id AND O.user_id = :user', array(':user' => $userID));
            if (!empty($data)) {
                $update['orders_offers_client'] = $data['offers'] + $data['chat'];
            }
        }
        if ($calcWorker) {
            # новые сообщения в заявках (в опубликованных заявках)
            $data = $this->db->one_array('
                SELECT SUM(chat_new_worker) AS chat
                FROM '.TABLE_ORDERS_OFFERS.' F
                WHERE F.user_id = :user', array(':user' => $userID));
            if (!empty($data)) {
                $update['orders_offers_worker'] = $data['chat'];
            }
            if (Orders::invitesEnabled()) {
                # новые предложения заказов
                $data = $this->db->one_array('
                    SELECT COUNT(order_id) AS invites
                    FROM ' . TABLE_ORDERS_INVITES . '
                    WHERE user_id = :user
                ', array(':user' => $userID));
                if (!empty($data['invites'])) {
                    if (empty($update['orders_offers_worker'])) {
                        $update['orders_offers_worker'] = $data['invites'];
                    } else {
                        $update['orders_offers_worker'] += $data['invites'];
                    }
                }
            }
        }
        if (Orders::ordersOpinions()) {
            # новые новых предложений стать исполнителем
            $offerPerformerCnt = $this->db->one_data('
                SELECT COUNT(id)
                FROM '.TABLE_ORDERS_OFFERS.' F
                WHERE F.user_id = :user AND F.status = :start', array(
                ':user'  => $userID,
                ':start' => Orders::OFFER_STATUS_PERFORMER_START,
            ));
            if ($offerPerformerCnt) {
                if (empty($update['orders_offers_worker'])) {
                    $update['orders_offers_worker'] = $offerPerformerCnt;
                } else {
                    $update['orders_offers_worker'] += $offerPerformerCnt;
                }
            }
        }
        if (!empty($update)) {
            Users::model()->userSave($userID, array(), $update);
        }
    }

    /**
     * Сохранение предложения заказа
     * @param array $data
     * @return bool
     */
    public function inviteSave($data)
    {
        $keys = array('order_id', 'user_id');
        $conditions = array();
        foreach ($keys as $k) {
            if (empty($data[$k])) return false;
            $conditions[$k] = $data[$k];
        }

        $filter = $this->prepareFilter($conditions);
        $cnt = $this->db->one_data('SELECT COUNT(*) FROM '.TABLE_ORDERS_INVITES.$filter['where'], $filter['bind']);
        if ($cnt) {
            return $this->db->update(TABLE_ORDERS_INVITES, array_diff_key($data, $conditions), $conditions);
        } else {
            $data['created'] = $this->db->now();
            return $this->db->insert(TABLE_ORDERS_INVITES, $data);
        }
    }

    /**
     * Удаление предложения заказа
     * @param array $filter
     * @return bool
     */
    public function inviteDelete(array $filter)
    {
        $keys = array('order_id', 'user_id');
        foreach ($keys as $k) {
            if (empty($filter[$k])) return false;
        }
        return $this->db->delete(TABLE_ORDERS_INVITES, $filter);
    }

    /**
     * Данные о предложениях по фильтру
     * @param array $filter
     * @param bool|array $fields false - только подсчитать количество
     * @param bool $oneArray
     * @return mixed
     */
    public function invitesDataByFilter(array $filter, $fields = false,  $oneArray = true)
    {
        $filter = $this->prepareFilter($filter, 'I');
        if (empty($fields)) {
            return (int)$this->db->one_data('SELECT COUNT(*) FROM ' . TABLE_ORDERS_INVITES . ' I ' . $filter['where'], $filter['bind']);
        } else {
            if ($oneArray) {
                return $this->db->one_array('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_ORDERS_INVITES . ' I ' . $filter['where'].' LIMIT 1', $filter['bind']);
            } else {
                return $this->db->select('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_ORDERS_INVITES . ' I ' . $filter['where'], $filter['bind']);
            }
        }
    }

    /**
     * Список предложений заказа исполнителям
     * @param array $users ID пользователей
     * @return array
     */
    public function orderInvitesList($users)
    {
        if ( empty($users)) return array();

        $data = $this->db->select_key('
            SELECT U.user_id, U.reg1_country, U.reg3_city, S.spec_id AS main_spec_id, 
                   U.login, U.name, U.surname, U.pro, U.avatar, U.verified, U.sex, US.last_activity
            FROM '.TABLE_USERS_STAT.' US,
                 '.TABLE_USERS.' U
                    LEFT JOIN '.TABLE_USERS_SPECIALIZATIONS.' S ON U.user_id = S.user_id AND S.main = 1 
            WHERE '.$this->db->prepareIN('U.user_id', $users).' AND U.user_id = US.user_id AND U.blocked = 0 AND U.deleted = 0
            ', 'user_id');
        $specs = array();
        foreach ($data as & $v) {
            if (Geo::countrySelect()) {
                $v['country_data'] = Geo::regionData($v['reg1_country']);
            }
            if ($v['reg3_city']) {
                $v['city_data'] = Geo::regionData($v['reg3_city']);
            }
            if ( ! in_array($v['main_spec_id'], $specs)) {
                $specs[] = $v['main_spec_id'];
            }
        } unset($v);
        if ( ! empty($specs)) {
            $specs = Specializations::model()->specializationsListingInArray($specs);
            foreach ($data as & $v) {
                if( ! isset($specs[ $v['main_spec_id'] ])) continue;
                $v['main_spec_title'] = $specs[ $v['main_spec_id'] ]['title'];
            } unset($v);
        }
        return $data;
    }

    # ----------------------------------------------------------------
    # Услуги

    /**
     * Данные об услугах (frontend)
     * @param integer $nTypeID ID типа Svc::type...
     * @return array
     */
    public function svcPromoteData($nTypeID)
    {
        if ($nTypeID == Svc::TYPE_SERVICE || empty($nTypeID))
        {
            $aData = $this->db->select_key('SELECT id, keyword, price, settings
                            FROM ' . TABLE_SVC . ' WHERE type = :type',
                'keyword', array(':type' => Svc::TYPE_SERVICE)
            );

            if (empty($aData)) return array();

            foreach ($aData as $k => $v) {
                $sett = func::unserialize($v['settings']); unset($v['settings']);
                $aData[$k] = array_merge($v, $sett);
            }

            return $aData;
        }
        elseif ($nTypeID == Svc::TYPE_SERVICEPACK)
        {
            $aData = $this->db->select('SELECT id, keyword, price, settings
                                FROM ' . TABLE_SVC . ' WHERE type = :type ORDER BY num',
                array(':type' => Svc::TYPE_SERVICEPACK)
            );

            foreach ($aData as $k => $v) {
                $sett = func::unserialize($v['settings']);
                unset($v['settings']);
                # оставляем текущую локализацию
                foreach ($this->langSvcPacks as $lngK => $lngV) {
                    $sett[$lngK] = (isset($sett[$lngK][LNG]) ? $sett[$lngK][LNG] : '');
                }
                $aData[$k] = array_merge($v, $sett);
            }

            return $aData;
        }
    }

    /**
     * Данные об услугах для формы, страницы продвижения
     * @return array
     */
    public function svcData()
    {
        $aFilter = array('module' => 'users');
        $aFilter = $this->prepareFilter($aFilter, 'S');

        $aData = $this->db->select_key('SELECT S.*
                                    FROM ' . TABLE_SVC . ' S
                                    ' . $aFilter['where']
            . ' ORDER BY S.type, S.num',
            'id', $aFilter['bind']
        );
        if (empty($aData)) return array();

        $oIcon = Orders::svcIcon();
        foreach ($aData as $k => &$v) {
            $v['id'] = intval($v['id']);
            $v['disabled'] = false;
            $sett = func::unserialize($v['settings']); unset($v['settings']);
            if (!empty($sett)) {
                $v = array_merge($sett, $v);
            }
            $v['title_view'] = (isset($v['title_view'][LNG]) ? $v['title_view'][LNG] : '');
            $v['description'] = (isset($v['description'][LNG]) ? $v['description'][LNG] : '');
            $v['description_full'] = (isset($v['description_full'][LNG]) ? $v['description_full'][LNG] : '');
            $v['icon_s'] = $oIcon->url($v['id'], $v['icon_s'], OrdersSvcIcon::SMALL);
            # исключаем выключенные услуги
            if (empty($v['on'])) unset($aData[$k]);
        }
        unset($v);

        return $aData;
    }

    public function svcCron()
    {
        $sNow = $this->db->now();

        # Деактивируем услугу "Выделение"
        $this->db->exec('UPDATE ' . TABLE_ORDERS . '
            SET svc = svc - :mark
            WHERE (svc & :mark) > 0 AND svc_marked_to <= :now',
            array(':now' => $sNow, ':mark' => Orders::SVC_MARK)
        );

        # Деактивируем услугу "Закрепление"
        $this->db->exec('UPDATE ' . TABLE_ORDERS . '
            SET svc = svc - :fix, svc_fixed_order = "0000-00-00 00:00:00"
            WHERE ( svc & :fix) > 0 AND svc_fixed_to <= :now',
            array(':now' => $sNow, ':fix' => Orders::SVC_FIX)
        );

    }

    /**
     * Сохранение настроек региональной стоимости услуг
     * @param integer $nSvcID ID услуг
     * @param array $aData данные
     */
    public function svcPriceExSave($nSvcID, array $aData)
    {
        if ($nSvcID <= 0) return;

        $sql = array();
        $id = 1;
        $num = 1;
        foreach ($aData as $v) {
            if ($v['price'] <= 0 || empty($v['regions'])) {
                continue;
            }
            $v['regions'] = array_unique($v['regions']);
            foreach ($v['regions'] as $region) {
                $sql[] = array(
                    'id'          => $id,
                    'svc_id'      => $nSvcID,
                    'price'       => $v['price'],
                    'region_id'   => $region,
                    'num'         => $num++,
                );
            }
            $id++;
        }

        $this->db->delete(TABLE_ORDERS_SVC_PRICE, array('svc_id' => $nSvcID));
        if (!empty($sql)) {
            foreach (array_chunk($sql, 25) as $v) {
                $this->db->multiInsert(TABLE_ORDERS_SVC_PRICE, $v);
            }
        }
    }

    /**
     * Загрузка настроек региональной стоимости услуг, для редактирования
     * @return array
     */
    public function svcPriceExEdit()
    {
        $aResult = array();
        $aData = $this->db->select('SELECT * FROM ' . TABLE_ORDERS_SVC_PRICE . ' ORDER BY svc_id, id, num');
        if (!empty($aData)) {
            $aRegionsID = array();
            foreach ($aData as $v) {
                if (!isset($aResult[$v['svc_id']])) {
                    $aResult[$v['svc_id']] = array();
                }
                if (!isset($aResult[$v['svc_id']][$v['id']])) {
                    $aResult[$v['svc_id']][$v['id']] = array(
                        'price'   => $v['price'],
                        'regions' => array()
                    );
                }
                if ($v['region_id'] > 0) {
                    $aResult[$v['svc_id']][$v['id']]['regions'][] = $v['region_id'];
                    $aRegionsID[] = $v['region_id'];
                }
            }

            $aRegionsID = array_unique($aRegionsID);
            $aLvl = array(Geo::lvlRegion, Geo::lvlCity);
            $bCountries = Geo::countrySelect();
            if($bCountries){
                $aLvl[] = Geo::lvlCountry;
            }
            $aRegionsData = Geo::model()->regionsList($aLvl, array('id' => $aRegionsID));
            if($bCountries){
                $aCountries = Geo::countryList();
            }

            foreach ($aResult as &$v) {
                foreach ($v as &$vv) {
                    $vv['regions'] = array_unique($vv['regions']);
                    $regionsResult = array();
                    foreach ($vv['regions'] as $id) {
                        if (isset($aRegionsData[$id])) {
                            if($bCountries){
                                $r = $aRegionsData[$id];
                                if($r['numlevel'] == Geo::lvlCountry){
                                    $t = $r['title'];
                                }else{
                                    $t = $aCountries[ $r['country'] ]['title'].' / '.$r['title'];
                                }
                            }else{
                                $t = $aRegionsData[$id]['title'];
                            }
                            $regionsResult[] = array('id' => $id, 't' => $t);
                        }
                    }
                    $vv['regions'] = $regionsResult;
                }
                unset($vv);
            }
            unset($v);
        }

        return $aResult;
    }

    /**
     * Получение региональной стоимости услуги в зависимости от города
     * @param array $svcID ID услуг
     * @param integer $cityID ID города
     * @return array - региональной стоимость услуг для указанного региона
     */
    public function svcPricesEx(array $svcID, $cityID)
    {
        if (empty($svcID) || !$cityID) return array();
        $result = array_fill_keys($svcID, 0);

        $cityData = Geo::regionData($cityID);
        if (empty($cityData) || !Geo::isCity($cityData) || !$cityData['pid']) return $result;

        # получаем доступные варианты региональной стоимости услуг
        $prices = $this->db->select('SELECT * FROM ' . TABLE_ORDERS_SVC_PRICE . '
                    WHERE ' . $this->db->prepareIN('svc_id', $svcID) . '
                    ORDER BY num');
        if (empty($prices)) return array();

        foreach ($svcID as $id) {
            # город
            foreach ($prices as $v) {
                if ($v['svc_id'] == $id && $v['region_id'] == $cityID) {
                    $result[$id] = $v['price'];
                    continue 2;
                }
            }
            # регион(область)
            foreach ($prices as $v) {
                if ($v['svc_id'] == $id && $v['region_id'] == $cityData['pid']) {
                    $result[$id] = $v['price'];
                    continue 2;
                }
            }
            # страна
            foreach ($prices as $v) {
                if ($v['svc_id'] == $id && $v['region_id'] == $cityData['country']) {
                    $result[$id] = $v['price'];
                    continue 2;
                }
            }
        }

        return $result;
    }

    /**
     * Статистика
     * @return array
     */
    public function statisticData()
    {
        $data = array();

        # заказов
        $filter = array(
            'status' => array(Orders::STATUS_OPENED),
            'visibility' => Orders::VISIBILITY_ALL,
        );
        if (Orders::searchClosed()) {
            $filter['status'][] = Orders::STATUS_CLOSED;
        }
        if (Orders::premoderation()) {
            $filter[':moderated'] = 'O.moderated > 0';
        }
        $filter[':user'] = 'O.user_id = U.user_id AND U.blocked = 0 AND U.deleted = 0 ';
        $filter = $this->prepareFilter($filter, 'O');
        $data['orders'] = (int)$this->db->one_data(' SELECT COUNT(O.id) FROM '.TABLE_ORDERS.' O, '.TABLE_USERS.' U '.$filter['where'], $filter['bind']);

        return $data;
    }

    # ----------------------------------------------------------------
    # Заметки пользователя о заказе

    /**
     * Данные заметки пользователя о заказе
     * @param integer $nID id заметки или id автора заметки
     * @param integer $nOrderID id заказа для заметки (0 - если указан id заметки)
     * @return array|mixed
     */
    public function noteData($nID, $nOrderID = 0)
    {
        if ($nOrderID) {
            $aFilter = array(
                'author_id' => $nID,
                'order_id'  => $nOrderID,
            );
        } else {
            $aFilter = array(
                'id' => $nID,
            );
        }

        $aFilter = $this->prepareFilter($aFilter);

        return $this->db->one_array('SELECT * 
                                     FROM ' . TABLE_ORDERS_NOTES .
                                     $aFilter['where'],
                          $aFilter['bind']);
    }

    /**
     * Сохранение заметки
     * @param integer $nNoteID id заметки
     * @param array $aData данных заметки
     * @return bool|int
     */
    public function noteSave($nNoteID, array $aData)
    {
        if ($nNoteID) {
            return $this->db->update(TABLE_ORDERS_NOTES, $aData, array('id' => $nNoteID));
        } else {
            return $this->db->insert(TABLE_ORDERS_NOTES, $aData);
        }
    }

    /**
     * Удаление заметки
     * @param $nNoteID - id заметки
     * @return bool
     */
    public function noteDelete($nNoteID)
    {
        return $this->db->delete(TABLE_ORDERS_NOTES, array('id' => $nNoteID));
    }

}