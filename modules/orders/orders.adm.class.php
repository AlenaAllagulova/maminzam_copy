<?php

/**
 * Права доступа группы:
 *  - orders: Заказы
 *      - orders-manage: Управление заказами (список, добавление, редактирование, удаление)
 *      - orders-moderate: Модерация заказов
 *      - offers: Управление заявками (список, удаление)
 *      - offers-moderate: Модерация заявок
 *      - tags: Управление тегами (список, добавление, редактирование, удаление)
 *      - svc: Управление услугами
 *      - settings: Настройки
 *      - seo: SEO
 */
class Orders_ extends OrdersBase
{
    public function orders_list()
    {
        if (!($this->haveAccessTo('orders-manage') || $this->haveAccessTo('orders-moderate'))) {
            return $this->showAccessDenied();
        }

        $accessManage = $this->haveAccessTo('orders-manage');

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'add': # добавление
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateOrderData(0, $bSubmit);
                    if ($bSubmit) {

                        $aData['status_prev'] = self::STATUS_NOTACTIVATED;
                        $aData['status'] = self::STATUS_OPENED;
                        $aData['moderated'] = 1;
                        if ($this->errors->no('orders.admin.order.submit',array('id'=>0,'data'=>&$aData))) {
                            $nOrderID = $this->model->orderSave(0, $aData, 'd');
                            if ($nOrderID > 0) {
                                if (static::imagesLimit()) {
                                    $this->orderImages($nOrderID)->saveTmp('img');
                                }
                                if (static::attachmentsLimit()) {
                                    $this->orderAttachments($nOrderID)->saveTmp('attach');
                                }

                                # теги
                                $this->orderTags()->tagsSave($nOrderID);

                                $this->security->userCounter('orders', 1, $aData['user_id']);

                                # уведомление исполнителей о новом заказе
                                $this->enotifyNewOrder($nOrderID);
                            }
                        }
                    }

                    $aData['id'] = 0;
                    $aData['catsOptions'] = (static::useProducts() ? Shop::model()->categoriesOptionsByLevel(Shop::model()->categoryParentsID(0), array('empty' => 'Выбрать')) : array());
                    $aData['specsOptions'] = Specializations::model()->specializationsOptions(0, 0, array(
                            'empty'     => 'Выбрать',
                            'emptyCats' => array('empty' => 'Выбрать'),
                            'noAllSpec' => 1
                        )
                    );
                    if(Geo::defaultCity()){
                        $aData['regions'][] = [
                            'reg3_city'     => Geo::defaultCity(),
                            'title'         => Geo::regionTitle(Geo::defaultCity()),
                            'country_title' => Geo::regionTitle(Geo::defaultCountry())
                        ];
                    }
                    $aData['act'] = $sAct;
                    $aData['images'] = array();
                    $aData['imgcnt'] = 0;
                    $aData['attachcnt'] = 0;
                    if (static::imagesLimit()) {
                        $aData['img'] = $this->orderImages(0);
                    }
                    if (static::attachmentsLimit()) {
                        $aData['attach'] = $this->orderAttachments(0);
                    }
                    $aData['offers'] = array();
                    $aData['types'] = self::aTypes();
                    $aData['service_types'] = self::aServiceTypes();

                    $aData['priceForm'] = $this->viewPHP($aData, 'admin.orders.form.price');
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.orders.form');
                }
                break;
                case 'edit': # редактирование
                {

                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nOrderID = $this->input->postget('id', TYPE_UINT);
                    if (!$nOrderID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {
                        if (!$accessManage) {
                            $this->errors->accessDenied(); break;
                        }

                        $aData = $this->validateOrderData($nOrderID, $bSubmit);
                        if ($this->errors->no('orders.admin.order.submit',array('id'=>$nOrderID,'data'=>&$aData))) {
                            $this->model->orderSave($nOrderID, $aData, 'd');
                            # теги
                            $this->orderTags()->tagsSave($nOrderID);
                        }
                        $aData['id'] = $nOrderID;
                    }
                    $aData = $this->model->orderData($nOrderID, array(), true);
                    if (empty($aData)) {
                        $this->errors->unknownRecord();
                        break;
                    }
                    $aData['catsOptions'] = (static::useProducts() ? Shop::model()->categoriesOptionsByLevel(Shop::model()->categoryParentsID(0), array('empty' => 'Выбрать')) : array());
                    $aData['specsOptions'] = Specializations::model()->specializationsOptions(0, 0, array(
                            'empty'     => 'Выбрать',
                            'emptyCats' => array('empty' => 'Выбрать'),
                            'noAllSpec' => 1
                        )
                    );

                    $aData['act'] = $sAct;
                    # формируем данные об изображениях
                    if (static::imagesLimit()) {
                        $aData['img'] = $this->orderImages($nOrderID);
                        $aData['images'] = $aData['img']->getData($aData['imgcnt']);
                    }
                    # формируем данные об файлах
                    if (static::attachmentsLimit()) {
                        $aData['attach'] = $this->orderAttachments($nOrderID);
                        $aData['attachments'] = $aData['attach']->getData($aData['attachcnt']);
                    }
                    $aData['offers'] = $this->model->offersListing(array('order_id' => $nOrderID), false, '', 'O.created');
                    if (!empty($aData['performer_id'])) {
                        $aPerformer = Users::model()->userData($aData['performer_id'], array('email'));
                        if (!empty($aPerformer)) {
                            $aData['performer_email'] = $aPerformer['email'];
                        }
                    }
                    switch ($aData['type']) {
                        case static::TYPE_SERVICE: {
                            $aFirstSpec = reset($aData['specs']);
                            if (static::dynpropsEnabled()) {
                                $aData['dp'] = Specializations::i()->dpOrdersForm($aFirstSpec['spec_id'], false, $aData);
                            } else {
                                $aData['dp'] = Specializations::i()->dpForm($aFirstSpec['spec_id'], false, $aData);
                            }
                        } break;
                        case static::TYPE_PRODUCT: {
                            $aFirstCat = reset($aData['cats']);
                            $aData['dp'] = (static::useProducts() ? Shop::i()->dpForm($aFirstCat['cat_id'], false, $aData) : '');
                        } break;
                    }
                    $aData['types'] = self::aTypes();
                    $aData['service_types'] = self::aServiceTypes();
                    if(empty($aData['regions']) && Geo::defaultCity()){
                        $aData['regions'][] = [
                            'reg3_city'     => Geo::defaultCity(),
                            'title'         => Geo::regionTitle(Geo::defaultCity()),
                            'country_title' => Geo::regionTitle(Geo::defaultCountry())
                        ];
                    }
                    $aData['priceForm'] = $this->viewPHP($aData, 'admin.orders.form.price');
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.orders.form');
                    if (Request::isAJAX()) $this->ajaxResponseForm($aResponse, 2, true);
                }
                break;
                case 'toggle': # переключатели
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }

                    $nOrderID = $this->input->postget('id', TYPE_UINT);
                    if (!$nOrderID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $sToggleType = $this->input->get('type', TYPE_STR);
                    $this->model->orderToggle($nOrderID, $sToggleType);
                }
                break;
                case 'delete': # удаление
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }

                    $nOrderID = $this->input->postget('id', TYPE_UINT);
                    if (!$nOrderID) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->orderDelete($nOrderID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    }
                }
                break;
                case 'user': # autocomplete: пользователь
                {
                    $sEmail = $this->input->post('q', TYPE_NOTAGS);
                    $sEmail = $this->input->cleanSearchString($sEmail);
                    $aFilter = array(
                        ':email'    => array(
                            (Users::model()->userEmailCrypted() ? 'BFF_DECRYPT(email)' : 'email') . ' LIKE :email',
                            ':email' => $sEmail . '%'
                        ),
                        'blocked'   => 0,
                        'activated' => 1,
                    );
                    if (Users::useClient()) {
                        $aFilter['type'] = Users::TYPE_CLIENT;
                    }
                    $aUsers = Users::model()->usersList($aFilter, array('user_id', 'email'));
                    $this->autocompleteResponse($aUsers, 'user_id', 'email');
                }
                break;
                case 'info': # краткая информация о заказе (popup)
                {
                    $nOrderID = $this->input->getpost('id', TYPE_UINT);
                    if (!$nOrderID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $aData = $this->model->orderData($nOrderID, array(
                            'id',
                            'user_id',
                            'created',
                            'status',
                            'status_prev',
                            'status_changed',
                            'blocked_reason',
                            'moderated',
                            'title',
                            'visibility',
                            'fairplay',
                        )
                    );
                    if (empty($aData)) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $aData['user'] = Users::model()->userData($aData['user_id'], array('email', 'name', 'blocked'));
                    echo $this->viewPHP($aData, 'admin.orders.info');
                    exit;
                }
                break;
                case 'img-upload': # изображения: загрузка
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied();
                        $this->ajaxResponse(array('success' => false, 'errors' => $this->errors->get()), true, false, true);
                    }


                    $nOrderID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->orderImages($nOrderID);
                    $mResult = $oImages->uploadQQ();
                    $aResponse = array('success' => ($mResult !== false && $this->errors->no()));

                    if ($mResult !== false) {
                        $aResponse = array_merge($aResponse, $mResult);
                        $aResponse = array_merge($aResponse,
                            $oImages->getURL($mResult,
                                array(OrdersOrderImages::szSmall, OrdersOrderImages::szView),
                                empty($nOrderID)
                            )
                        );
                    }
                    $aResponse['errors'] = $this->errors->get();
                    $this->ajaxResponse($aResponse, true, false, true);
                }
                break;
                case 'img-saveorder': # изображения: сохранение порядка
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }


                    $nOrderID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->orderImages($nOrderID);
                    $img = $this->input->post('img', TYPE_ARRAY);
                    if (!$oImages->saveOrder($img, false, true)) {
                        $this->errors->impossible();
                    }
                }
                break;
                case 'img-delete': # изображения: удаление
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }


                    $nOrderID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->orderImages($nOrderID);
                    $nImageID = $this->input->post('image_id', TYPE_UINT);
                    $sFilename = $this->input->post('filename', TYPE_NOTAGS);
                    if (!$nImageID && empty($sFilename)) {
                        $this->errors->impossible();
                        break;
                    }
                    if ($nImageID) {
                        $oImages->deleteImage($nImageID);
                    } else {
                        $oImages->deleteTmpFile($sFilename);
                    }
                }
                break;
                case 'img-delete-all': # изображения: удаление всех изображений
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }


                    $nOrderID = $this->input->postget('id', TYPE_UINT);
                    $oImages = $this->orderImages($nOrderID);
                    if ($nOrderID) {
                        $oImages->deleteAllImages(true);
                    } else {
                        $sFilename = $this->input->post('filenames', TYPE_ARRAY_STR);
                        $oImages->deleteTmpFile($sFilename);
                    }
                }
                break;
                case 'attach-upload': # файлы: загрузка
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied();
                        $this->ajaxResponse(array('success' => false, 'errors' => $this->errors->get()), true, false, true);
                    }


                    $nOrderID = $this->input->postget('id', TYPE_UINT);
                    $oAttachments = $this->orderAttachments($nOrderID);
                    $mResult = $oAttachments->uploadQQ();
                    $aResponse = array('success' => ($mResult !== false && $this->errors->no()));

                    if ($mResult !== false) {
                        $aResponse = array_merge($aResponse, $mResult);
                        $aResponse = array_merge($aResponse, array(
                                'url'  => $oAttachments->getURL($mResult, empty($nOrderID)),
                                'size' => tpl::filesize($mResult['size']),
                            )
                        );
                    }
                    $aResponse['errors'] = $this->errors->get();
                    $this->ajaxResponse($aResponse, true, false, true);
                }
                break;
                case 'attach-saveorder': # файлы: сохранение порядка
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }


                    $nOrderID = $this->input->postget('id', TYPE_UINT);
                    $oAttachments = $this->orderAttachments($nOrderID);
                    $attach = $this->input->post('attach', TYPE_ARRAY);
                    if (!$oAttachments->saveOrder($attach, false, true)) {
                        $this->errors->impossible();
                    }
                }
                break;
                case 'attach-delete': # файлы: удаление
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }


                    $nOrderID = $this->input->postget('id', TYPE_UINT);
                    $oAttachments = $this->orderAttachments($nOrderID);
                    $nAttachID = $this->input->post('attach_id', TYPE_UINT);
                    $sFilename = $this->input->post('filename', TYPE_NOTAGS);
                    if (!$nAttachID && empty($sFilename)) {
                        $this->errors->impossible();
                        break;
                    }
                    if ($nAttachID) {
                        $oAttachments->deleteAttach($nAttachID);
                    } else {
                        $oAttachments->deleteTmpFile($sFilename);
                    }
                }
                break;
                case 'attach-delete-all': # файлы: удаление всех файлов
                {
                    if (!$accessManage) {
                        $this->errors->accessDenied(); break;
                    }


                    $nOrderID = $this->input->postget('id', TYPE_UINT);
                    $oAttachments = $this->orderAttachments($nOrderID);
                    if ($nOrderID) {
                        $oAttachments->deleteAllAttachments(true);
                    } else {
                        $sFilename = $this->input->post('filenames', TYPE_ARRAY_STR);
                        $oAttachments->deleteTmpFile($sFilename);
                    }
                }
                break;
                case 'price-block':
                {
                    $nSpecID = $this->input->post('spec_id', TYPE_UINT);
                    if (!$nSpecID) {
                        break;
                    }
                    $aData = array(
                        'spec'       => array('spec_id' => $nSpecID),
                        'price'      => 0,
                        'price_ex'   => 0,
                        'price_rate' => 0,
                        'price_curr' => 0,
                    );
                    $aResponse['html'] = $this->viewPHP($aData, 'admin.orders.form.price');
                }
                break;
                case 'mass-approve':
                {
                    if ( ! $this->haveAccessTo('orders-moderate')) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $ids = $this->input->post('i', TYPE_ARRAY_UINT);
                    if(empty($ids)){
                        $this->errors->unknownRecord();
                        break;
                    }
                    $items = $this->model->ordersDataByFilter(array('id' => $ids), array('id', 'status', 'user_id', 'title', 'keyword'));
                    if (empty($items)) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $cnt = 0;
                    foreach($items as $v){
                        $this->order_approve($v['id'], $v);
                        if( ! $this->errors->no()){
                            break;
                        }
                        $cnt++;
                    }
                    $aResponse = array(
                        'updated'    => $cnt,
                    );
                }
                break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        # формируем фильтр списка заказов
        $f = $this->input->postgetm(array(
            'page'    => TYPE_UINT,
            'tab'     => TYPE_UINT,
            'title'   => TYPE_NOTAGS,
            'country' => TYPE_UINT,
            'region'  => TYPE_UINT,
            'type'    => TYPE_UINT,
            'cats'    => TYPE_ARRAY_UINT,
            'specs'   => TYPE_ARRAY_UINT,
            'user'    => TYPE_NOTAGS,
        ));
        $sql = array();

        switch ($f['tab']) {
            case 0: # Открытые
            {
                $sql['status'] = static::STATUS_OPENED;
                if (!static::premoderation()) {
                    $sql[':mod'] = 'O.moderated > 0';
                }
            }
            break;
            case 1: # Закрытые
            {
                $sql['status'] = static::STATUS_CLOSED;
            }
            break;
            case 2: # На модерации
            {
                $sql[':mod'] = 'O.moderated != 1';
            }
            break;
            case 3: # Заблокированные
            {
                $sql['status'] = static::STATUS_BLOCKED;
            }
            break;
            case 4: # Не активированные
            {
                $sql['status'] = static::STATUS_NOTACTIVATED;
            }
            break;
            case 5: # Все
            {
            }
            break;
        }

        $aTypes = static::aTypes();
        if (sizeof($aTypes) == 1) {
            $f['type'] = key($aTypes);
        }
        if (array_key_exists($f['type'], $aTypes)) {
            $sql[':type'] = array('O.type = :type', ':type' => $f['type']);
        }

        if (!empty($f['title'])) {
            if (is_numeric($f['title'])) {
                $sql[':title'] = array('O.id = :id', ':id' => $f['title']);
            } else {
                $sql[':title'] = $this->db->prepareFulltextQuery($f['title'], 'O.title,O.descr');
            }
        }

        if( ! empty($f['country'])){
            $sql[':country'] = array('(R.reg1_country = :country)', ':country' => $f['country']);
        }

        if (!empty($f['region'])) {
            $sql[':region'] = array('(R.reg3_city = :region OR R.reg2_region = :region)', ':region' => $f['region']);
        }

        if (!empty($f['cats']) && $f['type'] == static::TYPE_PRODUCT && static::useProducts()) {
            $sql[':cats'] = $this->db->prepareIN('C.cat_id', $f['cats']);

            if (!Request::isAJAX()) {
                $f['cats'] = Shop::model()->categoriesListingInArray($f['cats']);
                foreach ($f['cats'] as $k => $v) {
                    if ($v['numlevel'] == 2) {
                        $f['cats'][$k]['title'] = $v['pid_title'] . ' / ' . $v['title'];
                    }
                }
            }
        }

        if (!empty($f['specs']) && $f['type'] == static::TYPE_SERVICE) {
            $sql[':specs'] = $this->db->prepareIN('S.spec_id', $f['specs']);

            if (!Request::isAJAX()) {
                $f['specs'] = Specializations::model()->specializationsListingInArray($f['specs']);
            }
        }

        if (!empty($f['user'])) {
            $sql[':user'] = array(
                '(O.user_id = ' . intval($f['user']) . ' OR U.email LIKE :user OR U.login LIKE :user)',
                ':user' => '%' . $f['user'] . '%'
            );
        }

        $aData['f'] = &$f;

        $nCount = $this->model->ordersListing($sql, true);
        $oPgn = new Pagination($nCount, 15, '#', 'jOrdersOrdersList.page('.Pagination::PAGE_ID.'); return false;');
        $oPgn->pageNeighbours = 6;
        $aData['pgn'] = $oPgn->view(array('arrows'=>false));
        $aData['list'] = $this->model->ordersListing($sql, false, $oPgn->getLimitOffset(), 'id DESC');
        $aData['list'] = $this->viewPHP($aData, 'admin.orders.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'list' => $aData['list'],
                    'pgn'  => $aData['pgn'],
                )
            );
        }

        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;
        $aData['catsOptions'] = (static::useProducts() ? Shop::model()->categoriesOptionsByLevel(Shop::model()->categoryParentsID(0), array('empty' => 'Все категории')) : array());
        $aData['specsOptions'] = Specializations::model()->specializationsOptions(0, 0, array('empty' => 'Все специализации'));

        return $this->viewPHP($aData, 'admin.orders.listing');
    }

    public function orders_status()
    {
        if ( ! $this->haveAccessTo('orders-moderate')) {
            $this->errors->accessDenied();
        }


        $orderID = $this->input->post('id', TYPE_UINT);
        if (!$orderID) {
            $this->errors->unknownRecord();
        } else {
            $aData = $this->model->orderData($orderID);
            if (empty($aData)) {
                $this->errors->unknownRecord();
            }
        }
        if ( ! $this->errors->no()) {
            $this->ajaxResponseForm();
        }

        $aResponse = array();
        switch ($this->input->postget('act', TYPE_STR))
        {
            case 'activate': # модерация: активация
            {
                if ($aData['status'] != self::STATUS_NOTACTIVATED) {
                    $this->errors->impossible(); break;
                }

                # данные о владельце заказа
                $aUserData = Users::model()->userData($aData['user_id'], array('user_id','name','email','activated','blocked','password_salt'));
                if (empty($aUserData) || !empty($aUserData['blocked'])) {
                    $this->errors->impossible(); break;
                }

                $aUpdate = array(
                    'activate_key' => '', # чистим ключ активации
                    'status_prev'  => self::STATUS_NOTACTIVATED,
                    'status'       => self::STATUS_OPENED,
                    'moderated'    => 1, # отмечаем как промодерированный
                );

                $res = $this->model->orderSave($orderID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                } else {
                    # активируем аккаунт пользователя
                    if (empty($aUserData['activated'])) {
                        $nUserID = $aData['user_id'];
                        $sPassword = func::generator(12); # генерируем новый пароль
                        $aUserData['password'] = $this->security->getUserPasswordMD5($sPassword, $aUserData['password_salt']);
                        $bSuccess = Users::model()->userSave($nUserID, array(
                                'activated'    => 1,
                                'activate_key' => '',
                                'password'     => $aUserData['password'],
                            )
                        );
                        if ($bSuccess) {
                            # триггер активации аккаунта
                            bff::i()->callModules('onUserActivated', array($nUserID));
                            # отправляем письмо об успешной автоматической регистрации
                            bff::sendMailTemplate(array(
                                    'name'     => $aUserData['name'],
                                    'email'    => $aUserData['email'],
                                    'password' => $sPassword
                                ),
                                'users_register_auto', $aUserData['email']
                            );
                        }
                    }
                    # обновляем счетчик заказов "на модерации"
                    $this->moderationCounterUpdate();
                }
            }
            break;
            case 'approve': # модерация: одобрение
            {
                if ($aData['status'] == self::STATUS_NOTACTIVATED) {
                    $this->errors->impossible();
                    break;
                }
                $this->order_approve($orderID, $aData);
            }
            break;
            case 'close': # модерация: закрытие
            {
                if ($aData['status'] != self::STATUS_OPENED) {
                    $this->errors->impossible();
                    break;
                }

                $aUpdate = array(
                    'status_prev = status',
                    'status'    => self::STATUS_CLOSED,
                    'moderated' => 1,
                );

                $res = $this->model->orderSave($orderID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                }
                # обновляем счетчик заказов "на модерации"
                $this->moderationCounterUpdate();

            }
            break;
            case 'open': # модерация: открытие
            {
                if ($aData['status'] != self::STATUS_CLOSED) {
                    $this->errors->impossible();
                    break;
                }

                $aUpdate = array(
                    'status_prev = status',
                    'status'    => self::STATUS_OPENED,
                    'moderated' => 1,
                );

                # Срок приема ответов
                if (static::termsEnabled()) {
                    $aUpdate['expire'] = $this->termExpire($aData['term']);
                }

                $res = $this->model->orderSave($orderID, $aUpdate);
                if (empty($res)) {
                    $this->errors->impossible();
                }

                # обновляем счетчик заказов "на модерации"
                $this->moderationCounterUpdate();

            }
            break;
            case 'block': # модерация: блокировка
            {
                /**
                 * Блокировка заказа (если уже заблокирован => изменение причины блокировки)
                 * @param string 'blocked_reason' причина блокировки
                 * @param integer 'id' ID заказа
                 */

                $bUnblock = $this->input->post('unblock', TYPE_UINT);
                $sBlockedReason = $this->input->postget('blocked_reason', TYPE_NOTAGS, array('len' => 1000, 'len.sys' => 'orders.blocked_reason.limit'));
                $bBlocked = ($aData['status'] == self::STATUS_BLOCKED);

                $aUpdate = array(
                    'moderated'      => 1,
                    'blocked_reason' => $sBlockedReason,
                );

                $bBlockedResult = $bBlocked;
                if (!$bBlocked) { # блокируем
                    $aUpdate['status_prev'] = $aData['status'];
                    $aUpdate['status'] = self::STATUS_BLOCKED;
                    $bBlockedResult = true;

                    # Отправим письмо заказчику
                    Users::sendMailTemplateToUser($aData['user_id'], 'orders_order_blocked', array(
                        'order_id' => $orderID,
                        'order_title' => $aData['title'],
                        'order_url' => static::url('view', array('id'=>$orderID, 'keyword'=>$aData['keyword'])),
                        'blocked_reason' => nl2br($sBlockedReason),
                    ), Users::ENOTIFY_GENERAL);
                } else {
                    if ($bUnblock) {
                        # разблокируем
                        switch ($aData['status_prev']) {
                            case 0:
                            case self::STATUS_BLOCKED:
                                $aUpdate['status'] = self::STATUS_CLOSED;
                            break;
                            default:
                                $aUpdate['status'] = $aData['status_prev'];
                            break;
                        }
                        $aUpdate['status_prev'] = self::STATUS_BLOCKED;
                        $bBlockedResult = false;
                        $this->enotifyNewOrder($orderID);

                        # Отправим письмо заказчику
                        Users::sendMailTemplateToUser($aData['user_id'], 'orders_order_approved', array(
                            'order_id' => $orderID,
                            'order_title' => $aData['title'],
                            'order_url' => static::url('view', array('id'=>$orderID, 'keyword'=>$aData['keyword'])),
                        ), Users::ENOTIFY_GENERAL);
                    }
                }

                $res = $this->model->orderSave($orderID, $aUpdate);
                if (!$res) {
                    $this->errors->impossible();
                    break;
                }
                # обновляем счетчик заказов "на модерации"
                $this->moderationCounterUpdate();

                $aResponse['blocked'] = $bBlockedResult;
                $aResponse['reason'] = $sBlockedReason;
            }
            break;
        }

        if ($this->errors->no()) {
            $aData = $this->model->orderData($orderID);
            if ( ! empty($aData)) {
                $aData['only_form'] = true;
                $aData['is_popup'] = $this->input->post('is_popup', TYPE_BOOL);
                $aData['user'] = Users::model()->userData($aData['user_id'], array('blocked'));
                $aResponse['html'] = $this->viewPHP($aData, 'admin.orders.form.status');
            }
        }
        $this->ajaxResponseForm($aResponse);
    }

    protected function order_approve($orderID, $data)
    {
        $aUpdate = array(
            'moderated' => 1
        );

        if ($data['status'] == self::STATUS_BLOCKED) {
            /**
             * В случае если "Одобряем" заблокированный заказ
             * => значит он после блокировки был отредактирован пользователем
             * => следовательно открываем
             */
            $newStatus = self::STATUS_OPENED;
            $aUpdate[] = 'status_prev = status';
            $aUpdate['status'] = $newStatus;
        }

        $res = $this->model->orderSave($orderID, $aUpdate);
        if (empty($res)) {
            $this->errors->impossible();
        }

        if (static::premoderation()) {
            # уведомление исполнителей о новом заказе
            $this->enotifyNewOrder($orderID);
        }

        # обновляем счетчик заказов "на модерации"
        $this->moderationCounterUpdate();

        # отправим письмо заказчику
        Users::sendMailTemplateToUser($data['user_id'], 'orders_order_approved', array(
            'order_id' => $orderID,
            'order_title' => $data['title'],
            'order_url' => static::url('view', array('id'=>$orderID, 'keyword'=>$data['keyword'])),
        ), Users::ENOTIFY_GENERAL);

    }

    public function offers_list()
    {
        if (!($this->haveAccessTo('offers') || $this->haveAccessTo('offers-moderate'))) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = array();
            $bAccessManage = $this->haveAccessTo('offers');
            $bAccessModerate = $this->haveAccessTo('offers-moderate');
            switch ($sAct) {
                case 'delete':
                {
                    if (!$bAccessManage) {
                        $this->errors->accessDenied(); break;
                    }

                    $nOfferID = $this->input->postget('id', TYPE_UINT);
                    if (!$nOfferID) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->offerDelete($nOfferID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    }
                }
                break;
                case 'approve':
                {
                    if (!$bAccessModerate) {
                        $this->errors->accessDenied();
                        break;
                    }

                    $nOfferID = $this->input->postget('id', TYPE_UINT);
                    if (!$nOfferID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->offerData($nOfferID, array('order_id'));

                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $aUpdate = array(
                        'moderated' => 1
                    );

                    $res = $this->model->offerSave($nOfferID, $aUpdate);
                    if (empty($res)) {
                        $this->errors->impossible();
                    }
                    # обновляем счетчик заявок "на модерации"
                    $this->moderationOffersCounterUpdate();

                }
                break;
                case 'dev-offers-counters-update': # пересчет счетчиков заявок к заказам
                {
                    if (!FORDEV) {
                        return $this->showAccessDenied();
                    }

                    $this->model->reCalcOffersCnt();
                    $this->moderationCounterUpdate();
                    $this->moderationOffersCounterUpdate();

                    $this->adminRedirect(($this->errors->no() ? Errors::SUCCESS : Errors::IMPOSSIBLE), bff::$event);
                } break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = array();
        $this->input->postgetm(array(
                'page'    => TYPE_UINT,
                'tab'     => TYPE_UINT,
                'perpage' => TYPE_UINT,
                'user'    => TYPE_NOTAGS,
            ), $f
        );

        # формируем фильтр списка заявок
        $sql = array();
        $sqlOrder = 'O.created DESC';
        $aData['pgn'] = '';
        $aData['f'] = $f;

        switch ($f['tab']) {
            case 1: # Все
            {
            }
            break;
            default: # На модерации
            {
                $sql[':mod'] = 'O.moderated != 1';
            }
            break;
        }

        if (!empty($f['user'])) {
            $sql[':user'] = array(
                '(U.user_id = ' . intval($f['user']) . ' OR U.email LIKE :user OR U.login LIKE :user)',
                ':user' => '%' . $f['user'] . '%'
            );
        }

        $nCount = $this->model->offersListing($sql, true);
        $aPerpage = $this->preparePerpage($f['perpage'], array(20, 40, 60));
        $sFilter = http_build_query($f);
        unset($f['page']);
        $oPgn = new Pagination($nCount, $f['perpage'], $this->adminLink("offers_list&$sFilter&page=" . Pagination::PAGE_ID));
        $aData['pgn'] = $oPgn->view();

        $aData['offers'] = ($nCount > 0 ?
            $this->model->offersListing($sql, false, $oPgn->getLimitOffset(), $sqlOrder) :
            array());
        $aData['perpage'] = $aPerpage;

        return $this->viewPHP($aData, 'admin.offers.listing');
    }

    #---------------------------------------------------------------------------------------
    # теги

    public function tags()
    {
        if (!$this->haveAccessTo('tags')) {
            return $this->showAccessDenied();
        }

        return $this->orderTags()->manage();
    }

    public function ajax()
    {
        if (!$this->security->haveAccessToAdminPanel()) {
            return $this->showAccessDenied();
        }
        switch ($this->input->getpost('act', TYPE_STR)) {
            case 'tags-suggest': # autocomplete.fb
            {
                $sQuery = $this->input->postget('tag', TYPE_NOTAGS);
                $this->orderTags()->tagsAutocomplete($sQuery);
            }
            break;
            case 'tags-autocomplete': # autocomplete
            {
                $sQuery = $this->input->post('q', TYPE_NOTAGS);
                $this->orderTags()->tagsAutocomplete($sQuery);
            }
            break;
        }

        $this->ajaxResponse(Errors::IMPOSSIBLE);
    }

    # ------------------------------------------------------------------------------------------------------------------------------
    # Услуги

    public function svc_services()
    {
        if (!$this->haveAccessTo('svc')) {
            return $this->showAccessDenied();
        }

        $svc = Svc::model();

        if (Request::isPOST()) {
            $aResponse = array();

            switch ($this->input->getpost('act')) {
                case 'update':
                {

                    $nSvcID = $this->input->post('id', TYPE_UINT);
                    if (!$nSvcID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $aData = $svc->svcData($nSvcID, array('id', 'type', 'keyword'));
                    if (empty($aData) || $aData['type'] != Svc::TYPE_SERVICE) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $this->svcValidateData($nSvcID, Svc::TYPE_SERVICE, $aDataSave);

                    if ($this->errors->no('orders.admin.svc-service.submit',array('id'=>$nSvcID,'data'=>&$aDataSave,'before'=>$aData))) {
                        # загружаем иконки
                        $oIcon = self::svcIcon($nSvcID);
                        $oIcon->setAssignErrors(false);
                        foreach ($oIcon->getVariants() as $iconField => $v) {
                            $oIcon->setVariant($iconField);
                            $aIconData = $oIcon->uploadFILES($iconField, true, false);
                            if (!empty($aIconData)) {
                                $aDataSave[$iconField] = $aIconData['filename'];
                            } else {
                                if ($this->input->post($iconField . '_del', TYPE_BOOL)) {
                                    if ($oIcon->delete(false)) {
                                        $aDataSave[$iconField] = '';
                                    }
                                }
                            }
                        }

                        # сохраняем
                        $svc->svcSave($nSvcID, $aDataSave);
                    }

                }
                break;
                case 'reorder': # сортировка услуг
                {

                    $aSvc = $this->input->post('svc', TYPE_ARRAY_UINT);
                    $svc->svcReorder($aSvc, Svc::TYPE_SERVICE);
                }
                break;
                default:
                {
                    $this->errors->impossible();
                }
                break;
            }

            $this->iframeResponseForm($aResponse);
        }

        $aData = array(
            'svc' => $svc->svcListing(Svc::TYPE_SERVICE, $this->module_name),
        );

        # Подготавливаем данные о региональной стоимости услуг для редактирования
        $aData['price_ex'] = $this->model->svcPriceExEdit();
        return $this->viewPHP($aData, 'admin.svc.services');
    }

    /**
     * Проверка данных услуги
     * @param string $nSvcID ID услуги
     * @param integer $nType тип Svc::TYPE_
     * @param array $aData @ref проверенные данные
     */
    protected function svcValidateData($nSvcID, $nType, &$aData)
    {
        $aParams = array(
            'price' => TYPE_PRICE,
        );

        if ($nType == Svc::TYPE_SERVICE) {
            $aSettings = array(
                'on'     => TYPE_BOOL, # включена
            );

            switch($nSvcID){
                case static::SVC_MARK:
                    $aSettings['period'] = TYPE_UINT; # дней
                    break;
                case static::SVC_FIX:
                    $aSettings['per_day'] = TYPE_UINT; # цена за день
                    $aSettings['period']  = TYPE_UINT; # дней
                    break;
            }

            $aData = $this->input->postm($aParams);
            $aData['settings'] = $this->input->postm($aSettings);
            $this->input->postm_lang($this->model->langSvcServices, $aData['settings']);
            $aData['title'] = $aData['settings']['title_view'][LNG];

            switch($nSvcID){
                case static::SVC_MARK:
                case static::SVC_FIX:
                    if( ! $aData['settings']['period']){
                        $aData['settings']['period'] = 1;
                    }
                    break;
            }
            if (Request::isPOST()) {
                $priceEx = $this->input->post('price_ex', array(
                    TYPE_ARRAY_ARRAY,
                    'price'   => TYPE_PRICE,
                    'regions' => TYPE_ARRAY_UINT,
                ));
                $this->model->svcPriceExSave($nSvcID, $priceEx);
            }
        }
    }

    public function settings()
    {
        if (!$this->haveAccessTo('settings')) {
            return $this->showAccessDenied();
        }

        if (Request::isAJAX()) {
            $this->input->postm(array(
                'contacts_view'    => TYPE_UINT,
                'offers_limit'     => TYPE_UINT,
                'offers_limit_pro' => TYPE_UINT,
                'share_code'       => TYPE_STR,
                'orders_images_width_min'  => TYPE_UINT,
                'orders_images_width_max'  => TYPE_UINT,
                'orders_images_height_min' => TYPE_UINT,
                'orders_images_height_max' => TYPE_UINT,
                'offers_images_width_min'  => TYPE_UINT,
                'offers_images_width_max'  => TYPE_UINT,
                'offers_images_height_min' => TYPE_UINT,
                'offers_images_height_max' => TYPE_UINT,
                'enotify_neworders_type'     => TYPE_UINT,
                'enotify_neworders_user'     => TYPE_UINT,
                'enotify_neworders_begin'    => TYPE_NOTAGS,
                'enotify_neworders_interval' => TYPE_UINT,
            ), $aData);



            if ($aData['enotify_neworders_interval'] < 1) {
                $aData['enotify_neworders_interval'] = 1;
            }
            if ($aData['enotify_neworders_interval'] > 12) {
                $aData['enotify_neworders_interval'] = 12;
            }
            $time = strtotime($aData['enotify_neworders_begin']);
            if ($time === false) {
                $aData['enotify_neworders_begin'] = '06:00';
            } else {
                # оставляем минимум час до окончания суток
                if (intval(date('G', $time)) == 23 && intval(date('i', $time)) > 0) {
                    $aData['enotify_neworders_begin'] = '23:00';
                } else {
                    $aData['enotify_neworders_begin'] = date('H:i', $time);
                }
            }
            $aData = bff::filter('admin.orders.settings.save', $aData);
            
            $this->configSave($aData);
            $this->ajaxResponseForm();
        }

        $aData = $this->configLoad(array(
            'contacts_view'    => static::ordersContactsView(),
            'offers_limit'     => static::offersLimit(),
            'offers_limit_pro' => static::offersLimit(true),
            'orders_images_width_min'  => 500,
            'orders_images_width_max'  => 5000,
            'orders_images_height_min' => 500,
            'orders_images_height_max' => 5000,
            'offers_images_width_min'  => 500,
            'offers_images_width_max'  => 5000,
            'offers_images_height_min' => 500,
            'offers_images_height_max' => 5000,
            'enotify_neworders_type'     => static::ENOTIFY_NEWORDERS_NEVER,
            'enotify_neworders_user'     => static::ENOTIFY_RECEIVERS_ALL,
            'enotify_neworders_begin'    => '06:00',
            'enotify_neworders_interval' => 3,
        ));
        if( ! is_array($aData)) $aData = array();
        $aData = bff::filter('admin.orders.settings.data', $aData);

        return $this->viewPHP($aData, 'admin.settings');
    }

    public function settingsSystem(array &$options = array())
    {
        $aData = array('options'=>&$options);
        return $this->viewPHP($aData, 'admin.settings.sys');
    }
}