<?php

use mastiffApp\tamaranga\modules\orders\OrdersBase;

abstract class OrdersBase_ extends OrdersBase
    implements IModuleWithSvc
{
    # Тип заказа
    const TYPE_SERVICE = 1; # услуга
    const TYPE_PRODUCT = 2; # товар

    # Статус заказа
    const STATUS_NOTACTIVATED = 1; # не активировано
    const STATUS_OPENED       = 3; # открыт
    const STATUS_CLOSED       = 4; # закрыт
    const STATUS_BLOCKED      = 5; # заблокирован
    const STATUS_NOTMODERATED = 7; # ожидает модерации (используется только в фильтрах)
    const STATUS_IN_WORK      = 8; # в работе (используется только в фильтрах)
    const STATUS_REMOVED      = 9; # корзина (используется только в фильтрах)

    # Подтип для заказов - услуг
    const SERVICE_TYPE_NONE    = 0;
    const SERVICE_TYPE_JOB     = 1; # Постоянная работа
    const SERVICE_TYPE_ONE     = 2; # Разовый заказ
    const SERVICE_TYPE_CONTEST = 3; # Конкурс

    # Отображать ссылку на профиль заказчика в заказах
    const ORDERS_CONTACTS_VIEW_ALL  = 0; # для всех
    const ORDERS_CONTACTS_VIEW_AUTH = 1; # для авторизованных
    const ORDERS_CONTACTS_VIEW_PRO  = 2; # для pro

    # Статус заявок
    const OFFER_STATUS_ADD               = 0; # Предложение к заказу добавлено
    const OFFER_STATUS_PERFORMER         = 1; # Исполнитель
    const OFFER_STATUS_CANDIDATE         = 2; # Кандидат
    const OFFER_STATUS_CANCELED          = 3; # Отказать
    const OFFER_STATUS_PERFORMER_START   = 4; # Предложение стать исполнителем
    const OFFER_STATUS_PERFORMER_AGREE   = 5; # Исполнитель согласился
    const OFFER_STATUS_PERFORMER_DECLINE = 6; # Исполнитель отказался
    const OFFER_STATUS_INVITE_DECLINE    = 7; # Исполнитель отказался от предложенного заказа

    const OFFER_TEMPLATE = 'Интересует Ваше задание';
    # ID Услуг
    const SVC_UP        = 128;  # поднятие
    const SVC_MARK      = 256;  # выделение
    const SVC_FIX       = 512;  # закрепление
    const SVC_HIDDEN    = 1024; # cкрытие

    # Email-уведомления исполнителям о новых заказах
    const ENOTIFY_NEWORDERS_NEVER = 0;  # отключена
    const ENOTIFY_NEWORDERS_NOW   = 1;  # мгновенная
    const ENOTIFY_NEWORDERS_DAY   = 2;  # ежедневная
    const ENOTIFY_NEWORDERS_HOURS = 3;  # каждые X часов

    # Тип получателей
    const ENOTIFY_RECEIVERS_ALL = 1; # всем
    const ENOTIFY_RECEIVERS_PRO = 2; # только для PRO

    # Видимость заказов
    const VISIBILITY_ALL     = 0; # публичный заказ, виден всем
    const VISIBILITY_PRIVATE = 1; # приватный заказ, виден только исполнителям которым предложили

    # Безопасная сделка
    const FAIRPLAY_NONE = 0; # не использовать
    const FAIRPLAY_USE  = 1; # использовать

    # Вывод дин свойств в карточке заказа в упрощенном виде - необходимые группы
    const DP_ORDER_VIEW_PART_FIRST  = 1;
    const DP_ORDER_VIEW_PART_SECOND = 2;
    const DP_ORDER_VIEW_PART_THIRD  = 3;
    const DP_ORDER_VIEW_PART_FOURTH = 4;
    const DP_ORDER_VIEW_PART_FIFTH  = 5;

    /** @var OrdersModel */
    public $model = null;
    public $securityKey = '6deed599c5a89de4766565678b9be272';

    /**
     * @return Orders
     */
    public static function i()
    {
        return bff::module('Orders');
    }

    /**
     * @return OrdersModel
     */
    public static function model()
    {
        return bff::model('Orders');
    }

    public function init()
    {
        parent::init();
        $this->module_title = _t('orders','Заказы');
        bff::autoloadEx(array(
            'OrdersOrderImages'      => array('app', 'modules/orders/orders.order.images.php'),
            'OrdersOrderAttachments' => array('app', 'modules/orders/orders.order.attachments.php'),
            'OrdersTags'             => array('app', 'modules/orders/orders.tags.php'),
            'OrdersOfferImages'      => array('app', 'modules/orders/orders.offer.images.php'),
            'OrdersOfferExamples'    => array('app', 'modules/orders/orders.offer.examples.php'),
            'OrdersSvcIcon'          => array('app', 'modules/orders/orders.svc.icon.php'),
        ));
    }

    public function sendmailTemplates()
    {
        $aTemplates = array(
            'orders_order_activate' => array(
                'title'       => 'Заказы: Активация заказа',
                'description' => 'Уведомление, отправляемое <u>незарегистрированному пользователю</u> после добавления заказа',
                'vars'        => array(
                    '{name}'          => 'Имя пользователя',
                    '{email}'         => 'Email',
                    '{activate_link}' => 'Ссылка активации заказа',
                ),
                'impl'        => true,
                'priority'    => 20,
                'enotify'     => 0, # всегда
                'group'       => 'orders',
            ),
            'order_offer_change_status' => array(
                'title'       => 'Заказы: Статус заявки',
                'description' => 'Уведомление, отправляемое <u>исполнителю</u> при изменении статуса заявки исполнителя',
                'vars'        => array(
                    '{name}'      => 'Имя пользователя',
                    '{email}'     => 'Email',
                    '{title}'     => 'Заголовок заказа',
                    '{order_url}' => 'Ссылка для просмотра заказа',
                    '{status}'    => 'Статус:<br /> - Вас выбрали кандидатом.<br />- Вас выбрали исполнителем.<br />- Вы получили отказ.',
                ),
                'impl'        => true,
                'priority'    => 21,
                'enotify'     => 0, # всегда
                'group'       => 'orders',
            ),
            'order_offer_add' => array(
                'title'       => 'Заказы: Добавление заявки',
                'description' => 'Уведомление, отправляемое <u>заказчику</u> при добавлении заявки к заказу',
                'vars'        => array(
                    '{name}'          => 'Имя заказчика',
                    '{worker_name}'   => 'Имя исполнителя',
                    '{worker_login}'  => 'Логин исполнителя',
                    '{worker_url}'    => 'Ссылка на профиль исполнителя',
                    '{order_title}'   => 'Заголовок заказа',
                    '{order_url}'     => 'Ссылка для просмотра заказа',
                    '{offer_descr}'   => 'Текст заявки (150 символов)',
                ),
                'impl'        => true,
                'priority'    => 22,
                'enotify'     => 0, # всегда
                'group'       => 'orders',
            ),
            'orders_offer_chat' => array(
                'title'       => 'Заявки: Переписка',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при добавлении ответа в переписке к заявке',
                'vars'        => array(
                    '{name}'          => 'Имя адресата (заказчика или исполнителя)',
                    '{author_name}'   => 'Имя автора ответа',
                    '{order_id}'      => 'ID заказа',
                    '{order_title}'   => 'Заголовок заказа',
                    '{order_url}'     => 'Ссылка для просмотра заказа',
                    '{message}'       => 'Текст ответа (150 символов)',
                ),
                'impl'        => true,
                'priority'    => 31,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'orders',
            ),
            'orders_order_new_now' => array(
                'title'       => 'Заказы: Новый заказ (мгновенная отправка)',
                'description' => 'Уведомление, отправляемое <u>исполнителю</u> при создании нового заказа в одну из его специализаций',
                'vars'        => array(
                    '{fio}'           => 'ФИО исполнителя',
                    '{name}'          => 'Имя исполнителя',
                    '{order_id}'      => 'ID заказа',
                    '{order_title}'   => 'Заголовок заказа',
                    '{order_url}'     => 'Ссылка для просмотра заказа',
                    '{order_descr}'   => 'Текст заказа (150 символов)',
                    '{spec_title}'    => 'Название специализации',
                ),
                'impl'        => true,
                'priority'    => 23,
                'enotify'     => Users::ENOTIFY_ORDERS_NEW,
                'group'       => 'orders',
            ),
            'orders_order_new_group' => array(
                'title'       => 'Заказы: Новый заказ (групповая отправка)',
                'description' => 'Уведомление, отправляемое <u>исполнителю</u> при создании нового заказа в одну из его специализаций',
                'vars'        => array(
                    '{fio}'           => 'ФИО исполнителя',
                    '{name}'          => 'Имя исполнителя',
                    '{orders_block}'  => 'Блок заказов (заголовок, ссылка для просмотра)',
                ),
                'impl'        => true,
                'priority'    => 24,
                'enotify'     => Users::ENOTIFY_ORDERS_NEW,
                'group'       => 'orders',
            ),
            'orders_order_blocked' => array(
                'title'       => 'Заказы: Заблокирован модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> (автору заказа) при блокировке заказа модератором',
                'vars'        => array(
                    '{fio}'             => 'ФИО заказчика',
                    '{name}'            => 'Имя заказчика',
                    '{order_id}'        => 'ID заказа',
                    '{order_title}'     => 'Заголовок заказа',
                    '{order_url}'       => 'Ссылка для просмотра заказа',
                    '{blocked_reason}'  => 'Причина блокировки',
                ),
                'impl'        => true,
                'priority'    => 25,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'orders',
            ),
            'orders_order_approved' => array(
                'title'       => 'Заказы: Одобрен модератором',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> (автору заказа) при одобрении заказа модератором',
                'vars'        => array(
                    '{fio}'         => 'ФИО заказчика',
                    '{name}'        => 'Имя заказчика',
                    '{order_id}'    => 'ID заказа',
                    '{order_title}' => 'Заголовок заказа',
                    '{order_url}'   => 'Ссылка для просмотра заказа',
                ),
                'impl'        => true,
                'priority'    => 26,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'orders',
            ),
        );
        if (static::ordersOpinions()) {
            $aTemplates += array(
                'order_offer_performer_start' => array(
                    'title'       => 'Заявки: Предложение стать исполнителем',
                    'description' => 'Уведомление, отправляемое <u>исполнителю</u> с предложением стать исполнителем',
                    'vars'        => array(
                        '{fio}'          => 'ФИО исполнителя',
                        '{name}'         => 'Имя исполнителя',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_fio}'   => 'ФИО заказчика',
                        '{client_login}' => 'Логин заказчика',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                    ),
                    'impl'        => true,
                    'priority'    => 27,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
                'order_offer_performer_stop' => array(
                    'title'       => 'Заявки: Отмена предложения стать исполнителем',
                    'description' => 'Уведомление, отправляемое <u>исполнителю</u> в случае отмены заказчиком его предложения.',
                    'vars'        => array(
                        '{fio}'          => 'ФИО исполнителя',
                        '{name}'         => 'Имя исполнителя',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_fio}'   => 'ФИО заказчика',
                        '{client_login}' => 'Логин заказчика',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                    ),
                    'impl'        => true,
                    'priority'    => 28,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
                'order_offer_performer_agree' => array(
                    'title'       => 'Заявки: Подтверждение работы над заказом',
                    'description' => 'Уведомление, отправляемое <u>заказчику</u> при подтверждении исполнителем предложения.',
                    'vars'        => array(
                        '{fio}'          => 'ФИО заказчика',
                        '{name}'         => 'Имя заказчика',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_fio}'   => 'ФИО исполнителя',
                        '{worker_login}' => 'Логин исполнителя',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                    ),
                    'impl'        => true,
                    'priority'    => 29,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
                'order_offer_performer_decline' => array(
                    'title'       => 'Заявки: Отказ от работы над заказом',
                    'description' => 'Уведомление, отправляемое <u>заказчику</u> в случае отказа исполнителя от предложения.',
                    'vars'        => array(
                        '{fio}'          => 'ФИО заказчика',
                        '{name}'         => 'Имя заказчика',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{message}'      => 'Текст сообщения',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_fio}'   => 'ФИО исполнителя',
                        '{worker_login}' => 'Логин исполнителя',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                    ),
                    'impl'        => true,
                    'priority'    => 30,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
            );
        }
        if (static::invitesEnabled()) {
            $aTemplates += array(
                'order_invite_start' => array(
                    'title'       => 'Заказы: Предложение заказа исполнителю (Предложить заказ)',
                    'description' => 'Уведомление, отправляемое <u>исполнителю</u> при получении предложения заказа от заказчика',
                    'vars'        => array(
                        '{fio}'          => 'ФИО исполнителя',
                        '{name}'         => 'Имя исполнителя',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_fio}'   => 'ФИО заказчика',
                        '{client_login}' => 'Логин заказчика',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                      //'{message}'      => 'Текст сообщения (до 150 символов)',
                    ),
                    'impl'        => true,
                    'priority'    => 32,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
                'order_invite_agree' => array(
                    'title'       => 'Заказы: Согласие на предложенный заказ исполнителем (Предложить заказ)',
                    'description' => 'Уведомление, отправляемое <u>заказчику</u> в случае согласия исполнителя с заказом',
                    'vars'        => array(
                        '{fio}'          => 'ФИО заказчика',
                        '{name}'         => 'Имя заказчика',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_fio}'   => 'ФИО исполнителя',
                        '{worker_login}' => 'Логин исполнителя',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                        '{message}'      => 'Текст сообщения (до 150 символов)',
                    ),
                    'impl'        => true,
                    'priority'    => 33,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
                'order_invite_decline' => array(
                    'title'       => 'Заказы: Отказ от предложенного заказа исполнителем (Предложить заказ)',
                    'description' => 'Уведомление, отправляемое <u>заказчику</u> в случае отказа исполнителя от заказа',
                    'vars'        => array(
                        '{fio}'          => 'ФИО заказчика',
                        '{name}'         => 'Имя заказчика',
                        '{order_title}'  => 'Заголовок заказа',
                        '{order_url}'    => 'Ссылка для просмотра заказа',
                        '{client_url}'   => 'Ссылка на профиль заказчика',
                        '{worker_fio}'   => 'ФИО исполнителя',
                        '{worker_login}' => 'Логин исполнителя',
                        '{worker_url}'   => 'Ссылка на профиль исполнителя',
                        '{message}'      => 'Текст сообщения (до 150 символов)',
                    ),
                    'impl'        => true,
                    'priority'    => 34,
                    'enotify'     => 0, # всегда
                    'group'       => 'orders',
                ),
            );
        }

        Sendmail::i()->addTemplateGroup('orders', 'Заказы', 1);

        return $aTemplates;
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # Добавление заказа
            case 'add':
                $url .= '/orders/add' . static::urlQuery($opts);
                break;
            # Редактирование заказа
            case 'edit':
                $url .= '/orders/edit' . static::urlQuery($opts);
                break;
            # Информация о статусе заказа
            case 'status':
                $url .= '/orders/status' . static::urlQuery($opts);
                break;
            # Список заказов
            case 'list':
                $url = Geo::url($opts, $dynamic) . 'orders/'. (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Список заказов с фильтром по выбранной специализации
            case 'list.single':
                if (empty($opts['keyword'])) {
                    $url = Geo::url($opts, $dynamic) . (!empty($opts) ? static::urlQuery($opts) : '');
                    break;
                }
                $url = Geo::url($opts, $dynamic) . 'orders/' . $opts['keyword'] . '/' . static::urlQuery($opts, array('keyword'));
                break;
            case 'list.multi':
                if ( ! empty($opts['keyword'])) {
                    $opts['orders'] = $opts['keyword'];
                    unset($opts['keyword']);
                }
                $url = Geo::url($opts, $dynamic) . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
            # Поиск заказов по тегу
            case 'search-tag':
                $opts['tag'] = mb_strtolower($opts['tag']) . '-' . $opts['id'];
                $url = Geo::url($opts, $dynamic) . static::urlQuery($opts, array('id'));
                break;
            # Просмотр заказа
            case 'view':
                $url .= '/orders/' . $opts['id'] . '-' . $opts['keyword'] .'.html'.
                    static::urlQuery($opts, array('id', 'keyword'));
                break;
            # Продвижение заказа
            case 'promote':
                $url .= '/orders/promote' . static::urlQuery($opts);
                break;
            # Ссылка активации заказа
            case 'item.activate':
                $url .= '/orders/activate' . static::urlQuery($opts);
                break;
            # Список моих заказов (профиль)
            case 'my.orders':
                $opts['login'] = User::data('login');
                $url = static::url('user.listing', $opts, $dynamic);
                break;
            # Список заказов заказчика
            case 'user.listing':
                $url  = Users::url('profile', array('login'=>$opts['login'], 'tab'=>'orders'), $dynamic);
                $url .= static::urlQuery($opts, array('login'));
                break;
            # Список моих заявок (профиль)
            case 'my.offers':
                $url  = Users::url('profile', array('login'=>User::data('login'), 'tab'=>'offers'), $dynamic);
                $url .= static::urlQuery($opts, array('login'));
                break;
            # RSS лента
            case 'rss':
                $url .= '/orders/rss' . static::urlQuery($opts);
                break;
        }
        return bff::filter('orders.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Описание seo шаблонов страниц
     * @return array
     */
    public function seoTemplates()
    {
        $templates = array(
            'pages'  => array(
                'search-index' => array(
                    't'      => 'Список заказов (главная)',
                    'list'   => true,
                    'macros' => array(
                        'type' => array('t'=>'Тип заказов: "Услуги", "Товары"'),
                        'region' => array('t'=>'Регион поиска: город/область'),
                        'country' => array('t'=>'Страна поиска'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'search-service' => array(
                    't'      => 'Список заказов - услуги (специализация)',
                    'list'   => true,
                    'inherit'=> true,
                    'macros' => array(
                        'spec' => array('t'=>'Название специализации'),
                        'region' => array('t'=>'Регион поиска: город/область'),
                        'country' => array('t'=>'Страна поиска'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'search-product' => array(
                    't'      => 'Список заказов - товары (категория)',
                    'list'   => true,
                    'inherit'=> true,
                    'macros' => array(
                        'category' => array('t'=>'Название категории товаров'),
                        'region' => array('t'=>'Регион поиска: город/область'),
                        'country' => array('t'=>'Страна поиска'),
                    ),
                    'fields'  => array(
                        'seotext' => array(
                            't'    => 'SEO текст',
                            'type' => 'wy',
                        ),
                    ),
                ),
                'search-keyword' => array(
                    't'      => 'Поиск заказов (по ключевому слову)',
                    'list'   => true,
                    'macros' => array(
                        'query' => array('t'=>'Строка запроса'),
                    ),
                ),
                'add' => array(
                    't'      => 'Добавление заказа',
                    'macros' => array(),
                ),
                'view' => array(
                    't'      => 'Просмотр заказа',
                    'macros' => array(
                        'title'       => array('t' => 'Заголовок (до 50 символов)'),
                        'title.full'  => array('t' => 'Заголовок (полный)'),
                        'description' => array('t' => 'Описание (до 150 символов)'),
                        'price'       => array('t' => 'Цена'),
                        'tags'        => array('t' => 'Теги (перечисление)'),
                        'region'      => array('t' => 'Регион заказа'),
                        'spec'        => array('t' => 'Специализация'),
                        'category'    => array('t' => 'Категория'),
                    ),
                    'fields' => array(
                        'share_title' => array(
                            't'    => 'Заголовок (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                        'share_description' => array(
                            't'    => 'Описание (поделиться в соц. сетях)',
                            'type' => 'textarea',
                        ),
                        'share_sitename' => array(
                            't'    => 'Название сайта (поделиться в соц. сетях)',
                            'type' => 'text',
                        ),
                    ),
                ),
            ),
        );

        if ( ! Specializations::catsOn()) {
            unset($templates['pages']['view']['macros']['category']);
        }
        if ( ! static::useProducts()) {
            $templates['pages']['search-service']['t'] = 'Поиск заказов (специализация)';
            unset($templates['pages']['search-index']['macros']['type']);
            unset($templates['pages']['search-product']);
        }
        if ( ! Geo::countrySelect() ) {
            unset($templates['pages']['search-index']['macros']['country']);
            unset($templates['pages']['search-service']['macros']['country']);
            unset($templates['pages']['search-product']['macros']['country']);
        }

        return $templates;
    }

    /**
     * Использовать премодерацию заказов
     * По-умолчанию: true - премодерация включена
     * @return bool
     */
    public static function premoderation()
    {
        return config::sysAdmin('orders.premoderation', true, TYPE_BOOL);
    }

    /**
     * Использовать тип заказа "продукт"
     * По-умолчанию: true - использовать
     * @return bool
     */
    public static function useProducts()
    {
        return (config::sysAdmin('orders.use.products', true, TYPE_BOOL) && bff::moduleExists('shop') && Shop::enabled());
    }

    /**
     * Тип отображения контактов заказчика в заказах
     * По-умолчанию: отображать только авторизованным
     * @return mixed
     */
    public static function ordersContactsView()
    {
        return config::get('orders_contacts_view', static::ORDERS_CONTACTS_VIEW_AUTH);
    }

    /**
     * Использовать привязку к карте
     * По-умолчанию: true - карта используется
     * @return bool
     */
    public static function mapEnabled()
    {
        return config::sysAdmin('orders.map.enabled', true, TYPE_BOOL);
    }

    /**
     * Максимальное кол-во регионов прикрепляемых к заказу
     * По-умолчанию: 1
     * @return integer
     */
    public static function regionsLimit()
    {
        return config::sys('orders.regions.limit', 1, TYPE_UINT);
    }

    /**
     * Максимальное кол-во специализаций прикрепляемых к заказу типа "услуга"
     * По-умолчанию: 1
     * @return integer
     */
    public static function specsLimit()
    {
        return config::sys('orders.specs.limit', 1, TYPE_UINT);
    }

    /**
     * Максимальное кол-во категорий прикрепляемых к заказу типа "товар"
     * По-умолчанию: 1
     * @return integer
     */
    public static function catsLimit()
    {
        return config::sys('orders.cats.limit', 1, TYPE_UINT);
    }

    /**
     * Максимально допустимое кол-во фотографий прикрепляемых к заказу
     * 0 - возможность прикрепления фотографий выключена (недоступна)
     * По-умолчанию: 8
     * @return integer
     */
    public static function imagesLimit()
    {
        return config::sysAdmin('orders.images.limit', 8, TYPE_UINT);
    }

    /**
     * Максимально допустимое кол-во файлов прикрепляемых к заказу
     * 0 - возможность прикрепления файлов выключена (недоступна)
     * По-умолчанию: 4
     * @return integer
     */
    public static function attachmentsLimit()
    {
        return config::sysAdmin('orders.attachments.limit', 4, TYPE_UINT);
    }

    /**
     * Максимально доступное количество заявок на заказы в день
     * По-умолчанию: 1000 для PRO, 5 для не PRO
     * @param boolean $pro PRO-аккаунт
     * @return mixed
     */
    public static function offersLimit($pro = false)
    {
        if ($pro) return config::get('orders_offers_limit_pro', 1000);
        return config::get('orders_offers_limit', 5);
    }

    /**
     * Максимально допустимое кол-во примеров работ прикрепляемых к заявке
     * 0 - возможность прикрепления примеров работ выключена (недоступна)
     * По-умолчанию: 5
     * @return integer
     */
    public static function offerExamplesLimit()
    {
        return config::sysAdmin('orders.offer.examples.limit', 5, TYPE_UINT);
    }

    /**
     * Выполнять поиск заказов по одной специализации
     * По-умолчанию: false - поиск по нескольким специализациям
     * @return mixed
     */
    public static function searchOneSpec()
    {
        return config::sysAdmin('orders.search.one.spec', false, TYPE_BOOL);
    }

    /**
     * Проверка соответствия специализации исполнителя при добавлении заявки к заказу
     * По-умолчанию: false - проверка не выполняется
     * @return bool
     */
    public static function offerSpecCheck()
    {
        return config::sysAdmin('orders.offer.spec.check', false, TYPE_BOOL);
    }

    /**
     * Разрешено ли удалять заказ владельцу заказа
     * По-умолчанию: true - разрешено
     * @return bool
     */
    public static function deleteAllow()
    {
        return config::sysAdmin('orders.delete.allow', true, TYPE_BOOL);
    }

    /**
     * Добавить к рейтингу за выбор исполнителем
     * По-умолчанию: 0 - выбор исполнителем не влияет на рейтинг
     * @return int
     */
    public static function ratingPerformer()
    {
        return config::sysAdmin('users.rating.order.performer', 0, TYPE_UINT);
    }

    /**
     * Максимально допустимое кол-во символов в заголовке заказа
     * По-умолчанию: 200
     * @return int
     */
    public static function titleLimit()
    {
        $limit = config::sysAdmin('orders.title.limit', 200, TYPE_UINT);
        if ($limit<=0) $limit = 200;
        return $limit;
    }

    /**
     * Отображать в списках снятые с публикации (закрытые) заказы
     * По-умолчанию: true - отображать
     * @return bool
     */
    public static function searchClosed()
    {
        return config::sysAdmin('orders.search.closed', true, TYPE_BOOL);
    }

    /**
     * Склеивать теги в "еще" когда их кол-во превышает допустимый лимит
     * @return integer
     */
    public static function searchTagsLimit()
    {
        return config::sysAdmin('orders.search.tags.limit', 0, TYPE_UINT);
    }

    /**
     * Использовать для заказов отдельные дин. свойства
     * @return mixed
     */
    public static function dynpropsEnabled()
    {
        return config::sysAdmin('orders.dynprops', false, TYPE_BOOL);
    }

    /**
     * Использовать привязку отзывов к заказам
     * @return bool
     */
    public static function ordersOpinions()
    {
        return config::sysAdmin('orders.opinions', false, TYPE_BOOL);
    }

    /**
     * Метод обрабатывающий ситуацию с блокировкой/разблокировкой пользователя
     * @param integer $nUserID ID пользователя
     * @param boolean $bBlocked true - заблокирован, false - разблокирован
     */
    public function onUserBlocked($nUserID, $bBlocked)
    {
        if ($bBlocked) {
            # при блокировке пользователя -> блокируем все его заказы
            $aItems = $this->model->ordersDataByFilter(
                array(
                    'user_id' => $nUserID,
                    'status IN(' . self::STATUS_OPENED . ',' . self::STATUS_CLOSED . ')'
                ),
                array('id')
            );
            if (!empty($aItems)) {
                $this->model->ordersSave(array_keys($aItems), array(
                        'status_prev = status',
                        'status'         => self::STATUS_BLOCKED,
                        'blocked_reason' => _t('users', 'Аккаунт пользователя заблокирован'),
                    )
                );
            }
        } else {
            # при разблокировке -> разблокируем
            $aItems = $this->model->ordersDataByFilter(
                array(
                    'user_id' => $nUserID,
                    'status'  => self::STATUS_BLOCKED,
                    'status_prev != '.self::STATUS_BLOCKED, # все кроме ранее заблокированных
                ),
                array('id')
            );
            if (!empty($aItems)) {
                $this->model->ordersSave(array_keys($aItems), array(
                        'status = (CASE status_prev WHEN ' . self::STATUS_BLOCKED . ' THEN ' . self::STATUS_CLOSED . ' ELSE status_prev END)',
                        'status_prev' => self::STATUS_BLOCKED,
                        //'blocked_reason' => '', # оставляем последнюю причину блокировки
                    )
                );
            }
        }
    }

    /**
     * Тип заказа
     * @return array
     */
    public static function aTypes()
    {
        $aData = array(
            self::TYPE_SERVICE => array(
                'id'     => self::TYPE_SERVICE,
                't'      => _t('orders', 'Услуга'),
                'plural' => _t('orders', 'Услуги'),
            ),
            self::TYPE_PRODUCT => array(
                'id'     => self::TYPE_PRODUCT,
                't'      => _t('orders', 'Товар'),
                'plural' => _t('orders', 'Товары'),
            ),
        );
        if (!static::useProducts()) {
            unset($aData[self::TYPE_PRODUCT]);
        }
        reset($aData);

        return bff::filter('orders.types', $aData);
    }

    /**
     * Модификатор бюджета
     * @return array
     */
    public static function aServiceTypes()
    {
        return bff::filter('orders.service.types', array(
            static::SERVICE_TYPE_ONE     => array(
                'id' => static::SERVICE_TYPE_ONE,
                't'  => _t('orders', 'Разовый заказ'),
                'c'  => 'briefcase'
            ),
            static::SERVICE_TYPE_JOB     => array(
                'id' => static::SERVICE_TYPE_JOB,
                't'  => _t('orders', 'Постоянная работа'),
                'c'  => 'anchor'
            ),
            static::SERVICE_TYPE_CONTEST => array(
                'id' => static::SERVICE_TYPE_CONTEST,
                't'  => _t('orders', 'Конкурс'),
                'c'  => 'gavel'
            ),
        ));
    }

    /**
     * Указывать срок приема заявок для заказов
     * @return bool
     */
    public static function termsEnabled()
    {
        return config::sysAdmin('orders.terms', false, TYPE_BOOL);
    }

    /**
     * Варианты сроков приема заявок
     * @param integer $nDef @ref ID варианта по-умолчанию
     * @param bool $isInvite приватный заказ
     * @return array
     */
    public static function aTerms(&$nDef = 0, $isInvite = false)
    {
        static $aData = false, $def = false;
        if (!$aData || !$def) {
            $lang_days = _t('', 'день;дня;дней');
            $lang_weeks = _t('', 'неделя;недели;недель');
            $lang_months = _t('', 'месяц;месяца;месяцев');
            $aData = bff::filter('orders.terms', array(
                1 => array('days' => 3,  'def' => 0), # 3 дня
                2 => array('days' => 7,  'def' => 0, 't' => tpl::declension(1, $lang_weeks)), # 1 неделя
                3 => array('days' => 14, 'def' => 1, 't' => tpl::declension(2, $lang_weeks)), # 2 недели
                4 => array('days' => 30, 'def' => 0, 't' => tpl::declension(1, $lang_months)), # 1 месяц
                // 5 => array('days' => 60, 'def' => 0, 't' => tpl::declension(2, $lang_months)), # 2 месяца
                // 6 => array('days' => 90, 'def' => 0, 't' => tpl::declension(3, $lang_months)), # 3 месяца
                // 7 => array('days' => 180, 'def' => 0, 't' => tpl::declension(6, $lang_months)), # 6 месяцев
                0 => array('days' => 0,  'def' => 0, 't' => _t('', 'Бессрочно')), # Бессрочно
            ));
            # Приватные заказы по умолчанию бессрочные
            if($isInvite && isset($aData[0])){
                foreach($aData as & $v){
                    $v['def'] = 0;
                }unset($v);
                $aData[0]['def'] = 1;
            }
            $now = time();
            foreach ($aData as $k => &$v) {
                $v['id'] = $k;
                $v['date'] = false;
                if ($v['days'] > 0) {
                    if (!isset($v['t'])) $v['t'] = tpl::declension($v['days'], $lang_days);
                    $v['date'] = tpl::date_format2($now + $v['days'] * 86400, false, true);
                }
                if ($v['def']) {
                    $def = $k;
                }
            } unset($v);
        }
        $nDef = $def;
        return $aData;
    }

    /**
     * Варианты сроков приема заявок в виде select::options
     * @param integer $nSelected выбранный вариант
     * @param integer $nDays @ref
     * @param bool $isInvite приватный заказ
     * @return string HTML
     */
    public static function aTermsOptions($nSelected = false, & $nDays = 0, $isInvite = false)
    {
        $aData = static::aTerms($def, $isInvite);
        if ($nSelected !== false) {
            if (!array_key_exists($nSelected, array_keys($aData))) {
                $nSelected = false;
            }
        }

        $sRet = '';
        foreach ($aData as $v) {
            $attr = array('value' => $v['id']);
            if ($nSelected === false) {
                if ($v['def']) {
                    $attr[] = 'selected';
                    $nDays = $v['days'];
                }
            } else {
                if ($nSelected == $v['id']) {
                    $attr[] = 'selected';
                    $nDays = $v['days'];
                }
            }
            $sRet .= '<option' . HTML::attributes($attr) . '>' . $v['t'] . '</option>';
        }
        return $sRet;
    }

    /**
     * Расчет срока приема заявок
     * @param integer $term @ref ID варианта срока
     * @param bool $isInvite приватный заказ
     * @return string mysql date
     */
    public function termExpire(&$term, $isInvite = false)
    {
        do {
            if (!static::termsEnabled()) {
                $term = 0;
                break;
            }

            $terms = static::aTerms($default, $isInvite);
            if (!array_key_exists($term, $terms)) {
                if ($default !== false) {
                    $term = $default;
                } else {
                    break;
                }
            }

            $days = bff::filter('orders.terms.expire', $terms[$term]['days'], array('term'=>&$term, 'isInvite'=>$isInvite));
            if ($days) {
                return date('Y-m-d H:i:s', time() + $days * 86400);
            }
        } while(false);

        return '0000-00-00 00:00:00';
    }

    /**
     * Возможность предлагать заказы исполнителю
     */
    public static function invitesEnabled()
    {
        return config::sysAdmin('orders.invites', false, TYPE_BOOL);
    }

    /**
     * Варианты видимости заказов
     * @return array
     */
    public static function visibility()
    {
        return bff::filter('orders.visibility', array(
            static::VISIBILITY_ALL      => array('id' => static::VISIBILITY_ALL,     't' => _t('orders', 'Публичный')),
            static::VISIBILITY_PRIVATE  => array('id' => static::VISIBILITY_PRIVATE, 't' => _t('orders', 'Приватный')),
        ));
    }

    /**
     * Инициализация компонента OrdersOrderImages
     * @param integer $nOrderID ID заказа
     * @return OrdersOrderImages component
     */
    public function orderImages($nOrderID = 0)
    {
        static $i;
        if (!isset($i)) {
            $i = new OrdersOrderImages();
        }
        $i->setRecordID($nOrderID);

        return $i;
    }

    /**
     * Инициализация компонента OrdersOrderAttachments
     * @param integer $nOrderID ID заказа
     * @return OrdersOrderAttachments component
     */
    public function orderAttachments($nOrderID = 0)
    {
        static $i;
        if (!isset($i)) {
            $i = new OrdersOrderAttachments();
        }
        $i->setRecordID($nOrderID);

        return $i;
    }

    /**
     * Инициализация компонента работы с тегами
     * @return OrdersTags
     */
    public function orderTags()
    {
        static $i;
        if (!isset($i)) {
            $i = new OrdersTags();
            if (!bff::adminPanel()) {
                $i->module_dir_tpl = $this->module_dir_tpl;
            }
        }
        return $i;
    }

    /**
     * Инициализация компонента OrdersOfferImages
     * @param integer $nOfferID ID заявки
     * @return OrdersOfferImages component
     */
    public function offerImages($nOfferID = 0)
    {
        static $i;
        if (!isset($i)) {
            $i = new OrdersOfferImages();
        }
        $i->setRecordID($nOfferID);

        return $i;
    }

    /**
     * Инициализация компонента OrdersOfferExamples
     * @param integer $nOfferID ID заявки
     * @return OrdersOfferExamples component
     */
    public function offerExamples($nOfferID = 0)
    {
        static $i;
        if (!isset($i)) {
            $i = new OrdersOfferExamples();
        }
        $i->setRecordID($nOfferID);

        return $i;
    }

    /**
     * Валидация специализаций заказа
     * @param array $aData @ref данные
     * @param int $nTypeID - тип заказа
     */
    public function cleanSpecializations(array & $aData, $nTypeID)
    {
        if (!isset($aData['specs'])) {
            return;
        }

        if ($nTypeID != static::TYPE_SERVICE) {
            unset($aData['specs']);

            return;
        }
        if (!static::specsLimit()) {
            unset($aData['specs']);

            return;
        }
        $aSpecs = array();
        $aCats = array();
        foreach ($aData['specs'] as $v) {
            if (!isset($v['spec'])) {
                continue;
            }
            $nSpecID = $v['spec'];
            unset($v['spec']);
            if (!empty($v)) {
                $nCatID = end($v);
            } else {
                $nCatID = 0;
            }
            $aSpecs[] = array('cat_id' => $nCatID, 'spec_id' => $nSpecID);
            if ($nCatID && !in_array($nCatID, $aCats)) {
                $aCats[] = $nCatID;
            }
        }
        unset($aData['specs']);
        $aAllowCats = Specializations::model()->specializationsInCategories($aCats, array());
        foreach ($aSpecs as $k => $v) {
            if (!isset($aAllowCats[$v['cat_id']][$v['spec_id']])) {
                unset($aSpecs[$k]);
            }
        }
        if (empty($aSpecs)) {
            return;
        }
        $nCnt = 1;
        foreach ($aSpecs as $k => $v) {
            if ($nCnt > static::specsLimit()) {
                unset($aSpecs[$k]);
                continue;
            }
            $aSpecs[$k]['num'] = $nCnt++;
        }
        $aData['specs'] = $aSpecs;
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nOrderID ID заказа или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @param integer $nTypeID - тип заказа(если известен)
     * @return array параметры
     */
    function validateOrderData($nOrderID, $bSubmit, $nTypeID = 0)
    {
        $aData = array();
        $aParam = array(
            'title'        => array(TYPE_NOTAGS, 'len' => static::titleLimit(), 'len.sys' => 'orders.form.title.limit'), # Название
            'descr'        => array(TYPE_TEXT,   'len' => 3000, 'len.sys' => 'orders.order.descr.limit'), # Описание
            'service_type' => TYPE_UINT, # Подтип для услуги
            'cats'         => TYPE_ARRAY_UINT, # Категории
            'specs'        => TYPE_ARRAY, # специализации
            'regions'      => TYPE_ARRAY_UINT, # Регионы
            'district_id'  => TYPE_UINT,       # Районы города
            'price'        => TYPE_PRICE, # Бюджет
            'price_curr'   => TYPE_UINT, # Валюта бюджета
            'price_rate'   => TYPE_UINT, # Модификатор бюджета (период)
            'price_ex'     => TYPE_UINT, # Модификатор бюджета (по договору)
            'addr_addr'    => array(TYPE_NOTAGS, 'len' => 400, 'len.sys' => 'orders.order.addr.limit'), # адрес
            'addr_lat'     => TYPE_NUM, # адрес, координата LAT
            'addr_lng'     => TYPE_NUM, # адрес, координата LNG
            'pro'          => TYPE_BOOL, # только для PRO-аккаунтов
            'schedule'     => TYPE_ARRAY, #  график работы
            'start_date'   => TYPE_NOTAGS,  # дата начала
            'is_immediate' => TYPE_BOOL, # срочный заказ
        );
        $termsEnabled = static::termsEnabled();
        if (!$nOrderID) {
            $aParam['user_id'] = TYPE_UINT; # Пользователь
            if ($termsEnabled && $bSubmit) {
                $aParam['term'] = TYPE_UINT; # Срок приема заявок
            }
        } else {
            if ($termsEnabled && $this->input->post('set_term', TYPE_BOOL)) {
                $aParam['term'] = TYPE_UINT; # Срок приема заявок
            }
        }
        if (!$nTypeID) {
            $aParam['type'] = TYPE_UINT; # Тип заказа
        }
        if (static::invitesEnabled()) {
            if( ! $nOrderID) {
                $aParam['visibility'] = TYPE_UINT; # Видимость заказа
            }
        }
        if (bff::fairplayEnabled()) {
            $aParam['fairplay'] = TYPE_UINT; # Безопасная сделка
        }

        $this->input->postm($aParam, $aData);

        if ($bSubmit) {
            # Тип
            if (!$nTypeID) {
                $aTypes = static::aTypes();
                if (!static::useProducts() || !array_key_exists($aData['type'], $aTypes)) {
                    $aData['type'] = static::TYPE_SERVICE;
                }
                $nTypeID = $aData['type'];
            }

            bff::hook('orders.order.validate.step1', array('id'=>$nOrderID,'data'=>&$aData));

            if (!strlen($aData['title'])) {
                $this->errors->set(_t('orders', 'Укажите заголовок'), 'title');
            }
            if (mb_strlen($aData['descr']) < config::sysAdmin('orders.order.descr.min', 10, TYPE_UINT)) {
                $this->errors->set(_t('orders','Описание заказа слишком короткое'), 'descr');
            }

            if ( ! bff::adminPanel()) {
                # антиспам фильтр
                Site::i()->spamFilter(array(
                    array('text' => & $aData['title'], 'error'=>_t('', 'В указанном вами заголовке присутствует запрещенное слово "[word]"')),
                    array('text' => & $aData['descr'], 'error'=>_t('', 'В указанном вами описании присутствует запрещенное слово "[word]"')),
                ));
            }

            # Подтип услуги
            if ($nTypeID == static::TYPE_SERVICE) {
                if (!array_key_exists($aData['service_type'], static::aServiceTypes())) {
                    $aFirstType = static::aServiceTypes();
                    $aFirstType = reset($aFirstType);
                    $aData['service_type'] = $aFirstType['id'];
                }
            } else {
                $aData['service_type'] = static::SERVICE_TYPE_NONE;
            }

            # Проверим специализации
            $this->cleanSpecializations($aData, $nTypeID);

            switch ($nTypeID) {
                case static::TYPE_SERVICE:
                {
                    if (empty($aData['specs'])) {
                        $this->errors->set(_t('orders', 'Укажите специализацию'));
                    }
                }
                break;
                case static::TYPE_PRODUCT:
                {
                    if (empty($aData['cats'])) {
                        $this->errors->set(_t('orders', 'Укажите категорию'));
                    }
                }
                break;
            }

            # Кешируем модификатор бюджета
            $aData['price_rate_text'] = '';
            if ($nTypeID == static::TYPE_SERVICE && !empty($aData['specs'])) {
                $aFirstSpec = reset($aData['specs']);
                $nFirstSpecID = $aFirstSpec['spec_id'];
                if ($nFirstSpecID) {
                    $aPriceSett = Specializations::i()->aPriceSett($nFirstSpecID, true);
                    $aData['price_ex'] = $aData['price_ex'] & $aPriceSett['ex'];
                    if ($aData['price_ex']) {
                        $aTexts = $aPriceSett['price_title_mod'];
                        foreach ($aTexts as $k => $v) {
                            $aTexts[$k] = mb_strtoupper(mb_substr($v,0,1)).mb_strtolower(mb_substr($v,1));
                        }
                        $aData['price_rate_text'] = serialize($aTexts);
                    } else {
                        if ($aData['price_rate']) {
                            $nPriceRate = $aData['price_rate'];
                            if (!empty($aPriceSett['rates'][$nPriceRate])) {
                                $aRates = $aPriceSett['rates'][$nPriceRate];
                                foreach ($aRates as $k => $v) {
                                    $aRates[$k] = mb_strtolower($v);
                                }
                                $aData['price_rate_text'] = serialize($aRates);
                            }
                        }
                    }
                }
            } else {
                $aData['price_ex'] = 0;
            }
            if ($aData['price'] && $aData['price_curr']) {
                $aData['price_search'] = Site::currencyPriceConvertToDefault($aData['price'], $aData['price_curr']);
            }

            # Город
            if (!empty($aData['regions'])) {
                foreach ($aData['regions'] as $k => $city_id) {
                    if (!$city_id) {
                        unset($aData['regions'][$k]);
                        continue;
                    }
                    if (!Geo::isCity($city_id)) {
                        $this->errors->set(_t('orders', 'Город указан некорректно'));
                    }

                    # Район города
                    Geo::i()->validateDistrictID($aData['district_id'], $city_id);
                }
            }

            # Пользователь: при добавлении
            if (!$nOrderID) {
                if (bff::adminPanel()) {
                    if (empty($aData['regions']) && Geo::defaultCity()){
                        # Район города
                        Geo::i()->validateDistrictID($aData['district_id'], Geo::defaultCity());
                    }
                    if (!$aData['user_id']) {
                        $this->errors->set('Укажите пользователя');
                    } else {
                        $aUserData = Users::model()->userData($aData['user_id'], array('email'));
                        if (empty($aUserData)) {
                            $this->errors->set(_t('orders', 'Пользователь указан некорректно'));
                        }
                    }
                } else {
                    $aData['user_id'] = User::id();
                }
            }

            # URL-Keyword
            $aData['keyword'] = mb_strtolower(func::translit($aData['title']));
            $aData['keyword'] = trim(preg_replace('/[^a-zA-Z0-9_\-]/', '', $aData['keyword']), '- ');

            # Срок приема заявок
            if ($termsEnabled && isset($aData['term'])) {
                $aData['expire'] = $this->termExpire($aData['term']);
            }

            if (static::invitesEnabled()) {
                if (isset($aData['visibility']) && !array_key_exists($aData['visibility'], static::visibility())) {
                    $aData['visibility'] = static::VISIBILITY_ALL;
                }
            }

            # Проверка использования безопасной сделки
            if (bff::fairplayEnabled()) {
                if( ! in_array($aData['fairplay'], array(static::FAIRPLAY_NONE, static::FAIRPLAY_USE), true)) {
                    $aData['fairplay'] = static::FAIRPLAY_NONE;
                }
            }

            # Prepare and validate schedule
            if (!bff::adminPanel()){
                if (!empty($aData['schedule'])){
                    $this->prepareValidateSchedule($aData);
                } else {
                    $this->errors->set(_t('schedule','Ошибка заполнения графика работ: один временной период в один день должен быть выбран'));
                }
                if (!empty($aData['start_date'])) {
                    $from = strtotime($aData['start_date']);
                    $aData['start_date'] = date('Y-m-d 00:00:00', $from);
                }
            }
            bff::hook('orders.order.validate.step2', array('id'=>$nOrderID,'data'=>&$aData));

        }
        return $aData;
    }

    public function prepareValidateSchedule(&$aData)
    {
        $aDaysSchedule = [];
        foreach ($aData['schedule'] as $item){
            $aPerDay = explode('-', $item);
            $aDaysSchedule[$aPerDay[1]][] = $aPerDay[0];
        }
        $aData['schedule'] = $aDaysSchedule;
    }

    /**
     * Является ли текущий пользователь владельцем заказа
     * @param integer $nOrderID ID заказа
     * @param integer|bool $nOrderUserID ID пользователя заказа или FALSE (получаем из БД)
     * @return boolean
     */
    public function isOrderOwner($nOrderID, $nOrderUserID = false)
    {
        $nUserID = User::id();
        if (!$nUserID) {
            return false;
        }

        if ($nOrderUserID === false) {
            $aData = $this->model->orderData($nOrderID, array('user_id'));
            if (empty($aData)) {
                return false;
            }

            $nOrderUserID = $aData['user_id'];
        }

        return ($nOrderUserID > 0 && $nUserID == $nOrderUserID);
    }

    /**
     * Является ли текущий пользователь владельцем заявки
     * @param integer $nOfferID ID заявки
     * @param integer|bool $nOfferUserID ID пользователя заявки или FALSE (получаем из БД)
     * @return boolean
     */
    public function isOfferOwner($nOfferID, $nOfferUserID = false)
    {
        $nUserID = User::id();
        if (!$nUserID) {
            return false;
        }

        if ($nOfferUserID === false) {
            $aData = $this->model->offerData($nOfferID, array('user_id'));
            if (empty($aData)) {
                return false;
            }

            $nOfferUserID = $aData['user_id'];
        }

        return ($nOfferUserID > 0 && $nUserID == $nOfferUserID);
    }

    /**
     * Формируем ключ активации заказа
     * @return array (code, link, expire)
     */
    function getActivationInfo()
    {
        $aData = array();
        $aData['key'] = md5(uniqid('ASDAS(D90--00];&%#97665.,:{}' . BFF_NOW, true));
        $aData['link'] = self::url('item.activate', array('c' => $aData['key']));
        $aData['expire'] = date('Y-m-d H:i:s', strtotime('+1 day'));

        return $aData;
    }

    /**
     * Удаление заказа
     * @param $nOrderID
     * @return bool
     */
    public function orderDelete($nOrderID)
    {
        $aData = $this->model->orderData($nOrderID);
        if (empty($aData)) {
            return false;
        }

        # удаляем изображение
        $oImages = $this->orderImages($nOrderID);
        $oImages->deleteAllImages(false);
        # удаляем прикрепленные файлы
        $oAttach = $this->orderAttachments($nOrderID);
        $oAttach->deleteAllAttachments(false);

        $res = $this->model->orderDelete($nOrderID);
        if ($res) {
            # обновим счетчик кол-ва заказов пользователя
            $this->security->userCounter('orders', $this->model->ordersCnt($aData['user_id']), $aData['user_id'], false);
            # обновляем счетчик заказов "на модерации"
            $this->moderationCounterUpdate();
            # теги
            $this->orderTags()->onItemDelete($nOrderID);
        }

        return $res;
    }

    /**
     * Удаление неактивированных заказов
     */
    public function ordersDeleteUnactivated()
    {
        $aData = $this->model->ordersUnactivated();
        if ( ! empty($aData)) {
            foreach ($aData as $id) {
                $this->orderDelete($id);
            }
        }
    }

    /**
     * Удаление заявки
     * @param integer|array $mOfferID ID заявки (нескольких заявок)
     * @param bool $bUpdateCounters выполнять пересчет счетчиков
     * @return bool
     */
    public function offerDelete($mOfferID, $bUpdateCounters = true)
    {
        if (!is_array($mOfferID)) {
            $mOfferID = array($mOfferID);
        }

        foreach ($mOfferID as $v) {
            $this->offerExamples($v)->deleteAllExamples();
        }

        $res = $this->model->offersDelete($mOfferID, $bUpdateCounters);
        if ($res) {
            # обновляем счетчик заявок "на модерации"
            $this->moderationOffersCounterUpdate();
        }

        return $res;
    }

    /**
     * Событие изменение курса валюты
     * @param integer $nCurrencyID ID валюты
     */
    public function onCurrencyRateChange($nCurrencyID)
    {
        $this->model->onCurrencyRateChange($nCurrencyID);
    }

    /**
     * Актуализация счетчика заказов ожидающих модерации
     * @param integer|null $increment
     */
    public function moderationCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->ordersModeratingCounter();
            config::save('orders_orders_moderating', $count, true);
        } else {
            config::saveCount('orders_orders_moderating', $increment, true);
        }
    }

    /**
     * Актуализация счетчика заявок ожидающих модерации
     * @param integer|null $increment
     */
    public function moderationOffersCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->offersModeratingCounter();
            config::save('orders_offers_moderating', $count, true);
        } else {
            config::saveCount('orders_offers_moderating', $increment, true);
        }
    }

    /**
     * Старт рассылки уведомлений исполнителей о новом заказе
     * @param integer $nOrderID ID заказа
     */
    public function enotifyNewOrder($nOrderID)
    {
        $aData = $this->model->orderData($nOrderID, array('type','status','title','descr','keyword','approved', 'visibility'));
        if (empty($aData)) return;
        # не уведомляем о приватных заказах
        if (static::invitesEnabled() && $aData['visibility'] == static::VISIBILITY_PRIVATE) return;

        # если проверка заказа уже выполнялась, уведомлять о нем нет необходимости (он не новый)
        if ($aData['approved'] != '0000-00-00 00:00:00') return;

        # помечаем дату проверки заказа
        $this->model->orderSave($nOrderID, array('approved' => $this->db->now()));
        # уведомление выполняется только для заказов типа "Услуга"
        if ($aData['type'] != static::TYPE_SERVICE) return;

        # мгновенная рассылка
        if (config::get('orders_enotify_neworders_type') == static::ENOTIFY_NEWORDERS_NOW)
        {
            $nReceiversTotal = $this->model->makeEnotifyNewOrderNow($nOrderID, 0);
            if ( ! $nReceiversTotal) return;

            # получаем название специализации заказа
            $sSpecTitle = '?';
            $aSpecs = $this->model->orderSpecs($nOrderID);
            if (!empty($aSpecs)) {
                $aSpec = reset($aSpecs);
                if ( ! empty($aSpec)) {
                    $sSpecTitle = $aSpec['spec_title'];
                }
            }

            $aMailData = array(
                'order_id'      => $nOrderID,
                'order_title'   => $aData['title'],
                'order_url'     => static::url('view', array('id' => $nOrderID, 'keyword' => $aData['keyword'])),
                'order_descr'   => tpl::truncate($aData['descr'], config::sysAdmin('orders.enotifyNewOrder.descr.truncate', 150, TYPE_UINT)),
                'spec_title'    => $sSpecTitle,
            );
            $aTplData = Sendmail::i()->getMailTemplate('orders_order_new_now', $aMailData);

            $this->model->makeEnotifyNewOrderNow($nOrderID, $nReceiversTotal, $aTplData);
        }
    }

    /**
     * Активация услуги для пользователя
     * @param integer $nOrderID ID заказа
     * @param integer $nSvcID ID услуги (услуг)
     * @param mixed $aSvcData данные об услуге(*)/пакете услуг или FALSE
     * @param array $aSvcSettings @ref дополнительные параметры услуги / нескольких услуг / пакета услуг
     * @return boolean true - успешная активация, false - ошибка активации
     */
    public function svcActivate($nOrderID, $nSvcID, $aSvcData = false, array &$aSvcSettings = array())
    {
        if (!$nSvcID) {
            $this->errors->set(_t('svc', 'Неудалось активировать услугу'));

            return false;
        }
        $aData = $this->model->orderData($nOrderID, array('status', 'svc', 'svc_fixed_to', 'svc_marked_to', 'user_id', 'title', 'keyword'));
        if(empty($aData) || $aData['status'] != static::STATUS_OPENED){
            $this->errors->set(_t('svc', 'Неудалось активировать услугу'));
            return false;
        }

        $svcData = array();
        foreach(array(static::SVC_UP, static::SVC_HIDDEN, static::SVC_MARK, static::SVC_FIX) as $v){
            if($nSvcID & $v){
                $svcData[] = $v;
            }
        }
        $svcData = Svc::model()->svcData($svcData, 'id, keyword, settings, type');
        foreach($svcData as $k => $v){
            $svcData[$v['id']] = $v;
            unset($svcData[$k]);
        }

        $notifyTitle = '';
        $notifyDescr = array();
        $notifyCnt = 0;

        # хуки
        $customActivation = bff::filter('orders.svc.activate', $nSvcID, $svcData, $nOrderID, $aData, $aSvcSettings);
        if (is_bool($customActivation)) {
            return $customActivation;
        }

        $aUpdate = array('svc' => $aData['svc']);
        if($nSvcID & static::SVC_UP){
            $aUpdate['svc_order'] = $this->db->now();
            if($aData['svc'] & static::SVC_FIX){
                $aUpdate['svc_fixed_order'] = $this->db->now();
            }

            $notifyTitle = $svcData[static::SVC_UP]['title_view'][LNG];
            $notifyDescr[] = $notifyTitle;
            $notifyCnt++;
        }
        if($nSvcID & static::SVC_HIDDEN){
            $aUpdate['svc'] |= static::SVC_HIDDEN;

            $notifyTitle = $svcData[static::SVC_HIDDEN]['title_view'][LNG];
            $notifyDescr[] = $notifyTitle;
            $notifyCnt++;
        }
        if($nSvcID & static::SVC_MARK){
            $aSvc = $svcData[static::SVC_MARK];
            $days = $aSvc['period'];
            $now = time();
            if($aData['svc'] & static::SVC_MARK){
                # продление
                $from = strtotime($aData['svc_marked_to']);
                if($from < $now){
                    $from = $now;
                }
                $to = strtotime('+ '.$days.'day'.($days>1?'s':''), $from);
            } else {
                # активация новой
                $from = $now;
                $aUpdate['svc'] |= static::SVC_MARK;
                $to = strtotime('+ '.$days.'day'.($days>1?'s':''));
            }
            $aSvcSettings[static::SVC_MARK]['from'] = $from;
            $aSvcSettings[static::SVC_MARK]['to'] = $to;
            $aUpdate['svc_marked_to'] = date('Y-m-d H:i:s', $to);

            $notifyTitle = $aSvc['title_view'][LNG];
            $notifyDescr[] = _t('svc', '[title] c [from] по [to]', array(
                'title' => $notifyTitle,
                'from' => tpl::date_format2($from),
                'to' => tpl::date_format2($to)));
            $notifyCnt++;
        }
        if($nSvcID & static::SVC_FIX){
            $aSvc = $svcData[static::SVC_FIX];
            $days = $aSvc['period'];
            if( ! empty($aSvc['per_day']) && ! empty($aSvcSettings['fixed_days'])){
                $days = $aSvcSettings['fixed_days'];
            }
            $now = time();
            if($aData['svc'] & static::SVC_FIX){
                # продление
                $from = strtotime($aData['svc_fixed_to']);
                if($from < $now){
                    $from = $now;
                }
                $to = strtotime('+ '.$days.'day'.($days>1?'s':''), $from);
            } else {
                # активация новой
                $from = $now;
                $aUpdate['svc'] |= static::SVC_FIX;
                $to = strtotime('+ '.$days.'day'.($days>1?'s':''));
            }
            $aSvcSettings[static::SVC_FIX]['from'] = $from;
            $aSvcSettings[static::SVC_FIX]['to'] = $to;
            $aUpdate['svc_fixed_to'] = date('Y-m-d H:i:s', $to);
            $aUpdate['svc_fixed_order'] = $this->db->now();

            $notifyTitle = $aSvc['title_view'][LNG];;
            $notifyDescr[] = _t('svc', '[title] c [from] по [to]', array(
                'title' => $notifyTitle,
                'from' => tpl::date_format2($from),
                'to' => tpl::date_format2($to)));
            $notifyCnt++;
        }

        bff::hook('orders.svc.activate.custom', $nSvcID, $svcData, $nOrderID, $aData, array(
            'notifyTitle'=>&$notifyTitle, 'notifyDescr'=>&$notifyDescr, 'notifyCnt'=>&$notifyCnt,
            'settings'=>&$aSvcSettings, 'update'=>&$aUpdate));

        $result = $this->model->orderSave($nOrderID, $aUpdate);
        if ($result) {
            # Отправим уведомление пользователю
            $notifyDescr = join('<br />', $notifyDescr);
            if ($notifyCnt > 1) {
                Users::sendMailTemplateToUser($aData['user_id'], 'svc_activated_multiple', array(
                    'description' => $notifyDescr,
                    'item_id'     => $nOrderID,
                    'item_title'  => $aData['title'],
                    'item_link'   => static::url('view', array('id' => $nOrderID, 'keyword' => $aData['keyword'])),
                ), Users::ENOTIFY_GENERAL);
            } else {
                Users::sendMailTemplateToUser($aData['user_id'], 'svc_activated_single', array(
                    'title'       => $notifyTitle,
                    'description' => $notifyDescr,
                    'item_id'     => $nOrderID,
                    'item_title'  => $aData['title'],
                    'item_link'   => static::url('view', array('id' => $nOrderID, 'keyword' => $aData['keyword'])),
                ), Users::ENOTIFY_GENERAL);
            }
        }
        return $result;
    }

    /**
     * Формируем описание счета активации услуги (пакета услуг)
     * @param integer $nOrderID ID заказа
     * @param integer $nSvcID ID услуги
     * @param mixed $aData данные об услуге(*)/пакете услуг или FALSE
     * @param array $aSvcSettings @ref дополнительные параметры услуги / нескольких услуг / пакета услуг
     * @return string
     */
    public function svcBillDescription($nOrderID, $nSvcID, $aData = false, array &$aSvcSettings = array())
    {
        $aData = $this->model->orderData($nOrderID, array('keyword', 'title'));

        $nSvcIDs = $nSvcID;
        if( ! empty($aSvcSettings['svc'])){
            foreach($aSvcSettings['svc'] as $v){
                $nSvcIDs |= $v;
            }
        }

        $sReturn = '';
        if($nSvcIDs & static::SVC_UP){
            $sReturn = _t('svc', 'Поднятие');
        }
        if($nSvcIDs & static::SVC_MARK){
            if($sReturn) $sReturn .= ', '._t('svc', 'выделение');
            else $sReturn = _t('svc', 'Выделение');
            if( ! empty($aSvcSettings[static::SVC_MARK]['from']) && ! empty($aSvcSettings[static::SVC_MARK]['to'])) {
                $sReturn .= _t('scv', '(по [to])', array(
                    'to'   => tpl::date_format2($aSvcSettings[static::SVC_MARK]['to'])));
            }
        }
        if($nSvcIDs & static::SVC_FIX){
            if($sReturn) $sReturn .= ', '._t('svc', 'закрепление');
            else $sReturn = _t('svc', 'Закрепление');
            if( ! empty($aSvcSettings[static::SVC_FIX]['from']) && ! empty($aSvcSettings[static::SVC_FIX]['to'])) {
                $sReturn .= _t('scv', '(по [to])', array(
                    'to'   => tpl::date_format2($aSvcSettings[static::SVC_FIX]['to'])));
            }
        }
        if($nSvcIDs & static::SVC_HIDDEN){
            if($sReturn) $sReturn .= ', '._t('svc', 'скрытие');
            else $sReturn = _t('svc', 'Скрытие');
        }
        $sReturn = bff::filter('orders.svc.description.custom', $sReturn, $nSvcIDs, $aSvcSettings, $nOrderID, $aData);
        $sReturn .= ' '._t('svc', 'заказа');
        $sReturn .= '<br /><a href="'.static::url('view', array('id' => $nOrderID, 'keyword' => $aData['keyword']), false).'">'.$aData['title'].'</a>';
        return $sReturn;
    }

    /**
     * Инициализация компонента обработки иконок услуг/пакетов услуг OrdersSvcIcon
     * @param mixed $nSvcID ID услуги / пакета услуг
     * @return OrdersSvcIcon component
     */
    public static function svcIcon($nSvcID = false)
    {
        static $i;
        if (!isset($i)) {
            $i = new OrdersSvcIcon();
        }
        $i->setRecordID($nSvcID);

        return $i;
    }

    /**
     * Период: 1 раз в час
     */
    public function svcCron()
    {
        if (!bff::cron()) {
            return;
        }

        $this->model->svcCron();
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('offers', 'images')   => 'dir-split', # изображения заявок
            bff::path('orders', 'images')   => 'dir-split', # изображения заказов
            bff::path('attachments/orders') => 'dir-split', # вложения заказов
            bff::path('attachments/tmp')    => 'dir-only', # вложения tmp
            bff::path('tmp', 'images')      => 'dir-only', # tmp
        ));
    }

    /**
     * Метод обрабатывающий ситуацию с активацией пользователя
     * @param integer $userID ID пользователя
     */
    public function onUserActivated($userID)
    {
        # активируем заказы пользователя
        $orders = $this->model->ordersDataByFilter(
            array(
                'user_id' => $userID,
                'status'  => self::STATUS_NOTACTIVATED,
            ),
            array('id')
        );
        if (!empty($orders)) {
            $res = (int)$this->model->ordersSave(array_keys($orders), array(
                    'activate_key'     => '', # чистим ключ активации
                    'status_prev'      => self::STATUS_NOTACTIVATED,
                    'status'           => self::STATUS_OPENED,
                    'moderated'        => 0, # помечаем на модерацию
                )
            );
            if ($res > 0) {
                # обновляем счетчик "на модерации"
                $this->moderationCounterUpdate();
            }
        }
    }

}