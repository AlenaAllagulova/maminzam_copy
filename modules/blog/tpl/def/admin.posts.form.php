<?php
    /**
     * @var $this Blog
     */
    $aData = HTML::escape($aData, 'html', array('cat_id','title'));
    $aTabs = array(
        'info' => 'Основные',
        'images' => 'Изображения',
        # 'seo' => 'SEO',
    );
    $aData['edit'] = $edit = ! empty($id);
    if($edit){
        $aTabs['comments'] = 'Комментарии';
    }
    $tab = $this->input->getpost('ftab', TYPE_NOTAGS);
    if( ! isset($aTabs[$tab])) {
        $tab = 'info';
    }
?>
<form name="BlogPostsForm" id="BlogPostsForm" action="<?= $this->adminLink('posts') ?>" method="post" >
<input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
<input type="hidden" name="save" value="1" />
<input type="hidden" name="id" value="<?= $id ?>" />
<div class="tabsBar" id="BlogPostsFormTabs">
    <? foreach($aTabs as $k=>$v) { ?>
        <span class="tab<? if($k == $tab) { ?> tab-active<? } ?>"><a href="#" class="j-tab-toggler" data-key="<?= $k ?>"><?= $v ?></a></span>
    <? } ?>
    <? if($edit): ?><div class="right"><a target="_blank" href="<?= Blog::url('view', array('id' => $id, 'keyword' => $keyword)) ?>">Просмотр → </a></div><? endif; ?>
</div>
<div class="j-tab <?= $tab != 'info' ? 'hidden' : '' ?>" id="j-tab-info">
    <table class="admtbl tbledit">
    <tr>
        <td class="row1 field-title" width="100">Категория<span class="required-mark">*</span>:</td>
        <td class="row2">
           <select name="cat_id" id="post-cat"><?= $cats ?></select>
        </td>
    </tr>
    <tr>
        <td class="row1 field-title">Заголовок:</td>
        <td class="row2">
            <input class="stretch lang-field" type="text" id="post-title" name="title" value="<?= $title; ?>" />
        </td>
    </tr>
    <? if( ! empty($publicator)): ?>
        <tr>
            <td class="row1" colspan="2">
                <?= $publicator->form($content, $id, 'content', 'jPostFormPublicator'); ?>
            </td>
        </tr>
    <? else: ?>
    <tr>
        <td class="row1 field-title">Текст:</td>
        <td class="row2">
            <?= tpl::jwysiwyg($content, 'content', 0, 125); ?>
        </td>
    </tr>
    <? endif; ?>
    <tr>
        <td class="row1 field-title"></td>
        <td class="row2">
            <label class="checkbox">
                <input name="comments_enabled" type="checkbox" <?= $comments_enabled ? ' checked="checked"' : '' ?> />
                Комментарии разрешены
            </label>
        </td>
    </tr>
    <? if(Blog::tagsOn()){ ?>
    <tr>
        <td class="row1 field-title"><?= _t('', 'Tags') ?>:</td>
        <td class="row2">
            <?= $this->postTags()->tagsForm($id, $this->adminLink('posts&act=tags-suggest'), '700'); ?>
        </td>
    </tr>
    <? } ?>
    <?php if($edit): ?>
        <tr>
            <td class="row1 field-title"><?= _t('', 'User') ?></td>
            <td class="row2">
                <a href="#" class="ajax" onclick="return bff.userinfo(<?= $user_id ?>);"><?= $email ?></a>
            </td>
        </tr>
        <tr>
            <td class="row1" colspan="2">
                <?= $this->viewPHP($aData, 'admin.posts.form.status'); ?>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td class="row1 field-title"><?= _t('', 'User') ?><span class="required-mark">*</span></td>
            <td class="row2">
                <input type="hidden" name="user_id" value="0" id="j-post-user-id" />
                <input type="text" name="email" value="" id="j-post-user-email" class="autocomplete input-large" placeholder="<?= _t('', 'Enter user e-mail') ?>" />
            </td>
        </tr>
    <?php endif; ?>

    <tr class="footer">
        <td colspan="2">
            <input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save') ?>" onclick="jBlogPostsForm.save(false);" />
            <? if($edit) { ?><input type="button" class="btn btn-success button submit" value="<?= _t('', 'Save and back') ?>" onclick="jBlogPostsForm.save(true);" /><? } ?>
            <? if($edit) { ?><input type="button" onclick="jBlogPostsForm.del(); return false;" class="btn btn-danger button delete" value="<?= _t('', 'Delete') ?>" /><? } ?>
            <input type="button" class="btn button cancel" value="<?= _t('', 'Cancel') ?>" onclick="jBlogPostsFormManager.action('cancel');" />
        </td>
    </tr>
    </table>
</div>
<div class="j-tab <?= $tab != 'seo' ? 'hidden' : '' ?>" id="j-tab-seo">
    <?= SEO::i()->form($this, $aData, 'view'); ?>
</div>
</form>
<div class="j-tab <?= $tab != 'images' ? 'hidden' : '' ?>" id="j-tab-images"><?= $this->viewPHP($aData, 'admin.posts.form.images'); ?></div>
<? if($edit): ?>
<div class="j-tab <?= $tab != 'comments' ? 'hidden' : '' ?>" id="j-tab-comments"><?= $this->postComments()->admListing($id) ?></div>
<? endif; ?>

<script type="text/javascript">
var jBlogPostsForm =
(function(){
    var $progress, $form, formChk, id = parseInt(<?= $id ?>);
    var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

    $(function(){
        $progress = $('#BlogPostsFormProgress');
        $form = $('#BlogPostsForm');

        // tabs
        var $tabs = $('.j-tab');
        $form.find('#BlogPostsFormTabs .j-tab-toggler').on('click', function(e){ nothing(e);
            var key = $(this).data('key');
            $tabs.addClass('hidden');
            $('#j-tab-'+key).removeClass('hidden');
            $(this).parent().addClass('tab-active').siblings().removeClass('tab-active');
            if(bff.h) {
                window.history.pushState({}, document.title, ajaxUrl + '&act=<?= $act ?>&id=<?= $id ?>&ftab=' + key);
            }
        });

        <? if( ! $edit): ?>
        $form.find('#j-post-user-email').autocomplete(ajaxUrl+'&act=user',
            {valueInput: $form.find('#j-post-user-id')});
        <? endif; ?>
        
        formChk = new bff.formChecker( $form );
    });

    return {
        del: function()
        {
            if( id > 0 ) {
                bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                    false, {progress: $progress, repaint: false, onComplete:function(){
                        bff.success('Запись успешно удалена');
                        jBlogPostsFormManager.action('cancel');
                        jBlogPostsList.refresh();
                    }});
            }
        },
        save: function(returnToList)
        {
            if( ! formChk.check(true) ) return false;
            if(intval($form.find('#post-cat').val()) == 0) {
                bff.error('Выберите категорию');
                return false;
            }
            var data = $form.serialize();
            <? if( ! $edit): ?>
            data = data + '&' + jBlogPostImages.serialize();
            <? endif; ?>
            bff.ajax(ajaxUrl, data, function(data){
                if(data && data.success) {
                    bff.success('<?= _t('blog', 'Данные успешно сохранены') ?>');
                    if(returnToList || ! id) {
                        jBlogPostsFormManager.action('cancel');
                        jBlogPostsList.refresh( ! id);
                    }
                }
            }, $progress);
            return false;
        },
        onShow: function()
        {
            formChk = new bff.formChecker( $form );
        }
    };
}());
</script>