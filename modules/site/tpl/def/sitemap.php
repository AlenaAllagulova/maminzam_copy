<?php
/**
 * Карта сайта
 * @var $this Site
 * @var $blocks array
 */
?>
    <div class="container">
        <section class="l-mainContent">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <?= tpl::getBreadcrumbs($breadcrumbs); ?>
                    <h1 class="small"><?= _t('site', 'Карта сайта') ?></h1>
                    <p><a href="#" id="j-collapseAll" class="ajax-link" data-t="<?= _t('', 'Свернуть все') ?>"><i class="fa fa-expand c-link-icon"></i> <span><?= _t('', 'Развернуть все') ?></span></a></p>
                    <div class="panel-group" id="accordion-sitemap">
                        <? foreach($blocks as $k => $v): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion-sitemap" href="#sitemap-<?= $k ?>" class="ajax-link">
                                        <span><?= $v['title'] ?></span>
                                    </a>
                                    <a href="<?= $v['link'] ?>" class="panel-heading-icon" title="<?= _t('site', 'Весь раздел') ?>">
                                        <i class="fa fa-link"></i>
                                    </a>
                                </div>
                                <div id="sitemap-<?= $k ?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <?= $v['html'] ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    var $panels = $('#accordion-sitemap').find('.panel-collapse');
    var $link = $('#j-collapseAll');
    var $t = $link.find('span');
    $link.click(function(){
        var t = $link.data('t');
        $link.toggleClass('sh');
        $link.data('t', $t.html());
        $t.html(t);
        if ($link.hasClass('sh')) {
            $panels.collapse('show');
        } else {
            $panels.collapse('hide');
        }
        return false;
    });
});
<? js::stop(); ?>
</script>