<?php

abstract class SiteBase_ extends SiteModule
{
    # Тип отображения главной страницы:
    const INDEX_VIEW_ORDERS      = 1; # заказы
    const INDEX_VIEW_ORDERS_PLUS = 2; # заказы + шапка слайдер

    # Размеры логотипа:
    const LOGO_SIZE_NORMAL = 'normal';

    /** @var SiteModel */
    public $model = null;

    /** @var string Путь к изображениям */
    protected $watermarkPath = '';
    /** @var string URL к изображениям */
    protected $watermarkUrl = '';

    function __construct()
    {
        $this->watermarkPath = bff::path('images');
        $this->watermarkUrl = bff::url('images');
    }

    public static function currencyOptions($nSelectedID, $mEmpty = false)
    {
        $aCurrency = static::model()->currencyData(false);

        return HTML::selectOptions($aCurrency, $nSelectedID, $mEmpty, 'id', 'title_short');
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts доп. параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # главная страница
            case 'index':
                $url .= '/';
                break;
            # страница с описанием позиций баннеров
            case 'adv':
                $url .= '/adv.html';
                break;
            # статическая страница
            case 'page':
                $url .= '/' . $opts['filename'] . static::$pagesExtension;
                break;
            # карта сайта
            case 'sitemap':
                $url .= '/sitemap/';
                break;
        }
        return bff::filter('site.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Дополнительные параметры для редиректов
     * @param array $options доп. параметры
     * @param array $settings настройки
     * @return array
     */
    public static function urlExtra(array $options = array(), array $settings = array())
    {
        if (Geo::urlType() == Geo::URL_SUBDIR) {
            $options['region'] = array(
                'value' => function($key, $options) use ($settings) {
                    return Geo::filterUrl('keyword');
                },
                'position' => 2,
            );
        }

        return bff::filter('site.url.extra', parent::urlExtra($options, $settings));
    }

    public function init()
    {
        parent::init();

        bff::autoloadEx(array(
            'SiteIndexImages' => array('app', 'modules/site/site.index.images.php'),
        ));
    }

    /**
     * Описание seo шаблонов страниц
     * @return array
     */
    public function seoTemplates()
    {
        $templates = array(
            'groups' => array(
                'def' => array('t' => 'Общие'),
            ),
            'pages' => array(
                'page-view' => array(
                    't'       => 'Статические страницы',
                    'inherit' => true,
                    'macros'  => array(
                        'title' => array('t' => 'Заголовок страницы'),
                    ),
                ),
                'sitemap' => array(
                    't'      => 'Карта сайта',
                    'i'      => 0,
                    'macros' => array(),
                    'fields' => array(
                        'titleh1' => array(
                            't'      => 'Заголовок H1',
                            'type'   => 'text',
                        ),
                    ),
                ),
                'contact-form' => array(
                    't'       => 'Связь с администрацией',
                    'macros' => array(),
                    'fields' => array(
                        'titleh1' => array(
                            't'      => 'Заголовок H1',
                            'type'   => 'text',
                        ),
                    ),
                ),
                'services' => array(
                    't'       => 'Услуги',
                    'macros' => array(),
                    'fields' => array(
                        'titleh1' => array(
                            't'      => 'Заголовок H1',
                            'type'   => 'text',
                        ),
                    ),
                ),
                'offline'     => array(
                    't'       => _t('site','Выключение сайта'),
                    'i'       => true,
                    'macros'  => array(),
                ),
            ),
        );

        return $templates;
    }

    /**
     * Настройки ссылок соц. сетей
     * @param boolean $isView формирование данных для вывода
     * @return array
     */
    public function socialLinks($isView = false)
    {
        # Доступные типы:
        $types = bff::filter('site.social.links', array(
            'googleplus' => array('t'=>'Google+',  'icon'=>'fa fa-google-plus'),
            'linkedin'   => array('t'=>'LinkedIn', 'icon'=>'fa fa-linkedin'),
            'facebook'   => array('t'=>'Facebook', 'icon'=>'fa fa-facebook'),
            'twitter'    => array('t'=>'Twitter',  'icon'=>'fa fa-twitter'),
            'vk'         => array('t'=>'Вконтакте', 'icon'=>'fa fa-vk'),
            'odnoklassniki' => array('t'=>'Одноклассники', 'icon'=>'fa fa-circle-o'),
//            'instagram'     => array('t'=>'Instagram',    'icon'=>'fa fa-instagram'),
//            'flickr'     => array('t'=>'Flickr',    'icon'=>'fa fa-flickr'),
//            'html5'      => array('t'=>'HTML5',     'icon'=>'fa fa-html5'),
//            'pinterest'  => array('t'=>'Pinterest', 'icon'=>'fa fa-pinterest'),
//            'skype'      => array('t'=>'Skype',     'icon'=>'fa fa-skype'),
//            'tumblr'     => array('t'=>'Tumblr',    'icon'=>'fa fa-tumblr'),
//            'youtube'    => array('t'=>'Youtube',   'icon'=>'fa fa-youtube'),
            /**
             * При создании нового типа необходимо указать уникальный ключ и название типа
             * Класс иконки необходимо выбрать из доступных http://fortawesome.github.io/Font-Awesome/icons/,
             *  либо загрузить иконку в директорию /public_html/img и прописать соответствующий класс в файле /public_html/css/main.css
             */
        ));
        # Получаем текущие настройки ссылок
        foreach ($types as $k=>&$v) {
            $v['url'] = config::get('social_'.$k, false);
            if ($isView && empty($v['url'])) {
                unset($types[$k]);
            }
        } unset($v);

        return $types;
    }

    /**
     * Получение данных о текущих настройках водяного знака
     * @param mixed $settings array - сохраняем настройки, false - получаем текущие, 'module' - текущие с учетом модуля
     * @param mixed
     */
    public function watermarkSettings($settings = false)
    {
        $configKey = 'images_watermark';
        if (!is_array($settings)) {
            $module = (is_string($settings) ? $settings : false);
            $settings = config::get($configKey);
            $settings = func::unserialize($settings);
            $settings = $this->input->clean_array($settings, array(
                    'file'  => TYPE_ARRAY,
                    'pos_x' => TYPE_STR,
                    'pos_y' => TYPE_STR,
                    'modules' => TYPE_ARRAY_BOOL,
                )
            );
            if (!empty($module)) {
                 if (empty($settings['modules'][$module]) || empty($settings['file']['path'])) {
                    return false;
                 }
            }
            return $settings;
        } else {
            $settings = array_merge($this->watermarkSettings(), (array)$settings);
            config::save($configKey, serialize($settings));
        }
    }

    /**
     * Сохранение настроек водяного знака
     * @param string $fileUpload ключ для загрузки файла
     * @param boolean $fileDelete выполнить удаление файла (ранее загруженного)
     * @param string $positionX ключ позиции знака по-вертикали
     * @param string $positionY ключ позиции знака по-горизонтали
     * @param array $modules модули
     */
    public function watermarkSave($fileUpload, $fileDelete, $positionX, $positionY, $modules)
    {
        $settings = array();

        if (!in_array($positionX, array('left', 'center', 'right'))) {
            $positionX = 'right';
        }
        $settings['pos_x'] = $positionX;

        if (!in_array($positionY, array('top', 'center', 'bottom'))) {
            $positionY = 'bottom';
        }
        $settings['pos_y'] = $positionY;
        $settings['modules'] = $modules;

        if ($fileDelete) {
            $current = $this->watermarkSettings();
            if (!empty($current['file']['path']) && file_exists($current['file']['path'])) {
                unlink($current['file']['path']);
            }
            $settings['file'] = array();
        }

        if (!empty($_FILES[$fileUpload]) && $_FILES[$fileUpload]['error'] != UPLOAD_ERR_NO_FILE) {
            $uploader = new \bff\files\Attachment($this->watermarkPath, 5242880); # до 5мб.
            $uploader->setFiledataAsString(false);
            $uploader->setAllowedExtensions(array('jpg','jpeg','png','gif'));
            $file = $uploader->uploadFILES($fileUpload);
            if (!empty($file)) {
                $file['path'] = $this->watermarkPath . $file['filename'];
                $file['url'] = $this->watermarkUrl . $file['filename'];
                $settings['file'] = $file;
            }
        }

        $this->watermarkSettings($settings);
    }

    /**
     * Спам фильтр
     * @param array $params данные для фильтра в формате:
     *   array(
     *       array('text'  => & $var ссылка на проверяемый текст,
     *             'error' => сообщение об ошибке с макросом [word]),
     *   )
     * @return bool true - если найден спам
     */
    public function spamFilter(array $params = array())
    {
        $spamFound = false;
        $antimatEnabled = config::get('spam_antimat', false);
        foreach ($params as $v) {
            if ( ! isset($v['text'])) continue;
            if ($this->spamFilterMinusWords($v['text'], $word)) {
                $error = isset($v['error']) ? $v['error'] : _t('', 'Найдено запрещенное слово "[word]"');
                $error = strtr($error, array('[word]' => $word));
                $this->errors->set($error);
                $spamFound = true;
                continue;
            }
            if ($antimatEnabled) {
                $v['text'] = \bff\utils\TextParser::antimat($v['text']);
            }
        }
        return $spamFound;
    }

    /**
     * Поиск "минус слов" в тексте
     * @param string $text текст
     * @param string $wordFound @ref найденное слово
     * @return bool true - нашли минус слово, false - нет
     */
    public function spamFilterMinusWords($text, &$wordFound = '')
    {
        static $minusWords;
        if ( ! isset($minusWords)) {
            $minusWords = func::unserialize(config::get('spam_minuswords', false));
        }
        if (empty($minusWords[LNG])) return false;
        return \bff\utils\TextParser::minuswordsSearch($text, $wordFound, $minusWords[LNG]);
    }

    /**
     * Проверка на временный e-mail
     * @param $email
     * @return bool
     */
    public function spamEmailTemporary($email)
    {
        if (config::get('spam_emailtemp', false)) {
            return $this->input->isEmailTemporary($email);
        }
        return false;
    }

    /**
     * Тип отображения главной страницы
     * @param integer $type тип
     * @return integer|boolean
     */
    public static function indexView($type = false)
    {
        $current = config::get('site_index_view', static::INDEX_VIEW_ORDERS, TYPE_UINT);
        if (!empty($type)) {
            return ($current == $type);
        }
        return $current;
    }

    /**
     * Инициализация компонента SiteIndexImages
     * @return SiteIndexImages
     */
    public function indexImages()
    {
        static $i;
        if (!isset($i)) {
            $i = new SiteIndexImages();
        }
        $i->setRecordID(1);
        return $i;
    }

    /**
     * Настройки отображаемых счетчиков статистики в шапке + слайдер
     * @return array
     */
    public static function indexHeaderSliderStatistic()
    {
        $data = array(
            /* ключ => array(title => название, icon => класс иконки, module => название модуля, data => ключ необходимых данных модуля) */
            'orders'   => array('title' => _t('orders', 'Заказов'),  'icon' => 'fa fa-check-square',    'module' => 'orders',   'data' => 'orders'),
            'qa'       => array('title' => _t('qa', 'Вопросов'),     'icon' => 'fa fa-question-circle', 'module' => 'qa',       'data' => 'questions'),
            'users'    => array('title' => _t('users', 'Мастеров'),  'icon' => 'fa fa-users',           'module' => 'users',    'data' => 'workers'),
            'blog'     => array('title' => _t('blog', 'Блогов'),     'icon' => 'fa fa-comments',        'module' => 'blog',     'data' => 'posts'),
            'shop'     => array('title' => _t('shop', 'Товаров'),    'icon' => 'fa fa-shopping-cart',   'module' => 'shop',     'data' => 'products'),
            'articles' => array('title' => _t('articles', 'Статей'), 'icon' => 'fa fa-file',            'module' => 'articles', 'data' => 'articles'),
        );
        foreach (array('articles','blog','qa','shop') as $k) {
            if (isset($data[$k]) && (!bff::moduleExists($data[$k]['module']) || !$data[$k]['module']::enabled())){
                unset($data[$k]);
            }
        }
        foreach ($data as $k=>&$v) {
            $v['id'] = $k;
        } unset ($v);

        return bff::filter('site.index.header.slider.statistic', $data);
    }

    /**
     * Данные статистики
     * @param bool $resetCache сбросить кеш
     * @return array|mixed
     */
    public function statisticData($resetCache = false)
    {
        $cache = Cache::singleton('site', 'file', array('lifeTime'=>(15*60) /* 15 минут */));
        $cacheKey = 'statistic-data';
        if ($resetCache === true || ($data = $cache->get($cacheKey)) === false)
        {
            $data = array();
            foreach (bff::i()->getModulesList() as $module=>$moduleParams) {
                $model = bff::model($module);
                if (method_exists($model, 'statisticData')) {
                    $data[$module] = $model->statisticData();
                }
            } unset($v);
            $cache->set($cacheKey, $data);
        }
        return $data;
    }

    /**
     * Логотип сайта
     * Фильтры: 'site.logo.url', 'site.logo.url.{size}.{position}'
     * @param string $position информация о позиции отображения логотипа
     * @param string $size размер логотипа: self::LOGO_SIZE_NORMAL
     * @return string URL требуемого логотипа
     */
    public static function logoURL($position = '', $size = self::LOGO_SIZE_NORMAL)
    {
        static $list;
        if (!isset($list)) {
            $list = bff::filterSys('site.logo.url.sizes', array(
                self::LOGO_SIZE_NORMAL => '/img/logo-m.svg',
            ));
        }
        if (!isset($list[$size])) {
            $size = key($list);
        }
        $url = bff::filterSys('site.logo.url.'.$position, $list[$size], $size);
        return (mb_stripos($url, SITEURL_STATIC) === 0 ? $url : bff::url($url));
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('index', 'images') => 'dir-only', # главная: изображения
        ));
    }

    public static function btnSearchUrl()
    {
        $url = '';

        if(!User::id() || User::isClient()){
            $url = Users::url('list');
        }elseif (User::isWorker()){
            $url = Orders::url('list');
        }

        return $url;
    }

    /**
     * Check profile page
     * @return bool
     */
    public function isProfilePage()
    {
        if ((boolean)strstr($this->input->server('REQUEST_URI'), '/user/')){
            return true;
        }
        return false;

    }

    /**
     * Check settings questionary page
     * @return bool
     */
    public static function isSettingsQuestionaryPage(){
        if ((boolean)strstr(Request::uri(), 'settings?tab=questionary')){
            return true;
        }
        return false;
    }

    /**
     * Check users listing page
     * @return bool
     */
    public static function isUsersListingPage()
    {
        if ((boolean)strstr(Site::i()->input->server('REQUEST_URI'), '/users/')){
            return true;
        }
        return false;

    }

    /**
     * Check is current user profile page
     * @return bool
     */
    public static function isCurrentUserProfilePage()
    {
        $aUserData = User::data(['login'], true);
        if ((boolean)strstr(Site::i()->input->server('REQUEST_URI'), '/user/'. $aUserData['login']. '/')){
            return true;
        }
        return false;

    }

}