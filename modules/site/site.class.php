<?php

class Site_ extends SiteBase
{
    public function index()
    {
        if(User::isWorker()){
            $this->redirect(Orders::url('list'));
        }
        if(User::isClient()){
            $this->redirect(Users::url('list'));
        }
        $aData = array();
        return $this->viewPHP($aData, 'index');
    }

    /**
     * Шапка + cлайдер на главной
     * @return string HTML
     */
    public function index_header_slider()
    {
        $data = array();

        # статистика
        $statData = $this->statisticData();
        $stat = false;
        if (config::get('site_index_view_stat', 0)) {
            $stat = static::indexHeaderSliderStatistic();
            foreach ($stat as $k => &$v) {
                if ( ! isset($statData[$v['module']][$v['data']]) || ! config::get('site_index_view_stat_'.$k, false)) {
                    unset($stat[$k]); continue;
                }
                $v['data'] = $statData[$v['module']][$v['data']];
            } unset($v);
        }
        $data['stat'] = &$stat;

        # изображения
        $img = $this->indexImages();
        $data['images'] = $img->getData();
        foreach ($data['images'] as &$v) {
            $v['link'] = $img->getURL($v, SiteIndexImages::szOrginal);
        } unset($v);

        return $this->viewPHP($data, 'index.header.slider');
    }

    /**
     * Статические страницы
     */
    public function pageView()
    {
        $sFilename = $this->input->get('page', TYPE_NOTAGS);
        $aData = $this->model->pageDataView($sFilename);
        if (empty($aData)) {
            $this->errors->error404();
        }
        if (!empty($aData['noindex'])) {
            $this->seo()->robotsIndex(false); # Скрываем от поисковиков
        }

        # SEO: Статические страницы
        $this->urlCorrection(static::url('page', array('filename' => $sFilename)));
        $this->seo()->canonicalUrl(static::url('page', array('filename' => $sFilename), true));
        $this->setMeta('page-view', array('title' => $aData['title']), $aData);

        return $this->viewPHP($aData, 'page.view');
    }

    /**
     * Site offline mode
     */
    public function offlinePage()
    {
        $aData = array(
            'offlineReason' => config::get('offline_reason_'.LNG),
        );
        $this->setMeta('offline', array(), $aData);
        View::setLayout(false);
        $template = View::template('offline', $aData);
        $layout = View::getLayout();
        if ($layout !== false) {
            $data['centerblock'] = $template;
            return View::renderLayout($data, $layout);
        }
        return $template;
    }

    /**
     * Страница "Карта сайта"
     */
    public function sitemap()
    {
        $aData = array();

        # SEO: Карта сайта
        $this->urlCorrection(static::url('sitemap'));
        $this->seo()->canonicalUrl(static::url('sitemap', array(), true));
        $this->setMeta('sitemap', array(), $aData);

        # Хлебные крошки
        $aData['breadcrumbs'] = array(
            array('title' => _t('site', 'Карта сайта'), 'active' => true),
        );

        $aData['blocks'] = array();
        do {

            # кешируем на Х секунд
            $cacheOn = ! BFF_LOCALHOST;
            if ($cacheOn) {
                $cache = Cache::singleton('site', 'file', array('lifeTime'=>180));
                $cacheKey = 'sitemap-blocks-'.LNG;
                if (($aData['blocks'] = $cache->get($cacheKey)) !== false) break;
            }

            # Заказы
            $aPanel = Orders::i()->getSitemapData();
            if (isset($aPanel['products'])) {
                $aData['blocks'][] = array(
                    'title' => _t('orders', 'Заказы - услуги'),
                    'link'  => $aPanel['services']['link'],
                    'html'  => $this->sitemapBlock($aPanel['services']),
                );
                $aData['blocks'][] = array(
                    'title' => _t('orders', 'Заказы - товары'),
                    'link'  => $aPanel['products']['link'],
                    'html'  => $this->sitemapBlock($aPanel['products']),
                );
            } else {
                $aData['blocks'][] = array(
                    'title' => _t('orders', 'Заказы'),
                    'link'  => $aPanel['services']['link'],
                    'html'  => $this->sitemapBlock($aPanel['services']),
                );
            }

            # Исполнители
            $aPanel = Users::i()->getSitemapData();
            $aData['blocks'][] = array(
                'title' => _t('users', 'Исполнители'),
                'link'  => $aPanel['link'],
                'html'  => $this->sitemapBlock($aPanel),
            );

            # Магазин
            if (bff::moduleExists('shop') && Shop::enabled()) {
                $aPanel = Shop::i()->getSitemapData();
                $aData['blocks'][] = array(
                    'title' => _t('shop', 'Магазин'),
                    'link'  => $aPanel['link'],
                    'html'  => $this->sitemapBlock($aPanel),
                );
            }

            # Ответы
            if (bff::moduleExists('qa') && Qa::enabled()) {
                $aPanel = Qa::i()->getSitemapData();
                $aData['blocks'][] = array(
                    'title' => _t('qa', 'Ответы'),
                    'link'  => $aPanel['link'],
                    'html'  => $this->sitemapBlock($aPanel),
                );
            }

            # Блоги
            if (bff::moduleExists('blog') && Blog::enabled()) {
                $aPanel = Blog::i()->getSitemapData();
                $aData['blocks'][] = array(
                    'title' => _t('blog', 'Блоги'),
                    'link'  => $aPanel['link'],
                    'html'  => $this->sitemapBlock($aPanel),
                );
            }

            # Новости
            $aPanel = News::i()->getSitemapData();
            $aData['blocks'][] = array(
                'title' => _t('news', 'Новости'),
                'link'  => $aPanel['link'],
                'html'  => $this->sitemapBlock($aPanel),
            );

            # Статьи
            if (bff::moduleExists('articles') && Articles::enabled()) {
                $aPanel = Articles::i()->getSitemapData();
                $aData['blocks'][] = array(
                    'title' => _t('articles', 'Статьи'),
                    'link'  => $aPanel['link'],
                    'html'  => $this->sitemapBlock($aPanel['categories']).
                               $this->sitemapBlock($aPanel['themes']),
                );
            }

            # Помощь
            $aPanel = Help::i()->getSitemapData();
            $aData['blocks'][] = array(
                'title' => _t('help', 'Помощь'),
                'link'  => $aPanel['link'],
                'html'  => $this->sitemapBlock($aPanel),
            );

            if($cacheOn) $cache->set($cacheKey, $aData['blocks']);
        } while(false);

        return $this->viewPHP($aData, 'sitemap');
    }

    /**
     * Формирование раздела карты сайта
     * @param array $aData @ref
     * @return string
     */
    protected function sitemapBlock(& $aData)
    {
        do
        {
            $template = 'sitemap.subcat';
            if (isset($aData['cats'])) {
                $bSub = false;
                $bEmptSub = false;
                foreach ($aData['cats'] as $v) {
                    if ( ! empty($v['sub'])) {
                        $bSub = true;
                    } else {
                        $bEmptSub = true;
                    }
                }
                if ($bSub && ! $bEmptSub) {
                    # все с sub
                    $template = 'sitemap.subcat';
                } else if ($bEmptSub && ! $bSub) {
                    # все без sub
                    $template = 'sitemap.cat';
                } else {
                    # часть с sub, часть без
                    $aCat = array();
                    $aSub = array();
                    foreach ($aData['cats'] as $v) {
                        if ( ! empty($v['sub'])) {
                            if ( ! empty($aSub)) {
                                $aCat[] = array('sub' => $aSub);
                                $aSub = array();
                            }
                            $aCat[] = $v;
                        } else {
                            $aSub[] = $v;
                        }
                    }
                    if ( ! empty($aSub)) {
                        $aCat[] = array('sub' => $aSub);
                    }
                    $aData['cats'] = $aCat;
                    $template = 'sitemap.subcat';
                }
            } else if (isset($aData['tags'])) {
                $template = 'sitemap.tags';
            }

        } while (false);

        return $this->viewPHP($aData, $template);
    }

    /**
     * Cron: Формирование файла Sitemap.xml, Self-cleaning
     * Рекомендуемый период: раз в сутки
     */
    public function cronSitemapXML()
    {
        $data = array();

        # Посадочные страницы
        if (SEO::landingPagesEnabled()) {
            $data['landingpages'] = SEO::model()->landingpagesSitemapXmlData();
        }

        # Заказы
        $data['items'] = Orders::model()->ordersSitemapXmlData();

        # Дополнительно
        $data = bff::filter('site.cron.sitemapXML', $data);

        # Строим XML
        ini_set('memory_limit', '2048M');
        $sitemap = new CSitemapXML();
        $sitemap->setPing(config::sysAdmin('site.sitemapXML.ping', true, TYPE_BOOL));
        $sitemap->buildIterator($data, 'sitemap', bff::path(''), bff::url(''), config::sysAdmin('site.sitemapXML.gzip', true, TYPE_BOOL));

        # Self-cleaning:

        # Удаление временных файлов изображений / файлов
        $this->temporaryDirsCleanup(array(
            bff::path('tmp', 'images'),
            bff::path('attachments/tmp')
        ));

        # Сброс автоматических meta
        if ($this->input->getpost('meta-reset', TYPE_BOOL)) {
            $this->seo()->metaReset(array(
                join('_', array($this->module_name,'meta','main')),
            ));
        }

        # Очистка неактуальных запросов действий
        $this->model->requestsClear();
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {

        return array(
            'cronSitemapXML' => array('period' => '0 0 * * *'),
        );
    }

    public function ajax()
    {
        $aResponse = array();

        switch ($this->input->getpost('act', TYPE_STR)) {
            default:
            {
                $this->errors->impossible();
            }
        }

        $aResponse['res'] = $this->errors->no();
        $this->ajaxResponse($aResponse);
    }

}