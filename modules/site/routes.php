<?php

return [
    # главная страница
    'index' => [
        'pattern'  => '',
        'callback' => 'site/index/',
        'priority' => 10,
    ],
    # карта сайта
    'sitemap' => [
        'pattern'  => 'sitemap(.*)',
        'callback' => 'site/sitemap/',
        'priority' => 470,
    ],
    # статические страницы
    'pages' => [
        'pattern'  => '([a-z0-9\-]+)\.html',
        'callback' => 'site/pageView/page=$1',
        'priority' => 510,
    ],
];