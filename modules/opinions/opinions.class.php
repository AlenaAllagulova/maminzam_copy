<?php

class Opinions_ extends OpinionsBase
{
    /**
     * Кабинет: Отзывы
     * @param array $userData данные о пользователе
     */
    public function my_opinions(array $userData)
    {
        return $this->listing($userData['id'], $userData);
    }

    /**
     * Профиль: Отзывы
     * @param array $userData данные о пользователе
     * @return string HTML
     */
    public function user_opinions(array $userData)
    {
        return $this->listing($userData['id'], $userData);
    }

    /**
     * Список отзывов пользователя или о пользователе
     * @param integer $userID ID пользователя
     * @param array $userData данные о пользователе(если есть)
     * @return string HTML
     */
    protected function listing($userID, array $userData)
    {
        if ( ! $userID) {
            $this->errors->error404();
        }
        $f = $this->input->postgetm(array(
            'page' => TYPE_UINT, # № страницы
            't'    => TYPE_UINT, # тип отзыва
            'tm'   => TYPE_UINT, # за период
            'd'    => TYPE_UINT, # направление (0 - отзывы о пользователе; 1 - отзывы пользователя)
        ));

        $seoFilter = 0;

        $aFilter = array();
        if ($f['d']) {
            $aFilter['author_id'] = $userID;
            $seoFilter++;
        } else {
            $aFilter['user_id'] = $userID;
        }

        if ($f['t']) {
            $aTypes = static::aTypes();
            $aTypes = array_keys($aTypes);
            if (in_array($f['t'], $aTypes)) {
                $aFilter['type'] = $f['t'];
                $seoFilter++;
            } else {
                $f['t'] = 0;
            }
        }

        if ($f['tm']) {
            switch ($f['tm']) {
                case 1: # За последний год
                    $aFilter[':created'] = array('O.created > :created', ':created' => date('Y-m-d H:i:s', strtotime('-1 year')));
                    break;
                case 2: # За последние полгода
                    $aFilter[':created'] = array('O.created > :created', ':created' => date('Y-m-d H:i:s', strtotime('-6 month')));
                    break;
                case 3: # За последний месяц
                    $aFilter[':created'] = array('O.created > :created', ':created' => date('Y-m-d H:i:s', strtotime('-1 month')));
                    break;
            }
            if (isset($aFilter[':created'])) {
                $seoFilter++;
            }
        }

        $aData = array('f'=>&$f, 'user'=>&$userData, 'pgn'=>'');
        $aData['count'] = $this->model->opinionsList($aFilter, true);
        $nPageID = 1;
        if ($aData['count']) {
            $pgn = new Pagination($aData['count'], 10, array(
                'link'  => static::url('user.listing', array('login' => $userData['login'])),
                'query' => $f,
            ));
            $aData['list'] = $this->model->opinionsList($aFilter, false, $userID, $pgn->getLimitOffset());
            $aData['pgn'] = $pgn->view();
            $nPageID = $pgn->getCurrentPage();
        } else {
            foreach (static::aTypes() as $k => $v) {
                $aData['counters'][$k] = array('id' => $k, 'cnt' => 0);
            }
        }
        $aData['list'] = $this->viewPHP($aData, 'list.ajax');
        $aData['counters'] = $this->model->opinionsCounts($aFilter);
        $aData['terms'] = array(
            0 => array('id' => 0, 't' => _t('opinions', 'За все время')),
            1 => array('id' => 1, 't' => _t('opinions', 'За последний год')),
            2 => array('id' => 2, 't' => _t('opinions', 'За последние полгода')),
            3 => array('id' => 3, 't' => _t('opinions', 'За последний месяц')),
        );
        $aData['counter'] = tpl::declension($aData['count'], _t('opinions', 'отзыв;отзыва;отзывов')).' '.mb_strtolower($aData['terms'][ $f['tm'] ]['t']);

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'pgn'      => $aData['pgn'],
                'list'     => $aData['list'],
                'counter'  => $aData['counter'],
                'counters' => $aData['counters'],
            ));
        }

        $aData['types'] = array( 0 => array('id' => 0, 'plural' => _t('', 'Все') )) + static::aTypes();
        $aData['bAllowAdd'] = $this->isAllowAdd($userID, $userData['type'], $aData['reasonMessage']);

        $aUserTypes = Users::aTypes();
        $aData['directions'] = array(
            0 => array('id' => 0, 't' => _t('opinions', 'Отзывы').' '.(Users::useClient() ? $aUserTypes[ $userData['type'] ]['dativus'] : _t('opinions', 'пользователю'))),
            1 => array('id' => 1, 't' => _t('opinions', 'Отзывы другим пользователям')),
        );

        # SEO: Профиль: Отзывы
        $this->seo()->robotsIndex( ! $seoFilter);
        $seoURL = static::url('user.listing', array('login'=>$userData['login']), true);
        $this->urlCorrection(static::urlDynamic($seoURL));
        $this->seo()->canonicalUrl($seoURL, array('page'=>$nPageID));
        $this->seo()->setPageMeta($this->users(), 'profile-opinions', array(
            'name'    => $userData['name'],
            'surname' => $userData['surname'],
            'login'   => $userData['login'],
            'city'    => $userData['city'],
            'region'  => $userData['region'],
            'country' => $userData['country'],
            'page'    => $nPageID,
        ), $aData);

        return $this->viewPHP($aData, 'list');
    }

    /**
     * Дополнительные ajax запросы
     */
    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR))
        {
            case 'opinion-add': # добавление отзыва
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                if(bff::fairplayEnabled()){ # отзывы добавляются с помощью модуля БС
                    $this->errors->reloadPage();
                    break;
                }

                $aData = $this->validateOpinionData(0, true);
                if ($this->errors->no()) {
                    if (Orders::ordersOpinions()) {
                        if (empty($aData['order_id'])) {
                            $this->errors->reloadPage();
                            break;
                        }
                        $orderID = $aData['order_id'];
                        $userID = User::id();
                        $order = Orders::model()->orderData($orderID, array('user_id', 'performer_id'));
                        if (empty($order)) {
                            $this->errors->reloadPage();
                            break;
                        }
                        if ($order['user_id'] != $userID && $order['performer_id'] != $userID) {
                            $this->errors->reloadPage();
                            break;
                        }
                        $offer = Orders::model()->offersDataByFilter(array('order_id' => $orderID, 'user_id' => $order['performer_id']), array('status'));
                        if (empty($offer) || $offer['status'] != Orders::OFFER_STATUS_PERFORMER_AGREE) {
                            $this->errors->reloadPage();
                            break;
                        }
                        $cnt = $this->model->opinionsDataByFilter(array('COUNT(id) AS id'), array(
                            'order_id'  => $orderID,
                            'author_id' => $userID,
                        ), true);
                        if ($cnt['id']) {
                            $this->errors->reloadPage();
                            break;
                        }
                        if ($userID == $order['user_id']) {
                            $aData['user_id'] = $order['performer_id'];
                        } else {
                            $aData['user_id'] = $order['user_id'];
                        }
                    } else {
                        if (!$this->isAllowAdd($aData['user_id'])) {
                            $this->errors->impossible();
                            break;
                        }
                    }
                    if($this->opinionAdd($aData)){
                        $aResponse['msg'] = _t('', 'Операция выполнена успешно.');
                    }
                }
            }
            break;
            case 'opinion-edit': # редактирование отзыва
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage(); break;
                }

                $nOpinionID = $this->input->post('id', TYPE_UINT);
                $aOpinionData = $this->model->opinionData($nOpinionID, array('id','user_id','author_id','message','moderated','type'));
                if (empty($aOpinionData)){
                    $this->errors->reloadPage(); break;
                }

                if (!User::isCurrent($aOpinionData['author_id'])) {
                    $this->errors->reloadPage(); break;
                }

                $aData = $this->validateOpinionData($nOpinionID, true);
                if ($this->errors->no())
                {
                    # помечаем на модерацию при изменении сообщения
                    if ($aData['message'] != $aOpinionData['message']) {
                        if ($aOpinionData['moderated'] != 0 && ! User::isTrusted($this->module_name)) {
                            $aData['moderated'] = 2;
                        }
                    }
                    $aData['modified'] = $this->db->now(); # Дата изменения

                    $this->model->opinionSave($nOpinionID, $aData);
                    $aResponse['cache'] = $this->onUserOpinionsChange($aOpinionData['user_id'], 'edit', $aData['type'], $aOpinionData['type'], $nOpinionID);
                    $aUserData = Users::model()->userData($aOpinionData['user_id'], array('login'));
                    # обновляем счетчик отзывов "на модерации"
                    $this->moderationCounterUpdate();
                    $aResponse['cache'] = tpl::opinions($aResponse['cache'], array('login' => $aUserData['login']));
                    Rating::updateStars($nOpinionID, $aData);

                    # Отправим уведомление
                    if (! static::premoderation() || $aOpinionData['moderated']) {
                        $this->opinionEnotify($aOpinionData, 'change');
                    }
                }
            }
            break;
            case 'opinion-delete': # удаление отзыва
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage(); break;
                }

                $nOpinionID = $this->input->post('id', TYPE_UINT);
                $aOpinionData = $this->model->opinionData($nOpinionID, array('author_id', 'user_id', 'message', 'moderated'));
                if (empty($aOpinionData)) {
                    $this->errors->impossible(); break;
                }

                if (!User::isCurrent($aOpinionData['author_id'])) {
                    $this->errors->reloadPage(); break;
                }

                if (!$this->opinionDelete($nOpinionID, $aResponse['cache'])) {
                    $this->errors->reloadPage(); break;
                }
                Rating::deleteStars($nOpinionID);

                if (!empty($aResponse['cache'])) {
                    $aUserData = Users::model()->userData($aOpinionData['user_id'], array('login'));
                    $aResponse['cache'] = tpl::opinions($aResponse['cache'], array('login' => $aUserData['login']));
                }
            }
            break;
            case 'answer-add': # добавление ответа
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nOpinionID = $this->input->post('id', TYPE_UINT);
                $aOpinionData = $this->model->opinionData($nOpinionID, array('user_id', 'answer', 'author_id'));
                if (empty($aOpinionData)) {
                    $this->errors->impossible(); break;
                }

                if (!User::isCurrent($aOpinionData['user_id'])) {
                    $this->errors->reloadPage(); break;
                }

                $aData = $this->validateAnswerData(true);
                if ($this->errors->no()) {
                    $aData['answer_created'] = $this->db->now(); # Дата создания
                    $aData['answer_modified'] = $this->db->now(); # Дата изменения

                    $this->model->opinionSave($nOpinionID, $aData);

                    # Отправим уведомление автору отзыва
                    $aAuthorData = Users::model()->userData($aOpinionData['author_id'], array('login'));
                    if (!empty($aAuthorData))
                    {
                        $aUserData = User::data(array('login','name','surname'));
                        $fio = array();
                        if (!empty($aUserData['name'])) $fio[] = $aUserData['name'];
                        if (!empty($aUserData['surname'])) $fio[] = $aUserData['surname'];
                        $aUserData['fio'] = ( ! empty($fio) ? join(' ', $fio) : $aUserData['login']);
                        Users::sendMailTemplateToUser($aOpinionData['author_id'], 'opinions_opinion_answer', array(
                            'opinion_url' => static::url('user.listing', array('login' => $aUserData['login'])).'#'.$aAuthorData['login'],
                            'user_fio'    => $aUserData['fio'],
                            'user_login'  => $aUserData['login'],
                            'user_url'    => Users::url('profile', array('login' => $aUserData['login'])),
                        ), Users::ENOTIFY_GENERAL);
                    }
                }
            }
            break;
            case 'answer-edit': # редактирование ответа
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nOpinionID = $this->input->post('id', TYPE_UINT);
                $aOpinionData = $this->model->opinionData($nOpinionID, array('user_id', 'answer'));
                if (empty($aOpinionData)) {
                    $this->errors->impossible(); break;
                }

                if (!User::isCurrent($aOpinionData['user_id'])) {
                    $this->errors->reloadPage(); break;
                }

                $aData = $this->validateAnswerData(true);
                if ($this->errors->no()) {
                    $aData['answer_modified'] = $this->db->now(); # Дата изменения

                    $this->model->opinionSave($nOpinionID, $aData);
                }
            }
            break;
            case 'answer-delete': # удаление ответа
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }

                $nOpinionID = $this->input->post('id', TYPE_UINT);
                $aOpinionData = $this->model->opinionData($nOpinionID, array('user_id', 'answer'));
                if (empty($aOpinionData)) {
                    $this->errors->impossible(); break;
                }

                if (!User::isCurrent($aOpinionData['user_id'])) {
                    $this->errors->reloadPage(); break;
                }
                if ($this->errors->no()) {
                    $aData = array(
                        'answer' => '',
                        'answer_created' => '',
                        'answer_modified' => '',
                    );

                    $this->model->opinionSave($nOpinionID, $aData);
                }
            }
            break;
            case 'modal-add': # окно для добавления отзыва из кабинета пользователя
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! Orders::ordersOpinions()) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = array();
                $aResponse['html'] = $this->viewPHP($data, 'modal.add');
            }
            break;
            case 'modal-view': # окно для просмотра отзыва из кабинета пользователя
            {
                if (!$this->security->validateToken(false, true)) {
                    $this->errors->reloadPage();
                    break;
                }
                if ( ! Orders::ordersOpinions()) {
                    $this->errors->reloadPage();
                    break;
                }
                $id = $this->input->post('id', TYPE_UINT);
                if ( ! $id) {
                    $this->errors->reloadPage();
                    break;
                }
                $data = $this->model->opinionData($id);
                if (empty($data)) {
                    $this->errors->reloadPage();
                    break;
                }
                $data['my'] = $data['author_id'] == User::id();
                if ( ! $data['my']) {
                    $data['author'] = Users::model()->userData($data['author_id'], array('user_id', 'login', 'name', 'surname', 'avatar', 'sex', 'pro', 'verified'));
                    $data['order'] = Orders::model()->orderData($data['order_id'], array('user_id'));
                }
                $aResponse['html'] = $this->viewPHP($data, 'modal.view');
            }
            break;
            default:
                $this->errors->impossible();
        }
        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Добавление отзыва
     * @param array $aData данные
     * @return bool
     */
    function opinionAdd($aData)
    {
        if(empty($aData['message'])) return false;
        if(empty($aData['type'])) return false;

        $aData['author_id'] = User::id();
        $aData['created'] = $this->db->now(); # Дата создания
        $aData['modified'] = $this->db->now(); # Дата изменения

        # доверенный пользователь: без модерации
        if ($trusted = User::isTrusted($this->module_name)) {
            $aData['moderated'] = 1;
        }
        $nOpinionID = $this->model->opinionSave(0, $aData);
        if ($nOpinionID) {
            if ($trusted || ! static::premoderation()) {
                $aResponse['cache'] = $this->onUserOpinionsChange($aData['user_id'], 'add', $aData['type'], 0, $nOpinionID);
                $aUserData = Users::model()->userData($aData['user_id'], array('login'));
                $aResponse['cache'] = tpl::opinions($aResponse['cache'], array('login' => $aUserData['login']));

                # Отправим уведомление
                $aData['id'] = $nOpinionID;
                $this->opinionEnotify($aData, 'add');
            }
            # обновляем счетчик отзывов "на модерации"
            if (!$trusted) {
                $this->moderationCounterUpdate(1);
            }
            $aResponse['id'] = $nOpinionID;

            bff::log(__METHOD__ . 'test 1 Rating::addStars($nOpinionID, $aData)');

            Rating::addStars($nOpinionID, $aData);
        }
        return $nOpinionID;
    }

}