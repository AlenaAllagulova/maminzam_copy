<?php

define('TABLE_OPINIONS', DB_PREFIX.'opinions');

class OpinionsModel_ extends Model
{
    /** @var OpinionsBase */
    var $controller;

    public function init()
    {
        parent::init();
    }

    # --------------------------------------------------------------------
    # Отзывы

    /**
     * Список отзывов (admin)
     * @param array $aFilter фильтр списка отзывов
     * @param bool $bCount только подсчет кол-ва отзывов
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function opinionsListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $aFilter[':ujoin'] = ' O.author_id = aU.user_id AND  O.user_id = uU.user_id ';
        $aFilter = $this->prepareFilter($aFilter, 'O');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(O.id) FROM '.TABLE_OPINIONS.' O, ' . TABLE_USERS . ' aU,
                 ' . TABLE_USERS . ' uU '.$aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('
            SELECT O.id, O.created, O.message, O.moderated, O.author_id, O.user_id, O.type, O.answer, O.order_id,
                   aU.name AS author_name, aU.email AS author_email, aU.blocked AS author_blocked, aU.login AS author_login,
                   uU.name AS user_name, uU.email AS user_email, uU.blocked AS user_blocked, uU.login AS user_login
            FROM ' . TABLE_OPINIONS . ' O,
                 ' . TABLE_USERS . ' aU,
                 ' . TABLE_USERS . ' uU
            '.$aFilter['where']
            .( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '')
            .$sqlLimit, $aFilter['bind']);
    }

    /**
     * Список отзывов (frontend)
     * @param array $aFilter фильтр списка отзывов
     * @param bool $bCount только подсчет кол-ва отзывов
     * @param integer $nUserID ID пользователя владельца
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function opinionsList(array $aFilter = array(), $bCount = false, $nUserID = 0, $sqlLimit = '', $sqlOrder = 'O.created DESC')
    {
        if (Opinions::premoderation()) {
            $aFilter[':moderated'] = array('(O.moderated > 0 OR O.author_id = :author)', ':author' => User::id());
        }
        if ( ! empty($aFilter['author_id']) && $aFilter['author_id'] == $nUserID) {
            $aFilter[':user'] = 'O.user_id = U.user_id';
        } else {
            $aFilter[':user'] = 'O.author_id = U.user_id';
        }
        $aFilter[':user'] .= ' AND U.blocked = 0 AND U.deleted = 0';

        $ordersOpinions = Orders::ordersOpinions();
        if ($bCount) {
            $aFilter = $this->prepareFilter($aFilter, 'O');
            return $this->db->one_data('
                SELECT COUNT(O.id)
                FROM '.TABLE_OPINIONS.' O, '.TABLE_USERS.' U
                '.$aFilter['where'], $aFilter['bind']);
        }

        $aFilter[':own'] = 'O.user_id = uU.user_id';

        $aFilter = $this->prepareFilter($aFilter, 'O');
        $aData = $this->db->select('
            SELECT O.id, 
                   O.user_id, 
                   O.author_id, 
                   O.type, 
                   O.created, 
                   O.modified, 
                   O.message, 
                   O.answer, 
                   O.moderated,
                   O.answer_created,
                   O.answer_modified,
                   U.user_id AS opponent_id, 
                   U.login, 
                   U.avatar, 
                   U.name, 
                   U.surname, 
                   U.sex, 
                   U.pro, 
                   U.verified,
                   uU.type AS user_type,
                   uU.login AS answerer_login,
                   uU.avatar AS answerer_avatar, 
                   uU.name AS answerer_name, 
                   uU.surname AS answerer_surname, 
                   uU.sex AS answerer_sex  
                   '.($ordersOpinions ? ', O.order_id, R.title, R.keyword, R.price, R.price_curr, R.price_ex, R.price_rate_text, R.performer_id ' : '').'
            FROM '.TABLE_OPINIONS.' O '.($ordersOpinions ? ' LEFT JOIN '.TABLE_ORDERS.' R ON O.order_id = R.id ' : '').'
                , '.TABLE_USERS.' U, '.TABLE_USERS.' uU
            '.$aFilter['where'].'
            '.( ! empty($sqlOrder) ? ' ORDER BY '.$sqlOrder : '').'
            '.$sqlLimit, $aFilter['bind']);

        if ( ! empty($aData)){
            if ($ordersOpinions) {
                foreach($aData as & $v){
                    if ($v['price_rate_text']) {
                        $v['price_rate_text'] = func::unserialize($v['price_rate_text']);
                    }
                }unset($v);
            }
        }

        return $aData;
    }

    /**
     * Подсчет счетчиков отзывов для списка
     * @param array $aFilter
     * @return mixed
     */
    public function opinionsCounts(array $aFilter = array())
    {
        if (isset($aFilter['type'])) {
            unset($aFilter['type']);
        }
        if (Opinions::premoderation()) {
            $aFilter[':moderated'] = array('(O.moderated > 0 OR (O.moderated = 0 AND O.author_id = :author))', ':author' => User::id());
        }

        $aFilter = $this->prepareFilter($aFilter, 'O');
        $aData = $this->db->select_key('
            SELECT O.type, COUNT(O.id) AS cnt
            FROM '.TABLE_OPINIONS.' O
            '.$aFilter['where'].'
            GROUP BY O.type', 'type', $aFilter['bind']);
        $aTypes = Opinions::aTypes();
        $aTypes = array_keys($aTypes);
        $nSum = 0;
        foreach ($aTypes as $v) {
            if( ! isset($aData[$v])) {
                $aData[$v] = array('id' => $v, 'cnt' => 0);
            }
            $nSum += $aData[$v]['cnt'];
        }
        $aData[0] = array('id' => 0, 'cnt' => $nSum);
        return $aData;
    }

    /**
     * Получение данных отзыва
     * @param integer $nOpinionID ID отзыва
     * @param array $aFields список необходимых полей
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function opinionData($nOpinionID, array $aFields = array(), $bEdit = false)
    {
        if ($bEdit) {
            $aData = $this->db->one_array('SELECT O.*
                    FROM '.TABLE_OPINIONS.' O
                    WHERE O.id = :id',
                array(':id'=>$nOpinionID));

        } else {
            if (empty($aFields)) {
                $aFields = array('O.*');
            }
            $aData = $this->db->one_array('SELECT ' . join(',', $aFields) . '
                    FROM '.TABLE_OPINIONS.' O
                    WHERE O.id = :id',
                array(':id'=>$nOpinionID)
            );
        }
        return $aData;
    }

    /**
     * Сохранение отзыва
     * @param integer $nOpinionID ID отзыва
     * @param array $aData данные отзыва
     * @return boolean|integer
     */
    public function opinionSave($nOpinionID, array $aData)
    {
        if (empty($aData)) return false;

        if ($nOpinionID > 0)
        {
            $res = $this->db->update(TABLE_OPINIONS, $aData, array('id'=>$nOpinionID));
            return ! empty($res);
        }
        else
        {
            if (Orders::ordersOpinions()) {
                if (empty($aData['order_id'])) {
                    return false;
                }
            }
            $nOpinionID = $this->db->insert(TABLE_OPINIONS, $aData);
            if ($nOpinionID > 0) {
                //
            }
            return $nOpinionID;
        }
    }

    /**
     * Переключатели отзыва
     * @param integer $nOpinionID ID отзыва
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function opinionToggle($nOpinionID, $sField)
    {
        switch ($sField) {
            case '?': {
                // $this->toggleInt(TABLE_OPINIONS, $nOpinionID, $sField, 'id');
            } break;
        }
    }

    /**
     * Удаление отзыва
     * @param integer $nOpinionID ID отзыва
     * @return boolean
     */
    public function opinionDelete($nOpinionID)
    {
        if (empty($nOpinionID)) return false;
        $res = $this->db->delete(TABLE_OPINIONS, array('id'=>$nOpinionID));
        if ( ! empty($res)) {
            return true;
        }
        return false;
    }

    /**
     * Получаем общее кол-во отзывов, ожидающих модерации
     * @return integer
     */
    public function opinionsModeratingCounter()
    {
        return (int)$this->db->one_data('SELECT COUNT(O.id)
                FROM ' . TABLE_OPINIONS . ' O
                WHERE O.moderated != 1'
        );
    }

    /**
     * Проверка существования отзыва
     * @param $nUserID integer ID пользователя, кому добавляют отзыв
     * @param $nAuthorID integer ID пользователя, кто добавляет отзыв
     * @return mixed
     */
    public function isOpinionAdd($nUserID, $nAuthorID)
    {
        return $this->db->one_data('
            SELECT COUNT(id) FROM '.TABLE_OPINIONS.' WHERE user_id = :user AND author_id = :author',
            array(':user' => $nUserID, ':author' => $nAuthorID));
    }

    /**
     * Подсчет количества отзывов у пользователя
     * @param integer $nUserID ID пользователя
     * @return mixed
     */
    public function userOpinionsCounters($nUserID)
    {
        return $this->db->select_key('
            SELECT O.type, COUNT(id) AS cnt
            FROM '.TABLE_OPINIONS.' O
            WHERE O.user_id = :user '.(Opinions::premoderation() ? ' AND O.moderated > 0 ': '').'
            GROUP BY O.type
        ', 'type', array(':user' => $nUserID));
    }

    /**
     * Пересчет счетчиков отзывово у пользователей
     * @param array $aFilter фильтр пользователей
     */
    public function reCalcUsersOpinionsCache($aFilter = array())
    {
        $aFilter[':j'] = ' U.user_id = O.user_id ';

        $filter = $aFilter;
        unset($filter[':j']);
        $filter[':e'] = ' O.id IS NULL ';
        $filter = $this->prepareFilter($filter);
        $this->db->exec('
            UPDATE '.TABLE_USERS.' U
                LEFT JOIN '.TABLE_OPINIONS.' O ON U.user_id = O.user_id
            SET U.opinions_cache = ""
        '.$filter['where'], $filter['bind']);

        $aFilter = $this->prepareFilter($aFilter);
        $cnt = 0;
        $update = array();
        $userID = 0;
        $user = array();
        $this->db->select_iterator('
            SELECT U.user_id, U.opinions_cache, O.type, COUNT(O.id) AS cnt
            FROM '.TABLE_USERS.' U, '.TABLE_OPINIONS.' O
            '.$aFilter['where'].'
            GROUP BY U.user_id, O.type
            ORDER BY U.user_id
        ', ! empty($aFilter['bind']) ? $aFilter['bind'] : array(),
        function($v) use (& $userID, & $user, & $update, & $cnt){
            if($userID != $v['user_id']){
                if($userID){
                    $opinionsCache = $user['opinions'][Opinions::TYPE_POSITIVE].';'.$user['opinions'][Opinions::TYPE_NEUTRAL].';'.$user['opinions'][Opinions::TYPE_NEGATIVE];
                    if ($user['opinions_cache'] != $opinionsCache) {
                        $update[] = ' WHEN user_id = '.$userID.' THEN '.$this->db->str2sql($opinionsCache);
                        $cnt++;
                        if($cnt > 1000){
                            $this->db->exec('UPDATE ' . TABLE_USERS . ' SET opinions_cache = CASE ' . join(' ', $update) . ' ELSE opinions_cache END;');
                            $update = array();
                            $cnt = 0;
                        }
                    }
                }
                $userID = $v['user_id'];
                $user = array(
                    'opinions_cache' => $v['opinions_cache'],
                    'opinions'       => array(
                        Opinions::TYPE_POSITIVE => 0,
                        Opinions::TYPE_NEUTRAL => 0,
                        Opinions::TYPE_NEGATIVE => 0,
                    )
                );
            }
            $user['opinions'][ $v['type'] ] = $v['cnt'];
        });
        if ($userID) {
            $opinionsCache = $user['opinions'][Opinions::TYPE_POSITIVE] . ';' . $user['opinions'][Opinions::TYPE_NEUTRAL] . ';' . $user['opinions'][Opinions::TYPE_NEGATIVE];
            if ($user['opinions_cache'] != $opinionsCache) {
                $update[] = ' WHEN user_id = ' . $userID . ' THEN ' . $this->db->str2sql($opinionsCache);
            }
        }
        if( ! empty($update)) {
            $this->db->exec('UPDATE ' . TABLE_USERS . ' SET opinions_cache = CASE ' . join(' ', $update) . ' ELSE opinions_cache END;');
        }
    }

    /**
     * Данные об отзывах по фильтру
     * @param array $fields
     * @param array $filter
     * @param bool $oneArray
     * @return mixed
     */
    public function opinionsDataByFilter(array $fields, array $filter, $oneArray = false)
    {
        $filter = $this->prepareFilter($filter, 'O');
        if ($oneArray) {
            return $this->db->one_array('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_OPINIONS . ' O ' . $filter['where'], $filter['bind']);
        } else {
            return $this->db->select('SELECT ' . join(',', $fields) . ' FROM ' . TABLE_OPINIONS . ' O ' . $filter['where'], $filter['bind']);
        }
    }


}