<?php

abstract class OpinionsBase_ extends Module
{
    # Тип отзыва
    const TYPE_POSITIVE = 1; # положительный
    const TYPE_NEGATIVE = 2; # нейтральный
    const TYPE_NEUTRAL  = 3; # отрицательный

    /** @var OpinionsModel */
    var $model = null;
    var $securityKey = '78121484c55f093f5dd9ab37fc9b6c31';

    /**
     * @return Opinions
     */
    public static function i()
    {
        return bff::module('opinions');
    }

    /**
     * @return OpinionsModel
     */
    public static function model()
    {
        return bff::model('opinions');
    }

    public function init()
    {
        parent::init();
        $this->module_title = _t('opinions','Отзывы');
    }

    public function sendmailTemplates()
    {
        $aTemplates = array(
            'opinions_opinion_add' => array(
                'title'       => 'Отзывы: Новый отзыв',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> которому был оставлен отзыв',
                'vars'        => array(
                    '{fio}'           => 'ФИО пользователя',
                    '{name}'          => 'Имя пользователя',
                    '{opinion_url}'   => 'Ссылка для просмотра отзыва',
                    '{opinion_type}'  => 'Тип отзыва (положительный, отрицательный, нейтральный)',
                    '{author_fio}'    => 'ФИО автора отзыва',
                    '{author_login}'  => 'Логин автора отзыва',
                    '{author_url}'    => 'Ссылка для просмотра профиля автора отзыва',
                ),
                'impl'        => true,
                'priority'    => 50,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'profile',
            ),
            'opinions_opinion_change' => array(
                'title'       => 'Отзывы: Отзыв был изменен',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> у которого был изменен отзыв',
                'vars'        => array(
                    '{fio}'           => 'ФИО пользователя',
                    '{name}'          => 'Имя пользователя',
                    '{opinion_url}'   => 'Ссылка для просмотра отзыва',
                    '{opinion_type}'  => 'Тип отзыва (положительный, отрицательный, нейтральный )',
                    '{author_fio}'    => 'ФИО автора отзыва',
                    '{author_login}'  => 'Логин автора отзыва',
                    '{author_url}'    => 'Ссылка для просмотра профиля автора отзыва',
                ),
                'impl'        => true,
                'priority'    => 51,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'profile',
            ),
            'opinions_opinion_delete' => array(
                'title'       => 'Отзывы: Отзыв был удален',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> у которого был удален отзыв',
                'vars'        => array(
                    '{fio}'           => 'ФИО пользователя',
                    '{name}'          => 'Имя пользователя',
                    '{opinion_type}'  => 'Тип отзыва (положительный, отрицательный, нейтральный )',
                    '{author_fio}'    => 'ФИО автора отзыва',
                    '{author_login}'  => 'Логин автора отзыва',
                    '{author_url}'    => 'Ссылка для просмотра профиля автора отзыва',
                ),
                'impl'        => true,
                'priority'    => 52,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'profile',
            ),
            'opinions_opinion_answer' => array(
                'title'       => 'Отзывы: Ответ на отзыв',
                'description' => 'Уведомление, отправляемое <u>пользователю</u>, автору отзыва, при добавлении ответа',
                'vars'        => array(
                    '{fio}'           => 'ФИО пользователя (автор отзыва)',
                    '{name}'          => 'Имя пользователя (автор отзыва)',
                    '{opinion_url}'   => 'Ссылка для просмотра отзыва',
                    '{user_fio}  '    => 'ФИО пользователя (автор ответа)',
                    '{user_login}'    => 'Логин пользователя (автор ответа)',
                    '{user_url}'      => 'Ссылка для просмотра профиля пользователя (автор ответа)',
                ),
                'impl'        => true,
                'priority'    => 53,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'profile',
            ),
        );

        Sendmail::i()->addTemplateGroup('profile', 'Кабинет', 11);

        return $aTemplates;
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # Кабинет: отзывы
            case 'my.opinions':
                $opts['login'] = User::data('login');
                $url = static::url('user.listing', $opts, $dynamic);
                break;
            # Профиль: отзывы
            case 'user.listing':
                $url = Users::url('profile', array('login'=>$opts['login'], 'tab'=>'opinions'), $dynamic);
                $url .= static::urlQuery($opts, array('login'));
                break;
        }
        return bff::filter('opinions.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Использовать премодерацию отзывов
     * @return bool
     */
    public static function premoderation()
    {
        return config::sysAdmin('opinions.premoderation', true, TYPE_BOOL);
    }

    /**
     * Задержка возможности публикации отзыва (в днях) после регистрации
     * @return int
     */
    public static function registerTimeout()
    {
        return config::sysAdmin('opinions.register.timeout', 0, TYPE_UINT);
    }

    /**
     * Типы отзывов
     * @return array
     */
    public static function aTypes()
    {
        return bff::filter('opinions.types',array(
            static::TYPE_POSITIVE => array(
                'id'     => static::TYPE_POSITIVE,
                't'      => _t('opinions', 'Положительный'),
                'tl'     => _t('opinions', 'положительный'),
                'cl'     => 'success',
                'cf'     => 'success',
                'plural' => _t('opinions', 'Положительные'),
                'ct'     => 'text-success',
                'ck'     => ''
            ),
            static::TYPE_NEGATIVE => array(
                'id'     => static::TYPE_NEGATIVE,
                't'      => _t('opinions', 'Отрицательный'),
                'tl'     => _t('opinions', 'отрицательный'),
                'cl'     => 'danger',
                'cf'     => 'danger',
                'plural' => _t('opinions', 'Отрицательные'),
                'ct'     => 'text-red',
                'ck'     => ''
            ),
            static::TYPE_NEUTRAL => array(
                'id'     => static::TYPE_NEUTRAL,
                't'      => _t('opinions', 'Нейтральный'),
                'tl'     => _t('opinions', 'нейтральный'),
                'cl'     => 'default',
                'cf'     => 'inert',
                'plural' => _t('opinions', 'Нейтральные'),
                'ct'     => 'text-grey',
                'ck'     => 'checked'
            ),
        ));
    }

    /**
     * Проверка возможности добавления отзыва пользователю {$userID}
     * @param integer $userID ID пользователя, кому добавляют отзыв
     * @param integer|boolean $userType тип пользователя или FALSE - получаем тип по {$userID}
     * @param string $sReasonMessage @ref описание причины запрета
     * @return bool true - возможно
     */
    public function isAllowAdd($userID = 0, $userType = false, & $sReasonMessage = '')
    {
        if (Orders::ordersOpinions()) return false;

        $sReasonMessage = '';
        do{
            $authorID = User::id();
            $authorType = User::type();
            if ( ! $userID || ! $authorID || User::isCurrent($userID)) {
                break;
            }
            if ($userType === false) {
                $userData = Users::model()->userDataByFilter(
                    array('user_id' => $userID),
                    array('type')
                );
                if (empty($userData)) break;
                $userType = $userData['type'];
            }

            if (Users::useClient()) {
                if ($userType == $authorType) break;
            }

            # Проверка кол-ва дней после регистрации
            $nTimeoutDays = static::registerTimeout();
            if ($nTimeoutDays) {
                $mCreated = Users::model()->userData($authorID, array('created'));
                try {
                    $mCreated = new DateTime($mCreated['created']);
                    $dNow = new DateTime();
                    $nDays = $dNow->diff($mCreated)->format('%a');
                    if ($nDays < $nTimeoutDays) {
                        $nDelta = $nTimeoutDays - $nDays;
                        if ($nDelta < 1) $nDelta = 1;
                        if ($nDelta < 30) {
                            $sReasonMessage = _t('opinions', 'Вы сможете оставить отзыв через [days].', array('days' => tpl::declension($nDelta, _t('', 'день;дня;дней'))));
                        } else {
                            $nDelta = round($nDelta / 30);
                            if ($nDelta < 0) $nDelta = 1;
                            $sReasonMessage = _t('opinions', 'Вы сможете оставить отзыв через [months].', array('months' => tpl::declension($nDelta, _t('', 'месяц;месяца;месяцев'))));
                        }
                        break;
                    }
                } catch(Exception $e) {
                    break;
                }
            }

            return ! $this->model->isOpinionAdd($userID, $authorID);
        } while(false);

        return false;
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nOpinionID ID отзыва или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    public function validateOpinionData($nOpinionID, $bSubmit)
    {
        $aData = array();
        $aParams = array(
            'message' => array(TYPE_NOTAGS, 'len' => 3000, 'len.sys' => 'opinions.message.limit'), # Текст отзыва
            'type'    => TYPE_UINT,   # Тип отзыва
        );

        if( ! $nOpinionID){
            if (Orders::ordersOpinions()) {
                $aParams['order_id'] = TYPE_UINT; # ID заказа на основании которого добавляется отзыв
            } else {
                $aParams['user_id'] = TYPE_UINT; # ID пользователя которому оставляют отзыв
            }
        }

        $this->input->postm($aParams, $aData);

        if ($bSubmit)
        {
            if(empty($aData['message'])){
                $this->errors->set(_t('opinions', 'Не указан текст отзыва'));
            }

            $aTypes = static::aTypes();
            $aTypes = array_keys($aTypes);
            if( ! in_array($aData['type'], $aTypes)){
                $this->errors->set(_t('opinions', 'Не указан тип отзыва'));
            }

        }
        return $aData;
    }

    /**
     * Обрабатываем параметры ответа на отзыв
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validateAnswerData($bSubmit)
    {
        $aData = array();
        $aParams = array(
            'answer' => array(TYPE_NOTAGS, 'len' => 3000, 'len.sys' => 'opinions.answer.limit'), # Текст отзыва
        );

        $this->input->postm($aParams, $aData);

        if ($bSubmit)
        {
            if (empty($aData['answer'])){
                $this->errors->set(_t('opinions', 'Не указан текст ответа'));
            }

        }
        return $aData;
    }

    /**
     * Отправка почтового уведомления об отзыве подучателю отзыва (добавление / изменение / удаление)
     * @param array $opinionData данные об отзыве: id, user_id, author_id, type
     * @param string $action тип действия
     */
    protected function opinionEnotify($opinionData, $action)
    {
        if (empty($opinionData)) return;

        $userData = Users::model()->userData($opinionData['user_id'], array('user_id','login'));
        if (!empty($userData))
        {
            $opinionTypes = static::aTypes();

            if (User::isCurrent($opinionData['author_id'])) {
                $authorData = User::data(array('login','name','surname'));
            } else {
                $authorData = Users::model()->userData($opinionData['author_id'], array('name','surname','login'));
            }
            $authorFio = array();
            if (!empty($authorData['name'])) $authorFio[] = $authorData['name'];
            if (!empty($authorData['surname'])) $authorFio[] = $authorData['surname'];
            $authorFio = ( ! empty($authorFio) ? join(' ', $authorFio) : $authorData['login']);

            switch($action)
            {
                case 'add': # добавление
                    Users::sendMailTemplateToUser($userData['user_id'], 'opinions_opinion_add', array(
                        'opinion_url'   => static::url('user.listing', array('login' => $userData['login'])).'#'.$authorData['login'],
                        'opinion_type'  => $opinionTypes[ $opinionData['type'] ]['t'],
                        'author_fio'    => $authorFio,
                        'author_login'  => $authorData['login'],
                        'author_url'    => Users::url('profile', array('login' => $authorData['login'])),
                    ), Users::ENOTIFY_GENERAL);
                    break;
                case 'change': # изменение
                    Users::sendMailTemplateToUser($userData['user_id'], 'opinions_opinion_change', array(
                        'opinion_url'   => static::url('user.listing', array('login' => $userData['login'])).'#'.$authorData['login'],
                        'opinion_type'  => $opinionTypes[ $opinionData['type'] ]['t'],
                        'author_fio'    => $authorFio,
                        'author_login'  => $authorData['login'],
                        'author_url'    => Users::url('profile', array('login' => $authorData['login'])),
                    ), Users::ENOTIFY_GENERAL);
                    break;
                case 'delete': # удаление
                    Users::sendMailTemplateToUser($userData['user_id'], 'opinions_opinion_delete', array(
                        'opinion_type'  => $opinionTypes[ $opinionData['type'] ]['t'],
                        'author_fio'    => $authorFio,
                        'author_login'  => $authorData['login'],
                        'author_url'    => Users::url('profile', array('login' => $authorData['login'])),
                    ), Users::ENOTIFY_GENERAL);
                    break;
            }
        }
    }

    /**
     * Удаление отзыва
     * @param integer $nOpinionID ID отзыва
     * @param string $sOpinionsCache @ref
     * @return bool true - успешно удален
     */
    public function opinionDelete($nOpinionID, & $sOpinionsCache = '')
    {
        $aData = $this->model->opinionData($nOpinionID, array('id','user_id','author_id','type','moderated'));
        if (empty($aData)) return false;

        $res = $this->model->opinionDelete($nOpinionID);
        if ($res && ( ! static::premoderation() || $aData['moderated'])) {
            # Обновляем счетчики отзывов + рейтинг
            $sOpinionsCache = $this->onUserOpinionsChange($aData['user_id'], 'delete', $aData['type'], 0, $nOpinionID);

            # Отправим уведомление
            $this->opinionEnotify($aData, 'delete');
        }
        # обновляем счетчик отзывов "на модерации"
        $this->moderationCounterUpdate();

        return $res;
    }

    /**
     * Актуализация счетчика отзывоао ожидающих модерации
     * @param integer|null $increment
     */
    public function moderationCounterUpdate($increment = null)
    {
        if (empty($increment)) {
            $count = $this->model->opinionsModeratingCounter();
            config::save('opinions_items_moderating', $count, true);
        } else {
            config::saveCount('opinions_items_moderating', $increment, true);
        }
    }

    /**
     * Изменение отзывов о пользователе
     * @param integer $nUserID ID пользователя
     * @param string $sEvent событие add|edit|delete
     * @param integer $nOpinionType тип отзыва
     * @param integer $nOpinionTypePrev тип предыдущего отзыва (если есть)
     * @param integer $nOpinionID ID отзыва
     * @return string "opinions_cache"
     */
    protected function onUserOpinionsChange($nUserID, $sEvent, $nOpinionType, $nOpinionTypePrev = 0, $nOpinionID = 0)
    {
        $nRatingChanges = bff::getRatingAmount('opinion', $nOpinionType);

        switch($sEvent)
        {
            case 'add':
            {
                # прибавляем к рейтингу
            } break;
            case 'edit':
            {
                # пересчитываем
                $nPrev = bff::getRatingAmount('opinion', $nOpinionTypePrev);
                if($nPrev === $nRatingChanges) {
                    # без изменений
                    $nRatingChanges = 0;
                } else {
                    # 1. возвращаем рейтингу состояние до добавления отзыва
                    $nChange = 0;
                    if($nPrev < 0) {
                        $nChange += abs($nPrev);
                    } else {
                        $nChange -= $nPrev;
                    }
                    # 2. прибавляем к рейтингу по новой
                    $nRatingChanges += $nChange;
                }
            } break;
            case 'delete':
            {
                # вычитаем из рейтинга
                $nRatingChanges = -$nRatingChanges;
            } break;
        }

        # обновляем счетчики кол-ва отзывов и рейтинг
        $aData = $this->model->userOpinionsCounters($nUserID);
        $aUpdate = array(
            'opinions_cache' => ( ! empty($aData[ static::TYPE_POSITIVE ]) ? $aData[ static::TYPE_POSITIVE ]['cnt'] : 0 ).';'.
                                ( ! empty($aData[ static::TYPE_NEUTRAL ]) ?  $aData[ static::TYPE_NEUTRAL]['cnt'] : 0 ).';'.
                                ( ! empty($aData[ static::TYPE_NEGATIVE ]) ? $aData[ static::TYPE_NEGATIVE ]['cnt'] : 0 ),
        );
        Users::model()->userSave($nUserID, $aUpdate);
        Users::model()->ratingChange($nUserID, $nRatingChanges, 'opinions-'.$sEvent, array('opinion_id'=>$nOpinionID));

        return $aUpdate['opinions_cache'];
    }

}