<?php

class Bills_ extends BillsBase
{
    public function my_bill()
    {
        if ($this->input->getpost('pay', TYPE_UINT) > 0) {
            return $this->my_pay();
        }

        return $this->my_history();
    }

    protected function my_history()
    {
        if (!User::id()) {
            return $this->showForbiddenGuest();
        }

        $this->security->setTokenPrefix('bills-my-history');

        $aData = array(
            'list'   => array(),
            'pgn'    => '',
        );
        $f = $this->input->postgetm(array(
                'page' => TYPE_UINT,    # страница
                'fr'   => TYPE_NOTAGS,  # c
                'to'   => TYPE_NOTAGS,  # по
            )
        );
        $nPageSize = config::sysAdmin('bills.history.pagesize', 10, TYPE_UINT);

        $aFilter = array('user_id' => User::id(), 'status' => self::STATUS_COMPLETED);

        if (!empty($f['fr'])) {
            $from = strtotime($f['fr']);
            if (!empty($from)) {
                $aFilter[':from'] = array('created >= :from', ':from' => date('Y-m-d 00:00:00', $from));
            }
        }
        if (!empty($f['to'])) {
            $to = strtotime($f['to']);
            if (!empty($to)) {
                $aFilter[':to'] = array('created <= :to', ':to' => date('Y-m-d 23:59:59', $to));
            }
        }

        $nTotal = $this->model->billsList($aFilter, true);

        if ($nTotal > 0) {

            $pgn = new Pagination($nTotal, $nPageSize, array(
                'link'  => static::url('my.history'),
                'query' => $f,
            ));

            $aData['list'] = $this->model->billsList($aFilter, false, $pgn->getLimitOffset());
            $aData['pgn'] = $pgn->view();
        }

        foreach ($aData['list'] as $k => $v) {
            $site = SITEHOST;
            $aData['list'][$k]['description'] = str_replace('{sitehost}', $site, $v['description']);
        }
        $aData['curr'] = Site::currencyDefault();
        $aData['list'] = $this->viewPHP($aData, 'my.history.list');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'pgn'   => $aData['pgn'],
                    'list'  => $aData['list'],
                )
            );
        }

        $aData['f'] = & $f;
        $aData['list_empty'] = ($nTotal <= 1);
        $aData['balance'] = $this->security->getUserBalance();

        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => _t('bills', 'История операций по счету'), 'active' => true),
        );

        return $this->viewPHP($aData, 'my.history');
    }

    protected function my_pay()
    {
        if (!User::id()) {
            if (Request::isAJAX()) {
                $this->errors->reloadPage();
                $this->ajaxResponseForm();
            }
            return $this->showForbiddenGuest();
        }

        $this->security->setTokenPrefix('bills-my-pay');

        # данные о доступных способах оплаты
        $paySystems = static::getPaySystems();
        $ps = $this->input->getpost('ps', TYPE_STR);
        if (!$ps || !array_key_exists($ps, $paySystems)) {
            $ps = key($paySystems);
        }
        foreach ($paySystems as $k => &$v) {
            $v['active'] = ($k == $ps);
        }
        unset($v);

        # сумма к оплате в валюте по-умолчанию
        $amount = $this->input->getpost('amount', TYPE_UNUM);

        if (Request::isAJAX()) {
            $amount = round($amount, 0);
            $response = array('amount' => $amount);
            $currencyDefaultID = Site::currencyDefault('id');

            do {

                if (!$this->security->validateToken()) {
                    $this->errors->reloadPage();
                    break;
                }

                if (!$amount) {
                    $this->errors->set(_t('svc', 'Сумма пополнения указана некорректно'), 'amount');
                    break;
                }

                $ps_id = $paySystems[$ps]['id'];
                $ps_way = $paySystems[$ps]['way'];
                $money = Site::currencyPriceConvert($amount, $currencyDefaultID, $paySystems[$ps]['currency_id']);
                bff::hook('bills.pay.submit', $amount, $ps, array('amount'=>$money));

                # создаем счет: "Пополнение счета"
                $billID = $this->createBill_InPay(User::id(), $this->security->getUserBalance(),
                    $amount, # сумма в валюте сайте
                    $money, # сумма к оплате
                    $currencyDefaultID,
                    self::STATUS_WAITING,
                    $ps_id, $ps_way,
                    _t('svc', 'Пополнение счета через [system]', array(
                        'system' => (isset($this->psystemsData[$ps_id]['title']) ? $this->psystemsData[$ps_id]['title'] : $paySystems[$ps]['title'])
                    ))
                );
                if (!$billID) {
                    $this->errors->set(_t('svc', 'Ошибка пополнения счета, обратитесь к администрации'));
                    break;
                }

                # формируем форму запроса для системы оплаты
                $response['form'] = $this->buildPayRequestForm($ps_id, $ps_way, $billID, $money);

            } while (false);

            $this->ajaxResponseForm($response);
        }

        $aData['payMethodsList'] = $this->payMethodsList($paySystems);
        $aData['amount'] = $amount;
        $aData['balance'] = $this->security->getUserBalance();

        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => _t('bills', 'Пополнить счет'), 'active' => true),
        );
        return $this->viewPHP($aData, 'my.pay');
    }

    public function payMethodsList(& $paySystems = array(), $bBalanceUse = true)
    {
        if(empty($paySystems)){
            $paySystems = static::getPaySystems($bBalanceUse);
        }

        $aData['psystems'] = & $paySystems;
        return $this->viewPHP($aData, 'pay.methods.list');
    }

    public function createBill($ps, $sum, $aSvcSettings = array(), $nSvcKeys = 0, $nItemID = 0)
    {
        $paySystems = static::getPaySystems();
        do {
            if (!array_key_exists($ps, $paySystems)){
                break;
            }

            $ps_id = $paySystems[$ps]['id'];
            $ps_way = $paySystems[$ps]['way'];
            $currencyDefaultID = Site::currencyDefault('id');
            $money = Site::currencyPriceConvert($sum, $currencyDefaultID, $paySystems[$ps]['currency_id']);
            $billID = $this->createBill_InPay(User::id(), $this->security->getUserBalance(),
                $sum, # сумма в валюте сайте
                $money, # сумма к оплате
                $currencyDefaultID,
                self::STATUS_WAITING,
                $ps_id, $ps_way,
                _t('svc', 'Пополнение счета через [system]', array(
                    'system' => (isset($this->psystemsData[$ps_id]['title']) ? $this->psystemsData[$ps_id]['title'] : $paySystems[$ps]['title'])
                )),
                $nSvcKeys, false, $nItemID,
                $aSvcSettings
            );
            if (!$billID) {
                $this->errors->set(_t('svc', 'Ошибка создания счета, обратитесь к администрации'));
                break;
            }
            # формируем форму запроса для системы оплаты
            return $this->buildPayRequestForm($ps_id, $ps_way, $billID, $money);

        } while(false);

        $this->errors->set(_t('bills', 'Платежная система указанна некорректно'));
        return false;
    }

    # ---------------------------------------------------------------------------------

    /**
     * Обработка запроса от системы оплаты
     * @param string ::get 'psystem' система оплаты
     * @return string
     */
    public function processPayRequest()
    {
        $sPaySystem = $this->input->get('psystem', TYPE_NOTAGS);
        $sPayRequestMethod = $sPaySystem . '_request';
        bff::hook('bills.pay.process', $sPaySystem);

        $aData = $this->getPaySystemData($sPaySystem);
        if (!method_exists($this, $sPayRequestMethod)) {
            $this->log('Данный способ оплаты отключен: ' . $aData['title']);

            return $this->payError('off');
        } else {
            $this->$sPayRequestMethod();
        }
    }

    /**
     * Оплата счёта на основе данных от платёжной системы
     * @param int $nBillID ID счета (в таблице TABLE_BILLS)
     * @param float|int $fMoney сумма счета (оплачиваемая)
     * @param int $nPaySystem ID системы оплаты
     * @param mixed $mDetails детали от платежной системы
     * @param array $mExtra доп.параметры (если необходимо)
     * @return mixed
     */
    public function processBill($nBillID, $fMoney = 0, $nPaySystem = 0, $mDetails = false, $aExtra = array())
    {
        $sPaySystem = $this->getPaySystemTitle($nPaySystem);

        # Проверяем ID счета
        if (!is_numeric($nBillID) || $nBillID <= 0) {
            $this->log($sPaySystem . ': некорректный номер счета, #' . $nBillID);

            return $this->payError('wrong_bill_id');
        }

        $aBill = $this->model->billData($nBillID, array(
                'user_id',
                'psystem',
                'status',
                'amount',
                'money',
                'svc_id',
                'svc_activate',
                'item_id',
                'svc_settings'
            )
        );
        if (empty($aBill)) {
            $this->log($sPaySystem . ': Оплачен несуществующий счёт #' . $nBillID);

            return $this->payError('pay_error');
        }

        # Проверяем доступность способа оплаты
        if ($nPaySystem !== intval($aBill['psystem'])) {
            $this->log($sPaySystem . ': Cчёт #' . $nBillID . ' выставлен для оплаты другой системой оплаты (' . $this->getPaySystemTitle($aBill['psystem']) . ')');

            return $this->payError('pay_error');
        }

        # Проверяем статус счета
        if ($aBill['status'] == self::STATUS_CANCELED ||
            $aBill['status'] == self::STATUS_COMPLETED
        ) {
            $this->log($sPaySystem . ': Оплачен уже ранее оплаченный счёт или счёт с другим статусом, #' . $nBillID);

            return $this->payError('pay_error');
        }

        # Проверка суммы
        if ($fMoney < $aBill['money']) {
            $this->log("$sPaySystem: Сумма оплаты($fMoney) счета #$nBillID меньше выставленной ранее({$aBill['money']})");

            return $this->payError('amount_error');
        }

        # Закрываем счет:
        # - обновляем статус на "завершен"
        # - помечаем дату оплаты текущей
        if (!$this->completeBill($nBillID, true, array(
                'user_id' => $aBill['user_id'],
                'amount'  => $aBill['amount'],
                'add'     => true
            ), $mDetails
        )
        ) {
            return _t('bills', 'Ошибка закрытия счета #[id]', array('id' => $nBillID));
        } else {
            # пополняем счет пользователя на сумму
            $this->updateUserBalance($aBill['user_id'], $aBill['amount'], true);

            # отправим почтовое уведомление пользователю
            $user = Users::model()->userData($aBill['user_id'], array('balance'));
            Users::sendMailTemplateToUser($aBill['user_id'], 'users_balance_plus', array(
                'amount'  => $aBill['money'].' '.Site::currencyDefault(),
                'balance' => $user['balance'].' '.Site::currencyDefault(),
            ), Users::ENOTIFY_GENERAL);
        }

        do {

            # Активируем услугу, если:
            # - оплачивалась услуга (svc_id > 0)
            # - если отмечена необходимость ее активации после оплаты (svc_activate = 1)
            $nSvcID = $aBill['svc_id'];
            if ($nSvcID > 0 && !empty($aBill['svc_activate'])) {
                $oSvc = $this->svc();
                $aSvcData = $oSvc->model->svcData($nSvcID);
                if (!empty($aSvcData)) {
                    # Активируем услугу
                    # Снимаем деньги со счета пользователя
                    $mResult = $oSvc->activate($aSvcData['module'], $nSvcID, $aSvcData, $aBill['item_id'], $aBill['user_id'], $aBill['amount']);
                    if ($mResult === false) {
                        $this->log('Ошибка активации услуги: #' . $nSvcID . ', счет: ' . $nBillID);
                        break;
                    }
                } else {
                    $this->log('Ошибка активации услуги: #' . $nSvcID . ', счет: ' . $nBillID);
                    break;
                }
            }

            if( ! empty($aBill['svc_settings'])){
                $this->processSvcSettings($nSvcID, $aBill['item_id'], $aBill['user_id'], $aBill['amount'], $aBill['svc_settings']);
            }

        } while (false);

        return true;
    }

    # ---------------------------------------------------------------------------------

    public function success()
    {
        bff::hook('bills.pay.success');
        $sTitle = _t('bills', 'Пополнение счета');
        $sMessage = _t('bills', '');

        if(config::sys('bill.success.redirect', true)){
            $this->redirect(Svc::url('list', ['bill_success' => true]));
        }

        if (User::id()) {
            $sMessage .= _t('bills', 'Вы пополнили счет');
            $aFilter = array(
                'user_id' => User::id(),
                'status' => self::STATUS_COMPLETED,
                'type' => self::TYPE_IN_PAY,
                ':psystem' => 'psystem > 0'
            );
            $aListPayment = $this->model->billsList($aFilter, false, ' LIMIT 1');
            if (is_array($aListPayment) && !empty($aListPayment)){
                $lastPayment = reset($aListPayment);
                $lastPaymentCurrData = Site::currencyData($lastPayment['currency_id'], ['title_short']);
                $sMessage .= _t('bills', ' на <a href="[link]">[last_amount] [last_amount_curr]</a>',
                    array(
                        'link'    => static::url('my.history'),
                        'last_amount' => $lastPayment['amount'],
                        'last_amount_curr' => $lastPaymentCurrData['title_short'],
                        'balance' => $this->security->getUserBalance(),
                        'curr'    => Site::currencyDefault(),
                    )
                );

                if(Users::useClient() && User::isClient()){
                    $sMessage .= _t('bills', '<br> Теперь вы можете рекламировать свои объявления и покупать контакты нянь. ');
                }else{
                    $sMessage .= _t('bills', '<br> Теперь вы можете рекламировать свой профиль и покупать контакты родителей. ');
                }
            }
        }

        return $this->showSuccess($sTitle, $sMessage);
    }

    public function fail()
    {
        bff::hook('bills.pay.fail');
        $sPaySystemKey = $this->input->get('w', TYPE_NOTAGS);
        $aPaySystemTitle = $this->getPaySystemTitle($sPaySystemKey);

        $sTitle = _t('bills', 'Оплата счета');
        $sMessage = _t('bills', 'Ошибка оплаты счета');
        if (!empty($sPaySystemKey)) {
            $sMessage .= _t('bills', ' системой "[way]"', array(
                    'way' => $aPaySystemTitle
                )
            );
        }

        return $this->showForbidden($sTitle, $sMessage);
    }

    # Вспомогательный результирующий метод для системы оплаты LiqPay
    public function result()
    {
        bff::hook('bills.pay.result');
        $id = $this->input->get('id', TYPE_UINT);
        do {
            if (!$id) {
                break;
            }

            $billData = $this->model->billData($id, array('status','type','payed'));
            if (empty($billData)) {
                break;
            }

            if ($billData['status'] != static::STATUS_COMPLETED ||
                $billData['type'] != static::TYPE_IN_PAY ||
                $billData['payed'] == '0000-00-00 00:00:00' ||
                (time() - strtotime($billData['payed']) > 3600)) {
                break;
            }

            return $this->success();
        } while(false);

        return $this->fail();
    }
}