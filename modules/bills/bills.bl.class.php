<?php

abstract class BillsBase_ extends BillsModule
{
    /** @var BillsModel */
    public $model = null;

    public function init()
    {
        parent::init();

        # включаем доступные системы оплаты:
        $this->psystemsAllowed = array(
            self::PS_ROBOX,
            self::PS_WM,
            self::PS_W1,
        );

        /**
         * Настройки доступных систем оплаты указываются в файле [/config/sys.php]
         * Полный список доступных настроек указан в BillsModuleBase::init методе [/bff/modules/bills/base.php]
         * Формат: 'bills.[ключ системы оплаты].[ключ настройки]'
         * Пример: 'bills.robox.test' - тестовый режим системы оплаты Robokassa
         *
         * URL для систем оплат:
         * Result:  http://example.com/bill/process/(robox|wm)
         * Success: http://example.com/bill/success
         * Fail:    http://example.com/bill/fail
         */
    }

    public function sendmailTemplates()
    {
        $aTemplates = array(
            'users_balance_plus' => array(
                'title'       => 'Пополнение счета',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> в случае успешного пополнения счета',
                'vars'        => array(
                    '{fio}'     => 'ФИО пользователя',
                    '{name}'    => 'Имя пользователя',
                    '{email}'   => 'Email',
                    '{amount}'  => 'Сумма пополнения<div class="desc">100 '.Site::currencyDefault().'</div>',
                    '{balance}' => 'Текущий баланс<div class="desc">100 '.Site::currencyDefault().'</div>',
                ),
                'impl'        => true,
                'priority'    => 19,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'svc',
            ),
            'svc_activated_single' => array(
                'title'       => 'Услуги: Уведомление об активации услуги для заказа',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при активации услуги',
                'vars'        => array(
                    '{fio}'         => 'ФИО пользователя',
                    '{name}'        => 'Имя пользователя',
                    '{title}'       => 'Название услуги',
                    '{description}' => 'Описание услуги',
                    '{item_id}'     => 'ID объекта',
                    '{item_title}'  => 'Заголовок объекта',
                    '{item_link}'   => 'Ссылка на объект',
                ),
                'impl'        => true,
                'priority'    => 85,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'svc',
            ),
            'svc_activated_multiple' => array(
                'title'       => 'Услуги: Уведомление об активации нескольких услуг для заказа',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при активации нескольких услуг',
                'vars'        => array(
                    '{fio}'         => 'ФИО пользователя',
                    '{name}'        => 'Имя пользователя',
                    '{description}' => 'Описание услуг',
                    '{item_id}'     => 'ID объекта',
                    '{item_title}'  => 'Заголовок объекта',
                    '{item_link}'   => 'Ссылка на объект',
                ),
                'impl'        => true,
                'priority'    => 86,
                'enotify'     => Users::ENOTIFY_GENERAL,
                'group'       => 'svc',
            ),
        );

        if (bff::moduleExists('shop')) {
            $aTemplates['svc_activated_single']['title'] .= ' / товара';
            $aTemplates['svc_activated_multiple']['title'] .= ' / товара';
        }

        Sendmail::i()->addTemplateGroup('svc', 'Услуги и счет', 15);

        return $aTemplates;
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts доп. параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # История операций (список завершенных счетов)
            case 'my.history':
                $url .= '/user/bill/' . static::urlQuery($opts);
                break;
            # Пополнение счета
            case 'my.pay':
                $opts['pay'] = 1;
                $url = static::url('my.history', $opts, $dynamic);
                break;
            # Process
            case 'process':
                $url .= '/bill/process' . (isset($opts['ps']) ? '/'.$opts['ps'] : '') . static::urlQuery($opts, array('ps'));
                break;
            # Success | Fail | Result
            case 'success':
            case 'fail':
            case 'result':
                $url .= '/bill/'.$key . static::urlQuery($opts);
                break;
        }
        return bff::filter('bills.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    public static function getPaySystems($bBalanceUse = false)
    {
        $logoUrl = SITEURL_STATIC . '/img/ps/';
        $aData = array(
            'robox'    => array(
                'id'           => self::PS_ROBOX,
                'way'          => '',
                'logo_desktop' => $logoUrl . 'robox.png',
                'logo_phone'   => $logoUrl . 'robox.png',
                'title'        => _t('bills', 'Robokassa'),
                'currency_id'  => 2, # рубли
                'enabled'      => true, # true - включен, false - выключен
            ),
            'wm'       => array(
                'id'           => self::PS_WM,
                'way'          => 'wmz',
                'logo_desktop' => $logoUrl . 'wm.png',
                'logo_phone'   => $logoUrl . 'wm.png',
                'title'        => _t('bills', 'Webmoney'),
                'currency_id'  => 3, # доллары
                'enabled'      => false, # true - включен, false - выключен
            ),
            'terminal' => array(
                'id'           => self::PS_W1,
                'way'          => 'terminal',
                'logo_desktop' => $logoUrl . 'w1.png',
                'logo_phone'   => $logoUrl . 'w1.png',
                'title'        => _t('bills', 'Терминал'),
                'currency_id'  => 1, # гривны
                'enabled'      => false, # true - включен, false - выключен
            ),
            'paypal' => array(
                'id'           => self::PS_PAYPAL,
                'way'          => '',
                'logo_desktop' => $logoUrl . 'paypal.png',
                'logo_phone'   => $logoUrl . 'paypal.png',
                'title'        => _t('bills', 'Paypal'),
                'currency_id'  => 3, # доллары
                'enabled'      => false, # true - включен, false - выключен
            ),
            'liqpay' => array(
                'id'           => self::PS_LIQPAY,
                'way'          => '',
                'logo_desktop' => $logoUrl . 'liqpay.png',
                'logo_phone'   => $logoUrl . 'liqpay.png',
                'title'        => _t('bills', 'Liqpay'),
                'currency_id'  => 1, # гривны
                'enabled'      => false, # true - включен, false - выключен
            ),
            'yandex' => array(
                'id'           => self::PS_YANDEX_MONEY,
                'way'          => 'PC',
                'logo_desktop' => $logoUrl . 'ym_logo.gif',
                'logo_phone'   => $logoUrl . 'ym_logo.gif',
                'title'        => _t('bills', 'С кошелька'),
                'currency_id'  => 2, # рубли
                'enabled'      => false, # true - включен, false - выключен
            ),
            'yandexAC' => array(
                'id'           => self::PS_YANDEX_MONEY,
                'way'          => 'AC',
                'logo_desktop' => $logoUrl . 'ym_logo.gif',
                'logo_phone'   => $logoUrl . 'ym_logo.gif',
                'title'        => _t('bills', 'Банковская карта'),
                'currency_id'  => 2, # рубли
                'enabled'      => false, # true - включен, false - выключен
            ),
            'yandexMC' => array(
                'id'           => self::PS_YANDEX_MONEY,
                'way'          => 'MC',
                'logo_desktop' => $logoUrl . 'ym_logo.gif',
                'logo_phone'   => $logoUrl . 'ym_logo.gif',
                'title'        => _t('bills', 'С мобильного'),
                'currency_id'  => 2, # рубли
                'enabled'      => false, # true - включен, false - выключен
            ),
        );


        if ($bBalanceUse) {
            $aData = array(
                    'balance' => array(
                        'id'           => self::PS_UNKNOWN,
                        'way'          => '',
                        'logo_desktop' => Site::logoURL('bills.paysystems.list.balance.desktop'),
                        'logo_phone'   => Site::logoURL('bills.paysystems.list.balance.phone'),
                        'title'        => _t('bills', 'Счет [name]', array('name' => Site::title('bills.paysystems.balance'))),
                    )
                ) + $aData;
        }

        $aData = bff::filter('bills.pay.systems.user', $aData, array('balanceUse'=>$bBalanceUse, 'logoUrl'=>$logoUrl));

        # Исключаем выключенные из списка
        foreach ($aData as $k=>$v) {
            if (isset($v['enabled']) && empty($v['enabled'])) {
                unset($aData[$k]);
            }
        }
        # сортируем по приоритетности
        func::sortByPriority($aData, 'priority');

        return $aData;
    }

    public function processSvcSettings($nSvcID, $nItemID, $nUserID, $nSum, $aSettings)
    {
        if (empty($aSettings['module'])) return;

        $sModule = $aSettings['module'];
        if (bff::moduleExists($sModule)) {
            Svc::i()->activate($aSettings['module'], $nSvcID, array(), $nItemID, $nUserID, $nSum, false, $aSettings);
        }
    }
}