<?php
$langDescription = array(
    Bills::TYPE_IN_PAY => _t('bills', 'Пополнение счета'),
    Bills::TYPE_IN_GIFT => _t('bills', 'Пополнение счета'),
    Bills::TYPE_OUT_SERVICE => _t('bills', 'Оплата усуги'),
);
$curr = Site::currencyDefault();
if( ! empty($list)): ?>
    <table class="table table-hover p-bill-table">
        <thead>
        <tr>
            <th>Описание</th>
            <th style="min-width: 110px;" class="hidden-xs">№ операции</th>
            <th style="min-width: 80px;">Сумма</th>
        </tr>
        </thead>
        <tbody>
        <?  $dateLast = 0;
            foreach($list as $v):
                $in = ($v['type'] != Bills::TYPE_OUT_SERVICE);
                if( $dateLast !== $v['created_date']): ?>
                    <tr class="p-bill-date">
                        <td colspan="3"><?= tpl::date_format2($v['created_date']) ?></td>
                    </tr>
                <? endif;
                $dateLast = $v['created_date']; ?>
                <tr>
                    <td>
                        <?= $v['description'] ?>
                    </td>
                    <td class="hidden-xs text-center"><?= $v['id'] ?></td>
                    <td class="text-right"><?= ( ! $in ? '- ' : '').$v['amount'] ?> <?= $curr ?></td>
                </tr>
        <? endforeach; ?>
        </tbody>
    </table>
<? else: ?>
    <div class="alert alert-info"><?= _t('bills', 'Список операций по счету пустой'); ?></div>
<? endif; ?>

