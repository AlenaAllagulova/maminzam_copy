<?php

class M_Rating
{
    static function declareAdminMenu(CMenu $menu, Security $security)
    {
        if ( ! Rating::isActive()) {
            return null;
        }

        $sTitle = 'M.O.C. - Рейтинг';
        $menu->assign($sTitle, 'Список значений', Rating::$moduleName, 'listing', true, 1, FORDEV ? ['rlink' => ['event' => 'listing&act=add']] : []);
        $menu->assign($sTitle, 'Средние значения', Rating::$moduleName, 'average', true, 10);
        $menu->assign($sTitle, 'Настройки', Rating::$moduleName, 'settings', true, 20);
    }
}