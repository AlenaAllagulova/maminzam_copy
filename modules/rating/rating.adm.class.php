<?php

use \mastiffApp\db\models\RatingValues;
use \mastiffApp\db\models\RatingValuesLang;
use \mastiffApp\db\models\RatingUsers;

class Rating extends RatingBase
{
    public function listing()
    {
        $aData = [
            'user_types' => $this->getUsersType()
        ];
        $sAct = $this->input->postget('act',TYPE_STR);
        if ( ! empty($sAct) || Request::isPOST()) {
            $aResponse = [];
            switch ($sAct) {
                case 'add':
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = array_merge($aData, [
                        'id' => static::ID_NEW,
                        'title' => $this->model->getLanguages(),
                        'user_type' => null,
                        'stars_count' => static::DEFAULT_MIN_STARS_COUNT,
                        'stars_step' => static::DEFAULT_MIN_STARS_STEP,
                        'enable' => true,
                    ]);
                    if ($bSubmit) {
                        $aParams = [
                            'id' => TYPE_UINT,
                            'title' => TYPE_ARRAY,
                            'user_type' => TYPE_INT,
                            'stars_count' => TYPE_INT,
                            'stars_step' => TYPE_INT,
                            'enable' => TYPE_BOOL
                        ];
                        $aData = $this->input->postm($aParams);
                        if ( ! $this->validateRatingValuesData($aData)) {
                            $this->errors->set(_t('', 'Ошибка данных'));
                            break;
                        }
                        $this->model->saveRatingValues($aData);
                    }
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.value.form');
                    break;
                case 'edit':
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nId = $this->input->post('id', TYPE_INT);
                    if (empty($nId)) {
                        $this->errors->set(_t('', 'Запись не существует'));
                        break;
                    }
                    $oRatingValuesData = RatingValues::query()->find($nId);
                    $aData = array_merge($aData, [
                        'id' => $nId,
                        'user_type' => $oRatingValuesData->user_type,
                        'stars_count' => $oRatingValuesData->stars_count,
                        'stars_step' => $oRatingValuesData->stars_step,
                        'enable' => $oRatingValuesData->enable,
                    ]);
                    $aDataLangs = $this->model->getTranslations($oRatingValuesData);
                    if (is_array($aDataLangs)) {
                        $aData = array_merge($aData, $aDataLangs);
                    }
                    if ($bSubmit) {
                        $aParams = [
                            'id' => TYPE_UINT,
                            'title' => TYPE_ARRAY,
                            'user_type' => TYPE_INT,
                            'stars_count' => TYPE_INT,
                            'stars_step' => TYPE_INT,
                            'enable' => TYPE_BOOL
                        ];
                        $aData = $this->input->postm($aParams);
                        if ( ! $this->validateRatingValuesData($aData)) {
                            $this->errors->set(_t('', 'Ошибка данных'));
                            break;
                        }
                        $this->model->saveRatingValues($aData);
                    }
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.value.form');
                    break;
                case 'toggle':
                    $nId = $this->input->getpost('id', TYPE_INT);
                    $oRatingValuesData = RatingValues::query()->find($nId);
                    if (empty($oRatingValuesData)) {
                        $this->errors->set(_t('', 'Запись не существует'));
                        break;
                    }
                    if (empty($this->model->saveRatingValues([
                        'id' => $nId,
                        'enable' => ! $oRatingValuesData->enable
                    ]))) {
                        $this->errors->set(_t('', 'Ошибка данных'));
                    };
                    break;
                case 'delete':
                    $nId = $this->input->getpost('id', TYPE_INT);
                    if (RatingValues::query()->where('id', $nId)->delete()) {
                        RatingValuesLang::query()->where('id', $nId)->delete();
                    } else {
                        $this->errors->set(_t('', 'Не удается удалить запись'));
                    }
                    break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $aFilter = [];
        $aFilterParameters = [
            'page' => TYPE_UINT,
            'user_type' => TYPE_INT,
        ];
        $this->input->postgetm($aFilterParameters, $aFilter);

        $aData['pgn'] = '';
        $aData['list'] = [];

        $oRatingValues = $this->getFilterRatingValues($aFilter);
        $nCount = $oRatingValues->count();
        if ( ! empty($nCount)) {
            $oPgn = new Pagination($nCount, static::PAGINATION_PAGE_COUNT, '#', 'jRatingValuesList.page(' . Pagination::PAGE_ID . '); return false;');
            $oPgn->pageNeighbours = 6;
            $aData['pgn'] = $oPgn->view(['arrows' => false]);
            $aData['list'] = $oRatingValues
                ->limit($oPgn->getLimit())
                ->offset($oPgn->getOffset())
                ->get();
        }
        $aData['list'] = $this->viewPHP($aData, 'admin.value.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm([
                'list' => $aData['list'],
                'pgn' => $aData['pgn'],
            ]);
        }

        $aData['f'] = $aFilter;
        $aData['act'] = $sAct;
        return $this->viewPHP($aData, 'admin.value.listing');
    }

    public function average()
    {
        $aFilter = [];
        $this->input->postgetm([
                'page' => TYPE_UINT,
            ],
            $aFilter
        );
        $aRatingValues = [];
        $oRatingValues = RatingValues::query()->get();
        foreach ($oRatingValues as $item) {
            $aRatingValues[$item->sys_key] = $item->lang->title;
        }
        $aData = [
            'rating_values' => $aRatingValues,
            'pgn' => '',
            'list' => []
        ];

        $oRatingUsers = RatingUsers::query();
        $nCount = $oRatingUsers->count();
        if ( ! empty($nCount)) {
            $oPgn = new Pagination($nCount, static::PAGINATION_PAGE_COUNT, '#', 'jRatingUsersList.page(' . Pagination::PAGE_ID . '); return false;');
            $oPgn->pageNeighbours = 6;
            $aData['pgn'] = $oPgn->view(['arrows' => false]);
            $aData['list'] = $oRatingUsers
                ->limit($oPgn->getLimit())
                ->offset($oPgn->getOffset())
                ->get();
        }
        $aData['list'] = $this->viewPHP($aData, 'admin.average.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm([
                'list' => $aData['list'],
                'pgn' => $aData['pgn'],
            ]);
        }

        $aData['f'] = $aFilter;

        return $this->viewPHP($aData, 'admin.average.listing');
    }

    public function settings()
    {
        $aData = [];

        $sAct = $this->input->postget('act', TYPE_STR);
        if ( ! empty($sAct) || Request::isPOST()) {
            $aResponse = [];
            switch ($sAct) {
                case 'save':
                    $aData = $this->validateSettingsData();
                    $aFormulaPrepare = static::setting()->getFormula()->convertToArray();
                    if ( ! empty($aFormulaPrepare) && is_array($aFormulaPrepare)) {
                        $aData[static::setting()->getFormulaPrepare()->name] = $aFormulaPrepare;
                    }
                    $this->configSave($aData);
                    break;
                default:
                    $this->errors->impossible();
            }
            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $aData['keys'] = static::setting()->getFormula()->keys();
        return $this->viewPHP($aData, 'admin.value.settings.form');
    }
}