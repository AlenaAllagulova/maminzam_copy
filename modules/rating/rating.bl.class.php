<?php

use \mastiffApp\db\models\RatingValues;
use \mastiffApp\db\models\RatingOpinions;
use \mastiffApp\db\models\RatingUsers;
use \mastiffApp\tamaranga\modules\rating\Settings;
use \mastiffApp\tamaranga\modules\rating\Average;
use \mastiffApp\db\models\Users as UsersOrm;
use \Carbon\Carbon;

abstract class RatingBase extends \mastiffApp\tamaranga\Module
{
    /** @var RatingModel */
    var $model = null;
    var $securityKey = '1eafcb77d75c2fd78af5a3825caa12e3';

    public static $moduleName = 'rating'; // назване модуля
    protected static $isActive = null; // статус активации модуля sys.php
    protected static $oInstance;

    const ID_NEW = 0; // Id новой записи
    const PAGINATION_PAGE_COUNT = 10; // Кол-во элементов на странице листингов (front, admin)

    const DEFAULT_MIN_STARS_COUNT = 5; // Кол-во звёзд по умолчанию
    const DEFAULT_MIN_STARS_STEP = 1; // Значение шага по умолчанию
    const DEFAULT_VALUE = 0; // Значение по умолчанию

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return Rating
     */
    public static function i()
    {
        return self::getInstance();
    }

    /**
     * @return Rating
     */
    private static function getInstance()
    {
        if (empty(self::$oInstance)) {
            self::$oInstance = bff::module('rating');
        }
        return self::$oInstance;
    }

    /**
     * Вкл/Выкл. в sys.php
     * @return mixed|null
     */
    public static function isActive()
    {
        if (is_null(self::$isActive)) {
            self::$isActive = config::sys('users.rating.stars', false, TYPE_BOOL);
        }
        return self::$isActive;
    }

    /**
     * Тип пользователя или title
     * @param $nType - тип пользователя
     * @return array | string
     */
    public function getUsersType($nType = null)
    {
        $aTypes = [
            static::ID_NEW => _t('', 'Выбрать'),
            Users::TYPE_WORKER => _t('','Исполнитель'),
            Users::TYPE_CLIENT => _t('','Заказчик'),
        ];

        if ( ! is_null($nType) && array_key_exists($nType, $aTypes)) {
            return $aTypes[$nType];
        }

        return $aTypes;
    }

    /**
     * Генерация sys_key пользователя или title
     * @param $nId - id
     * @param $nType - тип пользователя
     * @return array | string
     */
    public static function getSysKey($nId = null, $nType = null)
    {
        $aPrefix = [
            Users::TYPE_WORKER => 'W',
            Users::TYPE_CLIENT => 'C',
        ];

        if (( ! is_null($nId)) && ( ! is_null($nType) && array_key_exists($nType, $aPrefix))) {
            return $aPrefix[$nType] . $nId;
        }

        return $aPrefix;
    }

    /**
     * Валидация данный с формы
     * @param $aData - данные формы
     * @return bool
     */
    public function validateRatingValuesData($aData)
    {
        $aTitle = $aData['title'];
        $bTitleEmpty = true;
        foreach ($aTitle as $k => $v) {
            if ( ! empty($v)) {
                $bTitleEmpty = false;
                break;
            }
        }

        if ($bTitleEmpty) {
            $this->errors->set(_t('', 'Укажите название'));
        }
        if (empty($aData['user_type'])) {
            $this->errors->set(_t('', 'Укажите тип пользователя'));
        }
        if (empty($aData['stars_count'])) {
            $this->errors->set(_t('', 'Укажите количество звёзд'));
        }
        if (empty($aData['stars_step'])) {
            $this->errors->set(_t('', 'Укажите значение шага'));
        }

        return $this->errors->no();
    }

    /**
     * Валидация полей формы настроек
     * @return mixed
     */
    protected function validateSettingsData()
    {
        $aParameters = [
            self::setting()->getPositiveOpinion()->name => TYPE_INT,
            self::setting()->getNegativeOpinion()->name => TYPE_INT,
            self::setting()->getRatingOneStar()->name => TYPE_INT,
            self::setting()->getIntervalOne()->name => TYPE_INT,
            self::setting()->getIntervalTwo()->name => TYPE_INT,
            self::setting()->getIntervalTree()->name => TYPE_INT,
            self::setting()->getFormula()->formula->name => TYPE_NOCLEAN,
        ];
        $this->input->postm($aParameters, $aData);
        return $aData;
    }

    /**
     * Фильтр RatingValues
     * @param array $aFilter
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getFilterRatingValues(array $aFilter)
    {
        $oRatingValues = RatingValues::query();
        if ( ! empty($aFilter['user_type'])) {
            $oRatingValues = $oRatingValues->where('user_type', $aFilter['user_type']);
        }
        return $oRatingValues;
    }

    /**
     * @return Settings
     */
    public static function setting()
    {
        return new Settings();
    }

    /**
     * Крон пересчета сортировки пользователя по формуле
     */
    public function cronUserPositionUpdate()
    {
        if (!bff::cron()) {
            return;
        }
        $oFormula = static::setting()->getFormula();
        $aUsers = Users::model()->searchList();
        foreach ($aUsers as $user) {
            $oUser = UsersOrm::query()
                ->where('user_id', $user['user_id']);
            if ( ! $oUser->exists()) {
                continue;
            }
            $oUser->update([
                'order_from_formula' => $oFormula->getFormulaCalculate($oUser)
            ]);
        }

        self::i()->configSave([
            static::setting()->getLastActionCronPosition()->name => Carbon::now()->toDateTimeString()
        ]);
    }

    /**
     * Крон подсчета среднего рейтинга пользователя по звездам отзывов
     */
    public function cronUserAverageRatingUpdate()
    {
        if (!bff::cron()) {
            return;
        }

        $sLastTimeCron = static::setting()->getLastActionCronAverage()->value;

        $oRatingOpinions = RatingOpinions::query();
        if ( ! empty($sLastTimeCron)) {
            $oRatingOpinions = $oRatingOpinions->where('modified', '>', $sLastTimeCron);
        }

        if ($oRatingOpinions->exists()) {
            $oRatingOpinions = $oRatingOpinions->get();
            $aData = Average::getCalc($oRatingOpinions);
            if ( ! empty($aData)) {
                foreach ($aData as $k => &$v) {
                    foreach ($v['stars'] as $key => $val) {
                        $v['stars'][$key] = $v['stars'][$key] / $v['iterations'];
                        $v['total'] += $v['stars'][$key];
                    }
                    $v['total'] = $v['total'] / count($v['stars']);

                    $this->model->saveRatingUsers($k, $v['stars'], $v['total']);

                } unset($v);
            }
        }

        self::i()->configSave([
            static::setting()->getLastActionCronAverage()->name => Carbon::now()->toDateTimeString()
        ]);
    }
}