<?php

define('TABLE_RATING_VALUES', DB_PREFIX . 'rating_values');
define('TABLE_RATING_LANG', DB_PREFIX . 'rating_values_lang');
define('TABLE_RATING_SETTINGS', DB_PREFIX . 'rating_settings');

use \mastiff\tamaranga\Model;
use \mastiffApp\db\models\RatingValues;
use \mastiffApp\db\models\RatingValuesLang;

class RatingModel extends Model
{
    /** @var RatingBase */
    var $controller;

    public $translationsFields = [
        'title' => TYPE_STR,  # Название
    ];

    public function init()
    {
        parent::init();
    }

    /**
     * Insert | Update RatingValues record
     * @param array $aData - data
     * @return int|mixed|null
     */
    public function saveRatingValues(array $aData)
    {
        if ( ! isset($aData['id'])) {
            return null;
        }
        if (empty($aData['id'])) {

            $oRatingValues = new RatingValues();
            $oRatingValues->user_type = $aData['user_type'];
            $oRatingValues->stars_count = $aData['stars_count'];
            $oRatingValues->stars_step = $aData['stars_step'];
            $oRatingValues->enable = $aData['enable'];
            $oRatingValues->save();

            if ( ! empty($oRatingValues->id)) {
                $aData['id'] = $oRatingValues->id;
                RatingValues::query()
                    ->where('id', $oRatingValues->id)
                    ->update([
                        'sys_key' => Rating::getSysKey(
                            $oRatingValues->id,
                            $oRatingValues->user_type
                        )
                    ]);
                $this->setTranslations($oRatingValues->id, $aData);
            }
        } else {
            $this->setTranslations($aData['id'], $aData);
            $aData = array_diff_key($aData, $this->translationsFields);
            if ( ! empty($aData['user_type'])) {
                $aData['sys_key'] = Rating::getSysKey($aData['id'], $aData['user_type']);
            }
            RatingValues::query()
                ->where('id', $aData['id'])
                ->update($aData);
        }
        return $aData['id'];
    }

    /**
     * Получить переводы
     * @param $oRatingValues - данные RatingValues
     * @return array
     */
    public function getTranslations(RatingValues $oRatingValues)
    {
        $aData = [];
        $aLangs = $this->getLanguages();
        foreach ($this->translationsFields as $field => $type) {
            $aData[$field] = [];
            foreach ($aLangs as $k => $v) {
                $aData[$field][$k] = $oRatingValues->langs->where('lang', $k)->first()->title;
            }
        }
        return $aData;
    }

    /**
     * Установить переводы
     * @param $nId
     * @param array $aData данные с мультиязычными полями
     * @return bool|null
     */
    public function setTranslations($nId, array $aData)
    {
        $aData = array_intersect_key($aData, $this->translationsFields);
        if (empty($aData) || ! is_array($aData)) {
            return null;
        }
        $aLangs = $this->getLanguages();
        RatingValuesLang::query()
            ->where('id', $nId)
            ->delete();
        foreach ($aLangs as $k => $v) {
            if (isset($aData['title'][$k])) {
                $oRatingValuesLang = new RatingValuesLang();
                $oRatingValuesLang->id = $nId;
                $oRatingValuesLang->lang = $k;
                $oRatingValuesLang->title = $aData['title'][$k];
                $oRatingValuesLang->save();
            }
        }
        return true;
    }

    /**
     * Сохранить средний рейтинг пользователя по звездам
     * @param $nUserId - id пользователя
     * @param $aStarsAvr - усредненные звезды по качествам
     * @param $nTotalAvr - общей средний рейтинг по звездам
     */
    public function saveRatingUsers($nUserId, $aStarsAvr, $nTotalAvr)
    {
        $oRatingUsersData = \mastiffApp\db\models\RatingUsers::query()
            ->where('user_id', $nUserId);

        if ($oRatingUsersData->exists()) {
            $oRatingUsersDataOld = $oRatingUsersData->first();
            $oAverage = \mastiffApp\tamaranga\modules\rating\Average::getInstance();
            if ($oAverage->isSerialized($oRatingUsersDataOld->stars_average)) {
                $aStarsAverage = unserialize($oRatingUsersDataOld->stars_average);
                foreach ($aStarsAverage as $k => $v) {
                    if (array_key_exists($k, $aStarsAvr)) {
                        $aStarsAvr[$k] = ($v + $aStarsAvr[$k]) / 2;
                    }
                }
            }
            $oRatingUsersData
                ->update([
                    'stars_average' => serialize($aStarsAvr),
                    'total_average' => ($nTotalAvr + $oRatingUsersDataOld->total_average) / 2
                ]);
        } else {
            $oRatingUsers = new \mastiffApp\db\models\RatingUsers();
            $oRatingUsers->user_id = $nUserId;
            $oRatingUsers->stars_average = serialize($aStarsAvr);
            $oRatingUsers->total_average = $nTotalAvr;
            $oRatingUsers->save();
        }
    }
}