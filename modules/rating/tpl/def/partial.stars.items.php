<?php
/**
 * @var $this Rating
 * @var $star \mastiffApp\db\models\RatingValues
 */
?>

<div class="row">
<div class="col-sm-12">
<table class="s-stars">
<? foreach ($stars as $star): ?>
    <tr>
        <td class="hidden">
            <?= $star->lang->title ?>:
        </td>
        <td>
            <input
                type="text"
                name="stars[<?= $star->sys_key ?>]"
                value="<?= (isset($values) && key_exists($star->sys_key, $values)) ? $values[$star->sys_key] : '' ?>"
                class="<?= (isset($read_only) && $read_only) ? $this::JS_CLASS_RATING_STARS_ITEM_READONLY : $this::JS_CLASS_RATING_STARS_ITEM ?>"
                data-min="0"
                data-sys_key="<?= $star->sys_key ?>"
                data-max="<?= $star->stars_count ?>"
                data-step="<?= $star->stars_step ?>"
                data-size="xs"
                data-display-only="<?= isset($read_only) ? $read_only : true ?>"
                data-show-clear="false"
                data-show-caption="false"
            >
        </td>
    </tr>
<? endforeach; ?>
</table>
</div>
</div>
