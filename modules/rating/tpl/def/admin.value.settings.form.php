<?php
/**
 * @var $this Rating
 */
?>

<form name="RatingSettingsForm" id="RatingSettingsForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="save" value="1" />
    <table class="admtbl tbledit">
        <tr>
            <td class="row1" colspan="2">
                <h4><?= _t('', 'Система начисления рейтинга') ?></h4>
            </td>
        </tr>
        <tr>
            <td class="row1" width="180">
                <span>
                    <?= _t('','Положительный отзыв') ?>:
                </span>
            </td>
            <td class="row2">
                <input type="hidden"
                       name="<?= $this::setting()->getPositiveOpinion()->name ?>"
                       value="<?= $this::setting()->getPositiveOpinion()->value ?>"
                />
                <strong>
                    <?= $this::setting()->getPositiveOpinion()->value ?>
                </strong>
                <span>
                    <?= _t('','единиц рейтинга Тамаранги') ?>
                    <span class="icon-info-sign" title="<?= _t('','Редактирование параметра доступно в sys.php, название users.rating.opinions.positive') ?>">&nbsp;</span>
                </span>
            </td>
        </tr>
        <tr>
            <td class="row1">
                <span>
                    <?= _t('','Отрицательный отзыв') ?>:
                </span>
            </td>
            <td class="row2">
                <input type="hidden"
                       name="<?= $this::setting()->getNegativeOpinion()->name ?>"
                       value="<?= $this::setting()->getNegativeOpinion()->value ?>"
                />
                <strong>
                    <?= $this::setting()->getNegativeOpinion()->value ?>
                </strong>
                <span>
                    <?= _t('','единиц рейтинга Тамаранги') ?>
                    <span class="icon-info-sign" title="<?= _t('','Редактирование параметра доступно в sys.php, название users.rating.opinions.negative') ?>">&nbsp;</span>
                </span>
            </td>
        </tr>
        <tr>
            <td class="row1">
                <span>
                    <?= _t('','Рейтинг за одну звезду') ?>:
                </span>
            </td>
            <td class="row2">
                <input type="number"
                       name="<?= $this::setting()->getRatingOneStar()->name ?>"
                       value="<?= $this::setting()->getRatingOneStar()->value ?>"
                />
                <span>
                    <?= _t('','единиц рейтинга Тамаранги') ?>
                </span>
            </td>
        </tr>
        <tr>
            <td class="row1">
                &nbsp;
            </td>
            <td class="row2">
                <span>
                    <b>
                        <?= _t('','На рейтинг пользователя могут повлиять дополнительные коэффициенты, указанные в sys.php') ?>
                    </b>
                </span>
            </td>
        </tr>
        <tr>
            <td class="row1" colspan="2"><hr></td>
        </tr>
        <tr>
            <td class="row1" colspan="2">
                <h4><?= _t('', 'Параметры временных интервалов') ?></h4>
            </td>
        </tr>
        <tr>
            <td class="row1" width="180">
                <span>
                    <?= _t('','Первый интервал') ?>:
                </span>
            </td>
            <td class="row2">
                <span>
                    <?= _t('','От [days] до ', ['days' => 0]) ?>
                </span>
                <input type="number"
                       name="<?= $this::setting()->getIntervalOne()->name ?>"
                       value="<?= $this::setting()->getIntervalOne()->value ?>"
                />
                <span>
                    <?= _t('','дней') ?>
                </span>
            </td>
        </tr>
        <tr>
            <td class="row1">
                <span>
                    <?= _t('','Второй интервал') ?>:
                </span>
            </td>
            <td class="row2">
                <span>
                    <?= _t('','От [days] до ', ['days' => ($this::setting()->getIntervalOne()->value + 1)]) ?>
                </span>
                <input type="number"
                       name="<?= $this::setting()->getIntervalTwo()->name ?>"
                       value="<?= $this::setting()->getIntervalTwo()->value ?>"
                />
                <span>
                    <?= _t('','дней') ?>
                </span>
            </td>
        </tr>
        <tr>
            <td class="row1">
                <span>
                    <?= _t('','Третий интервал') ?>:
                </span>
            </td>
            <td class="row2">
                <span>
                    <?= _t('','От [days] до ', ['days' => ($this::setting()->getIntervalTwo()->value + 1)]) ?>
                </span>
                <input type="number"
                       name="<?= $this::setting()->getIntervalTree()->name ?>"
                       value="<?= $this::setting()->getIntervalTree()->value ?>"
                />
                <span>
                    <?= _t('','дней') ?>
                </span>
            </td>
        </tr>
        <tr>
            <td class="row1" colspan="2"><hr></td>
        </tr>
        <tr>
            <td class="row1" colspan="2">
                <h4><?= _t('', 'Формула сортировки пользователей') ?></h4>
            </td>
        </tr>
        <tr>
            <td class="row1">
                <span>
                    <?= _t('','Формула') ?>:
                </span>
            </td>
            <td class="row2">
                <input type="text"
                       name="<?= $this::setting()->getFormula()->formula->name ?>"
                       value="<?= $this::setting()->getFormula()->formula->value ?>"
                       style="width: 90%;"
                />
            </td>
        </tr>
        <tr>
            <td class="row1">
                <p></p>
                <span>
                    <?= _t('','Доступные параметры') ?>:
                </span>
            </td>
            <td class="row2">
                <p></p>
                <p>
                    <?= _t('','Ниже перечислены параметры - ключи, которые могут быть использованы в формуле для расчёта в новой системе рейтинга пользователя.') ?>
                    <strong>
                        <?= _t('','Все параметры состоят из заглавных латинских букв и чисел, формула не долзна содержать пробелы и кирилицу.') ?>
                    </strong>
                </p>
                <ul>
                    <? foreach ($keys as $k => $v): ?>
                    <li>
                        <strong><?= $k ?></strong>
                        <span> - <?= $v ?></span>;
                    </li>
                    <? endforeach; ?>
                    <li>
                        <strong>|</strong>
                        <span> - <?= _t('','Приминить модификатор к параметру (вертикальная черта)') ?></span>;
                    </li>
                    <li>
                        <strong>{<?= _t('','<название ключа>') ?>}</strong>
                        <span> - <?= _t('','Обозначение для ключей') ?></span>;
                    </li>
                    <li>
                        <strong>{<?= _t('','<название ключа>') ?>|<?= _t('','<название модификатора>') ?>}</strong>
                        <span> - <?= _t('','Обозначение для ключей с модификатором') ?></span>;
                    </li>
                </ul>
                <p>
                    <?= _t('','Пример использования') ?>:
                </p>
                <ul>
                    <li>
                        <strong>
                            ((({OP|DI1}-{ON|DI1})/100)*50)+((({OP|DI2}-{ON|DI2})/100)*20)+(({LO|DI1}/100)*30)+{RU}
                        </strong>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="row1">
                <p></p>
                <strong>
                    <?= $this::setting()->getLastActionCronPosition()->value ?>
                </strong>
            </td>
            <td class="row2">
                <p></p>
                <p>
                    <?= _t('','Дата и время последнего обновления рейтинга пользователей'); ?>
                </p>
            </td>
        </tr>
        <tr>
            <td class="row1">
                <p></p>
                <strong>
                    <?= $this::setting()->getLastActionCronAverage()->value ?>
                </strong>
            </td>
            <td class="row2">
                <p></p>
                <p>
                    <?= _t('','Дата и время последнего обновления среднего рейтинга пользователей'); ?>
                </p>
            </td>
        </tr>
        <tr>
            <td class="row1" colspan="2"><hr></td>
        </tr>
        <tr class="footer">
            <td colspan="2">
                <input type="button" class="btn btn-success button submit" value="<?= _t('', 'Сохранить') ?>" onclick="jRatingSettingsForm.save(this);" data-act="save" />
                <input type="button" class="btn button cancel" value="<?= _t('', 'Отмена') ?>" onclick="jRatingSettingsForm.cancel();" />
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    var jRatingSettingsForm =
        (function(){
            var $form = $('#RatingSettingsForm');
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            function getUrl(self)
            {
                return ajaxUrl + '&act=' + $(self).data('act');
            }
            return {
                save: function(self)
                {
                    bff.ajax(getUrl(self), $form.serialize(), function(data) {
                        if(data && data.success) {
                            bff.success('<?= _t('', 'Данные успешно сохранены') ?>');
                        }
                    });
                },
                cancel: function()
                {
                    if (typeof jRatingValuesFormManager !== 'undefined') {
                        jRatingValuesFormManager.action('cancel');
                    }
                }
            };
        }());
</script>