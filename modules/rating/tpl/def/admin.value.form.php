<?php
/**
 * @var $this Rating
 */
$aData = HTML::escape($aData, 'html', array('user_id','keyword'));
$edit = ! empty($id);
?>

<form name="RatingValuesForm" id="RatingValuesForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
    <input type="hidden" name="save" value="1" />
    <input type="hidden" name="id" value="<?= $id ?>" />
    <table class="admtbl tbledit">
        <?= $this->locale->buildForm($aData, 'agencies-item','
        <tr>
            <td class="row1" width="160">
                <span class="field-title"><?= _t(\'\',\'Название\') ?><span class="required-mark">*</span>:</span>
            </td>
            <td class="row2">
                <input class="stretch <?= $key ?>" type="text" id="rating-values-title-<?= $key ?>" name="title[<?= $key ?>]" value="<?= HTML::escape($aData[\'title\'][$key]); ?>" />
            </td>
        </tr>
        '); ?>
        <tr class="required">
            <td class="row1" colspan="2"><hr></td>
        </tr>
        <tr>
            <td class="row1">
                <span class="field-title"><?= _t('','Тип пользователя') ?><span class="required-mark">*</span>:</span>
            </td>
            <td class="row2">
                <? if ( ! empty($user_types)): ?>
                    <select name="user_type" <?= FORDEV ? '' : 'disabled'; ?>>
                        <? foreach ($user_types as $k => $v): ?>
                            <option value="<?= $k ?>" <?= ($k == $user_type) ? 'selected' : '' ?>> <?= $v ?> </option>
                        <? endforeach; ?>
                    </select>
                    <? if (! FORDEV): ?>
                        <span class="icon-info-sign" title="<?= _t('','Редактирование доступно в режиме разработчика') ?>">&nbsp;</span>
                    <? endif; ?>
                <? endif; ?>
            </td>
        </tr>
        <tr>
            <td class="row1">
                <span class="field-title"><?= _t('','Количество звёзд') ?><span class="required-mark">*</span>:</span>
            </td>
            <td class="row2">
                <input class="stretch" type="number" name="stars_count" value="<?= $stars_count ?>" />
            </td>
        </tr>
        <tr>
            <td class="row1">
                <span class="field-title"><?= _t('','Значение шага') ?><span class="required-mark">*</span>:</span>
            </td>
            <td class="row2">
                <input class="stretch" type="number" name="stars_step" value="<?= $stars_step ?>" />
            </td>
        </tr>
        <tr>
            <td class="row1">
                <span class="field-title"><?= _t('','Включено') ?>:</span>
            </td>
            <td class="row2">
                <input type="checkbox" name="enable" <?= ($enable) ? 'checked' : '' ?> />
            </td>
        </tr>
        <tr class="required">
            <td class="row1" colspan="2"><hr></td>
        </tr>
        <tr class="footer">
            <td colspan="2">
                <input type="submit" class="btn btn-success button submit" value="<?= _t('','Сохранить') ?>" onclick="jRatingValuesForm.save(false);" />
                <? if ($edit): ?>
                    <input type="button" class="btn btn-success button submit" value="<?= _t('','Сохранить и вернуться') ?>" onclick="jRatingValuesForm.save(true);" />
                    <? if (FORDEV): ?>
                        <input type="button" onclick="jRatingValuesForm.del(); return false;" class="btn btn-danger button delete" value="<?= _t('','Удалить') ?>" />
                    <? endif; ?>
                <? endif; ?>
                <input type="button" class="btn button cancel" value="<?= _t('', 'Отмена') ?>" onclick="jRatingValuesFormManager.action('cancel');" />
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    var jRatingValuesForm =
        (function(){
            var $progress, $form, formChk, id = parseInt(<?= $id ?>);
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function(){
                $progress = $('#RatingValuesFormProgress');
                $form = $('#RatingValuesForm');

            });
            return {
                del: function()
                {
                    if( id > 0 ) {
                        bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                            false, {progress: $progress, repaint: false, onComplete:function(){
                                bff.success('<?= _t('','Запись успешно удалена') ?>');
                                jRatingValuesFormManager.action('cancel');
                                jRatingValuesList.refresh();
                            }});
                    }
                },
                save: function(returnToList)
                {
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('<?= _t('','Данные успешно сохранены') ?>');
                            if(returnToList || ! id) {
                                jRatingValuesFormManager.action('cancel');
                                jRatingValuesList.refresh( ! id);
                            }
                        }
                    }, $progress);
                }
            };
        }());
</script>