<?php
/**
 * @var $this Rating
 */

$sAdminLinkUser = $this->adminLink('user_edit', 'users&rec=%d&tuid=%s');
$sAdminLinkSettings = $this->adminLink('settings', 'agency&id=%d');

foreach ($list as $v): ?>
    <tr>
        <td class="small">
            <?= $v->id ?>
        </td>
        <td class="left">
            <?= $v->sys_key ?>
        </td>
        <td class="left">
            <?= $v->lang->title ?>
        </td>
        <td>
            <?= $this->getUsersType($v->user_type); ?>
        </td>
        <td>
            <a class="but <?= $v->enable ? 'un' : '' ?>block button-toggle" title="<?= _t('','Включен') ?>" href="#" data-type="enabled" data-id="<?= $v->id ?>"></a>
            <a class="but edit button-edit" title="<?= _t('','Редактировать') ?>" href="#" data-id="<?= $v->id ?>"></a>
            <? if (FORDEV): ?>
                <a class="but del button-del" title="<?= _t('','Удалить значение?') ?>" href="#" data-id="<?= $v->id ?>"></a>
            <? endif; ?>
        </td>
    </tr>
<? endforeach; ?>

<? if (empty($list)): ?>
    <tr class="norecords">
        <td colspan="5">
            <?= _t('agency','ничего не найдено') ?>
        </td>
    </tr>
<? endif;