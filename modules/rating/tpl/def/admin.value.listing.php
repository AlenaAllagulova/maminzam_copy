<?php
/**
 * @var $this Rating
 */

?>
<?= tplAdmin::blockStart(_t('', 'Добавление значение'), false, [
    'id' => 'RatingValuesFormBlock',
    'style' => 'display:none;'
]); ?>
    <div id="RatingValuesFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart(_t('','Список значений'), true, [
    'id' => 'RatingValuesListBlock',
    'class' => (!empty($act) ? 'hidden' : '')
], FORDEV ? [
    'title' => '+ ' . _t('', 'добавить значение'),
    'class' => 'ajax',
    'onclick' => 'return jRatingValuesFormManager.action(\'add\',0);'
] : []
); ?>

<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="RatingValuesListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />

        <div class="left">
            <? if ( ! empty($user_types)): ?>
                <select name="user_type" onchange="jRatingValuesList.onUserType(this);">
                    <? foreach ($user_types as $k => $v): ?>
                        <option value="<?= $k ?>" <?= ($k == $f['user_type']) ? 'selected' : '' ?>> <?= $v ?> </option>
                    <? endforeach; ?>
                </select>
            <? endif; ?>
        </div>
        <div class="right">
            <div id="RatingValuesProgress" class="progress" style="display: none;"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="RatingValuesListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="70">ID</th>
        <th class="left">SysKey</th>
        <th class="left"><?= _t('', 'Название') ?></th>
        <th width="150"><?= _t('', 'Тип пользователя') ?></th>
        <th width="135"><?= _t('', 'Действие') ?></th>
    </tr>
    </thead>
    <tbody id="RatingValuesList">
    <?= $list ?>
    </tbody>
</table>
<div id="RatingValuesListPgn"><?= $pgn ?></div>

<?= tplAdmin::blockStop(); ?>

<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">

    </div>
</div>

<script type="text/javascript">
    var jRatingValuesFormManager = (function(){
        var $progress, $block, $blockCaption, $formContainer, process = false;
        var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        $(function(){
            $formContainer = $('#RatingValuesFormContainer');
            $progress = $('#RatingValuesProgress');
            $block = $('#RatingValuesFormBlock');
            $blockCaption = $block.find('span.caption');

            <? if( ! empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
        });

        function onFormToggle(visible)
        {
            if(visible) {
                jRatingValuesList.toggle(false);
                if(typeof jRatingValuesForm !== 'undefined') {
                    jRatingValuesForm.onShow();
                }
            } else {
                jRatingValuesList.toggle(true);
            }
        }

        function initForm(type, id, params)
        {
            if( process ) return;
            bff.ajax(ajaxUrl,params,function(data){
                if(data && (data.success || intval(params.save)===1)) {
                    var blockCaption = '';
                    switch(type) {
                        case 'add':
                        {
                            blockCaption = '<?= _t('', 'Добавление') ?>';
                        }
                            break;
                        case 'edit':
                        {
                            blockCaption = '<?= _t('', 'Редактирование') ?>';
                        }
                            break;
                        case 'settings':
                        {
                            blockCaption = '<?= _t('', 'Настройки') ?>';
                        }
                            break;
                    }

                    $blockCaption.html(blockCaption);
                    $formContainer.html(data.form);
                    $block.show();
                    $.scrollTo( $blockCaption, {duration:500, offset:-300});
                    onFormToggle(true);
                    if(bff.h) {
                        window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                    }
                } else {
                    jRatingValuesList.toggle(true);
                }
            }, function(p){ process = p; $progress.toggle(); });
        }

        function action(type, id, params)
        {
            params = $.extend(params || {}, {act:type});
            switch(type) {
                case 'add':
                {
                    if( id > 0 ) return action('edit', id, params);
                    if($block.is(':hidden')) {
                        initForm(type, id, params);
                    } else {
                        action('cancel');
                    }
                }
                    break;
                case 'cancel':
                {
                    $block.hide();
                    onFormToggle(false);
                }
                    break;
                case 'edit':
                {
                    if( ! (id || 0) ) return action('add', 0, params);
                    params.id = id;
                    initForm(type, id, params);
                }
                    break;
                case 'settings':
                {
                    if ( ! id ) return action('cancel');
                    params.id = id;
                    initForm(type, id, params);
                }
                    break;
            }
            return false;
        }

        return {
            action: action
        };
    }());

    var jRatingValuesList =
        (function()
        {
            var $progress, $block, $list, $listTable, $listPgn, filters, processing = false;
            var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';

            $(function(){
                $progress  = $('#RatingValuesProgress');
                $block     = $('#RatingValuesListBlock');
                $list      = $block.find('#RatingValuesList');
                $listTable = $block.find('#RatingValuesListTable');
                $listPgn   = $block.find('#RatingValuesListPgn');
                filters    = $block.find('#RatingValuesListFilters').get(0);

                $list.on('click', 'a.button-edit', function(){
                    var id = intval($(this).data('id'));
                    if(id>0) jRatingValuesFormManager.action('edit',id);
                    return false;
                });

                $list.delegate('a.button-settings', 'click', function(){
                    var id = intval($(this).data('id'));
                    if(id>0) jRatingValuesFormManager.action('settings',id);
                    return false;
                });

                $list.delegate('a.button-toggle', 'click', function(){
                    var id = intval($(this).data('id'));
                    var type = $(this).data('type');
                    if(id>0) {
                        var params = {progress: $progress, link: this};
                        bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, params);
                    }
                    return false;
                });

                $list.delegate('a.button-del', 'click', function(){
                    var id = intval($(this).data('id'));
                    if(id>0) del(id, this);
                    return false;
                });

                $(window).bind('popstate',function(e){
                    if('state' in window.history && window.history.state === null) return;
                    var loc = document.location;
                    var actForm = /act=(add|edit)/.exec( loc.search.toString() );
                    if( actForm!=null ) {
                        var actId = /id=([\d]+)/.exec(loc.search.toString());
                        jRatingValuesFormManager.action(actForm[1], actId && actId[1]);
                    } else {
                        jRatingValuesFormManager.action('cancel');
                        updateList(false);
                    }
                });

            });

            function isProcessing()
            {
                return processing;
            }

            function del(id, link)
            {
                bff.ajaxDelete('<?= _t('', 'Удалить?') ?>', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
                return false;
            }

            function updateList(updateUrl)
            {
                if(isProcessing()) return;
                var f = $(filters).serialize();
                bff.ajax(ajaxUrl, f, function(data){
                    if(data) {
                        $list.html( data.list );
                        $listPgn.html( data.pgn );
                        if(updateUrl !== false && bff.h) {
                            window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                        }
                    }
                }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
            }

            function setPage(id)
            {
                filters.page.value = intval(id);
            }

            return {
                submit: function(resetForm)
                {
                    if(isProcessing()) return false;
                    setPage(1);
                    if(resetForm) {
                        //
                    }
                    updateList();
                },
                page: function (id)
                {
                    if(isProcessing()) return false;
                    setPage(id);
                    updateList();
                },
                refresh: function(resetPage, updateUrl)
                {
                    if(resetPage) setPage(0);
                    updateList(updateUrl);
                },
                toggle: function(show)
                {
                    if(show === true) {
                        $block.show();
                        if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                    }
                    else $block.hide();
                },
                onUserType: function(option)
                {
                    updateList();
                }
            };
        }());
</script>