<?php
/**
 * @var $this Rating
 */
?>

<div class="form-group">
    <div class="<?= $this::JS_CLASS_RATING_STARS_LIST ?>"><?= (isset($items) && ! empty($items)) ? $items : '' ?></div>
</div>