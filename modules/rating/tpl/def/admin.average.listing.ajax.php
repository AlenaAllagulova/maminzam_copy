<?php
/**
 * @var $this Rating
 */

$sAdminLinkUser = $this->adminLink('user_edit', 'users&rec=%d&tuid=%s');

foreach ($list as $v): ?>
    <tr>
        <td class="left">
            <a href="<?= sprintf($sAdminLinkUser, $v->user_id, Users::i()->makeTUID($v->user_id)) ?>" target="_blank">
                <?= $v->user->login ?>
            </a>
        </td>
        <td class="left">
            <b>
                <?= $v->total_average ?>
            </b>
        </td>
        <td class="left">
            <? if ($this->isSerialized($v->stars_average)):  ?>
                <? $stars_average = unserialize($v->stars_average); ?>
                <? foreach ($stars_average as $key => $val): ?>
                    <? if (isset($rating_values[$key])): ?>
                        <?= $rating_values[$key] ?>: <b><?= $val ?></b><br>
                    <? endif; ?>
                <? endforeach; ?>
            <? endif; ?>
        </td>
    </tr>
<? endforeach; ?>

<? if (empty($list)): ?>
    <tr class="norecords">
        <td colspan="5">
            <?= _t('agency','ничего не найдено') ?>
        </td>
    </tr>
<? endif;