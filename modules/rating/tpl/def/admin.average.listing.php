<?php
/**
 * @var $this Rating
 */

?>

<?= tplAdmin::blockStart(_t('','Список значений'), true, [
    'id' => 'RatingUsersListBlock',
    'class' => (!empty($act) ? 'hidden' : '')
]); ?>

<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="RatingUsersListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />

        <div class="right">
            <div id="RatingUsersProgress" class="progress" style="display: none;"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="RatingUsersListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th class="left"><?= _t('','Пользователь') ?></th>
        <th class="left"><?= _t('','Общее среднее') ?></th>
        <th class="left"><?= _t('', 'Среднее по качествам') ?></th>
    </tr>
    </thead>
    <tbody id="RatingUsersList">
    <?= $list ?>
    </tbody>
</table>
<div id="RatingUsersListPgn"><?= $pgn ?></div>

<?= tplAdmin::blockStop(); ?>

<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">

    </div>
</div>

<script type="text/javascript">
    var jRatingUsersList =
        (function()
        {
            var $progress, $block, $list, $listTable, $listPgn, filters, processing = false;
            var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';

            $(function(){
                $progress  = $('#RatingUsersProgress');
                $block     = $('#RatingUsersListBlock');
                $list      = $block.find('#RatingUsersList');
                $listTable = $block.find('#RatingUsersListTable');
                $listPgn   = $block.find('#RatingUsersListPgn');
                filters    = $block.find('#RatingUsersListFilters').get(0);

                $(window).bind('popstate',function(e){
                    if('state' in window.history && window.history.state === null) return;
                    var loc = document.location;
                    var actForm = /act=(add|edit)/.exec( loc.search.toString() );
                    if( actForm!=null ) {
                        var actId = /id=([\d]+)/.exec(loc.search.toString());
                        jRatingUsersFormManager.action(actForm[1], actId && actId[1]);
                    } else {
                        jRatingUsersFormManager.action('cancel');
                        updateList(false);
                    }
                });

            });

            function isProcessing()
            {
                return processing;
            }

            function updateList(updateUrl)
            {
                if(isProcessing()) return;
                var f = $(filters).serialize();
                bff.ajax(ajaxUrl, f, function(data){
                    if(data) {
                        $list.html( data.list );
                        $listPgn.html( data.pgn );
                        if(updateUrl !== false && bff.h) {
                            window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                        }
                    }
                }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
            }

            function setPage(id)
            {
                filters.page.value = intval(id);
            }

            return {
                page: function (id)
                {
                    if(isProcessing()) return false;
                    setPage(id);
                    updateList();
                }
            };
        }());
</script>