<p class=" rating-do-it">
    <input
        type="hidden"
        name="total_average"
        value="<?= empty($rating_user) ? 0 : round($rating_user->total_average) ?>"
        class="<?= Rating::JS_CLASS_RATING_STARS_ITEM_READONLY ?> rating-loading"
        data-min="0"
        data-max="5"
        data-step="1"
        data-size="none"
        data-show-clear="false"
        data-disabled="true"
        data-show-caption="false"
    >
</p>