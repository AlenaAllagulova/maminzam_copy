<?php

use \mastiffApp\db\models\RatingValues;
use \mastiffApp\db\models\RatingOpinions;
use \mastiffApp\db\models\RatingUsers;
use \mastiffApp\db\models\Orders as OrdersOrm;
use \mastiffApp\db\models\Opinions as OpinionsOrm;
use mastiffApp\db\models\Users as UsersOrm;
use \Carbon\Carbon;

class Rating extends RatingBase
{
    const JS_CLASS_RATING_STARS_BOX = 'j-rating_stars_box';
    const JS_CLASS_RATING_STARS_BOX_MODAL = 'j-rating_stars_box_modal';
    const JS_CLASS_RATING_STARS_TYPES_BOX = 'j-rating_stars_type_box';
    const JS_CLASS_RATING_STARS_LIST = 'j-rating_stars_list';
    const JS_CLASS_RATING_STARS_ITEM = 'j-rating_stars_item';
    const JS_CLASS_RATING_STARS_ITEM_READONLY = 'j-rating_stars_item-readonly';

    /**
     * @param string $starsItem
     * @param string $starsBox
     * @param string $btnOpinion
     * @return string
     */
    public static function JS_PARAMETERS_VIEW($starsItem, $starsBox = null, $btnOpinion = null)
    {
        $aParameters = [
            'starsItem' => '.'.$starsItem
        ];
        if (!is_null($starsBox)) {
            $aParameters['starsBox'] = $starsBox;
        }
        if (!is_null($btnOpinion)) {
            $aParameters['btnOpinion'] = $btnOpinion;
        }
        return func::php2js($aParameters);
    }

    /**
     * @param string $btnOpinion
     * @param string $starsBox
     * @param string $starsTypesBox
     * @param array $starsTypesData
     * @param string $starsList
     * @param string $starsItem
     * @return string
     */
    public static function JS_PARAMETERS_EDIT($btnOpinion, $starsBox = null, $starsTypesBox = null,
                                              array $starsTypesData = null, $starsList = null, $starsItem = null)
    {
        $aParameters = [
            'starsBox' => (preg_match('/^[#|\.]/', $starsBox) ? '' : '.').(is_null($starsBox) ? Rating::JS_CLASS_RATING_STARS_BOX : $starsBox),
            'starsTypesBox' => '.'.(is_null($starsTypesBox) ? Rating::JS_CLASS_RATING_STARS_TYPES_BOX : $starsTypesBox),
            'starsTypesData' => (is_null($starsTypesData) || !is_array($starsTypesData) ? [
                Opinions::TYPE_POSITIVE => true,
                Opinions::TYPE_NEGATIVE => false,
                Opinions::TYPE_NEUTRAL => true
            ] : $starsTypesData),
            'starsList' => '.'.(is_null($starsList) ? Rating::JS_CLASS_RATING_STARS_LIST : $starsList),
            'starsItem' => '.'.(is_null($starsItem) ? Rating::JS_CLASS_RATING_STARS_ITEM : $starsItem),
            'btnOpinion' => $btnOpinion
        ];
        return func::php2js($aParameters);
    }

    public static function getViewStars($nUserId = null, $nOpinionId = null, $bReadOnly = true)
    {
        if ( ! static::isActive()) {
            return null;
        }
        $aData = [];
        if (( ! is_null($nUserId) && ! empty($nUserId)) && ( ! is_null($nOpinionId) && ! empty($nOpinionId))) {
            $oRatingOpinions = RatingOpinions::query()
                ->where('user_id', $nUserId)
                ->where('opinion_id', $nOpinionId);
            if ($oRatingOpinions->exists() && Rating::i()->isSerialized($oRatingOpinions->first()->stars)) {
                $oTmp = OpinionsOrm::query()
                    ->where('id', $nOpinionId);
                if ($oTmp->exists()) {
                    $oTmp = $oTmp
                        ->first()
                        ->orderItem;
                    if ($oTmp->exists()) {
                        $oStars = RatingValues::query()
                            ->where('enable', true);
                        switch ($nUserId) {
                            case $oTmp->performer_id:
                                $oStars = $oStars
                                    ->where('user_type', Users::TYPE_WORKER);
                                break;
                            case $oTmp->user_id:
                                $oStars = $oStars
                                    ->where('user_type', Users::TYPE_CLIENT);
                                break;
                        }
                        if ($oStars->exists()) {
                            $aData['stars'] = $oStars->get();
                            $aData['values'] = unserialize($oRatingOpinions->first()->stars);
                            $aData['read_only'] = $bReadOnly;
                            $aData['items'] = Rating::i()->viewPHP($aData, 'partial.stars.items');
                        }
                    }
                }
            }
        }
        return Rating::i()->viewPHP($aData, 'partial.stars.list');
    }

    public static function getViewTotalAverage($nUserId = null, $sTpl = 'partial.total.average')
    {
        if ( ! static::isActive()) {
            return null;
        }
        if (is_null($nUserId)) {
            $nUserId = User::id();
        }
        $aData['rating_user'] = null;
        $oRatingUsers = RatingUsers::query()
            ->where('user_id', $nUserId);
        if ($oRatingUsers->exists()) {
            $aData['rating_user'] = $oRatingUsers->first();
        }
        return Rating::i()->viewPHP($aData, $sTpl);
    }

    public static function addStars($nOpinionId, $aOpinionData)
    {
        if ( ! static::isActive()) {
            return null;
        }
        $aData = self::i()->getUserOpinionData($nOpinionId, $aOpinionData);

        $oRatingOpinions = new RatingOpinions();
        $oRatingOpinions->author_id = $aData['author_id'];
        $oRatingOpinions->user_id = $aData['user_id'];
        $oRatingOpinions->opinion_id = $aData['opinion_id'];
        $oRatingOpinions->stars = $aData['stars_serialize'];
        $oRatingOpinions->sum = $aData['sum'];
        $oRatingOpinions->modified = Carbon::now()->toDateTimeString();
        $oRatingOpinions->save();

        self::updateUserRating($oRatingOpinions->user_id, $oRatingOpinions->sum);
    }

    public static function updateStars($nOpinionId, $aOpinionData)
    {
        if ( ! static::isActive()) {
            return null;
        }

        $aData = Rating::i()->getUserOpinionData($nOpinionId, $aOpinionData);

        $oRatingOpinions = RatingOpinions::query()
            ->where('opinion_id', $nOpinionId);

        if ($oRatingOpinions->exists() && ($oRatingOpinions->first()->sum != $aData['sum'])) {
            self::updateUserRating(
                $oRatingOpinions->first()->user->user_id,
                $aData['sum'] - $oRatingOpinions->first()->sum
            );
            $oRatingOpinions->update([
                'stars' => $aData['stars_serialize'],
                'sum' => $aData['sum'],
                'modified' => Carbon::now()->toDateTimeString()
            ]);
        }
    }

    public static function deleteStars($nOpinionId)
    {
        $oRatingOpinions = RatingOpinions::query()
            ->where('opinion_id', $nOpinionId);

        if ($oRatingOpinions->exists()) {
            self::updateUserRating(
                $oRatingOpinions->first()->user->user_id,
                -intval($oRatingOpinions->first()->sum)
            );
            $oRatingOpinions->delete();
        }
    }

    public function ajax()
    {
        $aResponse = array();
        switch ($this->input->getpost('act', TYPE_STR)) {
            case 'getStarsItems':
                $nOrderId = $this->input->postget('orderId', TYPE_INT);
                $sData = $this->getStarsItems(User::id(), $nOrderId, false, false);
                if (is_null($sData)) {
                    $this->errors->impossible();
                    break;
                }
                $aResponse['items'] = $sData['items'];
                $aResponse['html'] = $sData['html'];
                break;
            default:
                $this->errors->impossible();
        }
        $this->ajaxResponseForm($aResponse);
    }

    public function getStarsItems($nUserId, $nOrderId, $bGetContent = true, $bReadOnly = true)
    {
        if (empty($nOrderId) || empty($nUserId)) {
            return null;
        }

        $oStars = null;
        if (
            OrdersOrm::query()
                ->where('user_id', $nUserId)
                ->where('id', $nOrderId)
                ->exists()
        ) {
            $oStars = RatingValues::query()
                ->where('user_type', Users::TYPE_WORKER);
        } elseif (
            OrdersOrm::query()
                ->where('performer_id', $nUserId)
                ->where('id', $nOrderId)
                ->exists()
        ) {
            $oStars = RatingValues::query()
                ->where('user_type', Users::TYPE_CLIENT);
        }

        if (is_null($oStars)) {
            return null;
        }
        $oStars = $oStars->where('enable', true);
        $aData['stars'] = $oStars->get();

        $aData['items'] = [];
        $aData['opinion_id'] = bff::input()->postget('opinionId', TYPE_INT);
        $aData['order_id'] = $nOrderId;
        $aStarValues = [];
        $oRatingOpinions = RatingOpinions::query()
            ->where('author_id', $nUserId)
            ->where('opinion_id', $aData['opinion_id']);
        if ($oRatingOpinions->exists()) {
            $sValue = $oRatingOpinions->first()->stars;
            if ($this->isSerialized($sValue)) {
                $aStarValues = unserialize($sValue);
            }
        }

        foreach ($aData['stars'] as $star) {
            if ($oRatingOpinions->exists() && ! isset($sValue)) {
                $sValue = $oRatingOpinions->get()->stars;
                if ($this->isSerialized($sValue)) {
                    $aStarValues = unserialize($sValue);
                }
            }
            $aData['items'][$star->sys_key] = array_key_exists($star->sys_key, $aStarValues)
                ? $aStarValues[$star->sys_key]
                : static::DEFAULT_VALUE;
        }

        $aData['read_only'] = $bReadOnly;
        $aData['html'] = $this->viewPHP($aData, 'partial.stars.items');
        if ($bGetContent) {
            return $aData['html'];
        }

        return $aData;
    }

    /**
     * Сортировка по формуле
     * @param $sOrderadd
     * @return string
     */
    public static function getOrderFormula($sOrder)
    {
        if (static::isActive()) {
            $sOrder = 'U.pro DESC, U.order_from_formula DESC';
        }
        return $sOrder;
    }

    private function getUserOpinionData($nOpinionId, $aOpinionData)
    {
        $aData = self::i()->input->postgetm([
            'stars' => TYPE_ARRAY
        ]);
        $aData['opinion_id'] = $nOpinionId;
        $aData['sum'] = 0;
        foreach ($aData['stars'] as $k => $v) {
            $aData['sum'] += ($v * static::setting()->getRatingOneStar()->value);
        }
        $aData['stars_serialize'] = serialize(is_array($aData['stars']) ? $aData['stars'] : []);
        return array_merge($aData, $aOpinionData);
    }

    private static function updateUserRating($nUserId, $nSumStars)
    {
        $oUsersOrm = UsersOrm::query()->where('user_id', $nUserId);
        if ($oUsersOrm->exists()) {
            return !empty($oUsersOrm->update([
                'rating' => $oUsersOrm->first()->rating + ($nSumStars)
            ]));
        }
        return false;
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {
        return [
            'cronUserPositionUpdate' => [
                'period' => '*/5 * * * *'
            ],
            'cronUserAverageRatingUpdate' => [
                'period' => '*/5 * * * *'
            ],
        ];
    }
}