<?php

class Notes extends NotesBase
{
    const NUMBER_OF_PAGES = 25;

    public function listing()
    {
        if (!$this->haveAccessTo('Notes')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || Request::isPOST()) {
            $aResponse = [];
            switch ($sAct) {
                case 'add':
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validateNoteData();
                    if ($bSubmit) {
                        if ($this->errors->no()) {
                            $aData['id'] = $this->model->noteSave(0, $aData);
                            $this->model->noteSave($aData['id'], $aData);
                        }

                        break;
                    }
                    $aData['id'] = 0;
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.note.form');
                    break;
                case 'edit':
                    $bSubmit = $this->input->postget('save', TYPE_BOOL);
                    $nNoteID = $this->input->postget('id', TYPE_UINT);

                    if (!$nNoteID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {
                        $aData = $this->validateNoteData();

                        if ($this->errors->no()) {
                            $this->model->noteSave($nNoteID, $aData);
                        }

                        $aData['id'] = $nNoteID;
                    } else {
                        $aData = $this->model->noteData($nNoteID, true);

                        if (empty($aData)) {
                            $this->errors->unknownRecord();
                            break;
                        }

                        $aData['act'] = $sAct;
                        $aResponse['form'] = $this->viewPHP($aData, 'admin.note.form');
                    }
                    break;
                case 'delete':
                    $nNoteID = $this->input->postget('id', TYPE_UINT);
                    if (!$nNoteID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->noteData($nNoteID, true);
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->model->noteDelete($nNoteID);
                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    }

                    break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && Request::isAJAX()) {
                $this->ajaxResponseForm($aResponse);
            }
        }

        $f = [];
        $this->input->postgetm([
            'page' => TYPE_UINT, # пагинация
            'tab' => TYPE_INT,  # табы
            'title' => TYPE_NOTAGS, # поиск
        ], $f);
        # формируем фильтр списка
        $sql = [];
        $sqlOrder = '';
        $mPerpage = self::NUMBER_OF_PAGES;
        $aData['pgn'] = '';
        # Табы
        switch ($f['tab']) {
            case 0: # весь список
                break;
        }

        # Поисковая форма
        if (!empty($f['title'])) {
            $sql[':title'] = [
                '(N.id = ' . intval($f['title']) . ' OR U.email LIKE :name OR U.login LIKE :name)',
                ':name' => '%' . $f['title'] . '%'
            ];
        }
        if ($mPerpage !== false) {
            $nCount = $this->model->notesOrdersListing($sql, true);
            $sqlLimit = '';
            if ($nCount > 0) {

                $oPgn = new Pagination($nCount,
                    $mPerpage,
                    $this->adminLink('listing' . '&page=' . Pagination::PAGE_ID .
                        '&tab=' . $f['tab'] . '&title=' . $f['title']));
                $sqlLimit = $oPgn->getLimitOffset();
                $aData['pgn'] = $oPgn->view();
            }

            $aData['list'] = $this->model->notesOrdersListing($sql, false, $sqlLimit, $sqlOrder);
        } else {
            $aData['list'] = $this->model->notesOrdersListing($sql, false, '', $sqlOrder);
        }

        $aData['list'] = $this->viewPHP($aData, 'admin.notes.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm([
                'list' => $aData['list'],
                'pgn' => $aData['pgn'],
            ]);
        }

        $aData['f'] = $f;
        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        return $this->viewPHP($aData, 'admin.notes.listing');
    }

    /**
     * Обрабатываем параметры запроса
     * @return array параметры
     */
    protected function validateNoteData()
    {
        $aData = [];
        $this->input->postm([
            'note' => TYPE_NOCLEAN,
        ], $aData);

        return $aData;
    }

}