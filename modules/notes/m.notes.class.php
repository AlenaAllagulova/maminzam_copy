<?php

class M_Notes
{
    static function declareAdminMenu(CMenu $menu, Security $security)
    {
        $module = 'notes';
        $menuTitle = _t('Notes', 'Заметки');

        $menu->assign($menuTitle, 'Список', $module, 'listing', true, 1);
        
    }
}