<?php

class NotesModel extends Model
{
    /** @var NotesBase */
    var $controller;

    /**
     * Список заметок по заказам (admin)
     * @param array $aFilter фильтр списка
     * @param bool $bCount только подсчет кол-ва
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */

    public function notesOrdersListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $aFilter = $this->prepareFilter($aFilter, 'N');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(N.id) 
                                               FROM 
                                                   ' . TABLE_ORDERS_NOTES . '  N 
                                               INNER JOIN '.TABLE_USERS.' U ON N.author_id = U.user_id
                                               INNER JOIN '.DB_PREFIX.'orders'.' O ON N.order_id = O.id
                                            ' . $aFilter['where'],
                $aFilter['bind']);

        }


        return $this->db->select('
               SELECT
                   N.*,
                   U.name,
                   U.surname,
                   U.login,
                   U.email,
                   U.pro,
                   U.verified,
                   O.title as order_title,
                   O.keyword as order_keyword
               FROM 
                   ' . TABLE_ORDERS_NOTES . ' N 
               INNER JOIN '.TABLE_USERS.' U ON N.author_id = U.user_id
               INNER JOIN '.DB_PREFIX.'orders'.' O ON N.order_id = O.id
                ' . $aFilter['where']
            . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
            . $sqlLimit, $aFilter['bind']);
    }

    /**
     * Получение данных заметки
     * @param integer $nNoteID ID заметки
     * @param boolean $bEdit при редактировании
     * @return array
     */
    public function noteData($nNoteID, $bEdit = false)
    {
        $aData = [];
        if ($bEdit) {
            $aData = $this->db->one_array(
                'SELECT 
                       N.*,
                       U.name,
                       U.surname,
                       U.login,
                       U.email,
                       U.pro,
                       U.verified,
                       O.title as order_title,
                       O.keyword as order_keyword
                        FROM 
                             ' . TABLE_ORDERS_NOTES . ' N 
                        INNER JOIN '.TABLE_USERS.' U ON N.author_id = U.user_id
                        INNER JOIN '.DB_PREFIX.'orders'.' O ON N.order_id = O.id
                        WHERE N.id = :id',
                [':id' => $nNoteID]
            );
        }
        return $aData;
    }

    /**
     * Удаление заметки
     * @param integer $nNoteID ID заметки
     * @return boolean
     */
    public function noteDelete($nNoteID)
    {
        if (empty($nNoteID)) {
            return false;
        }
        $res = $this->db->delete(TABLE_ORDERS_NOTES, ['id' => $nNoteID]);

        return !empty($res);
    }

    /*
     * Сохранение заметки
     * @param integer $nNoteID ID заметки
     * @param array $aData данные заметки
     * @return boolean|integer
     */
    public function noteSave($nNoteID, array $aData)
    {
        if (empty($aData)) {
            return false;
        }

        if ($nNoteID > 0) {
            $res = $this->db->update(
                TABLE_ORDERS_NOTES,
                $aData,
                ['id' => $nNoteID]);

            return !empty($res);
        } else {
            $nNoteID = $this->db->insert(
                TABLE_ORDERS_NOTES,
                $aData
            );

            return !empty($nNoteID);
        }
    }
    

}