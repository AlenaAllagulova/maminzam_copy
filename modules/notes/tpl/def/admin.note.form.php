<?php

$aData = HTML::escape($aData, 'html', ['note']);
$edit = !empty($id);
$aTabs = array(
    'info' => _t('', 'Основное'),
);
$sDeleteMsg = _t('', 'Удалить?');

?>
<form name="NotesForm"
      id="NotesForm"
      class="j-spec-cat-form"
      action="<?= ($edit ? $this->adminLink('listing&act=edit') : $this->adminLink('listing&act=add')) ?>"
      enctype="multipart/form-data"
>
    <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>"/>
    <input type="hidden" name="save" value="1"/>
    <input type="hidden" name="id" value="<?= $id ?>"/>
    <div id="NotesFormProgress" class="progress" style="display: none;"></div>
    <!-- таб: Основное -->
    <div class="j-tab j-tab-info">
        <table class="admtbl tbledit">
                <tr class="required">
                    <td class="row1">
                        <span class="field-title">
                            <?=_t('','Заметка');?>
                            <span class="required-mark">*</span>:
                        </span>
                    </td>
                    <td class="row2">
                        <textarea name="note">
                            <?= !empty($aData['name']) ? HTML::escape($aData['note']) : '' ;?>
                        </textarea>
                    </td>
                </tr>
            <tr>
                <td class="row1">
                    <span class="field-title"><?= _t('', 'Заказ:'); ?></span>
                </td>
                <td class="row2">
                    <a href="<?= Orders::url('view', ['id' => $aData['order_id'], 'keyword' => $aData['order_keyword']]) ?>">
                        <?= $aData['order_title']?>
                    </a>
                </td>
            </tr>
            <tr>
                <td class="row1">
                    <span class="field-title"><?= _t('', 'Пользователь'); ?></span>
                </td>
                <td class="row2">
                    <?= tpl::userLink($aData, 'no-login, no-surname'); ?>
                </td>
            </tr>
        </table>
    </div>

    <div style="margin-top: 10px;">
        <input type="button"
               class="btn btn-success button submit j-btn-save"
               value="<?= _t('', 'Сохранить и вернуться'); ?>"
               data-redirect="true"
        />
        <input type="button" onclick="jNotesForm.del(); return false;"
               class="btn btn-danger button delete"
               value="<?= _t('', 'Удалить'); ?>"/>
        <input type="button"
               class="btn button cancel"
               value="<?= _t('', 'Отмена'); ?>"
               onclick="jNotesFormManager.action('cancel');"/>
    </div>
</form>

<script type="text/javascript">
    <?js::start()?>
    var jNotesForm =
        (function () {
            var $progress, $form, formChk, id = parseInt(<?= $id ?>), f;
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

            $(function () {
                $progress = $('#NotesFormProgress');
                $form = $('#NotesForm');
                f = $form.get(0);
                $('.j-btn-save').on('click', function (e) {
                    nothing(e);
                    var isRedirect = $(this).data('redirect');
                    var formData = new FormData($form.get(0));
                    $.ajax({
                        url: ajaxUrl,
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function (data) {
                            if (data.errors.length > 0) {
                                bff.error(data.errors);
                            } else {
                                bff.success('Данные успешно сохранены');

                                if (isRedirect == true || !id) {
                                    window.setTimeout(
                                        function () {
                                            jNotesFormManager.action('cancel');
                                            jNotesList.refresh(!id);
                                        }, 3000);
                                }
                            }

                        }
                    });
                });
            });
            return {
                del: function () {
                    if (id > 0) {
                        bff.ajaxDelete('<?=$sDeleteMsg;?>', id, ajaxUrl + '&act=delete&id=' + id,
                            false, {
                                progress: $progress, repaint: false, onComplete: function () {
                                    bff.success('<?=_t('', 'Запись успешно удалена');?>');
                                    jNotesFormManager.action('cancel');
                                    jNotesList.refresh();
                                }
                            });
                    }
                },
                onShow: function () {
                    formChk = new bff.formChecker($form);
                },
            };
        }());
    <?js::stop()?>
</script>