<?php
/**
 * @var $this Notes
 */

tpl::includeJS(array('autocomplete'), true);

?>
<?= tplAdmin::blockStart(_t('',''), false, ['id' => 'NotesFormBlock', 'style' => 'display:none;']); ?>
<div id="NotesFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart(_t('','Список заметок'), true, ['id' => 'NotesListBlock', 'class' => (!empty($act) ? 'hidden' : '')]); ?>
<?
$aTabs = [
    0 => ['t' => _t('','Заметки к заказам')],
];
?>

<div class="tabsBar" id="NotesListTabs">
    <? foreach ($aTabs as $k => $v) : ?>
        <span class="tab <? if ($f['tab'] == $k): ?>tab-active<? endif; ?>">
            <a href="#" onclick="return jNotesList.onTab(<?= $k ?>,this);"><?= $v['t'] ?></a>
        </span>
    <? endforeach; ?>
</div>

<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="NotesListFilters"
          onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>"/>
        <input type="hidden" name="ev" value="<?= bff::$event ?>"/>
        <input type="hidden" name="page" value="<?= $f['page'] ?>"/>
        <input type="hidden" name="tab" value="<?= $f['tab'] ?>"/>

        <div class="left">
            <div class="left" style="margin: 8px 0 0 8px;">
                <input style="width:284px;"
                       type="text"
                       maxlength="150"
                       name="title"
                       placeholder="<?=_t('','ID заметки или login/email пользователя');?>"
                       value="<?= isset($f['title'])? HTML::escape($f['title']):'' ?>"/>
                <input type="button" class="btn btn-small button cancel" style="margin-left: 8px;"
                       onclick="jNotesList.submit(false);" value="<?=_t('','найти');?>"/>
            </div>
            <div class="left" style="margin: 8px 0 0 8px;">
                <a class="ajax cancel" onclick="jNotesList.submit(true); return false;"><?=_t('','сбросить');?></a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="right">
            <div id="NotesProgress" class="progress" style="display: none;"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="NotesListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="70">ID</th>
        <th class="left"><?=_t('','Заметка');?></th>
        <th class="left"><?=_t('','Заказ');?></th>
        <th class="right"><?=_t('','Пользователь');?></th>
        <th width="135"><?=_t('','Действие');?></th>
    </tr>
    </thead>
    <tbody id="NotesList">
    <?= $list ?>
    </tbody>
</table>
<div id="NotesListPgn"><?= $pgn ?></div>

<?= tplAdmin::blockStop(); ?>

<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">

    </div>
</div>

<script type="text/javascript">
    <?js::start()?>
    var jNotesFormManager = (function () {
        var $progress, $block, $blockCaption, $formContainer, process = false;
        var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        $(function () {
            $formContainer = $('#NotesFormContainer');
            $progress = $('#NotesProgress');
            $block = $('#NotesFormBlock');
            $blockCaption = $block.find('span.caption');

            <? if( ! empty($act)):?>action('<?= $act ?>',<?= $id ?>);<? endif; ?>
        });

        function onFormToggle(visible) {
            if (visible) {
                jNotesList.toggle(false);
                if (jNotesForm) jNotesForm.onShow();
            } else {
                jNotesList.toggle(true);
            }
        }

        function initForm(type, id, params) {
            if (process) return;
            bff.ajax(ajaxUrl, params, function (data) {
                if (data && (data.success || intval(params.save) === 1)) {
                    $blockCaption.html(type == 'add' ? '<?=_t('','Добавление')?>' : '<?=_t('','Редактирование')?>');
                    $formContainer.html(data.form);
                    $block.show();
                    $.scrollTo($blockCaption, {duration: 500, offset: -300});
                    onFormToggle(true);
                    if (bff.h) {
                        window.history.pushState({}, document.title, ajaxUrl + '&act=' + type + '&id=' + id);
                    }
                } else {
                    jNotesList.toggle(true);
                }
            }, function (p) {
                process = p;
                $progress.toggle();
            });
        }

        function action(type, id, params) {
            params = $.extend(params || {}, {act: type});
            switch (type) {
                case 'add': {
                    if (id > 0) return action('edit', id, params);
                    if ($block.is(':hidden')) {
                        initForm(type, id, params);
                    } else {
                        action('cancel');
                    }
                }
                    break;
                case 'cancel': {
                    $block.hide();
                    onFormToggle(false);
                }
                    break;
                case 'edit': {
                    if (!(id || 0)) return action('add', 0, params);
                    params.id = id;
                    initForm(type, id, params);
                }
                    break;
            }
            return false;
        }

        return {
            action: action
        };
    }());

    var jNotesList =
        (function () {
            var $progress, $block, $list, $listTable, $listPgn, filters, processing = false, tab = <?= $f['tab'] ?>;
            var ajaxUrl = '<?= $this->adminLink(bff::$event . '&act='); ?>';

            $(function () {
                $progress = $('#NotesProgress');
                $block = $('#NotesListBlock');
                $list = $block.find('#NotesList');
                $listTable = $block.find('#NotesListTable');
                $listPgn = $block.find('#NotesListPgn');
                filters = $block.find('#NotesListFilters').get(0);

                $list.delegate('a.note-edit', 'click', function () {
                    var id = intval($(this).data('id'));
                    if (id > 0) jNotesFormManager.action('edit', id);
                    return false;
                });

                $list.delegate('a.note-del', 'click', function () {
                    var id = intval($(this).data('id'));
                    if (id > 0) del(id, this);
                    return false;
                });

                $(window).bind('popstate', function (e) {
                    if ('state' in window.history && window.history.state === null) return;
                    var loc = document.location;
                    var actForm = /act=(add|edit)/.exec(loc.search.toString());
                    if (actForm != null) {
                        var actId = /id=([\d]+)/.exec(loc.search.toString());
                        jNotesFormManager.action(actForm[1], actId && actId[1]);
                    } else {
                        jNotesFormManager.action('cancel');
                        updateList(false);
                    }
                });

            });

            function isProcessing() {
                return processing;
            }

            function del(id, link) {

                var msg = 'Удалить?';
                bff.ajaxDelete(msg, id, ajaxUrl + 'delete&id=' + id, link, {
                    progress: $progress,
                    repaint: false
                });
                return false;
            }

            function updateList(updateUrl) {
                if (isProcessing()) return;
                var f = $(filters).serialize();
                bff.ajax(ajaxUrl, f, function (data) {
                    if (data) {
                        $list.html(data.list);
                        $listPgn.html(data.pgn);
                        if (updateUrl !== false && bff.h) {
                            window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                        }
                    }
                }, function (p) {
                    $progress.toggle();
                    processing = p;
                    $list.toggleClass('disabled');
                });
            }

            function setPage(id) {
                filters.page.value = intval(id);
            }

            return {
                submit: function (resetForm) {
                    if (isProcessing()) return false;
                    setPage(1);
                    if (resetForm) {
                        filters['title'].value = '';
                        //
                    }
                    updateList();
                },
                page: function (id) {
                    if (isProcessing()) return false;
                    setPage(id);
                    updateList();
                },
                onTab: function (tabNew, link) {
                    if (isProcessing() || tabNew == tab) return false;
                    setPage(1);
                    tab = filters.tab.value = tabNew;
                    updateList();
                    $(link).parent().addClass('tab-active').siblings().removeClass('tab-active');
                    return false;
                },
                refresh: function (resetPage, updateUrl) {
                    if (resetPage) setPage(0);
                    updateList(updateUrl);
                },
                toggle: function (show) {
                    if (show === true) {
                        $block.show();
                        if (bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                    }
                    else $block.hide();
                }
            };
        }());

    <?js::stop()?>
</script>