<?php
/**
 * @var $this Notes
 */
foreach ($list as $k => &$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k % 2) ?>">
        <td class="small"><?= $id ?></td>
        <td class="left"><?= mb_strcut($v['note'], 0, 100) ?></td>
        <td class="left">
            <a href="<?= Orders::url('view', ['id' => $v['order_id'], 'keyword' => $v['order_keyword']]) ?>">
                <?= $v['order_title']?>
            </a>
        </td>
        <td lass="right"><?= tpl::userLink($v, 'no-login, no-surname'); ?></td>
        <td>
            <a class="but edit note-edit" title="<?= _t('', 'Редактировать'); ?>" href="javascript:void(0);"
               data-id="<?= $id ?>"></a>
            <a class="but del note-del"
               title="<?= _t('', 'Удалить'); ?>"
               href="#"
               data-id="<?= $id ?>"
            ></a>
        </td>
        </td>
    </tr>
<? endforeach;
unset($v);

if (empty($list) && !isset($skip_norecords)): ?>
    <tr class="norecords">
        <td colspan="7">
            <?= _t('', 'ничего не найдено'); ?>
        </td>
    </tr>
<? endif;