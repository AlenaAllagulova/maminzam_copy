<?php
?>
<tr>
    <td class="row1"><span class="field-title">Стоимость</span><span class="required-mark">*</span>:</td>
    <td class="row2">
        <input type="text" min="0" name="price" value="<?= $v['price'] ?>" class="input-mini" /><?= $pricePrefix; ?>
    </td>
</tr>
<tr>
    <td class="row1"><span class="field-title">Стоимость за</span>:</td>
    <td class="row2">
        <label class="radio">
            <input type="radio" name="per_day" value="0" <?= empty($v['per_day']) ? 'checked="checked"' : '' ?> />
            указанный период
        </label>
        <label class="radio">
            <input type="radio" name="per_day" value="1" <?= ! empty($v['per_day']) ? 'checked="checked"' : '' ?> />
            один день
        </label>
    </td>
</tr>
<tr class="j-price-period" <?= ! empty($v['per_day']) ? 'style="display:none;"' : '' ?>>
    <td class="row1"><span class="field-title">Период действия услуги</span><span class="required-mark">*</span>:</td>
    <td class="row2">
        <input type="text" name="period" min="1" value="<?= $v['period'] ?>" class="input-mini"><div class="help-inline">дней</div>
    </td>
</tr>
<script type="text/javascript">
    $(function(){
        $('[name="per_day"]').change(function(){
            $('.j-price-period').toggle(intval($(this).val()));
        });
    });
</script>