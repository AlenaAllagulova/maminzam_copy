<?php

# Таблицы
define('TABLE_INTERNALMAIL',               DB_PREFIX . 'internalmail'); # сообщения
define('TABLE_INTERNALMAIL_CONTACTS',      DB_PREFIX . 'internalmail_contacts'); # контакты пользователей
define('TABLE_INTERNALMAIL_FOLDERS',       DB_PREFIX . 'internalmail_folders'); # папки сообщений
define('TABLE_INTERNALMAIL_FOLDERS_USERS', DB_PREFIX . 'internalmail_folders_users'); # связь папок с пользователями

use bff\utils\LinksParser;

class InternalMailModel_ extends Model
{
    /** @var InternalMailBase */
    var $controller;

    /**
     * Отправка сообщения получателю(получателям)
     * @param integer $authorID ID отправителя
     * @param integer $recipientID ID получателя
     * @param string $message текст сообщения
     * @param string $attachment приложение
     * @param boolean $sendEmail отправлять уведомление на почту
     * @return integer отправленного сообщения или 0 (ошибка отправки)
     */
    public function sendMessage($authorID, $recipientID, $message = '', $attachment = '', $sendEmail = true)
    {
        $authorID = intval($authorID);
        if ($authorID <= 0) {
            return 0;
        }
        $recipientID = intval($recipientID);
        if ($recipientID <= 0) {
            return 0;
        }

        $messageCreated = $this->db->now();
        $messageID = $this->db->insert(TABLE_INTERNALMAIL, array(
                'author'    => $authorID,
                'recipient' => $recipientID,
                'message'   => nl2br($message),
                'attach'    => (!empty($attachment) ? $attachment : ''),
                'is_new'    => 1,
                'created'   => $messageCreated,
            ), 'id'
        );

        if (!empty($messageID)) {
            $this->updateContact($authorID, $recipientID, $messageID, $messageCreated, false);
            $this->updateContact($recipientID, $authorID, $messageID, $messageCreated, true);
            $this->updateNewMessagesCounter($recipientID);

            if($sendEmail){
                # уведомление о новом сообщении
                Users::sendMailTemplateToUser($recipientID, 'internalmail_new_message', array(
                    'link' => InternalMail::url('my.messages'),
                    'message' => nl2br(tpl::truncate($message, config::sysAdmin('internalmail.send.message.truncate', 100, TYPE_UINT), '...', true)),
                ), Users::ENOTIFY_INTERNALMAIL);
            }

            return $messageID;
        }

        return 0;
    }

    /**
     * Очистка текста сообщения
     * @param string $message текст сообщения
     * @param int $maxLength максимально допустимое кол-во символов или 0 - без ограничений
     * @param bool|array $activateLinks подсветка ссылок
     * @return string очищенный текст
     */
    public function cleanMessage($message, $maxLength = 4000, $activateLinks = true)
    {
        $message = htmlspecialchars($message);
        $message = $this->input->cleanTextPlain($message, config::sysAdmin('internalmail.message.maxlength', $maxLength, TYPE_UINT), $activateLinks);

        # дополнительная обработка
        $message = bff::filter('internalmail.message.validate', $message);

        return $message;
    }

    /**
     * Помечаем все сообщения переписки как "прочитанные"
     * @param integer $userID ID текущего пользователя
     * @param integer $interlocutorID ID собеседника
     * @param boolean $bUpdateNewCounter обновлять счетчик кол-ва новых сообщений
     * @return integer кол-во помеченных сообщений
     */
    public function setMessagesReaded($userID, $interlocutorID, $updateNewCounter = true)
    {
        $updated = $this->db->update(TABLE_INTERNALMAIL, array(
                'is_new' => 0,
                'readed' => $this->db->now(),
            ), array(
                'recipient' => $userID,
                'author'    => $interlocutorID,
                'is_new'    => 1,
            )
        );

        $this->db->update(TABLE_INTERNALMAIL_CONTACTS, array(
                'messages_new' => 0
            ), array(
                'user_id'         => $userID,
                'interlocutor_id' => $interlocutorID,
            )
        );

        if ($updateNewCounter && !empty($updated)) {
            $this->updateNewMessagesCounter($userID);
        }

        return $updated;
    }

    /**
     * Обновляем данные контакта
     * @param integer $userID ID пользователя
     * @param integer $interlocutorID ID собеседника
     * @param integer $messageID ID последнего сообщения
     * @param string $messageCreated дата создания последнего сообщения
     * @param boolean $messageIsNew является ли сообщение новым (обновляем контакт получателя)
     * @return boolean
     */
    public function updateContact($userID, $interlocutorID, $messageID, $messageCreated, $messageIsNew)
    {
        # обновляем существующий контакт
        $data = array(
            'last_message_id'   => $messageID,
            'last_message_date' => $messageCreated,
            'messages_total = messages_total + 1',
        );
        if ($messageIsNew) {
            $data[] = 'messages_new = messages_new + 1';
        }
        $res = $this->db->update(TABLE_INTERNALMAIL_CONTACTS, $data, array(
                'user_id'         => $userID,
                'interlocutor_id' => $interlocutorID,
            )
        );
        if (empty($res)) {
            # создаем контакт
            $res = $this->db->insert(TABLE_INTERNALMAIL_CONTACTS, array(
                    'user_id'           => $userID,
                    'interlocutor_id'   => $interlocutorID,
                    'last_message_id'   => $messageID,
                    'last_message_date' => $messageCreated,
                    'messages_total'    => 1,
                    'messages_new'      => ($messageIsNew ? 1 : 0),
                ), false
            );
        }

        return !empty($res);
    }

    /**
     * Перемещаем собеседника в папку
     * @param integer $userID ID пользователя (кто перемещает)
     * @param integer $interlocutorID ID пользователя собеседника (кого перемещает)
     * @param integer $folderID ID папки (куда перемещает)
     * @param boolean $toggle true - удалять из папки, если собеседник уже в ней находится
     * @return integer
     */
    public function interlocutorToFolder($userID, $interlocutorID, $folderID, $toggle = true)
    {
        if ($userID <= 0 || $interlocutorID <= 0 || $folderID <= 0) {
            return 0;
        }

        $exists = $this->db->one_data('SELECT interlocutor_id FROM ' . TABLE_INTERNALMAIL_FOLDERS_USERS . '
                WHERE user_id = :userID AND interlocutor_id = :interlocutorID AND folder_id = :folderID',
            array(':userID' => $userID, ':interlocutorID' => $interlocutorID, ':folderID' => $folderID)
        );

        if ($exists) {
            if (!$toggle) {
                return 1;
            }
            $this->db->delete(TABLE_INTERNALMAIL_FOLDERS_USERS,
                array('user_id' => $userID, 'interlocutor_id' => $interlocutorID, 'folder_id' => $folderID)
            );

            return 0;
        } else {
            $this->db->insert(TABLE_INTERNALMAIL_FOLDERS_USERS, array(
                    'user_id'         => $userID,
                    'interlocutor_id' => $interlocutorID,
                    'folder_id'       => $folderID,
                ), false
            );

            return 1;
        }
    }

    /**
     * Получаем список папок, в которых находится собеседник
     * @param integer $userID ID текущего пользователя
     * @param integer|array $interlocutorID ID собеседников
     * @return array
     */
    public function getInterlocutorFolders($userID, $interlocutorID)
    {
        if (empty($interlocutorID)) {
            return array();
        }

        $isArray = true;
        if (!is_array($interlocutorID)) {
            $interlocutorID = array($interlocutorID);
            $isArray = false;
        }
        $interlocutorID = array_map('intval', $interlocutorID);

        $folders = $this->db->select('SELECT folder_id as f, interlocutor_id as id
                        FROM ' . TABLE_INTERNALMAIL_FOLDERS_USERS . '
                    WHERE user_id = :userID
                      AND interlocutor_id IN(' . join(',', $interlocutorID) . ')',
            array(':userID' => $userID)
        );

        if (empty($folders)) {
            return array();
        }

        if (!$isArray && sizeof($interlocutorID) == 1) {
            $result = array();
            foreach ($folders as $v) {
                $result[] = $v['f'];
            }

            return $result;
        } else {
            $result = array();
            foreach ($folders as $v) {
                $result[$v['id']][] = $v['f'];
            }

            return $result;
        }
    }

    /**
     * Проверяет добавлен ли пользователь собеседником в папку
     * @param integer $userID ID проверяющего
     * @param integer $interlocutorID ID собеседника
     * @param integer $folderID ID папки
     * @return boolean
     */
    public function isUserInFolder($userID, $interlocutorID, $folderID)
    {
        if ($interlocutorID <= 0 || $folderID <= 0) {
            return false;
        }

        return (bool)$this->db->one_data('SELECT COUNT(*)
                     FROM ' . TABLE_INTERNALMAIL_FOLDERS_USERS . '
                   WHERE user_id = :interlocutorID
                     AND interlocutor_id = :userID
                     AND folder_id = :folder',
            array(':interlocutorID' => $interlocutorID, ':userID' => $userID, ':folder' => $folderID)
        );
    }

    /**
     * Создание/обновление папки пользователя
     * @param integer $userID ID пользователя
     * @param integer $folderID ID папки
     * @param array $data данные
     * @return boolean|integer ID созданной папки
     */
    public function userFolderSave($userID, $folderID, array $data = array())
    {
        if ($folderID > 0) {
            if ($userID <= 0 || empty($data)) {
                return false;
            }
            $res = $this->db->update(TABLE_INTERNALMAIL_FOLDERS, $data, array(
                    'id'      => $folderID,
                    'user_id' => $userID,
                )
            );

            return !empty($res);
        } else {
            if ($userID <= 0 || empty($data)) {
                return 0;
            }
            $data['user_id'] = $userID;

            return $this->db->insert(TABLE_INTERNALMAIL_FOLDERS, $data, 'id');
        }
    }

    /**
     * Список папок пользователя
     * @param integer $nUserID ID пользователя (владельца папок)
     * @return array|mixed
     */
    public function userFolderList($nUserID)
    {
        if( ! $nUserID) return array();
        return $this->db->select_key('
            SELECT id, title FROM '.TABLE_INTERNALMAIL_FOLDERS.'
            WHERE user_id = :user
            ORDER BY title ', 'id', array(':user' => $nUserID));
    }

    /**
     * Удаление папки пользователя
     * @param integer $nUserID ID пользователя (владельца папки)
     * @param integer $folderID ID папки
     * @return bool
     */
    public function userFolderDelete($nUserID, $folderID)
    {
        $this->db->delete(TABLE_INTERNALMAIL_FOLDERS_USERS,
            array('user_id' => $nUserID, 'folder_id' => $folderID)
        );

        return $this->db->delete(TABLE_INTERNALMAIL_FOLDERS,
            array('user_id' => $nUserID, 'id' => $folderID)
        );
    }

    /**
     * Формирование ленты сообщений пользователей (spy)
     * @param boolean $countOnly только подсчет кол-ва
     * @param string $sqlLimit лимит выборки
     * @return array|integer
     */
    public function getMessagesSpyLenta($countOnly = false, $sqlLimit = '') # adm
    {
        if ($countOnly) {
            return (integer)$this->db->one_data('SELECT COUNT(*) FROM ' . TABLE_INTERNALMAIL);
        }

        $aData = $this->db->select('SELECT I.id, I.created, I.message, I.is_new,
                   U1.user_id as from_id, U1.name as from_name, U1.login as from_login, U1.avatar as from_avatar, U1.sex as from_sex,
                   U2.user_id as to_id, U2.name as to_name, U2.login as to_login, U2.avatar as to_avatar, U2.sex as to_sex
                   FROM ' . TABLE_INTERNALMAIL . ' I
                       INNER JOIN ' . TABLE_USERS . ' U1 ON U1.user_id = I.author
                       INNER JOIN ' . TABLE_USERS . ' U2 ON U2.user_id = I.recipient
                   ORDER BY I.created DESC
                   ' . $sqlLimit
        );
        if (empty($aData)) {
            $aData = array();
        }

        return $aData;
    }

    /**
     * Получаем сообщения переписки
     * @param integer $userID ID пользователя, просматривающего свои сообщения
     * @param integer $interlocutorID ID собеседника
     * @param boolean $countOnly только считаем кол-во
     * @param string $sqlLimit
     * @param array|integer
     */
    public function getConversationMessages($userID, $interlocutorID, $countOnly = false, $sqlLimit = '')
    {
        $bind = array(
            ':userID'         => $userID,
            ':interlocutorID' => $interlocutorID,
        );

        if ($countOnly) {
            return $this->db->one_data('SELECT COUNT(M.id)
                        FROM ' . TABLE_INTERNALMAIL . ' M
                        WHERE ( (M.author=:userID AND M.recipient=:interlocutorID) OR
                                (M.author=:interlocutorID AND M.recipient=:userID) )',
                $bind
            );
        }

        return $this->db->select('SELECT M.*, DATE(M.created) as created_date, (M.author=:userID) as my,
                        (M.recipient=:userID AND M.is_new) as new
                   FROM ' . TABLE_INTERNALMAIL . ' M
                   WHERE ( (M.author=:userID AND M.recipient=:interlocutorID) OR
                           (M.recipient=:userID AND M.author=:interlocutorID) )
                   ORDER BY M.created ' . (bff::adminPanel() ? 'DESC' : 'ASC') . ' ' . $sqlLimit,
            $bind
        );
    }

    /**
     * Получаем список контактов пользователя (frontend)
     * @param integer $userID ID пользователя, просматривающего свои переписки
     * @param integer $folderID ID папки
     * @param string $filterMessages строка поиска (фильтр по сообщениям в переписке)
     * @param boolean $countOnly только считаем кол-во
     * @param string $sqlLimit
     * @param array|integer
     */
    public function getContactsListingFront($userID, $folderID = 0, $filterMessages = '', $countOnly = false, $sqlLimit = '')
    {
        $bind = array(':userID' => $userID);
        if ($folderID > 0) {
            $bind[':folderID'] = $folderID;
        }
        if (mb_strlen($filterMessages) > 2) {
            $filterMessages = ' AND ' . $this->db->prepareFulltextQuery($filterMessages, 'message,attach');
        } else {
            $filterMessages = '';
        }

        if ($countOnly) {
            if (!empty($filterMessages)) {
                return $this->db->one_data('SELECT COUNT(*) FROM (SELECT U.user_id
                           FROM ' . TABLE_INTERNALMAIL . ' M,
                                ' . TABLE_USERS . ' U
                           ' . ($folderID > 0 ? ' INNER JOIN ' . TABLE_INTERNALMAIL_FOLDERS_USERS . ' F
                                    ON F.user_id = :userID
                                   AND F.interlocutor_id = U.user_id
                                   AND F.folder_id = :folderID ' : '') . '
                           WHERE ( M.author = :userID OR M.recipient = :userID )
                              AND U.user_id = (CASE WHEN (M.author = :userID) THEN M.recipient ELSE M.author END)' .
                    $filterMessages.' GROUP BY U.user_id) sl',
                    $bind
                );
            } else {
                return $this->db->one_data('SELECT COUNT(C.interlocutor_id)
                    FROM ' . TABLE_INTERNALMAIL_CONTACTS . ' C
                        ' . ($folderID > 0 ? ' INNER JOIN ' . TABLE_INTERNALMAIL_FOLDERS_USERS . ' F
                            ON F.user_id = C.user_id
                           AND F.interlocutor_id = C.interlocutor_id
                           AND F.folder_id = :folderID ' : '') . '
                    WHERE C.user_id = :userID',
                    $bind
                );
            }
        }

        if (!empty($filterMessages)) {
            $interlocutorsID = $this->db->select_one_column('SELECT U.user_id
                       FROM ' . TABLE_INTERNALMAIL . ' M,
                            ' . TABLE_USERS . ' U
                       ' . ($folderID > 0 ? ' INNER JOIN ' . TABLE_INTERNALMAIL_FOLDERS_USERS . ' F
                                ON F.user_id = :userID
                               AND F.interlocutor_id = U.user_id
                               AND F.folder_id = :folderID ' : '') . '
                       WHERE ( M.author = :userID OR M.recipient = :userID )
                          AND U.user_id = (CASE WHEN (M.author = :userID) THEN M.recipient ELSE M.author END)
                          ' . $filterMessages,
                $bind
            );
            if (empty($interlocutorsID)) {
                return array();
            }
            $aContacts = $this->db->select_key('SELECT I.user_id, I.name, I.surname, I.login, I.pro, I.avatar, I.verified, I.sex, US.last_activity, I.activated,
                           C.messages_total AS msgs_total,
                           C.messages_new AS msgs_new,
                           C.last_message_id AS msgs_last_id,
                           C.last_message_date AS msgs_last_created,
                           N.id AS note_id, N.note
                   FROM ' . TABLE_INTERNALMAIL_CONTACTS . ' C,
                        ' . TABLE_USERS_STAT . ' US,
                        ' . TABLE_USERS . ' I
                            LEFT JOIN '.TABLE_USERS_NOTES.' N ON I.user_id = N.user_id AND N.author_id = :userID
                   WHERE C.user_id = :userID
                     AND C.interlocutor_id IN (' . join(',', array_unique($interlocutorsID)) . ')
                     AND C.interlocutor_id = I.user_id
                     AND C.interlocutor_id = US.user_id
                   ORDER BY C.last_message_date DESC' . $sqlLimit, 'user_id', array(':userID' => $userID)
            );
        } else {
            $aContacts = $this->db->select_key('SELECT I.user_id, I.name, I.surname, I.login, I.pro, I.avatar, I.verified, I.sex, US.last_activity, I.activated,
                           C.messages_total AS msgs_total,
                           C.messages_new AS msgs_new,
                           C.last_message_id AS msgs_last_id,
                           C.last_message_date AS msgs_last_created,
                            N.id AS note_id, N.note
                   FROM ' . TABLE_INTERNALMAIL_CONTACTS . ' C
                        INNER JOIN ' . TABLE_USERS_STAT . ' US ON C.interlocutor_id = US.user_id
                        INNER JOIN ' . TABLE_USERS . ' I ON C.interlocutor_id = I.user_id
                        LEFT JOIN '.TABLE_USERS_NOTES.' N ON I.user_id = N.user_id AND N.author_id = :userID
                        ' . ($folderID > 0 ? ' INNER JOIN ' . TABLE_INTERNALMAIL_FOLDERS_USERS . ' F
                                ON F.user_id = C.user_id
                               AND F.interlocutor_id = I.user_id
                               AND F.folder_id = :folderID ' : '') . '
                   WHERE C.user_id = :userID
                   ORDER BY C.last_message_date DESC' . $sqlLimit, 'user_id', $bind
            );
        }

        if (empty($aContacts)) {
            return array();
        }

        $aLastMessageID = array();
        foreach ($aContacts as &$v) {
            $v['folders'] = array();
            $aLastMessageID[] = $v['msgs_last_id'];
            unset($v['msgs_last_id'], $v['msgs_last_created']);
        }
        unset($v);

        # связь собеседников с папками
        if ($folderID !== -1) {
            $aUsersFolders = $this->getInterlocutorFolders($userID, array_keys($aContacts));
            foreach ($aUsersFolders as $k_id => $v_folders) {
                $aContacts[$k_id]['folders'] = $v_folders;
            }
        }

        # данные о последних сообщениях в контактах
        $aLastMessageID = $this->db->select('SELECT id, author, recipient, order_id, created, readed, is_new, message
                    FROM ' . TABLE_INTERNALMAIL . '
                    WHERE id IN (' . join(',', $aLastMessageID) . ')'
        );
        if (!empty($aLastMessageID)) {
            foreach ($aLastMessageID as $v) {
                $nInterlocatorID = ($v['author'] == $userID ? $v['recipient'] : $v['author']);
                $aContacts[$nInterlocatorID]['message'] = $v;
            }
        }

        return $aContacts;
    }

    /**
     * Получаем контакты пользователя (admin-панель)
     * @param integer $userID ID пользователя, просматривающего свои переписки
     * @param integer $folderID ID папки
     * @param boolean $countOnly только считаем кол-во
     * @param string $sqlLimit
     * @param array
     */
    public function getContactsListingAdm($userID, $folderID, $countOnly = false, $sqlLimit = '')
    {
        $bind = array(':userID' => $userID);
        if ($folderID > 0) {
            $bind[':folderID'] = $folderID;
        }

        if ($countOnly) {
            return $this->db->one_data('SELECT COUNT(C.interlocutor_id)
                FROM ' . TABLE_INTERNALMAIL_CONTACTS . ' C
                ' . ($folderID > 0 ? ' INNER JOIN ' . TABLE_INTERNALMAIL_FOLDERS_USERS . ' F
                        ON F.user_id = C.user_id
                       AND F.interlocutor_id = C.interlocutor_id
                       AND F.folder_id = :folderID ' : '') . '
                WHERE C.user_id = :userID',
                $bind
            );
        }

        $aContacts = $this->db->select_key('SELECT I.user_id, I.name, I.email, I.login, I.admin, I.avatar, I.sex, I.activated,
                   C.messages_total AS msgs_total,
                   C.messages_new AS msgs_new,
                   C.last_message_id AS lastmsg_id,
                   C.last_message_date AS msgs_last_created
           FROM ' . TABLE_INTERNALMAIL_CONTACTS . ' C
                INNER JOIN ' . TABLE_USERS . ' I ON C.interlocutor_id = I.user_id
           ' . ($folderID > 0 ? ' INNER JOIN ' . TABLE_INTERNALMAIL_FOLDERS_USERS . ' F
                        ON F.user_id = C.user_id
                       AND F.interlocutor_id = I.user_id
                       AND F.folder_id = :folderID ' : '') . '
           WHERE C.user_id = :userID
           ORDER BY C.last_message_date DESC ' .
            $sqlLimit, 'user_id', $bind
        );

        if (empty($aContacts)) {
            return array();
        }

        $aLastMessageID = array();
        foreach ($aContacts as &$v) {
            $v['folders'] = array();
            $aLastMessageID[] = $v['lastmsg_id'];
        }
        unset($v);

        # получаем связь собеседников с папками
        if ($folderID !== -1) {
            $aUsersFolders = $this->getInterlocutorFolders($userID, array_keys($aContacts));
            foreach ($aUsersFolders as $k_id => $v_folders) {
                $aContacts[$k_id]['folders'] = $v_folders;
            }
        }

        # получаем данные о последних сообщениях в переписках
        $aLastMessageID = $this->db->select('SELECT id, author, recipient, created, readed, is_new
                    FROM ' . TABLE_INTERNALMAIL . '
                    WHERE id IN (' . join(',', $aLastMessageID) . ')'
        );

        foreach ($aLastMessageID as $v) {
            $aContacts[($v['author'] == $userID ? $v['recipient'] : $v['author'])]['lastmsg'] = $v;
        }

        return $aContacts;
    }

    /**
     * Возвращает кол-во новых сообщений
     * @param integer $userID ID пользователя просматривающего свои сообщения
     * @return integer
     */
    public function getNewMessagesCount($userID)
    {
        return (integer)$this->db->one_data('SELECT COUNT(*) FROM ' . TABLE_INTERNALMAIL . '
                   WHERE recipient = :userID AND is_new = 1',
            array(':userID' => $userID)
        );
    }

    /**
     * Обновляем счетчик новых сообщений пользователя
     * @param integer $userID ID пользователя
     * @return boolean текущее значение счетчика
     */
    public function updateNewMessagesCounter($userID)
    {
        $newMessagesCount = $this->getNewMessagesCount($userID);
        $this->security->userCounter('internalmail_new', $newMessagesCount, $userID, false);

        return $newMessagesCount;
    }

    /**
     * Формирование списка получателей по email адресу
     * @param string $searchQuery первые символы email адреса
     * @param integer $currentUserID ID текущего пользователя
     * @param integer $limit максимальное кол-во совпадений
     * @return array
     */
    public function suggestInterlocutors($searchQuery, $currentUserID, $limit = 10) # adm
    {
        /**
         * получаем список подходящих по email'у собеседников, исключая:
         * - текущего пользователя
         * - запретивших им писать (im_noreply=1)
         * - заблокированных пользователей
         */
        return $this->db->select('SELECT U.user_id as id, U.email
                FROM ' . TABLE_USERS . ' U
                WHERE U.user_id != :userID
                  AND U.email LIKE (:q)
                  AND U.im_noreply = 0
                  AND U.blocked = 0
                ORDER BY U.email' .
            $this->db->prepareLimit(0, ($limit > 0 ? $limit : 10))
            , array(':q' => $searchQuery . '%', ':userID' => $currentUserID)
        );
    }

}