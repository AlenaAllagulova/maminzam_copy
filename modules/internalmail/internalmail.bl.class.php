<?php

abstract class InternalMailBase_ extends Module
{
    /** @var InternalMailModel */
    public $model = null;
    protected $securityKey = '258e6379ce805822c2c73b8c40de0a02';

    # системные папки
    const FOLDERS_ENABLED = true;
    const FOLDER_ALL      = 0; # все
    const FOLDER_FAVORITE = 1; # избранные
    const FOLDER_IGNORE   = 2; # игнорируемые

    public function init()
    {
        parent::init();

        $this->module_title = _t('internalmail','Сообщения');

        bff::autoloadEx(array(
            'InternalMailAttachment' => array('app', 'modules/internalmail/internalmail.attach.php'),
        ));
    }

    /**
     * Shortcut
     * @return InternalMail
     */
    public static function i()
    {
        return bff::module('internalmail');
    }

    /**
     * Shortcut
     * @return InternalMailModel
     */
    public static function model()
    {
        return bff::model('internalmail');
    }

    /**
     * Использовать вложения
     * @return bool
     */
    public static function attachmentsEnabled()
    {
        return config::sysAdmin('internalmail.attachments.enabled', true, TYPE_BOOL);
    }

    public function sendmailTemplates()
    {
        $aTemplates = array(
            'internalmail_new_message' => array(
                'title'       => 'Сообщения: Новое сообщение',
                'description' => 'Уведомление, отправляемое <u>пользователю</u> при получении нового сообщения',
                'vars'        => array(
                    '{fio}' => 'ФИО пользователя',
                    '{name}' => 'Имя пользователя',
                    '{message}' => 'Текст сообщения (до 100 символов)',
                    '{email}' => 'Email',
                    '{link}'  => 'Ссылка для прочтения',
                ),
                'impl'        => true,
                'priority'    => 100,
                'enotify'     => Users::ENOTIFY_INTERNALMAIL,
                'group'       => 'profile',
            ),
        );
        return $aTemplates;
    }
    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # сообщения пользователя
            case 'my.messages':
                $url .= '/user/messages' . static::urlQuery($opts);
                break;
            # сообщения пользователя с фильтрацией по ID заказа
            case 'item.messages':
                $url .= '/user/messages' . (!empty($opts['item']) ? '?qq=item:' . $opts['item'] : '');
                break;
            case 'my.chat':
                $url .= '/user/messages/chat' . static::urlQuery($opts);
                break;
        }
        return bff::filter('internalmail.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Получаем список папок
     * @return array
     */
    public function getFolders($nUserID = 0)
    {
        $folders = array(
            self::FOLDER_FAVORITE => array(
                'title'       => _t('internalmail', 'Избранные'),
                'notforadmin' => false,
                'class'       => 'i-imail-favorites',
                'class-admin' => 'fav',
                'icon-admin'  => 'icon-star',
            ),
            self::FOLDER_IGNORE   => array(
                'title'       => _t('internalmail', 'Игнорирую'),
                'notforadmin' => true,
                'class'       => 'i-imail-ignore',
                'class-admin' => 'ignore',
                'icon-admin'  => 'icon-ban-circle',
            ),
        );

        if( ! $nUserID){
            $nUserID = User::id();
        }
        $aData = $this->model->userFolderList($nUserID);
        if( ! empty($aData)) {
            foreach ($aData as $k => $v) {
                $aData[$k]['notforadmin'] = false;
                $aData[$k]['custom'] = true;
            }
            $folders += $aData;
        }
        return $folders;
    }

    /**
     * Инициализация компонента работы с вложениями
     * @return InternalMailAttachment
     */
    public function attach()
    {
        static $i;
        if (!isset($i)) {
            # до 5 мегабайт
            $i = new InternalMailAttachment(bff::path('im'), 5242880);
        }

        return $i;
    }

    /**
     * Загрузка приложения к сообщению
     * @param string $inputName имя input-file поля
     * @return string имя загруженного файла
     */
    public function attachUpload($inputName = 'attach')
    {
        if (!InternalMail::attachmentsEnabled()) {
            return '';
        }

        return $this->attach()->uploadFILES($inputName);
    }

    /**
     * Формирование списка директорий/файлов требующих проверки на наличие прав записи
     * @return array
     */
    public function writableCheck()
    {
        return array_merge(parent::writableCheck(), array(
            bff::path('im') => 'dir-only', # вложения
        ));
    }

    /**
     * Отправляем информацию для нового пользователя после регистрации
     * @param $nRecipientID
     */
    public function sendRegInfo($nRecipientID)
    {
        if (!$nRecipientID) {
            return;
        }
        $aRecipientData = Users::model()->userData($nRecipientID, array('type'));
        if ($aRecipientData['type'] != Users::TYPE_WORKER) {
            return;
        }
        $aUsersList = Users::model()->usersList(array('admin' => true), ['user_id'], false, '', 'user_id asc');
        $aAdminData = reset($aUsersList);
        $nUserID = $aAdminData['user_id'];

        $sMessage = _t('orders', 'Необходимо <a [href]>заполнить анкету</a> - это позволит вам получать заказы',
                       ['href' => 'href="'.(Users::url('my.settings', ['tab'=>'questionary'])).'"']);
        InternalMail::model()->sendMessage($nUserID, $nRecipientID, $sMessage, '', false);
    }

}