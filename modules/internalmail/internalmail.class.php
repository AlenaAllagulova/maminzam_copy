<?php

class InternalMail_ extends InternalMailBase
{
    /**
     * Список контактов
     */
    public function my_messages()
    {
        $nUserID = User::id();
        if (!$nUserID) {
            if (Request::isAJAX()) {
                $this->errors->reloadPage();
                $this->ajaxResponseForm();
            } else {
                return $this->showForbidden('', _t('users', 'Для доступа в кабинет необходимо авторизоваться'), true);
            }
        }

        bff::setMeta(_t('internamail', 'Сообщения'));
        $sInterlocutorLogin = $this->input->getpost('i', TYPE_NOTAGS);
        if (!empty($sInterlocutorLogin)) {
            return $this->my_chat($sInterlocutorLogin);
        }

        $sAction = $this->input->post('act', TYPE_STR);
        if (Request::isPOST() && !empty($sAction)) {
            $aResponse = array();
            switch ($sAction) {
                case 'move2folder':
                {
                    $nInterlocutorID = $this->input->post('user', TYPE_UINT);
                    $nFolderID = $this->input->post('folder', TYPE_UINT);

                    if (!self::FOLDERS_ENABLED || !$nInterlocutorID || !$nFolderID
                        || !$this->security->validateToken()
                    ) {
                        $this->errors->reloadPage();
                        break;
                    }
                    if (!array_key_exists($nFolderID, $this->getFolders())) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nInterlocutorData = Users::model()->userData($nInterlocutorID, array('user_id', 'admin'));
                    if (empty($nInterlocutorData) || ($nFolderID == self::FOLDER_IGNORE && $nInterlocutorData['admin'])) {
                        $aResponse['added'] = 0;
                    } else {
                        $aResponse['added'] = $this->model->interlocutorToFolder($nUserID, $nInterlocutorID, $nFolderID);
                    }
                } break;
                case 'folder-create':
                {
                    $aData = $this->input->postm(array(
                        'title' => array(TYPE_NOTAGS, 'len'=>25, 'len.sys' => 'internalmail.folder.limit'),
                    ));
                    if ( ! $aData['title']) {
                        $this->errors->set(_t('internalmail', 'Укажите название папки'));
                        break;
                    }
                    if (!$this->security->validateToken()) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $this->model->userFolderSave($nUserID, 0, $aData);
                } break;
                case 'folder-rename':
                {
                    $nFolderID = $this->input->post('id', TYPE_UINT);
                    if ( ! $nFolderID) {
                        $this->errors->impossible();
                        break;
                    }
                    $aData = $this->input->postm(array(
                        'title' => array(TYPE_NOTAGS, 'len'=>25, 'len.sys' => 'internalmail.folder.limit'),
                    ));
                    if ( ! $aData['title']) {
                        $this->errors->set(_t('internalmail', 'Укажите название папки'));
                        break;
                    }
                    if (!$this->security->validateToken()) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $this->model->userFolderSave($nUserID, $nFolderID, $aData);
                } break;
                case 'folder-delete':
                {
                    $nFolderID = $this->input->post('id', TYPE_UINT);
                    if ( ! $nFolderID) {
                        $this->errors->impossible();
                        break;
                    }
                    if (!$this->security->validateToken()) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $this->model->userFolderDelete($nUserID, $nFolderID);
                } break;
            }
            $this->ajaxResponseForm($aResponse);
        }

        $aData = array(
            'list'    => array(),
            'pgn'     => '',
            'folders' => array(
                    self::FOLDER_ALL => array('title' => _t('internalmail', 'Все')),
                ) + (self::FOLDERS_ENABLED ? $this->getFolders() : array()),
        );

        $f = $this->input->postgetm(array(
                'f'    => TYPE_UINT, # папка (self::FOLDER_)
                'qq'   => TYPE_NOTAGS, # строка поиска
                'page' => TYPE_UINT, # страница
            )
        );
        extract($f, EXTR_REFS | EXTR_PREFIX_ALL, 'f');
        if (!array_key_exists($f_f, $aData['folders'])) {
            $f_f = self::FOLDER_ALL;
        }
        if (!self::FOLDERS_ENABLED) {
            $f_f = -1;
        } # выключаем проверку папки

        if (!empty($f_qq)) {
            $f_qq = $this->input->cleanSearchString($f_qq, 50);
        }
        $nPageSize = config::sysAdmin('internalmail.contacts.pagesize', 10, TYPE_UINT);

        $nTotal = $aData['total'] = $this->model->getContactsListingFront($nUserID, $f_f, $f_qq, true);
        if ($nTotal > 0) {
            $url = static::url('my.messages');
            $urlQuery = unserialize(serialize($f));
            $oPgn = new Pagination($nTotal, $nPageSize, array(
                'link'  => $url,
                'query' => $urlQuery,
            ));
            $aData['list'] = $this->model->getContactsListingFront($nUserID, $f_f, $f_qq, false, $oPgn->getLimitOffset());
            if (!empty($aData['list'])) {
                foreach ($aData['list'] as &$v) {
                    $v['avatar'] = UsersAvatar::url($v['user_id'], $v['avatar'], UsersAvatar::szSmall, $v['sex']);
                    if ( ! empty($f_qq)) {
                        foreach (explode(' ', $f_qq) as $vv) {
                            $v['message']['message'] = str_replace($vv, '<em>' . $vv . '</em>', $v['message']['message']);
                        }
                    }
                    $aUserContacts = Users::i()->getUserContacts($v['user_id']);
                    if(is_array($aUserContacts) && !empty($aUserContacts)){
                        $v = array_merge($v, $aUserContacts);
                    }
                }
                unset($v);
            }
            $aData['pgn'] = $oPgn->view();
        }

        # формируем список
        $aData['qq'] = ! empty($f_qq);
        $aData['list'] = $this->viewPHP($aData, 'my.messages.list');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'pgn'   => $aData['pgn'],
                    'list'  => $aData['list'],
                    'total' => $aData['total'],
                )
            );
        }

        if ($f_f === -1) {
            $f_f = self::FOLDER_ALL;
        }
        $aData['f'] = &$f;

        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => _t('internamail', 'Внутренняя переписка'), 'active' => true),
        );

        return $this->viewPHP($aData, 'my.messages');
    }

    /**
     * Переписка
     * @param string $sInterlocutorLogin логин собеседника
     * @return string HTML
     */
    public function my_chat($sInterlocutorLogin = '')
    {
        if( ! $sInterlocutorLogin){
            $sInterlocutorLogin = $this->input->get('user', TYPE_NOTAGS);
        }
        # данные о собеседнике
        $aInterlocutor = Users::model()->userDataByFilter(array('login' => $sInterlocutorLogin),
            array(
                'user_id', 'avatar', 'sex', 'name', 'surname', 'login', 'pro', 'verified', 'last_activity',
                'blocked', 'activated', 'im_noreply'
            )
        );
        if (empty($aInterlocutor) && !Request::isPOST()) {
            return $this->showForbidden(
                _t('view', 'Ошибка'),
                _t('view', 'Указанный собеседник не найден либо был удален')
            );
        }
        $nUserID = User::id();
        if (User::isCurrent($aInterlocutor['user_id'])) {
            $this->redirect(static::url('my.messages'));
        } elseif (!$nUserID) {
            return $this->my_messages();
        }

        if (!Users::isAdminGroup($aInterlocutor['user_id']) && !Users::isOpenedContacts($aInterlocutor['user_id'])){
           return $this->showForbidden(
               '',
               _t('users',
                   'Для отправки сообщения необходимо <a href="[link]">купить контакты</a> пользователя ',
                   [
                       'link' => Users::url('profile', ['login'=> $aInterlocutor['login']])
                   ]
               )
           );

        }
        $aRecipient = Users::getCurrentUserAvatar();
        $aData['recipient'] = $aRecipient;

        $sAction = $this->input->getpost('act', TYPE_STR);
        if (Request::isPOST() && !empty($sAction)) {
            $aResponse = array();
            switch ($sAction) {
                case 'send': # отправка сообщения
                {
                    if (!$this->security->validateToken()) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $sMessage = $this->input->post('message', TYPE_NOTAGS);
                    $sMessage = $this->model->cleanMessage($sMessage);
                    if (mb_strlen($sMessage) < 2) {
                        $this->errors->set(_t('internalmail', 'Текст сообщения слишком короткий'), 'message');
                        break;
                    }

                    if (empty($aInterlocutor)) {
                        $this->errors->reloadPage();
                        break;
                    }

                    $nInterlocutorID = $aInterlocutor['user_id'];
                    if ($nInterlocutorID == $nUserID) {
                        $this->errors->reloadPage();
                        break;
                    }

                    if ($aInterlocutor['blocked']) {
                        $this->errors->set(_t('internalmail', 'Аккаунт пользователя был заблокирован'));
                        break;
                    }

                    if ($aInterlocutor['im_noreply'] || $this->model->isUserInFolder($nUserID, $nInterlocutorID, self::FOLDER_IGNORE)) {
                        $this->errors->set(_t('internalmail', 'Пользователь запретил отправлять ему сообщения'));
                        break;
                    }

                    $this->attach()->setAssignErrors(true);
                    $mAttachment = $this->attachUpload();
                    if (!$this->errors->no()) {
                        break;
                    }

                    $res = $this->model->sendMessage($nUserID, $nInterlocutorID, $sMessage, $mAttachment);
                    if (empty($res)) {
                        $this->errors->reloadPage();
                        break;
                    } else {
                        if (!$aInterlocutor['activated']) {
                            # для неактивированного аккаунта отправляем письмо на email
                            # ... TODO
                        }
                    }
                    $sDate = date('Y-m-d');
                    $aData = array('date_last' => $sDate);
                    $aData['messages'] = array(array(
                        'my' => 1,
                        'message'      => nl2br($sMessage),
                        'created'      => $this->db->now(),
                        'created_date' => $sDate,
                        'attach'       => $mAttachment,
                    ));
                    $aResponse['message'] = $this->viewPHP($aData, 'my.chat.list');

                }
                break;
            }
            $this->iframeResponseForm($aResponse);
        }

        $nInterlocutorID = $aInterlocutor['user_id'];
        $aInterlocutor['avatar'] = UsersAvatar::url($nInterlocutorID, $aInterlocutor['avatar'], UsersAvatar::szSmall, $aInterlocutor['sex']);
        $aInterlocutor['folders'] = $this->model->getInterlocutorFolders($nUserID, $nInterlocutorID);
        $aInterlocutorContacts = Users::i()->getUserContacts($aInterlocutor['user_id']);
        if(is_array($aInterlocutorContacts) && !empty($aInterlocutorContacts)){
            $aInterlocutor = array_merge($aInterlocutor, $aInterlocutorContacts);
        }
        $aData['i'] =& $aInterlocutor;

        # считаем кол-во сообщений в переписке
        $nTotal = $aData['messagesTotal'] = $this->model->getConversationMessages($nUserID, $nInterlocutorID, true);

        if ($nTotal > 0) {
            # получаем сообщения переписки
            $aData['messages'] = $this->model->getConversationMessages($nUserID, $nInterlocutorID, false);
        } else {
            $aData['messages'] = array();
        }

        # формируем список сообщений
        foreach ($aData['messages'] as &$v) {
            $v['created_date'] = strtotime($v['created_date']);
        }
        unset($v);
        $aData['list'] = $this->viewPHP($aData, 'my.chat.list');

        if (Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                    'list' => $aData['list'],
                )
            );
        }

        # отмечаем как прочитанные все новые сообщения, в которых пользователь является получателем
        $nNewViewed = 0;
        if (!empty($aData['messages'])) {
            foreach ($aData['messages'] as $v) {
                if ($v['new']) {
                    $nNewViewed++;
                }
            }
        }
        if ($nNewViewed > 0) {
            $this->model->setMessagesReaded($nUserID, $nInterlocutorID, true);
        }

        $aData['url_profile'] = Users::urlProfile($aInterlocutor['login']);
        $aData['url_back'] = InternalMail::url('my.messages');
        $referer = Request::referer();
        if (!empty($referer) && stripos($referer, $aData['url_back']) !== false) {
            $aData['url_back'] = $referer;
        }

        $aInterlocutor['ignoring'] = ($aInterlocutor['im_noreply'] || (self::FOLDERS_ENABLED &&
                $this->model->isUserInFolder($nUserID, $nInterlocutorID, self::FOLDER_IGNORE)));

        $aData['folders'] = array(
            self::FOLDER_ALL => array('title' => _t('internalmail', 'Все')),
            ) + (self::FOLDERS_ENABLED ? $this->getFolders() : array());

        # SEO:
        $this->seo()->robotsIndex(false);
        bff::setMeta(_t('internamail', 'Сообщения: переписка'));

        return $this->viewPHP($aData, 'my.chat');
    }

}