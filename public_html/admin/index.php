<?php

define('BFF_ADMINPANEL', 1);
require __DIR__.'/../../bff.php';



Site::adminPanel(array(
    'orders'=>_t('menu','Заказы'), 'shop'=>_t('menu','Магазин'),
    'fairplay'=>_t('menu','Безопасная сделка'), 'bills'=>_t('menu','Счет и услуги'),
    'users'=>_t('menu','Пользователи'), 'portfolio'=>_t('menu','Портфолио'),
    'opinions'=>_t('menu','Отзывы'), 'qa'=>_t('menu','Ответы'),
    'blog'=>_t('menu','Блог'), 'articles'=>_t('menu','Статьи'),
    'news'=>_t('menu','Новости'), 'help'=>_t('menu','Помощь'),
    'pages'=>_t('menu','Страницы'), 'internalmail'=>_t('menu','Сообщения'),
    'contacts'=>_t('menu','Контакты'), 'sendmail'=>_t('menu','Работа с почтой'),
    'banners'=>_t('menu','Баннеры'), 'sitemap'=>_t('menu','Карта сайта и меню'),
    'specializations'=>_t('menu','Специализации'), 'seo'=>_t('','SEO'),
    'settings'=>_t('menu','Настройка сайта'),
));