var jCatsSelect = (function(){
    var o = {block:false, cnt:0, limit:0, onSelect:false};
    var $block, data = {}, prev = {}, path = [];

    function init(options)
    {
        o = $.extend(o, options || {});
        $block = $(o.block);

        $block.on('show.bs.dropdown', '.j-cat-select', function(){
            var $el = $(this);
            var d = $el.metadata();
            prev[d.count] = $el.find('.j-menu').html();
            path = d.path;
        });

        $block.on('hide.bs.dropdown', '.j-cat-select', function(){
            var $el = $(this);
            var d = $el.metadata();
            if(prev.hasOwnProperty(d.count)){
                $el.find('.j-menu').html(prev[d.count]);
            }
            path = [];
        });

        $block.on('click', '.j-back', function(e){ nothing(e);
            var $el = $(this);
            var d = $el.metadata();
            menuBlock(d.pid, $el.closest('.j-menu'));
            return false;
        });

        $block.on('click', '.j-sub', function(e){ nothing(e);
            var $el = $(this);
            var d = $el.metadata();
            menuBlock(d.id, $el.closest('.j-menu'));
            return false;
        });


        $block.on('click', '.j-cat', function(e){ nothing(e);
            var $el = $(this);
            var d = $el.metadata();
            var $bl = $el.closest('.j-cat-select');
            var bd = $bl.metadata();
            $bl.find('.j-cat-value').val(d.id);
            bd.path = d.path;
            $bl.metadata(bd);

            var $menu = $el.closest('.j-menu');
            $menu.find('.j-cat, .j-sub').parent().removeClass('active');
            $el.parent().addClass('active');
            if(prev.hasOwnProperty(bd.count)){
                delete prev['bd.count'];
            }

            $bl.removeClass('open');
            $bl.find('.j-title-empty').addClass('hide');
            $bl.find('.j-title-selected > .j-title').html((d.pid_title.length ? d.pid_title + ' / ' : '') + $el.text());
            $bl.find('.j-title-selected').removeClass('hide');

            var $existsCats = $block.find('input.j-cat-value[value="'+d.id+'"]');
            if($existsCats.length > 1){
                var cnt = $existsCats.length;
                $existsCats.each(function(){
                    if(cnt <= 1) return;
                    var $del = $(this).closest('.j-cat-select').find('.j-delete');
                    if($del.length){
                        $del.trigger('click');
                        cnt--;
                    }
                });
            }
            if(o.onSelect){
                o.onSelect(d);
            }
            return false;
        });

        $block.on('click', '.j-delete', function(e){ nothing(e);
            $(this).closest('.j-cat-select').remove();
            $block.find('.j-cat-add').removeClass('hide');
            return false;
        });

        $block.on('click', '.j-cat-add', function(e){ nothing(e);
            var cnt = $block.find('.j-cat-select').length;
            if(cnt > o.limit) return false;
            bff.ajax(bff.ajaxURL('shop','add'), {cnt: o.cnt}, function(resp){
                if(resp && resp.success) {
                    $block.find('.j-cats-block').append(resp.html);
                    if($block.find('.j-cat-select').length == o.limit){
                        $block.find('.j-cat-add').addClass('hide');
                    }
                    if( ! $block.find('.j-cat-select.open').length){
                        $block.find('.j-cat-select:last').addClass('open');
                    }
                }
            });

            o.cnt++;
            return false;
        });

    }


    function menuBlock(cat, $bl)
    {
        if(data.hasOwnProperty(cat)){
            $bl.html(data[cat]);
            setActive($bl);
        }else{
            bff.ajax(bff.ajaxURL('shop','form-menu'), {cat:cat}, function(resp){
                if(resp && resp.success) {
                    data[cat] = resp.menu;
                    $bl.html(data[cat]);
                    setActive($bl);
                }
            });
        }
    }

    function setActive($bl)
    {
        var $cats = $bl.find('.j-sub, .j-cat');
        $cats.parent().removeClass('active');
        $cats.each(function(){
            var $el = $(this);
            var cd = $el.metadata();
            if($.inArray(cd.id, path) >= 0){
                $el.parent().addClass('active');
            }
        });
    }

    return {
        init:init,
        onSelect:function(f){  o.onSelect = f;  }
    }
}());
