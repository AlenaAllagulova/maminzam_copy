var jTags = (function(){
    var inited = false, o = {tags:{}, module:''};
    var $block, $tags;

    function init()
    {
        $block = $('#j-tags');
        $tags = $block.find('.j-selected-tags');

        var $t = $block.find('#j-tag-select');


        $t.autocomplete(bff.ajaxURL(o.module,'tags-autocomplete'),{
            valueInput: $block.find('#j-tag-value'), validate:false, newElement:true,
            onSelect: function(id, title, ex){
                add(id, title);
                $t.val('');
            }
        });

        $tags.on('click', '.j-delete', function(){
            $(this).closest('.j-tag').remove();
            return false;
        });

        for(var i in o.tags)
        {
            if(o.tags.hasOwnProperty(i)){
                if(o.tags[i].hasOwnProperty('id') && o.tags[i].hasOwnProperty('tag')){
                    add(o.tags[i].id, o.tags[i].tag);
                }
            }
        }
    }

    function add(id, title)
    {
        if(title.length){
            if(id == '__##'){
                id += title;
            }
            if( ! $tags.find('.j-value').filter('[value="'+id+'"]').length){
                $tags.append('<span class="j-tag">'+title+' <a href="#" class="j-delete"><i class="fa fa-times"></i></a><input type="hidden" name="tags[]" class="j-value" value="'+id+'"></span> ');
            }
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }

    }
}());
