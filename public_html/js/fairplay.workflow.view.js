var jFairplayWorkflowView = (function(){
    var inited = false, o = {lang:{}};
    var $block;

    function init()
    {
        $block = $('#j-workflow-view');


        chat.init();

        var $chatForm = $block.find('#j-chat-form');
        $block.on('click', '.j-fairplay-chat', function(e){
            e.preventDefault();
            $.scrollTo($chatForm, {duration:500, offset:-50,
                onAfter:function(){ $chatForm.find('[name="message"]').focus(); }});
        });
    }

    var chat = (function(){
        var $list;

        function init()
        {
            $list = $('#j-chat-list');
            var $more = $list.find('.j-more');
            $list.scrollTo($list.find('.j-message:last'), {duration:0, offset:50});
            var $f = $('#j-chat-form');
            var f = app.form($f, false, {noEnterSubmit: true});
            var $attach = f.$field('attach');
            var $message = f.$field('message');
            var $battach = $f.find('.j-attach');
            bff.iframeSubmit($f, function(resp, errors){
                if(resp && resp.success) {
                    if(resp.html){
                        var html = $('<div/>').html(resp.html).text();
                        appendToList(html);
                    }
                    $message.val('');
                    $attach.val('');
                    $battach.removeClass('hidden');
                    $attach.addClass('hidden');
                } else {
                    f.fieldsError(resp.fields, errors);
                }
            },{
                beforeSubmit: function(){
                    f.$field('id').val($list.find('.j-message:last').data('id'));
                    if( ! f.checkRequired({focus:true}) ) return false;
                    if( ! $message.val().length && ! $attach.val().length){
                        f.fieldError('message', o.lang.chat_empty);
                        return false
                    }
                    return true;
                },
                button: '.j-submit',
                url:bff.ajaxURL('fairplay', 'chat-add')
            });

            $battach.on('click', function(e){
                e.preventDefault();
                $battach.addClass('hidden');
                $attach.removeClass('hidden');
                $attach.trigger('click');
            });

            $more.click(function(e){
                e.preventDefault();
                var $fst = $list.find('.j-message:first');
                bff.ajax(bff.ajaxURL('fairplay', 'chat-more'),{workflow:o.id, id:$fst.data('id'), hash: app.csrf_token},function(resp, errors) {
                    if (resp && resp.success) {
                        $list.after('<div id="j-wrapper">' + resp.html + '</div>');
                        var $wrap = $('#j-wrapper');
                        var $dates = $list.find('.j-date');
                        $wrap.find('.j-date').each(function(){
                            var $d = $dates.filter('[data-d="'+$(this).data('d')+'"]');
                            if($d.length){
                                $d.remove();
                            }
                        });
                        $more.parent().after($wrap);
                        $wrap.find(':first').unwrap();
                        $list.scrollTo($fst, {duration:0, offset:-10});
                        if( ! intval(resp.more)){
                            $more.parent().remove();
                        }
                        $('.show-tooltip').tooltip();
                    } else {
                        app.alert.error(errors);
                    }
                });

            });

            setInterval(function(){
                if( ! document.hasFocus()){
                    return;
                }
                refresh();
            }, 20000);
        }

        function refresh()
        {
            var $last = $list.find('.j-message:last');
            bff.ajax(bff.ajaxURL('fairplay', 'chat-refresh'),{workflow:o.id, id:$last.data('id'), hash: app.csrf_token},function(resp, errors) {
                if (resp && resp.success) {
                    if( ! resp.html) return;
                    appendToList(resp.html);
                } else {
                    app.alert.error(errors);
                }
            });
        }

        function appendToList(html)
        {
            $list.after('<div id="j-wrapper">' + html + '</div>');
            var $wrap = $('#j-wrapper');
            var $dates = $wrap.find('.j-date');
            $list.find('.j-date').each(function(){
                var $d = $dates.filter('[data-d="'+$(this).data('d')+'"]');
                if($d.length){
                    $d.remove();
                }
            });
            $list.append($wrap);
            $wrap.find(':first').unwrap();
            var $last = $list.find('.j-message:last');
            $list.scrollTo($last, {duration:500, offset:-10});
        }

        return{
            init:init,
            refresh:refresh
        }
    }());

    function chatRefresh()
    {
        chat.refresh();
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        },
        chatRefresh:chatRefresh
    };
}());