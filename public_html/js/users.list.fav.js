var jUsersListFav = (function(){
    var inited = false, o = {lang:{}};
    var $block;

    function init()
    {
        $blocks = $('.j-usr-block-fav');
        $blocks.each(function () {
            var $bl = $(this);

            $bl.on('click', '.j-add-fav', function(){
                var $el = $(this);
                bff.ajax(bff.ajaxURL('users', 'fav-add'), {id:$el.data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $el.addClass('hidden');
                        $bl.find('.j-del-fav').removeClass('hidden');
                        if(resp.hasOwnProperty('fav_cnt')){
                            $('#j-fav-cnt').html(resp.fav_cnt);
                        }
                    } else {
                        app.alert.error(errors);
                    }
                });
                return false;
            });

            $bl.on('click', '.j-del-fav', function(){
                var $el = $(this);
                bff.ajax(bff.ajaxURL('users', 'fav-del'), {id:$el.data('id'), hash: app.csrf_token}, function(resp, errors){
                    if(resp && resp.success) {
                        $el.addClass('hidden');
                        $bl.find('.j-add-fav').removeClass('hidden');
                        if(resp.hasOwnProperty('fav_cnt')){
                            $('#j-fav-cnt').html(resp.fav_cnt);
                        }
                    } else {
                        app.alert.error(errors);
                    }
                });
                return false;
            });

        });
    }

    return {
        init: function(options)
        {
            if(inited) return;
            // inited = true; блокирует инициализацию при пагинации
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());