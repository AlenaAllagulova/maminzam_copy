'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Custom = function () {
    function Custom() {
        _classCallCheck(this, Custom);
    }

    _createClass(Custom, null, [{
        key: 'open_contacts',
        value: function open_contacts(id, is_profile) {
            bff.ajax(bff.ajaxURL('users&ev=openContacts'), {
                user_id: id,
                is_profile: is_profile
            }, function (data, errors) {
                if (data && data.success) {
                    if ($('div[data-btn-opend-user-id="' + id + '"]').length) {
                        $('div[data-btn-opend-user-id="' + id + '"]').hide();
                    } else {
                        $('.j-show-btn-block').hide();
                    }
                    if (data.hasOwnProperty('contacts_block')) {
                        if ($('div[data-opend-user-id="' + id + '"]').length) {
                            $('div[data-opend-user-id="' + id + '"]').append(data.contacts_block);
                        } else {
                            $('.j-contacts-block').append(data.contacts_block);
                        }
                    }
                } else {
                    app.alert.error(errors);
                }
            });
        }
    }, {
        key: 'buy_contacts',
        value: function buy_contacts() {

            var cnt = $('[name="contact_cnt"]').val();

            if (!Boolean(+cnt)) {
                return false;
            }

            bff.ajax(bff.ajaxURL('users&ev=buyUserContacts', ''), {
                cnt: cnt
            }, function (data, errors) {
                if (data && data.success) {
                    app.alert.success(data.success_msg);
                    window.setTimeout(function () {
                        if (data.redirect) {
                            bff.redirect(data.redirect);
                        } else {
                            history.back();
                        }
                    }, 2000);
                } else {
                    app.alert.error(errors);
                }
            });
        }
    }, {
        key: 'total_sum',
        value: function total_sum(amount) {
            var cnt = $('[name="contact_cnt"]').val();
            $('#total_sum').html(amount * cnt);
        }
    }, {
        key: 'simple_offer_add',
        value: function simple_offer_add(id) {
            bff.ajax(bff.ajaxURL('orders&ev=simple_offer_add&hash=' + app.csrf_token, 'add'), {
                order_id: id
            }, function (data, errors) {
                if (data && data.success) {
                    $('#j-offer-btn-block-' + id).hide();
                    $('#j-offer-add-success-' + id).removeClass('hidden');
                } else {
                    app.alert.error(errors);
                }
            });
        }
    }]);

    return Custom;
}();
//# sourceMappingURL=custom.js.map
