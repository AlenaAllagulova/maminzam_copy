'use strict';

$(function () {

    $('.j-print-window').click(function () {
        window.print();
    });

    $('.j-minus-count').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.j-plus-count').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });

    //collapse menu index
    $(".j-open-menu").click(function () {
        $(".j-menu-list").collapse('show');
        $('body').addClass('stop-scrolling');
    });
    $(".j-close-menu").click(function () {
        $(".j-menu-list").collapse('hide');
        $('body').removeClass('stop-scrolling');
    });

    $(".j-open-reg-form").click(function () {
        $(".j-reg-form").collapse('show');
        $(this).remove();
    });

    //SELECT2
    $(".j-select").select2({
        minimumResultsForSearch: Infinity
    });

    $(".j-select-search").select2({
        // minimumResultsForSearch: Infinity
    });

    //slick slider home-page
    $('.j-slider-general').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        prevArrow: '<i class="slider-general__arrow slider-general__arrow_pre icon-previous"></i>',
        nextArrow: '<i class="slider-general__arrow slider-general__arrow_next icon-previous"></i>',
        dotsClass: 'slider-general__dots',
        responsive: [{
            breakpoint: 769,
            settings: {
                arrows: false
            }
        }]
    });

    //loader js
    $(window).load(function () {
        $('.j-loader-script').addClass('active');
        function func() {
            $('.j-loader-script').remove();
        }
        setTimeout(func, 500);
    });
});
//# sourceMappingURL=scripts.js.map
