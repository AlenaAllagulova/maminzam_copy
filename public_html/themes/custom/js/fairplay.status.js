var jFairplayStauses = (function(){

    var inited = false, o = {lang:{}};
    var $block;

    function init()
    {
        $block = $('#j-fairplay-statuses-block');

        $block.on('click', '.j-fairplay-close', function(e){
            e.preventDefault();
            var $el = $(this);
            var id = $el.data('id');
            var $m = $('#j-modal-status-close-' + id);
            if($m.length){
                $m.modal('show');
            }else{
                bff.ajax(bff.ajaxURL('fairplay', 'status-close-modal'), {id: id,hash: app.csrf_token}, function(data, errors){
                    if (data && data.success) {
                        app.$B.append(data.html);
                        initModalClose(id);
                    }else{
                        app.alert.error(errors);
                    }
                }, function(yes){progress(yes, $el);});
            }
        });

        $block.on('click', '.j-fairplay-opinion-add', function(e){
            e.preventDefault();
            var $el = $(this);
            var id = $el.data('id');
            var $m = $('#j-modal-fairplay-opinion-add-' + id);
            if($m.length){
                $m.modal('show');
            }else{
                bff.ajax(bff.ajaxURL('fairplay', 'opinion-add-modal'), {id: id,hash: app.csrf_token}, function(data, errors){
                    if (data && data.success) {
                        app.$B.append(data.html);
                        initModalOpinionAdd(id);
                    }else{
                        app.alert.error(errors);
                    }
                }, function(yes){progress(yes, $el);});
            }
        });

        $block.on('click', '.j-opinion-view', function(e){
            e.preventDefault();
            var $el = $(this);
            var id = $el.data('id');
            var $m = $('#j-modal-opinion-view-' + id);
            if($m.length){
                $m.modal('show');
            }else{
                bff.ajax(bff.ajaxURL('fairplay', 'opinion-view'), {id: id, workflow:$el.data('wf'), hash: app.csrf_token}, function(data, errors){
                    if (data && data.success) {
                        app.$B.append(data.html);
                        $('#j-modal-opinion-view-' + id);
                        $el.trigger('click');
                        return false;
                    }else{
                        app.alert.error(errors);
                    }
                }, function(yes){progress(yes, $el);});
            }
        });

        $block.on('click', '.j-fairplay-reserved', function(e){
            e.preventDefault();
            var $el = $(this);
            var id = $el.data('id');
            var $m = $('#j-modal-fairplay-reserved-' + id);
            if($m.length){
                $m.modal('show');
            }else{
                bff.ajax(bff.ajaxURL('fairplay', 'reserved-modal'), {id: id,hash: app.csrf_token}, function(data, errors){
                    if (data && data.success) {
                        app.$B.append(data.html);
                        initModalReserved(id);
                    }else{
                        app.alert.error(errors);
                    }
                }, function(yes){progress(yes, $el);});
            }
        });

        $block.on('click', '.j-fairplay-cancel', function(e){
            e.preventDefault();
            var $el = $(this);
            var id = $el.data('id');
            if( ! confirm(o.lang.cancel_confirm)) return;
            bff.ajax(bff.ajaxURL('fairplay', 'status-cancel'), {id: id,hash: app.csrf_token}, function(data, errors){
                if (data && data.success) {
                    if(data.msg){
                        app.alert.success(data.msg);
                    }
                    updateHTML(id, data.html);
                }else{
                    app.alert.error(errors);
                }
            }, function(yes){progress(yes, $el);});
        });

        $block.on('click', '.j-arbitrage', function(e){
            e.preventDefault();
            var $el = $(this);
            var id = $el.data('id');
            var $m = $('#j-modal-arbitrage-' + id);
            if($m.length){
                $m.modal('show');
            }else{
                bff.ajax(bff.ajaxURL('fairplay', 'arbitrage-modal'), {id: id,hash: app.csrf_token}, function(data, errors){
                    if (data && data.success) {
                        app.$B.append(data.html);
                        initModalArbitrage(id);
                    }else{
                        app.alert.error(errors);
                    }
                }, function(yes){progress(yes, $el);});
            }
        });

        $block.on('click', '.j-fairplay-complete', function(e){
            e.preventDefault();
            var $el = $(this);
            var id = $el.data('id');
            var $m = $('#j-modal-complete-' + id);
            if($m.length){
                $m.modal('show');
            }else{
                bff.ajax(bff.ajaxURL('fairplay', 'complete-modal'), {id: id,hash: app.csrf_token}, function(data, errors){
                    if (data && data.success) {
                        app.$B.append(data.html);
                        initModalComplete(id);
                    }else{
                        app.alert.error(errors);
                    }
                }, function(yes){progress(yes, $el);});
            }
        });

        $block.on('click', '.j-fairplay-continue', function(e){
            e.preventDefault();
            var $el = $(this);
            var id = $el.data('id');
            var $m = $('#j-modal-continue-' + id);
            if($m.length){
                $m.modal('show');
            }else{
                bff.ajax(bff.ajaxURL('fairplay', 'continue-modal'), {id: id,hash: app.csrf_token}, function(data, errors){
                    if (data && data.success) {
                        app.$B.append(data.html);
                        initModalContinue(id);
                    }else{
                        app.alert.error(errors);
                    }
                }, function(yes){progress(yes, $el);});
            }
        });

        $block.on('click', '.j-fairplay-change', function(e){
            e.preventDefault();
            var $el = $(this);
            var id = $el.data('id');
            var $m = $('#j-modal-change-' + id);
            if($m.length){
                $m.modal('show');
            }else{
                bff.ajax(bff.ajaxURL('fairplay', 'change-modal'), {id: id,hash: app.csrf_token}, function(data, errors){
                    if (data && data.success) {
                        app.$B.append(data.html);
                        initModalChange(id);
                    }else{
                        app.alert.error(errors);
                    }
                }, function(yes){progress(yes, $el);});
            }
        });

    }

    function progress(yes, $el)
    {
        if(yes){
            if( ! $el.is('[data-loading-text]')) $el.attr('data-loading-text', app.lang.form_btn_loading);
            $el.button('loading');
        }else{
            $el.button('reset');
        }
    }

    function initModalClose(id)
    {
        var $bl = $('#j-modal-status-close-'+id);
        var form = app.form($bl.find('form'), function($f){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            if( ! $f.find('[name="type"]:checked').length){
                app.alert.error(o.lang.check_type);
                return;
            }
            f.ajax(bff.ajaxURL('fairplay', 'status-close-opinion-add'),{},function(resp, errors){
                if(resp && resp.success) {
                    $bl.modal('hide');
                    if(resp.msg){
                        app.alert.success(resp.msg);
                    }
                    updateHTML(id, resp.html);
                } else {
                    f.alertError(errors);
                }
            });
        });
        bff.maxlength(form.$field('message'), {limit:500, message:$bl.find('.j-help-block'), lang:{left: o.lang.left, symbols:o.lang.symbols}});
        $bl.modal('show');
    }

    function initModalArbitrage(id)
    {
        var $bl = $('#j-modal-arbitrage-'+id);
        var form = app.form($bl.find('form'), function($f){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            f.ajax(bff.ajaxURL('fairplay', 'arbitrage'),{},function(resp, errors){
                if(resp && resp.success) {
                    $bl.modal('hide');
                    if(resp.msg){
                        app.alert.success(resp.msg);
                    }
                    updateHTML(id, resp.html);
                } else {
                    f.alertError(errors);
                }
            });
        });
        bff.maxlength(form.$field('message'), {limit:500, message:$bl.find('.j-help-block'), lang:{left: o.lang.left, symbols:o.lang.symbols}});
        $bl.modal('show');
    }

    function initModalOpinionAdd(id)
    {
        var $bl = $('#j-modal-fairplay-opinion-add-'+id);
        var form = app.form($bl.find('form'), function($f){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            if( ! $f.find('[name="type"]:checked').length){
                app.alert.error(o.lang.check_type);
                return;
            }
            f.ajax(bff.ajaxURL('fairplay', 'opinion-add'),{},function(resp, errors){
                if(resp && resp.success) {
                    if(resp.msg){
                        app.alert.success(resp.msg);
                    }
                    $bl.modal('hide');
                    updateHTML(id, resp.html);
                } else {
                    f.alertError(errors);
                }
            });
        });
        bff.maxlength(form.$field('message'), {limit:500, message:$bl.find('.j-help-block'), lang:{left: o.lang.left, symbols:o.lang.symbols}});
        $bl.modal('show');

    }

    function initModalReserved(id)
    {
        var $bl = $('#j-modal-fairplay-reserved-'+id);
        $bl.on('click', '.j-ps', function(e){
            e.preventDefault();
            var $el = $(this);
            bff.ajax(bff.ajaxURL('fairplay', 'reserved'), {id: id, ps:$el.data('ps'), hash: app.csrf_token}, function(data, errors){
                if (data && data.success) {
                    if(data.form){
                        app.$B.append('<div id="j-pay-form-request" class="hidden"></div>');
                        var $form = $('#j-pay-form-request');
                        $form.html(data.form).find('form:first').submit();
                    }
                    if(data.msg){
                        app.alert.success(data.msg);
                    }
                    $bl.modal('hide');
                    updateHTML(id, data.html);
                }else{
                    app.alert.error(errors);
                }
            });
        });
        $bl.modal('show');
    }

    function initModalComplete(id)
    {
        var $bl = $('#j-modal-complete-'+id);
        var form = app.form($bl.find('form'), function($f){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            f.ajax(bff.ajaxURL('fairplay', 'complete'),{},function(resp, errors){
                if(resp && resp.success) {
                    if(resp.msg){
                        app.alert.success(resp.msg);
                    }
                    $bl.modal('hide');
                    updateHTML(id, resp.html);
                    if(typeof(jFairplayWorkflowView) == 'object'){
                        jFairplayWorkflowView.chatRefresh();
                    }
                } else {
                    f.alertError(errors);
                }
            });
        });
        bff.maxlength(form.$field('message'), {limit:500, message:$bl.find('.j-help-block'), lang:{left: o.lang.left, symbols:o.lang.symbols}});
        $bl.modal('show');
    }

    function initModalContinue(id)
    {
        var $bl = $('#j-modal-continue-'+id);
        var form = app.form($bl.find('form'), function($f){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            f.ajax(bff.ajaxURL('fairplay', 'continue'),{},function(resp, errors){
                if(resp && resp.success) {
                    if(resp.msg){
                        app.alert.success(resp.msg);
                    }
                    $bl.modal('hide');
                    updateHTML(id, resp.html);
                    if(typeof(jFairplayWorkflowView) == 'object'){
                        jFairplayWorkflowView.chatRefresh();
                    }
                } else {
                    f.alertError(errors);
                }
            });
        });
        bff.maxlength(form.$field('message'), {limit:500, message:$bl.find('.j-help-block'), lang:{left: o.lang.left, symbols:o.lang.symbols}});
        $bl.modal('show');
    }

    function initModalChange(id)
    {
        var $bl = $('#j-modal-change-'+id);
        var form = app.form($bl.find('form'), function($f){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            f.ajax(bff.ajaxURL('fairplay', 'change'),{},function(resp, errors){
                if(resp && resp.success) {
                    $bl.modal('hide');
                    if(typeof(jFairplayWorkflowsList) == 'object'){
                        jFairplayWorkflowsList.refresh();
                    }
                    if(typeof(jFairplayWorkflowView) == 'object'){
                        location.reload();
                    }
                    app.alert.success(o.lang.success);
                } else {
                    f.alertError(errors);
                }
            });
        });
        $bl.modal('show');
    }

    function updateHTML(id, html)
    {
        if( ! id || ! html) return;
        var $st = $block.find('.j-status[data-id="'+id+'"]');
        if($st.length){
            $st.after(html);
            $st.remove();
        }
        if(typeof(jFairplayWorkflowView) == 'object'){
            setTimeout(function(){location.reload();}, 400);
        }
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());