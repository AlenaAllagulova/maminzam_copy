var jOrdersOrderForm = (function(){
    var inited = false, o = {lang:{}, itemID:0, imgData:{}};
    var geo = {$block:0,$regionBlocks:0,$cityBlocks:0, country:0,cityAC:0,cityID:0,cityPreSuggest:{},
               addr:{$block:0,map:0,mapEditor:0,lastQuery:'', inited:0}};
    var img = {url:'',$block:0,type:{},$togglers:0,uploader:0,active:{}, classes:{progress:'j-progress'}};
    var attach = {url:'',$block:0, uploader:0};
    var dp = {$block:0, cache:{}, child:{}};
    var price = {cache:{}, $block:0, spec_id:0};

    var f, $f;

    function init()
    {
        o.typeID = intval(o.typeID);
        //submit
        $f = $('#j-order-form');
        f = app.form($f, false, {noEnterSubmit: true});
        bff.iframeSubmit($f, function(resp, errors){
            if(resp && resp.success) {
                if(resp.redirect){
                    bff.redirect( $('<div/>').html(resp.redirect).text() );
                }else{
                    f.alertSuccess(o.lang.saved_success);
                }
            } else {
                f.fieldsError(resp.fields, errors);
            }
        },{
            beforeSubmit: function(){
                if( ! f.checkRequired({focus:true}) ) return false;
                var $bl = $f.find('.j-specs:visible');
                if($bl.length){
                    if( ! intval($bl.find('.j-spec-value').val())){
                        app.alert.error(o.lang.spec_wrong);
                        return false;
                    }
                }
                $bl = $f.find('.j-cats:visible');
                if($bl.length){
                    if( ! intval($bl.find('.j-cat-value').val())){
                        app.alert.error(o.lang.cat_wrong);
                        return false;
                    }
                }
                $bl = $f.find('.j-agreement');
                if($bl.length){
                    if( ! $bl.is(':checked')){
                        app.alert.error(o.lang.agreement);
                        return false;
                    }
                }
                if(intval(o.fairplay)){
                    var $fp = $f.find('[name="fairplay"]:checked');
                    if( ! $fp.length){
                        app.alert.error(o.lang.fairplay);
                        return false;
                    }
                }

                return true;
            },
            button: '.j-submit'
        });
        $f.on('click', '.j-submit', function(){
            $f.submit();
            return false;
        });

        // cancel
        $f.on('click', '.j-cancel', function(){
            history.back();
            return false;
        });

        var $typeBlocks = $f.find('.j-type');
        f.$field('type').change(function(){
            var t = $(this).val();
            $typeBlocks.addClass('hidden');
            $typeBlocks.filter('.j-type-' + t).removeClass('hidden');
            setPrice(t);
            setDp();
        });

        var $name = $f.find('.j-name');
        $f.on('change', '[name="role_id"]', function(){
            var $el = $(this);
            var k = 'r'+$el.val();
            $name.text(o.lang.name[k].n);
            f.$field('name').attr('placeholder', o.lang.name[k].p);
        });

        if(typeof(jSpecsSelect) == 'object'){
            jSpecsSelect.init({block:$f.find('.j-specs'), cnt:1, limit:1});
            jSpecsSelect.onSelect(function(){
                setPrice();
                setDp();
            });
        }
        if(typeof(jCatsSelect) == 'object'){
            jCatsSelect.init({block:$f.find('.j-cats'), cnt:1, limit:1});
            jCatsSelect.onSelect(function(){
                setDp();
            });
        }

        $f.on('change', '[name="svc[]"]', function(){
            var $el = $(this);
            var id = intval($el.val());
            if($el.is(':checked')){
                $f.find('#j-svc-'+id).addClass('in');
            }else{
                $f.find('#j-svc-'+id).removeClass('in');
            }
        });

        var $phone = $f.find('.j-phone-number');
        if($phone.length){
            app.user.phoneInput($phone);
        }
        
        // geo
        geo.fn = (function(){
            geo.$block = $f;
            geo.$regionBlocks = geo.$block.find('.j-region');
            geo.$cityBlocks = geo.$block.find('.j-city');
            geo.country = o.defCountry;

            var $country = geo.$block.find('#j-order-country');
            if($country.length){
                $country.change(function(){
                    //country
                    geo.country = intval($country.val());
                    //o.reg1_country = geo.country;
                    o.geoCountry = $country.find(':selected').text();
                    if(geo.country){
                        geo.cityAC.setParam('country', geo.country);
                        geo.$cityBlocks.addClass('hidden');
                        if( geo.cityPreSuggest.hasOwnProperty(geo.country) ) {
                            geo.cityAC.setSuggest(geo.cityPreSuggest[geo.country], true);
                        }else{
                            bff.ajax(bff.ajaxURL('geo', 'country-presuggest'), {country:geo.country}, function(data){
                                geo.cityAC.setSuggest[geo.country] = data;
                                geo.cityAC.setSuggest(data, true);
                            });
                        }
                        geoMapSearch();
                        geo.$regionBlocks.removeClass('displaynone');
                    }else{
                        geo.$regionBlocks.addClass('displaynone');
                    }
                    svcPrice(0);
                    return true;
                });
                geo.country = intval($country.val());
            }

            //city
            geo.$block.find('#j-order-city-select').autocomplete( bff.ajaxURL('geo', 'region-suggest'),{
                valueInput: geo.$block.find('#j-order-city-value'), params:{reg:0, country: geo.country},
                suggest: o.regionPreSuggest ? o.regionPreSuggest : app.regionPreSuggest,
                onSelect: function(cityID, cityTitle, ex){
                    geo.fn.onCity(cityID, ex);
                },
                doPrepareText: function(html){
                    var regionTitlePos = html.toLowerCase().indexOf('<br');
                    if( regionTitlePos != -1 ) {
                        html = html.substr(0, regionTitlePos);
                    }
                    html = html.replace(/<\/?[^>]+>/gi, ''); // striptags
                    return $.trim(html);
                }
            }, function(){ geo.cityAC = this; });
            if( ! geo.country){
                geo.$regionBlocks.addClass('displaynone');
                geo.$cityBlocks.addClass('hidden');
            }
            //addr
            geo.addr.$addr = geo.$block.find('#j-order-addr-addr');
            geo.addr.$lat = geo.$block.find('#j-order-addr-lat');
            geo.addr.$lon = geo.$block.find('#j-order-addr-lng');
            if(intval(o.geoCity) > 0){
                geoMapInit();
            }

            geo.svcCache = {};
            function svcPrice(city)
            {
                function svcPriceShow(data)
                {
                    for(var i in data){
                        if( ! data.hasOwnProperty(i)) continue;
                        $f.find('.j-svc-price-'+i).text(data[i]);
                    }
                }

                if(geo.svcCache.hasOwnProperty(city)){
                    svcPriceShow(geo.svcCache[city]);
                }else{
                    bff.ajax(bff.ajaxURL('orders', 'order-form-svc-prices'), {city:city}, function(data){
                        if(data && data.prices){
                            geo.svcCache[city] = data.prices;
                            svcPriceShow(geo.svcCache[city]);
                        }
                    });

                }
            }

            return {
                onCity: function(cityID, ex)
                {
                    if( ! ex.changed ) return;
                    geo.cityID = cityID;
                    app.inputError(geo.$block.find('#j-order-city-value'), false);
                    // map
                    if(ex.title.length > 0) {
                        if(cityID){
                            geoMapInit();
                        }
                        geoMapSearch();
                    }
                    if(cityID){
                        geo.$cityBlocks.removeClass('hidden');
                    }else{
                        geo.$cityBlocks.addClass('hidden');
                    }
                    svcPrice(cityID);
                }
            };
        }());

        // images
        img.url = bff.ajaxURL('orders&ev=img&hash='+app.csrf_token+'&item_id='+o.itemID, '');
        img.$block = $f.find('.j-images');
        img.type = {
            $ajax: img.$block.find('.j-images-type-ajax'),
            $simple: img.$block.find('.j-images-type-simple')
        };
        img.$togglers = img.$block.find('.j-images-type');
        img.$togglers.on('click', '.j-images-toggler', function(e){ nothing(e);
            var type = $(this).data('type');
            img.$togglers.addClass('hide');
            img.$togglers.filter('.j-images-type-' + type).removeClass('hide');
            img.$block.find('.j-images-type-value').val(type);
        });

        img.uploader = new qq.FileUploaderBasic({
            button: null,
            action: img.url+'upload',
            limit: intval(o.imgLimit), sizeLimit: o.imgMaxSize,
            uploaded: intval(o.imgUploaded),
            multiple: true, allowedExtensions: ['jpeg','jpg','png','gif'],
            onSubmit: function(id, fileName) {
                return imgProgress(id, 'start', false);
            },
            onComplete: function(id, fileName, resp) {
                if(resp && resp.success) {
                    imgProgress(id, 'preview', resp);
                    imgRotate(true);
                } else {
                    if(resp.errors) {
                        app.alert.error(resp.errors);
                        imgProgress(id, 'remove');
                    }
                }
                return true;
            },
            onCancel: function(id, fileName) {
                imgProgress(id, 'remove');
            },
            showMessage: function(message, code) {
                app.alert.error(message);
            },
            messages: {
                typeError: o.lang.upload_typeError,
                sizeError: o.lang.upload_sizeError,
                minSizeError: o.lang.upload_minSizeError,
                emptyError: o.lang.upload_emptyError,
                limitError: o.lang.upload_limitError,
                onLeave: o.lang.upload_onLeave
            }
        });
        img.type.$ajax.find('.j-img-upload .j-img-link').each(function(){
            img.uploader._createUploadButton(this);
        });

        for(var i in o.imgData) {
            if( o.imgData.hasOwnProperty(i) ) {
                imgProgress('img'+i, 'start', true);
                imgProgress('img'+i, 'preview', o.imgData[i]);
            }
        }
        imgRotate();

        img.type.$ajax.on('click', '.j-img-delete', function(e){ nothing(e);
            imgRemove( $(this).data('id') );
        });


        //attachments
        attach.url = bff.ajaxURL('orders&ev=attach&hash='+app.csrf_token+'&item_id='+o.itemID, '');
        attach.$block = $f.find('.j-attachments');
        attach.$list = attach.$block.find('.j-list');
        attach.$progress = attach.$block.find('.j-progress');

            attach.uploader = new qq.FileUploaderBasic({
            button: attach.$block.find('.j-upload').get(0),
            action: attach.url+'upload',
            limit: intval(o.attachLimit), sizeLimit: o.attachMaxSize,
            uploaded: intval(o.attachUploaded),
            multiple: true,
            onSubmit: function(id, fileName) {
                attach.$progress.removeClass('hidden');
            },
            onComplete: function(id, fileName, resp) {
                attach.$progress.addClass('hidden');
                if(resp && resp.success) {
                    attachAdd(resp);
                } else {
                    if(resp.errors) {
                        app.alert.error(resp.errors);
                    }
                }
                return true;
            },
            showMessage: function(message, code) {
                app.alert.error(message);
            },
            messages: {
                typeError: o.lang.upload_typeError,
                sizeError: o.lang.upload_sizeError,
                minSizeError: o.lang.upload_minSizeError,
                emptyError: o.lang.upload_emptyError,
                limitError: o.lang.upload_limitError,
                onLeave: o.lang.upload_onLeave
            }
        });

        for (i in o.attachData) {
            if ( o.attachData.hasOwnProperty(i) ) {
                attachAdd(o.attachData[i]);
            }
        }

        attach.$list.on('click', '.j-delete', function(){
            var $el = $(this);
            bff.ajax(attach.url+'delete',
                {file_id:$el.data('id'), filename:$el.data('filename')}, function(resp){
                    if(resp && resp.success) {
                        $el.closest('.j-attach').remove();
                        attach.uploader.decrementUploaded();
                        attach.$block.find('.j-upload').show();
                    } else {
                        app.alert.error(resp.errors);
                    }
                });
            return false;
        });

        price.$block = $f.find('#j-order-price-block');
        price.hide = function(){
            price.$block.addClass('hidden');
        };
        price.set = function(specid){
            if(price.spec_id == specid){
                price.$block.removeClass('hidden');
                return;
            }
            if(price.cache.hasOwnProperty(specid)){
                price.show(price.cache[specid]);
                price.spec_id = specid;
            }else{
                bff.ajax(bff.ajaxURL('orders','price-form-sett'), {spec_id:specid}, function(resp){
                    if(resp && resp.success) {
                        price.show(price.cache[specid] = resp.html);
                        price.spec_id = specid;
                        jSelect();
                    }
                });
            }
        };
        price.show = function(data){
            price.$block.html(data);
            price.$block.removeClass('hidden');
        };

        price.$block.on('focus', '[name="price"]', function(){
            price.$block.find('.j-price-ex:first').prop('checked', true);
        });
        price.$block.on('focus', '[name="price_curr"]', function(){
            price.$block.find('.j-price-ex:first').prop('checked', true);
        });
        price.$block.on('focus', '[name="price_rate"]', function(){
            price.$block.find('.j-price-ex:first').prop('checked', true);
        });

        if( ! intval(o.itemID) && ! intval(o.invite)) setPrice();

        dp.$block = $f.find('#j-order-form-dp');

        dp.$block.on('change', '.j-dp-child', function(){
            var $el = $(this);
            var t = o.typeID;
            if( ! t){
                t = intval(f.$field('type').filter(':checked').val());
            }
            var id = $el.data('id');
            var prefix = $el.data('name');
            var val = $el.val();
            var key = id+'-'+val;
            if( ! intval(val)) dp.child[key] = {form:''};
            if( dp.child.hasOwnProperty(key) ) {
                var data = dp.child[key];
                $('.j-dp-child-'+id, dp.$block).html( ( data.form.length ? data.form : '') ).toggleClass('hidden', ! data.form.length);
            } else {
                bff.ajax(bff.ajaxURL('orders','dp-child'), {dp_id:id, dp_value:val, name_prefix:prefix, type:t}, function(data){
                    if(data && data.success) {
                        dp.child[key] = data;
                        $('.j-dp-child-'+id, dp.$block).html( ( data.form.length ? data.form : '') ).toggleClass('hidden', ! data.form.length);
                    }
                });
            }
        });

        var $term = $f.find('#j-term-title');
        f.$field('term').change(function(){
            var id = $(this).val();
            if(o.terms.hasOwnProperty(id)){
                if(o.terms[id]){
                    $term.html(o.terms[id]);
                    $term.parent().removeClass('hidden');
                }else{
                    $term.parent().addClass('hidden');
                }
            }else{
                $term.parent().addClass('hidden');
            }
        });

        $f.find('.j-term-change').click(function(e){
            e.preventDefault();
            var $el = $(this);
            $el.parent().remove();
            $f.append('<input type="hidden" name="set_term" value="1" />');
            $el = f.$field('term');
            $el.parent().removeClass('hidden');
            $el.trigger('change');
        });

        var unloadProcessed = false;
        app.$W.bind('beforeunload', function(){
            if( ! unloadProcessed && intval(o.itemID) === 0) {
                unloadProcessed = true;
                var fn = [];
                for(var i in img.active) {
                    if( img.active.hasOwnProperty(i) && img.active[i].data!==false ) {
                        var data = img.active[i].data;
                        if( data.tmp ) {
                            fn.push( data.filename );
                        }
                    }
                }
                if( fn.length ) {
                    bff.ajax(img.url+'delete-tmp', {filenames:fn}, false, false, {async:false});
                }
                fn = [];
                attach.$list.find('.j-attachfn').each(function(){
                    fn.push($(this).val());
                });
                if( fn.length ) {
                    bff.ajax(attach.url+'delete-tmp', {filenames:fn}, false, false, {async:false});
                }
            }
        });

    }

    function geoMapInit()
    {
        if( ! o.mapEnabled) return;
        if(geo.addr.inited) return;
        geo.addr.inited = 1;
        geo.addr.map = app.map(geo.$block.find('#j-order-addr-map').show().get(0), [geo.addr.$lat.val(), geo.addr.$lon.val()], function(map){
            geo.addr.mapEditor = bff.map.editor();
            geo.addr.mapEditor.init({
                map: map, version: '2.1',
                coords: [geo.addr.$lat, geo.addr.$lon],
                address: geo.addr.$addr,
                addressKind: 'house',
                updateAddressIgnoreClass: 'typed'
            });

            geo.addr.$addr.bind('change keyup input', $.debounce(function(){
                if( ! $.trim(geo.addr.$addr.val()).length ) {
                    geo.addr.$addr.removeClass('typed');
                } else {
                    geo.addr.$addr.addClass('typed');
                    geoMapSearch();
                }
            }, 700));
            geoMapSearch();
        }, {zoom: 13});
    }

    function geoMapSearch()
    {
        if( ! geo.addr.mapEditor) { return; }
        var query = [o.geoCountry];
        var city = $.trim( geo.$block.find('#j-order-city-select').val() );
        if(city) query.push(city);
        var addr = $.trim( geo.addr.$addr.val() );
        if(addr) query.push(addr);
        query = query.join(', ');
        if( geo.addr.lastQuery == query ) return;
        geo.addr.mapEditor.search( geo.addr.lastQuery = query, false, function(){
            geo.addr.mapEditor.centerByMarker();
        } );
    }

    function imgProgress(id, step, data)
    {
        var $slot, $preview;
        switch(step)
        {
            case 'start': {
                $slot = img.type.$ajax.find('.j-img-slot:not(.j-active) .j-img-upload:visible:first').closest('.j-img-slot');
                if( $slot.length ) {
                    img.active[id] = {slot:$slot,data:false};
                    $slot.addClass('j-active'+(data === false ? ' '+img.classes.progress : ''));
                    $slot.find('.j-img-img').attr('src', app.rootStatic+'/img/loader.gif');
                    $slot.find('.j-img-preview').removeClass('hidden');
                    $slot.find('.j-img-upload').addClass('hidden');
                }else{
                    return false;
                }
            } break;
            case 'preview': {
                if( img.active.hasOwnProperty(id) ) {
                    $slot = img.active[id].slot;
                    img.active[id].data = data;
                    $slot.removeClass(img.classes.progress)
                    $slot.find('.j-img-img').attr('src', data.i);
                    $slot.find('.j-img-fn').val(data.filename).attr('name','images['+data.id+']');
                    $slot.find('.j-img-delete').data('id', id);
                }
            } break;
            case 'remove': {
                imgRemove(id);
            } break;
        }
        return true;
    }

    function imgRotate(update)
    {
        var $slots = img.type.$ajax.find('.j-img-slots');
        if(update === true) {
            $slots.sortable('refresh');
        } else {
            $slots.sortable({
                items: '.j-active'
            });
        }
    }

    function imgRemove(id)
    {
        if( img.active.hasOwnProperty(id) )
        {
            var $slot = img.active[id].slot;
            var data = img.active[id].data;
            var clearSlot = function(id, $slot) {
                img.type.$ajax.find('.j-img-upload:visible:last').closest('.j-img-slot').after($slot);
                $slot.find('.j-img-img').attr('src', '');
                $slot.find('.j-img-fn').val('').attr('name','');
                $slot.find('.j-img-preview').addClass('hidden');
                $slot.find('.j-img-upload').removeClass('hidden');
                $slot.removeClass('j-active');

                img.uploader.decrementUploaded();
                delete img.active[id];
                imgRotate(true);
            };
            if( data !== false ) {
                bff.ajax(img.url+'delete',
                    {image_id:data.id, filename:data.filename}, function(resp){
                        if(resp && resp.success) {
                            clearSlot(id, $slot);
                        } else {
                            app.alert.error(resp.errors);
                        }
                    });
            } else {
                clearSlot(id, $slot);
            }
        }
    }

    function attachAdd(data)
    {
        attach.$list.append('<li class="j-attach">'+
            '<a href="#" class="ajax-link j-delete" data-id="'+data.id+'" data-filename="'+data.filename+'"><i class="fa fa-times"></i></a>'+
            '<a href="'+data.i+'" target="_blank">'+data.origin+'</a> ('+data.size+')'+
            (intval(o.itemID) ? '' : '<input type="hidden" class="j-attachfn" name="attach['+data.id+']" value="'+(data.filename)+'" />')+
            '</li>');
        if(attach.$list.find('.j-attach').length >= intval(o.attachLimit)){
            attach.$block.find('.j-upload').hide();
        }
    }

    function setPrice(type)
    {
        type = intval(type);
        if(type && type != intval(o.typeService)){
            price.hide();
            return;
        }

        var specid = 0;
        var $bl = $f.find('.j-specs:visible:first');
        if($bl.length){
            specid = intval($bl.find('.j-spec-value').val());
        }

        if( ! specid){
            price.hide();
            return;
        }
        price.set(specid);
    }

    function setDp()
    {
        var t = o.typeID, id = 0;
        if( ! t){
            t = intval(f.$field('type').filter(':checked').val());
        }
        if(t == intval(o.typeService)){
            var $bl = $f.find('.j-spec-select:first');
            if ($bl.length) {
                id = intval($bl.find('.j-spec-value').val());
            }
        }
        else if(t == intval(o.typeProduct)){
            var $bl = $f.find('.j-cat-select:first');
            if ($bl.length) {
                id = intval($bl.find('.j-cat-value').val());
            }
        }
        if(t && id){
            if (dp.cache.hasOwnProperty(t) && dp.cache[t].hasOwnProperty(id)) {
                dp.$block.html(dp.cache[t][id]);
                jSelect();
            } else {
                if ( ! dp.cache.hasOwnProperty(t)) {
                    dp.cache[t] = {};
                }
                if ( ! dp.cache[t].hasOwnProperty(id)) {
                    bff.ajax(bff.ajaxURL('orders', 'dp-form'), {type:t, id:id}, function(data){
                        if(data && data.dp){
                            dp.$block.html(dp.cache[t][id] = data.dp);
                            jSelect();
                        }else{
                            dp.$block.html('');
                        }
                        if(data.spec_vacancy_data){
                            if (Boolean(intval(data.spec_vacancy_data.price_vacancy_enabled))){
                                $('.j-vacancy-info').show();
                                $('.j-vacancy-price').html(data.spec_vacancy_data.price_vacancy);
                                $('input[name=price_vacancy]').val(data.spec_vacancy_data.price_vacancy);
                                $('.j-vacancy-days').html(data.spec_vacancy_data.period_vacancy_days);
                            } else {
                                $('.j-vacancy-info').hide();
                            }
                        }
                    });
                }
            }
        } else {
            dp.$block.html('');
        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };

    function jSelect(){
        $('.j-select').select2({
            minimumResultsForSearch: Infinity
        });
    }

}());