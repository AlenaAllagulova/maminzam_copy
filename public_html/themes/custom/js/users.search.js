var jUsersSearch = (function(){

    var inited = false, o = {lang:{}}, listMngr;
    var $main, $block, $form, $list, $pgn;
    var geo = {$block:0, $city:0, $country:0, country:0, cityAC:0,cityID:0,cityPreSuggest:{}, cityName:{}, metro:{name:{}, data:{}}};
    var map = {$block:0, $map:0, map:0, clusterer:0, points:[], bffmap:0, mapInfoWindow:0, mapMarkers:0};
    var dp = {$block:0, cache:{}, child:{}, t:1, id:0};
    var specscache = {};
    var onPopstate = false;
    var m = 0;

    function init()
    {
        $main = $('#j-users-search');
        $block = $('#j-users-search-list');

        $form = $('#j-users-search-form-block').find('form');
        $list = $block.find('.j-list');
        $pgn = $block.find('.j-pagination');
        map.$map = $block.find('#map-desktop');
        map.$block = map.$map.parent();

        if(o.ajax){
            $pgn.on('click', '.j-pgn-page', function(){
                var $el = $(this);
                listMngr.page($el.data('page'));
                return false;
            });
        }
        $pgn.on('change', '.j-pgn-goto', function(){
            var $el = $(this);
            listMngr.page($el.val());
            $el.val('');
            return false;
        });

        $form.on('click', '.j-cat-title', function(){
            var $el = $(this);
            var $li = $el.closest('li');
            $li.toggleClass('opened');
            $li.find('ul').toggleClass('hidden');
            $el.find('.fa').toggleClass('fa-caret-right fa-caret-down');
            return false;
        });

        var $mobileCats = $form.find('#j-mobile-cats');
        $form.on('click', '.j-mobile-cat', function(){
            var $el = $(this);
            $form.find('#j-mobile-cat-'+$el.data('id')).collapse('show');
            $mobileCats.collapse('hide');
            return false;
        });

        $form.on('click', '.j-mobile-cat-back', function(){
            $(this).closest('.j-mobile-cat-block').collapse('hide');
            $mobileCats.collapse('show');
            return false;
        });

        $form.on('show.bs.collapse', '.j-collapse', function(){
            var $el = $(this).prev('a.h6');
            if( ! $el.length) return;
            $el.addClass('active');
            $el.find('i.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
        });

        $form.on('hide.bs.collapse', '.j-collapse', function(){
            var $el = $(this).prev('a.h6');
            if( ! $el.length) return;
            $el.removeClass('active');
            $el.find('i.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
        });

        geo.fn = (function(){
            geo.$block = $('#j-left-region');
            geo.$city = geo.$block.find('#j-region-city-select');
            var $cityVal = geo.$block.find('#j-region-city-value');
            geo.$metro = geo.$block.find('#j-region-metro-select');
            var $metroVal = geo.$block.find('#j-region-metro-value');
            var $region = $form.find('[name="r"]');
            geo.country = intval(o.defCountry);
            geo.cityID = $cityVal.length ? intval($cityVal.val()) : intval(o.cityID);

            geo.$country = geo.$block.find('#j-left-region-country');
            if(geo.$country.length){
                geo.$country.change(function(){
                    geo.country = intval(geo.$country.val());
                    if(geo.country){
                        initCity();
                        geo.cityAC.setParam('country', geo.country);
                        geo.$city.addClass('hidden');
                        if( geo.cityPreSuggest.hasOwnProperty(geo.country) ) {
                            geo.cityAC.setSuggest(geo.cityPreSuggest[geo.country], true);
                        }else{
                            bff.ajax(bff.ajaxURL('geo', 'country-presuggest'), {country:geo.country}, function(data){
                                geo.cityAC.setSuggest[geo.country] = data;
                                geo.cityAC.setSuggest(data, true);
                            });
                        }
                        geo.$city.removeClass('hidden');
                    }else{
                        geo.$city.addClass('hidden');
                        geo.$metro.addClass('hidden');
                        $region.val(0);
                        $cityVal.val(0);
                        $metroVal.val(0);
                        if( ! onPopstate) {
                            listMngr.submit({}, true);
                        }
                    }
                    return true;
                });
                geo.country = intval(geo.$country.val());
            }else{
                $form.on('shown.bs.collapse', '#j-left-region', function(){
                    initCity();
                });
            }

            var cityInited = false;
            function initCity(){
                if(cityInited) return;
                cityInited = true;
                var hidden = false;
                if(geo.$city.hasClass('hidden')){
                    hidden = true;
                    geo.$city.removeClass('hidden');
                }
                geo.$city.autocomplete( bff.ajaxURL('geo', 'region-suggest'),{
                    valueInput: $cityVal, params:{reg:1, country: geo.country},
                    suggest: o.preSuggest ? o.preSuggest : app.regionPreSuggest,
                    onSelect: function(cityID, cityTitle, ex){
                        geo.cityName[intval(cityID)] = cityTitle;
                        if(ex.hasOwnProperty('data') && ex.data.hasOwnProperty(3)){
                            $region.val(ex.data[3]);
                        }else{
                            $region.val(0);
                        }
                        geo.fn.onCitySelect(cityID);
                    },
                    doPrepareText: function(html){
                        var regionTitlePos = html.toLowerCase().indexOf('<br');
                        if( regionTitlePos != -1 ) {
                            html = html.substr(0, regionTitlePos);
                        }
                        html = html.replace(/<\/?[^>]+>/gi, ''); // striptags
                        return $.trim(html);
                    }
                }, function(){ geo.cityAC = this; });
                if(hidden)  geo.$city.addClass('hidden');
            }
            if(geo.$city.is(':visible')) initCity();

            var metroInited = false;
            function initMetro(){
                if(metroInited) return;
                metroInited = true;
                var hidden = false;
                if(geo.$metro.hasClass('hidden')){
                    hidden = true;
                    geo.$metro.removeClass('hidden');
                }
                geo.$metro.autocomplete( bff.ajaxURL('geo', 'metro-suggest'),{
                    valueInput: $metroVal, params:{reg:1, city: geo.cityID},
                    onSelect: function(metroID, metroTitle, ex){
                        geo.metro.name[intval(metroID)] = metroTitle;
                        geo.fn.onMetroSelect(metroID);

                    },
                    doPrepareText: function(html){
                        var pos = html.toLowerCase().indexOf('<br');
                        if( pos != -1 ) {
                            html = html.substr(0, pos);
                        }
                        html = html.replace(/<\/?[^>]+>/gi, '');
                        return $.trim(html);
                    }
                }, function(){ geo.metroAC = this; });
                if(hidden)  geo.$metro.addClass('hidden');
            }
            if(geo.$metro.is(':visible')) initMetro();
            if(intval(o.cityID))  initMetro();

            return {
                onCitySelect:function(cityID){
                    geo.cityID = cityID;
                    initMetro();
                    geo.metroAC.setParam('city', geo.cityID);
                    geo.$metro.addClass('hidden');
                    if( geo.metro.data.hasOwnProperty(geo.cityID) ) {
                        if(geo.metro.data[geo.cityID]){
                            geo.$metro.removeClass('hidden');
                        }else{
                            geo.$metro.addClass('hidden');
                        }
                    }else{
                        bff.ajax(bff.ajaxURL('geo', 'has-metro'), {city:geo.cityID}, function(data){
                            if(data && data.success){
                                geo.metro.data[geo.cityID] = data.hasMetro;
                                if(geo.metro.data[geo.cityID]){
                                    geo.$metro.removeClass('hidden');
                                }else{
                                    geo.$metro.addClass('hidden');
                                }
                            }
                        });
                    }
                    geo.$metro.val('');
                    $metroVal.val(0);

                    if( ! onPopstate) {
                        listMngr.submit({}, true);
                    }
                },
                onMetroSelect:function(metroID){
                    if( ! onPopstate) {
                        listMngr.submit({}, true);
                    }
                },
                clear:function(){
                    if (geo.$country.length) {
                        geo.$country.val(0);
                        geo.$city.addClass('hidden');
                        geo.$metro.addClass('hidden');
                    }
                    $region.val(0);
                    $cityVal.val(0); geo.$city.val('');
                    $metroVal.val(0); geo.$metro.val('');
                },
                onPopstate:function(){
                    if(geo.$country.length){
                        if( ! geo.$country.find(':selected').length){
                            geo.$country.find(':first').prop('selected', true);
                        }
                    }
                    var city = intval(geo.$block.find('#j-region-city-value').val());
                    if(geo.cityName.hasOwnProperty(city)){
                        geo.$city.val(geo.cityName[city]);
                    }
                    var metro = intval($metroVal.val());
                    if(geo.metro.name.hasOwnProperty(metro)){
                        geo.$metro.val(geo.metro.name[metro]);
                    }
                    if( geo.metro.data.hasOwnProperty(city) ) {
                        if(geo.metro.data[city]){
                            geo.$metro.removeClass('hidden');
                        }else{
                            geo.$metro.addClass('hidden');
                        }
                    }
                }
            };
        }());

        var flags = {};
        $form.find('.j-checkbox-flag').each(function(){
            var $el = $(this);
            var name = $el.data('name');
            flags[name] = {
                $el:$el,
                $input:$form.find('[name="'+name+'"]')
            };
        });

        $form.on('click', '.j-checkbox-flag', function(){
            var $el = $(this);
            var name = $el.data('name');
            $el.toggleClass('checked');
            flags[name].$input.val($el.hasClass('checked') ? 1 : 0);
            $el.find('i.ico-square-checked, i.ico-square').toggleClass('ico-square-checked ico-square');
            if( ! onPopstate) {
                listMngr.submit({}, true);
            }
        });

        $form.on('click', '.j-toggle-icon', function(){
            var $el = $(this);
            $el.find('i.icon-arrow-point-to-right, i.icon-arrow-point-to-down').toggleClass('icon-arrow-point-to-right icon-arrow-point-to-down');

        });

        $form.on('click', '.j-clear-filter', function(){
            geo.fn.clear();
            $form.find('input:text, input:password, select, textarea').val('');
            $form.find('input:radio, input:checkbox').prop('checked', false);
            for(var i in flags){
                if(flags.hasOwnProperty(i)){
                    flags[i].$el.removeClass('checked');
                    flags[i].$input.val(0);
                    flags[i].$el.find('i.fa').removeClass('ico-square-checked').addClass('ico-square');
                }
            }
            listMngr.submit({}, true);
            // $form.find('.j-collapse.in:not(:first)').collapse('hide');
            return false;
        });

        $block.on('click', '.j-f-view-type', function(){
            var $el = $(this);
            m = intval($el.data('id'));
            $el.parent().addClass('active').siblings().removeClass('active');
            $form.find('[name="m"]').val(m);
            if(m){
                map.$block.removeClass('hidden');
                $list.before($pgn);
                map.fn.init();
            }else{
                map.$block.addClass('hidden');
                $pgn.before($list);
            }

            listMngr.submit({}, true);
            return false;
        });

        $block.on('click', '.j-map-marker', function(){
            if(map.bffmap.isYandex()){
                var n = intval($(this).data('n'));
                map.fn.openBalloon(n);
            }else if(map.bffmap.isGoogle()){
                var id = intval($(this).data('id'));
                map.fn.popupMarker(id, false);
            }
            $.scrollTo(map.$block, {duration:500, offset:-150});
            return false;
        });

        $form.on('change', '.j-input-change', function(){
            listMngr.submit({}, true);
        });


        m = intval($form.find('[name="m"]').val());
        listMngr = app.list($form, {
            onSubmit: function(resp, ex) {
                if(ex.scroll){
                    if(m){
                        $.scrollTo(map.$block, {offset: -150, duration:500});
                    }else{
                        $.scrollTo($list, {offset: -150, duration:500});
                    }
                }
                $list.html(resp.list);
                $pgn.html(resp.pgn);
                if(resp.hasOwnProperty('count')){
                    $main.find('.j-users-count').text(resp.count);
                }
                if(resp.hasOwnProperty('points') && resp.points){
                    map.fn.showPoints(resp.points);
                }else{
                    map.fn.clearPoints();
                }
                if(typeof(jUsersNote) == 'object'){
                    jUsersNote.init();
                }
                if(typeof(jUsersListFav) == 'object'){
                    jUsersListFav.init();
                }

            },
            onProgress: function(progress, ex) {
                if(ex.fade) $list.toggleClass('disabled');
            },
            onPopstate: function($form, query) {
            },
            onAfterDeserialize: function($form, query){
                dp.$block.find('.j-dp-parent').each(function(){
                    addDpChild($(this));
                });
                $form.deserialize(query, true);
                openDp();
                for(var i in flags){
                    if(flags.hasOwnProperty(i)){
                        if(intval(flags[i].$input.val())){
                            flags[i].$el.addClass('checked');
                            flags[i].$el.find('i.fa').addClass('ico-square-checked').removeClass('ico-square');
                        }else{
                            flags[i].$el.removeClass('checked');
                            flags[i].$el.find('i.fa').addClass('ico-square').removeClass('ico-square-checked');
                        }
                    }
                }
                geo.fn.onPopstate();
            },
            ajax: o.ajax,
            submitDefault:{scroll:true}
        });

        map.fn = (function()
        {
            var inited = false;
            function init(){
                if (inited) return;
                inited = true;

                if (o.coordDefaults) {
                    o.coordDefaults = o.coordDefaults.split(',');
                } else {
                    o.coordDefaults = [55.76, 37.64];
                }

                map.bffmap = app.map(map.$map.get(0), o.coordDefaults, function(mmap){
                    if (this.isYandex()) {
                        map.map = mmap;
                        map.clusterer = new ymaps.Clusterer({
                            preset: 'islands#blueIcon',
                            groupByCoordinates:true,
                            clusterBalloonLeftColumnWidth:50
                        });
                    } else if (this.isGoogle()) {
                        map.mapInfoWindow = new google.maps.InfoWindow({});
                    }
                    if(o.points && o.points.length) showPoints(o.points);
                }, {controls: 'search'});
            }

            function showPoints(points){
                if ( ! inited) {
                    o.points = points;
                    return;
                }
                if ( ! map.bffmap) return;

                if (map.bffmap.isYandex()) {

                    clearPoints();
                    map.points = [];

                    for (var i in points) {
                        if (points.hasOwnProperty(i)) {
                            var coord;
                            if (o.coordorder == 'longlat') {
                                coord = [points[i].lng, points[i].lat];
                            } else {
                                coord = [points[i].lat, points[i].lng];
                            }
                            map.points[i] = new ymaps.Placemark(
                                coord,
                                {
                                    balloonContentBody: points[i].b,
                                    clusterCaption: points[i].n
                                }, {
                                    preset: 'islands#blueIcon'
                                });

                        }
                    }
                    map.clusterer.add(map.points);
                    map.map.geoObjects.add(map.clusterer);

                    map.map.setBounds(map.clusterer.getBounds(), {
                        checkZoomRange: true
                    });
                }
                else if (map.bffmap.isGoogle())
                {
                    clearPoints();
                    var mapG = map.bffmap.getMap();
                    map.mapMarkers = {};
                    var mapMarkersToCluster = [];
                    map.mapInfoWindow.close();

                    for (var i in points) {
                        if (points.hasOwnProperty(i)) {
                            var v = points[i];
                            var id = intval(v.id);
                            var marker = new google.maps.Marker({
                                position: new google.maps.LatLng(parseFloat(v.lat), parseFloat(v.lng))
                            });
                            marker.itemID = id;
                            map.mapMarkers[id] = {
                                position: marker.getPosition(),
                                balloon: v.b
                            };
                            mapMarkersToCluster.push(marker);
                            google.maps.event.addListener(marker, 'click', function () {
                                map.fn.popupMarker(this.itemID, true);
                            });
                        }
                    }

                    map.clusterer = new MarkerClusterer(mapG, mapMarkersToCluster, {
                        imagePath: app.rootStatic+'/js/markerclusterer/images/m'
                    });
                    if (mapMarkersToCluster.length > 1) {
                        map.clusterer.fitMapToMarkers();
                    }
                    map.bffmap.refresh();
                }
            }

            function clearPoints()
            {
                if ( ! inited) return;
                if (map.bffmap.isYandex() && map.clusterer !== 0) {
                    map.clusterer.removeAll();
                    map.map.geoObjects.remove(map.clusterer);
                } else if (map.bffmap.isGoogle()) {
                    if (map.clusterer) {
                        map.clusterer.clearMarkers();
                    }
                }
            }

            function openBalloon(id)
            {
                if ( ! map.points.hasOwnProperty(id)) return;
                if (map.bffmap.isYandex()) {
                    try {
                        var geoObjectState = map.clusterer.getObjectState(map.points[id]);
                        if (geoObjectState.isShown) {
                            if (geoObjectState.isClustered) {
                                geoObjectState.cluster.state.set('activeObject', map.points[id]);
                                map.clusterer.balloon.open(geoObjectState.cluster);
                            } else {
                                try {
                                    map.points[id].balloon.open();
                                } catch (e) {
                                    map.map.setCenter(
                                        map.points[id].geometry.getCoordinates()
                                    ).then(function () {
                                            map.points[id].balloon.open();
                                        }, function (err) {
                                            bff_report_exception(err);
                                        }, this);
                                }
                            }
                        } else {
                            map.map.setCenter(
                                map.points[id].geometry.getCoordinates()
                            ).then(function () {
                                    openBalloon(id);
                                }, function (err) {
                                    bff_report_exception(err);
                                }, this);
                        }
                    } catch (e) {
                        bff_report_exception(e);
                    }
                }
            }

            function popupMarker(itemID, markerClick)
            {
                if (map.bffmap.isGoogle()) {
                    if (map.mapMarkers.hasOwnProperty(itemID)) {
                        var m = map.mapMarkers[itemID];
                        if (markerClick!==true) {
                            map.bffmap.getMap().panTo(m.position);
                        }
                        map.mapInfoWindow.close();
                        map.mapInfoWindow.setPosition(m.position);
                        map.mapInfoWindow.setContent(m.balloon);
                        map.mapInfoWindow.open(map.bffmap.getMap());
                    }
                }
                return false;
            }

            return {
                init:init,
                showPoints:showPoints,
                openBalloon:openBalloon,
                clearPoints:clearPoints,
                popupMarker:popupMarker
            }

        }());

        if(map.$map.is(':visible')){
            map.fn.init();
        }

        if(intval(o.currSpec)){
            dp.id = o.currSpec;
        }

        dp.$block = $form.find('#j-users-search-form-dp');

        dp.$block.on('change', '.j-dp-parent', function(){
            addDpChild($(this));
        });

        dp.$block.on('change', function(){
            listMngr.submit({}, true);
        });
    }

    function addDpChild($el)
    {
        var id = $el.data('id');
        var val = $el.val();
        var key = id+'-'+val;
        if($el.is(':checked')){
            var $dp = $('#j-dp-child-'+key, dp.$block);
            if($dp.length){
                $dp.toggleClass('hidden', false);
            }else{
                var $jdp = $el.closest('.j-dp');

                if( ! dp.child.hasOwnProperty(dp.t)) dp.child[dp.t] = {};
                if(dp.child[dp.t].hasOwnProperty(key)){
                    var data = dp.child[dp.t][key];
                    $jdp.after(data.form);
                    reorderChildDp($jdp, id);
                }else{
                    bff.ajax(bff.ajaxURL('users','dp-child-search'), {dp_id:id, dp_value:val, id:dp.id}, function(data){
                        if(data && data.success) {
                            if(data.form.length){
                                dp.child[dp.t][key] = data;
                                $jdp.after(data.form);
                                reorderChildDp($jdp, id);
                            }
                        }
                    });
                }
            }
        }else{
            $('#j-dp-child-'+key, dp.$block).toggleClass('hidden', true);
        }
    }

    function reorderChildDp($jdp, id)
    {
        var $childs = dp.$block.find('.j-dp-child-'+id);
        if($childs.length > 1){
            $childs.sort(function(a,b){
                var a_num = intval( $(a).data('num') );
                var b_num = intval( $(b).data('num') );
                return a_num < b_num ? 1 : -1;
            });
            $.each($childs, function(index, row){
                $jdp.after(row);
            });
        }
    }

    function openDp()
    {
        dp.$block.find('.j-dp').each(function(){
            var $bl = $(this);
            var open = $bl.find('input[type="checkbox"]:checked').length > 0;
            if( ! open){
                $bl.find('input[type="text"]').each(function(){
                    if(intval($(this).val())){
                        open = true;
                    }
                });
            }
            if(open){
                $bl.collapse();
            }
        });
    }

    return{
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    }
}());
