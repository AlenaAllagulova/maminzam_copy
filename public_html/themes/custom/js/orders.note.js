var jOrdersNote = (function(){
    var inited = false, o = {lang:{}};
    var $blocks;

    function init()
    {
        $blocks = $('.j-order-note');
        $blocks.each(function(){
            var $bl = $(this);
            if( ! $bl.hasClass('i')){

                $bl.addClass('i');
                var $blf = $bl.find('.j-notes-form-block');
                var $blshow = $bl.find('.j-notes-show');
                $bl.on('click', '.j-note-add', function(){
                    $(this).hide();
                    initform($blf, $blshow);
                    return false;
                });

                $bl.on('click', '.j-notes-input', function(){
                    initform($blf, $blshow);
                    return false;
                });

                $blf.on('click', '.j-note-cancel', function(){
                    $blf.hide();
                    if(intval($blshow.data('id'))){
                        $blshow.show();
                    }else{
                        $bl.find('.j-note-add').show();
                    }
                    return false;
                });

                $bl.on('click', '.j-note-edit', function(){
                    $blshow.hide();
                    initform($blf, $blshow);
                    return false;
                });

                $bl.on('click', '.j-note-delete', function(){
                    $blshow.hide();
                    bff.ajax(
                        bff.ajaxURL('orders', 'note-delete'), {id:$blshow.data('id'), hash: app.csrf_token}, function(resp, errors){
                            if(resp && resp.success) {
                                $('.j-notes-input').val('');
                                $blshow.data('id', 0);
                                $blf.find('[name="id"]').val(0);
                                initform($blf, $blshow);
                            } else {
                                app.alert.error(errors);
                            }
                    });
                    return false;
                });
            }
        });
    }

    function initform($blf, $blshow)
    {
        if($blf.hasClass('i')){
            $blf.removeClass('hidden');
            return;
        }
        $blf.addClass('i');
        $blf.removeClass('hidden');
        var form = app.form($blf.find('form'), function($f){
            var f = this;
            f.ajax(bff.ajaxURL('orders', 'note-save'), {}, function(resp, errors){
                if(resp && resp.success) {
                    if(resp.data){
                        if(resp.data.note){
                            $blshow.find('.j-note-text').html(resp.data.note);
                        }
                        if(resp.data.id){
                            f.$field('id').val(resp.data.id);
                            $blshow.data('id', resp.data.id);
                        }
                    }
                    $blf.addClass('hidden');
                    $blshow.show();
                } else {
                    f.alertError(errors);
                }
            });
        });
        $blf.show();
    }


    return {
        init: function(options)
        {
            if(inited) return; inited = false;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        },
        $init:init
    };
}());