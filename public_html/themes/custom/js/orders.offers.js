var jOrdersOfferEdit = (function(){
    var inited = false, o = {type:0, lang:{}};

    function init()
    {
        var $block = $('#j-offer-descr');
        var $blf = $('#j-offer-edit-form-block');

        var form = app.form($blf.find('form'), function($f){
            var f = this;
            f.ajax(bff.ajaxURL('orders&ev=offer_my_descr', 'descr'),{},function(resp, errors){
                if(resp && resp.success) {
                    if(resp.descr){
                        $block.find('.j-text').html(resp.descr);
                    }
                    $blf.hide();
                    $block.show();
                } else {
                    f.alertError(errors);
                }
            });
        });

        $block.on('click', '.j-edit', function(){
            $block.hide();
            $blf.show();
            return false;
        });

        $blf.on('click', '.j-cancel', function(){
            $blf.hide();
            $block.show();
            return false;
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());


var jOrdersOfferChat = (function(){
    var inited = false, o = {type:0, lang:{}, id:0};
    var $chats;

    function init()
    {
        $chats = $('.j-open-chat');
        $chats.on('click', function(){
            var $el = $(this);
            var $bl = $($el.attr('href'));
            if($bl.length){
                if( ! $bl.hasClass('i')){
                    var $block = $bl.closest('.j-chat-block');
                    $bl.addClass('i');
                    if(intval($bl.data('cnt'))){
                        bff.ajax(bff.ajaxURL('orders&ev=offers_list', 'chat'), {id:$bl.data('id'), hash:app.csrf_token}, function(resp, errors){
                            if(resp && resp.success) {
                                if(resp.html){
                                    $bl.find('.j-chat').html(resp.html);
                                    $('.show-tooltip').tooltip();
                                }
                                $el.find('.label-new').remove();
                            } else {
                                app.alert.error(errors);
                            }

                        });
                    }
                    app.form($bl.find('form'), function($f){
                        var f = this;
                        f.ajax(bff.ajaxURL('orders&ev=offers_list', 'chat-add'),{},function(resp, errors){
                            if(resp && resp.success) {
                                if(resp.html){
                                    $bl.find('.j-chat').append(resp.html);
                                    f.reset();
                                }
                            } else {
                                f.alertError(errors);
                            }
                        });
                    });
                    $bl.on('show.bs.collapse', function(){
                        $block.find('.j-open-chat-response').addClass('hidden');
                        $block.find('.j-open-chat-title').text(o.lang.chat_title_hide);
                    });
                    $bl.on('hide.bs.collapse', function(){
                        $block.find('.j-open-chat-response').removeClass('hidden');
                        $block.find('.j-open-chat-title').text(o.lang.chat_title_show);
                    });
                    $bl.on('shown.bs.collapse', function(){
                        if($bl.hasClass('r')){
                            $bl.removeClass('r');
                            $bl.find('[name="message"]').focus();
                        }
                    });
                }
                if($el.hasClass('j-open-chat-response')){
                    $bl.addClass('r');
                }
            }
        });
        o.id = intval(o.id);
        if(o.id){
            $('.j-open-chat-response').each(function(){
                var $el = $(this);
                if(intval($el.data('id')) == o.id){
                    $el.trigger('click');
                }
            });

        }
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());


var jOrdersOffersPerformer = (function(){
    var inited = false, o = {type:0, lang:{}};
    var $block, $order, $mobTitle;
    var st = {};
    var li_classes = {}, la_classes = {}, la_title = {};
    var orderOpinions = false;
    var fairplayEnabled = false, fairplay;

    function init()
    {
        for(var s in o.st){
            if(o.st.hasOwnProperty(s)){
                st[s] = intval(o.st[s]);
            }
        }
        for(var s in o.statuses){
            if(o.statuses.hasOwnProperty(s)){
                if(o.statuses[s].hasOwnProperty('cli')){
                    li_classes[s] = o.statuses[s].cli;
                }
                if(o.statuses[s].hasOwnProperty('cla')){
                    la_classes[s] = o.statuses[s].cla;
                }
                if(o.statuses[s].hasOwnProperty('tl')){
                    la_title[s] = o.statuses[s].tl;
                }
            }
        }
        orderOpinions = intval(o.ordersOpinions);
        fairplayEnabled = intval(o.fairplayEnabled);
        fairplay = intval(o.fairplay);

        $block = $('#j-offers');
        var $modal = $('#j-modal-worker');
        $block.on('click', '.j-status', function(){
            var $el = $(this);
            var s = intval($el.data('s'));
            if(orderOpinions && s == st.start && $modal.length){
                var $bl = $el.closest('.j-offer');
                var $name = $modal.find('.j-worker-name');
                $name.html($bl.find('.j-worker-info').html());
                $name.find('.o-freelancer-buttons').remove();
                $name.find('.label-new').remove();
                $('.show-tooltip').tooltip();
                $modal.find('[name="id"]').val($el.data('id'));
                $modal.find('[name="message"]').val('').trigger('keyup');
                $modal.modal('show');
                if(fairplayEnabled){
                    var $fp = $modal.find('[name="fairplay"]');
                    if(fairplay){
                        $fp.filter('[value="'+o.fairplay_use+'"]').prop('checked', true);
                    }else{
                        if(intval($el.data('fp'))){
                            $fp.filter('[value="'+o.fairplay_use+'"]').prop('checked', true);
                        }else{
                            $fp.filter('[value="'+o.fairplay_none+'"]').prop('checked', true);
                        }
                    }
                    var t = intval($el.data('term'));
                    if(t){
                        $modal.find('[name="term"]').val(t);
                    }
                }
                return;
            }
            bff.ajax(bff.ajaxURL('orders&ev=offers_list', 'status'), {id:$el.data('id'), status: s, hash:app.csrf_token}, function(resp, errors){
                if(resp && resp.success) {
                    $el.addClass('active').siblings().removeClass('active');
                    var $bl = $el.closest('.j-offer');
                    setClassLI($bl, s);
                    setClassLa($bl.find('.j-label'), s);
                    if(s == st.performer){
                        $block.find('.j-status-' + st.performer).prop('disabled', true);
                        $bl.find('.j-status-' + st.performer).prop('disabled', false);
                    }else{
                        if( ! $block.find('.active.j-status-' + st.performer).length){
                            $block.find('.j-status-' + st.performer).prop('disabled', false);
                        }
                    }
                    if(orderOpinions){
                        $bl.data('status', s);
                        statusFilter();
                    }
                    if(s == st.candidate){
                        app.alert.success(o.lang.candidate);
                    }
                } else {
                    app.alert.error(errors);
                }
            });
        });

        if(orderOpinions){
            $order = $('#j-order-view');
            $mobTitle = $order.find('.j-status-filter-title');
            $order.on('click', '.j-status-filter', function(e){
                e.preventDefault();
                var $el = $(this);
                $el.parent().addClass('active').siblings().removeClass('active');
                statusFilter();
            });
            statusFilter();

            var form = app.form($modal.find('form'), function($f){
                var f = this;
                if(fairplayEnabled){
                    if( ! f.checkRequired({focus:true}) ) return;
                }
                f.ajax(bff.ajaxURL('orders&ev=offers_list', 'performer-start'),{},function(resp, errors){
                    if(resp && resp.success) {
                        var $bl = $block.find('.j-status-'+st.start+'[data-id="'+form.$field('id').val()+'"]').closest('.j-offer');
                        if(resp.html){
                            $block.find('.j-performer').remove();
                            $bl.find('.j-allet-performer').prepend(resp.html);
                            // $bl.prepend(resp.html);
                        }
                        setClassLI($bl, st.start);
                        setClassLa($bl.find('.j-label'), st.start);
                        $block.find('.j-status-' + st.start).prop('disabled', true);
                        $bl.find('.j-status').prop('disabled', true);
                        $modal.modal('hide');
                        $bl.data('status', st.start);
                        $block.prepend($bl);
                        statusFilter();
                        $order.find('.j-status-filter[data-status="0"]').trigger('click');
                        setTimeout(function(){
                            $.scrollTo($bl, {offset: -50, duration:100});
                        }, 10);

                    } else {
                        f.alertError(errors);
                    }
                });
            });

            bff.maxlength(form.$field('message'), {limit:500, message:$modal.find('.j-help-block'), lang:{left: o.lang.left, symbols:o.lang.symbols}});

            $block.on('click', '.j-performer-start-cancel', function(e){
                e.preventDefault();
                var $el = $(this);
                var $bl = $el.closest('.j-offer');
                var id = intval($bl.find('.j-status-'+st.start).data('id'));
                if(id){
                    bff.ajax(bff.ajaxURL('orders&ev=offers_list', 'performer-cancel'), {id:id, hash:app.csrf_token}, function(resp, errors){
                        if(resp && resp.success) {
                            $block.find('.j-status-' + st.start).prop('disabled', false);
                            $bl.find('.j-status').prop('disabled', false);
                            $bl.find('.j-performer').remove();
                            setClassLI($bl, 0);
                            setClassLa($bl.find('.j-label'), 0);
                            $bl.data('status', 0);
                            statusFilter();
                            $order.find('.j-performer-start-cancel-'+id).remove();
                        } else {
                            app.alert.error(errors);
                        }
                    });
                }
            });

            $order.on('click', '.j-performer-start-cancel-general', function(e){
                e.preventDefault();
                $block.find('.j-offer[data-id="'+$(this).data('id')+'"]').find('.j-performer-start-cancel').trigger('click');
            });

            var $modalChange = false;
            $order.on('click', '.j-workflow-change', function(e){
                e.preventDefault();
                var $el = $(this);
                var id = intval($el.data('id'));
                if($modalChange && (intval($modalChange.data('id')) != id)){
                    $modalChange.remove();
                    $modalChange = false;
                }
                if($modalChange){
                    $modalChange.modal('show');
                }else{
                    bff.ajax(bff.ajaxURL('orders&ev=offers_list', 'modal-workflow-change'), {id:id, hash:app.csrf_token}, function(resp, errors){
                        if(resp && resp.success) {
                            $modalChange = $(resp.html);
                            app.$B.append($modalChange);
                            $modalChange.modal('show');
                            initWorkflowChange($modalChange);
                        } else {
                            app.alert.error(errors);
                        }
                    }, function(yes){
                        if(yes){
                            if( ! $el.is('[data-loading-text]')) $el.attr('data-loading-text', app.lang.form_btn_loading);
                            $el.button('loading');
                        }else{
                            $el.button('reset');
                        }
                    });
                }
            });

        }
    }

    function setClassLI($bl, s)
    {
        for(var i in li_classes){
            if(li_classes.hasOwnProperty(i)){
                $bl.removeClass(li_classes[i]);
            }
        }
        if(li_classes.hasOwnProperty(s)){
            $bl.addClass(li_classes[s]);
        }
    }

    function setClassLa($bl, s)
    {
        for(var i in la_classes){
            if(la_classes.hasOwnProperty(i)){
                $bl.removeClass(la_classes[i]);
            }
        }
        if(la_classes.hasOwnProperty(s)){
            $bl.addClass(la_classes[s]);
        }
        if(la_title.hasOwnProperty(s)){
            $bl.text(la_title[s]);
        }
    }

    function statusFilter()
    {
        var $active = $order.find('.active > .j-status-filter');
        var status = intval($active.data('status'));
        var $offers = $block.find('.j-offer');
        $offers.addClass('hidden').removeClass('last');
        var cnt = {0:0};
        for(var i in st){
            if( ! st.hasOwnProperty(i)) continue;
            cnt[ st[i] ] = 0;
        }
        $offers.each(function(){
            var $el = $(this);
            var s = intval($el.data('status'));
            cnt[s]++;
            if(status){
                if(s == status){
                    $el.removeClass('hidden');
                }
                if(status == st.decline && s == st.invite_no){
                    $el.removeClass('hidden');
                }
            }else{
                if( ! s || s == st.start || s == st.agree){
                    $el.removeClass('hidden');
                }
            }
        });
        cnt[0] += cnt[st.start];
        cnt[0] += cnt[st.agree];
        cnt[st.decline] += cnt[st.invite_no];

        $offers.filter(':visible:last').addClass('last');
        $order.find('.j-status-filter').each(function(){
            var $el = $(this);
            var s = intval($el.data('status'));
            if( ! cnt.hasOwnProperty(s)) return;
            $el.find('.label-count').html(cnt[s]);
        });
        $mobTitle.find('.j-title').text($active.find('.j-title').text());
        $mobTitle.find('.label-count').text($active.find('.label-count').text());
    }

    function initWorkflowChange($bl)
    {
        var f = app.form($bl.find('form'), function(){
            if( ! f.checkRequired({focus:true}) ) return;
            var id = f.$field('id').val();
            f.ajax(bff.ajaxURL('orders&ev=offers_list', 'workflow-change'),{},function(resp, errors){
                if(resp && resp.success) {
                    var $general = $order.find('.j-performer-start-cancel-'+id);
                    if($general.length){
                        $general.after(resp.general);
                        $general.remove();
                    }
                    var $inList = $block.find('.j-performer');
                    if($inList.length){
                        $inList.after(resp.html);
                        $inList.remove();
                    }
                    $bl.modal('hide');
                } else {
                    f.alertError(errors);
                }
            });
        });

    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
}());