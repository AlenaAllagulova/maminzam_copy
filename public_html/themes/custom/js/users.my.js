var jMySettings = (function(){
    var inited = false, o = {lang:{}, url_settings:''},
        geo = {$block:0,$regionBlocks:0,country:0,cityAC:0,cityID:0,cityPreSuggest:{},
               addr:{$block:0,map:0,mapEditor:0,lastQuery:'',inited:0}, metro:{data:{}}},
        $cont, avatarUploader, dpCache = {}, servicesCache = {}, dpCacheChild = {}, $specs;

    function init()
    {
        $cont = $('#j-my-settings');
        $specs = $cont.find('#j-my-specs');

        // avatar uploader
        avatarUploader = new qq.FileUploaderBasic({
            button: $cont.find('#j-my-avatar-upload').get(0),
            action: o.url_settings,
            params: {hash: app.csrf_token, act: 'avatar-upload'},
            uploaded: 0, multiple: false, sizeLimit: o.avatarMaxsize,
            allowedExtensions: ['jpeg','jpg','png','gif'],
            onSubmit: function(id, fileName) {
                setAvatarImg(false, true);
            },
            onComplete: function(id, fileName, data) {
                if(data && data.success) {
                    setAvatarImg(data[o.avatarSzBig]);
                } else {
                    if(data.errors) {
                        app.alert.error(data.errors, {title: o.lang.ava_upload});
                        function reload() {
                            location.reload();
                        }
                        setTimeout(reload, 2000);
                    }
                }
                return true;
            },
            messages: o.lang.ava_upload_messages,
            showMessage: function(message, code) {
                app.alert.error(message, {title: o.lang.ava_upload});
            }
        });

        $cont.on('click', '#j-my-avatar-delete', function(e){ nothing(e);
            bff.ajax(o.url_settings, {hash: app.csrf_token, act:'avatar-delete'}, function(data,errors){
                if(data && data.success) {
                    setAvatarImg(data[o.avatarSzBig]);
                }else{
                    if(errors) {
                        app.alert.error(errors);
                    }
                }
            });
            return false;
        });

        // contacts
        contactsInit(o.contactsLimit, o.contactsData);

        // specializations
        if(typeof(jSpecsSelect) == 'object'){
            jSpecsSelect.init({
                block: $specs,
                cnt: o.specCnt,
                limit: o.specLimit,
                onSelect:function(data, $bl){
                    if(data.spec){
                        $bl = $bl.find('.j-spec-ex'); $bl.html('').removeClass('hidden');
                        if(o.specsServices){
                            showSpec(intval(data.spec), $bl);
                        }
                        showDp(intval(data.spec), $bl);
                    }
                },
                onDelete:function($bl){
                    $bl.find('.j-spec-ex').html('');
                },
                catOnSelect: 0,
                cancelIfExists: 1,
                existsMessage: o.lang.existsMessage
            });
        }

        $specs.on('change', '.j-dp-child', function(){
            var $el = $(this);
            var id = $el.data('id');
            var prefix = $el.data('name');
            var val = $el.val();
            var key = id+'-'+val;
            if( ! intval(val)) dpCacheChild[key] = {form:''};
            if( dpCacheChild.hasOwnProperty(key) ) {
                var data = dpCacheChild[key];
                $('.j-dp-child-'+id, $specs).html( ( data.form.length ? data.form : '') ).toggleClass('hidden', ! data.form.length);
            } else {
                bff.ajax(bff.ajaxURL('users','dp-child'), {dp_id:id, dp_value:val, name_prefix:prefix}, function(data){
                    if(data && data.success) {
                        dpCacheChild[key] = data;
                        $('.j-dp-child-'+id, $specs).html( ( data.form.length ? data.form : '') ).toggleClass('hidden', ! data.form.length);
                    }
                });
            }
        });

        // geo
        geo.fn = (function(){
            geo.$block = $cont.find('#j-my-geo');
            if( ! geo.$block.length){
                return;
            }
            geo.$regionBlocks = geo.$block.find('.j-region');
            geo.$block.find('#j-my-geo-country').change(function(){
                //country
                geo.country = intval($(this).val());
                o.reg1_country = geo.country;
                o.geoCountry = $(this).find(':selected').text();
                if(geo.country){
                    geo.cityAC.setParam('country', geo.country);
                    if( geo.cityPreSuggest.hasOwnProperty(geo.country) ) {
                        geo.cityAC.setSuggest(geo.cityPreSuggest[geo.country], true);
                    }else{
                        bff.ajax(bff.ajaxURL('geo', 'country-presuggest'), {country:geo.country}, function(data){
                            geo.cityPreSuggest[geo.country] = data;
                            geo.cityAC.setSuggest(data, true);
                        });
                    }
                    geoMapInit();
                    geoMapSearch();
                    geo.$regionBlocks.removeClass('displaynone');
                } else {
                    geo.$regionBlocks.addClass('displaynone');
                }
                return true;
            });

            //city
            geo.cityID = o.reg3_city;
            geo.$block.find('#j-my-geo-city-select').autocomplete( bff.ajaxURL('geo', 'region-suggest'),{
                 valueInput: geo.$block.find('#j-my-geo-city-value'), params:{reg:0, country: o.reg1_country}, suggest: app.regionPreSuggest,
                 onSelect: function(cityID, cityTitle, ex){
                     geo.fn.onCity(cityID, ex);
                 },
                 doPrepareText: function(html){
                     var regionTitlePos = html.toLowerCase().indexOf('<br');
                     if( regionTitlePos != -1 ) {
                        html = html.substr(0, regionTitlePos);
                     }
                     html = html.replace(/<\/?[^>]+>/gi, ''); // striptags
                     return $.trim(html);
                 }
            }, function(){ geo.cityAC = this; });

            //addr
            geo.addr.$addr = geo.$block.find('#j-my-geo-addr-addr');
            geo.addr.$lat = geo.$block.find('#j-my-geo-addr-lat');
            geo.addr.$lon = geo.$block.find('#j-my-geo-addr-lng');
            if(intval(geo.cityID)){
                geoMapInit();
            }

            //metro
            geo.metro.$block = $('.j-my-geo-metro');
            geo.metro.$empty = geo.metro.$block.find('.j-my-geo-metro-empty');
            geo.metro.$selected = geo.metro.$block.find('.j-my-geo-metro-selected');
            geo.metro.$value = geo.metro.$block.find('.j-my-geo-metro-value');
            geo.metro.$step1 = geo.metro.$block.find('.j-my-geo-metro-popup-step1');
            geo.metro.$step2 = geo.metro.$block.find('.j-my-geo-metro-popup-step2');

            function getMetroData(cityID, func)
            {
                if(geo.metro.data.hasOwnProperty(cityID)){
                    if(func) func();
                    return;
                }
                bff.ajax(bff.ajaxURL('geo','form-metro'), {city:cityID}, function(resp){
                    if(resp && resp.success) {
                        geo.metro.data[cityID] = resp;
                        if(func) func();
                    }
                });
            }

            geo.metro.$block.on('show.bs.dropdown', function(){
                getMetroData(geo.cityID);
            });

            geo.metro.$block.on('click', '.j-station', function(){
                var station = $(this).metadata();
                if( station && geo.metro.data.hasOwnProperty(station.city) && station.branch ) {
                    var branch = geo.metro.data[station.city].data[station.branch];
                    geo.metro.$value.val(station.id);
                    geo.metro.$empty.addClass('hide');
                    geo.metro.$selected.find('.j-color').css({backgroundColor:branch.color});
                    geo.metro.$selected.find('.j-title').html(branch.t + ' / ' + branch.st[station.id].t);
                    geo.metro.$selected.removeClass('hide');
                    geo.metro.$block.removeClass('open');
                }else{
                    getMetroData(geo.cityID);
                }
                return false;
            });

            geo.metro.$block.on('click', '.j-back', function(){
                geo.metro.$step1.removeClass('hide');
                geo.metro.$step2.addClass('hide');
                geo.metro.$step2.html('');
                return false;
            });

            geo.metro.$block.on('click', '.j-branch', function(){
                var branch = $(this).metadata();
                if( branch && geo.metro.data.hasOwnProperty(branch.city) ) {
                    geo.metro.$step1.addClass('hide');
                    geo.metro.$step2.html( geo.metro.data[branch.city].stations[branch.id] );
                    geo.metro.$step2.removeClass('hide');
                }else{
                    getMetroData(geo.cityID);
                }
                return false;
            });

            return {
                onCity: function(cityID, ex)
                {
                    if( ! ex.changed ) return;
                    geo.cityID = cityID;
                    app.inputError(geo.$block.find('#j-my-geo-city-value'), false);
                    // map
                    if(ex.title.length > 0) {
                        if(cityID){
                            geoMapInit();
                        }
                        geoMapSearch();
                    }
                    geo.fn.refreshMetro(cityID);
                },
                refreshMetro:function(cityID)
                {
                    if( ! geo.metro.data.hasOwnProperty(cityID)){
                        getMetroData(cityID, function(){ geo.fn.refreshMetro(cityID); });
                        return;
                    }
                    var metro = geo.metro.data[cityID];
                    if(metro.branches){
                        geo.metro.$step1.removeClass('hide');
                        geo.metro.$step1.html(metro.branches);
                        geo.metro.$step2.addClass('hide');
                        geo.metro.$empty.removeClass('hide');
                        geo.metro.$selected.addClass('hide');
                        geo.metro.$block.removeClass('hide');
                        geo.metro.$value.val(0);
                    }else{
                        geo.metro.$block.addClass('hide');
                    }
                }
            };
        }());

        // contacts form
        app.form($cont.find('.j-form-contacts'), function(){ return false;}, {
            onInit: function($f){
                var f = this;
                $f.on('click', '.j-submit', function(){
                    if( ! f.checkRequired({focus:true}) ) return false;
                    f.ajax(o.url_settings,{},function(data,errors){
                        if(data && data.success) {
                            f.alertSuccess(o.lang.saved_success);
                            f.$field('name').val(data.name);
                            if(intval(data.reload)){
                                setTimeout(function(){
                                    location.reload();
                                }, 500);
                            }
                        } else {
                            f.fieldsError(data.fields, errors);
                        }
                    });
                    return false;
                });
            }
        });

        // contacts form
        app.form($cont.find('.j-form-specs'), function(){ return false;}, {
            onInit: function($f){
                var f = this;
                $f.on('click', '.j-submit', function(){
                    if( ! f.checkRequired({focus:true}) ) return false;
                    f.ajax(o.url_settings,{},function(data,errors){
                        if(data && data.success) {
                            f.alertSuccess(o.lang.saved_success);
                            if(intval(data.reload)){
                                setTimeout(function(){
                                    location.reload();
                                }, 500);
                            }
                        } else {
                            f.fieldsError(data.fields, errors);
                        }
                    });
                    return false;
                });
            }
        });

        // social form
        socialButtonsInit();

        // enotify form
        app.form($cont.find('.j-form-enotify'), function($f){
            var f = this;
            f.ajax(o.url_settings,{},function(resp,errors){
                if(resp && resp.success) {
                    f.alertSuccess(o.lang.saved_success);
                } else {
                    f.alertError(errors);
                }
            });
        }, {noEnterSubmit: true});

        // tabs form
        app.form($cont.find('.j-form-profile-tabs'), function($f){
            var f = this;
            f.ajax(o.url_settings,{},function(resp,errors){
                if(resp && resp.success) {
                    f.alertSuccess(o.lang.saved_success);
                } else {
                    f.alertError(errors);
                }
            });
        }, {noEnterSubmit: true});

        // pass form
        app.form($cont.find('.j-form-pass'), function(){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            if( f.fieldStr('pass1') != f.fieldStr('pass2') ) {
                f.fieldsError(['pass2'], o.lang.pass_confirm);
                return;
            }
            f.ajax(o.url_settings,{},function(data,errors){
                if(data && data.success) {
                    f.alertSuccess(o.lang.pass_changed, {reset:true});
                } else {
                    f.fieldsError(data.fields, errors);
                }
            });
        }, {noEnterSubmit: true});

        // email form
        app.form($cont.find('.j-form-email'), function(){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            if( ! bff.isEmail( f.fieldStr('email') ) ) {
                f.fieldsError(['email'], o.lang.email_wrong);
                return;
            }
            if( f.fieldStr('email') === f.fieldStr('email0') ) {
                f.fieldsError(['email'], o.lang.email_diff);
                return;
            }
            f.ajax(o.url_settings,{},function(data,errors){
                if(data && data.success) {
                    f.alertSuccess(o.lang.email_changed, {reset:true});
                    f.$field('email0').val( data.email );
                } else {
                    f.fieldsError(data.fields, errors);
                }
            });
        }, {noEnterSubmit: true});

        // destroy form
        app.form($cont.find('.j-form-destroy'), function(){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            f.ajax(o.url_settings,{},function(data,errors){
                if(data && data.success) {
                    f.alertSuccess(o.lang.account_destoyed, {reset:true});
                    setTimeout(function(){
                        bff.redirect(data.redirect);
                    }, 1500);
                } else {
                    f.fieldsError(data.fields, errors);
                }
            });
        }, {noEnterSubmit: true});

        // join form
        var $joinOn = $cont.find('.j-form-join-on');
        var $joinOff = $cont.find('.j-form-join-off');
        app.form($joinOn, function(){
            var f = this;
            if( ! f.checkRequired({focus:true}) ) return;
            f.ajax(o.url_settings,{},function(data,errors){
                if(data && data.success) {
                    f.alertSuccess(data.msg);
                    $joinOff.removeClass('hidden');
                    $joinOn.addClass('hidden');
                    $cont.find('.j-join-email').val(data.email);
                } else {
                    f.fieldsError(data.fields, errors);
                }
            });
        }, {noEnterSubmit: true});

        $cont.on('click', '.j-join-off', function(e){
            e.preventDefault();
            bff.ajax(o.url_settings, {hash: app.csrf_token, act:'join-profile-off'}, function(data,errors){
                if(data && data.success) {
                    app.alert.success(data.msg);
                    $joinOff.addClass('hidden');
                    $joinOn.removeClass('hidden');
                    $cont.find('.j-join-email').val('');
                }else{
                    if(errors) {
                        app.alert.error(errors);
                    }
                }
            });
            return false;
        });

        // phone form
        var $phF = $cont.find('.j-form-phone');
        if($phF.length) {
            var phF = app.form($phF, function () {
                var f = this;
                if (!f.checkRequired({focus: true})) return;
                f.ajax(o.url_settings, {}, function (data, errors) {
                    if (data && data.success) {
                        $phF.find('.j-step1').addClass('hidden');
                        $phF.find('.j-step2').removeClass('hidden');
                        f.$field('step').val('finish');
                        if (data.msg) {
                            app.alert.success(data.msg);
                        }
                        if(data.phone){
                            $phF.find('.j-step1').removeClass('hidden');
                            $phF.find('.j-step2').addClass('hidden');
                            f.$field('step').val('code-send');
                            f.$field('phone0').val(data.phone);
                            f.$field('phone').val('');
                            f.$field('code').val('');
                        }
                    } else {
                        f.fieldsError(data.fields, errors);
                    }
                });
            }, {noEnterSubmit: true});
            app.user.phoneInput($phF.find('.j-phone-number'));

            $cont.on('click', '.j-phone-change-back', function(e){
                e.preventDefault();
                $phF.find('.j-step1').removeClass('hidden');
                $phF.find('.j-step2').addClass('hidden');
                phF.$field('step').val('code-send');
            });

            $cont.on('click', '.j-phone-change-repeate', function(e){
                e.preventDefault();
                phF.$field('step').val('code-send');
                $phF.submit();
                phF.$field('step').val('finish');
            });
        }

        $specs.on('change', '.j-spec-service-ch', function(){
            $(this).closest('.j-spec-service').find('input[type="checkbox"]').prop('checked', true);
        });

        var oss = intval(o.openSpecServices);
        if(oss){
            oss = $specs.find('#j-services-settings-'+oss);
            if(oss.length) {
                oss.collapse('show');
                $.scrollTo(oss, {offset: -150, duration: 500});
            }
        }

        var $verified = $cont.find('.j-form-verified');
        if($verified.length){
            var verF = app.form($verified, false, {noEnterSubmit: true});
            bff.iframeSubmit($verified, function(resp, errors){
                if(resp && resp.success) {
                    verF.alertSuccess(o.lang.saved_success);
                } else {
                    verF.fieldsError(resp.fields, errors);
                }
                if(intval(resp.reload)){
                    location.reload();
                }
            },{
                beforeSubmit: function(){
                    if( ! verF.checkRequired({focus:true}) ) return false;

                    return true;
                },
                button: '.j-submit'
            });
            var i = 1;
            var $files = $verified.find('.j-files');
            var $add = $verified.find('.j-add');
            $add.click(function(){
                $files.append('<li><a href="#" class="link-red j-delete"><i class="fa fa-times"></i></a> <input type="file" name="verified_'+i+'" style="display: inline;" /> </li>');
                $files.find('input[type="file"]:last').trigger('click');
                i++;
                if($files.find('li').length >= o.verified_limit){
                    $add.hide();
                }
            });
            $files.on('click', '.j-delete', function(e){
                e.preventDefault();
                var $el = $(this);
                $el.closest('li').remove();
                $add.show();
                var fn = $el.data('fn');
                if(fn && fn.length){
                    $verified.append('<input type="hidden" name="verified_deleted[]" value="'+$el.data('fn')+'" />');
                }
            });
        }
    }

    function showDp(specID, $bl)
    {
        if(dpCache.hasOwnProperty(specID)){
            $bl.append(dpCache[specID]);
            $bl.find('.j-spec-dp').find('.collapse').collapse('show');
            jSelect();
        }else{
            bff.ajax(bff.ajaxURL('users', 'dp-form'), {id:specID}, function(data){
                if(data && data.dp){
                    $bl.append(dpCache[specID] = data.dp);
                    $bl.find('.j-spec-dp').find('.collapse').collapse('show');
                    jSelect();
                }
            });
        }
    }

    function showSpec(specID, $bl)
    {
        if (servicesCache.hasOwnProperty(specID)) {
            $bl.append(servicesCache[specID]);
            $bl.find('.j-spec-services').find('.collapse').collapse('show');
        } else {
            bff.ajax(bff.ajaxURL('users', 'specServices-form'), {id:specID}, function(data){
                if(data && data.services){
                    $bl.append(servicesCache[specID] = data.services);
                    $bl.find('.j-spec-services').find('.collapse').collapse('show');
                }
            });
        }
    }

    function setAvatarImg(img, bProgress)
    {
        var $img = $cont.find('#j-my-avatar-img');
        if(bProgress){
            $img.parent().addClass('hidden').after(o.avatarUploadProgress);
        }else{
            $img.parent().removeClass('hidden').next('.j-progress').remove();
        }

        if( img ) {
            $img.attr('src', img);
        } else {
            //
        }
    }

    function contactsInit(limit, contacts)
    {
        var index  = 0, total = 0;
        var $block = $cont.find('#j-my-contacts');
        var $add = $block.find('.j-add');

        function add(contact)
        {
            contact = contact || {};
            if(limit>0 && total>=limit) return;
            index++; total++;
            var value = '';
            if(contact.hasOwnProperty('v') && contact.v) value = contact.v;
            $add.before('<div class="row j-contact">'+
                            '<div class="col-sm-3">'+
                                '<div class="form-group select-custom_global"><select type="text" class="form-control j-select j-type" name="contacts['+index+'][t]">'+o.contactsTypesOptions+'</select></div>'+
                            '</div>'+
                            '<div class="col-xs-8"><div class="form-group">'+
                                '<input type="text" class="form-control j-value" maxlength="40" name="contacts['+index+'][v]" value="'+value.replace(/"/g, "&quot;")+'">'+
                            '</div></div>'+
                            '<div class="col-xs-1 p-profileCabinet-contacts-delete">'+
                                '<a href="#" class="p-delete j-remove"><i class="fa fa-trash-o"></i></a>'+
                            '</div></div>');

            if(contact.hasOwnProperty('t')){
                $block.find('.j-contact:last').find('.j-type').val(contact.t);
            }
            if(limit>0 && total>=limit) {
                $add.hide();
            }
            $('.j-contact').find('.j-select').select2({
                minimumResultsForSearch: Infinity
            });
        }

        $block.on('click', '.j-remove', function(e){ nothing(e);
            var $contact = $(this).closest('.j-contact');
            if( $contact.find('.j-value').val() != '' ) {
                if(confirm('Удалить контакт?')) {
                    $contact.remove(); total--;
                }
            } else {
                $contact.remove(); total--;
            }
            if(limit>0 && total<limit) {
                $add.show();
            }
        });

        $block.on('click', '.j-add', function(e){ nothing(e);
            add();
            return false;
        });

        contacts = contacts || {};
        for(var i in contacts) {
            if( contacts.hasOwnProperty(i) ) {
                add(contacts[i]);
            }
        }
        if( ! total ) {
            add();
        }

    }

    function geoMapInit()
    {
        if(geo.addr.inited) return;
        geo.addr.inited = 1;
        geo.addr.map = app.map(geo.$block.find('#j-my-geo-addr-map').get(0), [geo.addr.$lat.val(), geo.addr.$lon.val()], function(map){
            geo.addr.mapEditor = bff.map.editor();
            geo.addr.mapEditor.init({
                map: map, version: '2.1',
                coords: [geo.addr.$lat, geo.addr.$lon],
                address: geo.addr.$addr,
                addressKind: 'house',
                updateAddressIgnoreClass: 'typed'
            });


            geo.addr.$addr.bind('change keyup input', $.debounce(function(){
                if( ! $.trim(geo.addr.$addr.val()).length ) {
                    geo.addr.$addr.removeClass('typed');
                } else {
                    geo.addr.$addr.addClass('typed');
                    geoMapSearch();
                }
            }, 700));
        }, {zoom: 13});
    }

    function geoMapSearch()
    {
        if( ! geo.addr.mapEditor) { return; }
        var query = [o.geoCountry];
        var city = $.trim( geo.$block.find('#j-my-geo-city-select').val() );
        if(city) query.push(city);
        var addr = $.trim( geo.addr.$addr.val() );
        if(addr) query.push(addr);
        query = query.join(', ');
        if( geo.addr.lastQuery == query ) return;
        geo.addr.mapEditor.search( geo.addr.lastQuery = query, false, function(){
            geo.addr.mapEditor.centerByMarker();
        } );
    }

    function socialButtonsInit()
    {
        var popup;
        $cont.on('click', '.j-my-social-btn', function(e){ nothing(e);
            var $btn = $(this);
            var m = $btn.metadata(); if( ! m || ! m.hasOwnProperty('provider') ) return;
            if( ! $btn.hasClass('not-active') ) {
                bff.ajax(o.url_settings, {act:'social-unlink',provider:m.provider,hash:app.csrf_token}, function(resp, errors){
                    if( resp && resp.success ) {
                        $btn.addClass('not-active');
                        $btn.find('i.fa').remove();
                    } else {
                        app.alert.error(errors);
                    }
                });
                return;
            }
            m = $.extend({w: 450, h: 380}, m || {});
            if(popup !== undefined) popup.close();
            popup = window.open(o.url_social + m.provider+'?ret='+encodeURIComponent(o.url_settings + '?tab=access'), "u_login_social_popup",
                "width=" + m.w + "," +
                "height=" + m.h + "," +
                "left=" + ( (app.$W.width() - m.w) / 2 ) + "," +
                "top=" + ( (app.$W.height() - m.h) / 2 ) + "," +
                "resizable=yes,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=yes");
            popup.focus();
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = $.extend(o, options || {});
            $(function(){ init(); });
        }
    };
    function jSelect(){
        $('.j-select').select2({
            minimumResultsForSearch: Infinity
        });
    }
}());