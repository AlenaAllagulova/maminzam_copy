<?php

# paths
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}
define('BFF_PRODUCT', 'freelance');
define('BFF_VERSION', '2.3.1');

require 'paths.php';
require PATH_CORE . 'init.php';


class bff extends \bff\base\app
{
    static public $userSettings = 0;
    # тип устройства:
    const DEVICE_DESKTOP = 'desktop';
    const DEVICE_TABLET  = 'tablet';
    const DEVICE_PHONE   = 'phone';

    /**
     * Инициализация приложения
     * @return void
     */
    public function init()
    {
        # переопределяем базовые компоненты
        static::autoloadEx(array(
            'User' => array('app', 'app/user.php'),
            'Hooks' => array('app', 'app/hooks.php'),
        ));

        #mastiff autoload
        require_once PATH_BASE . '/mastiff/autoload.php';
        require_once PATH_BASE . '/mastiffApp/db/config/database.php';

        # Инициализируем base\app
        parent::init();

        # Yandex Карты 2.1
        Geo::$ymapsCoordOrder = 'latlong';
        Geo::$ymapsDefaultCoords = '55.7481,37.6206';
        Geo::$ymapsJS = Request::scheme().'://api-maps.yandex.ru/2.1/?lang=ru_RU';

        if (bff::cron()) {
            return;
        }

        # Middleware
        $this->_middleware = static::filter('app.middleware', array(
            'Offline'    => ['callback'=>\app\middleware\Offline::class,    'admin'=>false, 'priority'=>10],
            'RememberMe' => ['callback'=>\app\middleware\RememberMe::class, 'admin'=>false, 'priority'=>20],
        ));

        # подключаем Javascript + CSS
        tpl::includeJS(array('jquery', 'bff'), true);
        if (!bff::adminPanel()) {
            # для фронтенда
            tpl::includeJS(array('bootstrap3.min'), false);
            js::setDefaultPosition(js::POS_FOOT); # переносим все инициализируемые inline-скрипты в footer
            tpl::includeCSS('custom');
            tpl::includeJS(array('app'), false, 15);
            self::$userSettings = static::DI('input')->cookie(static::cookiePrefix() . 'usett');
        } else {
            # для админки
            tpl::includeJS(array('bootstrap.min'), false);
            tpl::includeJS(array('admin/bff', 'fancybox'), true);
        }

        if (($userID = User::id())) {
            # актуализируем "Время последней активности" пользователя
            if ((BFF_NOW - func::SESSION('last_activity', 0)) >= config::sys('users.activity.timeout', 600)) {
                Users::model()->userSave($userID, false, array('last_activity' => static::DI('database')->now()));
                func::setSESSION('last_activity', BFF_NOW);
            }
            # актуализируем счетчики пользователя
            static::DI('security')->userCounter(null);
        }
    }

    public static function isIndex()
    {
        return parent::isIndex() || ((!empty($_GET['index']) && SEO::landingPage() === false) || Request::uri('/') === '/');
    }



    public static function setActiveMenu($sPath, $bUpdateMeta = true, $mActiveStateData = 1)
    {
        if (Request::isAJAX()) {
            return;
        }
        $sPath = str_replace('//', '/main/', $sPath);
        Sitemap::i()->setActiveMenuByPath($sPath, $bUpdateMeta, $mActiveStateData);
    }

    /**
     * Проверка / сохранение типа текущего устройства:
     * > if( bff::device(bff::DEVICE_DESKTOP) ) - проверяем, является ли текущее устройство DESKTOP
     * > if( bff::device(array(bff::DEVICE_DESKTOP,bff::DEVICE_TABLET)) ) - проверяем, является ли текущее устройство DESKTOP или TABLET
     * > $deviceID = bff::device() - получаем текущий тип устройства
     * > bff::device(bff::DEVICE_DESKTOP, true) - сохраняем тип текущего устройства
     * @param string|array|bool $device ID устройства (self::DEVICE_), ID нескольких устройств или FALSE
     * @param bool $set true - сохраняем тип текущего устройства
     * @return bool|int
     */
    public static function device($device = 0, $set = false)
    {
        static $detected;
        $cookieKey = static::cookiePrefix() . 'device';

        # получаем тип устройства
        if (!$set) {
            if (!isset($detected)) {
                $detected = static::input()->cookie($cookieKey, TYPE_STR);
                if (empty($detected) || !in_array($detected, array(
                        self::DEVICE_DESKTOP, self::DEVICE_TABLET, self::DEVICE_PHONE), true)
                   ) {
                    $detected = static::deviceDetector();
                }
            }
            if (!empty($device)) {
                # для desktop загружаем весь контент (эмулируем все устройства)
                if (static::deviceDetector(self::DEVICE_DESKTOP)) {
                    return true;
                }

                return (is_string($device) ? $detected === $device :
                    (is_array($device) ? in_array($detected, $device, true) :
                        false));
            } else {
                return $detected;
            }
        } # устанавливаем тип устройства
        else {
            if (empty($device) || is_array($device) || !in_array($device, array(
                        self::DEVICE_DESKTOP,
                        self::DEVICE_TABLET,
                        self::DEVICE_PHONE
                    )
                )
            ) {
                $device = static::deviceDetector();
            }
            if ($device !== static::input()->cookie($cookieKey, TYPE_STR)) {
                unset($detected);
                setcookie($cookieKey, $device, time() + 604800, '/', '.' . SITEHOST);
                $_COOKIE[$cookieKey] = $device;
            }
        }
    }

    /**
     * Алгоритм расчета изменения рейтинга
     * @param string $sType тип события
     * @param mixed $mExtra данные для расчета
     * @return float|int
     */
    public static function getRatingAmount($sType, $mExtra)
    {
        $result = 0;
        switch($sType)
        {
            # отзывы пользователю
            case 'opinion':
            {
                $nStatus = (int)$mExtra;
                switch($nStatus)
                {
                    case Opinions::TYPE_POSITIVE: $result = config::sysAdmin('users.rating.opinions.positive', 10, TYPE_INT);  break;
                    case Opinions::TYPE_NEGATIVE: $result = config::sysAdmin('users.rating.opinions.negative', -10, TYPE_INT); break;
                    case Opinions::TYPE_NEUTRAL:  $result = config::sysAdmin('users.rating.opinions.neutral', 0, TYPE_INT);    break;
                }
            } break;
        }
        return bff::filter('rating.amount', $result, $sType, $mExtra);
    }

    public static function servicesEnabled()
    {
        if (bff::adminPanel()) {
            return bff::moduleExists('svc', false);
        } else {
            return config::sys('services.enabled', false, TYPE_BOOL) && bff::moduleExists('svc', false);
        }
    }

    /**
     * Проверка включения Безопасной сделки
     * @return bool
     */
    public static function fairplayEnabled()
    {
        return config::sysAdmin('orders.opinions', false, TYPE_BOOL) && (config::sysAdmin('fairplay.enabled', false, TYPE_BOOL)
            || bff::adminPanel()) && bff::moduleExists('fairplay');
    }
}

bff::i()->init();

# объявляем константы типа текущего устройства пользователя
define('DEVICE_DESKTOP', bff::device(bff::DEVICE_DESKTOP));
define('DEVICE_TABLET', bff::device(bff::DEVICE_TABLET));
define('DEVICE_PHONE', bff::device(bff::DEVICE_PHONE));
define('DEVICE_DESKTOP_OR_TABLET', DEVICE_DESKTOP || DEVICE_TABLET);
define('DEVICE_TABLET_OR_PHONE', DEVICE_TABLET || DEVICE_PHONE);