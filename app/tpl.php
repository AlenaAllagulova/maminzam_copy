<?php

abstract class tpl_ extends \bff\base\tpl
{
    const PGN_COMPACT = 'pagination.compact';

    public static function formatPrice($price, $decPoint = ',')
    {
        static $format;
        if (!isset($format)) $format = config::sysAdmin('format.price', false, TYPE_BOOL);
        if (!$format) return $price;
        $price = floatval($price);
        if (fmod($price, 1) > 0) {
            $price = number_format($price, 2, $decPoint, ($price >= 1000 ? ' ' : ''));
        } else {
            $price = number_format($price, 0, '', ($price >= 1000 ? ' ' : ''));
        }
        return $price;
    }

    public static function date_format_pub($mDatetime, $sFormat = 'H:i, d.m.y')
    {
        if (is_string($mDatetime)) {
            $mDatetime = strtotime($mDatetime);
        }

        return date($sFormat, $mDatetime);
    }

    public static function date_format3($sDatetime, $sFormat = false)
    {
        //get datetime
        if (!$sDatetime) {
            return '';
        }
        $date = func::parse_datetime($sDatetime);

        if ($sFormat !== false) {
            return date($sFormat, mktime($date['hour'], $date['min'], 0, $date['month'], $date['day'], $date['year']));
        }

        //get now
        $now = array();
        list($now['year'], $now['month'], $now['day']) = explode(',', date('Y,m,d'));

        //дата позже текущей
        if ($now['year'] < $date['year']) {
            return '';
        }

        if ($now['year'] == $date['year'] && $now['month'] == $date['month']) {
            if ($now['day'] == $date['day']) {
                return _t('', 'сегодня') . " {$date['hour']}:{$date['min']}";
            } else {
                if ($now['day'] == $date['day'] - 1) {
                    return _t('', 'вчера') . " {$date['hour']}:{$date['min']}";
                }
            }
        }

        return "{$date['day']}.{$date['month']}.{$date['year']} в {$date['hour']}:{$date['min']}";
    }

    public static function date_format3_month($sDatetime, $sFormat = false)
    {
        $aShortMonths = explode(',', _t('view', 'янв,фев,мар,апр,май,июн,июл,авг,сен,окт,ноя,дек'));

        //get datetime
        if (!$sDatetime) {
            return '';
        }
        $date = func::parse_datetime($sDatetime);

        if ($sFormat !== false) {
            return date($sFormat, mktime($date['hour'], $date['min'], 0, $date['month'], $date['day'], $date['year']));
        }

        //get now
        $now = array();
        list($now['year'], $now['month'], $now['day']) = explode(',', date('Y,m,d'));

        //дата позже текущей
        if ($now['year'] < $date['year']) {
            return '';
        }

        if ($now['year'] == $date['year'] && $now['month'] == $date['month']) {
            if ($now['day'] == $date['day']) {
                return _t('', 'сегодня') . " {$date['hour']}:{$date['min']}";
            } else {
                if ($now['day'] == $date['day'] - 1) {
                    return _t('', 'вчера') . " {$date['hour']}:{$date['min']}";
                }
            }
        }

        return "{$date['day']} {$aShortMonths[$date['month'] - 1]}. {$date['year']}, {$date['hour']}:{$date['min']}";
    }

    /**
     * Формирование строки с описанием прошедшего времени от даты {$mDatetime}
     * @param string|integer $mDatetime дата
     * @param bool $getTime добавлять время
     * @param bool $addBack добавлять слово "назад"
     * @param bool $bOneWord только одно слово
     * @param array $aExtra доп параметры
     * @return string
     */
    public static function date_format_spent($mDatetime, $getTime = false, $addBack = true, $bOneWord = true, $aExtra = array())
    {
        # локализация
        static $lng;
        if (!isset($lng)) {
            switch (LNG) {
                case 'en':
                    $lng = array(
                        's'     => explode(';', _t('','second;seconds;seconds')),
                        'min'   => explode(';', _t('','minute;minutes;minutes')),
                        'h'     => explode(';', _t('','hour;hours;hours')),
                        'd'     => explode(';', _t('','day;days;days')),
                        'mon'   => explode(';', _t('','month;months;months')),
                        'y'     => explode(';', _t('','year;years;years')),
                        'now'   => _t('','now'),
                        'today' => _t('','today'),
                        'yesterday' => _t('','yesterday'),
                        'back'  => _t('','ago'),
                    );
                    break;
                case 'ru':
                    $lng = array(
                        's' => 'секунда;секунды;секунд',
                        'min' => 'минута;минуты;минут',
                        'h' => 'час;часа;часов',
                        'd' => 'день;дня;дней',
                        'mon' => 'месяц;месяца;месяцев',
                        'y' => 'год;года;лет',
                        'now' => 'сейчас',
                        'today' => 'сегодня',
                        'yesterday' => 'вчера',
                        'back' => 'назад',
                    );
                    break;
                case 'ua':
                default:
                    $lng = array(
                        's' => 'секунда;секунди;секунд',
                        'min' => 'хвилина;хвилини;хвилин',
                        'h' => 'година;години;годин',
                        'd' => 'день;дні;днів',
                        'mon' => 'місяць;місяці;місяців',
                        'y' => 'рік;роки;років',
                        'now' => 'зараз',
                        'today' => 'сьогодні',
                        'yesterday' => 'вчора',
                        'back' => 'тому',
                    );
                    break;
            }
        }

        if( ! empty($aExtra['lng'])) {
            foreach ($lng as $k => $v) {
                if (isset($aExtra['lng'][$k])) {
                    $lng[$k] = $aExtra['lng'][$k];
                }
            }
        }

        # проверяем дату
        if (!$mDatetime) return '';

        $dtFrom = date_create($mDatetime);
        $dtTo = date_create();
        # дата позже текущей
        if ($dtFrom > $dtTo)
            return '';

        # считаем разницу
        $interval = date_diff($dtFrom, $dtTo);
        if ($interval === false) return '';
        $since = array(
            'year'  => $interval->y,
            'month' => $interval->m,
            'day'   => $interval->d,
            'hour'  => $interval->h,
            'min'   => $interval->i,
            'sec'   => $interval->s,
        );

        $text = '';
        $allowBack = true;
        do {
            # разница в год и более (X лет [X месяцев])
            if ($since['year']) {
                $text .= $since['year'] . ' ' . static::declension($since['year'], $lng['y'], false);
                if ( ! $bOneWord && $since['month']) {
                    $text .= ' ' . $since['month'] . ' ' . static::declension($since['month'], $lng['mon'], false);
                }
                break;
            }
            # разница в месяц и более (X месяцев [X дней])
            if ($since['month']) {
                $text .= $since['month'] . ' ' . static::declension($since['month'], $lng['mon'], false);
                if ( ! $bOneWord && $since['day'])
                    $text .= ' ' . $since['day'] . ' ' . static::declension($since['day'], $lng['d'], false);
                break;
            }
            # разница в день и более  (X дней [X часов])
            if ($since['day']) {
                if ($getTime) {
                    $text .= $since['day'] . ' ' . static::declension($since['day'], $lng['d'], false);
                    if ( ! $bOneWord && $since['hour'] > 0) {
                        $text .= ' ' . $since['hour'] . ' ' . static::declension($since['hour'], $lng['h'], false);
                    }
                } else {
                    if ($since['day'] == 1) {
                        $text = $lng['yesterday'];
                        $allowBack = false;
                    } else {
                        $text .= $since['day'] . ' ' . static::declension($since['day'], $lng['d'], false);
                    }
                }
                break;
            }

            if ($getTime) {
                # разница в час и более  (X часов [X минут])
                if ($since['hour']) {
                    $text .= $since['hour'] . ' ' . static::declension($since['hour'], $lng['h'], false);
                    if ( ! $bOneWord && $since['min']) {
                        $text .= ' ' . $since['min'] . ' ' . static::declension($since['min'], $lng['min'], false);
                    }
                    break;
                }

                # разница более 3 минут (X минут)
                if ($since['min'] > 3) {
                    $text = $since['min'] . ' ' . static::declension($since['min'], $lng['min'], false);
                } else {
                    $text = $lng['now']; # сейчас
                    $allowBack = false;
                }
            } else {
                if (intval($dtTo->format('d')) > intval($dtFrom->format('d'))) {
                    $text = $lng['yesterday']; # вчера
                } else {
                    $text = $lng['today']; # сегодня
                }
                $allowBack = false;
            }

        } while (false);

        return $text . ($addBack && $allowBack ? ' ' . $lng['back'] : '');
    }

    /**
     * Формирование строки с описанием прошедшего времени(в месяцах) от даты {$mDatetime}
     * @param string|integer $mDatetime дата
     * @return string
     */
    public static function date_spent_month($mDatetime)
    {
        # локализация
        static $lng;

        if (!isset($lng)) {
            switch (LNG) {
                case 'en':
                    $lng = array(
                        'mon'   => explode(';', _t('','month;months;months')),
                        'year'  => explode(';', _t('','year;years;years')),
                    );
                    break;
                case 'ru':
                    $lng = array(
                        'mon' => 'месяц;месяца;месяцев',
                        'year' => 'год;года;лет',
                    );
                    break;
                case 'ua':
                default:
                    $lng = array(
                        'mon' => 'місяць;місяці;місяців',
                        'year' => 'рік;роки;років',
                    );
                    break;
            }
        }

        # проверяем дату
        if (!$mDatetime) return '';

        $dtFrom = date_create($mDatetime);
        $dtTo = date_create();
        # дата позже текущей
        if ($dtFrom > $dtTo)
            return '';

        # считаем разницу
        $interval = date_diff($dtFrom, $dtTo);
        if ($interval === false) return '';
        $since = array(
            'year'  => $interval->y,
            'month' => $interval->m,
            'day'   => $interval->d,
            'hour'  => $interval->h,
            'min'   => $interval->i,
            'sec'   => $interval->s,
        );
        $year = $since['year'];
        $month = $since['month'];

        if($month > 0 || $year > 0){
            return ($year > 0 ? $year.' '.static::declension($year, $lng['year'], false).' '  : '').
                   ($month > 0 ? $month . ' ' . static::declension($month, $lng['mon'], false) : '');
        }
        return _t('', 'меньше месяца');
    }

    public static function datePublicated($mDatetime, $sDateFormat = 'Y-m-d H:i:s', $bTime = true, $sSeparator = '<br />')
    {
        static $now, $lng;
        if (!isset($now)) {
            $now = array();
            list($now['year'], $now['month'], $now['day']) = explode(',', date('Y,m,d'));
            $now = array_map('intval', $now);
            $lng = array(
                'today'     => _t('', 'Сегодня'),
                'yesterday' => _t('', 'Вчера'),
            );
        }
        if (!is_string($mDatetime)) {
            $mDatetime = date($sDateFormat, $mDatetime);
        }

        $date = date_parse_from_format($sDateFormat, $mDatetime);
        if (!empty($date['error_count'])) {
            return '';
        }

        if ($now['month'] == $date['month'] && $now['year'] == $date['year']) {
            if ($now['day'] == $date['day']) { # сегодня
                return $lng['today'] . ($bTime ? $sSeparator . sprintf('%01d:%02d', $date['hour'], $date['minute']) : '');
            } else {
                if ($now['day'] == $date['day'] - 1) { # вчера
                    return $lng['yesterday'] . ($bTime ? $sSeparator . sprintf('%01d:%02d', $date['hour'], $date['minute']) : '');
                }
            }
        }

        return $date['day'] . ' ' . \bff::locale()->getMonthTitle($date['month']) . ($now['year'] != $date['year'] ? $sSeparator . $date['year'] : '');
    }

    public static function getBreadcrumbs(array $aCrumbs = array(), $bActiveIsLink = false)
    {
        $aData = array(
            'crumbs'         => $aCrumbs,
            'active_is_link' => $bActiveIsLink,
        );

        return View::template('breadcrumbs', $aData);
    }

    public static function getTabs(array $aData = array())
    {
        $aData = $aData + array('tabs' => array(), 'a' => '', 'class' => '');

        return View::template('tabs', $aData);
    }

    /**
     * Ссылка на контакт
     * @param array $aContact
     * @param bool $bAnchorOnly только ссылка
     * @return string
     */
    public static function linkContact($aContact, $bAnchorOnly = false)
    {
        if (empty($aContact['t']) || empty($aContact['v'])) {
            return '';
        }
        $aTypes = Users::aContactsTypes();
        if (empty($aTypes[$aContact['t']])) {
            return '';
        }
        $t = $aTypes[$aContact['t']];
        switch ($aContact['t']) {
            case Users::CONTACT_TYPE_PHONE:
            case Users::CONTACT_TYPE_SKYPE:
            case Users::CONTACT_TYPE_ICQ:
                return $bAnchorOnly ?
                    $aContact['v'] :
                    '<i class="fa fa-' . $t['c'] . '"></i> ' . $aContact['v'];
            case Users::CONTACT_TYPE_EMAIL:
                return $bAnchorOnly ?
                    '<a href="mailto:' . $aContact['v'] . '" rel="nofollow">' . $aContact['v'] . '</a>' :
                    '<i class="fa fa-' . $t['c'] . '"></i> <a href="mailto:' . $aContact['v'] . '" rel="nofollow">' . $aContact['v'] . '</a>';
            case Users::CONTACT_TYPE_SITE:
                return $bAnchorOnly ?
                    '<a href="' . $aContact['v'] . '" target="_blank" rel="nofollow">' . static::truncate($aContact['v'], config::sysAdmin('app.tpl.link.anchor.truncate', 100, TYPE_UINT), '...', true) . '</a>' :
                    '<i class="fa fa-' . $t['c'] . '"></i> <a href="' . $aContact['v'] . '" target="_blank" rel="nofollow">' . static::truncate($aContact['v'], config::sysAdmin('app.tpl.link.truncate', 30, TYPE_UINT), '...', true) . '</a>';
        }

        return $bAnchorOnly ?
            $aContact['v'] :
            '<i class="fa fa-' . $t['c'] . '"></i> ' . $aContact['v'];
    }

    /**
     * HTML ссылка на профиль пользователя
     * @param array $user @ref данные о пользователе
     * @param string $type тип ссылки, доступные варианты указываются строкой с разделителем " ":
     *  text - без ссылки,
     *  strong - <strong>,
     *  no-login - не указывать логин,
     *  no-surname - не указывать фамилию,
     *  icon - иконка перед ссылкой
     * @param string $tab раздел профиля
     * @param array $attr аттрибуты для тега <a>
     * @return string
     */
    public static function userLink(array &$user, $type = '', $tab = '', array $attr = array())
    {
        foreach (array('name','surname','login','pro','verified') as $k) {
            if (!isset($user[$k])) $user[$k] = '';
        }

        # verified
        $verified = '';
        if (strpos($type, 'no-verified') === false) {
            if (Users::verifiedEnabled()){
                if ($user['verified'] == Users::VERIFIED_STATUS_APPROVED) {
                    $verified = ' <i class="fa fa-check-circle show-tooltip c-verified" data-toggle="tooltip" data-placement="top" data-container="body" title="' . _t('users', 'Подтвержденный профиль') . '" data-original-title="' . _t('users', 'Подтвержденный профиль') . '"></i>';
                }
            }
        }

        # pro
        $pro = '';
        if(strpos($type, 'no-pro') === false) {
            $pro = (!empty($user['pro']) ? ' <span class="pro">pro</span>' : '');
        }

        # surname
        $surname = ( strpos($type,'no-surname') !== false || empty($user['surname']) ? '' : ' '.$user['surname'] );

        # <a>
        $prefA = '';
        $postA = '';
        if (strpos($type, 'text') === false) {
            $attr['href'] = Users::url('profile', array('login'=>$user['login'], 'tab'=>$tab));
            $prefA = '<a '.HTML::attributes($attr).'>';
            $postA = '</a>';
        }

        # strong
        $prefTag = '';
        $postTag = '';
        if (strpos($type, 'strong') !== false) {
            $prefTag = '<strong>';
            $postTag = '</strong>';
        }

        # login
        $login = ' ['.$user['login'].']';
        if (isset($user['login_title'])) {
            $login = ' ['.$user['login_title'].']';
        }
        if (strpos($type, 'no-login') !== false) {
            if ( ! empty($user['name']) || ! empty($surname)) {
                $login = '';
            }
        }

        # icon
        $icon = '';
        if (strpos($type, 'icon') !== false) {
            $icon = '<i class="fa fa-user c-link-icon"></i>';
        }

        return $prefA.$prefTag.$icon.$user['name'].$surname.$login.$postTag.$postA.$pro.$verified;
    }

    /**
     * ALT для аватара пользователя
     * @param array $user @ref данные о пользователе
     * @return string HTML
     */
    public static function avatarAlt(array &$user = array())
    {
        if (empty($user)) return '';

        $alt = array();
        if (!empty($user['name'])) {
            $alt[] = $user['name'];
        }
        if (!empty($user['surname'])) {
            $alt[] = $user['surname'];
        }
        if (empty($alt) && !empty($user['login'])) {
            $alt[] = $user['login'];
        }
        return HTML::escape(join(' ', $alt));
    }

    /**
     * Выводим аватар пользователя
     * @param array $user @ref данные о пользователе
     * @return string HTML
     */
    public static function userAvatar(array &$user = array())
    {
        $src = UsersAvatar::url($user['user_id'], $user['avatar'], UsersAvatar::szNormal, $user['sex']);
        return '<img src="'.$src.'" class="img-circle" alt="'.static::avatarAlt($user).'" /> '.static::userOnline($user);
    }

    /**
     * Online статус пользователя
     * @param array $user @ref данные о пользователе
     * @return string HTML
     */
    public static function userOnline(array &$user = array(), $classes = '')
    {
        if (isset($user['last_activity']) && $user['last_activity'] != '0000-00-00 00:00:00') {
            if (Users::isOnline($user['last_activity'])) {
                return '<span class="c-status '.$classes.' c-status-online show-tooltip" data-container="body" title="'._te('users', 'онлайн').'"></span>';
            } else {
                return '<span class="c-status '.$classes.' c-status-offline show-tooltip" data-container="body" title="'._te('users', 'был [spent]', array('spent' => static::date_format_spent($user['last_activity'], true, true))).'"></span>';
            }
        }
        return '';
    }

    /**
     * ALT данные изображения
     * @param array $data
     * @return array|string
     */
    public static function imageAlt($data = array())
    {
        if (empty($data)) return '';

        $alt = array();
        if (!empty($data['t'])) {
            $alt[] = $data['t'];
        }
        if (isset($data['k'])) {
            $alt[] = _t('', 'изображение [num]', array('num'=>$data['k'] + 1));
        }
        return HTML::escape(join(' ', $alt));
    }

    /**
     * HTML блок с отзывами о пользователе
     * @param string $counters значение отзывов разделенных ";" положительных;нейтральных;отрицательных
     * @param array $user параметры пользователя: login
     * @param bool $noLinks только текст, без ссылок на пользователя
     * @return string HTML
     */
    public static function opinions($counters, array $user, $noLinks = false)
    {
        if ( ! isset($user['login'])) $user['login'] = '';

        if ( ! $counters) {
            $data = array(0,0,0);
        } else {
            $data = explode(';', $counters, 3);
            if ( ! isset($data[1])) $data[1] = 0;
            if ( ! isset($data[2])) $data[2] = 0;
        }
        if ($noLinks) {
            $html = '<span class="o-good">+'.$data[0].'</span> / ';
            $html .= '<span>'.$data[1].'</span> / ';
            $html .= '<span class="o-bad">-'.$data[2].'</span>';
            return $html;
        }

        $url = Opinions::url('user.listing', array('login' => $user['login']));
        $html = '<a class="o-good" href="'.$url.'?t='.Opinions::TYPE_POSITIVE.'">+'.$data[0].'</a> / ';
        $html .= '<a href="'.$url.'?t='.Opinions::TYPE_NEUTRAL.'">'.$data[1].'</a> / ';
        $html .= '<a class="o-bad" href="'.$url.'?t='.Opinions::TYPE_NEGATIVE.'">-'.$data[2].'</a>';
        return $html;
    }

    /**
     * Форматирование текста о "причине блокировки"
     * @param string $content
     * @return string
     */
    public static function blockedReason($content)
    {
        $parser = new \bff\utils\LinksParser();
        return nl2br($parser->parse($content));
    }

    /**
     * Форматируем дату к виду: "1 января 2011[, 11:20]"
     * @param mixed $mDatetime дата: integer, string - 0000-00-00[ 00:00:00]
     * @param boolean $getTime добавлять время
     * @param boolean $bSkipYearIfCurrent опускать год, если текущий
     * @param string $glue1 склейка между названием месяца и годом (если не опускается)
     * @param string $glue2 склейка между датой и временем (если добавляется)
     * @param boolean $bSkipYear всегда опускать год
     * @return string
     */
    public static function date_format2($mDatetime, $getTime = false, $bSkipYearIfCurrent = false, $glue1 = ' ', $glue2 = ', ', $bSkipYear = false)
    {
        static $months;
        if (!isset($months)) $months = \bff::locale()->getMonthTitle();

        if (!$mDatetime) {
            if (is_string($bSkipYearIfCurrent)) return $bSkipYearIfCurrent;

            return false;
        }
        $res = \func::parse_datetime((is_int($mDatetime) ? date('Y-m-j H:i:s', $mDatetime) : $mDatetime));
        $currentYear = date('Y', time()) == $res['year'];

        return intval($res['day']) . ' ' . $months[intval($res['month'])] . ($bSkipYear === true || ($bSkipYearIfCurrent === true && $currentYear) ? '' : $glue1 . $res['year']) .
        ( $currentYear ? ($getTime ? !(int)$res['hour'] && !(int)$res['min'] ? '' : $glue2 . $res['hour'] . ':' . $res['min'] : '') : '');
    }

}