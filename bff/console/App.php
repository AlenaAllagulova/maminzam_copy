<?php namespace bff\console;

/**
 * Консоль: класс приложения
 * @version 0.21
 * @modified 26.sep.2017
 */

use Symfony\Component\Console\Application;

class App extends Application
{
    public function __construct($version = BFF_VERSION)
    {
        parent::__construct('Tamaranga '.mb_strtoupper(BFF_PRODUCT).' - https://tamaranga.com.', $version);

        $this->addCommands(\bff::filter('app.console.commands.list', array(
            new commands\Maintenance(),
            new commands\migrations\Migrate(),
            new commands\migrations\Rollback(),
        )));
    }
}