<?php
    use bff\db\Publicator;

    /** @var $this Publicator */
    $params = $this->getSettings( array('use_wysiwyg', 'use_reformator', 'photo_wysiwyg', 'photo_align', 'photo_zoom', 'gallery_photos_limit', 'gallery_inline') );
    $params['name'] = bff::$class;

    if ($params['use_wysiwyg']) {
        tpl::includeJS('wysiwyg', true);
        if ($params['use_reformator']) {
            tpl::includeJS('reformator/reformator', true);
        }
    }

    # мультиязычность
    $aLangs = $this->langs;
    $useLangs = !empty($aLangs);
    $escapeWySpecials = function(&$text)
    {
        if (!isset($text)) return $text; // enc
        if (mb_stripos($text, '<script')!==false) {
            $text = preg_replace("/\<script/sui", '&lt;script', $text);
            $text = preg_replace("/\<\/script>/sui", '&lt;/script&gt;', $text);
        }
        if (mb_stripos($text, '<iframe')!==false) {
            $text = preg_replace("/\<iframe/sui", '&lt;iframe', $text);
            $text = preg_replace("/\<\/iframe>/sui", '&lt;/iframe&gt;', $text);
        }
        return $text;
    };

    $useMap = (empty($controls) || in_array(Publicator::blockTypeMap, $controls));
    $defLon = $defLat = 0;
    if ($useMap) {
        Geo::mapsAPI(true);
        $coord = explode(',', Geo::$ymapsDefaultCoords);
        $defLat = trim($coord[0]);
        $defLon = trim($coord[1]);
    }

    $index = mt_rand(1,1000);
?>
<div id="j-publicator-<?= $index ?>">
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3">
            <h5><?= _t('publicator', 'Конструктор'); ?></h5>
            <div class="help-block">
                <?= _t('publicator', 'Добавляйте любые доступные элементы и манипулируйте их позициями как угодно.'); ?>
            </div>
        </div>
    </div>
    <div class="j-publicator-data hidden">
        <?php if( ! empty($content['b'])){ foreach($content['b'] as $v){
            ?><div class="block"><div class="type"><?= $v['type']; ?></div><?php
            switch($v['type']) {
                case Publicator::blockTypeText:
                {
                    ?><div class="text"><?php if( ! isset($v['text'])) continue;
                    if($useLangs) {
                        foreach($v['text'] as $k=>$lv) { ?><div class="<?= $k ?>"><?= $escapeWySpecials($lv) ?></div><?php }
                    } else {
                        echo $escapeWySpecials($v['text']);
                    } ?></div><?php
                } break;
                case Publicator::blockTypeTextMD:
                {
                    ?><div class="text"><?php if( ! isset($v['text_edit'])) continue;
                    if ($useLangs) {
                        foreach($v['text_edit'] as $k=>$lv) { ?><div class="<?= $k ?>"><?= HTML::escape($lv) ?></div><?php }
                    } else {
                        echo HTML::escape($v['text_edit']);
                    } ?></div><?php
                } break;
                case Publicator::blockTypeQuote:
                {
                    ?><div class="text"><?php if( ! isset($v['text'])) continue;
                    if($useLangs) {
                        foreach($v['text'] as $k=>$lv) { ?><div class="<?= $k ?>"><?= $escapeWySpecials($lv) ?></div><?php }
                    } else {
                        echo $escapeWySpecials($v['text']);
                    } ?></div><?php
                } break;
                case Publicator::blockTypePhoto:
                {
                    ?><div class="photo"><?= $v['photo']; ?></div><?php
                    foreach ($v['url'] as $uk=>$uv) { ?><div class="url_<?= $uk ?>"><?= $uv ?></div><?php }
                    ?><div class="align"><?= ( ! empty($v['align']) ? $v['align'] : 'center'); ?></div><?php
                    ?><div class="zoom"><?=  ( ! empty($v['zoom']) ? 1 : 0); ?></div><?php
                    ?><div class="view"><?=  ( ! empty($v['view']) ? $v['view'] : ''); ?></div><?php
                    ?><div class="text"><?php if( ! isset($v['text'])) continue;
                    if ($useLangs) {
                        foreach($v['text'] as $k=>$lv) { ?><div class="<?= $k ?>"><?= $lv ?></div><?php }
                    } else {
                        echo $v['text'];
                    } ?></div><?php
                } break;
                case Publicator::blockTypeGallery:
                {
                    # htmlspecialchars_decode для 'alt'
                    if (!empty($v['p'])) {
                        foreach ($v['p'] as $gk=>&$gv) {
                            if (!empty($gv['alt'])) {
                                foreach ($gv['alt'] as $gv_alt_k=>$gv_alt_v) {
                                    if (!empty($gv_alt_v)) {
                                        $gv['alt'][$gv_alt_k] = htmlspecialchars_decode($gv_alt_v);
                                    }
                                }
                            }
                        } unset ($gv);
                    }
                    ?><div class="photos"><?= func::php2js($v['p']); ?></div><?php
                    ?><div class="inline_link"><?= ( ! empty($v['inline_link']) ? 1 : 0); ?></div><?php
                    ?><div class="inline_text"><?php if(isset($v['inline_text'])) {
                    if ($useLangs) {
                        if (!empty($v['inline_text'])) {
                            foreach($v['inline_text'] as $k=>$lv) { ?><div class="<?= $k ?>"><?= $lv ?></div><?php }
                        }
                    } else {
                        echo $v['inline_text'];
                    } } ?></div><?php
                } break;
                case Publicator::blockTypeVideo:
                {
                    foreach (array('source','video','embed','thumb') as $vv) {
                        if (isset($v[$vv])) {
                            ?><div class="<?= $vv ?>"><?= $v[$vv]; ?></div><?php
                        }
                    }
                } break;
                case Publicator::blockTypeSubtitle:
                {
                    ?><div class="text"><?php if( ! isset($v['text'])) continue;
                    if($useLangs) {
                        foreach($v['text'] as $k=>$lv) { ?><div class="<?= $k ?>"><?= $lv ?></div><?php }
                    } else {
                        echo $v['text'];
                    } ?></div><?php
                    ?><div class="size"><?= (!empty($v['size']) ? $v['size'] : ''); ?></div><?php
                } break;
                case Publicator::blockTypeMap:
                {
                    ?><div class="text"><?php
                    if($useLangs) {
                        foreach($v['text'] as $k=>$lv) { ?><div class="<?= $k ?>"><?= $lv ?></div><?php }
                    } else {
                        echo $v['text'];
                    } ?></div><?php
                    ?><div class="lat"><?= (!empty($v['lat']) ? $v['lat'] : ''); ?></div><?php
                    ?><div class="lon"><?= (!empty($v['lon']) ? $v['lon'] : ''); ?></div><?php
                } break;
            }
            ?></div><?php
        } } ?>
    </div>
    <div class="j-blocks"></div>
    <div class="publicator-options j-controls">
        <div class="a-options-inside">
            <?php if(empty($controls) || in_array(Publicator::blockTypeText, $controls)):     ?><a href="#" class="show-tooltip j-text" data-toggle="tooltip" data-placement="bottom" title="<?= _te('publicator', 'Текст'); ?>" data-original-title="<?= _te('publicator', 'Текст'); ?>"><i class="fa fa-align-justify"></i></a><?php endif; ?>
            <?php if(empty($controls) || in_array(Publicator::blockTypeTextMD, $controls)):   ?><a href="#" class="show-tooltip j-textmd" data-toggle="tooltip" data-placement="bottom" title="<?= _te('publicator', 'Текст'); ?>" data-original-title="<?= _te('publicator', 'Текст'); ?>"><i class="fa fa-align-justify"></i></a><?php endif; ?>
            <?php if(empty($controls) || in_array(Publicator::blockTypePhoto, $controls)):    ?><a href="javascript:;" class="show-tooltip j-photo" data-toggle="tooltip" data-placement="bottom" title="<?= _te('publicator', 'Фото'); ?>" data-original-title="<?= _te('publicator', 'Фото'); ?>"><i class="fa fa-camera"></i></a><?php endif; ?>
            <?php if(empty($controls) || in_array(Publicator::blockTypeGallery, $controls)):  ?><a href="#" class="show-tooltip j-gallery" data-toggle="tooltip" data-placement="bottom" title="<?= _te('publicator', 'Галерея'); ?>" data-original-title="<?= _te('publicator', 'Галерея'); ?>"><i class="fa fa-picture-o"></i></a><?php endif; ?>
            <?php if(empty($controls) || in_array(Publicator::blockTypeVideo, $controls)):    ?><a href="#" class="show-tooltip j-video" data-toggle="tooltip" data-placement="bottom" title="<?= _te('publicator', 'Видео'); ?>" data-original-title="<?= _te('publicator', 'Видео'); ?>"><i class="fa fa-film"></i></a><?php endif; ?>
            <?php if(empty($controls) || in_array(Publicator::blockTypeSubtitle, $controls)): ?><a href="#" class="show-tooltip j-subtitle" data-toggle="tooltip" data-placement="bottom" title="<?= _te('publicator', 'Заголовок'); ?>" data-original-title="<?= _te('publicator', 'Заголовок'); ?>"><i class="fa fa-header"></i></a><?php endif; ?>
            <?php if($useMap):                                                                ?><a href="#" class="show-tooltip j-map" data-toggle="tooltip" data-placement="bottom" title="<?= _te('publicator', 'Карта'); ?>" data-original-title="<?= _te('publicator', 'Карта'); ?>"  ><i class="fa fa-map-marker"></i></a><?php endif; ?>
        </div>
    </div>
</div>
<script type="text/javascript">
<?php js::start(); ?>
<?= ( ! empty($js_object)? ' var '.$js_object.';':''); ?>
$(function(){
    <?= ( ! empty($js_object)? $js_object.' = ':''); ?>
    bffPublicatorFrontend.init('#j-publicator-<?= $index ?>', {
        id: <?= $id; ?>, fieldname: '<?= $fieldname; ?>',
        langs: <?= ($useLangs?func::php2js($aLangs):'false'); ?>, langs_cur: '<?= LNG ?>',
        lang: <?= func::php2js(array(
            'init_error'=>_t('publicator', 'Ошибка инициализации'),
            'text_tip'=>_t('publicator', 'Добавить текст'),
            'subtitle_tip'=>_t('publicator', 'Подзаголовок'),
            'insert'=> _t('publicator', 'Вставить'),
            'del'=>_t('publicator', 'Удалить?'),
            'delete'=>_t('publicator', 'Удалить'),
            'photo_add'=>_t('publicator', 'добавить фото'),
            'photo_tip'=>_t('publicator', 'Без описания'),
            'video_tip'=>_t('publicator', 'Укажите ссылку на видео'),
            'video_submit'=>_t('publicator', 'Готово'),
            'video_supported'=>_t('publicator', 'Поддерживаются: YouTube, Vimeo, RuTube, Vkontakte'),
            'video_not_valid'=>_t('publicator', 'Неверный формат ссылки'),
            'upload_typeError'    => _t('form', 'Допустимы только следующие типы файлов: {extensions}'),
            'upload_sizeError'    => _t('form', 'Файл {file} слишком большой, максимально допустимый размер {sizeLimit}'),
            'upload_minSizeError' => _t('form', 'Файл {file} имеет некорректный размер'),
            'upload_emptyError'   => _t('form', 'Файл {file} имеет некорректный размер'),
            'upload_limitError'   => _t('form', 'Вы можете загрузить не более {limit} файлов'),
            'upload_onLeave'      => _t('form', 'Происходит загрузка изображения, если вы покинете эту страницу, загрузка будет прекращена'),
            'map_search'=>_t('publicator', 'Найти'),
            'map_tip'=>_t('publicator', 'Адрес'),
        )) ?>,
        m: <?= func::php2js($params); ?>,
        url: '<?= bff::ajaxURL($this->owner_module.'&ev='.bff::$event.'&publicator=1', '') ?>',
        map_def_lat: '<?= $defLat ?>',
        map_def_lon: '<?= $defLon ?>'
    });
});
<?php js::stop(); ?>
</script>