<?php

    # Настройки сайта (site, config)
    define('TABLE_CONFIG',                      DB_PREFIX.'config'); // настройки сайта
    define('TABLE_MIGRATIONS',                  DB_PREFIX.'migrations'); // миграции

    # Пользователи (users)
    define('TABLE_USERS',                           DB_PREFIX.'users'); // пользователи
    define('TABLE_USERS_STAT',                      DB_PREFIX.'users_stat'); // стат. данные пользователей
    define('TABLE_USERS_SOCIAL',                    DB_PREFIX.'users_social'); // соц. аккаунты пользователей
    define('TABLE_USERS_GROUPS',                    DB_PREFIX.'users_groups'); // группы пользователей
    define('TABLE_USER_IN_GROUPS',                  DB_PREFIX.'user_in_group'); // вхождение пользователей в группы
    define('TABLE_USERS_GROUPS_PERMISSIONS',        DB_PREFIX.'users_groups_permissions'); // доступ группы
    define('TABLE_USERS_BANLIST',                   DB_PREFIX.'users_banlist'); // ban-list пользователей
    define('TABLE_MODULE_METHODS',                  DB_PREFIX.'module_methods'); // module-method - схема доступа групп
    define('TABLE_USERS_SPECIALIZATIONS',           DB_PREFIX.'users_specializations'); // специализации пользователей
    define('TABLE_USERS_SPECIALIZATIONS_SERVICES',  DB_PREFIX.'users_specializations_services'); // услуги специализаций пользователей
    define('TABLE_USERS_IN_TAGS',                   DB_PREFIX.'users_in_tags'); // связь навыков с пользователями
    define('TABLE_USERS_NOTES',                     DB_PREFIX.'users_notes'); // заметки о пользователях
    define('TABLE_USERS_FAVS',                      DB_PREFIX.'users_favs'); // избранные пользователи
    define('TABLE_USERS_SVC_CAROUSEL',              DB_PREFIX.'users_svc_carousel'); // таблица услуги "карусель"
    define('TABLE_USERS_SVC_STAIRS',                DB_PREFIX.'users_svc_stairs'); // таблица услуги "лестница"
    define('TABLE_USERS_SVC_SETTINGS',              DB_PREFIX.'users_svc_settings'); // настройки услуг пользователей
    define('TABLE_USERS_VIEWS',                     DB_PREFIX.'users_views'); // статистика просмотров профиля пользователя
    define('TABLE_USERS_RATING',                    DB_PREFIX.'users_rating'); // статистика изменения рейтинга пользователей
    define('TABLE_USERS_VERIFIED_FILES',            DB_PREFIX.'users_verified_files'); // документы верификации профиля пользователя
    define('TABLE_USERS_TABS',                      DB_PREFIX.'users_tabs'); //доступные табы пользователя
    define('TABLE_USERS_SCHEDULE',                  DB_PREFIX.'users_schedule'); // график занятости пользователей

    # Регионы(страны, города, ...) (geo)
    define('TABLE_REGIONS',                     DB_PREFIX.'regions'); // регионы
    define('TABLE_REGIONS_DISTRICTS',           DB_PREFIX.'regions_districts');
    define('TABLE_REGIONS_METRO',               DB_PREFIX.'regions_metro'); // метро / ветки метро
    define('TABLE_REGIONS_GEOIP',               DB_PREFIX.'regions_geoip'); // регионы - geo ip

    # Страницы (site)
    define('TABLE_PAGES',                       DB_PREFIX.'pages'); // страницы
    define('TABLE_PAGES_LANG',                  DB_PREFIX.'pages_lang'); // страницы - lang

    # Карта сайта (sitemap)
    define('TABLE_SITEMAP',                     DB_PREFIX.'sitemap'); // карта сайта
    define('TABLE_SITEMAP_LANG',                DB_PREFIX.'sitemap_lang'); // карта сайта - lang

    # Счетчики (site)
    define('TABLE_COUNTERS',                    DB_PREFIX.'counters'); // счетчики

    # Валюты (site)
    define('TABLE_CURRENCIES',                  DB_PREFIX.'currencies'); // валюты
    define('TABLE_CURRENCIES_LANG',             DB_PREFIX.'currencies_lang'); // валюты -  lang

    # Оплата(счета), услуги (bills, svc)
    define('TABLE_BILLS',                       DB_PREFIX.'bills'); // счета
    define('TABLE_SVC',                         DB_PREFIX.'svc'); // настройки платных услуг

    # Работа с почтой - массовая рассылка (sendmail)
    define('TABLE_MASSEND',                     DB_PREFIX.'massend');
    define('TABLE_MASSEND_RECEIVERS',           DB_PREFIX.'massend_receivers');

    # Навыки
    define('TABLE_TAGS',                        DB_PREFIX.'tags');

    # Специализации
    define('TABLE_SPECIALIZATIONS',             DB_PREFIX.'specializations');
    define('TABLE_SPECIALIZATIONS_LANG',        DB_PREFIX.'specializations_lang');
    define('TABLE_SPECIALIZATIONS_DP',          DB_PREFIX.'specializations_dp');
    define('TABLE_SPECIALIZATIONS_DPM',         DB_PREFIX.'specializations_dpm');
    define('TABLE_SPECIALIZATIONS_DP_ORDERS',   DB_PREFIX.'specializations_dp_orders');
    define('TABLE_SPECIALIZATIONS_DPM_ORDERS',  DB_PREFIX.'specializations_dpm_orders');

    # Специализации: категории
    define('TABLE_SPECIALIZATIONS_CATS',        DB_PREFIX.'specializations_cats');
    define('TABLE_SPECIALIZATIONS_CATS_LANG',   DB_PREFIX.'specializations_cats_lang');
    define('TABLE_SPECIALIZATIONS_IN_CATS',     DB_PREFIX.'specializations_in_cats');
    # Специализации: услуги
    define('TABLE_SPECIALIZATIONS_SERVICES',        DB_PREFIX.'specializations_services');
    define('TABLE_SPECIALIZATIONS_SERVICES_LANG',   DB_PREFIX.'specializations_services_lang');
    define('TABLE_SPECIALIZATIONS_SERVICES_REGION', DB_PREFIX.'specializations_services_region');

    # Магазин: категории
    define('TABLE_SHOP_CATS',                   DB_PREFIX.'shop_cats');
    define('TABLE_SHOP_CATS_LANG',              DB_PREFIX.'shop_cats_lang');
    define('TABLE_SHOP_CATS_DP',                DB_PREFIX.'shop_cats_dp');
    define('TABLE_SHOP_CATS_DPM',               DB_PREFIX.'shop_cats_dpm');

    # Заказы
    define('TABLE_ORDERS_NOTES',                DB_PREFIX.'orders_notes'); // заметки о заказах
    define('TABLE_ORDERS_SCHEDULE',             DB_PREFIX.'orders_schedule'); // график работ в заказах


