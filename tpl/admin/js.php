<?php
/**
 * Подключение JavaScript файлов для админ. панели
 */
?>
<?php bff::hook('admin.js.extra'); ?>
<script type="text/javascript">//<![CDATA[
var app = {adm: true, host:'<?= SITEHOST ?>', root: '<?= SITEHOST ?>', rootStatic: '<?= SITEURL_STATIC ?>', cookiePrefix: '<?= bff::cookiePrefix() ?>'};
//]]></script>
<?php foreach(bff::filter('admin.js.includes', tpl::$includesJS) as $v) { ?>
<script src="<?= $v ?>" type="text/javascript" charset="utf-8"></script>
<?php } ?>
<script type="text/javascript">
$(function() {
    bff.map.setType('<?= Geo::mapsType() ?>');
    //init admin menu
    var colapse_process = false;
    $('#adminmenu a.main:not(a.logout)').click(function() {
        if($(this).next().is('.empty')) 
            return true;           
            
        if(colapse_process) return false; colapse_process = true;  

        if(! $(this).next().is(':visible') )
            $('#adminmenu ul.sub li:visible').parent().slideFadeToggle();
        
        $(this).next().slideFadeToggle(function(){ colapse_process = false; });
        return false;
    }).next().hide();
    $('#adminmenu a.active.main').next().show();
});

bff.extend(bff,
{
    rotateTable: function(list, url, progressSelector, callback, addParams, rotateClass, onDrop)
    {
        if(!$.tableDnD) return;

        callback    = callback || $.noop;
        rotateClass = rotateClass || 'rotate';
        addParams   = addParams || {};
        onDrop      = onDrop || $.noop;
        $(list).tableDnD({
            onDragClass: rotateClass,
            onDrop: function(table, dragged, target, position, changed)
            {
                if(changed && url!==false) {
                    if(onDrop) onDrop(addParams);
                    bff.ajax(url,
                        $.extend({ dragged : dragged.id, target : target.id, position : position }, addParams),
                        callback, progressSelector);
                }
            }
        });
    },
    orderinfo: function(itemID) {
        if(itemID) {
            $.fancybox('', {ajax:true, href:'<?= tpl::adminLink('orders_list&act=info&id=','orders') ?>'+itemID});
        }
        return false;
    }
});
</script>