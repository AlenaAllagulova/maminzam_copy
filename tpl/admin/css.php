<?php
/**
 * Подключение CSS файлов для админ. панели
 */
tpl::includeCSS('admin-bootstrap.css');
tpl::includeCSS('admin-responsive.css');
tpl::includeCSS('admin.css');
bff::hook('admin.css.extra');
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?
foreach (bff::filter('admin.css.includes', tpl::$includesCSS) as $v) {
    ?><link rel="stylesheet" href="<?= $v; ?>" type="text/css" /><?php
}