<?php
bff::hook('js.extra');
Minifier::process(tpl::$includesJS);
foreach(bff::filter('js.includes', tpl::$includesJS) as $v) { ?>
<script src="<?= $v ?>" type="text/javascript" charset="utf-8"></script>
<?php } ?>
<?

?>
<script type="text/javascript">
<? js::start(); ?>
$(function(){
    app.init({adm: false, host:'<?= SITEHOST ?>', root: '<?= bff::urlBase() ?>', rootStatic: '<?= SITEURL_STATIC ?>',
              cookiePrefix: 'bff_', regionPreSuggest: <?= Geo::regionPreSuggest() ?>, lng: '<?= LNG ?>',
    lang: <?= func::php2js(bff::filter('js.app.lang', array(
                'form_btn_loading'=>_t('', 'Подождите...'),
                'form_alert_errors'=>_t('', 'При заполнении формы возникли следующие ошибки:'),
                'form_alert_required'=>_t('', 'Заполните все отмеченные поля'),
            ))); ?>,
    mapType: '<?= Geo::mapsType() ?>',
    logined: <?= User::id() > 0 ? 'true' : 'false'; ?>,
    device: '<?= bff::device() ?>'
    });
 });
<? js::stop(true); ?>
</script>
<?= js::renderInline(js::POS_HEAD); ?>