<!DOCTYPE html>
<html class="no-js">
<head>
<?= SEO::i()->metaRender(array('content-type'=>true, 'csrf-token'=>true)) ?>
<?= View::template('css'); ?>
</head>
<body data-dbq="<?= bff::database()->statQueryCnt(); ?>">
<?= View::template('alert'); ?>
<div id="wrapper">
    <?= View::template('header.short'); ?>
	<!-- #page-wrap -->
    <div id="page-wrap">
        <?= $centerblock; ?>
	</div><!-- [end] #page-wrap -->
    <?= View::template('footer'); ?>
</div>
</body>
</html>