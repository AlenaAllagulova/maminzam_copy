<?= Site::favicon() ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" media="all" type="text/css" />
<?

?>
<link rel="stylesheet" media="all" type="text/css" href="<?= bff::url('/css/bootstrap.min.css') ?>" id="bootstrap" />
<link rel="stylesheet" media="all" type="text/css" href="<?= bff::url('/css/main.css', 2) ?>" id="global-styles" />
<?
bff::hook('css.extra');
 ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<?
Minifier::process(tpl::$includesCSS);
foreach(bff::filter('css.includes', tpl::$includesCSS) as $v) {
    ?><link rel="stylesheet" href="<?= $v; ?>" type="text/css" /><?
}