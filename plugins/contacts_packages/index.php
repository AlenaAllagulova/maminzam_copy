<?php

const TABLE_PACKAGES_CONTACTS = DB_PREFIX.'packages_contacts';
const TABLE_USERS_PACKAGES = DB_PREFIX.'users_packages';
const PACKAGES_PERIOD_DAYS = 30;
const PACKAGES_UNLIM_QUANTITY = -1; # тип пакетов с неограниченным количеством контактов



class Plugin_Contacts_packages extends Plugin
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'plugin_title'   => 'Пакеты контактов',
            'plugin_version' => '1.0.0',
            'extension_id'   => 'p2ec3fce1898821ae31f9ff47d5a2330e42ea1d7',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            //
        ));
    }

    protected function start()
    {


        bff::i()->moduleRegister('packages', $this->path('src'));

        bff::autoloadEx([
            'PluginLoaderContactsPackages' => ['app',
                'plugins' . DS . trim(str_replace(PATH_PLUGINS, '', __DIR__) . DS . 'plugin.loader.php'),
            ]]);

        # set access to the plugin object, for example PluginLoaderContactsPackages::getPluginInstance()->isEnabled()
        PluginLoaderContactsPackages::setPluginInstance($this);

        if (bff::adminPanel()){
            \bff::hooks()->javascriptExtra(false, function () {
                \tpl::includeJS([$this->url('js/dist/packages.js')],
                    false);
            });
        }

        \bff::hooks()->javascriptExtra(true, function () {
            \tpl::includeJS([$this->url('js/dist/packages.js')],
                false);
        });


    }
}