'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Packages = function () {
    function Packages() {
        _classCallCheck(this, Packages);
    }

    _createClass(Packages, null, [{
        key: 'total_cost',
        value: function total_cost() {
            var cnt = $('[name="quantity"]').val();
            var price = $('[name="price_per_one"]').val();
            $('#total_sum').html(cnt * price);
        }
    }, {
        key: 'buy_package',
        value: function buy_package(o) {
            var pack_id = $('#j-package-buy').find('[name="pack_id"]:checked');

            if (!pack_id.length) {
                app.alert.error(o.lang.select_pack);
                return;
            }

            bff.ajax(bff.ajaxURL('packages&ev=buyContactsPackage', ''), {
                pack_id: pack_id.val()
            }, function (data, errors) {
                if (data && data.success) {
                    app.alert.success(data.success_msg);
                    window.setTimeout(function () {
                        if (data.redirect) {
                            bff.redirect(data.redirect);
                        } else {
                            history.back();
                        }
                    }, 2000);
                } else {
                    app.alert.error(errors);
                }
            });
        }
    }]);

    return Packages;
}();
//# sourceMappingURL=packages.js.map
