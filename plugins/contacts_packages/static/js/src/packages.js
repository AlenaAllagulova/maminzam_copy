class Packages{

    static total_cost(){
        let cnt = $('[name="quantity"]').val();
        let price = $('[name="price_per_one"]').val();
        $('#total_sum').html(cnt*price);
    }

    static buy_package(o)
    {
        let pack_id = $('#j-package-buy').find('[name="pack_id"]:checked');

        if( ! pack_id.length){
            app.alert.error(o.lang.select_pack);
            return;
        }

        bff.ajax(bff.ajaxURL('packages&ev=buyContactsPackage', ''),
            {
                pack_id: pack_id.val(),
            },
            function (data, errors) {
                if (data && data.success) {
                    app.alert.success(data.success_msg);
                    window.setTimeout(function(){
                        if(data.redirect){
                            bff.redirect(data.redirect);
                        }else{
                            history.back();
                        }
                    }, 2000);
                }else {
                    app.alert.error(errors);
                }
            }
        );


    }

}