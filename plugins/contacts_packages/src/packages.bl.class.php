<?php

abstract class PackagesBase extends Module
{
    /** @var PackagesModel */
    var $model = null;
    var $securityKey = '87644f55730f40bb584fa28e27efc2ee';

    # доступны для покупки пакеты подписок на контакты за N дней
    public static $bAllowedPackagesUnlim = null;
    # доступны для покупки пакеты открытий N контактов в месяц
    public static $bAllowedPackagesContacts = null;
    # куплены ли у текущего пользователя пакеты подписок на контакты за N дней
    public static $bAllowedCurrentUserPackagesUnlim = null;

    /**
     * @return Packages
     */
    public static function i()
    {
        return bff::module('Packages');
    }

    /**
     * @return PackagesModel
     */
    public static function model()
    {
        return bff::model('Packages');
    }

    /**
     * Формирование URL
     * @param string $key ключ
     * @param array $opts параметры
     * @param boolean $dynamic динамическая ссылка
     * @return string
     */
    public static function url($key, array $opts = array(), $dynamic = false)
    {
        $url = $base = static::urlBase(LNG, $dynamic);
        switch ($key) {
            # Страница покупки пакетов контактов
            case 'packages_buy':
                if (!empty($opts['anchor'])) {
                    $anchor = $opts['anchor'];
                    unset($opts['anchor']);
                }
                $url .= '/packages/buy_contacts/'. (!empty($anchor) ? $anchor: '') . (!empty($opts) ? static::urlQuery($opts) : '');
                break;
        }
        return bff::filter('packages.url', $url, array('key'=>$key, 'opts'=>$opts, 'dynamic'=>$dynamic, 'base'=>$base));
    }

    /**
     * Приведение к необходимому формату данных о купленном пакете для записи в БД
     * @param array $aPackageData
     * @param boolean $bPackageUnlim - тип пакета (подписка на N дней или пакет на 30 дней)
     * @return bool
     */
    public function prepareBoughtPackageData(array $aPackageData, $bPackageUnlim)
    {
        if(empty($aPackageData)){
            return false;
        }
        $aData['pack_settings'] = serialize($aPackageData);
        if ($bPackageUnlim){
            $aData['left_quantity'] = PACKAGES_UNLIM_QUANTITY;
            $aData['end_date'] = date("Y-m-d H:i:s", strtotime($this->db->now()) + ($aPackageData['period'] * (24 * 60 * 60)));
        }else{
            $aData['left_quantity'] = $aPackageData['quantity'];
            $aData['end_date'] = date("Y-m-d H:i:s", strtotime($this->db->now()) + (PACKAGES_PERIOD_DAYS * (24 * 60 * 60)));

        }
        return $aData;
    }

    /**
     * @param array $aPackagesData
     * @return array|bool
     */
    public function prepareBoughtPackageListing(array $aPackagesData)
    {
        if(empty($aPackagesData)){
            return false;
        }
        foreach ($aPackagesData as &$package){
            $package['end_date'] = tpl::date_format2($package['end_date'], true);
            $package['pack_settings'] = unserialize($package['pack_settings']);
        }unset($package);

        return $aPackagesData;


    }

    /**
     * Доступны ли для покупки пакеты без ограничения на открытие контактов для текущего типа пользователя
     * @return bool|null
     */
    public static function isEnabledPackagesUnlim()
    {
        if (is_null(self::$bAllowedPackagesUnlim)) {

            # пакеты без ограничения на открытие контактов на Nдней доступные для покупки
            $nCurrentUserType = User::isClient() ? Users::TYPE_CLIENT : Users::TYPE_WORKER;
            $aFilter = [
                'quantity' => PACKAGES_UNLIM_QUANTITY,
                'user_type' => $nCurrentUserType,
                'enabled'   => true,
            ];
            $aDataPackagesUnlim = self::model()->packagesContactsListing($aFilter, false, '', 'P.period');

            self::$bAllowedPackagesUnlim = !empty($aDataPackagesUnlim);
        }

        return self::$bAllowedPackagesUnlim;
    }

    /**
     * Доступны ли для покупки пакеты открытия N контактов
     * @return bool|null
     */
    public static function isEnabledPackagesContacts()
    {
        if (is_null(self::$bAllowedPackagesContacts)) {
            $aDataPackages = self::model()->packagesContactsListing(['quantity > 0', 'enabled'   => true], false, '', 'P.quantity');
            self::$bAllowedPackagesContacts = !empty($aDataPackages);
        }

        return self::$bAllowedPackagesContacts;
    }

    /**
     * Куплены ли у текущего пользователя пакеты подписок на контакты за N дней
     * @return bool|null
     */
    public static function isEnabledCurrentUserPackagesUnlim()
    {
        if(is_null(self::$bAllowedCurrentUserPackagesUnlim)){
            $aDataUserPackagesUnlim = self::model()->getUserContactsPackages(User::id(), '', '', true);
            self::$bAllowedCurrentUserPackagesUnlim = !empty($aDataUserPackagesUnlim);
        }
        return self::$bAllowedCurrentUserPackagesUnlim;
    }

}