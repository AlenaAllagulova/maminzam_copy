<?php

class Packages extends PackagesBase
{

    /**
     * Пакеты контактов
     * @return string
     */
    public function packs_listing()
    {
        if (!$this->haveAccessTo('packages')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || \Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'add': {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validatePackagesData(0, $bSubmit);
                    if ($bSubmit) {
                        if ($this->errors->no()) {
                            $this->model->packageSave(0, $aData);
                        }
                    }

                    $aData['id'] = 0;
                    $aData['enabled'] = false;
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.package.form');
                }
                    break;
                case 'edit': {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nPackID = $this->input->postget('id', TYPE_UINT);

                    if (!$nPackID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {
                        $aData = $this->validatePackagesData($nPackID, $bSubmit);

                        if ($this->errors->no()) {
                            $this->model->packageSave($nPackID, $aData);
                        }
                        $aData['id'] = $nPackID;
                    } else {
                        $aData = $this->model->packageData($nPackID);

                        if (empty($aData)) {
                            $this->errors->unknownRecord();
                            break;
                        }
                    }
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.package.form');
                }
                    break;
                case 'toggle': {
                    $nPackID = $this->input->postget('id', TYPE_UINT);

                    if (!$nPackID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $sToggleType = $this->input->get('type', TYPE_STR);

                    $this->model->packageToggle($nPackID, $sToggleType);
                }
                    break;
                case 'delete': {
                    $nPackID = $this->input->postget('id', TYPE_UINT);
                    if (!$nPackID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->packageData($nPackID);
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->model->packageDelete($nPackID);

                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    }
                }
                    break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && \Request::isAJAX()) $this->ajaxResponseForm($aResponse);
        }

        # формируем фильтр списка
        $sql = ['quantity > 0' ]; # кол-во контактов
        $sqlOrder = 'P.id';

        $aData['list'] = $this->model->packagesContactsListing($sql, false, '', $sqlOrder);

        $aData['list'] = $this->viewPHP($aData, 'admin.packages.listing.ajax');

        if (\Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'list' => $aData['list'],
            ));
        }

        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        return $this->viewPHP($aData, 'admin.packages.listing');
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nPackID ID пакета контактов или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validatePackagesData($nPackID, $bSubmit)
    {
        $aData =[];

        $this->input->postm(array(
            'title'         => TYPE_STR,  # Название
            'quantity'      => TYPE_UINT, # Количество контактов в пакете
            'price_per_one' => TYPE_UINT, # Цена за один контакт в пакете
            'saving'        => TYPE_STR,  # Экономия
            'enabled'       => TYPE_BOOL, # Включен
        ), $aData);

        return $aData;
    }


    /**
     * Пакеты контактов
     * @return string
     */
    public function packs_listing_unlim()
    {
        if (!$this->haveAccessTo('packages')) {
            return $this->showAccessDenied();
        }

        $sAct = $this->input->postget('act', TYPE_STR);
        if (!empty($sAct) || \Request::isPOST()) {
            $aResponse = array();
            switch ($sAct) {
                case 'add': {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $aData = $this->validatePackagesUnlimData(0, $bSubmit);
                    if ($bSubmit) {
                        if ($this->errors->no()) {
                            $this->model->packageSave(0, $aData);
                        }
                    }

                    $aData['id'] = 0;
                    $aData['enabled'] = false;
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.package_unlim.form');
                }
                    break;
                case 'edit': {
                    $bSubmit = $this->input->post('save', TYPE_BOOL);
                    $nPackID = $this->input->postget('id', TYPE_UINT);

                    if (!$nPackID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    if ($bSubmit) {
                        $aData = $this->validatePackagesUnlimData($nPackID, $bSubmit);

                        if ($this->errors->no()) {
                            $this->model->packageSave($nPackID, $aData);
                        }
                        $aData['id'] = $nPackID;
                    } else {
                        $aData = $this->model->packageData($nPackID);

                        if (empty($aData)) {
                            $this->errors->unknownRecord();
                            break;
                        }
                    }
                    $aResponse['form'] = $this->viewPHP($aData, 'admin.package_unlim.form');
                }
                    break;
                case 'toggle': {
                    $nPackID = $this->input->postget('id', TYPE_UINT);

                    if (!$nPackID) {
                        $this->errors->unknownRecord();
                        break;
                    }

                    $sToggleType = $this->input->get('type', TYPE_STR);

                    $this->model->packageToggle($nPackID, $sToggleType);
                }
                    break;
                case 'delete': {
                    $nPackID = $this->input->postget('id', TYPE_UINT);
                    if (!$nPackID) {
                        $this->errors->impossible();
                        break;
                    }

                    $aData = $this->model->packageData($nPackID);
                    if (empty($aData)) {
                        $this->errors->impossible();
                        break;
                    }

                    $res = $this->model->packageDelete($nPackID);

                    if (!$res) {
                        $this->errors->impossible();
                        break;
                    }
                }
                    break;
                default:
                    $aResponse = false;
            }

            if ($aResponse !== false && \Request::isAJAX()) $this->ajaxResponseForm($aResponse);
        }

        # формируем фильтр списка
        $sql = ['quantity' => PACKAGES_UNLIM_QUANTITY ];
        $sqlOrder = 'P.user_type DESC';
        $aData['list'] = $this->model->packagesContactsListing($sql, false, '', $sqlOrder);

        $aData['list'] = $this->viewPHP($aData, 'admin.packages_unlim.listing.ajax');

        if (\Request::isAJAX()) {
            $this->ajaxResponseForm(array(
                'list' => $aData['list'],
            ));
        }

        $aData['id'] = $this->input->get('id', TYPE_UINT);
        $aData['act'] = $sAct;

        return $this->viewPHP($aData, 'admin.packages_unlim.listing');
    }

    /**
     * Обрабатываем параметры запроса
     * @param integer $nPackID ID пакета контактов или 0
     * @param boolean $bSubmit выполняем сохранение/редактирование
     * @return array параметры
     */
    protected function validatePackagesUnlimData($nPackID, $bSubmit)
    {
        $aData =[];

        $this->input->postm(array(
            'title'         => TYPE_STR,  # Название
            'quantity'      => TYPE_INT,  # Количество контактов в пакете - неограничено
            'period'        => TYPE_UINT, # Количество дней в пакете
            'price_per_one' => TYPE_UINT, # Цена за пакет, в данном типе пакетов
            'user_type'     => TYPE_UINT, # Тип пользователя (исполнитель/заказчик)
            'enabled'       => TYPE_BOOL, # Включен
        ), $aData);

        if ($bSubmit) {
            if ($aData['user_type'] != Users::TYPE_CLIENT && $aData['user_type'] != Users::TYPE_WORKER){
                $this->errors->set('Тип пользователя должен быть либо "исполнитель", либо "заказчик"');
                $this->errors->reloadPage();
            }

            $aData['quantity'] = PACKAGES_UNLIM_QUANTITY;
        }

        return $aData;
    }


}