<?php

class PackagesModel extends Model
{
    /** @var PackagesBase */
    var $controller;

    /**
     * Сохранение пакета контактов
     * @param integer $nPackID ID пакета контактов
     * @param array $aData данные пакета контактов
     * @return boolean|integer
     */
    public function packageSave($nPackID, array $aData)
    {

        if (empty($aData)) {
            return false;
        }

        if ($nPackID > 0) {
            $bResult = $this->db->update(TABLE_PACKAGES_CONTACTS, $aData, array('id' => $nPackID));
            return !empty($bResult);
        } else {
            $nPackID = $this->db->insert(TABLE_PACKAGES_CONTACTS, $aData);
            return $nPackID;
        }
    }

    /**
     * Список пакетов контактов
     * @param array $aFilter фильтр списка, 'quantity > 0' для пакетов на 30 дней,
     * @param bool $bCount только подсчет кол-ва
     * @param string $sqlLimit
     * @param string $sqlOrder
     * @return mixed
     */
    public function packagesContactsListing(array $aFilter, $bCount = false, $sqlLimit = '', $sqlOrder = '')
    {
        $aFilter = $this->prepareFilter($aFilter, 'P');

        if ($bCount) {
            return $this->db->one_data('SELECT COUNT(BFF.id) FROM ' . TABLE_PACKAGES_CONTACTS . ' P ' . $aFilter['where'], $aFilter['bind']);
        }

        return $this->db->select('SELECT *
                                  FROM ' . TABLE_PACKAGES_CONTACTS .' P
                                   ' . $aFilter['where']
                                . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
                                . $sqlLimit, $aFilter['bind']);
    }

    /**
     * Получаем данные пакета
     * @param $nPackID пакета контактов
     * @return mixed
     */
    public function packageData($nPackID)
    {
        if (!$nPackID) {
            return false;
        }

        return $this->db->one_array('SELECT  *
                                     FROM ' . TABLE_PACKAGES_CONTACTS . ' P
                                     WHERE id = :id',
                                    [':id' => $nPackID]);
    }

    /**
     * Переключатель доступности пакета контактов
     * @param integer $nPackID ID типа
     * @param string $sField переключаемое поле
     * @return mixed @see toggleInt
     */
    public function packageToggle($nPackID, $sField)
    {
        if ($sField) {
            return $this->toggleInt(TABLE_PACKAGES_CONTACTS, $nPackID, $sField, 'id');
        }

        return false;
    }

    /**
     * Удаление пакета контактов
     * @param integer $nPackID ID типа
     * @return boolean
     */
    public function packageDelete($nPackID)
    {
        if (empty($nPackID)) {
            return false;
        }

        $bResult = $this->db->delete(TABLE_PACKAGES_CONTACTS, array('id' => $nPackID));
        if (!empty($bResult)) {
            return true;
        }
        return false;
    }

    public function saveUserPackage($nUserID, array $aPackageData)
    {
        if (empty($nUserID) || empty($aPackageData)) {
            return false;
        }

        $aPackageData['user_id'] = $nUserID;
        return $this->db->insert(TABLE_USERS_PACKAGES, $aPackageData);
    }

    public function getUserContactsPackages($nUserID, $sqlLimit = '', $sqlOrder = '', $bPackageUnlim = false)
    {
        if (!$nUserID) {
            $nUserID = User::id();
        }

        $aFilter['user_id'] = ['user_id' => $nUserID];
        $aFilter[] = 'end_date > NOW()';
        if ($bPackageUnlim){
            $aFilter['left_quantity'] = ['left_quantity' => PACKAGES_UNLIM_QUANTITY];
        }else{
            $aFilter[] = 'left_quantity > 0';

        }
        $aFilter = $this->prepareFilter($aFilter, 'UP');

        $sqlOrder = ' UP.end_date ASC ';

        return $this->db->select('SELECT * 
                                  FROM ' . TABLE_USERS_PACKAGES . ' UP' .
                                  $aFilter['where']
                                  . (!empty($sqlOrder) ? ' ORDER BY ' . $sqlOrder : '')
                                  . $sqlLimit,
                                 $aFilter['bind']);

    }

    public function updateUserActualPackage($nCurrentUserId, $nPackageID, $bPackageUnlim = false)
    {
        if (!$nCurrentUserId || !$nPackageID){
            return false;
        }

        $aFields = ['left_quantity = left_quantity - 1'];

        if($bPackageUnlim){
            $aFields =  ['left_quantity = '.PACKAGES_UNLIM_QUANTITY];
        }
        return $this->db->update(TABLE_USERS_PACKAGES,
                                $aFields,
                                ['user_id' => $nCurrentUserId, 'id' => $nPackageID]);

    }


}