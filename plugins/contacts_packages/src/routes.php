<?php

return [
    # покупка пакета
    'package' => [
        'pattern'  => 'packages/buy_contacts(/|)',
        'callback' => 'packages/buy_page/',
        'priority' => 100,
    ],
];
