<?php

$aData = HTML::escape($aData, 'html', array('title','enabled'));
$edit = ! empty($id);

?>
<form name="PackagesForm" id="PackagesForm" action="<?= $this->adminLink(null) ?>" method="get" onsubmit="return false;">
    <input type="hidden" name="act" value="<?= ($edit ? 'edit' : 'add') ?>" />
    <input type="hidden" name="save" value="1" />
    <input type="hidden" name="id" value="<?= $id ?>" />
    <table class="admtbl tbledit">
        <tr class="required">
            <td class="row1" width="200">
                <span class="field-title">
                    <?=_t('','Название пакета контактов');?>
                    <span class="required-mark">*</span>:
                </span>
            </td>
            <td class="row2">
                <input type="text"
                       name="title"
                       value="<?= !empty($aData['title']) ? $aData['title'] : ''?>"
                />
            </td>
        </tr>
        <tr class="required">
            <td class="row1" width="200">
                <span class="field-title">
                    <?=_t('','Количество контактов');?>
                    <span class="required-mark">*</span>:
                </span>
            </td>
            <td class="row2">
                <input type="number"
                       min="1" step="1"
                       class="short"
                       name="quantity"
                       value="<?= !empty($aData['quantity']) ? $aData['quantity'] : ''?>"
                       onchange="Packages.total_cost()"/>
            </td>
        </tr>
        <tr class="required">
            <td class="row1" width="200">
                <span class="field-title">
                    <?=_t('','Цена за один контакт, [curr]:', ['curr' => Site::currencyDefault()]);?>
                    <span class="required-mark">*</span>:
                </span>
            </td>
            <td class="row2">
                <input type="number"
                       min="1" step="1"
                       class="short"
                       name="price_per_one"
                       value="<?= !empty($aData['price_per_one']) ? $aData['price_per_one'] : ''?>"
                       onchange="Packages.total_cost()"/>
            </td>
        </tr>
        <tr>
            <td class="row1" width="200">
                <span><?=_t('', 'Стоимость пакета:')?></span>
            </td>
            <td class="row2">
                <span id="total_sum"></span>
                <span><?= Site::currencyDefault()?></span>
            </td>
        </tr>
        <tr>
            <td class="row1" width="200">
                <span class="field-title" title="фраза или количественный показатель">
                    <?=_t('','Экономия: ');?>
                </span>
            </td>
            <td class="row2">
                <input type="text"
                       name="saving"
                       value="<?= !empty($aData['saving']) ? $aData['saving'] : ''?>"
                />
            </td>
        </tr>
        <tr>
            <td class="row1"><span class="field-title"><?=_t('','Включен:');?></span></td>
            <td class="row2">
                <label class="checkbox">
                    <input type="checkbox" name="enabled"<? if($enabled){ ?> checked="checked"<? } ?> />
                </label>
            </td>
        </tr>
        <tr class="footer">
            <td colspan="2">
                <input type="submit" class="btn btn-success button submit" value="<?=_t('','Сохранить');?>" onclick="jPackagesForm.save(false);" />
                <? if ($edit) { ?><input type="button" class="btn btn-success button submit" value="<?=_t('','Сохранить и вернуться');?>" onclick="jPackagesForm.save(true);" /><? } ?>
                <? if ($edit) { ?><input type="button" onclick="jPackagesForm.del(); return false;" class="btn btn-danger button delete" value="Удалить" /><? } ?>
                <input type="button" class="btn button cancel" value="Отмена" onclick="jPackagesFormManager.action('cancel');" />
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    var jPackagesForm =
        (function(){
            var $progress, $form, formChk, id = parseInt(<?= $id ?>);
            var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';
            $(function(){
                $progress = $('#PackagesFormProgress');
                $form = $('#PackagesForm');

            });
            return {
                del: function()
                {
                    if( id > 0 ) {
                        bff.ajaxDelete('sure', id, ajaxUrl+'&act=delete&id='+id,
                            false, {progress: $progress, repaint: false, onComplete:function(){
                                bff.success('Запись успешно удалена');
                                jPackagesFormManager.action('cancel');
                                jPackagesList.refresh();
                            }});
                    }
                },
                save: function(returnToList)
                {
                    if( ! formChk.check(true) ) return;
                    bff.ajax(ajaxUrl, $form.serialize(), function(data){
                        if(data && data.success) {
                            bff.success('Данные успешно сохранены');
                            if(returnToList || ! id) {
                                jPackagesFormManager.action('cancel');
                                jPackagesList.refresh( ! id);
                            }
                        }
                    }, $progress);
                },
                onShow: function ()
                {
                    formChk = new bff.formChecker($form);
                },
            };
        }());
        Packages.total_cost();
</script>