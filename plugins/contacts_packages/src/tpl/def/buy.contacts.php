<?php

?>
<div class="container">
    <section class="l-mainContent">
        <div class="row">
            <div class="col-md-12">
                <?= tpl::getBreadcrumbs($breadcrumbs); ?>
                <div class="mrgt50">

                    <h1 class="text-center"><?=_t('', 'Покупка пакета контактов [user]', ['user' => ($bClient)?  'исполнителя': 'заказчика'])?></h1>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <? if(!empty($user_packages_unlim)):?>
                                <span>
                        <?=_t('', 'У Вас оплачено <strong>[cnt]</strong> на контакты',
                            ['cnt' => tpl::declension(count($user_packages_unlim), _t('package', 'подписка;подписки;подписок'))]
                        );?>
                    </span>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col"><?=_t('', 'Название')?></th>
                                        <th scope="col"><?=_t('', 'Количество контактов')?></th>
                                        <th scope="col"><?=_t('', 'Действует до')?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <? foreach ($user_packages_unlim as $package):?>
                                        <tr>
                                            <th scope="row">
                                <span>
                                    "<?= $package['pack_settings']['title']?>"
                                </span>
                                            </th>
                                            <td>

                                <span class="show-tooltip"
                                      data-original-title="<?=_t('', 'Бесконечный список контактов')?>">
                                    <img style="width: 32px;" src="<?= bff::url('/img/infinity_b.svg') ?>">
                                </span>
                                            </td>
                                            <td>
                                <span>
                                    <?= $package['end_date'];?>
                                </span>
                                            </td>
                                        </tr>
                                    <? endforeach;?>
                                    </tbody>
                                </table>
                            <? endif;?>

                            <? if(!empty($user_packages)):?>
                                <span>
                            <?=_t('', 'У Вас оплачено <strong>[cnt]</strong> контактов',
                                ['cnt' => tpl::declension(count($user_packages), _t('package', 'пакет;пакета;пакетов'))]
                            );?>
                        </span>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col"><?=_t('', 'Название')?></th>
                                        <th scope="col"><?=_t('', 'Контактов осталось')?></th>
                                        <th scope="col"><?=_t('', 'Действует до')?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <? foreach ($user_packages as $package):?>
                                        <tr>
                                            <th scope="row">
                                <span>
                                    "<?= $package['pack_settings']['title']?>"
                                </span>
                                            </th>
                                            <td>
                                <span>
                                    <?= $package['left_quantity']._t('',' ( из [init_cnt])',
                                        ['init_cnt' => $package['pack_settings']['quantity']]);?>
                                </span>
                                            </td>
                                            <td>
                                <span>
                                    <?= $package['end_date'];?>
                                </span>
                                            </td>
                                        </tr>
                                    <? endforeach;?>
                                    </tbody>
                                </table>
                            <? endif;?>

                        </div>
                    </div>
                    <div>
                        <form method="post" action="" onsubmit="return false;" id="j-package-buy">
                            <?if(!empty($packages_unlim)):?>
                                <div>
                                    <p class="text-center">
                                        <?=_t('buy_contacts', 'Вам доступны для покупки подписки на открытие 
                                                           неограниченного количесва контактов [user] в <b> N дней </b>',
                                            ['user' => ($bClient)?  'исполнителя': 'заказчика'])?>
                                    </p>
                                </div>
                                <div class="services-list ">
                                    <? foreach ($packages_unlim as $pack):?>
                                        <? if($pack['enabled']):?>
                                            <div class="services-list__item-label">
                                                <a name="unlim-pack-<?= $pack['id'] ?>"></a>
                                                <label class="w100p h100p">
                                                    <input type="radio"
                                                           name="pack_id"
                                                           value="<?= $pack['id'] ?>"
                                                           href="javascript:void(0)"">
                                                    <span class="services-list__label">
                                                        <span class="services-list__title">
                                                            <?= _t('','Пакет') ?>
                                                            <?= _t('package','"[name]" ', ['name' => $pack['title']])?>
                                                        </span>
                                                        <?= _t('','Неограниченное количество контактов на') ?>
                                                        <?= tpl::declension($pack['period'], _t('package', 'день;дня;дней')) ?>
                                                        <span class="services-list__price mrgt10">
                                                            <?= (int)$pack['price_per_one'].' '.Site::currencyDefault()?>
                                                        </span>
                                                    </span>
                                                </label>
                                            </div>
                                        <?endif;?>
                                    <? endforeach;?>
                                </div>
                            <? endif;?>
                            <?if($bClient && !empty($packages)):?>
                                <div class="text-center">
                                    <?=_t('buy_contacts', 'Вам доступны для покупки пакеты на открытие <b>N контактов </b>
                                                           [user] в месяц',
                                        ['user' => ($bClient)?  'исполнителя': 'заказчика'])?>
                                </div>
                                <div class="services-list">
                                    <? foreach ($packages as $pack):?>
                                        <? if($pack['enabled']):?>
                                            <div class="services-list__item-label">
                                                <a name="days-pack-<?= $pack['id'] ?>"></a>
                                                <label class="w100p h100p">
                                                    <input type="radio" name="pack_id" value="<?= $pack['id'] ?>" href="javascript:void(0)">
                                                    <span class="services-list__label">
                                                        <span class="services-list__title">
                                                            <?= _t('','Пакет ');?>"<?= $pack['title']; ?>"
                                                        </span>
                                                            <?= tpl::declension($pack['quantity'], _t('package', 'контакт;контакта;контактов')) ?>
                                                            x
                                                            <?= tpl::declension(PACKAGES_PERIOD_DAYS, _t('package', 'день;дня;дней')) ?>
                                                            <span class="services-list__price mrgt10">
                                                            <?= (int)$pack['quantity']*(int)$pack['price_per_one'].' '.Site::currencyDefault()?>
                                                        </span>
                                                    </span>
                                                </label>
                                            </div>
                                        <?endif;?>
                                    <? endforeach;?>
                                </div>
                            <?endif;?>
                            <div class="text-center mrgt20">
                                <button class="btn btn-primary min-w-200px"
                                        onclick='Packages.buy_package(<?= func::php2js(['lang' => [
                                                'select_pack' => _t('svc', 'Выберите пакет'),
                                            ]]
                                        ) ?>)'>
                                    <?= _t('svc', 'Купить'); ?>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="hidden-xs">
                        <h6></h6>
                        <div class="table se-table">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>