<?php
?>
<?= tplAdmin::blockStart(_t('','Додавить пакет'), false, array('id'=>'PackagesFormBlock','style'=>'display:none;')); ?>
<div id="PackagesFormContainer"></div>
<?= tplAdmin::blockStop(); ?>

<?= tplAdmin::blockStart(_t('','Пакеты контактов'), true, array('id'=>'PackagesListBlock','class'=>(!empty($act) ? 'hidden' : '')),
    array('title'=>'+ добавить','class'=>'ajax','onclick'=>'return jPackagesFormManager.action(\'add\',0);'),
    array()
); ?>
<div class="actionBar">
    <form method="get" action="<?= $this->adminLink(NULL) ?>" id="PackagesListFilters" onsubmit="return false;" class="form-inline">
        <input type="hidden" name="s" value="<?= bff::$class ?>" />
        <input type="hidden" name="ev" value="<?= bff::$event ?>" />
        <div class="left">
        </div>
        <div class="right">
            <div id="PackagesProgress" class="progress" style="display: none;"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>

<table class="table table-condensed table-hover admtbl tblhover" id="PackagesListTable">
    <thead>
    <tr class="header nodrag nodrop">
        <th width="70"><?=_t('','ID');?></th>
        <th class="left"><?=_t('', 'Название');?></th>
        <th class="left"><?=_t('', 'Кол-во контактов');?></th>
        <th class="left"><?=_t('', 'Количество дней');?></th>
        <th class="left"><?=_t('', 'Цена подписки, [cur]', ['cur' => Site::currencyDefault()]);?></th>
        <th class="left"><?=_t('', 'Тип пользователя');?></th>
        <th width="135"><?=_t('', 'Действие');?></th>
    </tr>
    </thead>
    <tbody id="PackagesList">
    <?= $list ?>
    </tbody>
</table>

<?= tplAdmin::blockStop(); ?>

<div>
    <div class="left"></div>
    <div class="right desc" style="width:60px; text-align:right;">

    </div>
</div>

<script type="text/javascript">
    var jPackagesFormManager = (function(){
        var $progress, $block, $blockCaption, $formContainer, process = false;
        var ajaxUrl = '<?= $this->adminLink(bff::$event); ?>';

        $(function(){
            $formContainer = $('#PackagesFormContainer');
            $progress = $('#PackagesProgress');
            $block = $('#PackagesFormBlock');
            $blockCaption = $block.find('span.caption');

            <? if( ! empty($act)) { ?>action('<?= $act ?>',<?= $id ?>);<? } ?>
        });

        function onFormToggle(visible)
        {
            if(visible) {
                jPackagesList.toggle(false);
                if(jPackagesForm) jPackagesForm.onShow();
            } else {
                jPackagesList.toggle(true);
            }
        }

        function initForm(type, id, params)
        {
            if( process ) return;
            bff.ajax(ajaxUrl,params,function(data){
                if(data && (data.success || intval(params.save)===1)) {
                    $blockCaption.html((type == 'add' ? 'Добавление' : 'Редактирование'));
                    $formContainer.html(data.form);
                    $block.show();
                    $.scrollTo( $blockCaption, {duration:500, offset:-300});
                    onFormToggle(true);
                    if(bff.h) {
                        window.history.pushState({}, document.title, ajaxUrl + '&act='+type+'&id='+id);
                    }
                } else {
                    jPackagesList.toggle(true);
                }
            }, function(p){ process = p; $progress.toggle(); });
        }

        function action(type, id, params)
        {
            params = $.extend(params || {}, {act:type});
            switch(type) {
                case 'add':
                {
                    if( id > 0 ) return action('edit', id, params);
                    if($block.is(':hidden')) {
                        initForm(type, id, params);
                    } else {
                        action('cancel');
                    }
                } break;
                case 'cancel':
                {
                    $block.hide();
                    onFormToggle(false);
                } break;
                case 'edit':
                {
                    if( ! (id || 0) ) return action('add', 0, params);
                    params.id = id;
                    initForm(type, id, params);
                } break;
            }
            return false;
        }

        return {
            action: action
        };
    }());

    var jPackagesList =
        (function()
        {
            var $progress, $block, $list, $listTable, processing = false;
            var ajaxUrl = '<?= $this->adminLink(bff::$event.'&act='); ?>';

            $(function(){
                $progress  = $('#PackagesProgress');
                $block     = $('#PackagesListBlock');
                $list      = $block.find('#PackagesList');
                $listTable = $block.find('#PackagesListTable');
                filters    = $block.find('#PackagesListFilters').get(0);

                $list.delegate('a.package-edit', 'click', function(){
                    var id = intval($(this).data('id'));
                    if(id>0) jPackagesFormManager.action('edit',id);
                    return false;
                });

                $list.delegate('a.package-toggle', 'click', function(){
                    var id = intval($(this).data('id'));
                    var type = $(this).data('type');
                    if(id>0) {
                        var params = {progress: $progress, link: this};
                        bff.ajaxToggle(id, ajaxUrl+'toggle&type='+type+'&id='+id, params);
                    }
                    return false;
                });

                $list.delegate('a.package-del', 'click', function(){
                    var id = intval($(this).data('id'));
                    if(id>0) del(id, this);
                    return false;
                });

                $(window).bind('popstate',function(e){
                    if('state' in window.history && window.history.state === null) return;
                    var loc = document.location;
                    var actForm = /act=(add|edit)/.exec( loc.search.toString() );
                    if( actForm!=null ) {
                        var actId = /id=([\d]+)/.exec(loc.search.toString());
                        jPackagesFormManager.action(actForm[1], actId && actId[1]);
                    } else {
                        jPackagesFormManager.action('cancel');
                        updateList(false);
                    }
                });

            });

            function isProcessing()
            {
                return processing;
            }

            function del(id, link)
            {
                bff.ajaxDelete('Удалить?', id, ajaxUrl+'delete&id='+id, link, {progress: $progress, repaint: false});
                return false;
            }

            function updateList(updateUrl)
            {
                if(isProcessing()) return;
                var f = $(filters).serialize();
                bff.ajax(ajaxUrl, f, function(data){
                    if(data) {
                        $list.html( data.list );
                        if(updateUrl !== false && bff.h) {
                            window.history.pushState({}, document.title, $(filters).attr('action') + '?' + f);
                        }
                    }
                }, function(p){ $progress.toggle(); processing = p; $list.toggleClass('disabled'); });
            }

            return {
                submit: function(resetForm)
                {
                    if(isProcessing()) return false;

                    updateList();
                },
                page: function (id)
                {
                    if(isProcessing()) return false;
                    updateList();
                },
                refresh: function(resetPage, updateUrl)
                {
                    updateList(updateUrl);
                },
                toggle: function(show)
                {
                    if(show === true) {
                        $block.show();
                        if(bff.h) window.history.pushState({}, document.title, $(filters).attr('action') + '?' + $(filters).serialize());
                    }
                    else $block.hide();
                }
            };
        }());
</script>
