<?php

foreach ($list as $k=>&$v):
    $id = $v['id']; ?>
    <tr class="row<?= ($k%2) ?>">
        <td class="small"><?= $id ?></td>
        <td class="left"><?= $v['title'] ?></td>
        <td class="left"><?= $v['quantity'] ?></td>
        <td class="left"><?= $v['price_per_one'] ?></td>
        <td class="left"><?= (int)$v['quantity']*(int)$v['price_per_one'] ?></td>
        <td class="left"><?= $v['saving'] ?></td>
        <td>
            <a class="but <?= ($v['enabled']?'un':'') ?>block package-toggle" title="Включен" href="#" data-type="enabled" data-id="<?= $id ?>"></a>
            <a class="but edit package-edit" title="<?=_t('','Редактировать');?>" href="#" data-id="<?= $id ?>"></a>
            <a class="but del package-del" title="Удалить" href="#" data-id="<?= $id ?>"></a>
        </td>
    </tr>
<? endforeach; unset($v);

if (empty($list) && ! isset($skip_norecords)): ?>
    <tr class="norecords">
        <td colspan="8">
            <?=_t('','ничего не найдено');?>
        </td>
    </tr>
<? endif;