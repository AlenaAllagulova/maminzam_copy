<?php

class Packages extends PackagesBase
{

    public function buy_page()
    {
        $aData = [];
        $aData['breadcrumbs'] = array(
            array('title' => _t('','Profile'), 'link' => Users::url('my.profile')),
            array('title' => _t('users', 'Платные сервисы'), 'link' => Svc::url('list')),
            array('title' => _t('packages', 'Покупка пакета контактов'), 'active' => true),
        );

        $aData['nUserID'] = User::id();
        $aData['user_packages'] = $this->model->getUserContactsPackages($aData['nUserID']);
        $aData['user_packages'] = $this->prepareBoughtPackageListing($aData['user_packages']);
        $aData['user_packages_unlim'] = $this->model->getUserContactsPackages($aData['nUserID'], '', '', true);
        $aData['user_packages_unlim'] = $this->prepareBoughtPackageListing($aData['user_packages_unlim']);

        $aData['bClient'] = (Users::useClient() && User::isClient());
        if($aData['bClient'] ){
            # пакеты с ограничениями по контактам на месяц доступные для покупки
            $aData['packages'] = $this->model->packagesContactsListing(['quantity > 0', 'enabled'   => true], false, '', 'P.quantity');
        }
        # пакеты без ограничения на открытие контактов на Nдней доступные для покупки
        $nCurrentUserType = User::isClient()? Users::TYPE_CLIENT : Users::TYPE_WORKER;
        $aFilter = [
            'quantity'  => PACKAGES_UNLIM_QUANTITY,
            'user_type' => $nCurrentUserType,
            'enabled'   => true,
        ];
        $aData['packages_unlim'] = $this->model->packagesContactsListing($aFilter, false, '', 'P.period');


        return $this->viewPHP($aData, 'buy.contacts');
    }

    public function buyContactsPackage()
    {
        $aResponse = [];
        $nUserID = User::id();

        if (!$nUserID || !Request::isAJAX()) {
            $this->errors->accessDenied();
            $this->ajaxResponseForm($aResponse);
        }

        $nPackageID = $this->input->getpost('pack_id', TYPE_UINT);

        if (!$nPackageID) {
            $this->errors->set(_t('', 'Выберите пакет контактов'));
            $this->ajaxResponseForm($aResponse);
        }

        # проверка пакета
        $aPackageData = $this->model->packageData($nPackageID);

        if (empty($aPackageData)  || !$aPackageData['enabled']){
            $this->errors->set(_t('', 'Пакет не доступен для покупки'));
            $this->errors->reloadPage();
            $this->ajaxResponseForm($aResponse);
        }

        $bClient = (Users::useClient() && User::isClient());
        $bPackageUnlim = ($aPackageData['quantity'] == PACKAGES_UNLIM_QUANTITY);

        if (!$bClient && !$bPackageUnlim ){
            $this->errors->set(_t('', 'Пакет контактов на месяц доступен только для заказчиков'));
            $this->ajaxResponseForm($aResponse);
        }

        if ( ($bClient  && $aPackageData['user_type'] != Users::TYPE_CLIENT && $bPackageUnlim) ||
             (!$bClient && $aPackageData['user_type'] != Users::TYPE_WORKER && $bPackageUnlim)){
            $this->errors->set(_t('', 'Подписка на контакты в данном пакете доступна только для [user]',
                                      ['user' => ($bClient)?  'заказчика' : 'исполнителя']));
            $this->ajaxResponseForm($aResponse);
        }

        # проверка баланса
        $aBalLink = Bills::url('my.history');
        $nBalance = User::balance();
        $sTitleCurrDefault = Site::currencyDefault();

        if ( !$bPackageUnlim) {
            $nTotalSum = (int)$aPackageData['quantity'] * (int)$aPackageData['price_per_one'];
            $sCnt = tpl::declension($aPackageData['quantity'], 'анкета;анкеты;анкет');
            $sMsg = 'пакета контактов';
        } else {
            $nTotalSum = (int)$aPackageData['price_per_one'];
            $sCnt = tpl::declension($aPackageData['period'], 'день;дня;дней');
            $sMsg = 'пакета подписки на контакты';
        }

        if ($nTotalSum > $nBalance) {
            $this->errors->set(_t('specs_comission', "Недостаточно средств для оплаты пакета контактов.
                                                        <a href='{$aBalLink}'>Пополните счет</a> ,
                                                        Ваш баланс : {$nBalance} {$sTitleCurrDefault}"));
            $this->ajaxResponseForm($aResponse);
            return false;
        }

        $sBillDescr = _t('package_contacts', 'Оплата [msg] "[name]" - [cnt] за [total] [cur]',
            [
                'msg'   => $sMsg,
                'cnt'   => $sCnt,
                'name'  => $aPackageData['title'],
                'total' => $nTotalSum,
                'cur'   => Site::currencyDefault()
            ]);
        # 1) создаем закрытый счет снятия комиссии по заказу
        $nBillID = Bills::i()->createBill_OutService(0, 0, $nUserID, ($nBalance - $nTotalSum),
            $nTotalSum, $nTotalSum, Bills::STATUS_COMPLETED, $sBillDescr, []);

        # 2) снимаем деньги со счета пользователя
        $bSuccess = $this->bills()->updateUserBalance($nUserID, $nTotalSum, false);

        if (!$nBillID || !$bSuccess) {
            $this->errors->set(_t('specs_comission', "Ошибка оплаты открытия анкет "));
            $this->ajaxResponseForm($aResponse);
        }

        if($bSuccess && $this->errors->no()){
            # сохраняем купленный пакет контактов
            $this->model->saveUserPackage($nUserID, $this->prepareBoughtPackageData($aPackageData, $bPackageUnlim));
        }

        $aResponse['success'] = $this->errors->no();
        if ($aResponse['success']) {
            $aResponse['success_msg'] = _t('', 'Оплата пакета "[name]" прошла успешно', ['name'  => $aPackageData['title']]);
            $aResponse['redirect'] = false;
        }

        $this->ajaxResponseForm($aResponse);
    }

    public static function getUserActualPackage($nCurrentUserId, $bPackageUnlim = false)
    {
        $aResult = self::model()->getUserContactsPackages($nCurrentUserId, 'LIMIT 1', '', $bPackageUnlim);

        if(empty($aResult)){
            return false;
        }

        return reset($aResult);
    }

    public static function updateUserActualPackage($nCurrentUserId, $nPackageID, $bPackageUnlim = false)
    {
        return self::model()->updateUserActualPackage($nCurrentUserId, $nPackageID, $bPackageUnlim);

    }

    public static function getUserAllActualPackagesAndCounters()
    {
        $aPackages = self::model()->getUserContactsPackages(User::id());
        $aPackages = PackagesBase::i()->prepareBoughtPackageListing($aPackages);
        if (empty($aPackages)){
            return false;
        }
        $aPackages['all_left_quantity'] = 0;
        foreach ($aPackages as $package){
            $aPackages['all_left_quantity'] += $package['left_quantity'];
        }
        return $aPackages;
    }

}