<?php

abstract class PluginLoaderContactsPackages
{
    private static $instance = null;

    /**
     * @return Plugin
     * @throws Exception
     */
    public static function getPluginInstance() {
        if(is_null(self::$instance))
            throw new Exception('Plugin Contacts_packages instance not set');
        return self::$instance;
    }

    /**
     * @param Plugin $oPlugin
     */
    public static function setPluginInstance($oPlugin) {
        self::$instance = $oPlugin;
    }
}