<?php

use bff\db\migrations\Migration as Migration;

class ExtP2ec3fce1898821ae31f9ff47d5a2330e42ea1d7V1x0x1 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(TABLE_PACKAGES_CONTACTS)
             ->changeColumn('quantity', 'integer', ['signed' => true])
             ->save();
        $this->table(TABLE_USERS_PACKAGES)
             ->changeColumn('left_quantity', 'integer', ['signed' => true])
             ->save();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->table(TABLE_PACKAGES_CONTACTS)
             ->changeColumn('quantity', 'integer', ['signed' => false])
             ->save();
        $this->table(TABLE_USERS_PACKAGES)
             ->changeColumn('left_quantity', 'integer', ['signed' => false])
             ->save();
    }
}