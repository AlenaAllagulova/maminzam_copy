<?php

use bff\db\migrations\Migration as Migration;

class ExtP2ec3fce1898821ae31f9ff47d5a2330e42ea1d7V1x0x0 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(
            TABLE_PACKAGES_CONTACTS,
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', ['signed' => false, 'identity' => true])
            ->addColumn('title', 'string')
            ->addColumn('quantity', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('price_per_one', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('saving', 'string')
            ->addColumn('user_type', 'integer', ['signed' => false, 'limit' => 2, 'default' => Users::TYPE_WORKER])
            ->addColumn('period', 'integer', ['signed' => false, 'null' => false, 'default' => PACKAGES_PERIOD_DAYS])
            ->addColumn('enabled', 'boolean', ['default' => true])
            ->create();

        $this->table(
            TABLE_USERS_PACKAGES,
            ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', ['signed' => false, 'identity' => true])
            ->addColumn('user_id', 'integer', ['signed' => false, 'null' => false])
            ->addColumn('left_quantity', 'integer', ['signed' => false, 'null' => true])
            ->addColumn('end_date', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('pack_settings', 'text')
            ->addForeignKey(
                ['user_id'],
                TABLE_USERS,
                'user_id',
                ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
            ->create();

        # should exist a table bff_users_opened_contacts with fields user_id, opened_user_id in theme "custom"
        if ($this->table(TABLE_USERS_OPENED_CONTACTS)->exists()) {
            $this->table(TABLE_USERS_OPENED_CONTACTS)
                ->addColumn('id_pack', 'integer', ['signed' => false, 'null' => true])
                ->addColumn('date_opened', 'datetime', ['null' => true])
                ->update();
        } else {
            $this->table(
                DB_PREFIX . 'users_opened_contacts',
                ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['user_id', 'opened_user_id']])
                ->addColumn('user_id', 'integer', ['signed' => false, 'null' => false])
                ->addColumn('opened_user_id', 'integer', ['signed' => false, 'null' => false])
                ->addColumn('id_pack', 'integer', ['signed' => false, 'null' => true])
                ->addColumn('date_opened', 'datetime', ['null' => true])
                ->addForeignKey(
                    ['user_id'],
                    TABLE_USERS,
                    'user_id',
                    ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
                ->create();
        }


    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->dropIfExists(TABLE_PACKAGES_CONTACTS);
        $this->dropIfExists(TABLE_USERS_PACKAGES);
        $this->table(DB_PREFIX . 'users_opened_contacts')->removeColumn('id_pack')->update();
        $this->table(DB_PREFIX . 'users_opened_contacts')->removeColumn('date_opened')->update();
    }
}